package com.vanmarcke.saferpay.api.populators;

import com.vanmarcke.saferpay.api.data.*;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.store.BaseStoreModel;

public class PaymentPageInitializeRequestPopulator<SOURCE extends AbstractOrderModel> implements Populator<SOURCE, PaymentPageInitializeRequest> {

    private final Converter<SOURCE, RequestHeader> requestHeaderConverter;
    private final Converter<SOURCE, Payment> paymentConverter;
    private final Converter<SOURCE, ReturnUrls> returnUrlsConverter;
    private final Converter<SOURCE, PendingNotification> notificationConverter;

    public PaymentPageInitializeRequestPopulator(final Converter<SOURCE, RequestHeader> requestHeaderConverter,
                                                 final Converter<SOURCE, Payment> paymentConverter,
                                                 final Converter<SOURCE, ReturnUrls> returnUrlsConverter,
                                                 final Converter<SOURCE, PendingNotification> notificationConverter) {
        this.requestHeaderConverter = requestHeaderConverter;
        this.paymentConverter = paymentConverter;
        this.returnUrlsConverter = returnUrlsConverter;
        this.notificationConverter = notificationConverter;
    }

    @Override
    public void populate(final SOURCE source, final PaymentPageInitializeRequest target) {
        final BaseStoreModel store = source.getStore();

        target.setRequestHeader(requestHeaderConverter.convert(source));
        target.setTerminalId(store.getSaferpayTerminalId());
        target.setPayment(paymentConverter.convert(source));
        target.setReturnUrls(returnUrlsConverter.convert(source));
        target.setNotification(notificationConverter.convert(source));
    }

}