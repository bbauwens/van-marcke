package com.vanmarcke.saferpay.api.commands;

import com.vanmarcke.saferpay.api.data.PaymentPageInitializeRequest;
import com.vanmarcke.saferpay.api.data.PaymentPageInitializeResponse;
import eu.elision.integration.command.configuration.AuthenticationType;
import eu.elision.integration.command.configuration.annotation.CommandConfig;
import org.springframework.http.HttpMethod;

/**
 * Command to start a transaction with the Payment Page.
 *
 * @author Taki Korovessis
 * @since 24-01-2020
 */
@CommandConfig(serviceName = "paymentPageInitialize", authenticationType = AuthenticationType.BASIC, httpMethod = HttpMethod.POST, url = "/Payment/v1/PaymentPage/Initialize")
public class PaymentPageInitializeCommand extends AbstractCommand<PaymentPageInitializeResponse> {


    /**
     * Creates a new instance of the {@link PaymentPageInitializeCommand} class.
     *
     * @param baseURL  the base URL
     * @param username the username
     * @param password the password
     * @param request  the request
     */
    public PaymentPageInitializeCommand(final String baseURL,
                                        final String username,
                                        final String password,
                                        final PaymentPageInitializeRequest request) {
        super(baseURL, username, password);
        setPayLoad(request);
    }
}