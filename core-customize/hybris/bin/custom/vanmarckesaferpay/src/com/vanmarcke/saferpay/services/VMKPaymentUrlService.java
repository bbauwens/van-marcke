package com.vanmarcke.saferpay.services;

import de.hybris.platform.core.model.order.AbstractOrderModel;

/**
 * Provides methods to create callback URLs when communicating with the external payment provider.
 *
 * @author Tom van den Berg
 * @since 03-02-2021
 */
public interface VMKPaymentUrlService {

    /**
     * Creates an url for the given cart and
     *
     * @param cart       the cart
     * @param urlPathKey the URL path key
     * @return the url
     */
    String getUrlForCart(AbstractOrderModel cart, String urlPathKey);
}
