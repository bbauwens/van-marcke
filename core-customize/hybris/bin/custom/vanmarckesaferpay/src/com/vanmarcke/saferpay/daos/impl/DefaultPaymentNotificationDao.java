package com.vanmarcke.saferpay.daos.impl;

import com.vanmarcke.saferpay.daos.PaymentNotificationDao;
import com.vanmarcke.saferpay.model.SaferpayNotificationModel;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;

import java.util.List;

/**
 * Implementation for PaymentNotificationDao
 */
public class DefaultPaymentNotificationDao extends DefaultGenericDao<SaferpayNotificationModel> implements PaymentNotificationDao {

    /**
     * Constructor for {@link DefaultPaymentNotificationDao}
     */
    public DefaultPaymentNotificationDao() {
        super(SaferpayNotificationModel._TYPECODE);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<SaferpayNotificationModel> findAllPaymentNotifications() {
        return find();
    }
}
