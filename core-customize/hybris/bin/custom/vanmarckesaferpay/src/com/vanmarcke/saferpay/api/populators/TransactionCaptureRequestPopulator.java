package com.vanmarcke.saferpay.api.populators;

import com.vanmarcke.saferpay.api.data.RequestHeader;
import com.vanmarcke.saferpay.api.data.TransactionCaptureRequest;
import com.vanmarcke.saferpay.api.data.TransactionReference;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

public class TransactionCaptureRequestPopulator<SOURCE extends AbstractOrderModel> implements Populator<SOURCE, TransactionCaptureRequest> {

    private final Converter<SOURCE, RequestHeader> requestHeaderConverter;
    private final Converter<SOURCE, TransactionReference> transactionReferenceConverter;

    public TransactionCaptureRequestPopulator(final Converter<SOURCE, RequestHeader> requestHeaderConverter,
                                              final Converter<SOURCE, TransactionReference> transactionReferenceConverter) {
        this.requestHeaderConverter = requestHeaderConverter;
        this.transactionReferenceConverter = transactionReferenceConverter;
    }

    @Override
    public void populate(final SOURCE source, final TransactionCaptureRequest target) {
        target.setRequestHeader(requestHeaderConverter.convert(source));
        target.setTransactionReference(transactionReferenceConverter.convert(source));
    }

}