package com.vanmarcke.saferpay.services;

import com.vanmarcke.saferpay.exceptions.PaymentValidationException;
import com.vanmarcke.saferpay.facades.data.CartTokenData;
import de.hybris.platform.core.model.order.OrderModel;

/**
 * Defines methods for polling the order creation process.
 *
 * @author Tom van den Berg
 * @since 06-02-2021
 */
public interface VMKPaymentOrderPollingService {

    /**
     * Poll the order creation process the for its status.
     * If the status is {@code ORDER_CREATION_IN_PROGRESS} poll again after a specified timeout.
     *
     * @param cartToken the cart token
     * @return the order model
     */
    OrderModel pollOrderCreationProcess(CartTokenData cartToken) throws PaymentValidationException;
}
