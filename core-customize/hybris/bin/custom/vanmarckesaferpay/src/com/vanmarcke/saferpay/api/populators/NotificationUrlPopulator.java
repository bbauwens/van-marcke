package com.vanmarcke.saferpay.api.populators;

import com.vanmarcke.saferpay.api.data.PendingNotification;
import com.vanmarcke.saferpay.services.VMKPaymentUrlService;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;

/**
 * Populator for {@link PendingNotification}
 *
 * @param <SOURCE> which can be either {@link CartModel} or {@link OrderModel}
 * @author Tom van den Berg
 * @since 03-02-2021
 */
public class NotificationUrlPopulator<SOURCE extends AbstractOrderModel> implements Populator<SOURCE, PendingNotification> {

    private static final String CONFIG_URL_PATH_NOTIFY = "saferpay.paymentpage.url.path.notify";

    private final VMKPaymentUrlService paymentUrlService;

    /**
     * Creates an instance of the {@link NotificationUrlPopulator} class.
     *
     * @param paymentUrlService the payment url service
     */
    public NotificationUrlPopulator(VMKPaymentUrlService paymentUrlService) {
        this.paymentUrlService = paymentUrlService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void populate(SOURCE source, PendingNotification target) {
        target.setNotifyUrl(paymentUrlService.getUrlForCart(source, CONFIG_URL_PATH_NOTIFY));
    }
}