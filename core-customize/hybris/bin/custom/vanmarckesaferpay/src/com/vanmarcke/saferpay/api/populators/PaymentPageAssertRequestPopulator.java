package com.vanmarcke.saferpay.api.populators;

import com.vanmarcke.saferpay.api.data.PaymentPageAssertRequest;
import com.vanmarcke.saferpay.api.data.RequestHeader;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import static com.vanmarcke.core.util.PaymentTransactionUtils.getCurrentPaymentTransaction;

public class PaymentPageAssertRequestPopulator<SOURCE extends AbstractOrderModel> implements Populator<SOURCE, PaymentPageAssertRequest> {

    private final Converter<SOURCE, RequestHeader> requestHeaderConverter;

    public PaymentPageAssertRequestPopulator(final Converter<SOURCE, RequestHeader> requestHeaderConverter) {
        this.requestHeaderConverter = requestHeaderConverter;
    }

    @Override
    public void populate(final SOURCE source, final PaymentPageAssertRequest target) {
        final PaymentTransactionModel paymentTransactionModel = getCurrentPaymentTransaction(source.getPaymentTransactions());

        target.setRequestHeader(requestHeaderConverter.convert(source));
        target.setToken(paymentTransactionModel == null ? null : paymentTransactionModel.getRequestToken());
    }

}