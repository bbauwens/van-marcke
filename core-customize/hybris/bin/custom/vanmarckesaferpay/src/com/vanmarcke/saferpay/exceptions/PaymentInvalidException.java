package com.vanmarcke.saferpay.exceptions;

/**
 * Exception used within the context of processing external payment provider communications.
 */
public class PaymentInvalidException extends SaferpayException {

    /**
     * Provides an instance of the PaymentInvalidException.
     *
     * @param message the message
     */
    public PaymentInvalidException(String message) {
        super(message);
    }
}
