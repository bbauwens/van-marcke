package com.vanmarcke.saferpay.daos;

import com.vanmarcke.saferpay.model.SaferpayNotificationModel;

import java.util.List;

/**
 * Dao for functionality regarding Payment Notifications
 */
public interface PaymentNotificationDao {

    /**
     * Find all the Payment Notifications
     *
     * @return List with all {@link SaferpayNotificationModel}
     */
    List<SaferpayNotificationModel> findAllPaymentNotifications();
}
