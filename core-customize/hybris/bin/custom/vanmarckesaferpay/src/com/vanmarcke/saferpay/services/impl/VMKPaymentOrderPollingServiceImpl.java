package com.vanmarcke.saferpay.services.impl;

import com.vanmarcke.core.order.VMKOrderDao;
import com.vanmarcke.saferpay.exceptions.PaymentValidationException;
import com.vanmarcke.saferpay.facades.data.CartTokenData;
import com.vanmarcke.saferpay.services.VMKPaymentOrderPollingService;
import de.hybris.platform.core.model.order.OrderModel;
import org.apache.log4j.Logger;

import java.util.concurrent.TimeUnit;

/**
 * Implements methods for polling the order creation process.
 *
 * @author Tom van den Berg
 * @since 06-02-2021
 */
public class VMKPaymentOrderPollingServiceImpl implements VMKPaymentOrderPollingService {

    private static final Logger LOG = Logger.getLogger(VMKPaymentOrderPollingServiceImpl.class);

    private static final String PAYMENT_PROCESS_SEVERE_ERROR = "checkout.severe.error.process.payment";

    private final VMKOrderDao orderDao;
    private final int numberOfSeconds;

    /**
     * Provides an instance of the VMKPaymentOrderPollingServiceImpl.
     *
     * @param orderDao the order DAO
     */
    public VMKPaymentOrderPollingServiceImpl(VMKOrderDao orderDao, int numberOfSeconds) {
        this.numberOfSeconds = numberOfSeconds;
        this.orderDao = orderDao;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OrderModel pollOrderCreationProcess(CartTokenData cartToken) throws PaymentValidationException {
        OrderModel order = orderDao.findOrderByGuidAndSite(cartToken.getCartUid(), cartToken.getBaseSite());
        int currentIteration = 0;

        while (order == null && currentIteration++ < numberOfSeconds) {
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException ex) {
                LOG.debug("Interruption while polling for order.");
                Thread.currentThread().interrupt();
            }
            order = orderDao.findOrderByGuidAndSite(cartToken.getCartUid(), cartToken.getBaseSite());
        }

        if (order == null) {
            LOG.error(String.format("Order could not be found after polling for %s seconds.", numberOfSeconds));
            throw new PaymentValidationException(PAYMENT_PROCESS_SEVERE_ERROR);
        }
        return order;
    }
}
