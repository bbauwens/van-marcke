package com.vanmarcke.saferpay.facades.impl;

import com.vanmarcke.saferpay.facades.PaymentNotificationFacade;
import com.vanmarcke.saferpay.model.SaferpayNotificationModel;
import de.hybris.platform.servicelayer.model.ModelService;

/**
 * Implementation for PaymentNotificationFacade
 */
public class VMKPaymentNotificationFacade implements PaymentNotificationFacade {

    private final ModelService modelService;

    /**
     * Constructor for {@link PaymentNotificationFacade}
     *
     * @param modelService the model service
     */
    public VMKPaymentNotificationFacade(ModelService modelService) {
        this.modelService = modelService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SaferpayNotificationModel createNotificationForCartToken(String token) {
        SaferpayNotificationModel notification = modelService.create(SaferpayNotificationModel.class);
        notification.setCartToken(token);
        modelService.save(notification);
        return notification;
    }
}
