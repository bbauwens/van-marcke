package com.vanmarcke.saferpay.facades;

import com.vanmarcke.saferpay.exceptions.SaferpayException;
import de.hybris.platform.commercefacades.order.data.OrderData;

/**
 * The Payment Page Interface
 */
public interface VMKPaymentFacade {

    /**
     * Initializes the Payment and generates the RedirectUrl for the Payment Page
     *
     * @return the Redirect URL
     */
    String initializePayment();

    /**
     * Processes a payment confirmation from an external payment provider.
     * <p>
     * For saferpay, this confirmation is received through a /success or /notify callback.
     *
     * @param token the token
     * @return order data
     * @throws SaferpayException
     */
    OrderData processPaymentConfirmation(String token) throws SaferpayException;
}