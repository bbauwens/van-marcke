package com.vanmarcke.saferpay.api.commands;

import eu.elision.integration.command.AbstractRESTCommand;
import eu.elision.integration.command.Response;
import eu.elision.integration.command.configuration.AuthenticationType;
import eu.elision.integration.command.impl.ResponseImpl;
import eu.elision.integration.interceptors.LogClientHttpRequestInterceptor;
import org.apache.commons.collections4.MapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Map;

import static java.util.Arrays.asList;
import static org.springframework.http.MediaType.APPLICATION_JSON;

public abstract class AbstractCommand<T> extends AbstractRESTCommand<T> {

    private static final Logger LOG = LoggerFactory.getLogger(AbstractCommand.class);

    private final String baseURL;
    private final String username;
    private final String password;

    /**
     * Serves as a starting point for the classes that inherit from the {@link AbstractCommand} class.
     *
     * @param baseURL  the base URL
     * @param username the username
     * @param password the password
     */
    public AbstractCommand(final String baseURL,
                           final String username,
                           final String password) {
        this.baseURL = baseURL;
        this.username = username;
        this.password = password;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void buildHttpHeaders(final HttpHeaders httpHeaders) {
        httpHeaders.setAccept(asList(APPLICATION_JSON, APPLICATION_JSON));
        httpHeaders.setContentType(APPLICATION_JSON);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String buildUri(final Map<String, Object> serviceProperties) {
        UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder.fromUriString(baseURL + getUrl());
        if (MapUtils.isNotEmpty(requestParams)) {
            for (Map.Entry<String, String> requestParam : requestParams.entrySet()) {
                uriComponentsBuilder.queryParam(requestParam.getKey(), requestParam.getValue());
            }
        }
        return uriComponentsBuilder.build().toUriString();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Response<T> execute(final Map<String, Object> serviceProperties) {
        Response<T> response = new ResponseImpl<>();

        restTemplate.setRequestFactory(buildRequestFactory(serviceProperties));
        restTemplate.getInterceptors().add(new LogClientHttpRequestInterceptor());

        if (AuthenticationType.BASIC.equals(this.authenticationType)) {
            restTemplate.getInterceptors().add(new BasicAuthorizationInterceptor(username, password));
        }

        buildHttpHeaders(httpHeaders);

        final String uri = buildUri(serviceProperties);

        try {
            HttpEntity<Object> request = null;
            if (HttpMethod.GET.equals(httpMethod)) {
                request = new HttpEntity<>(httpHeaders);
            } else if (HttpMethod.POST.equals(httpMethod) || HttpMethod.PUT.equals(httpMethod)) {
                request = new HttpEntity<>(getPayLoad(), httpHeaders);
            }

            ResponseEntity<T> exchangeResponse = restTemplate.exchange(uri, httpMethod, request, getResponseType(), getPathParams());
            response.setHttpStatus(exchangeResponse.getStatusCode());
            T body = exchangeResponse.getBody();
            if (body != null) {
                response.setPayLoad(body);
            }
        } catch (Exception e) {
            LOG.error("Exception during command execution: " + e.getMessage(), e);
        }
        return response;
    }
}