package com.vanmarcke.saferpay.strategies.impl;

import com.vanmarcke.saferpay.strategies.PaymentTransactionStrategy;
import de.hybris.platform.payment.PaymentService;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.dto.TransactionStatusDetails;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.internal.service.AbstractBusinessService;
import org.springframework.beans.factory.annotation.Required;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

import static com.google.common.base.Preconditions.checkArgument;
import static com.vanmarcke.saferpay.constants.VanmarckesaferpayConstants.PAYMENT_PROVIDER;
import static com.vanmarcke.saferpay.constants.VanmarckesaferpayConstants.TRANSACTION_STATUS_PENDING;
import static org.apache.commons.collections4.MapUtils.getObject;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

public class DefaultPaymentTransactionStrategy extends AbstractBusinessService implements PaymentTransactionStrategy {

    private transient CommonI18NService commonI18NService;
    private transient PaymentService paymentService;
    private transient Map<String, PaymentTransactionType> transactionStatusMap;

    @Override
    public PaymentTransactionModel savePaymentTransaction(final String merchantCode, final String token) {
        checkArgument(isNotBlank(merchantCode), "Parameter merchantCode cannot be blank");
        checkArgument(isNotBlank(token), "Parameter token cannot be blank");

        final PaymentTransactionModel transactionModel = getModelService().create(PaymentTransactionModel.class);
        transactionModel.setCode(merchantCode);
        transactionModel.setRequestToken(token);
        transactionModel.setPaymentProvider(PAYMENT_PROVIDER);
        getModelService().save(transactionModel);

        return transactionModel;
    }

    @Override
    public void savePaymentTransactionEntry(final PaymentTransactionModel paymentTransaction, final String transactionId, final String transactionStatus, final Date transactionDate, final BigDecimal amount, final String currency) {
        checkArgument(paymentTransaction != null, "Parameter paymentTransaction cannot be null");
        checkArgument(transactionId != null, "Parameter transactionId cannot be null");
        checkArgument(transactionStatus != null, "Parameter transactionStatus cannot be null");
        checkArgument(transactionDate != null, "Parameter transactionDate cannot be null");
        checkArgument(currency != null, "Parameter currency cannot be null");

        // create transaction entry
        final PaymentTransactionType paymentTransactionType = getObject(transactionStatusMap, transactionStatus);
        final PaymentTransactionEntryModel transactionEntryModel = getModelService().create(PaymentTransactionEntryModel.class);
        transactionEntryModel.setType(paymentTransactionType);
        transactionEntryModel.setTime(transactionDate);
        transactionEntryModel.setPaymentTransaction(paymentTransaction);
        transactionEntryModel.setRequestId(transactionId);
        if (TRANSACTION_STATUS_PENDING.equals(transactionStatus)) {
            transactionEntryModel.setTransactionStatus(TransactionStatus.REVIEW.name());
            transactionEntryModel.setTransactionStatusDetails(TransactionStatusDetails.REVIEW_NEEDED.name());
        } else {
            transactionEntryModel.setTransactionStatus(TransactionStatus.ACCEPTED.name());
            transactionEntryModel.setTransactionStatusDetails(TransactionStatusDetails.SUCCESFULL.name());
        }
        transactionEntryModel.setCode(paymentService.getNewPaymentTransactionEntryCode(paymentTransaction, paymentTransactionType));
        transactionEntryModel.setAmount(amount);
        transactionEntryModel.setCurrency(commonI18NService.getCurrency(currency));
        getModelService().save(transactionEntryModel);
        // update transaction
        paymentTransaction.setRequestId(transactionId);
        getModelService().save(paymentTransaction);
        // refresh transaction
        getModelService().refresh(paymentTransaction);
    }

    @Required
    public void setCommonI18NService(final CommonI18NService commonI18NService) {
        this.commonI18NService = commonI18NService;
    }

    @Required
    public void setPaymentService(final PaymentService paymentService) {
        this.paymentService = paymentService;
    }

    @Required
    public void setTransactionStatusMap(final Map<String, PaymentTransactionType> transactionStatusMap) {
        this.transactionStatusMap = transactionStatusMap;
    }
}