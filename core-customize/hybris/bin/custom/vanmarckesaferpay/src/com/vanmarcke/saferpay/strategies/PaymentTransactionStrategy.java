package com.vanmarcke.saferpay.strategies;

import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Strategy for payment transactions business logic.
 */
public interface PaymentTransactionStrategy {

    /**
     * Creates {@link PaymentTransactionModel} with the specified merchant code and token
     *
     * @param merchantCode the merchantCode
     * @param token        the token
     * @return newly created {@link PaymentTransactionModel}
     */
    PaymentTransactionModel savePaymentTransaction(String merchantCode, String token);

    /**
     * Creates a new {@link PaymentTransactionEntryModel} for the specified parameters.
     *
     * @param paymentTransaction the paymentTransaction
     * @param transactionId      the transactionId
     * @param transactionStatus  the transactionStatus
     * @param transactionDate    the transactionDate
     * @param amount             the amount
     * @param currency           the currency
     */
    void savePaymentTransactionEntry(PaymentTransactionModel paymentTransaction, String transactionId, String transactionStatus, Date transactionDate, BigDecimal amount, String currency);
}