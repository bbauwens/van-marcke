package com.vanmarcke.saferpay.facades.impl;

import com.vanmarcke.saferpay.exceptions.PaymentValidationException;
import com.vanmarcke.saferpay.exceptions.SaferpayException;
import com.vanmarcke.saferpay.facades.VMKPaymentFacade;
import com.vanmarcke.saferpay.facades.data.CartTokenData;
import com.vanmarcke.saferpay.services.VMKPaymentOrderService;
import com.vanmarcke.saferpay.services.VMKPaymentService;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commerceservices.strategies.CheckoutCustomerStrategy;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.util.Assert;


/**
 * Default implementation for {@link VMKPaymentFacade}.
 *
 * @author Taki Korovessis, Tom van den Berg
 * @since 25-1-2020
 */
public class VMKPaymentFacadeImpl implements VMKPaymentFacade {

    private static final String PAYMENT_PROCESS_SEVERE_ERROR = "checkout.severe.error.process.payment";

    private static final Logger LOG = Logger.getLogger(VMKPaymentFacadeImpl.class);

    private final CartService cartService;
    private final CheckoutCustomerStrategy checkoutCustomerStrategy;
    private final VMKPaymentService paymentPageService;
    private final VMKPaymentOrderService paymentOrderService;
    private final Converter<String, CartTokenData> cartTokenConverter;

    /**
     * Creates a new instance of {@link VMKPaymentFacadeImpl}.
     *
     * @param cartService              the cartService
     * @param checkoutCustomerStrategy the checkoutCustomerStrategy
     * @param paymentPageService       the paymentPageService
     * @param paymentOrderService      the payment order service
     * @param cartTokenConverter       the cart token converter
     */
    public VMKPaymentFacadeImpl(CartService cartService,
                                CheckoutCustomerStrategy checkoutCustomerStrategy,
                                VMKPaymentService paymentPageService,
                                VMKPaymentOrderService paymentOrderService,
                                Converter<String, CartTokenData> cartTokenConverter) {
        this.cartService = cartService;
        this.checkoutCustomerStrategy = checkoutCustomerStrategy;
        this.paymentPageService = paymentPageService;
        this.paymentOrderService = paymentOrderService;
        this.cartTokenConverter = cartTokenConverter;
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public String initializePayment() {
        if (checkIfCurrentUserIsTheCartUser()) {
            CartModel cart = getSessionCart();
            return paymentPageService.initializePayment(cart);
        }
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OrderData processPaymentConfirmation(String token) throws SaferpayException {
        CartTokenData cartToken = convertToken(token);
        OrderData orderData = paymentOrderService.checkForExistingOrder(cartToken);
        if (orderData == null) {
            orderData = paymentOrderService.placeOrderForCartToken(cartToken);
        }
        return orderData;
    }

    /**
     * Converts the given token to a {@link CartTokenData} instance.
     *
     * @param token the token
     * @return the cart token data instance
     */
    protected CartTokenData convertToken(String token) throws PaymentValidationException {
        Assert.notNull(token, "The cart token is empty");
        CartTokenData cartTokenData = cartTokenConverter.convert(token);
        if (StringUtils.isEmpty(cartTokenData.getCartUid()) || cartTokenData.getBaseSite() == null) {
            LOG.error(String.format("Error converting the cart token with token: %s.", token));
            throw new PaymentValidationException(PAYMENT_PROCESS_SEVERE_ERROR);
        }
        return cartTokenData;
    }

    /**
     * Retrieve the session cart
     *
     * @return the cart
     */
    protected CartModel getSessionCart() {
        return cartService.hasSessionCart() ? cartService.getSessionCart() : null;
    }

    /**
     * Determine if the session cart belongs to the current user for checkout.
     *
     * @return boolean
     */
    protected boolean checkIfCurrentUserIsTheCartUser() {
        CartModel cartModel = getSessionCart();
        CustomerModel currentUser = getCurrentUserForCheckout();
        return cartModel != null && currentUser != null && currentUser.equals(cartModel.getUser());
    }

    /**
     * Retrieve the current session {@link CustomerModel}.
     *
     * @return the user
     */
    protected CustomerModel getCurrentUserForCheckout() {
        return checkoutCustomerStrategy.getCurrentUserForCheckout();
    }
}