package com.vanmarcke.saferpay.services.impl;

import com.vanmarcke.saferpay.api.clients.PaymentClient;
import com.vanmarcke.saferpay.api.data.*;
import com.vanmarcke.saferpay.model.SaferpayPaymentInfoModel;
import com.vanmarcke.saferpay.services.VMKPaymentService;
import com.vanmarcke.saferpay.strategies.PaymentTransactionStrategy;
import de.hybris.platform.commerceservices.strategies.GenerateMerchantTransactionCodeStrategy;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.internal.service.AbstractBusinessService;
import org.springframework.beans.factory.annotation.Required;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkState;
import static com.vanmarcke.core.util.PaymentTransactionUtils.getCurrentPaymentTransaction;
import static com.vanmarcke.saferpay.api.utils.ResponseUtils.checkIfSuccess;
import static com.vanmarcke.saferpay.constants.VanmarckesaferpayConstants.BIG_DECIMAL_HUNDRED;
import static org.apache.commons.lang.StringUtils.isNotEmpty;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.apache.commons.lang3.StringUtils.trimToNull;

/**
 * Default implementation of {@link VMKPaymentService}
 */
public class VMKPaymentServiceImpl extends AbstractBusinessService implements VMKPaymentService {

    private static final String TRANSACTION_STATUS_CAPTURED = "CAPTURED";

    private transient GenerateMerchantTransactionCodeStrategy generateMerchantTransactionCodeStrategy;
    private transient PaymentClient paymentClient;
    private transient PaymentTransactionStrategy paymentTransactionStrategy;

    /**
     * {@inheritDoc}
     */
    @Override
    public String initializePayment(CartModel cart) {
        checkArgument(cart != null, "Parameter cart cannot be null");

        checkState(cart.getPaymentInfo() instanceof SaferpayPaymentInfoModel, "cart.paymentInfo should be of type SaferpayPaymentInfo");

        PaymentPageInitializeResponse result = paymentClient.initialize(cart);

        if (checkIfSuccess(result)) {
            // create transaction
            String merchantCode = generateMerchantTransactionCodeStrategy.generateCode(cart);
            PaymentTransactionModel paymentTransaction = paymentTransactionStrategy.savePaymentTransaction(merchantCode, result.getToken());
            // update transaction
            paymentTransaction.setOrder(cart);
            paymentTransaction.setInfo(cart.getPaymentInfo());
            // save all
            getModelService().saveAll(cart, paymentTransaction);
            // return redirect URL
            return result.getRedirectUrl();
        }
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean validatePayment(AbstractOrderModel abstractOrder) {
        if (abstractOrder == null) {
            return true;
        }

        checkState(abstractOrder.getPaymentInfo() instanceof SaferpayPaymentInfoModel, "cart.paymentInfo should be of type SaferpayPaymentInfo");

        PaymentTransactionModel paymentTransactionModel = getCurrentPaymentTransaction(abstractOrder.getPaymentTransactions());
        checkState(paymentTransactionModel != null, "Cannot Assert without Initialization");

        if (isNotBlank(paymentTransactionModel.getRequestToken())) {
            PaymentPageAssertResponse result = paymentClient.validate(abstractOrder); //assert
            if (checkIfSuccess(result) && checkAuthenticationAndLiability(result)) {
                // create transaction entry
                Transaction transaction = result.getTransaction(); // Transaction container is mandatory
                Amount amount = transaction.getAmount(); // Amount container is mandatory
                paymentTransactionStrategy.savePaymentTransactionEntry(paymentTransactionModel,
                        getTransactionId(transaction),
                        transaction.getStatus(),
                        transaction.getDate(),
                        calculateAmount(amount.getValue()),
                        amount.getCurrencyCode());
                // update payment info
                SaferpayPaymentInfoModel saferPayPaymentInfoModel = (SaferpayPaymentInfoModel) abstractOrder.getPaymentInfo();
                populatePaymentInfo(result.getPaymentMeans(), saferPayPaymentInfoModel);
                getModelService().save(saferPayPaymentInfoModel);
                // return validation result
                return true;
            }
        }
        return false;
    }

    /**
     * Get the transaction ID form the given transaction
     *
     * @param transaction the given transaction
     * @return the transaction ID
     */
    protected String getTransactionId(Transaction transaction) {
        return TRANSACTION_STATUS_CAPTURED.equals(transaction.getStatus()) && isNotEmpty(transaction.getCaptureId()) ? transaction.getCaptureId() : transaction.getId();
    }

    /**
     * Calculate the amount based on the amount value as String
     *
     * @param amount the amount in text
     * @return the calculated amount
     */
    protected BigDecimal calculateAmount(String amount) {
        return new BigDecimal(amount).divide(BIG_DECIMAL_HUNDRED, 2, RoundingMode.HALF_UP);
    }

    /**
     * Populates the payment info with the payment means
     *
     * @param source the {@link PaymentMeans} as source
     * @param target the {@link SaferpayPaymentInfoModel} as target to populate
     */
    protected void populatePaymentInfo(PaymentMeans source, SaferpayPaymentInfoModel target) {
        target.setNumber(source.getDisplayText());
        target.setType(trimToNull(source.getBrand().getPaymentMethod())); // Brand container is mandatory
        Card card = source.getCard();
        if (card != null) {
            target.setCcOwner(trimToNull(card.getHolderName()));
            target.setValidToMonth(card.getExpMonth() != null ? card.getExpMonth().toString() : null);
            target.setValidToYear(card.getExpYear() != null ? card.getExpYear().toString() : null);
        }
    }

    /**
     * Check to see if the request has been authenticated and if there is a liability shift
     *
     * @param response the response with information
     * @return the decision if the response is authenticated and if there is a liability shift
     */
    private boolean checkAuthenticationAndLiability(PaymentPageAssertResponse response) {
        Liability liability = response.getLiability();
        if (liability != null && liability.getThreeDs() != null) {
            ThreeDs threeDs = liability.getThreeDs();
            return threeDs.getAuthenticated() && threeDs.getLiabilityShift();
        }
        return Boolean.FALSE;
    }

    @Required
    public void setGenerateMerchantTransactionCodeStrategy(GenerateMerchantTransactionCodeStrategy generateMerchantTransactionCodeStrategy) {
        this.generateMerchantTransactionCodeStrategy = generateMerchantTransactionCodeStrategy;
    }

    @Required
    public void setPaymentClient(PaymentClient paymentClient) {
        this.paymentClient = paymentClient;
    }

    @Required
    public void setPaymentTransactionStrategy(PaymentTransactionStrategy paymentTransactionStrategy) {
        this.paymentTransactionStrategy = paymentTransactionStrategy;
    }
}