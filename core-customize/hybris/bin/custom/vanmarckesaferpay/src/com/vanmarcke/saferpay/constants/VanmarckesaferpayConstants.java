package com.vanmarcke.saferpay.constants;

import java.math.BigDecimal;

/**
 * Global class for all Vanmarckesaferpay constants. You can add global constants for your extension into this class.
 */
public final class VanmarckesaferpayConstants extends GeneratedVanmarckesaferpayConstants {
    public static final String EXTENSIONNAME = "vanmarckesaferpay";

    private VanmarckesaferpayConstants() {
        //empty to avoid instantiating this constant class
    }

    // implement here constants used by this extension
    public static final BigDecimal BIG_DECIMAL_HUNDRED = BigDecimal.valueOf(100);

    public static final String PAYMENT_PROVIDER = "Saferpay";

    public static final String TRANSACTION_STATUS_CAPTURED = "CAPTURED";
    public static final String TRANSACTION_STATUS_PENDING = "PENDING";
}