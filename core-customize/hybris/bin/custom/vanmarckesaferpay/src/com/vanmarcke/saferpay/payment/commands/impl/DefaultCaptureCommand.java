package com.vanmarcke.saferpay.payment.commands.impl;

import com.vanmarcke.saferpay.api.clients.PaymentClient;
import com.vanmarcke.saferpay.api.data.TransactionCaptureResponse;
import com.vanmarcke.saferpay.daos.PaymentTransactionDao;
import de.hybris.platform.payment.AdapterException;
import de.hybris.platform.payment.commands.CaptureCommand;
import de.hybris.platform.payment.commands.request.CaptureRequest;
import de.hybris.platform.payment.commands.result.CaptureResult;
import de.hybris.platform.payment.model.PaymentTransactionModel;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkState;
import static com.vanmarcke.saferpay.api.utils.ResponseUtils.checkIfSuccess;
import static com.vanmarcke.saferpay.constants.VanmarckesaferpayConstants.TRANSACTION_STATUS_CAPTURED;
import static de.hybris.platform.payment.dto.TransactionStatus.ACCEPTED;
import static de.hybris.platform.payment.dto.TransactionStatus.REVIEW;
import static de.hybris.platform.payment.dto.TransactionStatusDetails.REVIEW_NEEDED;
import static de.hybris.platform.payment.dto.TransactionStatusDetails.SUCCESFULL;
import static java.lang.String.format;
import static org.apache.commons.lang3.ArrayUtils.getLength;
import static org.apache.commons.lang3.ArrayUtils.subarray;
import static org.apache.commons.lang3.StringUtils.joinWith;
import static org.apache.commons.lang3.StringUtils.split;

/**
 * Issues a Capture request
 */
public class DefaultCaptureCommand implements CaptureCommand {

    private final PaymentTransactionDao paymentTransactionDao;
    private final PaymentClient paymentClient;

    /**
     * Creates a new instance of {@link DefaultCaptureCommand}
     *
     * @param paymentTransactionDao the paymentTransactionDao
     * @param paymentClient         the paymentClient
     */
    public DefaultCaptureCommand(final PaymentTransactionDao paymentTransactionDao, final PaymentClient paymentClient) {
        this.paymentTransactionDao = paymentTransactionDao;
        this.paymentClient = paymentClient;
    }

    @Override
    public CaptureResult perform(final CaptureRequest request) {
        checkArgument(request != null, "Parameter request cannot be null");

        final PaymentTransactionModel paymentTransactionModel;
        try {
            final String paymentTransactionCode = calculatePaymentTransactionCode(request);
            paymentTransactionModel = paymentTransactionDao.findByCode(paymentTransactionCode);
        } catch (Exception e) {
            throw new AdapterException(format("Capture failed : %s", e.getMessage()), e);
        }

        final TransactionCaptureResponse response = paymentClient.capture(paymentTransactionModel.getOrder());
        if (!checkIfSuccess(response)) {
            throw new AdapterException(format("Capture failed : %s", response.getErrorName()));
        }

        final CaptureResult result = new CaptureResult();
        result.setCurrency(request.getCurrency());
        result.setTotalAmount(request.getTotalAmount());
        result.setRequestTime(response.getDate());
        if (TRANSACTION_STATUS_CAPTURED.equals(response.getStatus())) {
            // CAPTURED
            result.setTransactionStatus(ACCEPTED);
            result.setTransactionStatusDetails(SUCCESFULL);
            result.setRequestId(response.getCaptureId());
        } else {
            // CAPTURE PENDING
            result.setTransactionStatus(REVIEW);
            result.setTransactionStatusDetails(REVIEW_NEEDED);
            result.setRequestId(request.getRequestId());
        }
        return result;
    }

    protected String calculatePaymentTransactionCode(final CaptureRequest request) {
        final String[] result = split(request.getMerchantTransactionCode(), "-");

        checkState(getLength(result) > 2, "Invalid merchant transaction code '%s'", request.getMerchantTransactionCode());
        return joinWith("-", subarray(result, 0, result.length - 2));
    }
}