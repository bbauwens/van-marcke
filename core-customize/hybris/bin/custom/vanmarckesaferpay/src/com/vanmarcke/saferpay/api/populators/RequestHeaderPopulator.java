package com.vanmarcke.saferpay.api.populators;

import com.vanmarcke.saferpay.api.data.RequestHeader;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;
import de.hybris.platform.store.BaseStoreModel;

import static java.lang.String.format;
import static org.apache.commons.lang3.math.NumberUtils.INTEGER_ZERO;

public class RequestHeaderPopulator<SOURCE extends AbstractOrderModel> implements Populator<SOURCE, RequestHeader> {

    private static final String REQUEST_ID_FORMAT = "%s-%s";

    private final KeyGenerator requestIdGenerator;

    public RequestHeaderPopulator(final KeyGenerator requestIdGenerator) {
        this.requestIdGenerator = requestIdGenerator;
    }

    @Override
    public void populate(final SOURCE source, final RequestHeader target) {
        final BaseStoreModel store = source.getStore();

        target.setSpecVersion(store.getSaferpaySpecVersion());
        target.setCustomerId(store.getSaferpayCustomerId());
        target.setRequestId(format(REQUEST_ID_FORMAT, source.getCode(), requestIdGenerator.generate()));
        target.setRetryIndicator(INTEGER_ZERO);
    }
}