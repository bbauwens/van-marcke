package com.vanmarcke.saferpay.services.impl;

import com.vanmarcke.core.order.VMKOrderDao;
import com.vanmarcke.facades.order.VMKCheckoutFacade;
import com.vanmarcke.saferpay.exceptions.PaymentInvalidException;
import com.vanmarcke.saferpay.exceptions.PaymentValidationException;
import com.vanmarcke.saferpay.exceptions.PlaceOrderException;
import com.vanmarcke.saferpay.exceptions.SaferpayException;
import com.vanmarcke.saferpay.facades.data.CartTokenData;
import com.vanmarcke.saferpay.services.VMKPaymentOrderPollingService;
import com.vanmarcke.saferpay.services.VMKPaymentOrderService;
import com.vanmarcke.saferpay.services.VMKPaymentService;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;
import de.hybris.platform.servicelayer.session.SessionService;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

import static de.hybris.platform.core.enums.OrderStatus.ORDER_CREATION_IN_PROGRESS;

/**
 * Provides methods for handling orders within the payment provider context.
 *
 * @author Tom van den Berg
 * @since 05-02-2021
 */
public class VMKPaymentOrderServiceImpl implements VMKPaymentOrderService {

    private static final Logger LOG = Logger.getLogger(VMKPaymentOrderServiceImpl.class);
    private static final String PAYMENT_PROCESS_SEVERE_ERROR = "checkout.severe.error.process.payment";

    private final CommerceCartService commerceCartService;
    private final SessionService sessionService;
    private final VMKPaymentService paymentPageService;
    private final VMKPaymentOrderPollingService pollingService;
    private final VMKCheckoutFacade checkoutFacade;
    private final Converter<OrderModel, OrderData> orderConverter;
    private final VMKOrderDao orderDao;

    /**
     * Provides an instance of the VMKPaymentOrderServiceImpl.
     *
     * @param checkoutFacade      the checkout facade
     * @param orderDao            the order DAO
     * @param commerceCartService the commerce cart service
     * @param sessionService      the session service
     * @param paymentPageService  the payment page service
     * @param pollingService      the order polling service
     * @param orderConverter      the order converter
     */
    public VMKPaymentOrderServiceImpl(VMKCheckoutFacade checkoutFacade,
                                      VMKOrderDao orderDao,
                                      CommerceCartService commerceCartService,
                                      SessionService sessionService,
                                      VMKPaymentService paymentPageService,
                                      VMKPaymentOrderPollingService pollingService,
                                      Converter<OrderModel, OrderData> orderConverter) {
        this.checkoutFacade = checkoutFacade;
        this.orderDao = orderDao;
        this.commerceCartService = commerceCartService;
        this.sessionService = sessionService;
        this.paymentPageService = paymentPageService;
        this.pollingService = pollingService;
        this.orderConverter = orderConverter;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OrderData checkForExistingOrder(CartTokenData cartToken) throws SaferpayException {
        OrderData orderData = null;
        CartModel cart = commerceCartService.getCartForGuidAndSite(cartToken.getCartUid(), cartToken.getBaseSite());
        if (cart == null) { // If the cart doesn't exist, it is likely already placed as an order.
            orderData = getOrderDetailsForCartToken(cartToken);
        } else if (ORDER_CREATION_IN_PROGRESS.equals(cart.getStatus())) { // To prevent race conditions
            orderData = pollOrderCreationProcess(cartToken);
        }
        return orderData;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OrderData placeOrderForCartToken(CartTokenData cartToken) throws SaferpayException {
        CartModel cart = commerceCartService.getCartForGuidAndSite(cartToken.getCartUid(), cartToken.getBaseSite());

        if (cart == null) {
            LOG.error("Could not find cart while placing order for cart token.");
            throw new PlaceOrderException(PAYMENT_PROCESS_SEVERE_ERROR);
        }

        if (isInvalid(cart)) {
            LOG.debug(String.format("Payment for order with UID %s is not valid.", cartToken.getCartUid()));
            throw new PaymentInvalidException(PAYMENT_PROCESS_SEVERE_ERROR);
        }

        //todo: opkuisen
        try {
            Map<String, Object> sessionParameters = new HashMap<>();
            sessionParameters.put("user", cart.getUser());
            sessionParameters.put("cart", cart);
            sessionParameters.put("currentSite", cartToken.getBaseSite());
            return sessionService.executeInLocalViewWithParams(sessionParameters,
                    new SessionExecutionBody() {
                        OrderData orderData = null;

                        // TODO: UNIT TEST
                        @Override
                        public Object execute() {
                            try {
                                orderData = checkoutFacade.placeOrder();
                            } catch (InvalidCartException e) {
                                e.printStackTrace();
                                throw new RuntimeException(PAYMENT_PROCESS_SEVERE_ERROR, e);
                            }
                            return orderData;
                        }
                    });


        } catch (Exception e) {
            LOG.error(String.format("Failed to process order with GUID: %s.", cartToken.getCartUid()), e.getCause());
            throw new PlaceOrderException(PAYMENT_PROCESS_SEVERE_ERROR, e.getCause());
        }
    }

    /**
     * Determine whether the given cart is valid.
     *
     * @param cart the cart
     * @return true if cart is invalid
     */
    protected boolean isInvalid(CartModel cart) {
        boolean isValid = false;
        try {
            isValid = paymentPageService.validatePayment(cart);
        } catch (Exception e) {
            LOG.error(String.format("Failed to validate the payment for order with code: %s", cart.getCode()), e);
        }
        return !isValid;
    }

    /**
     * Retrieve the order details for the given cart GUID and base site.
     *
     * @param cartToken the cart token
     * @return the order details for GUID
     */
    protected OrderData getOrderDetailsForCartToken(CartTokenData cartToken) {
        OrderModel order = orderDao.findOrderByGuidAndSite(cartToken.getCartUid(), cartToken.getBaseSite());
        return order != null ? orderConverter.convert(order) : null;
    }

    /**
     * Poll the order creation process for the given cart token.
     *
     * @param cartToken the token
     * @return the order data
     */
    protected OrderData pollOrderCreationProcess(CartTokenData cartToken) throws PaymentValidationException {
        OrderModel orderModel = pollingService.pollOrderCreationProcess(cartToken);
        return orderModel != null ? orderConverter.convert(orderModel) : null;
    }
}
