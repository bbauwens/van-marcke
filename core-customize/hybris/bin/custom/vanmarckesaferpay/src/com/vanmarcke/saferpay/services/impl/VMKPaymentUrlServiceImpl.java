package com.vanmarcke.saferpay.services.impl;

import com.vanmarcke.saferpay.services.VMKPaymentUrlService;
import com.vanmarcke.saferpay.utils.URLBuilder;
import de.hybris.platform.acceleratorservices.urlresolver.SiteBaseUrlResolutionService;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import org.springframework.util.Assert;

import static com.google.common.base.Preconditions.checkState;
import static java.lang.String.format;

/**
 * Provides methods to create return and notification urls when interacting with saferpay.
 *
 * @author Tom van den Berg
 * @since 03-02-2021
 */
public class VMKPaymentUrlServiceImpl implements VMKPaymentUrlService {

    private final ConfigurationService configurationService;
    private final SiteBaseUrlResolutionService siteBaseUrlResolutionService;

    /**
     * Provides an instance of the VMKPaymentUrlServiceImpl.
     *
     * @param configurationService         the configuration service
     * @param siteBaseUrlResolutionService the site base url resolution service
     */
    public VMKPaymentUrlServiceImpl(ConfigurationService configurationService,
                                    SiteBaseUrlResolutionService siteBaseUrlResolutionService) {
        this.configurationService = configurationService;
        this.siteBaseUrlResolutionService = siteBaseUrlResolutionService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getUrlForCart(AbstractOrderModel cart, String urlPathKey) {
        BaseSiteModel site = cart.getSite();
        String baseUrl = siteBaseUrlResolutionService.getWebsiteUrlForSite(site, true, null);
        String path = configurationService.getConfiguration().getString(urlPathKey);
        String token = createTokenForCart(cart);

        Assert.hasLength(baseUrl, "The base url cannot be empty");
        Assert.hasLength(path, "The path cannot be empty");

        return URLBuilder.aSaferPayUrl()
                .withBaseUrl(baseUrl)
                .withPath(path)
                .withToken(token)
                .build();
    }

    /**
     * Creates a token for the given cart.
     *
     * @param cart the cart
     * @return the token
     */
    private String createTokenForCart(AbstractOrderModel cart) {
        BaseSiteModel site = cart.getSite();
        String cartGuid = cart.getGuid();
        checkState(cartGuid != null, "Cart ID cannot be null.");
        checkState(site != null, "The site cannot be null.");

        return format("%s_%s", cartGuid, site.getUid());
    }
}
