package com.vanmarcke.saferpay.daos;

import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.internal.dao.GenericDao;

/**
 * Dao which provides functionality around payment transactions
 */
public interface PaymentTransactionDao extends GenericDao<PaymentTransactionModel> {

    /**
     * Returns for the given payment transaction <code>code</code> the {@link PaymentTransactionModel}.
     *
     * @param code the code
     * @return a {@link PaymentTransactionModel}
     * @throws IllegalArgumentException     if the given <code>code</code> is <code>null</code>
     * @throws AmbiguousIdentifierException if more than one payment transaction was found
     * @throws UnknownIdentifierException   if more no payment transaction was found
     */
    PaymentTransactionModel findByCode(String code);
}