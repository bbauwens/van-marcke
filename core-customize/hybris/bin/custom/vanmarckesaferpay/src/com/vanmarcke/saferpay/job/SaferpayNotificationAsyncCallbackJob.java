package com.vanmarcke.saferpay.job;

import com.vanmarcke.saferpay.daos.PaymentNotificationDao;
import com.vanmarcke.saferpay.facades.VMKPaymentFacade;
import com.vanmarcke.saferpay.model.SaferpayNotificationAsyncCallbackCronJobModel;
import com.vanmarcke.saferpay.model.SaferpayNotificationModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import org.apache.log4j.Logger;
import org.joda.time.LocalTime;

import java.util.List;

/**
 * Job that performs the Async Callback for saferpay
 */
public class SaferpayNotificationAsyncCallbackJob extends AbstractJobPerformable<SaferpayNotificationAsyncCallbackCronJobModel> {

    private static final Logger LOG = Logger.getLogger(SaferpayNotificationAsyncCallbackJob.class);

    private final PaymentNotificationDao paymentNotificationDao;
    private final VMKPaymentFacade paymentFacade;

    /**
     * Constructor for {@link SaferpayNotificationAsyncCallbackJob}
     *
     * @param paymentNotificationDao the paymentNotificationDao
     * @param paymentFacade          the payment facade
     */
    public SaferpayNotificationAsyncCallbackJob(PaymentNotificationDao paymentNotificationDao,
                                                VMKPaymentFacade paymentFacade) {
        this.paymentNotificationDao = paymentNotificationDao;
        this.paymentFacade = paymentFacade;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PerformResult perform(SaferpayNotificationAsyncCallbackCronJobModel cronJob) {
        PerformResult result = new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
//        TODO: PAS FILTERING AAN ZODAT ENKEL OUDER DAN 15 MINS WORDT TERUGGEGEVEN
        List<SaferpayNotificationModel> allOpenNotifications = paymentNotificationDao.findAllPaymentNotifications();
        allOpenNotifications.stream()
                .filter(this::isOlderThan15Minutes)
                .forEach(this::processPaymentEvent);
        return result;
    }

    /**
     * Determine whether the saferpay event was created at least 15 minutes ago.
     * This to give priority to saferpay 'Success', before 'Notify'.
     *
     * @param notification the saferpay event
     * @return true if event is older than 15 minutes before now.
     */
    private boolean isOlderThan15Minutes(SaferpayNotificationModel notification) {
        LocalTime time = LocalTime.fromDateFields(notification.getCreationtime());
        return time.isBefore(LocalTime.now().minusMinutes(15));
    }

    /**
     * Processes the given notification.
     *
     * @param notification the notification
     */
    private void processPaymentEvent(SaferpayNotificationModel notification) {
        try {
            paymentFacade.processPaymentConfirmation(notification.getCartToken());
            modelService.remove(notification);
        } catch (Exception e) {
            //todo: inform order manager
            // TODO: we should clean up the notication too.
            LOG.error("Error while processing payment from saferpay event", e);
        }
    }
}
