package com.vanmarcke.saferpay.api.clients;

import com.vanmarcke.saferpay.api.data.PaymentPageAssertResponse;
import com.vanmarcke.saferpay.api.data.PaymentPageInitializeResponse;
import com.vanmarcke.saferpay.api.data.TransactionCaptureResponse;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;

/**
 * Client interface for the Payment Page Integration
 */
public interface PaymentClient {

    /**
     * Payment Page Initialize
     *
     * @param cart the cart
     * @return the Payment Page Initialize Response
     */
    PaymentPageInitializeResponse initialize(CartModel cart);

    /**
     * Payment Page Assert
     *
     * @param order the order
     * @return the Payment Page Assert Response
     */
    PaymentPageAssertResponse validate(AbstractOrderModel order);

    /**
     * Transaction Capture
     *
     * @param order the order
     * @return the Transaction Capture Response
     */
    TransactionCaptureResponse capture(AbstractOrderModel order);
}