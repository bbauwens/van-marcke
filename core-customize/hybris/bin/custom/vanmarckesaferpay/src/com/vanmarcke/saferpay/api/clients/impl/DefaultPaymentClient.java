package com.vanmarcke.saferpay.api.clients.impl;

import com.vanmarcke.saferpay.api.clients.PaymentClient;
import com.vanmarcke.saferpay.api.commands.PaymentPageAssertCommand;
import com.vanmarcke.saferpay.api.commands.PaymentPageInitializeCommand;
import com.vanmarcke.saferpay.api.commands.TransactionCaptureCommand;
import com.vanmarcke.saferpay.api.data.*;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.store.BaseStoreModel;
import eu.elision.integration.command.Command;
import eu.elision.integration.command.Response;
import eu.elision.integration.command.executer.CommandExecutor;

public class DefaultPaymentClient implements PaymentClient {

    private final CommandExecutor commandExecutor;
    private final Converter<AbstractOrderModel, PaymentPageInitializeRequest> initializeConverter;
    private final Converter<AbstractOrderModel, PaymentPageAssertRequest> assertConverter;
    private final Converter<AbstractOrderModel, TransactionCaptureRequest> captureConverter;

    /**
     * Creates a new instance of {@link DefaultPaymentClient}
     *
     * @param commandExecutor     the commandExecutor
     * @param initializeConverter the initializeConverter
     * @param assertConverter     the assertConverter
     * @param captureConverter    the captureConverter
     */
    public DefaultPaymentClient(final CommandExecutor commandExecutor,
                                final Converter<AbstractOrderModel, PaymentPageInitializeRequest> initializeConverter,
                                final Converter<AbstractOrderModel, PaymentPageAssertRequest> assertConverter,
                                final Converter<AbstractOrderModel, TransactionCaptureRequest> captureConverter) {
        this.commandExecutor = commandExecutor;
        this.initializeConverter = initializeConverter;
        this.assertConverter = assertConverter;
        this.captureConverter = captureConverter;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PaymentPageInitializeResponse initialize(final CartModel cart) {
        final BaseStoreModel store = cart.getStore();
        final PaymentPageInitializeCommand cmd = new PaymentPageInitializeCommand(store.getSaferpayApiURL(), store.getSaferpayApiUsername(), store.getSaferpayApiPassword(), initializeConverter.convert(cart));
        return perform(cmd);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PaymentPageAssertResponse validate(final AbstractOrderModel order) {
        final BaseStoreModel store = order.getStore();
        final PaymentPageAssertCommand cmd = new PaymentPageAssertCommand(store.getSaferpayApiURL(), store.getSaferpayApiUsername(), store.getSaferpayApiPassword(), assertConverter.convert(order));
        return perform(cmd);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TransactionCaptureResponse capture(final AbstractOrderModel order) {
        final BaseStoreModel store = order.getStore();
        final TransactionCaptureCommand cmd = new TransactionCaptureCommand(store.getSaferpayApiURL(), store.getSaferpayApiUsername(), store.getSaferpayApiPassword(), captureConverter.convert(order));
        return perform(cmd);
    }

    protected <T> T perform(final Command<T> cmd) {
        final Response<T> response = commandExecutor.executeCommand(cmd, null);
        return response.getPayLoad();
    }
}