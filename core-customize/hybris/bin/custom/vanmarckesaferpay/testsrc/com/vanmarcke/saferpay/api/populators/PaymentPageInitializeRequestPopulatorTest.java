package com.vanmarcke.saferpay.api.populators;

import com.vanmarcke.saferpay.api.data.*;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.store.BaseStoreModel;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class PaymentPageInitializeRequestPopulatorTest {

    @Mock
    private Converter<AbstractOrderModel, RequestHeader> requestHeaderConverter;
    @Mock
    private Converter<AbstractOrderModel, Payment> paymentConverter;
    @Mock
    private Converter<AbstractOrderModel, ReturnUrls> returnUrlsConverter;
    @Mock
    private Converter<AbstractOrderModel, PendingNotification> notificationConverter;

    private PaymentPageInitializeRequestPopulator paymentPageInitializeRequestPopulator;

    @Before
    public void setup() {
        paymentPageInitializeRequestPopulator = new PaymentPageInitializeRequestPopulator(requestHeaderConverter, paymentConverter, returnUrlsConverter, notificationConverter);
    }

    @Test
    public void testPopulate() {
        AbstractOrderModel abstractOrderModel = mock(AbstractOrderModel.class);
        BaseStoreModel baseStoreModel = mock(BaseStoreModel.class);
        RequestHeader requestHeader = mock(RequestHeader.class);
        Payment payment = mock(Payment.class);
        ReturnUrls returnUrls = mock(ReturnUrls.class);
        PendingNotification notification = mock(PendingNotification.class);

        when(baseStoreModel.getSaferpayTerminalId()).thenReturn("saferpay-terminal-id");

        when(abstractOrderModel.getStore()).thenReturn(baseStoreModel);

        when(requestHeaderConverter.convert(abstractOrderModel)).thenReturn(requestHeader);

        when(paymentConverter.convert(abstractOrderModel)).thenReturn(payment);

        when(returnUrlsConverter.convert(abstractOrderModel)).thenReturn(returnUrls);

        when(notificationConverter.convert(abstractOrderModel)).thenReturn(notification);

        PaymentPageInitializeRequest result = new PaymentPageInitializeRequest();
        paymentPageInitializeRequestPopulator.populate(abstractOrderModel, result);

        assertThat(result.getTerminalId()).isEqualTo("saferpay-terminal-id");
        assertThat(result.getRequestHeader()).isEqualTo(requestHeader);
        assertThat(result.getPayment()).isEqualTo(payment);
        assertThat(result.getReturnUrls()).isEqualTo(returnUrls);
        assertThat(result.getNotification()).isEqualTo(notification);
    }

}