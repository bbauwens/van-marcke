package com.vanmarcke.saferpay.services.impl;

import com.vanmarcke.saferpay.api.clients.PaymentClient;
import com.vanmarcke.saferpay.api.data.*;
import com.vanmarcke.saferpay.model.SaferpayPaymentInfoModel;
import com.vanmarcke.saferpay.strategies.PaymentTransactionStrategy;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.commerceservices.strategies.GenerateMerchantTransactionCodeStrategy;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.payment.InvoicePaymentInfoModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.model.ModelService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.Date;

import static java.util.Collections.singletonList;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKPaymentServiceTest {

    @Mock
    private ModelService modelService;
    @Mock
    private GenerateMerchantTransactionCodeStrategy generateMerchantTransactionCodeStrategy;
    @Mock
    private PaymentClient paymentPageClient;
    @Mock
    private PaymentTransactionStrategy paymentTransactionStrategy;
    @InjectMocks
    private VMKPaymentServiceImpl defaultPaymentPageService;

    @Captor
    private ArgumentCaptor<CommerceCheckoutParameter> commerceCheckoutParameterArgumentCaptor;

    @Test
    public void testInitialize() {
        CartModel cartModel = mock(CartModel.class);
        SaferpayPaymentInfoModel paymentInfoModel = mock(SaferpayPaymentInfoModel.class);
        PaymentTransactionModel paymentTransactionModel = mock(PaymentTransactionModel.class);
        PaymentPageInitializeResponse response = mock(PaymentPageInitializeResponse.class);

        when(cartModel.getPaymentInfo()).thenReturn(paymentInfoModel);

        when(response.getToken()).thenReturn("token");
        when(response.getRedirectUrl()).thenReturn("/redirect-url");

        when(paymentPageClient.initialize(cartModel)).thenReturn(response);

        when(generateMerchantTransactionCodeStrategy.generateCode(cartModel)).thenReturn("merchant-code");

        when(paymentTransactionStrategy.savePaymentTransaction("merchant-code", "token")).thenReturn(paymentTransactionModel);

        String result = defaultPaymentPageService.initializePayment(cartModel);

        assertThat(result).isEqualTo("/redirect-url");

        verify(paymentTransactionModel).setOrder(cartModel);
        verify(paymentTransactionModel).setInfo(paymentInfoModel);

        verify(modelService).saveAll(cartModel, paymentTransactionModel);
    }

    @Test
    public void testInitialize_withError() {
        CartModel cartModel = mock(CartModel.class);
        SaferpayPaymentInfoModel paymentInfoModel = mock(SaferpayPaymentInfoModel.class);
        PaymentTransactionModel paymentTransactionModel = mock(PaymentTransactionModel.class);
        PaymentPageInitializeResponse response = mock(PaymentPageInitializeResponse.class);

        when(cartModel.getPaymentInfo()).thenReturn(paymentInfoModel);

        when(response.getErrorName()).thenReturn("ERROR");

        when(paymentPageClient.initialize(cartModel)).thenReturn(response);

        String result = defaultPaymentPageService.initializePayment(cartModel);

        assertThat(result).isNull();

        verifyZeroInteractions(generateMerchantTransactionCodeStrategy);
        verifyZeroInteractions(paymentTransactionStrategy);
        verifyZeroInteractions(modelService);
    }

    @Test(expected = IllegalStateException.class)
    public void testInitialize_withInvoicePaymentInfo() {
        CartModel cartModel = mock(CartModel.class);
        InvoicePaymentInfoModel invoicePaymentInfoModel = mock(InvoicePaymentInfoModel.class);

        when(cartModel.getPaymentInfo()).thenReturn(invoicePaymentInfoModel);

        defaultPaymentPageService.initializePayment(cartModel);
    }

    @Test
    public void testValidate() {
        CartModel cartModel = mock(CartModel.class);
        SaferpayPaymentInfoModel saferpayPaymentInfoModel = mock(SaferpayPaymentInfoModel.class);
        PaymentTransactionModel paymentTransactionModel = mock(PaymentTransactionModel.class);
        PaymentPageAssertResponse response = mock(PaymentPageAssertResponse.class);
        Transaction transaction = mock(Transaction.class);
        Amount amount = mock(Amount.class);
        PaymentMeans paymentMeans = mock(PaymentMeans.class);
        Brand brand = mock(Brand.class);
        Card card = mock(Card.class);
        Date date = mock(Date.class);
        Liability liability = mock(Liability.class);
        ThreeDs threeDs = mock(ThreeDs.class);

        when(paymentTransactionModel.getRequestToken()).thenReturn("request-token");

        when(cartModel.getPaymentTransactions()).thenReturn(singletonList(paymentTransactionModel));
        when(cartModel.getPaymentInfo()).thenReturn(saferpayPaymentInfoModel);

        when(amount.getCurrencyCode()).thenReturn("EUR");
        when(amount.getValue()).thenReturn("12399");

        when(transaction.getId()).thenReturn("transaction-id");
        when(transaction.getStatus()).thenReturn("transaction-status");
        when(transaction.getDate()).thenReturn(date);
        when(transaction.getAmount()).thenReturn(amount);

        when(brand.getPaymentMethod()).thenReturn("brand-payment-method");

        when(card.getHolderName()).thenReturn("card-holder-name");
        when(card.getExpMonth()).thenReturn(1);
        when(card.getExpYear()).thenReturn(2020);

        when(paymentMeans.getBrand()).thenReturn(brand);
        when(paymentMeans.getCard()).thenReturn(card);
        when(paymentMeans.getDisplayText()).thenReturn("payment-means-display-text");

        when(response.getTransaction()).thenReturn(transaction);
        when(response.getPaymentMeans()).thenReturn(paymentMeans);
        when(response.getLiability()).thenReturn(liability);
        when(liability.getThreeDs()).thenReturn(threeDs);
        when(threeDs.getAuthenticated()).thenReturn(Boolean.TRUE);
        when(threeDs.getLiabilityShift()).thenReturn(Boolean.TRUE);

        when(paymentPageClient.validate(cartModel)).thenReturn(response);

        CommerceCheckoutParameter parameter = new CommerceCheckoutParameter();
        parameter.setCart(cartModel);
        boolean result = defaultPaymentPageService.validatePayment(cartModel);

        assertThat(result).isTrue();

        verify(paymentTransactionStrategy).savePaymentTransactionEntry(paymentTransactionModel, "transaction-id", "transaction-status", date, BigDecimal.valueOf(123.99), "EUR");

        verify(saferpayPaymentInfoModel).setNumber("payment-means-display-text");
        verify(saferpayPaymentInfoModel).setType("brand-payment-method");
        verify(saferpayPaymentInfoModel).setCcOwner("card-holder-name");
        verify(saferpayPaymentInfoModel).setValidToMonth("1");
        verify(saferpayPaymentInfoModel).setValidToYear("2020");

        verify(modelService).save(saferpayPaymentInfoModel);
    }

    @Test
    public void testValidate_NotAuthorized_LiabilityShift() {
        CartModel cartModel = mock(CartModel.class);
        SaferpayPaymentInfoModel saferpayPaymentInfoModel = mock(SaferpayPaymentInfoModel.class);
        PaymentTransactionModel paymentTransactionModel = mock(PaymentTransactionModel.class);
        PaymentPageAssertResponse response = mock(PaymentPageAssertResponse.class);
        Liability liability = mock(Liability.class);
        ThreeDs threeDs = mock(ThreeDs.class);

        when(paymentTransactionModel.getRequestToken()).thenReturn("request-token");

        when(cartModel.getPaymentTransactions()).thenReturn(singletonList(paymentTransactionModel));
        when(cartModel.getPaymentInfo()).thenReturn(saferpayPaymentInfoModel);


        when(response.getLiability()).thenReturn(liability);
        when(liability.getThreeDs()).thenReturn(threeDs);
        when(threeDs.getAuthenticated()).thenReturn(Boolean.FALSE);
        when(threeDs.getLiabilityShift()).thenReturn(Boolean.TRUE);

        when(paymentPageClient.validate(cartModel)).thenReturn(response);

        boolean result = defaultPaymentPageService.validatePayment(cartModel);

        assertThat(result).isFalse();

        verifyZeroInteractions(generateMerchantTransactionCodeStrategy);
        verifyZeroInteractions(paymentTransactionStrategy);
        verifyZeroInteractions(modelService);
    }

    @Test
    public void testValidate_missingLiabilityObject() {
        CartModel cartModel = mock(CartModel.class);
        SaferpayPaymentInfoModel saferpayPaymentInfoModel = mock(SaferpayPaymentInfoModel.class);
        PaymentTransactionModel paymentTransactionModel = mock(PaymentTransactionModel.class);
        PaymentPageAssertResponse response = mock(PaymentPageAssertResponse.class);

        when(paymentTransactionModel.getRequestToken()).thenReturn("request-token");

        when(cartModel.getPaymentTransactions()).thenReturn(singletonList(paymentTransactionModel));
        when(cartModel.getPaymentInfo()).thenReturn(saferpayPaymentInfoModel);

        when(response.getLiability()).thenReturn(null);

        when(paymentPageClient.validate(cartModel)).thenReturn(response);

        boolean result = defaultPaymentPageService.validatePayment(cartModel);

        assertThat(result).isFalse();

        verifyZeroInteractions(generateMerchantTransactionCodeStrategy);
        verifyZeroInteractions(paymentTransactionStrategy);
        verifyZeroInteractions(modelService);
    }

    @Test
    public void testValidate_Authorized_NoLiabilityShift() {
        CartModel cartModel = mock(CartModel.class);
        SaferpayPaymentInfoModel saferpayPaymentInfoModel = mock(SaferpayPaymentInfoModel.class);
        PaymentTransactionModel paymentTransactionModel = mock(PaymentTransactionModel.class);
        PaymentPageAssertResponse response = mock(PaymentPageAssertResponse.class);
        Liability liability = mock(Liability.class);
        ThreeDs threeDs = mock(ThreeDs.class);

        when(paymentTransactionModel.getRequestToken()).thenReturn("request-token");

        when(cartModel.getPaymentTransactions()).thenReturn(singletonList(paymentTransactionModel));
        when(cartModel.getPaymentInfo()).thenReturn(saferpayPaymentInfoModel);

        when(response.getLiability()).thenReturn(liability);
        when(liability.getThreeDs()).thenReturn(threeDs);
        when(threeDs.getAuthenticated()).thenReturn(Boolean.TRUE);
        when(threeDs.getLiabilityShift()).thenReturn(Boolean.FALSE);

        when(paymentPageClient.validate(cartModel)).thenReturn(response);

        boolean result = defaultPaymentPageService.validatePayment(cartModel);

        assertThat(result).isFalse();

        verifyZeroInteractions(generateMerchantTransactionCodeStrategy);
        verifyZeroInteractions(paymentTransactionStrategy);
        verifyZeroInteractions(modelService);
    }

    @Test
    public void testValidate_NotAuthorized_NoLiabilityShift() {
        CartModel cartModel = mock(CartModel.class);
        SaferpayPaymentInfoModel saferpayPaymentInfoModel = mock(SaferpayPaymentInfoModel.class);
        PaymentTransactionModel paymentTransactionModel = mock(PaymentTransactionModel.class);
        PaymentPageAssertResponse response = mock(PaymentPageAssertResponse.class);
        Liability liability = mock(Liability.class);
        ThreeDs threeDs = mock(ThreeDs.class);

        when(paymentTransactionModel.getRequestToken()).thenReturn("request-token");

        when(cartModel.getPaymentTransactions()).thenReturn(singletonList(paymentTransactionModel));
        when(cartModel.getPaymentInfo()).thenReturn(saferpayPaymentInfoModel);

        when(response.getLiability()).thenReturn(liability);
        when(liability.getThreeDs()).thenReturn(threeDs);
        when(threeDs.getAuthenticated()).thenReturn(Boolean.FALSE);
        when(threeDs.getLiabilityShift()).thenReturn(Boolean.FALSE);

        when(paymentPageClient.validate(cartModel)).thenReturn(response);

        boolean result = defaultPaymentPageService.validatePayment(cartModel);

        assertThat(result).isFalse();

        verifyZeroInteractions(generateMerchantTransactionCodeStrategy);
        verifyZeroInteractions(paymentTransactionStrategy);
        verifyZeroInteractions(modelService);
    }

    @Test
    public void testValidate_withCaptureStatus() {
        CartModel cartModel = mock(CartModel.class);
        SaferpayPaymentInfoModel saferpayPaymentInfoModel = mock(SaferpayPaymentInfoModel.class);
        PaymentTransactionModel paymentTransactionModel = mock(PaymentTransactionModel.class);
        PaymentPageAssertResponse response = mock(PaymentPageAssertResponse.class);
        Transaction transaction = mock(Transaction.class);
        Amount amount = mock(Amount.class);
        PaymentMeans paymentMeans = mock(PaymentMeans.class);
        Brand brand = mock(Brand.class);
        Card card = mock(Card.class);
        Date date = mock(Date.class);
        Liability liability = mock(Liability.class);
        ThreeDs threeDs = mock(ThreeDs.class);

        when(paymentTransactionModel.getRequestToken()).thenReturn("request-token");

        when(cartModel.getPaymentTransactions()).thenReturn(singletonList(paymentTransactionModel));
        when(cartModel.getPaymentInfo()).thenReturn(saferpayPaymentInfoModel);

        when(amount.getCurrencyCode()).thenReturn("EUR");
        when(amount.getValue()).thenReturn("12399");

        when(transaction.getId()).thenReturn("transaction-id");
        when(transaction.getStatus()).thenReturn("CAPTURED");
        when(transaction.getCaptureId()).thenReturn("transaction-capture-id");
        when(transaction.getDate()).thenReturn(date);
        when(transaction.getAmount()).thenReturn(amount);

        when(brand.getPaymentMethod()).thenReturn("brand-payment-method");

        when(card.getHolderName()).thenReturn("card-holder-name");
        when(card.getExpMonth()).thenReturn(1);
        when(card.getExpYear()).thenReturn(2020);

        when(paymentMeans.getBrand()).thenReturn(brand);
        when(paymentMeans.getCard()).thenReturn(card);
        when(paymentMeans.getDisplayText()).thenReturn("payment-means-display-text");

        when(response.getTransaction()).thenReturn(transaction);
        when(response.getPaymentMeans()).thenReturn(paymentMeans);
        when(response.getLiability()).thenReturn(liability);
        when(liability.getThreeDs()).thenReturn(threeDs);
        when(threeDs.getAuthenticated()).thenReturn(Boolean.TRUE);
        when(threeDs.getLiabilityShift()).thenReturn(Boolean.TRUE);

        when(paymentPageClient.validate(cartModel)).thenReturn(response);

        CommerceCheckoutParameter parameter = new CommerceCheckoutParameter();
        parameter.setCart(cartModel);
        boolean result = defaultPaymentPageService.validatePayment(cartModel);

        assertThat(result).isTrue();

        verify(paymentTransactionStrategy).savePaymentTransactionEntry(paymentTransactionModel, "transaction-capture-id", "CAPTURED", date, BigDecimal.valueOf(123.99), "EUR");

        verify(saferpayPaymentInfoModel).setNumber("payment-means-display-text");
        verify(saferpayPaymentInfoModel).setType("brand-payment-method");
        verify(saferpayPaymentInfoModel).setCcOwner("card-holder-name");
        verify(saferpayPaymentInfoModel).setValidToMonth("1");
        verify(saferpayPaymentInfoModel).setValidToYear("2020");

        verify(modelService).save(saferpayPaymentInfoModel);
    }

    @Test
    public void testValidate_withoutToken() {
        CartModel cartModel = mock(CartModel.class);
        SaferpayPaymentInfoModel paymentInfoModel = mock(SaferpayPaymentInfoModel.class);
        PaymentTransactionModel paymentTransactionModel = mock(PaymentTransactionModel.class);

        when(cartModel.getPaymentInfo()).thenReturn(paymentInfoModel);
        when(cartModel.getPaymentTransactions()).thenReturn(singletonList(paymentTransactionModel));

        boolean result = defaultPaymentPageService.validatePayment(cartModel);

        assertThat(result).isFalse();
    }

}