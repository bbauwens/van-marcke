package com.vanmarcke.saferpay.api.populators;

import com.vanmarcke.saferpay.api.data.ReturnUrls;
import com.vanmarcke.saferpay.services.VMKPaymentUrlService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import org.apache.commons.lang.RandomStringUtils;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ReturnUrlsPopulatorTest {

    private static final String FAIL = RandomStringUtils.randomAlphabetic(10);
    private static final String ABORT = RandomStringUtils.randomAlphabetic(10);
    private static final String SUCCESS = RandomStringUtils.randomAlphabetic(10);

    private static final String CONFIG_URL_PATH_SUCCESS = "saferpay.paymentpage.url.path.success";
    private static final String CONFIG_URL_PATH_FAIL = "saferpay.paymentpage.url.path.fail";
    private static final String CONFIG_URL_PATH_ABORT = "saferpay.paymentpage.url.path.abort";

    @Mock
    private VMKPaymentUrlService paymentUrlService;

    @InjectMocks
    private ReturnUrlsPopulator<AbstractOrderModel> returnUrlsPopulator;

    @Rule
    public final ExpectedException thrown = ExpectedException.none();

    @Test
    public void testPopulate() {
        AbstractOrderModel cart = mock(AbstractOrderModel.class);

        when(paymentUrlService.getUrlForCart(cart, CONFIG_URL_PATH_ABORT))
                .thenReturn(ABORT);
        when(paymentUrlService.getUrlForCart(cart, CONFIG_URL_PATH_FAIL))
                .thenReturn(FAIL);
        when(paymentUrlService.getUrlForCart(cart, CONFIG_URL_PATH_SUCCESS))
                .thenReturn(SUCCESS);

        ReturnUrls result = new ReturnUrls();
        returnUrlsPopulator.populate(cart, result);

        assertThat(result.getAbort()).isEqualTo(ABORT);
        assertThat(result.getFail()).isEqualTo(FAIL);
        assertThat(result.getSuccess()).isEqualTo(SUCCESS);

        verify(paymentUrlService).getUrlForCart(cart, CONFIG_URL_PATH_ABORT);
        verify(paymentUrlService).getUrlForCart(cart, CONFIG_URL_PATH_FAIL);
        verify(paymentUrlService).getUrlForCart(cart, CONFIG_URL_PATH_SUCCESS);
    }
}