package com.vanmarcke.saferpay.daos.impl;

import com.vanmarcke.saferpay.model.SaferpayNotificationModel;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import static java.util.Collections.singletonList;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultPaymentNotificationDaoTest {

    @Mock
    private FlexibleSearchService flexibleSearchService;
    @InjectMocks
    private DefaultPaymentNotificationDao defaultPaymentNotificationDao;

    @Captor
    private ArgumentCaptor<FlexibleSearchQuery> flexibleSearchQueryCaptor;

    @Test
    public void testFindAllPaymentNotifications() {
        SaferpayNotificationModel saferpayNotification = mock(SaferpayNotificationModel.class);
        SearchResult searchResult = mock(SearchResult.class);

        when(searchResult.getResult()).thenReturn(singletonList(saferpayNotification));

        when(flexibleSearchService.search(flexibleSearchQueryCaptor.capture())).thenReturn(searchResult);

        List<SaferpayNotificationModel> result = defaultPaymentNotificationDao.findAllPaymentNotifications();
        assertThat(result).isNotEmpty().contains(saferpayNotification);

        FlexibleSearchQuery flexibleSearchQuery = flexibleSearchQueryCaptor.getValue();
        assertThat(flexibleSearchQuery).isNotNull();
        assertThat(flexibleSearchQuery.getQuery()).isEqualTo("GET {SaferpayNotification}");
        verify(flexibleSearchService).search(flexibleSearchQuery);
    }
}