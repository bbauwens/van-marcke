package com.vanmarcke.saferpay.api.populators;

import com.vanmarcke.saferpay.api.data.RequestHeader;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;
import de.hybris.platform.store.BaseStoreModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class RequestHeaderPopulatorTest {

    @Mock
    private KeyGenerator requestIdGenerator;
    @InjectMocks
    private RequestHeaderPopulator requestHeaderPopulator;

    @Test
    public void testPopulate() {
        AbstractOrderModel abstractOrderModel = mock(AbstractOrderModel.class);
        BaseStoreModel baseStoreModel = mock(BaseStoreModel.class);

        when(baseStoreModel.getSaferpayCustomerId()).thenReturn("saferpay-customer-id");
        when(baseStoreModel.getSaferpaySpecVersion()).thenReturn("saferpay-spec-version");

        when(abstractOrderModel.getCode()).thenReturn("order-code");
        when(abstractOrderModel.getStore()).thenReturn(baseStoreModel);

        when(requestIdGenerator.generate()).thenReturn("generated-request-id");

        RequestHeader result = new RequestHeader();
        requestHeaderPopulator.populate(abstractOrderModel, result);

        assertThat(result.getCustomerId()).isEqualTo("saferpay-customer-id");
        assertThat(result.getSpecVersion()).isEqualTo("saferpay-spec-version");
        assertThat(result.getRequestId()).isEqualTo("order-code-generated-request-id");
        assertThat(result.getRetryIndicator()).isZero();
    }

}