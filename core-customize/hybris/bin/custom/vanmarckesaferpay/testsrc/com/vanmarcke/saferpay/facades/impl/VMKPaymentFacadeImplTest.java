package com.vanmarcke.saferpay.facades.impl;

import com.vanmarcke.saferpay.exceptions.PaymentValidationException;
import com.vanmarcke.saferpay.exceptions.SaferpayException;
import com.vanmarcke.saferpay.facades.data.CartTokenData;
import com.vanmarcke.saferpay.services.VMKPaymentOrderService;
import com.vanmarcke.saferpay.services.VMKPaymentService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commerceservices.strategies.CheckoutCustomerStrategy;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKPaymentFacadeImplTest {

    private static final String PAYMENT_PROCESS_SEVERE_ERROR = "checkout.severe.error.process.payment";

    private static final String TOKEN = RandomStringUtils.randomAlphabetic(10);
    private static final String CART_UID = RandomStringUtils.randomAlphabetic(10);
    private static final Boolean IS_VALID = RandomUtils.nextBoolean();

    private CartTokenData cartToken;
    private OrderData orderData;
    private BaseSiteModel baseSite;
    private CartModel cart;

    @Mock
    private CartService cartService;
    @Mock
    private CheckoutCustomerStrategy checkoutCustomerStrategy;
    @Mock
    private VMKPaymentService paymentPageService;
    @Mock
    private VMKPaymentOrderService paymentOrderService;
    @Mock
    private Converter<String, CartTokenData> converter;
    @InjectMocks
    private VMKPaymentFacadeImpl paymentFacade;

    @Rule
    public final ExpectedException thrown = ExpectedException.none();

    @Before
    public void setUp() throws Exception {
        cartToken = new CartTokenData();
        baseSite = mock(BaseSiteModel.class);
        cart = mock(CartModel.class);
        orderData = mock(OrderData.class);
    }

    @Test
    public void testInitialize() {
        CartModel cartModel = mock(CartModel.class);
        CustomerModel userModel = mock(CustomerModel.class);

        when(cartModel.getUser()).thenReturn(userModel);

        when(cartService.hasSessionCart()).thenReturn(true);
        when(cartService.getSessionCart()).thenReturn(cartModel);

        when(checkoutCustomerStrategy.getCurrentUserForCheckout()).thenReturn(userModel);

        when(paymentPageService.initializePayment(cartModel)).thenReturn("/redirect-url");

        String result = paymentFacade.initializePayment();

        assertThat(result).isEqualTo("/redirect-url");
    }

    @Test
    public void testInitialize_withoutSessionCart() {
        when(cartService.hasSessionCart()).thenReturn(false);

        String result = paymentFacade.initializePayment();

        assertThat(result).isNull();

        verify(cartService, never()).getSessionCart();
        verify(paymentPageService, never()).initializePayment(any(CartModel.class));
    }

    @Test
    public void testInitialize_withInvalidUserForCheckout() {
        CartModel cartModel = mock(CartModel.class);
        CustomerModel userModel = mock(CustomerModel.class);
        CustomerModel checkoutUserModel = mock(CustomerModel.class);

        when(cartModel.getUser()).thenReturn(userModel);

        when(cartService.hasSessionCart()).thenReturn(true);
        when(cartService.getSessionCart()).thenReturn(cartModel);

        when(checkoutCustomerStrategy.getCurrentUserForCheckout()).thenReturn(checkoutUserModel);

        String result = paymentFacade.initializePayment();

        assertThat(result).isNull();

        verifyZeroInteractions(paymentPageService);
    }

    @Test
    public void testProcessPaymentConfirmation_allNull() throws SaferpayException {
        cartToken.setBaseSite(baseSite);
        cartToken.setCartUid(CART_UID);
        when(converter.convert(TOKEN)).thenReturn(cartToken);

        when(paymentOrderService.checkForExistingOrder(cartToken)).thenReturn(null);
        when(paymentOrderService.placeOrderForCartToken(cartToken)).thenReturn(null);

        assertThat(paymentFacade.processPaymentConfirmation(TOKEN)).isNull();
    }

    @Test
    public void testProcessPaymentConfirmation_newOrder() throws SaferpayException {
        cartToken.setBaseSite(baseSite);
        cartToken.setCartUid(CART_UID);
        when(converter.convert(TOKEN)).thenReturn(cartToken);

        when(paymentOrderService.checkForExistingOrder(cartToken)).thenReturn(null);
        when(paymentOrderService.placeOrderForCartToken(cartToken)).thenReturn(orderData);

        assertThat(paymentFacade.processPaymentConfirmation(TOKEN)).isEqualTo(orderData);

        verify(converter).convert(TOKEN);
        verify(paymentOrderService).checkForExistingOrder(cartToken);
        verify(paymentOrderService).placeOrderForCartToken(cartToken);

    }

    @Test
    public void testProcessPaymentConfirmation_existingOrder() throws SaferpayException {
        cartToken.setBaseSite(baseSite);
        cartToken.setCartUid(CART_UID);

        when(converter.convert(TOKEN)).thenReturn(cartToken);

        when(paymentOrderService.checkForExistingOrder(cartToken)).thenReturn(orderData);

        OrderData result = paymentFacade.processPaymentConfirmation(TOKEN);

        assertThat(result).isEqualTo(orderData);

        verify(converter).convert(TOKEN);
        verify(paymentOrderService).checkForExistingOrder(cartToken);
        verify(paymentOrderService, never()).placeOrderForCartToken(cartToken);
    }

    @Test
    public void testConvertToken_tokenNull() throws PaymentValidationException {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("The cart token is empty");

        paymentFacade.convertToken(null);

        verifyZeroInteractions(converter);
    }

    @Test
    public void testConvertToken_cartUidEmpty() throws PaymentValidationException {
        thrown.expect(PaymentValidationException.class);
        thrown.expectMessage(PAYMENT_PROCESS_SEVERE_ERROR);

        when(converter.convert(TOKEN)).thenReturn(cartToken);

        paymentFacade.convertToken(TOKEN);

        verify(converter).convert(TOKEN);
    }

    @Test
    public void testConvertToken_siteNull() throws PaymentValidationException {
        thrown.expect(PaymentValidationException.class);
        thrown.expectMessage(PAYMENT_PROCESS_SEVERE_ERROR);

        when(converter.convert(TOKEN)).thenReturn(cartToken);
        cartToken.setCartUid(CART_UID);
        paymentFacade.convertToken(TOKEN);

        verify(converter).convert(TOKEN);
    }

    @Test
    public void testConvertToken() throws PaymentValidationException {
        when(converter.convert(TOKEN)).thenReturn(cartToken);
        cartToken.setCartUid(CART_UID);
        cartToken.setBaseSite(baseSite);
        paymentFacade.convertToken(TOKEN);

        verify(converter).convert(TOKEN);

        assertThat(cartToken.getBaseSite()).isEqualTo(baseSite);
        assertThat(cartToken.getCartUid()).isEqualTo(CART_UID);
    }

    @Test
    public void testGetSessionCart_noSessionCart() {
        when(cartService.hasSessionCart()).thenReturn(false);

        assertThat(paymentFacade.getSessionCart()).isNull();

        verify(cartService).hasSessionCart();
        verify(cartService, never()).getSessionCart();
    }

    @Test
    public void testGetSessionCart() {
        when(cartService.hasSessionCart()).thenReturn(true);
        when(cartService.getSessionCart()).thenReturn(cart);

        assertThat(paymentFacade.getSessionCart()).isEqualTo(cart);

        verify(cartService).hasSessionCart();
        verify(cartService).getSessionCart();
    }

    @Test
    public void testCheckIfCurrentUserIsTheCartuser_cartNull() {
        when(cartService.hasSessionCart()).thenReturn(false);

        assertThat(paymentFacade.checkIfCurrentUserIsTheCartUser()).isFalse();
    }

    @Test
    public void testCheckIfCurrentUserIsTheCartuser_currentUserNull() {
        when(cartService.hasSessionCart()).thenReturn(true);
        when(cartService.getSessionCart()).thenReturn(cart);

        assertThat(paymentFacade.checkIfCurrentUserIsTheCartUser()).isFalse();
    }

    @Test
    public void testCheckIfCurrentUserIsTheCartuser_usersNotSame() {
        when(cartService.hasSessionCart()).thenReturn(true);
        when(cartService.getSessionCart()).thenReturn(cart);

        CustomerModel user1 = mock(CustomerModel.class);
        CustomerModel user2 = mock(CustomerModel.class);

        when(checkoutCustomerStrategy.getCurrentUserForCheckout()).thenReturn(user1);
        when(cart.getUser()).thenReturn(user2);
        assertThat(paymentFacade.checkIfCurrentUserIsTheCartUser()).isFalse();
    }

    @Test
    public void testCheckIfCurrentUserIsTheCartuser() {
        when(cartService.hasSessionCart()).thenReturn(true);
        when(cartService.getSessionCart()).thenReturn(cart);

        CustomerModel user1 = mock(CustomerModel.class);

        when(checkoutCustomerStrategy.getCurrentUserForCheckout()).thenReturn(user1);
        when(cart.getUser()).thenReturn(user1);
        assertThat(paymentFacade.checkIfCurrentUserIsTheCartUser()).isTrue();
    }
}