package com.vanmarcke.saferpay.utils;

import de.hybris.bootstrap.annotations.UnitTest;
import org.apache.commons.lang.RandomStringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class URLBuilderTest {

    private String BASE_URL = RandomStringUtils.randomAlphabetic(10);
    private String TOKEN = RandomStringUtils.randomAlphabetic(10);
    private String PATH = RandomStringUtils.randomAlphabetic(10);
    private static final String SEPARATOR = "/";


    @Test
    public void testAppend_withBaseURLAndPathAndToken() {
        String result = URLBuilder.aSaferPayUrl()
                .withToken(TOKEN)
                .withPath(PATH)
                .withBaseUrl(BASE_URL)
                .build();
        assertThat(result).isEqualTo(BASE_URL + SEPARATOR + PATH + SEPARATOR + TOKEN);
    }
}