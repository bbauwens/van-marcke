package com.vanmarcke.saferpay.facades.populators;

import com.vanmarcke.saferpay.facades.data.CartTokenData;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.site.BaseSiteService;
import org.apache.commons.lang.RandomStringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKCartTokenDataPopulatorTest {

    private static final String CART_GUID = RandomStringUtils.randomAlphabetic(10);
    private static final String SITE_UID = RandomStringUtils.randomAlphabetic(10);
    private static final String TOKEN = CART_GUID + "_" + SITE_UID;

    @Mock
    private BaseSiteService baseSiteService;

    @InjectMocks
    private VMKCartTokenDataPopulator populator;

    @Test
    public void testPopulate() {
        CartTokenData cartToken = new CartTokenData();
        BaseSiteModel baseSite = mock(BaseSiteModel.class);

        when(baseSiteService.getBaseSiteForUID(SITE_UID))
                .thenReturn(baseSite);

        populator.populate(TOKEN, cartToken);

        assertThat(cartToken.getCartUid()).isEqualTo(CART_GUID);
        assertThat(cartToken.getBaseSite()).isEqualTo(baseSite);

        verify(baseSiteService).getBaseSiteForUID(SITE_UID);
    }
}