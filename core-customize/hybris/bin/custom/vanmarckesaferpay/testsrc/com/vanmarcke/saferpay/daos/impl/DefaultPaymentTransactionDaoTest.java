package com.vanmarcke.saferpay.daos.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.fest.assertions.Assertions.assertThat;
import static org.fest.assertions.MapAssert.entry;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultPaymentTransactionDaoTest {

    @Mock
    private FlexibleSearchService flexibleSearchService;
    @InjectMocks
    private DefaultPaymentTransactionDao defaultPaymentTransactionDao;

    @Captor
    private ArgumentCaptor<FlexibleSearchQuery> flexibleSearchQueryCaptor;

    @Test
    public void testFindByCode() {
        PaymentTransactionModel paymentTransactionModel = mock(PaymentTransactionModel.class);
        SearchResult searchResult = mock(SearchResult.class);

        when(searchResult.getResult()).thenReturn(singletonList(paymentTransactionModel));

        when(flexibleSearchService.search(flexibleSearchQueryCaptor.capture())).thenReturn(searchResult);

        PaymentTransactionModel result = defaultPaymentTransactionDao.findByCode("payment-transaction-code");
        assertThat(result).isEqualTo(paymentTransactionModel);

        FlexibleSearchQuery flexibleSearchQuery = flexibleSearchQueryCaptor.getValue();
        assertThat(flexibleSearchQuery).isNotNull();
        assertThat(flexibleSearchQuery.getQuery()).isEqualTo("GET {PaymentTransaction} WHERE {code}=?code");
        assertThat(flexibleSearchQuery.getQueryParameters()).hasSize(1).includes(entry("code", "payment-transaction-code"));
    }

    @Test(expected = UnknownIdentifierException.class)
    public void testFindByCode_withUnknown() {
        SearchResult searchResult = mock(SearchResult.class);

        when(searchResult.getResult()).thenReturn(emptyList());

        when(flexibleSearchService.search(flexibleSearchQueryCaptor.capture())).thenReturn(searchResult);

        defaultPaymentTransactionDao.findByCode("payment-transaction-code");
    }

    @Test(expected = AmbiguousIdentifierException.class)
    public void testFindByCode_withAmbiguous() {
        PaymentTransactionModel paymentTransactionModel1 = mock(PaymentTransactionModel.class);
        PaymentTransactionModel paymentTransactionModel2 = mock(PaymentTransactionModel.class);
        SearchResult searchResult = mock(SearchResult.class);

        when(searchResult.getResult()).thenReturn(asList(paymentTransactionModel1, paymentTransactionModel2));

        when(flexibleSearchService.search(flexibleSearchQueryCaptor.capture())).thenReturn(searchResult);

        defaultPaymentTransactionDao.findByCode("payment-transaction-code");
    }

}