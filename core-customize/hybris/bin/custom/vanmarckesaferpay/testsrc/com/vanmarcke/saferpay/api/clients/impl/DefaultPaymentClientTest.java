package com.vanmarcke.saferpay.api.clients.impl;

import com.vanmarcke.saferpay.api.commands.PaymentPageAssertCommand;
import com.vanmarcke.saferpay.api.commands.PaymentPageInitializeCommand;
import com.vanmarcke.saferpay.api.commands.TransactionCaptureCommand;
import com.vanmarcke.saferpay.api.data.*;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import eu.elision.integration.command.Response;
import eu.elision.integration.command.executer.CommandExecutor;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpStatus.OK;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultPaymentClientTest {

    @Mock
    private CommandExecutor commandExecutor;
    @Mock
    private Converter<AbstractOrderModel, PaymentPageInitializeRequest> initializeConverter;
    @Mock
    private Converter<AbstractOrderModel, PaymentPageAssertRequest> assertConverter;
    @Mock
    private Converter<AbstractOrderModel, TransactionCaptureRequest> captureConverter;

    private DefaultPaymentClient paymentClient;

    @Before
    public void setup() {
        paymentClient = new DefaultPaymentClient(commandExecutor, initializeConverter, assertConverter, captureConverter);
    }

    @Test
    public void testInitialize() {
        CartModel cartModel = mock(CartModel.class);

        PaymentPageInitializeRequest initializeRequest = mock(PaymentPageInitializeRequest.class);
        Response<PaymentPageInitializeResponse> response = mock(Response.class);
        PaymentPageInitializeResponse payload = mock(PaymentPageInitializeResponse.class);

        when(response.getHttpStatus()).thenReturn(OK);
        when(response.getPayLoad()).thenReturn(payload);

        when(initializeConverter.convert(cartModel)).thenReturn(initializeRequest);

        when(commandExecutor.executeCommand(any(PaymentPageInitializeCommand.class), any(null))).thenReturn(response);

        PaymentPageInitializeResponse result = paymentClient.initialize(cartModel);

        assertThat(result).isEqualTo(payload);
    }

    @Test
    public void testValidate() {
        AbstractOrderModel abstractOrderModel = mock(AbstractOrderModel.class);

        PaymentPageAssertRequest request = mock(PaymentPageAssertRequest.class);
        Response<PaymentPageAssertResponse> response = mock(Response.class);
        PaymentPageAssertResponse payload = mock(PaymentPageAssertResponse.class);

        when(response.getHttpStatus()).thenReturn(OK);
        when(response.getPayLoad()).thenReturn(payload);

        when(assertConverter.convert(abstractOrderModel)).thenReturn(request);

        when(commandExecutor.executeCommand(any(PaymentPageAssertCommand.class), any(null))).thenReturn(response);

        PaymentPageAssertResponse result = paymentClient.validate(abstractOrderModel);

        assertThat(result).isEqualTo(payload);
    }

    @Test
    public void testCapture() {
        AbstractOrderModel abstractOrderModel = mock(AbstractOrderModel.class);

        TransactionCaptureRequest request = mock(TransactionCaptureRequest.class);
        Response<TransactionCaptureResponse> response = mock(Response.class);
        TransactionCaptureResponse payload = mock(TransactionCaptureResponse.class);

        when(response.getHttpStatus()).thenReturn(OK);
        when(response.getPayLoad()).thenReturn(payload);

        when(captureConverter.convert(abstractOrderModel)).thenReturn(request);

        when(commandExecutor.executeCommand(any(TransactionCaptureCommand.class), any(null))).thenReturn(response);

        TransactionCaptureResponse result = paymentClient.capture(abstractOrderModel);

        assertThat(result).isEqualTo(payload);
    }

}