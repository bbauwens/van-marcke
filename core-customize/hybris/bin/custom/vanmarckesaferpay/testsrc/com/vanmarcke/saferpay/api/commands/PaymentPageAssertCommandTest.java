package com.vanmarcke.saferpay.api.commands;

import com.vanmarcke.saferpay.api.data.PaymentPageAssertRequest;
import com.vanmarcke.saferpay.api.data.PaymentPageAssertResponse;
import de.hybris.bootstrap.annotations.UnitTest;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;

import java.text.MessageFormat;
import java.util.HashMap;

import static org.fest.assertions.Assertions.assertThat;
import static org.springframework.http.MediaType.APPLICATION_JSON;

/**
 * The {@link PaymentPageAssertCommandTest} class contains the unit tests for the {@link PaymentPageAssertCommand}
 * class.
 *
 * @author Taki Korovessis
 * @since 30-01-2020
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class PaymentPageAssertCommandTest {

    private static final String BASE_URL = RandomStringUtils.random(10);
    private static final String USERNAME = RandomStringUtils.random(10);
    private static final String PASSWORD = RandomStringUtils.random(10);

    @Mock
    private PaymentPageAssertRequest request;

    private PaymentPageAssertCommand command;

    @Before
    public void setUp() {
        command = new PaymentPageAssertCommand(BASE_URL, USERNAME, PASSWORD, request);
    }

    @Test
    public void testConstructor() {
        assertThat(this.command.getHttpMethod()).isEqualTo(HttpMethod.POST);
        assertThat(this.command.getServiceName()).isEqualTo("paymentPageAssert");
        assertThat(this.command.getUrl()).isEqualTo("/Payment/v1/PaymentPage/Assert");
        assertThat(this.command.getPayLoad()).isEqualTo(request);
        assertThat(this.command.getResponseType()).isEqualTo(PaymentPageAssertResponse.class);
    }

    @Test
    public void testBuildHttpHeaders() {
        HttpHeaders httpHeaders = new HttpHeaders();

        command.buildHttpHeaders(httpHeaders);

        assertThat(httpHeaders).hasSize(2);
        assertThat(httpHeaders.getAccept()).containsExactly(APPLICATION_JSON, APPLICATION_JSON);
        assertThat(httpHeaders.getContentType()).isEqualTo(APPLICATION_JSON);
    }

    @Test
    public void testBuildUri() {
        String actualURL = command.buildUri(new HashMap<>());

        assertThat(actualURL).isEqualTo(MessageFormat.format("{0}/Payment/v1/PaymentPage/Assert", BASE_URL));
    }
}