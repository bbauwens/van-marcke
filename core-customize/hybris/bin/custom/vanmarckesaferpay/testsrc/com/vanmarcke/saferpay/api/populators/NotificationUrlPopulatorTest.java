package com.vanmarcke.saferpay.api.populators;

import com.vanmarcke.saferpay.api.data.PendingNotification;
import com.vanmarcke.saferpay.services.VMKPaymentUrlService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import org.apache.commons.lang.RandomStringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class NotificationUrlPopulatorTest {

    private static final String CONFIG_URL_PATH_NOTIFY = "saferpay.paymentpage.url.path.notify";
    private static final String NOTIFY_URL = RandomStringUtils.randomAlphabetic(10);

    @Mock
    private VMKPaymentUrlService paymentUrlService;

    @InjectMocks
    private NotificationUrlPopulator<AbstractOrderModel> notificationPopulator;

    @Test
    public void testPopulate() {
        AbstractOrderModel cart = mock(AbstractOrderModel.class);
        PendingNotification result = new PendingNotification();

        when(paymentUrlService.getUrlForCart(cart, CONFIG_URL_PATH_NOTIFY))
                .thenReturn(NOTIFY_URL);

        notificationPopulator.populate(cart, result);

        assertThat(result.getNotifyUrl()).isEqualTo(NOTIFY_URL);

        verify(paymentUrlService).getUrlForCart(cart, CONFIG_URL_PATH_NOTIFY);
    }
}