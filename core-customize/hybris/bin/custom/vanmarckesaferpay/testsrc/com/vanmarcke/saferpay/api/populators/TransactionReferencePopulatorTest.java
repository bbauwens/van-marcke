package com.vanmarcke.saferpay.api.populators;

import com.vanmarcke.saferpay.api.data.TransactionReference;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import static java.util.Collections.singletonList;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * The {@link TransactionReferencePopulatorTest} class contains the unit tests for the
 * {@link TransactionReferencePopulator} class.
 *
 * @author Taki Korovessis, Tom van den Berg, Christiaan Janssen
 * @since 31-01-2020
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TransactionReferencePopulatorTest {

    private static final String ORDER_CODE = RandomStringUtils.random(10);
    private static final String REQUEST_ID = RandomStringUtils.random(10);

    @InjectMocks
    private TransactionReferencePopulator<OrderModel> transactionReferencePopulator;

    @Test
    public void testPopulate_withTransactionId() {
        PaymentTransactionEntryModel paymentTransactionEntryModel = mock(PaymentTransactionEntryModel.class);
        when(paymentTransactionEntryModel.getRequestId()).thenReturn(REQUEST_ID);

        PaymentTransactionModel paymentTransactionModel = mock(PaymentTransactionModel.class);
        when(paymentTransactionModel.getEntries()).thenReturn(singletonList(paymentTransactionEntryModel));

        OrderModel orderModel = mock(OrderModel.class);
        when(orderModel.getCode()).thenReturn(ORDER_CODE);
        when(orderModel.getPaymentTransactions()).thenReturn(singletonList(paymentTransactionModel));

        TransactionReference result = new TransactionReference();

        transactionReferencePopulator.populate(orderModel, result);

        assertThat(result.getOrderId()).isNull();
        assertThat(result.getTransactionId()).isEqualTo(REQUEST_ID);
    }

    @Test
    public void testPopulate_withoutTransactionId() {
        PaymentTransactionEntryModel paymentTransactionEntryModel = mock(PaymentTransactionEntryModel.class);

        PaymentTransactionModel paymentTransactionModel = mock(PaymentTransactionModel.class);
        when(paymentTransactionModel.getEntries()).thenReturn(singletonList(paymentTransactionEntryModel));

        OrderModel orderModel = mock(OrderModel.class);
        when(orderModel.getCode()).thenReturn(ORDER_CODE);
        when(orderModel.getPaymentTransactions()).thenReturn(singletonList(paymentTransactionModel));

        TransactionReference result = new TransactionReference();

        transactionReferencePopulator.populate(orderModel, result);

        assertThat(result.getOrderId()).isEqualTo(ORDER_CODE);
        assertThat(result.getTransactionId()).isNull();
    }
}
