package com.vanmarcke.saferpay.payment.commands.impl;

import com.vanmarcke.saferpay.api.clients.PaymentClient;
import com.vanmarcke.saferpay.api.data.TransactionCaptureResponse;
import com.vanmarcke.saferpay.daos.PaymentTransactionDao;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.payment.AdapterException;
import de.hybris.platform.payment.commands.request.CaptureRequest;
import de.hybris.platform.payment.commands.result.CaptureResult;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Currency;
import java.util.Date;

import static de.hybris.platform.payment.dto.TransactionStatus.ACCEPTED;
import static de.hybris.platform.payment.dto.TransactionStatus.REVIEW;
import static de.hybris.platform.payment.dto.TransactionStatusDetails.REVIEW_NEEDED;
import static de.hybris.platform.payment.dto.TransactionStatusDetails.SUCCESFULL;
import static java.math.BigDecimal.TEN;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultCaptureCommandTest {

    @Mock
    private PaymentTransactionDao paymentTransactionDao;
    @Mock
    private PaymentClient paymentClient;
    @InjectMocks
    private DefaultCaptureCommand defaultCaptureCommand;

    @Test
    public void testPerform_captured() {
        AbstractOrderModel abstractOrderModel = mock(AbstractOrderModel.class);
        CaptureRequest captureRequest = mock(CaptureRequest.class);
        Currency currency = Currency.getInstance("EUR"); // cannot mock final class
        Date transactionCaptureDate = mock(Date.class);
        PaymentTransactionModel paymentTransactionModel = mock(PaymentTransactionModel.class);
        TransactionCaptureResponse transactionCaptureResponse = mock(TransactionCaptureResponse.class);

        when(captureRequest.getMerchantTransactionCode()).thenReturn("user-uuid-AUTHORIZATION-1");
        when(captureRequest.getCurrency()).thenReturn(currency);
        when(captureRequest.getTotalAmount()).thenReturn(TEN);

        when(transactionCaptureResponse.getCaptureId()).thenReturn("transaction-capture-id");
        when(transactionCaptureResponse.getDate()).thenReturn(transactionCaptureDate);
        when(transactionCaptureResponse.getStatus()).thenReturn("CAPTURED");

        when(paymentTransactionModel.getOrder()).thenReturn(abstractOrderModel);

        when(paymentTransactionDao.findByCode("user-uuid")).thenReturn(paymentTransactionModel);

        when(paymentClient.capture(abstractOrderModel)).thenReturn(transactionCaptureResponse);

        CaptureResult result = defaultCaptureCommand.perform(captureRequest);

        assertThat(result).isNotNull();
        assertThat(result.getTotalAmount()).isEqualTo(TEN);
        assertThat(result.getCurrency()).isEqualTo(currency);
        assertThat(result.getRequestTime()).isEqualTo(transactionCaptureDate);
        assertThat(result.getTransactionStatus()).isEqualTo(ACCEPTED);
        assertThat(result.getTransactionStatusDetails()).isEqualTo(SUCCESFULL);
        assertThat(result.getRequestId()).isEqualTo("transaction-capture-id");
    }

    @Test
    public void testPerform_pendingCapture() {
        AbstractOrderModel abstractOrderModel = mock(AbstractOrderModel.class);
        CaptureRequest captureRequest = mock(CaptureRequest.class);
        Currency currency = Currency.getInstance("EUR"); // cannot mock final class
        PaymentTransactionModel paymentTransactionModel = mock(PaymentTransactionModel.class);
        TransactionCaptureResponse transactionCaptureResponse = mock(TransactionCaptureResponse.class);

        when(captureRequest.getMerchantTransactionCode()).thenReturn("user-uuid-AUTHORIZATION-1");
        when(captureRequest.getCurrency()).thenReturn(currency);
        when(captureRequest.getTotalAmount()).thenReturn(TEN);
        when(captureRequest.getRequestId()).thenReturn("request-id");

        when(transactionCaptureResponse.getStatus()).thenReturn("PENDING");

        when(paymentTransactionModel.getOrder()).thenReturn(abstractOrderModel);

        when(paymentTransactionDao.findByCode("user-uuid")).thenReturn(paymentTransactionModel);

        when(paymentClient.capture(abstractOrderModel)).thenReturn(transactionCaptureResponse);

        CaptureResult result = defaultCaptureCommand.perform(captureRequest);

        assertThat(result).isNotNull();
        assertThat(result.getTotalAmount()).isEqualTo(TEN);
        assertThat(result.getCurrency()).isEqualTo(currency);
        assertThat(result.getRequestTime()).isNull();
        assertThat(result.getTransactionStatus()).isEqualTo(REVIEW);
        assertThat(result.getTransactionStatusDetails()).isEqualTo(REVIEW_NEEDED);
        assertThat(result.getRequestId()).isEqualTo("request-id");
    }

    @Test
    public void testPerform_failedCapture() {
        AbstractOrderModel abstractOrderModel = mock(AbstractOrderModel.class);
        CaptureRequest captureRequest = mock(CaptureRequest.class);
        PaymentTransactionModel paymentTransactionModel = mock(PaymentTransactionModel.class);
        TransactionCaptureResponse transactionCaptureResponse = mock(TransactionCaptureResponse.class);

        when(captureRequest.getMerchantTransactionCode()).thenReturn("user-uuid-AUTHORIZATION-1");

        when(transactionCaptureResponse.getErrorName()).thenReturn("CAPTURED_FAILED");

        when(paymentTransactionModel.getOrder()).thenReturn(abstractOrderModel);

        when(paymentTransactionDao.findByCode("user-uuid")).thenReturn(paymentTransactionModel);

        when(paymentClient.capture(abstractOrderModel)).thenReturn(transactionCaptureResponse);

        try {
            defaultCaptureCommand.perform(captureRequest);
        } catch (AdapterException e) {
            assertThat(e.getMessage()).isEqualTo("Capture failed : CAPTURED_FAILED");
        }
    }

    @Test
    public void testPerform_withoutPaymentTransaction() {
        CaptureRequest captureRequest = mock(CaptureRequest.class);

        when(captureRequest.getMerchantTransactionCode()).thenReturn("user-uuid-AUTHORIZATION-1");

        when(paymentTransactionDao.findByCode("user-uuid")).thenThrow(Exception.class);

        try {
            defaultCaptureCommand.perform(captureRequest);
        } catch (AdapterException e) {
            assertThat(e.getMessage()).isEqualTo("Capture failed : null");
        }

        verifyZeroInteractions(paymentClient);
    }

    @Test
    public void testPerform_withoutInvalidPaymentTransaction() {
        CaptureRequest captureRequest = mock(CaptureRequest.class);

        when(captureRequest.getMerchantTransactionCode()).thenReturn("AUTHORIZATION-1");

        try {
            defaultCaptureCommand.perform(captureRequest);
        } catch (AdapterException e) {
            assertThat(e.getMessage()).isEqualTo("Capture failed : Invalid merchant transaction code 'AUTHORIZATION-1'");
        }

        verifyZeroInteractions(paymentTransactionDao);
        verifyZeroInteractions(paymentClient);
    }
}