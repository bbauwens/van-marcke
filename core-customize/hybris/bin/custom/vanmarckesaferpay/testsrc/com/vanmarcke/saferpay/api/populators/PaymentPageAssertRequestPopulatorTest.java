package com.vanmarcke.saferpay.api.populators;

import com.vanmarcke.saferpay.api.data.PaymentPageAssertRequest;
import com.vanmarcke.saferpay.api.data.RequestHeader;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static java.util.Collections.singletonList;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class PaymentPageAssertRequestPopulatorTest {

    @Mock
    private Converter<AbstractOrderModel, RequestHeader> requestHeaderConverter;

    @InjectMocks
    private PaymentPageAssertRequestPopulator<AbstractOrderModel> paymentPageAssertRequestPopulator;

    @Test
    public void testPopulate() {
        AbstractOrderModel abstractOrderModel = mock(AbstractOrderModel.class);
        PaymentTransactionModel paymentTransactionModel = mock(PaymentTransactionModel.class);
        RequestHeader requestHeader = mock(RequestHeader.class);

        when(paymentTransactionModel.getRequestToken()).thenReturn("saferpay-token");

        when(abstractOrderModel.getPaymentTransactions()).thenReturn(singletonList(paymentTransactionModel));

        when(requestHeaderConverter.convert(abstractOrderModel)).thenReturn(requestHeader);

        PaymentPageAssertRequest result = new PaymentPageAssertRequest();
        paymentPageAssertRequestPopulator.populate(abstractOrderModel, result);

        assertThat(result.getRequestHeader()).isEqualTo(requestHeader);
        assertThat(result.getToken()).isEqualTo("saferpay-token");
    }

}