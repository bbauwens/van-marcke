package com.vanmarcke.saferpay.services.impl;

import com.vanmarcke.core.order.VMKOrderDao;
import com.vanmarcke.saferpay.exceptions.PaymentValidationException;
import com.vanmarcke.saferpay.facades.data.CartTokenData;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.order.OrderModel;
import org.apache.commons.lang.RandomStringUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKPaymentOrderPollingServiceImplTest {

    private static final String CART_UID = RandomStringUtils.randomAlphabetic(10);
    private static final int NUMBER_OF_SECONDS = 2;

    private BaseSiteModel baseSite;
    private OrderModel orderModel;
    private CartTokenData cartToken;

    @Mock
    private VMKOrderDao orderDao;

    private VMKPaymentOrderPollingServiceImpl pollingService;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setUp() throws Exception {
        pollingService = new VMKPaymentOrderPollingServiceImpl(orderDao, NUMBER_OF_SECONDS);

        orderModel = mock(OrderModel.class);
        baseSite = mock(BaseSiteModel.class);
        cartToken = new CartTokenData();
        cartToken.setCartUid(CART_UID);
        cartToken.setBaseSite(baseSite);
    }

    @Test
    public void testPollOrderCreationProcess_nothingFound() throws PaymentValidationException {
        when(orderDao.findOrderByGuidAndSite(CART_UID, baseSite))
                .thenReturn(null);

        expectedException.expectMessage("checkout.severe.error.process.payment");
        expectedException.expect(PaymentValidationException.class);

        pollingService.pollOrderCreationProcess(cartToken);

        verify(orderDao, times(NUMBER_OF_SECONDS + 1)).findOrderByGuidAndSite(CART_UID, baseSite);
    }

    @Test
    public void testPollOrderCreationProcess_foundOnSecondTry() throws PaymentValidationException {
        when(orderDao.findOrderByGuidAndSite(CART_UID, baseSite))
                .thenReturn(null, orderModel);

        OrderModel result = pollingService.pollOrderCreationProcess(cartToken);

        assertThat(result).isEqualTo(orderModel);

        verify(orderDao, times(NUMBER_OF_SECONDS)).findOrderByGuidAndSite(CART_UID, baseSite);
    }
}