package scripts.datadump
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Script which generates a tree of the products / variantProducts and categories for a specified catalogVersion
//
// Sample output:
//  category.1
//      category.1.1
//          product.1.1.1
//          product.1.1.2
//      category.1.2
//          product.1.2.1
//          category.1.2.1
//              product.1.2.1.1
//  category.2
//
// @author vangeda
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

import de.hybris.platform.category.model.CategoryModel
import de.hybris.platform.core.model.product.ProductModel

////////////////////////////////////////////
// configuration of the script goes here...
////////////////////////////////////////////
catalogToExport = "vanmarckeProductCatalog";
catalogVersionToExport = "Staged";
includeProducts = true;
////////////////////////////////////////////
// END OF CONFIGURATION
////////////////////////////////////////////

flexibleSearch = spring.getBean("flexibleSearchService");
catalogVersionService = spring.getBean("catalogVersionService");
categoryService = spring.getBean("categoryService");
productService = spring.getBean("productService");
Tree categoryTree = new Tree();
Tree objectTree = new Tree();

// fetch catalogs and select required catalog
catalogs = flexibleSearch.search(/select {pk} from {Catalog}/).getResult();
catalogs.each {
    if (catalogToExport.equals(it.getId())) {
        catalogVersion = catalogVersionService.getCatalogVersion(it.getId(), catalogVersionToExport);
    }
}

// fetch all categories
result = flexibleSearch.search("select {pk} from {Category} where {catalogVersion} = '" + catalogVersion.getPk() + "'").getResult();
result.each {
    List<CategoryModel> categories = categoryService.getPath(it);
    Node categoryNode = null;
    if (categories.size() > 1) {
        categoryNode = new Node(categories.get(0).getCode(), categories.get(1).getCode(), it, "Category");
    } else {
        categoryNode = new Node(categories.get(0).getCode(), null, it, "Category");
    }
    categoryTree.getElements().add(categoryNode);
}

//to avoid ConcurrentModificationException...
objectTree = cloneTree(categoryTree);

if (includeProducts) {
    //since there are lots of duplicate categories, it is better to handle product fetching in a separate loop (in the above loop, it would take to much resources
    for (categoryElement in categoryTree.getElements()) {

        // now do some stuff to fetch products and variants for this category
        //List<ProductModel> products = productService.getProductsForCategory(categoryElement.getObject());
        List<ProductModel> products = ((CategoryModel) categoryElement.getObject()).getProducts();

        for (product in products) {
            Node productNode = new Node(product.getCode(), categoryElement.getValue(), "BaseProduct");
            objectTree.getElements().add(productNode);

            // now the variants
            for (variant in product.getVariants()) {
                Node variantNode = new Node(variant.getCode(), product.getCode(), "Variant");
                objectTree.getElements().add(variantNode);
            }
        }
    }
}

// group elements in the tree
Set rootCategoryElements = new HashSet();
for (element in objectTree.getElements()) {
    node1 = objectTree.find(element.getValue());
    node2 = objectTree.find(element.getParentValue());

    if (node1 != null) {
        node1.setParent(node2);
    }
    if (node2 != null) {
        node2.addChild(node1);
    }
    if (element.getParentValue() == null) {
        rootCategoryElements.add(node1);
    }
}

// Loop over root elements and start printing the tree
for (rootCategory in rootCategoryElements) {
    int level = 0;
    printANode(0, rootCategory);
    displaySubTree(objectTree, level, rootCategory);
}

// Print a single Node
void printANode(level, Node item) {
    print item.getType();
    print "\t" * (level+1);
    println item.getValue();
}

// print the subTree
void displaySubTree(Tree tree, int level, Node node) {
    Set<Node> subItems = node.getChildren();

    for (item in subItems) {
        printANode(level + 1, item);
        displaySubTree(tree, level + 1, item);
    }
}

// Dirty solution to clone the tree (to avoid concurrentModificationExceptions...
Tree cloneTree(Tree original) {
    Tree clone = new Tree();
    Set<Node> nodeSet = new HashSet<>();
    nodeSet.addAll(original.getElements());
    clone.setElements(nodeSet);
    return clone;
}

// Tree object
public class Tree {
    Set<Node> elements;

    public Set<Node> getElements() {
        return elements;
    }

    public Tree() {
        elements = new HashSet();
    }

    public void add(Node element) {
        elements.add(element);
    }

    public Node find(String value) {
        for (it in elements) {
            if (it.getValue() == value) {
                return it;
            }
        }
    }
}

// Node in the Tree
public class Node {
    private String type = null;
    private Node parent = null;
    private Set<Node> children = null;
    private String value = "";
    private String parentValue = "";
    private Object object = null;

    public Node(String value, String parent, String type) {
        this.children = new HashSet<>();
        this.value = value;
        this.parentValue = parent;
        this.type = type;
    }
    //to be able to keep track of the linked category...
    public Node(String value, String parent, Object object, String type) {
        this.children = new HashSet<>();
        this.value = value;
        this.parentValue = parent;
        this.object = object;
        this.type = type;
    }

    public Set<Node> getChildren() {
        return children;
    }

    public void addChild(Node child) {
        children.add(child); child.addParent(this);
    }

    public addParent(Node parentNode) {
        parent = parentNode;
    }

    public getValue() {
        return value;
    }

    public String getParentValue() {
        return parentValue;
    }

    public setParent(Node node) {
        parent = node;
    }

    public Object getObject() {
        return object;
    }

    public String getType() {
        return type;
    }
}

