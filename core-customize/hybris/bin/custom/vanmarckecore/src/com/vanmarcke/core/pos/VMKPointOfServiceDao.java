package com.vanmarcke.core.pos;

import de.hybris.platform.storelocator.PointOfServiceDao;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.util.List;

/**
 * DAO used for Point of Service
 */
public interface VMKPointOfServiceDao extends PointOfServiceDao {

    /**
     * find all Point of Services for a given list of IDs
     *
     * @param storeIDs the given list of IDs
     * @return the results of the search or {null} if none found
     */
    List<PointOfServiceModel> getPointOfServicesForIDs(List<String> storeIDs);
}
