package com.vanmarcke.core.order.impl;

import com.vanmarcke.core.order.VMKOrderDao;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.order.daos.impl.DefaultOrderDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.time.TimeService;

import java.util.Date;
import java.util.List;

import static org.apache.commons.lang3.time.DateUtils.addHours;

/**
 * Implementation for {@link VMKOrderDao}
 *
 * @author Niels Raemaekers
 * @since 19-05-2020
 */
public class VMKOrderDaoImpl extends DefaultOrderDao implements VMKOrderDao {

    private static final String QUERY_FAILED_ORDERS = "SELECT {pk} FROM {Order} WHERE {externalCode} IS NULL AND {creationtime} < ?date";
    private static final String QUERY_ORDERS_BY_GUID_AND_SITE = "SELECT {pk} FROM {Order} WHERE {guid} = ?guid AND {site} = ?baseSite";

    private final TimeService timeService;

    /**
     * Constructor for {@link VMKOrderDaoImpl}
     *
     * @param timeService the {@link TimeService}
     */
    public VMKOrderDaoImpl(TimeService timeService) {
        this.timeService = timeService;
    }

    /**
     * @inheritDoc
     */
    @Override
    public List<OrderModel> findFailedOrders() {
        final FlexibleSearchQuery fQuery = new FlexibleSearchQuery(QUERY_FAILED_ORDERS);
        final Date date = addHours(timeService.getCurrentTime(), -1);
        fQuery.addQueryParameter("date", date);

        SearchResult<OrderModel> result = getFlexibleSearchService().search(fQuery);
        return result.getResult();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OrderModel findOrderByGuidAndSite(String cartGuid, BaseSiteModel baseSite) {
        FlexibleSearchQuery fQuery = new FlexibleSearchQuery(QUERY_ORDERS_BY_GUID_AND_SITE);
        fQuery.addQueryParameter("guid", cartGuid);
        fQuery.addQueryParameter("baseSite", baseSite);

        SearchResult<OrderModel> result = getFlexibleSearchService().search(fQuery);
        return result.getCount() > 0 ? result.getResult().get(0) : null;
    }
}
