package com.vanmarcke.core.store;

import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.daos.BaseStoreDao;

import java.util.List;

public interface VMKBaseStoreDao extends BaseStoreDao {

    /**
     * Get all base stores which have solr facet search config assigned.
     *
     * @return list of base stores
     */
    List<BaseStoreModel> findAllStoresWithSolrFacetSearchConfig();
}
