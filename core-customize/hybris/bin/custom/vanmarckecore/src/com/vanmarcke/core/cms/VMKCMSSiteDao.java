package com.vanmarcke.core.cms;

import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.daos.CMSSiteDao;
import de.hybris.platform.commerceservices.enums.SiteChannel;

import java.util.List;

/**
 * Custom implementation of CMSSiteDao
 */
public interface VMKCMSSiteDao extends CMSSiteDao {

    /**
     * Find all the sites for the given {@link SiteChannel}
     *
     * @param channel the {@link SiteChannel}
     * @return a list with CMSSiteModels or an empty list
     */
    List<CMSSiteModel> findAllSitesForChannel(SiteChannel channel);
}