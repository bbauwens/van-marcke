package com.vanmarcke.core.wishlist2;

import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.core.servicelayer.data.PaginationData;
import de.hybris.platform.core.servicelayer.data.SearchPageData;
import de.hybris.platform.core.servicelayer.data.SortData;
import de.hybris.platform.wishlist2.impl.daos.Wishlist2Dao;
import de.hybris.platform.wishlist2.model.Wishlist2EntryModel;
import de.hybris.platform.wishlist2.model.Wishlist2Model;
import org.springframework.lang.NonNull;

import java.util.List;

/**
 * DAO for Wishlist related operations
 */
public interface VMKWishlist2Dao extends Wishlist2Dao {

    /**
     * Returns a {@link Wishlist2Model} instance for the given {@code user} and {@code name}.
     *
     * @param user the user
     * @param name the name
     * @return the {@link List} of {@link Wishlist2Model} instances
     */
    Wishlist2Model getWishlistByUserAndName(@NonNull UserModel user, @NonNull String name);

    /**
     * Returns a {@link Wishlist2Model} instance for the given {@code user} and {@code name}.
     *
     * @param user the user
     * @param name the name
     * @return the {@link List} of {@link Wishlist2Model} instances
     */
    Wishlist2Model findWishListByUserAndName(@NonNull UserModel user, @NonNull String name);

    /**
     * Returns the {@link Wishlist2EntryModel} instances for the given {@code user} and {@code pk}.
     *
     * @param pk the primary key
     * @return the {@link Wishlist2EntryModel} instances
     */
    List<Wishlist2EntryModel> findWishListEntriesByUserAndPK(@NonNull UserModel user, @NonNull String pk);

    /**
     * Returns the {@link Wishlist2Model} instances for the given {@code user} using {@code paginationData} and {@code sortData}.
     *
     * @param user the user instance
     * @param paginationData Pagination information
     * @param sortData Sorting information
     * @return the {@link Wishlist2Model} instances
     */
    SearchPageData<Wishlist2Model> getWishlistsForUser(@NonNull UserModel user, PaginationData paginationData, SortData sortData);

    Integer getSavedCartsCountForSiteAndUser(UserModel user);
}
