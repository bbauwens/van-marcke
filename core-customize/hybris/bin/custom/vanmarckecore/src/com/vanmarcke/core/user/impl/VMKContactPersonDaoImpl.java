package com.vanmarcke.core.user.impl;

import com.vanmarcke.core.model.VanmarckeContactModel;
import com.vanmarcke.core.user.VMKContactPersonDao;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.servicelayer.internal.dao.AbstractItemDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;

/**
 * Implementation for {@link VMKContactPersonDao}
 *
 * @author Niels Raemaekers, Tom van den Berg
 * @since 19-06-2020
 */
public class VMKContactPersonDaoImpl extends AbstractItemDao implements VMKContactPersonDao {

    private static final String QUERY_FIND_CONTACT_FOR_COUNTRY_AND_POSTALCODE = "SELECT {vc.pk} from {VanmarckeContact AS vc JOIN VanmarckeContactLocation AS vcl ON {vc.pk} = {vcl.contact}} WHERE {vcl.country} =?country AND {vcl.postalcode} =?postalcode";

    /**
     * {@inheritDoc}
     */
    @Override
    public VanmarckeContactModel findContactPersonForCountryAndPostalcode(CountryModel country, String postalCode) {
        final FlexibleSearchQuery fQuery = new FlexibleSearchQuery(QUERY_FIND_CONTACT_FOR_COUNTRY_AND_POSTALCODE);
        fQuery.addQueryParameter("country", country);
        fQuery.addQueryParameter("postalcode", postalCode);

        try {
            return getFlexibleSearchService().searchUnique(fQuery);
        } catch (Exception ex) {
            return null;
        }
    }
}
