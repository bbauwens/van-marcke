package com.vanmarcke.core.product;

import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.servicelayer.data.SearchPageData;
import de.hybris.platform.servicelayer.internal.dao.Dao;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.Date;

/**
 * DAO providing paged {@link VariantProductModel} functionality.
 */
public interface VMKPagedVariantProductDao extends Dao {

    /**
     * Finds all {@link VariantProductModel} for a given {@link CatalogVersionModel} and that are modified since {@link Date}.
     * If no {@link Date} is specified, then all {@link VariantProductModel} that belong to the given {@link CatalogVersionModel} are returned.
     *
     * @param catalogVersion the product catalog version
     * @param date           the date
     * @param searchPageData the search page data
     * @return A paged result of {@link VariantProductModel}
     */
    SearchPageData<VariantProductModel> findProductsByCatalogVersionAndDate(CatalogVersionModel catalogVersion, Date date, SearchPageData searchPageData);

    /**
     * Finds all {@link VariantProductModel} for a given {@link CatalogVersionModel}
     *
     * @param catalogVersion the product catalog version
     * @param approvalStatus the product approvalStatus ex. ARCHIVED
     * @param searchPageData the search page data
     * @return A paged result of {link VariantProductModel}
     */
    SearchPageData<VariantProductModel> findProductsByCatalogVersionAndApprovalStatus(CatalogVersionModel catalogVersion, ArticleApprovalStatus approvalStatus, SearchPageData searchPageData);

    /**
     * Finds all {@link VariantProductModel} for a given {@link CatalogVersionModel}
     *
     * @param catalogVersion the product catalog version
     * @param approvalStatus the product approvalStatus ex. ARCHIVED
     * @param modifiedTime   the product modifiedTime
     * @param searchPageData the search page data
     * @return A paged result of {link VariantProductModel}
     */
    SearchPageData<VariantProductModel> findProductsByCatalogVersionApprovalStatusAndDate(CatalogVersionModel catalogVersion, ArticleApprovalStatus approvalStatus, Date modifiedTime, SearchPageData searchPageData);

    /**
     * Finds all {@link VariantProductModel} for the given {@link CatalogVersionModel} that are not archived
     *
     * @param catalogVersion the product catalog version
     * @param searchPageData the search page data
     * @return a paged result of all matching {@link VariantProductModel}
     */
    SearchPageData<VariantProductModel> findNonArchivedProductsByCatalogVersion(CatalogVersionModel catalogVersion, SearchPageData searchPageData);
}