package com.vanmarcke.core.product.impl;

import com.google.common.base.Preconditions;
import com.vanmarcke.core.product.VMKPagedVariantProductDao;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.servicelayer.data.SearchPageData;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.paginated.PaginatedFlexibleSearchParameter;
import de.hybris.platform.servicelayer.search.paginated.dao.impl.DefaultPaginatedGenericDao;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

/**
 * Default implementation of the {@link VMKPagedVariantProductDao}
 *
 * @author Jeroen Vandevelde, korovpa, Niels Raemaekers
 * @since 16-08-2018
 */
public class VMKPagedVariantProductDaoImpl extends DefaultPaginatedGenericDao<VariantProductModel> implements VMKPagedVariantProductDao {

    private static final String QUERY_VARIANTS_BY_STATUS_AND_CATALOGVERSION = "SELECT {pk} FROM {VariantProduct} WHERE {approvalStatus} !=?approvalStatus AND {catalogVersion} =?catalogVersion";

    public VMKPagedVariantProductDaoImpl() {
        super(VariantProductModel._TYPECODE);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SearchPageData<VariantProductModel> findProductsByCatalogVersionAndDate(final CatalogVersionModel catalogVersion, final Date date, final SearchPageData searchPageData) {
        validateParameterNotNull(catalogVersion, "catalog version cannot be null");

        final Map<String, Object> queryParams = new HashMap<>();
        queryParams.put(VariantProductModel.CATALOGVERSION, catalogVersion);

        final StringBuilder queryString = new StringBuilder();
        queryString.append("SELECT {v:").append(VariantProductModel.PK).append("}");
        queryString.append(" FROM {").append(VariantProductModel._TYPECODE).append(" AS v");

        if (date != null) {
            queryString.append(" JOIN ").append(ProductModel._TYPECODE).append(" AS p ON {p:").append(ProductModel.PK).append("} = {v:").append(VariantProductModel.BASEPRODUCT).append("}}");
        } else {
            queryString.append("}");
        }

        queryString.append(" WHERE {v:").append(VariantProductModel.CATALOGVERSION).append("} = ?").append(VariantProductModel.CATALOGVERSION);

        if (date != null) {
            queryParams.put(VariantProductModel.MODIFIEDTIME, date);
            queryString.append(" AND ({v:").append(VariantProductModel.MODIFIEDTIME).append("} >= ?").append(VariantProductModel.MODIFIEDTIME);
            queryString.append(" OR {p:").append(ProductModel.MODIFIEDTIME).append("} >= ?").append(VariantProductModel.MODIFIEDTIME).append(")");
        }

        final FlexibleSearchQuery query = new FlexibleSearchQuery(queryString.toString(), queryParams);

        final PaginatedFlexibleSearchParameter parameter = new PaginatedFlexibleSearchParameter();
        parameter.setFlexibleSearchQuery(query);
        parameter.setSearchPageData(searchPageData);
        return getPaginatedFlexibleSearchService().search(parameter);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SearchPageData<VariantProductModel> findProductsByCatalogVersionAndApprovalStatus(final CatalogVersionModel catalogVersion, final ArticleApprovalStatus approvalStatus, final SearchPageData searchPageData) {
        validateParameterNotNull(catalogVersion, "catalog version cannot be null");
        validateParameterNotNull(catalogVersion, "article approval status cannot be null");

        final Map<String, Object> queryParams = new HashMap<>();
        queryParams.put(VariantProductModel.CATALOGVERSION, catalogVersion);
        queryParams.put(VariantProductModel.APPROVALSTATUS, approvalStatus);

        return find(queryParams, searchPageData);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SearchPageData<VariantProductModel> findProductsByCatalogVersionApprovalStatusAndDate(final CatalogVersionModel catalogVersion, final ArticleApprovalStatus approvalStatus, final Date modifiedTime, final SearchPageData searchPageData) {
        validateParameterNotNull(catalogVersion, "catalog version cannot be null");
        validateParameterNotNull(catalogVersion, "article approval status cannot be null");

        final Map<String, Object> queryParams = new HashMap<>();
        queryParams.put(VariantProductModel.CATALOGVERSION, catalogVersion);
        queryParams.put(VariantProductModel.APPROVALSTATUS, approvalStatus);

        final StringBuilder queryString = new StringBuilder();
        queryString.append("SELECT {v:").append(VariantProductModel.PK).append("}");
        queryString.append(" FROM {").append(VariantProductModel._TYPECODE).append(" AS v}");

        queryString.append(" WHERE {v:").append(VariantProductModel.CATALOGVERSION).append("} = ?").append(VariantProductModel.CATALOGVERSION);
        queryString.append(" AND {v:").append(VariantProductModel.APPROVALSTATUS).append("} = ?").append(VariantProductModel.APPROVALSTATUS);

        if (modifiedTime != null) {
            queryParams.put(VariantProductModel.MODIFIEDTIME, modifiedTime);
            queryString.append(" AND {v:").append(VariantProductModel.MODIFIEDTIME).append("} > ?").append(VariantProductModel.MODIFIEDTIME);
        }

        queryString.append(" ORDER BY {v:").append(VariantProductModel.MODIFIEDTIME).append("} DESC");

        final FlexibleSearchQuery query = new FlexibleSearchQuery(queryString.toString(), queryParams);

        final PaginatedFlexibleSearchParameter parameter = new PaginatedFlexibleSearchParameter();
        parameter.setFlexibleSearchQuery(query);
        parameter.setSearchPageData(searchPageData);
        return getPaginatedFlexibleSearchService().search(parameter);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SearchPageData<VariantProductModel> findNonArchivedProductsByCatalogVersion(CatalogVersionModel catalogVersion, SearchPageData searchPageData) {
        Preconditions.checkNotNull(catalogVersion, "Catalog Version cannot be null!");

        FlexibleSearchQuery fQuery = new FlexibleSearchQuery(QUERY_VARIANTS_BY_STATUS_AND_CATALOGVERSION);
        fQuery.addQueryParameter("catalogVersion", catalogVersion);
        fQuery.addQueryParameter("approvalStatus", ArticleApprovalStatus.ARCHIVED);

        final PaginatedFlexibleSearchParameter parameter = new PaginatedFlexibleSearchParameter();
        parameter.setFlexibleSearchQuery(fQuery);
        parameter.setSearchPageData(searchPageData);

        return getPaginatedFlexibleSearchService().search(parameter);
    }
}