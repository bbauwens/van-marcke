package com.vanmarcke.core.user;

import com.vanmarcke.core.model.VanmarckeContactModel;
import de.hybris.platform.core.model.c2l.CountryModel;

/**
 * Defines methods for retrieving contact persons
 *
 * @author Niels Raemaekers, Tom van den Berg
 * @since 19-06-2020
 */
public interface VMKContactPersonDao {

    /**
     * Find the contact information based on country and postalCode
     *
     * @param country    the given country
     * @param postalCode the given postal code
     * @return the result or null
     */
    VanmarckeContactModel findContactPersonForCountryAndPostalcode(CountryModel country, String postalCode);
}
