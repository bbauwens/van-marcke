package com.vanmarcke.core.util;

import com.google.common.collect.Iterables;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;

import java.util.Collection;

import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;

public final class PaymentTransactionUtils {

    private PaymentTransactionUtils() {
    }

    /**
     * Gets the current {@link PaymentTransactionModel} from the given list of {@link PaymentTransactionModel}
     *
     * @param paymentTransactions the paymentTransactions
     * @return the current {@link PaymentTransactionModel} or <code>null</code> if none found
     */
    public static PaymentTransactionModel getCurrentPaymentTransaction(Collection<PaymentTransactionModel> paymentTransactions) {
        if (isNotEmpty(paymentTransactions)) {
            return Iterables.getLast(paymentTransactions);
        }
        return null;
    }

    /**
     * Gets the current {@link PaymentTransactionEntryModel} from the given list of {@link PaymentTransactionModel}
     *
     * @param paymentTransactions the paymentTransactions
     * @return the current {@link PaymentTransactionEntryModel} or <code>null</code> if none found
     */
    public static PaymentTransactionEntryModel getCurrentPaymentTransactionEntry(final Collection<PaymentTransactionModel> paymentTransactions) {
        final PaymentTransactionModel paymentTransactionModel = getCurrentPaymentTransaction(paymentTransactions);
        if (paymentTransactionModel != null && isNotEmpty(paymentTransactionModel.getEntries())) {
            return Iterables.getLast(paymentTransactionModel.getEntries());
        }
        return null;
    }
}