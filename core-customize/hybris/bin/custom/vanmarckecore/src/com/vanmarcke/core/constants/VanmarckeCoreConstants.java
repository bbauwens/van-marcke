package com.vanmarcke.core.constants;

/**
 * Global class for all VanmarckeCore constants. You can add global constants for your extension into this class.
 */
public final class VanmarckeCoreConstants extends GeneratedVanmarckeCoreConstants {

    public static final String EXTENSIONNAME = "vanmarckecore";

    public static final String ETIM_CLASSIFICATION = "EtimClassification";
    public static final String GS1_CLASSIFICATION = "GS1Classification";

    public static final String REQUEST_SEPARATE_INVOICE = "REQUEST_SEPARATE_INVOICE";
    public static final int SAVED_CART_DESCRIPTION_FIELD_LIMIT = 1020;

    public interface Export {

        String EXPORT_LOCAL_ROOT_PATH = "productexport.local.root.folder";
        String EXPORT_NON_ARCHIVED_FILE_NAME = "exportNonArchivedProducts";
        String EXPORT_DATE_TIME_SUFFIX_FORMAT = "yyyyMMdd";
        String EXPORT_EXTENSION = ".csv";
        String EXPORT_HYPHEN = "-";

        String FULL_ETIM_PRODUCT_EXPORT_FILE_NAME = "fullEtimProductExport";
    }

    public interface Languages {
        String ISO_CODE_NL = "nl";
        String ISO_CODE_FR = "fr";
    }

    public interface DeliveryMethods {
        String SHIPPING = "DELIVERY";
        String PICKUP = "PICKUP";
    }

    private VanmarckeCoreConstants() {
        //empty
    }
}
