package com.vanmarcke.core.product;

import de.hybris.platform.product.daos.ProductDao;

/**
 * Custom extension of defaut product dao
 */
public interface VMKProductDao extends ProductDao {

    /**
     * returns the base product code for the child product code provided as parameter
     * @param code the product for which we want to retrieve the base product
     * @return base product code
     */
    String getBaseProductCode(final String code);
}
