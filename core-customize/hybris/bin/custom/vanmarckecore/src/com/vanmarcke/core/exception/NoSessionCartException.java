package com.vanmarcke.core.exception;

/**
 * Thrown when a session cart is required but not found.
 *
 * @author Tom van den Berg
 * @since 31-12-2020
 */
public class NoSessionCartException extends RuntimeException {

    /**
     * {@inheritDoc}
     */
    public NoSessionCartException(String message) {
        super(message);
    }
}
