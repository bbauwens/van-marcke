package com.vanmarcke.core.references.impl;

import com.vanmarcke.core.references.VMKProductReferencesDao;
import de.hybris.platform.catalog.model.ProductReferenceModel;
import de.hybris.platform.catalog.references.daos.impl.DefaultProductReferencesDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.List;

/**
 * Implementation for {@link VMKProductReferencesDao}
 *
 * @author Niels Raemaekers
 * @since 12-05-2020
 */
public class VMKProductReferencesDaoImpl extends DefaultProductReferencesDao implements VMKProductReferencesDao {

    private static final String QUERY_ALL_INACTIVE = "SELECT {pk} FROM {ProductReference} WHERE {active} = 0";

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ProductReferenceModel> findAllInactiveProductReferences() {
        final FlexibleSearchQuery fQuery = new FlexibleSearchQuery(QUERY_ALL_INACTIVE);
        SearchResult<ProductReferenceModel> result = getFlexibleSearchService().search(fQuery);
        return result.getResult();
    }
}
