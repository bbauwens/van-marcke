package com.vanmarcke.core.cms.impl;

import com.vanmarcke.core.cms.VMKCMSSiteDao;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.daos.impl.DefaultCMSSiteDao;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.List;

import static java.util.Collections.singletonMap;

/**
 * Custom implementation of DefaultCMSSiteDao
 */
public class VMKCMSSiteDaoImpl extends DefaultCMSSiteDao implements VMKCMSSiteDao {

    private static final String ALL_ACTIVE_SITES = "SELECT {" + CMSSiteModel.PK + "} FROM {" + CMSSiteModel._TYPECODE + "}"
            + " WHERE {" + CMSSiteModel.CHANNEL + "} = ?" + CMSSiteModel.CHANNEL;

    @Override
    public List<CMSSiteModel> findAllSitesForChannel(final SiteChannel channel) {
        final SearchResult<CMSSiteModel> searchResult = getFlexibleSearchService().search(ALL_ACTIVE_SITES, singletonMap(CMSSiteModel.CHANNEL, channel));
        return searchResult.getResult();
    }
}