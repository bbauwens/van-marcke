package com.vanmarcke.core.helper;

import com.vanmarcke.cpi.data.order.ShippingDateInfoResponseData;
import com.vanmarcke.cpi.data.order.ShippingEntryInfoResponseData;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import org.apache.commons.collections4.CollectionUtils;

import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.Objects;
import java.util.Optional;

/**
 * Helper class for the checkout dates
 *
 * @author Niels Raemaekers
 * @since 09-04-2021
 */
public class CheckoutDateHelper {

    /**
     * Determines the first possible date for the given cart/response data.
     *
     * @param response the shipping date info response
     * @return the first possible date
     */
    public Optional<Date> getFirstPossibleShippingDate(ShippingDateInfoResponseData response) {
        return CollectionUtils.emptyIfNull(response.getResults()).stream()
                .map(ShippingEntryInfoResponseData::getDeliveryDate)
                .filter(Objects::nonNull)
                .min(Date::compareTo);
    }

    public Date getCheckoutLastPossibleDateForCart(CartData cart) {
        return cart.getEntries().stream()
                .map(OrderEntryData::getFirstPossibleDate)
                .filter(Objects::nonNull)
                .max(Comparator.naturalOrder())
                .orElse(null);
    }

    /**
     * Determines whether the given shipping or pickup date is on or after today's date.
     *
     * @param date the delivery date
     * @return false if before today's date
     */
    public boolean isValidDate(Date date) {
        if (date == null) {
            return false;
        }
        Calendar c = Calendar.getInstance();

        // set the calendar to start of today
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);

        Date today = c.getTime();

        return !date.before(today);
    }
}
