package com.vanmarcke.core.store.impl;

import com.vanmarcke.core.store.VMKBaseStoreDao;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.daos.impl.DefaultBaseStoreDao;

import java.util.List;
import java.util.Map;

public class VMKBaseStoreDaoImpl extends DefaultBaseStoreDao implements VMKBaseStoreDao {

    private static final String FIND_BASE_STORES_WITH_FACET_SEARCH_CONFIG = "SELECT DISTINCT {bs.pk} " +
            "FROM {" + BaseStoreModel._TYPECODE + " AS bs " +
            "JOIN StoresForCMSSite AS bs2s ON {bs2s.target} = {bs.PK} " +
            "JOIN " + CMSSiteModel._TYPECODE + " AS s ON {bs2s.source} = {s.PK} }" +
            "WHERE {s." + CMSSiteModel.SOLRFACETSEARCHCONFIGURATION + "} IS NOT NULL " +
            "AND {s." + CMSSiteModel.ACTIVE + "} = ?active";

    @Override
    public List<BaseStoreModel> findAllStoresWithSolrFacetSearchConfig() {
        return getFlexibleSearchService().<BaseStoreModel>search(FIND_BASE_STORES_WITH_FACET_SEARCH_CONFIG, Map.of("active", 1))
                .getResult();
    }
}
