package com.vanmarcke.core.user.impl;

import com.vanmarcke.core.model.VanmarckeContactModel;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;
import static org.fest.assertions.MapAssert.entry;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKContactPersonDaoImplTest {

    @Captor
    private ArgumentCaptor<FlexibleSearchQuery> flexibleSearchQueryCaptor;
    @Mock
    private FlexibleSearchService flexibleSearchService;
    @InjectMocks
    private VMKContactPersonDaoImpl contactPersonDao;

    @Test
    public void testFindReturnContactForCountryAndPostalcode() {
        VanmarckeContactModel contact = mock(VanmarckeContactModel.class);
        CountryModel country = mock(CountryModel.class);
        String postalCode = "3500";

        when(this.flexibleSearchService.searchUnique(this.flexibleSearchQueryCaptor.capture())).thenReturn(contact);
        VanmarckeContactModel result = contactPersonDao.findContactPersonForCountryAndPostalcode(country, postalCode);

        assertThat(result).isEqualTo(contact);
        final FlexibleSearchQuery fQuery = this.flexibleSearchQueryCaptor.getValue();
        assertThat(fQuery.getQueryParameters()).includes(entry("country", country));
        assertThat(fQuery.getQueryParameters()).includes(entry("postalcode", postalCode));
        assertThat(fQuery.getQuery()).isEqualTo("SELECT {vc.pk} from {VanmarckeContact AS vc JOIN VanmarckeContactLocation AS vcl ON {vc.pk} = {vcl.contact}} WHERE {vcl.country} =?country AND {vcl.postalcode} =?postalcode");
    }

    @Test
    public void testFindReturnContactForCountryAndPostalcode_modelNotFound() {
        CountryModel country = mock(CountryModel.class);
        String postalCode = "0000";

        when(this.flexibleSearchService.searchUnique(this.flexibleSearchQueryCaptor.capture())).thenThrow(ModelNotFoundException.class);
        VanmarckeContactModel result = contactPersonDao.findContactPersonForCountryAndPostalcode(country, postalCode);

        assertThat(result).isNull();
        final FlexibleSearchQuery fQuery = this.flexibleSearchQueryCaptor.getValue();
        assertThat(fQuery.getQueryParameters()).includes(entry("country", country));
        assertThat(fQuery.getQueryParameters()).includes(entry("postalcode", postalCode));
        assertThat(fQuery.getQuery()).isEqualTo("SELECT {vc.pk} from {VanmarckeContact AS vc JOIN VanmarckeContactLocation AS vcl ON {vc.pk} = {vcl.contact}} WHERE {vcl.country} =?country AND {vcl.postalcode} =?postalcode");
    }

    @Test
    public void testFindReturnContactForCountryAndPostalcode_ambigious() {
        CountryModel country = mock(CountryModel.class);
        String postalCode = "0000";

        when(this.flexibleSearchService.searchUnique(this.flexibleSearchQueryCaptor.capture())).thenThrow(AmbiguousIdentifierException.class);
        VanmarckeContactModel result = contactPersonDao.findContactPersonForCountryAndPostalcode(country, postalCode);

        assertThat(result).isNull();
        final FlexibleSearchQuery fQuery = this.flexibleSearchQueryCaptor.getValue();
        assertThat(fQuery.getQueryParameters()).includes(entry("country", country));
        assertThat(fQuery.getQueryParameters()).includes(entry("postalcode", postalCode));
        assertThat(fQuery.getQuery()).isEqualTo("SELECT {vc.pk} from {VanmarckeContact AS vc JOIN VanmarckeContactLocation AS vcl ON {vc.pk} = {vcl.contact}} WHERE {vcl.country} =?country AND {vcl.postalcode} =?postalcode");
    }
}