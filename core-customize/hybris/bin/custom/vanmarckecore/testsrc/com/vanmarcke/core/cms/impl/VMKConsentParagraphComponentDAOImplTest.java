package com.vanmarcke.core.cms.impl;

import com.vanmarcke.core.model.ConsentParagraphComponentModel;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.CatalogModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.search.impl.SearchResultImpl;
import org.apache.commons.lang3.RandomStringUtils;
import org.fest.assertions.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;

import static org.fest.assertions.MapAssert.entry;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * The {@link VMKConsentParagraphComponentDAOImplTest} class contains the unit tests for the
 * {@link VMKConsentParagraphComponentDAOImpl} class.
 *
 * @author Christiaan Janssen
 * @since 16-07-2020
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKConsentParagraphComponentDAOImplTest {

    private static final String CATALOG_ID = RandomStringUtils.random(10);
    private static final String CATALOG_VERSION = RandomStringUtils.random(10);

    @Mock
    private FlexibleSearchService flexibleSearchService;

    @InjectMocks
    private VMKConsentParagraphComponentDAOImpl consentParagraphComponentDAO;

    @Captor
    private ArgumentCaptor<FlexibleSearchQuery> captor;

    @Test
    public void testFindAll() {
        ConsentParagraphComponentModel paragraphComponent = mock(ConsentParagraphComponentModel.class);

        SearchResult<ConsentParagraphComponentModel> searchResult = new SearchResultImpl<>(Collections.singletonList(paragraphComponent), 1, 1, 0);

        when(flexibleSearchService.<ConsentParagraphComponentModel>search(any(FlexibleSearchQuery.class))).thenReturn(searchResult);

        CatalogModel c = mock(CatalogModel.class);
        when(c.getId()).thenReturn(CATALOG_ID);

        CatalogVersionModel cv = mock(CatalogVersionModel.class);
        when(cv.getVersion()).thenReturn(CATALOG_VERSION);
        when(cv.getCatalog()).thenReturn(c);

        consentParagraphComponentDAO.findAll(cv);

        verify(flexibleSearchService).search(captor.capture());

        FlexibleSearchQuery actualQuery = captor.getValue();

        Assertions
                .assertThat(actualQuery.getQuery())
                .isEqualTo("SELECT {cp.pk} FROM {ConsentParagraphComponent AS cp JOIN CatalogVersion AS cv ON {cp.catalogVersion} = {cv.pk} JOIN Catalog AS c ON {cv.pk} = {c.activeCatalogVersion}} WHERE {c.id} = ?catalogId AND {cv.version} = ?catalogVersion");

        Assertions
                .assertThat(actualQuery.getQueryParameters())
                .includes(
                        entry("catalogId", CATALOG_ID),
                        entry("catalogVersion", CATALOG_VERSION)
                );
    }
}