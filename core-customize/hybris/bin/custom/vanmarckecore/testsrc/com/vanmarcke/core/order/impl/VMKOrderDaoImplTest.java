package com.vanmarcke.core.order.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.time.TimeService;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.*;

import static org.apache.commons.lang3.time.DateUtils.addHours;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKOrderDaoImplTest {

    private static final String CART_GUID = RandomStringUtils.randomAlphabetic(10);
    private static final String FAILED_ORDERS_QUERY = "SELECT {pk} FROM {Order} WHERE {externalCode} IS NULL AND {creationtime} < ?date";

    @Mock
    private TimeService timeService;

    @Mock
    private FlexibleSearchService flexibleSearchService;

    private VMKOrderDaoImpl orderDao;

    @Captor
    private ArgumentCaptor<FlexibleSearchQuery> flexibleSearchQueryArgumentCaptor;

    @Before
    public void setup() {
        orderDao = new VMKOrderDaoImpl(timeService);
        orderDao.setFlexibleSearchService(flexibleSearchService);
    }

    @Test
    public void testFindFailedOrders() {
        SearchResult searchResult = mock(SearchResult.class);
        List<OrderModel> orderModels = mock(List.class);
        Date date = new GregorianCalendar(2020, Calendar.MAY, 20, 10, 0, 0).getTime();

        when(timeService.getCurrentTime()).thenReturn(date);
        when(flexibleSearchService.search(flexibleSearchQueryArgumentCaptor.capture())).thenReturn(searchResult);
        when(searchResult.getResult()).thenReturn(orderModels);

        List<OrderModel> result = orderDao.findFailedOrders();

        verify(timeService).getCurrentTime();
        assertThat(result).isEqualTo(orderModels);
        final FlexibleSearchQuery flexibleSearchQuery = flexibleSearchQueryArgumentCaptor.getValue();
        assertThat(flexibleSearchQuery.getQueryParameters().get("date")).isEqualTo(addHours(date, -1));
        assertThat(flexibleSearchQuery.getQuery()).isEqualTo(FAILED_ORDERS_QUERY);
    }

    @Test
    public void testOrderByGuidAndSite() {
        SearchResult searchResult = mock(SearchResult.class);
        OrderModel order = mock(OrderModel.class);
        BaseSiteModel baseSite = mock(BaseSiteModel.class);

        List<OrderModel> orderModels = Collections.singletonList(order);

        when(flexibleSearchService.search(flexibleSearchQueryArgumentCaptor.capture())).thenReturn(searchResult);
        when(searchResult.getResult()).thenReturn(orderModels);
        when(searchResult.getCount()).thenReturn(1);

        OrderModel result = orderDao.findOrderByGuidAndSite(CART_GUID, baseSite);

        assertThat(result).isEqualTo(order);

        FlexibleSearchQuery flexibleSearchQuery = flexibleSearchQueryArgumentCaptor.getValue();
        assertThat(flexibleSearchQuery.getQueryParameters().get("guid")).isEqualTo(CART_GUID);
        assertThat(flexibleSearchQuery.getQueryParameters().get("baseSite")).isEqualTo(baseSite);
        assertThat(flexibleSearchQuery.getQuery()).isEqualTo("SELECT {pk} FROM {Order} WHERE {guid} = ?guid AND {site} = ?baseSite");
    }

    @Test
    public void testOrderByGuidAndSite_noResults() {
        SearchResult searchResult = mock(SearchResult.class);
        BaseSiteModel baseSite = mock(BaseSiteModel.class);

        List<OrderModel> orderModels = Collections.emptyList();

        when(flexibleSearchService.search(flexibleSearchQueryArgumentCaptor.capture())).thenReturn(searchResult);
        when(searchResult.getResult()).thenReturn(orderModels);
        when(searchResult.getCount()).thenReturn(0);

        OrderModel result = orderDao.findOrderByGuidAndSite(CART_GUID, baseSite);

        assertThat(result).isNull();

        FlexibleSearchQuery flexibleSearchQuery = flexibleSearchQueryArgumentCaptor.getValue();
        assertThat(flexibleSearchQuery.getQueryParameters().get("guid")).isEqualTo(CART_GUID);
        assertThat(flexibleSearchQuery.getQueryParameters().get("baseSite")).isEqualTo(baseSite);
        assertThat(flexibleSearchQuery.getQuery()).isEqualTo("SELECT {pk} FROM {Order} WHERE {guid} = ?guid AND {site} = ?baseSite");
    }
}
