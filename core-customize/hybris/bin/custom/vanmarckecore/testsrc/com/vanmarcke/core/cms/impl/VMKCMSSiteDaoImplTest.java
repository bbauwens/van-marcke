package com.vanmarcke.core.cms.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;

import static de.hybris.platform.commerceservices.enums.SiteChannel.B2B;
import static org.fest.assertions.Assertions.assertThat;
import static org.fest.assertions.MapAssert.entry;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKCMSSiteDaoImplTest {

    @Mock
    private FlexibleSearchService flexibleSearchService;

    @InjectMocks
    private VMKCMSSiteDaoImpl defaultBlueCMSSiteDao;

    @Captor
    private ArgumentCaptor<String> flexibleSearchQueryCaptor;

    @Captor
    private ArgumentCaptor<Map<String, Object>> parametersCaptor;

    @Test
    public void testFindAllSitesForType() throws Exception {
        CMSSiteModel cmsSiteModel = mock(CMSSiteModel.class);

        SearchResult searchResult = mock(SearchResult.class);
        when(flexibleSearchService.search(flexibleSearchQueryCaptor.capture(), parametersCaptor.capture())).thenReturn(searchResult);
        when(searchResult.getResult()).thenReturn(Collections.singletonList(cmsSiteModel));

        Collection<CMSSiteModel> result = defaultBlueCMSSiteDao.findAllSitesForChannel(B2B);

        assertThat(result).hasSize(1).contains(cmsSiteModel);

        String flexibleSearchQuery = flexibleSearchQueryCaptor.getValue();
        assertThat(flexibleSearchQuery).isEqualTo("SELECT {pk} FROM {CMSSite} WHERE {channel} = ?channel");

        Map<String, Object> parameters = parametersCaptor.getValue();
        assertThat(parameters).includes(entry("channel", B2B));
    }
}