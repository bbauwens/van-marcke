package com.vanmarcke.core.product.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.servicelayer.data.SearchPageData;
import de.hybris.platform.servicelayer.search.paginated.PaginatedFlexibleSearchParameter;
import de.hybris.platform.servicelayer.search.paginated.PaginatedFlexibleSearchService;
import de.hybris.platform.variants.model.VariantProductModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Date;

import static org.fest.assertions.Assertions.assertThat;
import static org.fest.assertions.MapAssert.entry;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@UnitTest
public class VMKPagedVariantProductDaoImplTest {

    @Mock
    private PaginatedFlexibleSearchService paginatedFlexibleSearchService;

    @InjectMocks
    private VMKPagedVariantProductDaoImpl defaultPagedVariantProductDao;

    @Captor
    private ArgumentCaptor<PaginatedFlexibleSearchParameter> paginatedFlexibleSearchParameterArgumentCaptor;

    @Test(expected = IllegalArgumentException.class)
    public void testFindProductsByCatalogVersionAndDate_withoutCatalogVersion() throws Exception {
        final SearchPageData searchPageData = mock(SearchPageData.class);
        defaultPagedVariantProductDao.findProductsByCatalogVersionAndDate(null, null, searchPageData);
    }

    @Test
    public void testFindProductsByCatalogVersionAndDate_withoutDate() throws Exception {
        final CatalogVersionModel catalogVersion = mock(CatalogVersionModel.class);
        final SearchPageData searchPageData = mock(SearchPageData.class);

        final SearchPageData searchPageDataResult = mock(SearchPageData.class);
        when(paginatedFlexibleSearchService.search(paginatedFlexibleSearchParameterArgumentCaptor.capture())).thenReturn(searchPageDataResult);

        final SearchPageData<VariantProductModel> result = defaultPagedVariantProductDao.findProductsByCatalogVersionAndDate(catalogVersion, null, searchPageData);
        assertThat(result).isEqualTo(searchPageDataResult);

        final PaginatedFlexibleSearchParameter paginatedFlexibleSearchParameter = paginatedFlexibleSearchParameterArgumentCaptor.getValue();
        assertThat(paginatedFlexibleSearchParameter.getFlexibleSearchQuery()).isNotNull();
        assertThat(paginatedFlexibleSearchParameter.getFlexibleSearchQuery().getQuery()).isEqualTo("SELECT {v:pk} FROM {VariantProduct AS v} WHERE {v:catalogVersion} = ?catalogVersion");
        assertThat(paginatedFlexibleSearchParameter.getFlexibleSearchQuery().getQueryParameters()).hasSize(1).includes(entry("catalogVersion", catalogVersion));
    }

    @Test
    public void testFindProductsByCatalogVersionAndDate_withDate() throws Exception {
        final CatalogVersionModel catalogVersion = mock(CatalogVersionModel.class);
        final SearchPageData searchPageData = mock(SearchPageData.class);
        final Date date = new Date();

        final SearchPageData searchPageDataResult = mock(SearchPageData.class);
        when(paginatedFlexibleSearchService.search(paginatedFlexibleSearchParameterArgumentCaptor.capture())).thenReturn(searchPageDataResult);

        final SearchPageData<VariantProductModel> result = defaultPagedVariantProductDao.findProductsByCatalogVersionAndDate(catalogVersion, date, searchPageData);
        assertThat(result).isEqualTo(searchPageDataResult);

        final PaginatedFlexibleSearchParameter paginatedFlexibleSearchParameter = paginatedFlexibleSearchParameterArgumentCaptor.getValue();
        assertThat(paginatedFlexibleSearchParameter.getFlexibleSearchQuery()).isNotNull();
        assertThat(paginatedFlexibleSearchParameter.getFlexibleSearchQuery().getQuery()).isEqualTo("SELECT {v:pk} FROM {VariantProduct AS v JOIN Product AS p ON {p:pk} = {v:baseProduct}} WHERE {v:catalogVersion} = ?catalogVersion AND ({v:modifiedtime} >= ?modifiedtime OR {p:modifiedtime} >= ?modifiedtime)");
        assertThat(paginatedFlexibleSearchParameter.getFlexibleSearchQuery().getQueryParameters()).hasSize(2).includes(entry("catalogVersion", catalogVersion), entry("modifiedtime", date));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFindProductsByCatalogVersionAndApprovalStatus_withoutCatalogVersion() throws Exception {
        final ArticleApprovalStatus articleApprovalStatus = ArticleApprovalStatus.APPROVED;
        final SearchPageData searchPageData = mock(SearchPageData.class);

        defaultPagedVariantProductDao.findProductsByCatalogVersionAndApprovalStatus(null, articleApprovalStatus, searchPageData);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFindProductsByCatalogVersionAndApprovalStatus_withoutApprovalStatus() throws Exception {
        final CatalogVersionModel catalogVersion = mock(CatalogVersionModel.class);
        final SearchPageData searchPageData = mock(SearchPageData.class);

        defaultPagedVariantProductDao.findProductsByCatalogVersionAndApprovalStatus(catalogVersion, null, searchPageData);
    }

    @Test
    public void testFindProductsByCatalogVersionAndApprovalStatus() throws Exception {
        final CatalogVersionModel catalogVersion = mock(CatalogVersionModel.class);
        final ArticleApprovalStatus articleApprovalStatus = ArticleApprovalStatus.APPROVED;
        final SearchPageData searchPageData = mock(SearchPageData.class);

        final SearchPageData searchPageDataResult = mock(SearchPageData.class);
        when(paginatedFlexibleSearchService.search(paginatedFlexibleSearchParameterArgumentCaptor.capture())).thenReturn(searchPageDataResult);

        final SearchPageData<VariantProductModel> result = defaultPagedVariantProductDao.findProductsByCatalogVersionAndApprovalStatus(catalogVersion, articleApprovalStatus, searchPageData);
        assertThat(result).isEqualTo(searchPageDataResult);

        final PaginatedFlexibleSearchParameter paginatedFlexibleSearchParameter = paginatedFlexibleSearchParameterArgumentCaptor.getValue();
        assertThat(paginatedFlexibleSearchParameter.getFlexibleSearchQuery()).isNotNull();
        assertThat(paginatedFlexibleSearchParameter.getFlexibleSearchQuery().getQuery()).isEqualTo("SELECT {c:pk} FROM {VariantProduct AS c} WHERE {c:catalogVersion} = ?catalogVersion AND {c:approvalStatus} = ?approvalStatus ");
        assertThat(paginatedFlexibleSearchParameter.getFlexibleSearchQuery().getQueryParameters()).hasSize(2).includes(entry("catalogVersion", catalogVersion), entry("approvalStatus", articleApprovalStatus));
    }

    @Test
    public void testFindProductsByCatalogVersionApprovalStatusAndDate() throws Exception {
        final CatalogVersionModel catalogVersion = mock(CatalogVersionModel.class);
        final ArticleApprovalStatus articleApprovalStatus = ArticleApprovalStatus.APPROVED;
        final Date modifiedTime = mock(Date.class);
        final SearchPageData searchPageData = mock(SearchPageData.class);

        final SearchPageData searchPageDataResult = mock(SearchPageData.class);
        when(paginatedFlexibleSearchService.search(paginatedFlexibleSearchParameterArgumentCaptor.capture())).thenReturn(searchPageDataResult);

        final SearchPageData<VariantProductModel> result = defaultPagedVariantProductDao.findProductsByCatalogVersionApprovalStatusAndDate(catalogVersion, articleApprovalStatus, modifiedTime, searchPageData);
        assertThat(result).isEqualTo(searchPageDataResult);

        final PaginatedFlexibleSearchParameter paginatedFlexibleSearchParameter = paginatedFlexibleSearchParameterArgumentCaptor.getValue();
        assertThat(paginatedFlexibleSearchParameter.getFlexibleSearchQuery()).isNotNull();
        assertThat(paginatedFlexibleSearchParameter.getFlexibleSearchQuery().getQuery()).isEqualTo("SELECT {v:pk} FROM {VariantProduct AS v} WHERE {v:catalogVersion} = ?catalogVersion AND {v:approvalStatus} = ?approvalStatus AND {v:modifiedtime} > ?modifiedtime ORDER BY {v:modifiedtime} DESC");
        assertThat(paginatedFlexibleSearchParameter.getFlexibleSearchQuery().getQueryParameters()).hasSize(3).includes(entry("catalogVersion", catalogVersion), entry("approvalStatus", articleApprovalStatus));
    }

    @Test
    public void testFindNonArchivedProductsByCatalogVersion() {
        CatalogVersionModel catalogVersion = mock(CatalogVersionModel.class);
        SearchPageData searchPageData = mock(SearchPageData.class);
        SearchPageData searchPageDataResult = mock(SearchPageData.class);

        when(paginatedFlexibleSearchService.search(paginatedFlexibleSearchParameterArgumentCaptor.capture())).thenReturn(searchPageDataResult);
        SearchPageData<VariantProductModel> result = defaultPagedVariantProductDao.findNonArchivedProductsByCatalogVersion(catalogVersion, searchPageData);

        assertThat(result).isEqualTo(searchPageDataResult);
        final PaginatedFlexibleSearchParameter paginatedFlexibleSearchParameter = paginatedFlexibleSearchParameterArgumentCaptor.getValue();
        assertThat(paginatedFlexibleSearchParameter.getFlexibleSearchQuery()).isNotNull();
        assertThat(paginatedFlexibleSearchParameter.getFlexibleSearchQuery().getQuery()).isEqualTo("SELECT {pk} FROM {VariantProduct} WHERE {approvalStatus} !=?approvalStatus AND {catalogVersion} =?catalogVersion");
        assertThat(paginatedFlexibleSearchParameter.getFlexibleSearchQuery().getQueryParameters()).hasSize(2).includes(entry("catalogVersion", catalogVersion), entry("approvalStatus", ArticleApprovalStatus.ARCHIVED));
    }
}