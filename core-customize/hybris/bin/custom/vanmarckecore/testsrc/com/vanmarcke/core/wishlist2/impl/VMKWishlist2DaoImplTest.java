package com.vanmarcke.core.wishlist2.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.search.impl.SearchResultImpl;
import de.hybris.platform.wishlist2.model.Wishlist2EntryModel;
import de.hybris.platform.wishlist2.model.Wishlist2Model;
import org.apache.commons.lang3.RandomStringUtils;
import org.fest.assertions.Assertions;
import org.fest.assertions.MapAssert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKWishlist2DaoImplTest {

    private static final String NAME = RandomStringUtils.random(10);
    private static final String PK = RandomStringUtils.random(10);

    @Mock
    private FlexibleSearchService flexibleSearchService;

    @InjectMocks
    private VMKWishlist2DaoImpl wishlist2Dao;

    @Captor
    private ArgumentCaptor<FlexibleSearchQuery> captor;

    @Test
    public void testFindWishListsByUserAndName() {
        final UserModel user = mock(UserModel.class);
        final Wishlist2Model wishlist = mock(Wishlist2Model.class);

        final SearchResult<Wishlist2Model> searchResult = new SearchResultImpl<>(Collections.singletonList(wishlist), 1, 1, 1);

        when(flexibleSearchService.<Wishlist2Model>search(any(FlexibleSearchQuery.class))).thenReturn(searchResult);

        final Wishlist2Model result = wishlist2Dao.getWishlistByUserAndName(user, NAME);

        Assertions
                .assertThat(result)
                .isEqualTo(wishlist);

        verify(flexibleSearchService).search(captor.capture());

        final FlexibleSearchQuery query = captor.getValue();

        Assertions
                .assertThat(query)
                .isNotNull();

        Assertions
                .assertThat(query.getQuery())
                .isEqualTo("SELECT {pk} FROM {Wishlist2} WHERE {user} = ?user AND {name} = ?name");

        Assertions
                .assertThat(query.getQueryParameters())
                .hasSize(2)
                .includes(
                        MapAssert.entry("user", user),
                        MapAssert.entry("name", NAME)
                );
    }

    @Test
    public void testFindWishListEntriesByPK() {
        final UserModel user = mock(UserModel.class);
        final Wishlist2EntryModel entry = mock(Wishlist2EntryModel.class);

        final SearchResult<Wishlist2EntryModel> searchResult = new SearchResultImpl<>(Collections.singletonList(entry), 1, 1, 1);

        when(flexibleSearchService.<Wishlist2EntryModel>search(any(FlexibleSearchQuery.class))).thenReturn(searchResult);

        final List<Wishlist2EntryModel> result = wishlist2Dao.findWishListEntriesByUserAndPK(user, PK);

        Assertions
                .assertThat(result)
                .containsExactly(entry);

        verify(flexibleSearchService).search(captor.capture());

        final FlexibleSearchQuery query = captor.getValue();

        Assertions
                .assertThat(query)
                .isNotNull();

        Assertions
                .assertThat(query.getQuery())
                .isEqualTo("SELECT {wle.pk} FROM {Wishlist2Entry AS wle JOIN Wishlist2 AS wl ON {wle.wishlist} = {wl.pk}} WHERE {wle.pk} = ?pk AND {wl.user} = ?user");

        Assertions
                .assertThat(query.getQueryParameters())
                .hasSize(2)
                .includes(
                        MapAssert.entry("user", user),
                        MapAssert.entry("pk", PK)
                );
    }
}
