package com.vanmarcke.core.product.impl;

import com.vanmarcke.core.model.VanMarckeVariantProductModel;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import junit.framework.TestCase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@UnitTest
public class VMKProductDaoImplTest extends TestCase {

    @Mock
    FlexibleSearchService flexibleSearchService;

    @InjectMocks
    VMKProductDaoImpl vmkProductDao;

    @Captor
    private ArgumentCaptor<FlexibleSearchQuery> flexibleSearchQueryArgumentCaptor;

    @Test
    public void testGetBaseProductCode() {
        String productCode = "test_code";
        String baseProductCode = "BP_test_code";

        SearchResult searchResult = mock(SearchResult.class);
        when(searchResult.getResult()).thenReturn(List.of(baseProductCode));

        when(flexibleSearchService.search(flexibleSearchQueryArgumentCaptor.capture())).thenReturn(searchResult);
        String result = vmkProductDao.getBaseProductCode(productCode);

        String expectedQuery = "SELECT {bp:code} FROM {Product AS bp JOIN VanMarckeVariantProduct AS p ON {p:baseProduct}={bp:pk} } WHERE {p:code} = ?code";
        assertEquals(expectedQuery, flexibleSearchQueryArgumentCaptor.getValue().getQuery());
        assertEquals(productCode, flexibleSearchQueryArgumentCaptor.getValue().getQueryParameters().get(VanMarckeVariantProductModel.CODE));
        assertEquals(baseProductCode, result);
    }
}