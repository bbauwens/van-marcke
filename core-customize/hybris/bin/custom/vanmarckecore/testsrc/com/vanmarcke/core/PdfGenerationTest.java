package com.vanmarcke.core;

import com.lowagie.text.*;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import org.junit.Test;

import java.io.FileOutputStream;
import java.io.IOException;

public class PdfGenerationTest {

    @Test
    public void test() {
        System.out.println("My first table");
        // step 1: creation of a document-object
        Document document = new Document();
        try {

            Image bmp1 = Image.getInstance("/home/vagrant/Pictures/create-funny-cartoon-characters.png");

            // step 2:
            // we create a writer that listens to the document
            // and directs a PDF-stream to a file
            PdfWriter.getInstance(document,
                    new FileOutputStream("/home/vagrant/MyFirstTable.pdf"));
            // step 3: we open the document
            document.open();
            // step 4: we create a table and add it to the document
            PdfPTable table = new PdfPTable(2);
            table.addCell("0.0");
            table.addCell("0.1");
            table.addCell("1.0");
            table.addCell("1.1");
            table.addCell(new Phrase("\nTITLE TEXT", new Font(Font.HELVETICA, 16, Font.BOLD | Font.UNDERLINE)));

            PdfPCell cell = new PdfPCell();
            cell.addElement(bmp1);

            table.addCell(cell);
            table.addCell("test");


            document.add(table);
            document.add(new Paragraph("converted to PdfPTable:"));
            //table.setConvert2pdfptable(true);
            document.add(table);
        } catch (DocumentException de) {
            System.err.println(de.getMessage());
        } catch (IOException ioe) {
            System.err.println(ioe.getMessage());
        }
        // step 5: we close the document
        document.close();
    }

    public static PdfPCell createImageCell(String path) throws DocumentException, IOException {
        Image img = Image.getInstance(path);
        PdfPCell cell = new PdfPCell(img, true);
        return cell;
    }

    public static PdfPCell createTextCell(String text) throws DocumentException, IOException {
        PdfPCell cell = new PdfPCell();
        Paragraph p = new Paragraph(text);
        p.setAlignment(Element.ALIGN_RIGHT);
        cell.addElement(p);
        cell.setVerticalAlignment(Element.ALIGN_BOTTOM);
        cell.setBorder(Rectangle.NO_BORDER);
        return cell;
    }
}
