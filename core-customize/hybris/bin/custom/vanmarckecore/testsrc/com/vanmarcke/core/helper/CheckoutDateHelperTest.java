package com.vanmarcke.core.helper;

import com.vanmarcke.cpi.data.order.ShippingDateInfoResponseData;
import com.vanmarcke.cpi.data.order.ShippingEntryInfoResponseData;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import org.assertj.core.util.Lists;
import org.junit.Test;

import java.time.LocalDate;
import java.util.Collections;
import java.util.Date;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * The {@link CheckoutDateHelperTest} class contains the unit tests for the {@link CheckoutDateHelper}
 * class.
 *
 * @author Niels Raemaekers
 * @since 09-04-2021
 */
@UnitTest
public class CheckoutDateHelperTest {

    private final CheckoutDateHelper checkoutDateHelper = new CheckoutDateHelper();

    @Test
    public void testGetFirstPossibleShippingDate_withValue() {
        // given
        final LocalDate date = LocalDate.now();

        final ShippingEntryInfoResponseData firstEntry = new ShippingEntryInfoResponseData();
        firstEntry.setDeliveryDate(java.sql.Date.valueOf(date.plusDays(3)));

        final ShippingEntryInfoResponseData secondEntry = new ShippingEntryInfoResponseData();
        secondEntry.setDeliveryDate(java.sql.Date.valueOf(date));

        final ShippingDateInfoResponseData response = new ShippingDateInfoResponseData();
        response.setResults(Lists.newArrayList(firstEntry, secondEntry));

        // when
        final Optional<Date> actualDate = checkoutDateHelper.getFirstPossibleShippingDate(response);

        // then
        assertThat(actualDate).hasValue(java.sql.Date.valueOf(date));
    }

    @Test
    public void testGetFirstPossibleShippingDate_withoutValue() {
        // given
        final ShippingEntryInfoResponseData firstEntry = new ShippingEntryInfoResponseData();

        final ShippingDateInfoResponseData response = new ShippingDateInfoResponseData();
        response.setResults(Collections.singletonList(firstEntry));

        // when
        final Optional<Date> actualDate = checkoutDateHelper.getFirstPossibleShippingDate(response);

        // then
        assertThat(actualDate).isEmpty();
    }

    @Test
    public void testGetCheckoutLastPossibleDateForCart_withValue() {
        // given
        final LocalDate date = LocalDate.now();

        final OrderEntryData firstEntry = new OrderEntryData();
        firstEntry.setFirstPossibleDate(java.sql.Date.valueOf(date.plusDays(3)));

        final OrderEntryData secondEntry = new OrderEntryData();
        secondEntry.setFirstPossibleDate(java.sql.Date.valueOf(date));

        final CartData cart = new CartData();
        cart.setEntries(Lists.newArrayList(firstEntry, secondEntry));

        // when
        final Date actualDate = checkoutDateHelper.getCheckoutLastPossibleDateForCart(cart);

        // then
        assertThat(actualDate).isEqualTo(java.sql.Date.valueOf(date.plusDays(3)));
    }

    @Test
    public void testGetCheckoutLastPossibleDateForCart_withoutValue() {
        // given
        final OrderEntryData firstEntry = new OrderEntryData();

        final CartData cart = new CartData();
        cart.setEntries(Collections.singletonList(firstEntry));

        // when
        final Date actualDate = checkoutDateHelper.getCheckoutLastPossibleDateForCart(cart);

        // then
        assertThat(actualDate).isNull();
    }
}
