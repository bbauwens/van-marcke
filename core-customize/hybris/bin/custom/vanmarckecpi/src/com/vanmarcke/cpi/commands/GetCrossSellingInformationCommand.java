package com.vanmarcke.cpi.commands;

import com.vanmarcke.cpi.data.product.CrossSellingRequestData;
import com.vanmarcke.cpi.data.product.CrossSellingResponseData;
import eu.elision.integration.command.configuration.AuthenticationType;
import eu.elision.integration.command.configuration.annotation.CommandConfig;
import org.springframework.http.HttpMethod;

/**
 * Command which gets the CrossSellingInformation from the backend (via the ESB)
 */
@CommandConfig(serviceName = "crossSellingInformationService", authenticationType = AuthenticationType.BASIC, httpMethod = HttpMethod.POST, url = "/togetheritems")
public class GetCrossSellingInformationCommand extends AbstractVMKCPICommand<CrossSellingResponseData> {

    /**
     * Constructor which sets the cross selling request object to the payload.
     */
    public GetCrossSellingInformationCommand(final CrossSellingRequestData crossSellingRequest) {
        setPayLoad(crossSellingRequest);
    }
}
