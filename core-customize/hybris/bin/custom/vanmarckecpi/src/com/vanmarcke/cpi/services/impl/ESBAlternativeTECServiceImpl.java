package com.vanmarcke.cpi.services.impl;

import com.vanmarcke.cpi.commands.GetAlternativeTECInformationCommand;
import com.vanmarcke.cpi.data.order.AlternativeTECRequestData;
import com.vanmarcke.cpi.data.order.AlternativeTECResponseData;
import com.vanmarcke.cpi.services.ESBAlternativeTECService;
import eu.elision.integration.command.Response;
import eu.elision.integration.command.executer.CommandExecutor;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;

import java.text.MessageFormat;

/**
 * The {@code ESBAlternativeTECServiceImpl} class implements the business logic to retrieve alternative TEC stores
 * from the ESB.
 *
 * @author Niels Raemaekers, Christiaan Janssen
 * @since 11-12-2019
 */
public class ESBAlternativeTECServiceImpl implements ESBAlternativeTECService {

    private static final Logger LOG = Logger.getLogger(ESBAlternativeTECServiceImpl.class);

    private final CommandExecutor commandExecutor;

    /**
     * Creates a new instance of the {@link ESBAlternativeTECServiceImpl} class.
     *
     * @param commandExecutor the commandExecutor
     */
    public ESBAlternativeTECServiceImpl(CommandExecutor commandExecutor) {
        this.commandExecutor = commandExecutor;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AlternativeTECResponseData getAlternativeTECInformation(AlternativeTECRequestData request) {
        AlternativeTECResponseData payload = null;
        try {
            Response<AlternativeTECResponseData> response = commandExecutor.executeCommand(new GetAlternativeTECInformationCommand(request), null);
            if (HttpStatus.OK.equals(response.getHttpStatus()) && response.getPayLoad() != null) {
                payload = response.getPayLoad();
            } else {
                LOG.error(MessageFormat.format("Something went wrong while retrieving alternative tec stores. Response Status: {0}.", response.getHttpStatus()));
            }
        } catch (Exception e) {
            LOG.error(MessageFormat.format("Something went wrong while retrieving alternative tec stores: {0}.", e.getMessage()), e);
        }
        return payload;
    }
}
