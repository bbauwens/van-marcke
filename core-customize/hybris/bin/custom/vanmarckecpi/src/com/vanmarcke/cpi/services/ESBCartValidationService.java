package com.vanmarcke.cpi.services;

import com.vanmarcke.cpi.data.order.CartValidationResponseData;
import com.vanmarcke.cpi.data.order.OrderRequestData;

/**
 * The {@code ESBCartValidationService} interface defines the methods to validate carts at the ESB.
 *
 * @author Niels Raemaekers, Christiaan Janssen
 * @since 03-02-2020
 */
public interface ESBCartValidationService {

    /**
     * Validates the cart at the ESB.
     *
     * @param request order Request Object
     * @return a list of invalid order entries
     */
    CartValidationResponseData getCartValidationInformation(OrderRequestData request);
}
