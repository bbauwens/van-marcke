package com.vanmarcke.cpi.services;

import com.vanmarcke.cpi.data.order.OrderRequestData;
import com.vanmarcke.cpi.data.order.PlaceOrderResponseData;

/**
 * The {@code ESBPlaceOrderService} interface defines the methods to place an order at the ESB.
 *
 * @author Niels Raemaekers, Christiaan Janssen
 * @since 20-02-2020
 */
public interface ESBPlaceOrderService {

    /**
     * Places the order at the ESB.
     *
     * @param request the request object
     * @return a list of invalid order entries
     */
    PlaceOrderResponseData getPlaceOrderInformation(OrderRequestData request);
}
