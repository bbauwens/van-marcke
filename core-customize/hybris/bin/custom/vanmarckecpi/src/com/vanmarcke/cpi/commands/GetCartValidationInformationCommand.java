package com.vanmarcke.cpi.commands;

import com.vanmarcke.cpi.data.order.CartValidationResponseData;
import com.vanmarcke.cpi.data.order.OrderRequestData;
import eu.elision.integration.command.configuration.AuthenticationType;
import eu.elision.integration.command.configuration.annotation.CommandConfig;
import org.springframework.http.HttpMethod;

/**
 * Command which gets the CartValidationInformation from the backend (via the ESB)
 */
@CommandConfig(serviceName = "cartValidationInformation", authenticationType = AuthenticationType.BASIC, httpMethod = HttpMethod.POST, url = "/cartvalidation")
public class GetCartValidationInformationCommand extends AbstractVMKCPICommand<CartValidationResponseData> {

    /**
     * Constructor which sets the order request object to the payload.
     *
     * @param orderRequest the orderRequest
     */
    public GetCartValidationInformationCommand(final OrderRequestData orderRequest) {
        setPayLoad(orderRequest);
    }
}
