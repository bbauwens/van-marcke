package com.vanmarcke.cpi.services.impl;

import com.vanmarcke.cpi.commands.PostFindAddressCommand;
import com.vanmarcke.cpi.data.ptv.PTVAddressData;
import com.vanmarcke.cpi.data.ptv.PTVOptionData;
import com.vanmarcke.cpi.data.ptv.PTVRequestData;
import com.vanmarcke.cpi.data.ptv.PTVResponseData;
import com.vanmarcke.cpi.services.PTVXServerService;
import eu.elision.integration.command.Response;
import eu.elision.integration.command.executer.CommandExecutor;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;

import java.text.MessageFormat;
import java.util.Arrays;

/**
 * The {@code PTVXServerServiceImpl} class implements the business logic to validate addresses against PTV-X.
 *
 * @author David Vangeneugden, Christiaan Janssen
 * @since 06-06-2019
 */
public class PTVXServerServiceImpl implements PTVXServerService {

    private static final Logger LOG = Logger.getLogger(PTVXServerServiceImpl.class);

    private final CommandExecutor commandExecutor;

    /**
     * Creates a new instance of the {@link PTVXServerServiceImpl} class.
     *
     * @param commandExecutor the command executor
     */
    public PTVXServerServiceImpl(CommandExecutor commandExecutor) {
        this.commandExecutor = commandExecutor;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PTVResponseData findAddress(PTVAddressData address) {
        PTVResponseData payload = null;
        try {
            Response<PTVResponseData> response = commandExecutor.executeCommand(new PostFindAddressCommand(prepareRequest(address)), null);
            if (HttpStatus.OK.equals(response.getHttpStatus()) && response.getPayLoad() != null) {
                payload = response.getPayLoad();
            } else {
                LOG.error(MessageFormat.format("Something went wrong while validating the address. Response Status: {0}.", response.getHttpStatus()));
            }
        } catch (Exception e) {
            LOG.error(MessageFormat.format("Something went wrong while validating the address: {0}.", e.getMessage()), e);
        }
        return payload;
    }

    /**
     * Prepares the request for the given {@code address}.
     *
     * @param address the address
     * @return the request
     */
    private PTVRequestData prepareRequest(PTVAddressData address) {
        PTVRequestData ptvRequest = new PTVRequestData();
        ptvRequest.setAddr(address);
        //TODO: This probably needs to be adapted to be more flexible...
        PTVOptionData optionDataResultLanguage = createOptionData("SearchOption", "RESULT_LANGUAGE", "DUT");
        PTVOptionData optionDataCountryCodeType = createOptionData("SearchOption", "COUNTRY_CODETYPE", "1");
        ptvRequest.setOptions(Arrays.asList(optionDataResultLanguage, optionDataCountryCodeType));
        return ptvRequest;
    }

    /**
     * Creates an option for the given {@code key} and {@code value}.
     *
     * @param type  the type of option
     * @param key   the key
     * @param value the value
     * @return the option
     */
    private PTVOptionData createOptionData(String type, String key, String value) {
        PTVOptionData option = new PTVOptionData();
        option.setType(type);
        option.setParam(key);
        option.setValue(value);
        return option;
    }
}
