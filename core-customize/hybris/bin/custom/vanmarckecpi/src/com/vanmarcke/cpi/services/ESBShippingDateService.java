package com.vanmarcke.cpi.services;

import com.vanmarcke.cpi.data.order.ShippingDateRequestData;
import com.vanmarcke.cpi.data.order.ShippingDateResponseData;

/**
 * The {@code ESBShippingDateService} interface defines the methods to retrieve the shipping dates from the ESB.
 *
 * @author Taki Korovessis, Christiaan Janssen
 * @since 10-12-2019
 */
public interface ESBShippingDateService {

    /**
     * Retrieves the shipping dates.
     *
     * @param request the request
     * @return the shipping dates
     */
    ShippingDateResponseData getShippingDateInformation(ShippingDateRequestData request);
}