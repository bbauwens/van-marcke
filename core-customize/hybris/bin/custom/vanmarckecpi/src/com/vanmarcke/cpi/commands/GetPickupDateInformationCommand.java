package com.vanmarcke.cpi.commands;

import com.vanmarcke.cpi.data.order.PickupDateRequestData;
import com.vanmarcke.cpi.data.order.PickupDateResponseData;
import eu.elision.integration.command.configuration.AuthenticationType;
import eu.elision.integration.command.configuration.annotation.CommandConfig;
import org.springframework.http.HttpMethod;

@CommandConfig(serviceName = "pickupDateInformationService", authenticationType = AuthenticationType.BASIC, httpMethod = HttpMethod.POST, url = "/pickupdate")
public class GetPickupDateInformationCommand extends AbstractVMKCPICommand<PickupDateResponseData> {

    /**
     * Creates a new instance of {@link GetPickupDateInformationCommand} with the specified request.
     *
     * @param request the request
     */
    public GetPickupDateInformationCommand(final PickupDateRequestData request) {
        setPayLoad(request);
    }
}
