package com.vanmarcke.cpi.services;

import com.vanmarcke.cpi.data.ptv.PTVAddressData;
import com.vanmarcke.cpi.data.ptv.PTVResponseData;

/**
 * The {@code PTVXServerService} interface defines the methods to validate addresses against PTV-X.
 *
 * @author David Vangeneugden, Christiaan Janssen
 * @since 06-06-2019
 */
public interface PTVXServerService {

    /**
     * Validates the given {@code address} against PTV-X.
     *
     * @param address the address to validate
     * @return the validated address
     */
    PTVResponseData findAddress(PTVAddressData address);
}
