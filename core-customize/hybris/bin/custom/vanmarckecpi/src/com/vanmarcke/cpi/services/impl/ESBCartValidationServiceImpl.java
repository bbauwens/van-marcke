package com.vanmarcke.cpi.services.impl;

import com.vanmarcke.cpi.commands.GetCartValidationInformationCommand;
import com.vanmarcke.cpi.data.order.CartValidationResponseData;
import com.vanmarcke.cpi.data.order.OrderRequestData;
import com.vanmarcke.cpi.services.ESBCartValidationService;
import eu.elision.integration.command.Response;
import eu.elision.integration.command.executer.CommandExecutor;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;

import java.text.MessageFormat;

/**
 * The {@code ESBCartValidationServiceImpl} class implements the business logic to validate carts at the ESB.
 *
 * @author Niels Raemaekers, Christiaan Janssen
 * @since 03-02-2020
 */
public class ESBCartValidationServiceImpl implements ESBCartValidationService {

    private static final Logger LOG = Logger.getLogger(ESBCartValidationServiceImpl.class);

    private final CommandExecutor commandExecutor;

    /**
     * Creates a new instance of the {@link ESBCartValidationServiceImpl} class.
     *
     * @param commandExecutor the command executor
     */
    public ESBCartValidationServiceImpl(CommandExecutor commandExecutor) {
        this.commandExecutor = commandExecutor;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CartValidationResponseData getCartValidationInformation(OrderRequestData request) {
        CartValidationResponseData payload = null;
        try {
            Response<CartValidationResponseData> response = commandExecutor.executeCommand(new GetCartValidationInformationCommand(request), null);
            if (HttpStatus.OK.equals(response.getHttpStatus()) && response.getPayLoad() != null) {
                payload = response.getPayLoad();
            } else {
                LOG.error(MessageFormat.format("Something went wrong while validating a cart. Response Status: {0}.", response.getHttpStatus()));
            }
        } catch (Exception e) {
            LOG.error(MessageFormat.format("Something went wrong while validating a cart: {0}.", e.getMessage()), e);
        }
        return payload;
    }
}
