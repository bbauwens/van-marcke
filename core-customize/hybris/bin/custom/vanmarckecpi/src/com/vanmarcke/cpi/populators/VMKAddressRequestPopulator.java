package com.vanmarcke.cpi.populators;

import com.vanmarcke.cpi.data.order.AddressRequestData;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.commerceservices.strategies.CustomerNameStrategy;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import org.apache.commons.lang3.StringUtils;

/**
 * The {@link VMKAddressRequestPopulator} class used to populate instances of the {@link AddressRequestData} class with
 * the information of the {@link AddressModel} class.
 *
 * @param <SOURCE> the SAP Commerce address
 * @param <TARGET> the outbound address
 * @author Taki Korovessis, Niels Raemaekers, Tom van den Berg, Christiaan Janssen
 * @since 11/12/2019
 */
public class VMKAddressRequestPopulator<SOURCE extends AddressModel, TARGET extends AddressRequestData> implements Populator<SOURCE, TARGET> {

    private final CustomerNameStrategy customerNameStrategy;

    /**
     * Creates a new instance of {@link VMKAddressRequestPopulator}.
     *
     * @param customerNameStrategy the customerNameStrategy
     */
    public VMKAddressRequestPopulator(CustomerNameStrategy customerNameStrategy) {
        this.customerNameStrategy = customerNameStrategy;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void populate(SOURCE source, TARGET target) {
        String email = getEmailAddress(source);

        target.setName(getName(source));
        target.setStreetName(source.getStreetname());
        target.setStreetNumber(source.getStreetnumber());
        target.setApartment(source.getAppartment() == null ? StringUtils.EMPTY : source.getAppartment());
        target.setPostalCode(source.getPostalcode());
        target.setCity(source.getTown());
        target.setCountry(source.getCountry() == null ? null : source.getCountry().getIsocode());
        target.setEmail(email == null ? StringUtils.EMPTY : email);
        target.setPhone(source.getPhone1() == null ? StringUtils.EMPTY : source.getPhone1());
        target.setMobile(source.getPhone2() == null ? StringUtils.EMPTY : source.getPhone2());
        target.setFax(source.getFax() == null ? StringUtils.EMPTY : source.getFax());
    }

    /**
     * Returns the name for the given {@code address}.
     *
     * @param address the address model
     * @return the name
     */
    protected String getName(SOURCE address) {
        String name = address.getCompany();
        if (StringUtils.isNotBlank(address.getFirstname()) || StringUtils.isNotBlank(address.getLastname())) {
            name = customerNameStrategy.getName(address.getFirstname(), address.getLastname());
        }
        return name;
    }

    /**
     * Returns the email address for the given {@code address}.
     *
     * @param address the address model
     * @return the email address
     */
    protected String getEmailAddress(SOURCE address) {
        String emailAddress = address.getEmail();
        AbstractOrderModel order = getOrder(address);
        if (order != null) {
            B2BCustomerModel user = (B2BCustomerModel) order.getUser();
            if (user != null) {
                emailAddress = user.getUid();
            }
        }
        return emailAddress;
    }

    /**
     * Returns the {@link AbstractOrderModel} for the given source.
     *
     * @param address the address model
     * @return the {@link AbstractOrderModel} instance
     */
    protected AbstractOrderModel getOrder(SOURCE address) {
        AbstractOrderModel order = null;
        ItemModel owner = address.getOwner();
        if (owner instanceof OrderModel) {
            order = (OrderModel) owner;
        } else if (owner instanceof CartModel) {
            order = (CartModel) owner;
        }
        return order;
    }
}