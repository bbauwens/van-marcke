package com.vanmarcke.cpi.populators;

import com.vanmarcke.cpi.data.order.DeliveryEntryRequestData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;

/**
 * The {@link VMKDeliveryEntryRequestPopulator} class is used to populate instances of the
 * {@link DeliveryEntryRequestData} class with the information of the {@link AbstractOrderEntryModel} class.
 *
 * @param <SOURCE> the order entry model
 * @param <TARGET> the order entry request data
 * @author Taki Korovessis, Tom van den Berg, Christiaan Janssen
 * @since 11-12-2019
 */
public class VMKDeliveryEntryRequestPopulator<SOURCE extends AbstractOrderEntryModel, TARGET extends DeliveryEntryRequestData> implements Populator<SOURCE, TARGET> {

    /**
     * {@inheritDoc}
     */
    @Override
    public void populate(SOURCE source, TARGET target) {
        target.setProductID(source.getProduct() == null ? null : source.getProduct().getCode());
        target.setQuantity(source.getQuantity());
    }
}