package com.vanmarcke.cpi.data.builder;

import com.vanmarcke.cpi.data.price.PriceRequestData;

import java.util.List;

/**
 * The {@code PriceRequestDataBuilder} class is an implementation of the builder pattern. It makes the creation of
 * {@link PriceRequestData} instances easier and cleaner.
 *
 * @author Christiaan Janssen
 * @since 29-04-2020
 */
public final class PriceRequestDataBuilder {

    private final String customerID;
    private final List<String> productIDs;

    /**
     * Creates a new instance of the {@link PriceRequestDataBuilder} class.
     *
     * @param customerID the customer ID
     * @param productIDs the product IDs
     */
    private PriceRequestDataBuilder(String customerID, List<String> productIDs) {
        this.customerID = customerID;
        this.productIDs = productIDs;
    }

    /**
     * The entry point to the {@link PriceRequestDataBuilder} class.
     * <p>
     * Start building a {@link PriceRequestData} instance with the given {@code customerID} and {@code productIDs}.
     *
     * @param customerID the customer ID
     * @param productIDs the product IDs
     * @return the {@link PriceRequestDataBuilder} instance for method chaining
     */
    public static PriceRequestDataBuilder aPriceRequest(String customerID, List<String> productIDs) {
        return new PriceRequestDataBuilder(customerID, productIDs);
    }

    /**
     * Creates and returns a new {@link PriceRequestData} instance.
     *
     * @return the {@link PriceRequestData} instance
     */
    public PriceRequestData build() {
        PriceRequestData priceRequestData = new PriceRequestData();
        priceRequestData.setCustomerID(customerID);
        priceRequestData.setProductIDs(productIDs);
        return priceRequestData;
    }
}
