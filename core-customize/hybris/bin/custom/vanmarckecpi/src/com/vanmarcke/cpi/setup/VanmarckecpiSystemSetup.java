/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.vanmarcke.cpi.setup;

import com.vanmarcke.cpi.constants.VanmarckecpiConstants;
import de.hybris.platform.core.initialization.SystemSetup;

@SystemSetup(extension = VanmarckecpiConstants.EXTENSIONNAME)
public class VanmarckecpiSystemSetup {
}