package com.vanmarcke.cpi.populators;

import com.vanmarcke.cpi.data.order.AddressRequestData;
import com.vanmarcke.cpi.data.order.OrderEntryRequestData;
import com.vanmarcke.cpi.data.order.OrderRequestData;
import com.vanmarcke.cpi.data.order.PaymentInfoRequestData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

/**
 * The {@link VMKPlaceOrderRequestPopulator} class contains is used to populate instances of the
 * {@link OrderRequestData} class with the information of instances of the {@link AbstractOrderModel} class.
 *
 * @param <SOURCE> the order model
 * @param <TARGET> the outbound order
 * @author Taki Korovessis, Niels Raemaekers, Tom van den Berg, Christiaan Janssen
 * @since 20-12-2020
 */
public class VMKCreditCheckOrderRequestPopulator<SOURCE extends AbstractOrderModel, TARGET extends OrderRequestData> extends AbstractOrderRequestPopulator<SOURCE, TARGET> {

    /**
     * Constructor for {@link VMKCreditCheckOrderRequestPopulator}
     *
     * @param addressRequestConverter            the address request converter
     * @param orderEntryRequestConverter         the order entry request converter
     * @param paymentInformationRequestConverter the payment information request converter
     */
    public VMKCreditCheckOrderRequestPopulator(Converter<AddressModel, AddressRequestData> addressRequestConverter,
                                               Converter<AbstractOrderEntryModel, OrderEntryRequestData> orderEntryRequestConverter,
                                               Converter<PaymentTransactionEntryModel, PaymentInfoRequestData> paymentInformationRequestConverter) {
        super(addressRequestConverter, orderEntryRequestConverter, paymentInformationRequestConverter);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void populate(final SOURCE source, final TARGET target) {
        addCommon(source, target);
        addEntries(source, target);
        addTotals(source, target);
    }
}