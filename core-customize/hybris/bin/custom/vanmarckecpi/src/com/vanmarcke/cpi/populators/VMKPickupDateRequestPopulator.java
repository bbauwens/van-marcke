package com.vanmarcke.cpi.populators;

import com.vanmarcke.cpi.data.order.DeliveryEntryRequestData;
import com.vanmarcke.cpi.data.order.PickupDateRequestData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

public class VMKPickupDateRequestPopulator<SOURCE extends AbstractOrderModel, TARGET extends PickupDateRequestData> implements Populator<SOURCE, TARGET> {

    private final Converter<AbstractOrderEntryModel, DeliveryEntryRequestData> entryRequestDataConverter;

    private final UserService userService;

    /**
     * Creates a new instance of {@link VMKPickupDateRequestPopulator}
     *
     * @param entryRequestDataConverter the entry request converter
     * @param userService               the userService
     */
    public VMKPickupDateRequestPopulator(final Converter<AbstractOrderEntryModel, DeliveryEntryRequestData> entryRequestDataConverter, final UserService userService) {
        this.entryRequestDataConverter = entryRequestDataConverter;
        this.userService = userService;
    }

    @Override
    public void populate(final SOURCE source, final TARGET target) {
        target.setOrderID(source.getCode());
        target.setCustomerID(source.getUnit().getUid());

        target.setEntries(entryRequestDataConverter.convertAll(source.getEntries()));

        final PointOfServiceModel deliveryPointOfService = getPointOfServiceForOrder(source);
        if (deliveryPointOfService != null) {
            target.setStoreID(deliveryPointOfService.getName());

            final AddressModel deliveryPointOfServiceAddress = deliveryPointOfService.getAddress();
            if (deliveryPointOfServiceAddress != null) {
                target.setStorePostalCode(deliveryPointOfServiceAddress.getPostalcode());
                target.setStoreCity(deliveryPointOfServiceAddress.getTown());
                if (deliveryPointOfServiceAddress.getCountry() != null) {
                    target.setStoreCountryCode(deliveryPointOfServiceAddress.getCountry().getIsocode());
                }
            }
        }
    }

    protected PointOfServiceModel getPointOfServiceForOrder(final SOURCE source) {
        return source.getDeliveryPointOfService() != null ? source.getDeliveryPointOfService() : userService.getCurrentUser().getSessionStore();
    }
}