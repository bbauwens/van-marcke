package com.vanmarcke.cpi.commands;

import com.vanmarcke.cpi.data.order.OrderRequestData;
import com.vanmarcke.cpi.data.order.PlaceOrderResponseData;
import eu.elision.integration.command.configuration.AuthenticationType;
import eu.elision.integration.command.configuration.annotation.CommandConfig;
import org.springframework.http.HttpMethod;

/**
 * Command which gets the PlaceOrderInformation from the backend (via the ESB)
 */
@CommandConfig(serviceName = "placeOrderInformation", authenticationType = AuthenticationType.BASIC, httpMethod = HttpMethod.POST, url = "/placeorder")
public class GetPlaceOrderInformationCommand extends AbstractVMKCPICommand<PlaceOrderResponseData> {

    /**
     * Constructor which sets the order request object to the payload.
     *
     * @param orderRequest the orderRequest
     */
    public GetPlaceOrderInformationCommand(final OrderRequestData orderRequest) {
        setPayLoad(orderRequest);
    }
}
