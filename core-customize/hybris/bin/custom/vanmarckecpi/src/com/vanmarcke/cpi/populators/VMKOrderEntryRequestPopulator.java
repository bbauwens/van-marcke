package com.vanmarcke.cpi.populators;

import com.vanmarcke.cpi.data.order.OrderEntryRequestData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;

import static com.vanmarcke.cpi.utils.NumberUtils.toScaledBigDecimal;

/**
 * The {@link VMKOrderEntryRequestPopulator} class is used to populate instances of the {@link OrderEntryRequestData}
 * class with the information of the instances of the {@link AbstractOrderEntryModel} class.
 *
 * @param <SOURCE> the order entry
 * @param <TARGET> the outbound order entry
 * @author Taki Korovessis, Tom van den Berg, Christiaan Janssen
 * @since 11-12-2019
 */
public class VMKOrderEntryRequestPopulator<SOURCE extends AbstractOrderEntryModel, TARGET extends OrderEntryRequestData> implements Populator<SOURCE, TARGET> {

    /**
     * {@inheritDoc}
     */
    @Override
    public void populate(final SOURCE source, final TARGET target) {
        target.setProductID(source.getProduct() == null ? null : source.getProduct().getCode());
        target.setQuantity(source.getQuantity());
        target.setBasePrice(toScaledBigDecimal(source.getBasePrice()));
        target.setTotalPrice(toScaledBigDecimal(source.getTotalPrice()));
        target.setDeliveryDate(source.getDeliveryDate());
    }
}
