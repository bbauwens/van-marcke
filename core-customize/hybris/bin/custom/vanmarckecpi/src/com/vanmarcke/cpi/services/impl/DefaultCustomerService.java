package com.vanmarcke.cpi.services.impl;

import com.vanmarcke.cpi.commands.GetCustomerInformationCommand;
import com.vanmarcke.cpi.data.customer.CustomerRequestData;
import com.vanmarcke.cpi.data.customer.CustomerResponseData;
import com.vanmarcke.cpi.services.CustomerService;
import eu.elision.integration.command.Response;
import eu.elision.integration.command.executer.CommandExecutor;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;

import java.text.MessageFormat;

/**
 * The {@code DefaultCustomerService} class implemenst the business logic to retrieve customer information from the ERP.
 *
 * @author Taki Korovessis, Christiaan Janssen
 * @since 19-12-2019
 */
public class DefaultCustomerService implements CustomerService {

    private static final Logger LOG = Logger.getLogger(DefaultCustomerService.class);

    private final CommandExecutor commandExecutor;

    /**
     * Creates an new instance of the {@link DefaultCustomerService} class.
     *
     * @param commandExecutor the commandExecutor
     */
    public DefaultCustomerService(CommandExecutor commandExecutor) {
        this.commandExecutor = commandExecutor;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CustomerResponseData getCustomer(CustomerRequestData request) {
        CustomerResponseData payload = null;
        try {
            Response<CustomerResponseData> response = commandExecutor.executeCommand(new GetCustomerInformationCommand(request), null);
            if (HttpStatus.OK.equals(response.getHttpStatus()) && response.getPayLoad() != null) {
                payload = response.getPayLoad();
            } else {
                LOG.error(MessageFormat.format("Something went wrong while retrieving customer information. Response Status: {0}.", response.getHttpStatus()));
            }
        } catch (Exception e) {
            LOG.error(MessageFormat.format("Something went wrong while retrieving customer information: {0}.", e.getMessage()), e);
        }
        return payload;
    }
}