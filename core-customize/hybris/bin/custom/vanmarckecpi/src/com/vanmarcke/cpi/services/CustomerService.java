package com.vanmarcke.cpi.services;

import com.vanmarcke.cpi.data.customer.CustomerRequestData;
import com.vanmarcke.cpi.data.customer.CustomerResponseData;

/**
 * The {@code CustomerService} interface defines the methods to retrieve customer information from the ERP.
 *
 * @author Taki Korovessis, Christiaan Janssen
 * @since 19-12-2019
 */
public interface CustomerService {

    /**
     * Retrieves customer information from the ERP.
     *
     * @param request the request object
     * @return the customer information
     */
    CustomerResponseData getCustomer(CustomerRequestData request);
}
