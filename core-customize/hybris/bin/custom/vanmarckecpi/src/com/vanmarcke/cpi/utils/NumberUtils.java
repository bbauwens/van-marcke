package com.vanmarcke.cpi.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Provides extra functionality for Numbers.
 */
public final class NumberUtils {

    private NumberUtils() {
    }

    /**
     * Convert a <code>Double</code> to a <code>BigDecimal</code> with a scale of
     * two that has been rounded using <code>RoundingMode.HALF_UP</code>. If the supplied
     * <code>value</code> is null, then <code>BigDecimal.ZERO</code> is returned.
     *
     * @param value the <code>BigDecimal</code> to convert
     * @return the scaled, with appropriate rounding, <code>BigDecimal</code>.
     */
    public static BigDecimal toScaledBigDecimal(Double value) {
        if (value == null) {
            return BigDecimal.ZERO.setScale(2, RoundingMode.HALF_UP);
        }
        return BigDecimal.valueOf(value).setScale(2, RoundingMode.HALF_UP);
    }

    /**
     * Convert a <code>BigDecimal</code> to a <code>BigDecimal</code> whose scale is the
     * specified value with a <code>RoundingMode.HALF_UP</code> applied. If the input <code>value</code>
     * is <code>null</code>, we simply return <code>BigDecimal.ZERO</code>.
     *
     * @param value the <code>BigDecimal</code> to convert, may be null.
     * @return the scaled, with appropriate rounding, <code>BigDecimal</code>.
     */
    public static BigDecimal toScaledBigDecimal(final BigDecimal value) {
        if (value == null) {
            return BigDecimal.ZERO.setScale(2, RoundingMode.HALF_UP);
        }
        return value.setScale(2, RoundingMode.HALF_UP);
    }

}