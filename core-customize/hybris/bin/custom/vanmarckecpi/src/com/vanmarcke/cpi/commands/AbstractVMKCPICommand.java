package com.vanmarcke.cpi.commands;

import eu.elision.integration.command.AbstractRESTCommand;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

import static com.google.common.base.Preconditions.checkState;
import static java.util.Collections.singletonList;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;
import static org.springframework.http.HttpHeaders.COOKIE;
import static org.springframework.http.HttpHeaders.SET_COOKIE;
import static org.springframework.http.HttpMethod.OPTIONS;
import static org.springframework.http.MediaType.ALL;

public abstract class AbstractVMKCPICommand<T> extends AbstractRESTCommand<T> {

    private static final String HEADER_X_CSRF_TOKEN = "X-CSRF-Token";

    @Override
    protected void beforeExecute(final RestTemplate restTemplate, final String url) {
        final HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setAccept(singletonList(ALL));
        httpHeaders.add(HEADER_X_CSRF_TOKEN, "Fetch");

        final HttpEntity<Object> request = new HttpEntity<>(httpHeaders);
        final ResponseEntity<Object> response = restTemplate.exchange(url, OPTIONS, request, Object.class);

        final String csrfToken = response.getHeaders().getFirst(HEADER_X_CSRF_TOKEN);
        checkState(isNotEmpty(csrfToken), "The CSRF Token must not be empty");

        addHttpHeader(HEADER_X_CSRF_TOKEN, response.getHeaders().getFirst(HEADER_X_CSRF_TOKEN));

        final List<String> setCookie = new ArrayList<>(response.getHeaders().get(SET_COOKIE));

        checkState(isNotEmpty(setCookie), "The Set-Cookie must not be empty");

        setCookie.forEach(e -> addHttpHeader(COOKIE, e));
    }
}