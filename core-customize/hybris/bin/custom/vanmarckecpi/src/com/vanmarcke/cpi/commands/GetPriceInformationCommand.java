package com.vanmarcke.cpi.commands;

import com.vanmarcke.cpi.data.price.PriceRequestData;
import com.vanmarcke.cpi.data.price.PriceResponseData;
import eu.elision.integration.command.configuration.AuthenticationType;
import eu.elision.integration.command.configuration.annotation.CommandConfig;
import org.springframework.http.HttpMethod;

/**
 * Command which gets the PriceInformation from the backend (via the ESB)
 */
@CommandConfig(serviceName = "priceInformationService", authenticationType = AuthenticationType.BASIC, httpMethod = HttpMethod.POST, url = "/pricing")
public class GetPriceInformationCommand extends AbstractVMKCPICommand<PriceResponseData> {

    /**
     * Constructor which sets the price request object to the payload.
     */
    public GetPriceInformationCommand(PriceRequestData priceRequest) {
        setPayLoad(priceRequest);
    }
}