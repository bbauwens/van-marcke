package com.vanmarcke.cpi.populators;

import com.vanmarcke.cpi.data.order.AddressRequestData;
import com.vanmarcke.cpi.data.order.OrderEntryRequestData;
import com.vanmarcke.cpi.data.order.OrderRequestData;
import com.vanmarcke.cpi.data.order.PaymentInfoRequestData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.apache.commons.lang3.StringUtils;

/**
 * The {@link VMKCartValidationRequestPopulator} class is used as a starting point to populate instances of the
 * {@link OrderRequestData} class with information of the {@link AbstractOrderModel} instances.
 *
 * @param <SOURCE> the SAP Commerce order
 * @param <TARGET> the outbound order
 * @author Taki Korovessis, Niels Raemaekers, Tom van den Berg, Christiaan Janssen
 * @since 20-12-2019
 */
public class VMKCartValidationRequestPopulator<SOURCE extends AbstractOrderModel, TARGET extends OrderRequestData> extends AbstractOrderRequestPopulator<SOURCE, TARGET> {

    /**
     * Constructor for {@link VMKCartValidationRequestPopulator}
     *
     * @param addressRequestConverter            the addressRequestConverter
     * @param orderEntryRequestConverter         the orderEntryRequestConverter
     * @param paymentInformationRequestConverter the payment information request converter
     */
    public VMKCartValidationRequestPopulator(Converter<AddressModel, AddressRequestData> addressRequestConverter,
                                             Converter<AbstractOrderEntryModel, OrderEntryRequestData> orderEntryRequestConverter,
                                             Converter<PaymentTransactionEntryModel, PaymentInfoRequestData> paymentInformationRequestConverter) {
        super(addressRequestConverter, orderEntryRequestConverter, paymentInformationRequestConverter);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void populate(SOURCE source, TARGET target) {
        addCommon(source, target);
        addPurchaseOrderNumber(source, target);
        addDeliveryInformation(source, target);
        addEmptyPayment(target);
        addEntries(source, target);
        addTotals(source, target);
    }

    /**
     * Add an empty payment so we fulfill the required data on the target.
     *
     * @param target the outbound order
     */
    private void addEmptyPayment(TARGET target) {
        PaymentInfoRequestData paymentInfoRequestData = new PaymentInfoRequestData();
        paymentInfoRequestData.setMethod(StringUtils.EMPTY);
        paymentInfoRequestData.setReference(StringUtils.EMPTY);
        paymentInfoRequestData.setStatus(StringUtils.EMPTY);
        target.setPaymentInfo(paymentInfoRequestData);
    }
}