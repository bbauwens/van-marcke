package com.vanmarcke.cpi.commands;

import com.vanmarcke.cpi.data.customer.CustomerRequestData;
import com.vanmarcke.cpi.data.customer.CustomerResponseData;
import eu.elision.integration.command.configuration.AuthenticationType;
import eu.elision.integration.command.configuration.annotation.CommandConfig;
import org.springframework.http.HttpMethod;

@CommandConfig(serviceName = "customerInformationService", authenticationType = AuthenticationType.BASIC, httpMethod = HttpMethod.POST, url = "/customerAddress")
public class GetCustomerInformationCommand extends AbstractVMKCPICommand<CustomerResponseData> {

    /**
     * Creates a new instance of {@link GetCustomerInformationCommand} with the specified request.
     *
     * @param request the request
     */
    public GetCustomerInformationCommand(final CustomerRequestData request) {
        setPayLoad(request);
    }
}