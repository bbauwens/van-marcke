package com.vanmarcke.cpi.services.impl;

import com.vanmarcke.cpi.commands.GetShippingDateInformationCommand;
import com.vanmarcke.cpi.data.order.ShippingDateRequestData;
import com.vanmarcke.cpi.data.order.ShippingDateResponseData;
import com.vanmarcke.cpi.services.ESBShippingDateService;
import eu.elision.integration.command.Response;
import eu.elision.integration.command.executer.CommandExecutor;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;

import java.text.MessageFormat;

/**
 * The {@code DefaultESBShippingDateService} class implements the business logic to retrieve the shipping dates from
 * the ESB.
 *
 * @author Taki Korovessis, Christiaan Janssen
 * @since 10-12-201
 */
public class DefaultESBShippingDateService implements ESBShippingDateService {

    private static final Logger LOG = Logger.getLogger(DefaultESBShippingDateService.class);

    private final CommandExecutor commandExecutor;

    /**
     * Creates an new instance of the {@link DefaultESBShippingDateService} class.
     *
     * @param commandExecutor the command executor
     */
    public DefaultESBShippingDateService(CommandExecutor commandExecutor) {
        this.commandExecutor = commandExecutor;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ShippingDateResponseData getShippingDateInformation(ShippingDateRequestData request) {
        ShippingDateResponseData payload = null;
        try {
            Response<ShippingDateResponseData> response = commandExecutor.executeCommand(new GetShippingDateInformationCommand(request), null);
            if (HttpStatus.OK.equals(response.getHttpStatus()) && response.getPayLoad() != null) {
                payload = response.getPayLoad();
            } else {
                LOG.error(MessageFormat.format("Something went wrong while retrieving delivery dates. Response Status: {0}.", response.getHttpStatus()));
            }
        } catch (Exception e) {
            LOG.error(MessageFormat.format("Something went wrong while retrieving delivery dates: {0}.", e.getMessage()), e);
        }
        return payload;
    }
}