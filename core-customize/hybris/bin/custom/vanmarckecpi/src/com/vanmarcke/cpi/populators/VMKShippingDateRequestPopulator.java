package com.vanmarcke.cpi.populators;

import com.vanmarcke.cpi.data.order.AddressRequestData;
import com.vanmarcke.cpi.data.order.DeliveryEntryRequestData;
import com.vanmarcke.cpi.data.order.ShippingDateRequestData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

public class VMKShippingDateRequestPopulator<SOURCE extends AbstractOrderModel, TARGET extends ShippingDateRequestData> implements Populator<SOURCE, TARGET> {

    private final Converter<AddressModel, AddressRequestData> addressRequestConverter;
    private final Converter<AbstractOrderEntryModel, DeliveryEntryRequestData> entryRequestDataConverter;

    /**
     * Creates a new instance of {@link VMKShippingDateRequestPopulator}
     *
     * @param addressRequestConverter   the addressRequestConverter
     * @param entryRequestDataConverter the entry request converter
     */
    public VMKShippingDateRequestPopulator(Converter<AddressModel, AddressRequestData> addressRequestConverter, final Converter<AbstractOrderEntryModel, DeliveryEntryRequestData> entryRequestDataConverter) {
        this.addressRequestConverter = addressRequestConverter;
        this.entryRequestDataConverter = entryRequestDataConverter;
    }

    @Override
    public void populate(final SOURCE source, final TARGET target) {
        target.setOrderID(source.getCode());
        target.setCustomerID(source.getUnit().getUid());

        target.setEntries(entryRequestDataConverter.convertAll(source.getEntries()));

        final AddressModel deliveryAddress = source.getDeliveryAddress();
        if (deliveryAddress != null) {
            target.setShippingAddress(addressRequestConverter.convert(deliveryAddress));
        }
    }
}