package com.vanmarcke.cpi.services.impl;

import com.vanmarcke.cpi.commands.GetPickupDateInformationCommand;
import com.vanmarcke.cpi.data.order.PickupDateRequestData;
import com.vanmarcke.cpi.data.order.PickupDateResponseData;
import com.vanmarcke.cpi.services.ESBPickupDateService;
import eu.elision.integration.command.Response;
import eu.elision.integration.command.executer.CommandExecutor;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;

import java.text.MessageFormat;

/**
 * The {@code DefaultESBPickupDateService} class implements the business logic to retrieve the pickup dates from the
 * ESB.
 *
 * @author Taki Korovessis, Christiaan Janssen
 * @since 26-11-2019
 */
public class DefaultESBPickupDateService implements ESBPickupDateService {

    private static final Logger LOG = Logger.getLogger(DefaultESBPickupDateService.class);

    private final CommandExecutor commandExecutor;

    /**
     * Creates an new instance of the {@link DefaultESBPickupDateService} class.
     *
     * @param commandExecutor the command executor
     */
    public DefaultESBPickupDateService(CommandExecutor commandExecutor) {
        this.commandExecutor = commandExecutor;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PickupDateResponseData getPickupDateInformation(PickupDateRequestData request) {
        PickupDateResponseData payload = null;
        try {
            Response<PickupDateResponseData> response = commandExecutor.executeCommand(new GetPickupDateInformationCommand(request), null);
            if (HttpStatus.OK.equals(response.getHttpStatus()) && response.getPayLoad() != null) {
                payload = response.getPayLoad();
            } else {
                LOG.error(MessageFormat.format("Something went wrong while retrieving pickup dates. Response Status: {0}.", response.getHttpStatus()));
            }
        } catch (Exception e) {
            LOG.error(MessageFormat.format("Something went wrong while retrieving pickup dates: {0}.", e.getMessage()), e);
        }
        return payload;
    }
}