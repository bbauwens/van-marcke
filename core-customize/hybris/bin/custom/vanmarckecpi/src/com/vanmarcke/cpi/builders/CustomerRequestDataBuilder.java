package com.vanmarcke.cpi.builders;

import com.vanmarcke.cpi.data.customer.CustomerRequestData;
import org.apache.commons.lang3.builder.Builder;

/**
 * The builder for the {@link CustomerRequestDataBuilder} class.
 */
public class CustomerRequestDataBuilder implements Builder<CustomerRequestData> {

    private final CustomerRequestData customerRequestData = new CustomerRequestData();

    /**
     * Hide the default constructor.
     */
    private CustomerRequestDataBuilder() {
    }

    /**
     * Creates a new CustomerRequestDataBuilder.
     *
     * @return a CustomerRequestDataBuilder
     */
    public static CustomerRequestDataBuilder customerRequest() {
        return new CustomerRequestDataBuilder();
    }

    public CustomerRequestDataBuilder withCustomerID(final String customerID) {
        customerRequestData.setCustomerID(customerID);
        return this;
    }

    @Override
    public CustomerRequestData build() {
        return customerRequestData;
    }
}