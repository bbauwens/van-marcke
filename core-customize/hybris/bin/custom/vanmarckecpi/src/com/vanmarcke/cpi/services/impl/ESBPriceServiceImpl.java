package com.vanmarcke.cpi.services.impl;

import com.vanmarcke.cpi.commands.GetPriceInformationCommand;
import com.vanmarcke.cpi.data.price.PriceRequestData;
import com.vanmarcke.cpi.data.price.PriceResponseData;
import com.vanmarcke.cpi.data.price.ProductPriceResponseData;
import com.vanmarcke.cpi.services.ESBPriceService;
import eu.elision.integration.command.Response;
import eu.elision.integration.command.executer.CommandExecutor;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.List;
import java.util.stream.Collectors;

/**
 * The {@link ESBPriceServiceImpl} class implements the business logic to retrieve price information from the ESB.
 *
 * @author Joris Cryns, Christiaan Janssen
 * @since 24-06-2019
 */
public class ESBPriceServiceImpl implements ESBPriceService {

    private static final Logger LOG = Logger.getLogger(ESBPriceServiceImpl.class);

    private final CommandExecutor commandExecutor;

    /**
     * Creates a new instance of the {@link ESBPriceServiceImpl} class.
     *
     * @param commandExecutor the command executor
     */
    public ESBPriceServiceImpl(CommandExecutor commandExecutor) {
        this.commandExecutor = commandExecutor;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PriceResponseData getPriceInformation(PriceRequestData request) {
        PriceResponseData payload = null;
        try {
            Response<PriceResponseData> response = commandExecutor.executeCommand(new GetPriceInformationCommand(request), null);
            if (HttpStatus.OK.equals(response.getHttpStatus()) && response.getPayLoad() != null) {
                payload = response.getPayLoad();
                payload.setPricing(removeInvalidPrices(payload.getPricing()));
            } else {
                LOG.error(MessageFormat.format("Something went wrong while retrieving price information. Response Status: {0}.", response.getHttpStatus()));
            }
        } catch (Exception e) {
            LOG.error(MessageFormat.format("Something went wrong while retrieving price information: {0}.", e.getMessage()), e);
        }
        return payload;
    }

    /**
     * Removes the invalid prices from the given {@code entries}.
     *
     * @param entries the price objects
     * @return the valid price objects
     */
    private List<ProductPriceResponseData> removeInvalidPrices(List<ProductPriceResponseData> entries) {
        return entries
                .stream()
                .filter(this::isValidPrice)
                .collect(Collectors.toList());
    }

    /**
     * Checks whether the given {@code entry} is valid.
     * <p>
     * In order to be valid, a price object must contain a product code and its value must be greater than zero.
     *
     * @param entry the entry to check
     * @return {@code true} in case the given {@code entry} is valid, {@code false} otherwise
     */
    private boolean isValidPrice(ProductPriceResponseData entry) {
        return StringUtils.isNotEmpty(entry.getProductCode())
                && entry.getPrice() != null
                && entry.getPrice().compareTo(BigDecimal.ZERO) > 0;
    }
}