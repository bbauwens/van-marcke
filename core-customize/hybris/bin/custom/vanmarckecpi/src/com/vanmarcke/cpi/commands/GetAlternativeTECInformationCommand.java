package com.vanmarcke.cpi.commands;

import com.vanmarcke.cpi.data.order.AlternativeTECRequestData;
import com.vanmarcke.cpi.data.order.AlternativeTECResponseData;
import eu.elision.integration.command.configuration.AuthenticationType;
import eu.elision.integration.command.configuration.annotation.CommandConfig;
import org.springframework.http.HttpMethod;

/**
 * Command which gets the AlternativeTECInformation from the backend (via the ESB)
 */
@CommandConfig(serviceName = "alternativeTECInformationService", authenticationType = AuthenticationType.BASIC, httpMethod = HttpMethod.POST, url = "/alternativetecs")
public class GetAlternativeTECInformationCommand extends AbstractVMKCPICommand<AlternativeTECResponseData> {

    /**
     * Constructor which sets the alternative tec request object to the payload.
     */
    public GetAlternativeTECInformationCommand(final AlternativeTECRequestData alternativeTECRequest) {
        setPayLoad(alternativeTECRequest);
    }
}
