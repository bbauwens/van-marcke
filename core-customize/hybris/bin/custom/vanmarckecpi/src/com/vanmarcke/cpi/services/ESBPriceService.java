package com.vanmarcke.cpi.services;

import com.vanmarcke.cpi.data.price.PriceRequestData;
import com.vanmarcke.cpi.data.price.PriceResponseData;

/**
 * The {@link ESBPriceService} interface defines the methods to retrieve price information from the ESB.
 *
 * @author Joris Cryns, Christiaan Janssen
 * @since 24-06-2019
 */
public interface ESBPriceService {

    /**
     * Retrieves the price information from the ESB.
     *
     * @param request the request object
     * @return the price information
     */
    PriceResponseData getPriceInformation(PriceRequestData request);
}
