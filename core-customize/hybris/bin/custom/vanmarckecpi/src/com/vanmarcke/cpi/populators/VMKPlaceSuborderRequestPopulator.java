package com.vanmarcke.cpi.populators;

import com.google.common.collect.Iterables;
import com.vanmarcke.core.model.IBMOrderModel;
import com.vanmarcke.cpi.data.order.AddressRequestData;
import com.vanmarcke.cpi.data.order.OrderEntryRequestData;
import com.vanmarcke.cpi.data.order.OrderRequestData;
import com.vanmarcke.cpi.data.order.PaymentInfoRequestData;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.commerceservices.model.PickUpDeliveryModeModel;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.util.TaxValue;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

import static com.vanmarcke.cpi.utils.NumberUtils.toScaledBigDecimal;
import static java.math.BigDecimal.ZERO;
import static org.apache.commons.lang3.BooleanUtils.isTrue;
import static org.apache.commons.lang3.math.NumberUtils.DOUBLE_ZERO;
import static org.apache.commons.lang3.time.DateUtils.isSameDay;
import static org.joda.time.DateTime.now;

/**
 * The {@link VMKPlaceSuborderRequestPopulator} class contains is used to populate instances of the
 * {@link OrderRequestData} class with the information of instances of the {@link IBMOrderModel} class.
 * `
 *
 * @author Christiaan Janssen
 * @since 05-08-2021
 */
public class VMKPlaceSuborderRequestPopulator<TARGET extends OrderRequestData> implements Populator<IBMOrderModel, TARGET> {

    private final Converter<AddressModel, AddressRequestData> addressRequestConverter;
    private final Converter<AbstractOrderEntryModel, OrderEntryRequestData> orderEntryRequestConverter;
    private final Converter<PaymentTransactionEntryModel, PaymentInfoRequestData> paymentInformationRequestConverter;

    /**
     * Creates a new instance of the {@link VMKPlaceSuborderRequestPopulator} class.
     *
     * @param addressRequestConverter            the address request converter
     * @param orderEntryRequestConverter         the order entry request converter
     * @param paymentInformationRequestConverter the payment information request converter
     */
    public VMKPlaceSuborderRequestPopulator(final Converter<AddressModel, AddressRequestData> addressRequestConverter,
                                            final Converter<AbstractOrderEntryModel, OrderEntryRequestData> orderEntryRequestConverter,
                                            final Converter<PaymentTransactionEntryModel, PaymentInfoRequestData> paymentInformationRequestConverter) {
        this.addressRequestConverter = addressRequestConverter;
        this.orderEntryRequestConverter = orderEntryRequestConverter;
        this.paymentInformationRequestConverter = paymentInformationRequestConverter;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void populate(final IBMOrderModel source, final TARGET target) {
        final AbstractOrderModel order = source.getOrder();

        addCommon(order, target);
        addPurchaseOrderNumber(order, target);
        addDeliveryInformation(order, target);
        addPaymentInformation(order, target);
        addEntries(source, target);
        addTotals(source, target);
        target.setIsPartial(true);
    }

    /**
     * Adds the common order information.
     *
     * @param source the SAP Commerce order
     * @param target the outbound order
     */
    protected void addCommon(final AbstractOrderModel source, TARGET target) {
        target.setOrderID(source.getCode());
        target.setCreationDate(source.getCreationtime());
        target.setCurrency(source.getCurrency() == null ? null : source.getCurrency().getIsocode());
        target.setLanguage(source.getLocale());
        target.setCustomerID(source.getUnit() == null ? null : source.getUnit().getUid());

        if (source.getUser() instanceof B2BCustomerModel) {
            B2BCustomerModel user = (B2BCustomerModel) source.getUser();
            target.setUserID(user.getUid());
            target.setCardID(getVirtualCardNumber(user.getCustomerID()));
        }

        // Added this so the call doesn't fail
        target.setPoNumber(StringUtils.EMPTY);
    }

    /**
     * Returns the virtual card number for the given {@code customerID}.
     *
     * @param customerID the customer ID
     * @return the virtual card number
     */
    protected String getVirtualCardNumber(String customerID) {
        if (StringUtils.isEmpty(customerID)) {
            return null;
        }
        int cartID = Integer.parseInt(StringUtils.removeStart(customerID, "P"));
        cartID += 500000;

        return String.valueOf(cartID);
    }

    /**
     * Adds the order entries to the order request.
     *
     * @param source the order model
     * @param target the order request data
     */
    protected void addEntries(final IBMOrderModel source, TARGET target) {
        target.setEntries(orderEntryRequestConverter.convertAll(source.getEntries()));
    }

    /**
     * Adds total prices to the order request.
     *
     * @param source the order model
     * @param target the order request data
     */
    protected void addTotals(final IBMOrderModel source, TARGET target) {
        // TODO: THIS HAS TO BE CLARIFIED
        target.setSubTotal(ZERO);
        target.setDeliveryCost(ZERO);
        target.setTotalTax(ZERO);
        target.setTotalPrice(ZERO);
    }

    /**
     * Adds delivery information to the order request.
     *
     * @param source the order model
     * @param target the order request data
     */
    protected void addDeliveryInformation(final AbstractOrderModel source, TARGET target) {
        target.setStoreID(StringUtils.EMPTY);
        target.setStoreCollect(false);
        target.setDeliveryReference(source.getYardReference() == null ? StringUtils.EMPTY : StringUtils.normalizeSpace(source.getYardReference()));
        target.setDeliveryComment(source.getDeliveryComment() == null ? StringUtils.EMPTY : StringUtils.normalizeSpace(source.getDeliveryComment()));

        if (source.getDeliveryMode() instanceof ZoneDeliveryModeModel && source.getDeliveryAddress() != null) {
            target.setDeliveryAddress(addressRequestConverter.convert(source.getDeliveryAddress()));
        } else if (source.getDeliveryMode() instanceof PickUpDeliveryModeModel) {
            target.setStoreID(source.getDeliveryPointOfService() == null ? StringUtils.EMPTY : source.getDeliveryPointOfService().getName());
            target.setDeliveryAddress(addEmptyAddress());
            target.setStoreCollect(isSameDay(now().toDate(), source.getDeliveryDate()));
        }

        target.setIsPartial(areRequestedDeliveryDatesTheSame(source));
    }

    /**
     * Check to see if the requested delivery dates on the entries are all the same date
     *
     * @param source the source
     * @return the decision
     */
    protected boolean areRequestedDeliveryDatesTheSame(final AbstractOrderModel source) {
        List<Date> requestedDates = source.getEntries()
                .stream()
                .map(AbstractOrderEntryModel::getDeliveryDate)
                .distinct()
                .collect(Collectors.toList());
        return requestedDates.size() <= 1;
    }

    /**
     * Adds purchase order number to the order request.
     *
     * @param source the order model
     * @param target the order request data
     */
    protected void addPurchaseOrderNumber(final AbstractOrderModel source, TARGET target) {
        target.setPurchaseOrderNumber(source.getPurchaseOrderNumber() == null ? StringUtils.EMPTY : source.getPurchaseOrderNumber());
    }

    /**
     * Adds payment information to the order request.
     *
     * @param source the order model
     * @param target the order request data
     */
    protected void addPaymentInformation(final AbstractOrderModel source, TARGET target) {
        PaymentTransactionEntryModel transactionEntry = getCurrentPaymentTransactionEntry(source);
        if (transactionEntry != null) {
            target.setPaymentInfo(paymentInformationRequestConverter.convert(transactionEntry));
        }
    }

    /**
     * Returns the last (thus current) payment transaction entry or the given order.
     * <p>
     * Returns {@code null} if there's no {@link PaymentTransactionEntryModel} found.
     *
     * @param source the order
     * @return the {@link PaymentTransactionEntryModel} instance
     */
    private PaymentTransactionEntryModel getCurrentPaymentTransactionEntry(final AbstractOrderModel source) {
        PaymentTransactionEntryModel paymentTransactionEntry = null;
        if (CollectionUtils.isNotEmpty(source.getPaymentTransactions())) {
            PaymentTransactionModel paymentTransaction = Iterables.getLast(source.getPaymentTransactions());
            if (CollectionUtils.isNotEmpty(paymentTransaction.getEntries())) {
                paymentTransactionEntry = Iterables.getLast(paymentTransaction.getEntries());
            }
        }
        return paymentTransactionEntry;
    }

    /**
     * Creates a {@link AddressRequestData} instance with empty fields.
     *
     * @return the address request
     */
    private AddressRequestData addEmptyAddress() {
        AddressRequestData addressRequest = new AddressRequestData();
        addressRequest.setApartment(StringUtils.EMPTY);
        addressRequest.setCity(StringUtils.EMPTY);
        addressRequest.setCountry(StringUtils.EMPTY);
        addressRequest.setEmail(StringUtils.EMPTY);
        addressRequest.setFax(StringUtils.EMPTY);
        addressRequest.setMobile(StringUtils.EMPTY);
        addressRequest.setPhone(StringUtils.EMPTY);
        addressRequest.setName(StringUtils.EMPTY);
        addressRequest.setPostalCode(StringUtils.EMPTY);
        addressRequest.setStreetName(StringUtils.EMPTY);
        addressRequest.setStreetNumber(StringUtils.EMPTY);
        return addressRequest;
    }
}
