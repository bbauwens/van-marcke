package com.vanmarcke.cpi.services;

import com.vanmarcke.cpi.data.order.PickupDateRequestData;
import com.vanmarcke.cpi.data.order.PickupDateResponseData;

/**
 * The {@code ESBPickupDateService} interface defines the methods to retrieve the pickup dates from the ESB.
 *
 * @author Taki Korovessis, Christiaan Janssen
 * @since 26-11-2019
 */
public interface ESBPickupDateService {

    /**
     * Retrieves the pickup dates.
     *
     * @param request the request
     * @return the pickup dates
     */
    PickupDateResponseData getPickupDateInformation(PickupDateRequestData request);
}