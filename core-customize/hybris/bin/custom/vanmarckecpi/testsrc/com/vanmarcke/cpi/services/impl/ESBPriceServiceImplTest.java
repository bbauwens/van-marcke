package com.vanmarcke.cpi.services.impl;

import com.vanmarcke.cpi.commands.GetPriceInformationCommand;
import com.vanmarcke.cpi.data.price.PriceRequestData;
import com.vanmarcke.cpi.data.price.PriceResponseData;
import com.vanmarcke.cpi.data.price.ProductPriceResponseData;
import de.hybris.bootstrap.annotations.UnitTest;
import eu.elision.integration.command.Response;
import eu.elision.integration.command.executer.CommandExecutor;
import eu.elision.integration.command.impl.ResponseImpl;
import org.apache.commons.lang3.RandomStringUtils;
import org.assertj.core.util.Lists;
import org.fest.assertions.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;

import java.math.BigDecimal;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

/**
 * The {@code ESBPriceServiceImplTest} class contains the unit test for the {@link ESBPriceServiceImpl} class.
 *
 * @author Joris Cryns, Taki Korovessis, Christiaan Janssen
 * @since 24-06-2019
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ESBPriceServiceImplTest {

    @Mock
    private CommandExecutor executor;

    @InjectMocks
    private ESBPriceServiceImpl esbPriceService;

    @Captor
    private ArgumentCaptor<GetPriceInformationCommand> captor;

    @Test
    public void testGetPriceInformationWithInvalidResponseCode() {
        PriceRequestData request = new PriceRequestData();

        PriceResponseData expectedPayload = new PriceResponseData();

        Response<PriceResponseData> response = new ResponseImpl<>();
        response.setHttpStatus(HttpStatus.FORBIDDEN);
        response.setPayLoad(expectedPayload);

        when(executor.executeCommand(any(GetPriceInformationCommand.class), eq(null))).thenReturn(response);

        PriceResponseData actualPayload = esbPriceService.getPriceInformation(request);

        verify(executor).executeCommand(captor.capture(), eq(null));

        Assertions
                .assertThat(actualPayload)
                .isNull();

        GetPriceInformationCommand command = captor.getValue();

        Assertions
                .assertThat(command)
                .isNotNull();

        Assertions
                .assertThat(command.getPayLoad())
                .isEqualTo(request);
    }

    @Test
    public void testGetPriceInformationWithoutPayload() {
        PriceRequestData request = new PriceRequestData();

        Response<PriceResponseData> response = new ResponseImpl<>();
        response.setHttpStatus(HttpStatus.OK);

        when(executor.executeCommand(any(GetPriceInformationCommand.class), eq(null))).thenReturn(response);

        PriceResponseData actualPayload = esbPriceService.getPriceInformation(request);

        verify(executor).executeCommand(captor.capture(), eq(null));

        Assertions
                .assertThat(actualPayload)
                .isNull();

        GetPriceInformationCommand command = captor.getValue();

        Assertions
                .assertThat(command)
                .isNotNull();

        Assertions
                .assertThat(command.getPayLoad())
                .isEqualTo(request);
    }

    @Test
    public void testGetPriceInformationWithExceptionDuringRemoteCall() {
        PriceRequestData request = new PriceRequestData();

        RuntimeException exception = mock(RuntimeException.class);

        when(executor.executeCommand(any(GetPriceInformationCommand.class), eq(null))).thenThrow(exception);

        PriceResponseData actualPayload = esbPriceService.getPriceInformation(request);

        verify(executor).executeCommand(captor.capture(), eq(null));

        Assertions
                .assertThat(actualPayload)
                .isNull();

        GetPriceInformationCommand command = captor.getValue();

        Assertions
                .assertThat(command)
                .isNotNull();

        Assertions
                .assertThat(command.getPayLoad())
                .isEqualTo(request);
    }

    @Test
    public void testGetPriceInformation() {
        PriceRequestData request = new PriceRequestData();

        ProductPriceResponseData priceEntryWithoutProductCode = new ProductPriceResponseData();
        priceEntryWithoutProductCode.setPrice(BigDecimal.ONE);

        ProductPriceResponseData priceEntryWithoutPriceValue = new ProductPriceResponseData();
        priceEntryWithoutPriceValue.setProductCode(RandomStringUtils.random(10));

        ProductPriceResponseData priceEntryWithNegativePriceValue = new ProductPriceResponseData();
        priceEntryWithNegativePriceValue.setProductCode(RandomStringUtils.random(10));
        priceEntryWithNegativePriceValue.setPrice(BigDecimal.valueOf(-1L));

        ProductPriceResponseData priceEntryWithZeroPriceValue = new ProductPriceResponseData();
        priceEntryWithZeroPriceValue.setProductCode(RandomStringUtils.random(10));
        priceEntryWithZeroPriceValue.setPrice(BigDecimal.ZERO);

        ProductPriceResponseData valid = new ProductPriceResponseData();
        valid.setProductCode(RandomStringUtils.random(10));
        valid.setPrice(BigDecimal.ONE);

        PriceResponseData expectedPayload = new PriceResponseData();
        expectedPayload.setPricing(Lists.newArrayList(priceEntryWithoutProductCode, priceEntryWithoutPriceValue, priceEntryWithNegativePriceValue, priceEntryWithZeroPriceValue, valid));

        Response<PriceResponseData> response = new ResponseImpl<>();
        response.setHttpStatus(HttpStatus.OK);
        response.setPayLoad(expectedPayload);

        when(executor.executeCommand(any(GetPriceInformationCommand.class), eq(null))).thenReturn(response);

        PriceResponseData actualPayload = esbPriceService.getPriceInformation(request);

        verify(executor).executeCommand(captor.capture(), eq(null));

        Assertions
                .assertThat(actualPayload)
                .isEqualTo(expectedPayload);

        Assertions
                .assertThat(actualPayload.getPricing())
                .containsExactly(valid);

        GetPriceInformationCommand command = captor.getValue();

        Assertions
                .assertThat(command)
                .isNotNull();

        Assertions
                .assertThat(command.getPayLoad())
                .isEqualTo(request);
    }
}