package com.vanmarcke.cpi.services.impl;

import com.vanmarcke.cpi.commands.GetShippingDateInformationCommand;
import com.vanmarcke.cpi.data.order.ShippingDateRequestData;
import com.vanmarcke.cpi.data.order.ShippingDateResponseData;
import de.hybris.bootstrap.annotations.UnitTest;
import eu.elision.integration.command.Response;
import eu.elision.integration.command.executer.CommandExecutor;
import eu.elision.integration.command.impl.ResponseImpl;
import org.fest.assertions.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

/**
 * The {@code DefaultESBDeliveryDateServiceTest} class contains the unit tests for the
 * {@link DefaultESBShippingDateService} class.
 *
 * @author Taki Korovessis, Christiaan Janssen
 * @since 10-12-2019
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultESBDeliveryDateServiceTest {

    @Mock
    private CommandExecutor executor;

    @InjectMocks
    private DefaultESBShippingDateService defaultESBShippingDateService;

    @Captor
    private ArgumentCaptor<GetShippingDateInformationCommand> captor;

    @Test
    public void testGetDeliveryDateInformationWithInvalidResponseCode() {
        ShippingDateRequestData request = new ShippingDateRequestData();

        ShippingDateResponseData expectedPayload = new ShippingDateResponseData();

        Response<ShippingDateResponseData> response = new ResponseImpl<>();
        response.setHttpStatus(HttpStatus.FORBIDDEN);
        response.setPayLoad(expectedPayload);

        when(executor.executeCommand(any(GetShippingDateInformationCommand.class), eq(null))).thenReturn(response);

        ShippingDateResponseData actualPayload = defaultESBShippingDateService.getShippingDateInformation(request);

        verify(executor).executeCommand(captor.capture(), eq(null));

        Assertions
                .assertThat(actualPayload)
                .isNull();

        GetShippingDateInformationCommand command = captor.getValue();

        Assertions
                .assertThat(command)
                .isNotNull();

        Assertions
                .assertThat(command.getPayLoad())
                .isEqualTo(request);
    }

    @Test
    public void testGetShippingDateInformationWithoutPayload() {
        ShippingDateRequestData request = new ShippingDateRequestData();

        Response<ShippingDateResponseData> response = new ResponseImpl<>();
        response.setHttpStatus(HttpStatus.OK);

        when(executor.executeCommand(any(GetShippingDateInformationCommand.class), eq(null))).thenReturn(response);

        ShippingDateResponseData actualPayload = defaultESBShippingDateService.getShippingDateInformation(request);

        verify(executor).executeCommand(captor.capture(), eq(null));

        Assertions
                .assertThat(actualPayload)
                .isNull();

        GetShippingDateInformationCommand command = captor.getValue();

        Assertions
                .assertThat(command)
                .isNotNull();

        Assertions
                .assertThat(command.getPayLoad())
                .isEqualTo(request);
    }

    @Test
    public void testGetShippingDateInformationWithExceptionDuringRemoteCall() {
        ShippingDateRequestData request = new ShippingDateRequestData();

        RuntimeException exception = mock(RuntimeException.class);

        when(executor.executeCommand(any(GetShippingDateInformationCommand.class), eq(null))).thenThrow(exception);

        ShippingDateResponseData actualPayload = defaultESBShippingDateService.getShippingDateInformation(request);

        verify(executor).executeCommand(captor.capture(), eq(null));

        Assertions
                .assertThat(actualPayload)
                .isNull();

        GetShippingDateInformationCommand command = captor.getValue();

        Assertions
                .assertThat(command)
                .isNotNull();

        Assertions
                .assertThat(command.getPayLoad())
                .isEqualTo(request);
    }

    @Test
    public void testGetShippingDateInformation() {
        ShippingDateRequestData request = new ShippingDateRequestData();

        ShippingDateResponseData expectedPayload = new ShippingDateResponseData();

        Response<ShippingDateResponseData> response = new ResponseImpl<>();
        response.setHttpStatus(HttpStatus.OK);
        response.setPayLoad(expectedPayload);

        when(executor.executeCommand(any(GetShippingDateInformationCommand.class), eq(null))).thenReturn(response);

        ShippingDateResponseData actualPayload = defaultESBShippingDateService.getShippingDateInformation(request);

        verify(executor).executeCommand(captor.capture(), eq(null));

        Assertions
                .assertThat(actualPayload)
                .isEqualTo(expectedPayload);

        GetShippingDateInformationCommand command = captor.getValue();

        Assertions
                .assertThat(command)
                .isNotNull();

        Assertions
                .assertThat(command.getPayLoad())
                .isEqualTo(request);
    }
}