package com.vanmarcke.cpi.commands;

import com.vanmarcke.cpi.data.ptv.PTVRequestData;
import com.vanmarcke.cpi.data.ptv.PTVResponseData;
import de.hybris.bootstrap.annotations.UnitTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpMethod;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class PostFindAddressCommandTest {

    private PostFindAddressCommand command;

    @Test
    public void testConstructor() {
        PTVRequestData requestData = mock(PTVRequestData.class);

        command = new PostFindAddressCommand(requestData);

        assertThat(command.getHttpMethod()).isEqualTo(HttpMethod.POST);
        assertThat(command.getServiceName()).isEqualTo("addressValidationService");
        assertThat(command.getUrl()).isEqualTo("/findAddress");
        assertThat(command.getPayLoad()).isEqualTo(requestData);
        assertThat(command.getResponseType()).isEqualTo(PTVResponseData.class);
    }

}