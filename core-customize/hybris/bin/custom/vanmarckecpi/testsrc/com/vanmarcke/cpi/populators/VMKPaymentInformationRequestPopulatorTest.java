package com.vanmarcke.cpi.populators;

import com.vanmarcke.cpi.data.order.PaymentInfoRequestData;
import com.vanmarcke.saferpay.model.SaferpayPaymentInfoModel;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

/**
 * The {@link VMKPaymentInformationRequestPopulatorTest} class contains the unit tests for the
 * {@link VMKPaymentInformationRequestPopulator} class.
 *
 * @author Christiaan Janssen
 * @since 05-02-2021
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKPaymentInformationRequestPopulatorTest {

    private static final String STATUS = RandomStringUtils.random(10);
    private static final String TYPE = RandomStringUtils.random(10);
    private static final String REFERENCE = RandomStringUtils.random(10);

    @Mock
    private EnumerationService enumerationService;

    @InjectMocks
    private VMKPaymentInformationRequestPopulator paymentInformationPopulator;

    @Test
    public void testPopulate_withoutValues() {
        PaymentTransactionModel paymentTransactionModel = mock(PaymentTransactionModel.class);

        PaymentTransactionEntryModel paymentTransactionEntryModel = mock(PaymentTransactionEntryModel.class);
        when(paymentTransactionEntryModel.getPaymentTransaction()).thenReturn(paymentTransactionModel);

        PaymentInfoRequestData paymentInfoRequestData = new PaymentInfoRequestData();

        paymentInformationPopulator.populate(paymentTransactionEntryModel, paymentInfoRequestData);

        assertThat(paymentInfoRequestData.getStatus()).isNull();
        assertThat(paymentInfoRequestData.getMethod()).isEqualTo("INVOICE");
        assertThat(paymentInfoRequestData.getReference()).isEmpty();

        verifyZeroInteractions(enumerationService);
    }

    @Test
    public void testPopulate_withValues() {
        SaferpayPaymentInfoModel paymentInfoModel = mock(SaferpayPaymentInfoModel.class);
        when(paymentInfoModel.getType()).thenReturn(TYPE);

        PaymentTransactionModel paymentTransactionModel = mock(PaymentTransactionModel.class);
        when(paymentTransactionModel.getInfo()).thenReturn(paymentInfoModel);

        PaymentTransactionEntryModel paymentTransactionEntryModel = mock(PaymentTransactionEntryModel.class);
        when(paymentTransactionEntryModel.getType()).thenReturn(PaymentTransactionType.AUTHORIZATION);
        when(paymentTransactionEntryModel.getPaymentTransaction()).thenReturn(paymentTransactionModel);
        when(paymentTransactionEntryModel.getRequestId()).thenReturn(REFERENCE);

        when(enumerationService.getEnumerationName(PaymentTransactionType.AUTHORIZATION)).thenReturn(STATUS);

        PaymentInfoRequestData paymentInfoRequestData = new PaymentInfoRequestData();

        paymentInformationPopulator.populate(paymentTransactionEntryModel, paymentInfoRequestData);

        assertThat(paymentInfoRequestData.getStatus()).isEqualTo(STATUS);
        assertThat(paymentInfoRequestData.getMethod()).isEqualTo(TYPE);
        assertThat(paymentInfoRequestData.getReference()).isEqualTo(REFERENCE);

        verify(enumerationService).getEnumerationName(PaymentTransactionType.AUTHORIZATION);
    }
}
