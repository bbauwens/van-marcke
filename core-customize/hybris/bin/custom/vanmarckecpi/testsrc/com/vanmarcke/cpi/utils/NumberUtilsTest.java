package com.vanmarcke.cpi.utils;

import de.hybris.bootstrap.annotations.UnitTest;
import org.junit.Test;

import java.math.BigDecimal;

import static org.fest.assertions.Assertions.assertThat;

@UnitTest
public class NumberUtilsTest {

    @Test
    public void testToScaledBigDecimal_withoutValue() {
        BigDecimal result = NumberUtils.toScaledBigDecimal((Double) null);
        assertThat(result.toString()).isEqualTo("0.00");
    }

    @Test
    public void testToScaledBigDecimal_with2Decimals() {
        BigDecimal result = NumberUtils.toScaledBigDecimal(13.95);
        assertThat(result.toString()).isEqualTo("13.95");
    }

    @Test
    public void testToScaledBigDecimal_withRoundDown() {
        BigDecimal result = NumberUtils.toScaledBigDecimal(13.954);
        assertThat(result.toString()).isEqualTo("13.95");
    }

    @Test
    public void testToScaledBigDecimal_withRoundUp() {
        BigDecimal result = NumberUtils.toScaledBigDecimal(13.955);
        assertThat(result.toString()).isEqualTo("13.96");
    }

    @Test
    public void testToScaledBigDecimal_withBigDecimalAnd2Decimals() {
        BigDecimal result = NumberUtils.toScaledBigDecimal(BigDecimal.valueOf(13.95));
        assertThat(result.toString()).isEqualTo("13.95");
    }

    @Test
    public void testToScaledBigDecimal_withBigDecimalAndRoundDown() {
        BigDecimal result = NumberUtils.toScaledBigDecimal(BigDecimal.valueOf(13.954));
        assertThat(result.toString()).isEqualTo("13.95");
    }

    @Test
    public void testToScaledBigDecimal_withBigDecimalAndRoundUp() {
        BigDecimal result = NumberUtils.toScaledBigDecimal(BigDecimal.valueOf(13.955));
        assertThat(result.toString()).isEqualTo("13.96");
    }

    @Test
    public void testToScaledBigDecimal_withBigDecimalAndwithoutValue() {
        BigDecimal result = NumberUtils.toScaledBigDecimal((BigDecimal) null);
        assertThat(result.toString()).isEqualTo("0.00");
    }

}