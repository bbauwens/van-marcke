package com.vanmarcke.cpi.commands;

import com.vanmarcke.cpi.data.customer.CustomerRequestData;
import com.vanmarcke.cpi.data.customer.CustomerResponseData;
import de.hybris.bootstrap.annotations.UnitTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpMethod;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class GetCustomerInformationCommandTest {

    private GetCustomerInformationCommand command;

    @Test
    public void testConstructor() {
        CustomerRequestData requestData = mock(CustomerRequestData.class);

        command = new GetCustomerInformationCommand(requestData);

        assertThat(command.getHttpMethod()).isEqualTo(HttpMethod.POST);
        assertThat(command.getServiceName()).isEqualTo("customerInformationService");
        assertThat(command.getUrl()).isEqualTo("/customerAddress");
        assertThat(command.getPayLoad()).isEqualTo(requestData);
        assertThat(command.getResponseType()).isEqualTo(CustomerResponseData.class);
    }
}