package com.vanmarcke.cpi.services.impl;

import com.vanmarcke.cpi.commands.GetPickupDateInformationCommand;
import com.vanmarcke.cpi.data.order.PickupDateRequestData;
import com.vanmarcke.cpi.data.order.PickupDateResponseData;
import de.hybris.bootstrap.annotations.UnitTest;
import eu.elision.integration.command.Response;
import eu.elision.integration.command.executer.CommandExecutor;
import eu.elision.integration.command.impl.ResponseImpl;
import org.fest.assertions.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

/**
 * The {@code DefaultESBPickupDateServiceTest} class contains the unit tests for the
 * {@link DefaultESBPickupDateService} class.
 *
 * @author Taki Korovessis, Christiaan Janssen
 * @since 26-11-2019
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultESBPickupDateServiceTest {

    @Mock
    private CommandExecutor executor;

    @InjectMocks
    private DefaultESBPickupDateService defaultESBPickupDateService;

    @Captor
    private ArgumentCaptor<GetPickupDateInformationCommand> captor;

    @Test
    public void testGetPickupDateInformationWithInvalidResponseCode() {
        PickupDateRequestData request = new PickupDateRequestData();

        PickupDateResponseData expectedPayload = new PickupDateResponseData();

        Response<PickupDateResponseData> response = new ResponseImpl<>();
        response.setHttpStatus(HttpStatus.FORBIDDEN);
        response.setPayLoad(expectedPayload);

        when(executor.executeCommand(any(GetPickupDateInformationCommand.class), eq(null))).thenReturn(response);

        PickupDateResponseData actualPayload = defaultESBPickupDateService.getPickupDateInformation(request);

        verify(executor).executeCommand(captor.capture(), eq(null));

        Assertions
                .assertThat(actualPayload)
                .isNull();

        GetPickupDateInformationCommand command = captor.getValue();

        Assertions
                .assertThat(command)
                .isNotNull();

        Assertions
                .assertThat(command.getPayLoad())
                .isEqualTo(request);
    }

    @Test
    public void testGetPickupDateInformationWithoutPayload() {
        PickupDateRequestData request = new PickupDateRequestData();

        Response<PickupDateResponseData> response = new ResponseImpl<>();
        response.setHttpStatus(HttpStatus.OK);

        when(executor.executeCommand(any(GetPickupDateInformationCommand.class), eq(null))).thenReturn(response);

        PickupDateResponseData actualPayload = defaultESBPickupDateService.getPickupDateInformation(request);

        verify(executor).executeCommand(captor.capture(), eq(null));

        Assertions
                .assertThat(actualPayload)
                .isNull();

        GetPickupDateInformationCommand command = captor.getValue();

        Assertions
                .assertThat(command)
                .isNotNull();

        Assertions
                .assertThat(command.getPayLoad())
                .isEqualTo(request);
    }

    @Test
    public void testGetPickupDateInformationWithExceptionDuringRemoteCall() {
        PickupDateRequestData request = new PickupDateRequestData();

        RuntimeException exception = mock(RuntimeException.class);

        when(executor.executeCommand(any(GetPickupDateInformationCommand.class), eq(null))).thenThrow(exception);

        PickupDateResponseData actualPayload = defaultESBPickupDateService.getPickupDateInformation(request);

        verify(executor).executeCommand(captor.capture(), eq(null));

        Assertions
                .assertThat(actualPayload)
                .isNull();

        GetPickupDateInformationCommand command = captor.getValue();

        Assertions
                .assertThat(command)
                .isNotNull();

        Assertions
                .assertThat(command.getPayLoad())
                .isEqualTo(request);
    }

    @Test
    public void testGetPickupDateInformation() {
        PickupDateRequestData request = new PickupDateRequestData();

        PickupDateResponseData expectedPayload = new PickupDateResponseData();

        Response<PickupDateResponseData> response = new ResponseImpl<>();
        response.setHttpStatus(HttpStatus.OK);
        response.setPayLoad(expectedPayload);

        when(executor.executeCommand(any(GetPickupDateInformationCommand.class), eq(null))).thenReturn(response);

        PickupDateResponseData actualPayload = defaultESBPickupDateService.getPickupDateInformation(request);

        verify(executor).executeCommand(captor.capture(), eq(null));

        Assertions
                .assertThat(actualPayload)
                .isEqualTo(expectedPayload);

        GetPickupDateInformationCommand command = captor.getValue();

        Assertions
                .assertThat(command)
                .isNotNull();

        Assertions
                .assertThat(command.getPayLoad())
                .isEqualTo(request);
    }
}