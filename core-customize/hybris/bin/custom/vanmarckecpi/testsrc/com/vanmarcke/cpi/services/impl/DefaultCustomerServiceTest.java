package com.vanmarcke.cpi.services.impl;

import com.vanmarcke.cpi.commands.GetCustomerInformationCommand;
import com.vanmarcke.cpi.data.customer.CustomerRequestData;
import com.vanmarcke.cpi.data.customer.CustomerResponseData;
import de.hybris.bootstrap.annotations.UnitTest;
import eu.elision.integration.command.Response;
import eu.elision.integration.command.executer.CommandExecutor;
import eu.elision.integration.command.impl.ResponseImpl;
import org.fest.assertions.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

/**
 * The {@code DefaultCustomerServiceTest} class contains the unit tests for the {@link DefaultCustomerService} class.
 *
 * @author Taki Korovessis, Christiaan Janssen
 * @since 19-12-2019
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultCustomerServiceTest {

    @Mock
    private CommandExecutor executor;

    @InjectMocks
    private DefaultCustomerService defaultCustomerService;

    @Captor
    private ArgumentCaptor<GetCustomerInformationCommand> captor;

    @Test
    public void testGetCustomerWithInvalidResponseCode() {
        CustomerRequestData request = new CustomerRequestData();

        CustomerResponseData expectedPayload = new CustomerResponseData();

        Response<CustomerResponseData> response = new ResponseImpl<>();
        response.setHttpStatus(HttpStatus.FORBIDDEN);
        response.setPayLoad(expectedPayload);

        when(executor.executeCommand(any(GetCustomerInformationCommand.class), eq(null))).thenReturn(response);

        CustomerResponseData actualPayload = defaultCustomerService.getCustomer(request);

        verify(executor).executeCommand(captor.capture(), eq(null));

        Assertions
                .assertThat(actualPayload)
                .isNull();

        GetCustomerInformationCommand command = captor.getValue();

        Assertions
                .assertThat(command)
                .isNotNull();

        Assertions
                .assertThat(command.getPayLoad())
                .isEqualTo(request);
    }

    @Test
    public void testGetCustomerWithoutPayload() {
        CustomerRequestData request = new CustomerRequestData();

        Response<CustomerResponseData> response = new ResponseImpl<>();
        response.setHttpStatus(HttpStatus.OK);

        when(executor.executeCommand(any(GetCustomerInformationCommand.class), eq(null))).thenReturn(response);

        CustomerResponseData actualPayload = defaultCustomerService.getCustomer(request);

        verify(executor).executeCommand(captor.capture(), eq(null));

        Assertions
                .assertThat(actualPayload)
                .isNull();

        GetCustomerInformationCommand command = captor.getValue();

        Assertions
                .assertThat(command)
                .isNotNull();

        Assertions
                .assertThat(command.getPayLoad())
                .isEqualTo(request);
    }

    @Test
    public void testGetCustomerWithExceptionDuringRemoteCall() {
        CustomerRequestData request = new CustomerRequestData();

        RuntimeException exception = mock(RuntimeException.class);

        when(executor.executeCommand(any(GetCustomerInformationCommand.class), eq(null))).thenThrow(exception);

        CustomerResponseData actualPayload = defaultCustomerService.getCustomer(request);

        verify(executor).executeCommand(captor.capture(), eq(null));

        Assertions
                .assertThat(actualPayload)
                .isNull();

        GetCustomerInformationCommand command = captor.getValue();

        Assertions
                .assertThat(command)
                .isNotNull();

        Assertions
                .assertThat(command.getPayLoad())
                .isEqualTo(request);
    }

    @Test
    public void testGetCustomer() {
        CustomerRequestData request = new CustomerRequestData();

        CustomerResponseData expectedPayload = new CustomerResponseData();

        Response<CustomerResponseData> response = new ResponseImpl<>();
        response.setHttpStatus(HttpStatus.OK);
        response.setPayLoad(expectedPayload);

        when(executor.executeCommand(any(GetCustomerInformationCommand.class), eq(null))).thenReturn(response);

        CustomerResponseData actualPayload = defaultCustomerService.getCustomer(request);

        verify(executor).executeCommand(captor.capture(), eq(null));

        Assertions
                .assertThat(actualPayload)
                .isEqualTo(expectedPayload);

        GetCustomerInformationCommand command = captor.getValue();

        Assertions
                .assertThat(command)
                .isNotNull();

        Assertions
                .assertThat(command.getPayLoad())
                .isEqualTo(request);
    }
}