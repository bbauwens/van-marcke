package com.vanmarcke.cpi.commands;

import com.vanmarcke.cpi.data.order.AlternativeTECRequestData;
import com.vanmarcke.cpi.data.order.AlternativeTECResponseData;
import de.hybris.bootstrap.annotations.UnitTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpMethod;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class GetAlternativeTECInformationCommandTest {

    private GetAlternativeTECInformationCommand command;

    @Test
    public void testConstructor() {
        final AlternativeTECRequestData requestData = mock(AlternativeTECRequestData.class);

        this.command = new GetAlternativeTECInformationCommand(requestData);

        assertThat(this.command.getHttpMethod()).isEqualTo(HttpMethod.POST);
        assertThat(this.command.getServiceName()).isEqualTo("alternativeTECInformationService");
        assertThat(this.command.getUrl()).isEqualTo("/alternativetecs");
        assertThat(this.command.getPayLoad()).isEqualTo(requestData);
        assertThat(this.command.getResponseType()).isEqualTo(AlternativeTECResponseData.class);
    }
}