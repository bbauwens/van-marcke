package com.vanmarcke.cpi.services.impl;

import com.vanmarcke.cpi.commands.GetCreditCheckInformationCommand;
import com.vanmarcke.cpi.data.order.CreditCheckResponseData;
import com.vanmarcke.cpi.data.order.OrderRequestData;
import de.hybris.bootstrap.annotations.UnitTest;
import eu.elision.integration.command.Response;
import eu.elision.integration.command.executer.CommandExecutor;
import eu.elision.integration.command.impl.ResponseImpl;
import org.fest.assertions.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

/**
 * THe {@code ESBCreditCheckServiceImplTest} class contains the unit tests for the
 * {@link ESBCreditCheckServiceImpl} class.
 *
 * @author Niels Raemaekers, Christiaan Janssen
 * @since 23-07-2019
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ESBCreditCheckServiceImplTest {

    @Mock
    private CommandExecutor executor;

    @InjectMocks
    private ESBCreditCheckServiceImpl esbCreditCheckService;

    @Captor
    private ArgumentCaptor<GetCreditCheckInformationCommand> captor;

    @Test
    public void testGetCreditCheckInformationWithInvalidResponseCode() {
        OrderRequestData request = new OrderRequestData();

        CreditCheckResponseData expectedPayload = new CreditCheckResponseData();

        Response<CreditCheckResponseData> response = new ResponseImpl<>();
        response.setHttpStatus(HttpStatus.FORBIDDEN);
        response.setPayLoad(expectedPayload);

        when(executor.executeCommand(any(GetCreditCheckInformationCommand.class), eq(null))).thenReturn(response);

        CreditCheckResponseData actualPayload = esbCreditCheckService.getCreditCheckInformation(request);

        verify(executor).executeCommand(captor.capture(), eq(null));

        Assertions
                .assertThat(actualPayload)
                .isNull();

        GetCreditCheckInformationCommand command = captor.getValue();

        Assertions
                .assertThat(command)
                .isNotNull();

        Assertions
                .assertThat(command.getPayLoad())
                .isEqualTo(request);
    }

    @Test
    public void testGetCreditCheckInformationWithoutPayload() {
        OrderRequestData request = new OrderRequestData();

        Response<CreditCheckResponseData> response = new ResponseImpl<>();
        response.setHttpStatus(HttpStatus.OK);

        when(executor.executeCommand(any(GetCreditCheckInformationCommand.class), eq(null))).thenReturn(response);

        CreditCheckResponseData actualPayload = esbCreditCheckService.getCreditCheckInformation(request);

        verify(executor).executeCommand(captor.capture(), eq(null));

        Assertions
                .assertThat(actualPayload)
                .isNull();

        GetCreditCheckInformationCommand command = captor.getValue();

        Assertions
                .assertThat(command)
                .isNotNull();

        Assertions
                .assertThat(command.getPayLoad())
                .isEqualTo(request);
    }

    @Test
    public void testGetCreditCheckInformationWithExceptionDuringRemoteCall() {
        OrderRequestData request = new OrderRequestData();

        RuntimeException exception = mock(RuntimeException.class);

        when(executor.executeCommand(any(GetCreditCheckInformationCommand.class), eq(null))).thenThrow(exception);

        CreditCheckResponseData actualPayload = esbCreditCheckService.getCreditCheckInformation(request);

        verify(executor).executeCommand(captor.capture(), eq(null));

        Assertions
                .assertThat(actualPayload)
                .isNull();

        GetCreditCheckInformationCommand command = captor.getValue();

        Assertions
                .assertThat(command)
                .isNotNull();

        Assertions
                .assertThat(command.getPayLoad())
                .isEqualTo(request);
    }

    @Test
    public void testGetCreditCheckInformation() {
        OrderRequestData request = new OrderRequestData();

        CreditCheckResponseData expectedPayload = new CreditCheckResponseData();

        Response<CreditCheckResponseData> response = new ResponseImpl<>();
        response.setHttpStatus(HttpStatus.OK);
        response.setPayLoad(expectedPayload);

        when(executor.executeCommand(any(GetCreditCheckInformationCommand.class), eq(null))).thenReturn(response);

        CreditCheckResponseData actualPayload = esbCreditCheckService.getCreditCheckInformation(request);

        verify(executor).executeCommand(captor.capture(), eq(null));

        Assertions
                .assertThat(actualPayload)
                .isEqualTo(expectedPayload);

        GetCreditCheckInformationCommand command = captor.getValue();

        Assertions
                .assertThat(command)
                .isNotNull();

        Assertions
                .assertThat(command.getPayLoad())
                .isEqualTo(request);
    }
}