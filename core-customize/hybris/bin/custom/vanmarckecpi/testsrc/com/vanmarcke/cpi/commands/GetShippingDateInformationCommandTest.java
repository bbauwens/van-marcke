package com.vanmarcke.cpi.commands;

import com.vanmarcke.cpi.data.order.ShippingDateRequestData;
import com.vanmarcke.cpi.data.order.ShippingDateResponseData;
import de.hybris.bootstrap.annotations.UnitTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpMethod;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class GetShippingDateInformationCommandTest {

    private GetShippingDateInformationCommand command;

    @Test
    public void testConstructor() {
        ShippingDateRequestData requestData = mock(ShippingDateRequestData.class);

        command = new GetShippingDateInformationCommand(requestData);

        assertThat(command.getHttpMethod()).isEqualTo(HttpMethod.POST);
        assertThat(command.getServiceName()).isEqualTo("shippingDateInformationService");
        assertThat(command.getUrl()).isEqualTo("/deliverydate1");
        assertThat(command.getPayLoad()).isEqualTo(requestData);
        assertThat(command.getResponseType()).isEqualTo(ShippingDateResponseData.class);
    }

}
