package com.vanmarcke.cpi.services.impl;

import com.vanmarcke.cpi.commands.GetAlternativeTECInformationCommand;
import com.vanmarcke.cpi.data.order.AlternativeTECRequestData;
import com.vanmarcke.cpi.data.order.AlternativeTECResponseData;
import de.hybris.bootstrap.annotations.UnitTest;
import eu.elision.integration.command.Response;
import eu.elision.integration.command.executer.CommandExecutor;
import eu.elision.integration.command.impl.ResponseImpl;
import org.fest.assertions.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

/**
 * The {@code ESBAlternativeTECServiceImplTest} class contains the unit tests for the
 * {@link ESBAlternativeTECServiceImpl} class.
 *
 * @author Niels Raemaekers, Christiaan Janssen
 * @since 22-11-2019
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ESBAlternativeTECServiceImplTest {

    @Mock
    private CommandExecutor executor;

    @InjectMocks
    private ESBAlternativeTECServiceImpl esbAlternativeTECService;

    @Captor
    private ArgumentCaptor<GetAlternativeTECInformationCommand> captor;

    @Test
    public void testGetAlternativeTECInformationWithInvalidResponseCode() {
        AlternativeTECRequestData request = new AlternativeTECRequestData();

        AlternativeTECResponseData expectedPayload = new AlternativeTECResponseData();

        Response<AlternativeTECResponseData> response = new ResponseImpl<>();
        response.setHttpStatus(HttpStatus.FORBIDDEN);
        response.setPayLoad(expectedPayload);

        when(executor.executeCommand(any(GetAlternativeTECInformationCommand.class), eq(null))).thenReturn(response);

        AlternativeTECResponseData actualPayload = esbAlternativeTECService.getAlternativeTECInformation(request);

        verify(executor).executeCommand(captor.capture(), eq(null));

        Assertions
                .assertThat(actualPayload)
                .isNull();

        GetAlternativeTECInformationCommand command = captor.getValue();

        Assertions
                .assertThat(command)
                .isNotNull();

        Assertions
                .assertThat(command.getPayLoad())
                .isEqualTo(request);
    }

    @Test
    public void testGetAlternativeTECInformationWithoutPayload() {
        AlternativeTECRequestData request = new AlternativeTECRequestData();

        Response<AlternativeTECResponseData> response = new ResponseImpl<>();
        response.setHttpStatus(HttpStatus.OK);

        when(executor.executeCommand(any(GetAlternativeTECInformationCommand.class), eq(null))).thenReturn(response);

        AlternativeTECResponseData actualPayload = esbAlternativeTECService.getAlternativeTECInformation(request);

        verify(executor).executeCommand(captor.capture(), eq(null));

        Assertions
                .assertThat(actualPayload)
                .isNull();

        GetAlternativeTECInformationCommand command = captor.getValue();

        Assertions
                .assertThat(command)
                .isNotNull();

        Assertions
                .assertThat(command.getPayLoad())
                .isEqualTo(request);
    }

    @Test
    public void testGetAlternativeTECInformationWithExceptionDuringRemoteCall() {
        AlternativeTECRequestData request = new AlternativeTECRequestData();

        RuntimeException exception = mock(RuntimeException.class);

        when(executor.executeCommand(any(GetAlternativeTECInformationCommand.class), eq(null))).thenThrow(exception);

        AlternativeTECResponseData actualPayload = esbAlternativeTECService.getAlternativeTECInformation(request);

        verify(executor).executeCommand(captor.capture(), eq(null));

        Assertions
                .assertThat(actualPayload)
                .isNull();

        GetAlternativeTECInformationCommand command = captor.getValue();

        Assertions
                .assertThat(command)
                .isNotNull();

        Assertions
                .assertThat(command.getPayLoad())
                .isEqualTo(request);
    }

    @Test
    public void testGetAlternativeTECInformation() {
        AlternativeTECRequestData request = new AlternativeTECRequestData();

        AlternativeTECResponseData expectedPayload = new AlternativeTECResponseData();

        Response<AlternativeTECResponseData> response = new ResponseImpl<>();
        response.setHttpStatus(HttpStatus.OK);
        response.setPayLoad(expectedPayload);

        when(executor.executeCommand(any(GetAlternativeTECInformationCommand.class), eq(null))).thenReturn(response);

        AlternativeTECResponseData actualPayload = esbAlternativeTECService.getAlternativeTECInformation(request);

        verify(executor).executeCommand(captor.capture(), eq(null));

        Assertions
                .assertThat(actualPayload)
                .isEqualTo(expectedPayload);

        GetAlternativeTECInformationCommand command = captor.getValue();

        Assertions
                .assertThat(command)
                .isNotNull();

        Assertions
                .assertThat(command.getPayLoad())
                .isEqualTo(request);
    }
}