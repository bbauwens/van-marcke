package com.vanmarcke.cpi.populators;

import com.vanmarcke.cpi.data.order.PickupDateRequestData;
import com.vanmarcke.cpi.data.order.ShippingDateRequestData;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static java.util.Collections.singletonList;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKPickupDateRequestPopulatorTest {

    @Mock
    private Converter<AbstractOrderEntryModel, ShippingDateRequestData> entryRequestDataConverter;
    @Mock
    private UserService userService;

    @InjectMocks
    private VMKPickupDateRequestPopulator populator;

    @Test
    public void testPopulate() {
        PointOfServiceModel pointOfServiceModel = mock(PointOfServiceModel.class);
        AddressModel addressModel = mock(AddressModel.class);
        CountryModel countryModel = mock(CountryModel.class);
        B2BUnitModel unitModel = mock(B2BUnitModel.class);
        AbstractOrderEntryModel abstractOrderEntryModel = mock(AbstractOrderEntryModel.class);
        AbstractOrderModel source = mock(AbstractOrderModel.class);

        when(unitModel.getUid()).thenReturn("unit-uid");

        when(source.getCode()).thenReturn("code");
        when(source.getUnit()).thenReturn(unitModel);

        when(countryModel.getIsocode()).thenReturn("pos-country-code");

        when(addressModel.getPostalcode()).thenReturn("pos-postalcode");
        when(addressModel.getTown()).thenReturn("pos-town");
        when(addressModel.getCountry()).thenReturn(countryModel);

        when(pointOfServiceModel.getName()).thenReturn("pos-name");
        when(pointOfServiceModel.getAddress()).thenReturn(addressModel);

        when(source.getDeliveryPointOfService()).thenReturn(pointOfServiceModel);

        when(source.getEntries()).thenReturn(singletonList(abstractOrderEntryModel));

        ShippingDateRequestData entryRequest = mock(ShippingDateRequestData.class);
        when(entryRequestDataConverter.convertAll(singletonList(abstractOrderEntryModel))).thenReturn(singletonList(entryRequest));

        PickupDateRequestData result = new PickupDateRequestData();
        populator.populate(source, result);

        assertThat(result.getOrderID()).isEqualTo("code");
        assertThat(result.getCustomerID()).isEqualTo("unit-uid");
        assertThat(result.getStoreID()).isEqualTo("pos-name");
        assertThat(result.getStorePostalCode()).isEqualTo("pos-postalcode");
        assertThat(result.getStoreCity()).isEqualTo("pos-town");
        assertThat(result.getStoreCountryCode()).isEqualTo("pos-country-code");
        assertThat(result.getEntries()).containsExactly(entryRequest);

        verifyZeroInteractions(userService);
    }

    @Test
    public void testPopulate_withoutPointOfService() {
        UserModel userModel = mock(UserModel.class);
        PointOfServiceModel pointOfServiceModel = mock(PointOfServiceModel.class);
        AddressModel addressModel = mock(AddressModel.class);
        CountryModel countryModel = mock(CountryModel.class);
        B2BUnitModel unitModel = mock(B2BUnitModel.class);
        AbstractOrderEntryModel abstractOrderEntryModel = mock(AbstractOrderEntryModel.class);
        AbstractOrderModel source = mock(AbstractOrderModel.class);

        when(unitModel.getUid()).thenReturn("unit-uid");

        when(source.getCode()).thenReturn("code");
        when(source.getUnit()).thenReturn(unitModel);

        when(countryModel.getIsocode()).thenReturn("pos-country-code");

        when(addressModel.getPostalcode()).thenReturn("pos-postalcode");
        when(addressModel.getTown()).thenReturn("pos-town");
        when(addressModel.getCountry()).thenReturn(countryModel);

        when(pointOfServiceModel.getName()).thenReturn("pos-name");
        when(pointOfServiceModel.getAddress()).thenReturn(addressModel);

        when(userModel.getSessionStore()).thenReturn(pointOfServiceModel);

        when(source.getEntries()).thenReturn(singletonList(abstractOrderEntryModel));

        when(userService.getCurrentUser()).thenReturn(userModel);

        ShippingDateRequestData entryRequest = mock(ShippingDateRequestData.class);
        when(entryRequestDataConverter.convertAll(singletonList(abstractOrderEntryModel))).thenReturn(singletonList(entryRequest));

        PickupDateRequestData result = new PickupDateRequestData();
        populator.populate(source, result);

        assertThat(result.getOrderID()).isEqualTo("code");
        assertThat(result.getCustomerID()).isEqualTo("unit-uid");
        assertThat(result.getStoreID()).isEqualTo("pos-name");
        assertThat(result.getStorePostalCode()).isEqualTo("pos-postalcode");
        assertThat(result.getStoreCity()).isEqualTo("pos-town");
        assertThat(result.getStoreCountryCode()).isEqualTo("pos-country-code");
        assertThat(result.getEntries()).containsExactly(entryRequest);
    }
}