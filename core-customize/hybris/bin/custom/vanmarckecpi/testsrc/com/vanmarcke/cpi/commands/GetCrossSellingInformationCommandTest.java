package com.vanmarcke.cpi.commands;

import com.vanmarcke.cpi.data.product.CrossSellingRequestData;
import com.vanmarcke.cpi.data.product.CrossSellingResponseData;
import de.hybris.bootstrap.annotations.UnitTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpMethod;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class GetCrossSellingInformationCommandTest {

    private GetCrossSellingInformationCommand command;

    @Test
    public void testConstructor() {
        final CrossSellingRequestData crossSellingRequestData = mock(CrossSellingRequestData.class);

        this.command = new GetCrossSellingInformationCommand(crossSellingRequestData);

        assertThat(this.command.getHttpMethod()).isEqualTo(HttpMethod.POST);
        assertThat(this.command.getServiceName()).isEqualTo("crossSellingInformationService");
        assertThat(this.command.getUrl()).isEqualTo("/togetheritems");
        assertThat(this.command.getPayLoad()).isEqualTo(crossSellingRequestData);
        assertThat(this.command.getResponseType()).isEqualTo(CrossSellingResponseData.class);
    }
}