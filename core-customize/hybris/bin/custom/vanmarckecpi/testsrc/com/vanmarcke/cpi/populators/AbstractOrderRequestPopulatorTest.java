package com.vanmarcke.cpi.populators;

import com.vanmarcke.cpi.data.order.AddressRequestData;
import com.vanmarcke.cpi.data.order.OrderEntryRequestData;
import com.vanmarcke.cpi.data.order.OrderRequestData;
import com.vanmarcke.cpi.data.order.PaymentInfoRequestData;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.model.PickUpDeliveryModeModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

/**
 * The {@link AbstractOrderRequestPopulatorTest} class contains the unit tests for the
 * {@link AbstractOrderRequestPopulator} class.
 *
 * @author Christiaan Janssen, Niels Raemaekers
 * @since 05-02-2021
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AbstractOrderRequestPopulatorTest {

    private static final Long CUSTOMER_ID = RandomUtils.nextLong(0, 99999);

    private static final String ORDER_DELIVERY_REFERENCE = RandomStringUtils.random(10);
    private static final String ORDER_DELIVERY_COMMENT = RandomStringUtils.random(10);
    private static final Date ORDER_DELIVERY_DATE = new Date();

    private static final String POS_NAME = RandomStringUtils.random(10);

    @Mock
    private Converter<AddressModel, AddressRequestData> addressRequestConverter;

    @Mock
    private Converter<AbstractOrderEntryModel, OrderEntryRequestData> orderEntryRequestConverter;

    @Mock
    private Converter<PaymentTransactionEntryModel, PaymentInfoRequestData> paymentInformationRequestConverter;

    private MockOrderRequestPopulator mockOrderRequestPopulator;

    @Before
    public void setUp() {
        mockOrderRequestPopulator = new MockOrderRequestPopulator(addressRequestConverter, orderEntryRequestConverter, paymentInformationRequestConverter);
    }

    @Test
    public void testGetVirtualCardNumber_withoutCustomerID() {
        String actualVirtualCardNumber = mockOrderRequestPopulator.getVirtualCardNumber(null);

        assertThat(actualVirtualCardNumber).isNull();
    }

    @Test
    public void testGetVirtualCardNumber_withCustomerID() {
        String actualVirtualCardNumber = mockOrderRequestPopulator.getVirtualCardNumber("P" + CUSTOMER_ID);

        assertThat(actualVirtualCardNumber).isEqualTo(String.valueOf(500000 + CUSTOMER_ID));
    }

    @Test
    public void testAddDeliveryInformation_withoutValues() {
        OrderModel orderModel = mock(OrderModel.class);

        OrderRequestData orderRequestData = new OrderRequestData();

        mockOrderRequestPopulator.addDeliveryInformation(orderModel, orderRequestData);

        assertThat(orderRequestData.getStoreID()).isEmpty();
        assertThat(orderRequestData.isStoreCollect()).isFalse();
        assertThat(orderRequestData.getDeliveryReference()).isEmpty();
        assertThat(orderRequestData.getDeliveryComment()).isEmpty();
        assertThat(orderRequestData.getDeliveryAddress()).isNull();

        verifyZeroInteractions(addressRequestConverter);
    }

    @Test
    public void testAddDeliveryInformation_withZoneDeliveryMode_withoutAddress() {
        ZoneDeliveryModeModel deliveryModeModel = mock(ZoneDeliveryModeModel.class);

        OrderModel orderModel = mock(OrderModel.class);
        when(orderModel.getDeliveryDate()).thenReturn(ORDER_DELIVERY_DATE);
        when(orderModel.getDeliveryComment()).thenReturn(ORDER_DELIVERY_COMMENT);
        when(orderModel.getYardReference()).thenReturn(ORDER_DELIVERY_REFERENCE);
        when(orderModel.getDeliveryMode()).thenReturn(deliveryModeModel);

        OrderRequestData orderRequestData = new OrderRequestData();

        mockOrderRequestPopulator.addDeliveryInformation(orderModel, orderRequestData);

        assertThat(orderRequestData.getStoreID()).isEmpty();
        assertThat(orderRequestData.isStoreCollect()).isFalse();
        assertThat(orderRequestData.getDeliveryReference()).isEqualTo(ORDER_DELIVERY_REFERENCE);
        assertThat(orderRequestData.getDeliveryComment()).isEqualTo(ORDER_DELIVERY_COMMENT);
        assertThat(orderRequestData.getDeliveryAddress()).isNull();

        verifyZeroInteractions(addressRequestConverter);
    }

    @Test
    public void testAddDeliveryInformation_withZoneDeliveryMode_withAddress() {
        AddressModel deliveryAddressModel = mock(AddressModel.class);

        ZoneDeliveryModeModel deliveryModeModel = mock(ZoneDeliveryModeModel.class);

        OrderModel orderModel = mock(OrderModel.class);
        when(orderModel.getDeliveryDate()).thenReturn(ORDER_DELIVERY_DATE);
        when(orderModel.getDeliveryComment()).thenReturn(ORDER_DELIVERY_COMMENT);
        when(orderModel.getYardReference()).thenReturn(ORDER_DELIVERY_REFERENCE);
        when(orderModel.getDeliveryMode()).thenReturn(deliveryModeModel);
        when(orderModel.getDeliveryAddress()).thenReturn(deliveryAddressModel);

        AddressRequestData deliveryAddressData = new AddressRequestData();

        when(addressRequestConverter.convert(deliveryAddressModel)).thenReturn(deliveryAddressData);

        OrderRequestData orderRequestData = new OrderRequestData();

        mockOrderRequestPopulator.addDeliveryInformation(orderModel, orderRequestData);

        assertThat(orderRequestData.getStoreID()).isEmpty();
        assertThat(orderRequestData.isStoreCollect()).isFalse();
        assertThat(orderRequestData.getDeliveryReference()).isEqualTo(ORDER_DELIVERY_REFERENCE);
        assertThat(orderRequestData.getDeliveryComment()).isEqualTo(ORDER_DELIVERY_COMMENT);
        assertThat(orderRequestData.getDeliveryAddress()).isEqualTo(deliveryAddressData);

        verify(addressRequestConverter).convert(deliveryAddressModel);
    }

    @Test
    public void testAddDeliveryInformation_withPickUpDeliveryMode_withTECCollect() {
        PickUpDeliveryModeModel deliveryModeModel = mock(PickUpDeliveryModeModel.class);

        PointOfServiceModel posModel = mock(PointOfServiceModel.class);
        when(posModel.getName()).thenReturn(POS_NAME);

        Calendar calendar = new GregorianCalendar();
        calendar.add(Calendar.DATE, 1);

        OrderModel orderModel = mock(OrderModel.class);
        when(orderModel.getDeliveryDate()).thenReturn(calendar.getTime());
        when(orderModel.getDeliveryComment()).thenReturn(ORDER_DELIVERY_COMMENT);
        when(orderModel.getYardReference()).thenReturn(ORDER_DELIVERY_REFERENCE);
        when(orderModel.getDeliveryMode()).thenReturn(deliveryModeModel);
        when(orderModel.getDeliveryPointOfService()).thenReturn(posModel);

        OrderRequestData orderRequestData = new OrderRequestData();

        mockOrderRequestPopulator.addDeliveryInformation(orderModel, orderRequestData);

        assertThat(orderRequestData.getStoreID()).isEqualTo(POS_NAME);
        assertThat(orderRequestData.isStoreCollect()).isFalse();
        assertThat(orderRequestData.getDeliveryReference()).isEqualTo(ORDER_DELIVERY_REFERENCE);
        assertThat(orderRequestData.getDeliveryComment()).isEqualTo(ORDER_DELIVERY_COMMENT);
        assertThat(orderRequestData.getDeliveryAddress()).isNotNull();
        assertThat(orderRequestData.getDeliveryAddress().getApartment()).isEqualTo(StringUtils.EMPTY);
        assertThat(orderRequestData.getDeliveryAddress().getCity()).isEqualTo(StringUtils.EMPTY);
        assertThat(orderRequestData.getDeliveryAddress().getCountry()).isEqualTo(StringUtils.EMPTY);
        assertThat(orderRequestData.getDeliveryAddress().getEmail()).isEqualTo(StringUtils.EMPTY);
        assertThat(orderRequestData.getDeliveryAddress().getFax()).isEqualTo(StringUtils.EMPTY);
        assertThat(orderRequestData.getDeliveryAddress().getMobile()).isEqualTo(StringUtils.EMPTY);
        assertThat(orderRequestData.getDeliveryAddress().getPhone()).isEqualTo(StringUtils.EMPTY);
        assertThat(orderRequestData.getDeliveryAddress().getName()).isEqualTo(StringUtils.EMPTY);
        assertThat(orderRequestData.getDeliveryAddress().getPostalCode()).isEqualTo(StringUtils.EMPTY);
        assertThat(orderRequestData.getDeliveryAddress().getStreetName()).isEqualTo(StringUtils.EMPTY);
        assertThat(orderRequestData.getDeliveryAddress().getStreetNumber()).isEqualTo(StringUtils.EMPTY);

        verifyZeroInteractions(addressRequestConverter);
    }

    @Test
    public void testAddDeliveryInformation_withPickUpDeliveryMode_withPickUp() {
        PickUpDeliveryModeModel deliveryModeModel = mock(PickUpDeliveryModeModel.class);

        PointOfServiceModel posModel = mock(PointOfServiceModel.class);
        when(posModel.getName()).thenReturn(POS_NAME);

        OrderModel orderModel = mock(OrderModel.class);
        when(orderModel.getDeliveryDate()).thenReturn(ORDER_DELIVERY_DATE);
        when(orderModel.getDeliveryComment()).thenReturn(ORDER_DELIVERY_COMMENT);
        when(orderModel.getYardReference()).thenReturn(ORDER_DELIVERY_REFERENCE);
        when(orderModel.getDeliveryMode()).thenReturn(deliveryModeModel);
        when(orderModel.getDeliveryPointOfService()).thenReturn(posModel);

        OrderRequestData orderRequestData = new OrderRequestData();

        mockOrderRequestPopulator.addDeliveryInformation(orderModel, orderRequestData);

        assertThat(orderRequestData.getStoreID()).isEqualTo(POS_NAME);
        assertThat(orderRequestData.isStoreCollect()).isTrue();
        assertThat(orderRequestData.getDeliveryReference()).isEqualTo(ORDER_DELIVERY_REFERENCE);
        assertThat(orderRequestData.getDeliveryComment()).isEqualTo(ORDER_DELIVERY_COMMENT);
        assertThat(orderRequestData.getDeliveryAddress()).isNotNull();
        assertThat(orderRequestData.getDeliveryAddress().getApartment()).isEqualTo(StringUtils.EMPTY);
        assertThat(orderRequestData.getDeliveryAddress().getCity()).isEqualTo(StringUtils.EMPTY);
        assertThat(orderRequestData.getDeliveryAddress().getCountry()).isEqualTo(StringUtils.EMPTY);
        assertThat(orderRequestData.getDeliveryAddress().getEmail()).isEqualTo(StringUtils.EMPTY);
        assertThat(orderRequestData.getDeliveryAddress().getFax()).isEqualTo(StringUtils.EMPTY);
        assertThat(orderRequestData.getDeliveryAddress().getMobile()).isEqualTo(StringUtils.EMPTY);
        assertThat(orderRequestData.getDeliveryAddress().getPhone()).isEqualTo(StringUtils.EMPTY);
        assertThat(orderRequestData.getDeliveryAddress().getName()).isEqualTo(StringUtils.EMPTY);
        assertThat(orderRequestData.getDeliveryAddress().getPostalCode()).isEqualTo(StringUtils.EMPTY);
        assertThat(orderRequestData.getDeliveryAddress().getStreetName()).isEqualTo(StringUtils.EMPTY);
        assertThat(orderRequestData.getDeliveryAddress().getStreetNumber()).isEqualTo(StringUtils.EMPTY);

        verifyZeroInteractions(addressRequestConverter);
    }

    @Test
    public void testAreRequestedDeliveryDatesTheSame_SameDates() throws Exception {
        OrderModel orderModel = mock(OrderModel.class);
        OrderEntryModel entry1 = mock(OrderEntryModel.class);
        OrderEntryModel entry2 = mock(OrderEntryModel.class);

        when(orderModel.getEntries()).thenReturn(Arrays.asList(entry1, entry2));
        when(entry1.getDeliveryDate()).thenReturn(new SimpleDateFormat("yyyy-MM-dd").parse("2021-04-21"));
        when(entry2.getDeliveryDate()).thenReturn(new SimpleDateFormat("yyyy-MM-dd").parse("2021-04-21"));
        boolean result = mockOrderRequestPopulator.areRequestedDeliveryDatesTheSame(orderModel);

        assertThat(result).isTrue();
    }

    @Test
    public void testAreRequestedDeliveryDatesTheSame_DifferentDates() throws Exception {
        OrderModel orderModel = mock(OrderModel.class);
        OrderEntryModel entry1 = mock(OrderEntryModel.class);
        OrderEntryModel entry2 = mock(OrderEntryModel.class);

        when(orderModel.getEntries()).thenReturn(Arrays.asList(entry1, entry2));
        when(entry1.getDeliveryDate()).thenReturn(new SimpleDateFormat("yyyy-MM-dd").parse("2021-04-21"));
        when(entry2.getDeliveryDate()).thenReturn(new SimpleDateFormat("yyyy-MM-dd").parse("2021-04-28"));
        boolean result = mockOrderRequestPopulator.areRequestedDeliveryDatesTheSame(orderModel);

        assertThat(result).isFalse();
    }

    /**
     * The {@link MockOrderRequestPopulator} class is used to test the functionality of the
     * {@link AbstractOrderRequestPopulator} class.
     */
    private static class MockOrderRequestPopulator extends AbstractOrderRequestPopulator<AbstractOrderModel, OrderRequestData> {

        /**
         * Creates a new instance of the {@link MockOrderRequestPopulator} class
         *
         * @param addressRequestConverter            the address request converter
         * @param orderEntryRequestConverter         the order entry request converter
         * @param paymentInformationRequestConverter the payment information request converter
         */
        public MockOrderRequestPopulator(Converter<AddressModel, AddressRequestData> addressRequestConverter,
                                         Converter<AbstractOrderEntryModel, OrderEntryRequestData> orderEntryRequestConverter,
                                         Converter<PaymentTransactionEntryModel, PaymentInfoRequestData> paymentInformationRequestConverter) {
            super(addressRequestConverter, orderEntryRequestConverter, paymentInformationRequestConverter);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void populate(final AbstractOrderModel abstractOrderModel, final OrderRequestData orderRequestData) throws ConversionException {
            // Do nothing
        }
    }
}
