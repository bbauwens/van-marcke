package com.vanmarcke.cpi.commands;

import de.hybris.bootstrap.annotations.UnitTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import static java.util.Collections.singletonList;
import static org.fest.assertions.Assertions.assertThat;
import static org.fest.assertions.MapAssert.entry;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.internal.util.reflection.Whitebox.getInternalState;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AbstractVMKCPICommandTest {

    private MockCPICommand mockCPICommand = new MockCPICommand();

    @Captor
    private ArgumentCaptor<HttpEntity<Object>> httpEntityArgumentCaptor;

    @Test
    public void testBeforeExecute() {
        RestTemplate restTemplate = mock(RestTemplate.class);

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.add("X-CSRF-Token", "x-csrf-token-value");
        responseHeaders.add("Set-Cookie", "set-cookie-value");

        ResponseEntity<Object> response = ResponseEntity
                .ok()
                .headers(responseHeaders)
                .build();

        when(restTemplate.exchange(eq("https://cpi/http/mock"), eq(HttpMethod.OPTIONS), httpEntityArgumentCaptor.capture(), eq(Object.class))).thenReturn(response);

        mockCPICommand.beforeExecute(restTemplate, "https://cpi/http/mock");

        HttpEntity<Object> tokenRequest = httpEntityArgumentCaptor.getValue();
        assertThat(tokenRequest.getHeaders()).hasSize(2).includes(entry("Accept", singletonList("*/*")), entry("X-CSRF-Token", singletonList("Fetch")));

        HttpHeaders resultHeaders = (HttpHeaders) getInternalState(mockCPICommand, "httpHeaders");
        assertThat(resultHeaders).hasSize(2).includes(entry("X-CSRF-Token", singletonList("x-csrf-token-value")), entry("Cookie", singletonList("set-cookie-value")));
    }

    public static class MockCPICommand extends AbstractVMKCPICommand<String> {
    }
}