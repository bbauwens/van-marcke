package com.vanmarcke.cpi.populators;

import com.vanmarcke.cpi.data.order.DeliveryEntryRequestData;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * The {@link VMKDeliveryEntryRequestPopulatorTest} class contains the unit tests for the
 * {@link VMKDeliveryEntryRequestPopulator} class.
 *
 * @author Taki Korovessis, Tom van den Berg, Christiaan Janssen
 * @since 11-12-2019
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKDeliveryEntryRequestPopulatorTest {

    private static final String PRODUCT_CODE = RandomStringUtils.random(10);
    private static final Long QUANTITY = RandomUtils.nextLong();

    @InjectMocks
    private VMKDeliveryEntryRequestPopulator<AbstractOrderEntryModel, DeliveryEntryRequestData> deliveryEntryRequestPopulator;

    @Test
    public void testPopulate_withoutValues() {
        AbstractOrderEntryModel orderEntryModel = mock(AbstractOrderEntryModel.class);

        DeliveryEntryRequestData deliveryEntryRequestData = new DeliveryEntryRequestData();

        deliveryEntryRequestPopulator.populate(orderEntryModel, deliveryEntryRequestData);

        assertThat(deliveryEntryRequestData.getProductID()).isNull();
        assertThat(deliveryEntryRequestData.getQuantity()).isEqualTo(0L);
    }

    @Test
    public void testPopulate_withValues() {
        ProductModel productModel = mock(ProductModel.class);
        when(productModel.getCode()).thenReturn(PRODUCT_CODE);

        AbstractOrderEntryModel orderEntryModel = mock(AbstractOrderEntryModel.class);
        when(orderEntryModel.getQuantity()).thenReturn(QUANTITY);
        when(orderEntryModel.getProduct()).thenReturn(productModel);

        DeliveryEntryRequestData deliveryEntryRequestData = new DeliveryEntryRequestData();

        deliveryEntryRequestPopulator.populate(orderEntryModel, deliveryEntryRequestData);

        assertThat(deliveryEntryRequestData.getProductID()).isEqualTo(PRODUCT_CODE);
        assertThat(deliveryEntryRequestData.getQuantity()).isEqualTo(QUANTITY);
    }
}