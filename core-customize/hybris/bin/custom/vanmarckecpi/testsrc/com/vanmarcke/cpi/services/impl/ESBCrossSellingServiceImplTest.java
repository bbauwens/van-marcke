package com.vanmarcke.cpi.services.impl;

import com.vanmarcke.cpi.commands.GetCrossSellingInformationCommand;
import com.vanmarcke.cpi.data.product.CrossSellingRequestData;
import com.vanmarcke.cpi.data.product.CrossSellingResponseData;
import de.hybris.bootstrap.annotations.UnitTest;
import eu.elision.integration.command.Response;
import eu.elision.integration.command.executer.CommandExecutor;
import eu.elision.integration.command.impl.ResponseImpl;
import org.fest.assertions.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

/**
 * The {@code ESBCrossSellingServiceImplTest} class contains the unit tests for the
 * {@link ESBCrossSellingServiceImpl} class.
 *
 * @author Christiaan Janssen
 * @since 21-11-2019
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ESBCrossSellingServiceImplTest {

    @Mock
    private CommandExecutor executor;

    @InjectMocks
    private ESBCrossSellingServiceImpl esbCrossSellingService;

    @Captor
    private ArgumentCaptor<GetCrossSellingInformationCommand> captor;

    @Test
    public void testGetCrossSellingInformationWithInvalidResponseCode() {
        CrossSellingRequestData request = new CrossSellingRequestData();

        CrossSellingResponseData expectedPayload = new CrossSellingResponseData();

        Response<CrossSellingResponseData> response = new ResponseImpl<>();
        response.setHttpStatus(HttpStatus.FORBIDDEN);
        response.setPayLoad(expectedPayload);

        when(executor.executeCommand(any(GetCrossSellingInformationCommand.class), eq(null))).thenReturn(response);

        CrossSellingResponseData actualPayload = esbCrossSellingService.getCrossSellingInformation(request);

        verify(executor).executeCommand(captor.capture(), eq(null));

        Assertions
                .assertThat(actualPayload)
                .isNull();

        GetCrossSellingInformationCommand command = captor.getValue();

        Assertions
                .assertThat(command)
                .isNotNull();

        Assertions
                .assertThat(command.getPayLoad())
                .isEqualTo(request);
    }

    @Test
    public void testGetCrossSellingInformationWithoutPayload() {
        CrossSellingRequestData request = new CrossSellingRequestData();

        Response<CrossSellingResponseData> response = new ResponseImpl<>();
        response.setHttpStatus(HttpStatus.OK);

        when(executor.executeCommand(any(GetCrossSellingInformationCommand.class), eq(null))).thenReturn(response);

        CrossSellingResponseData actualPayload = esbCrossSellingService.getCrossSellingInformation(request);

        verify(executor).executeCommand(captor.capture(), eq(null));

        Assertions
                .assertThat(actualPayload)
                .isNull();

        GetCrossSellingInformationCommand command = captor.getValue();

        Assertions
                .assertThat(command)
                .isNotNull();

        Assertions
                .assertThat(command.getPayLoad())
                .isEqualTo(request);
    }

    @Test
    public void testGetCrossSellingInformationWithExceptionDuringRemoteCall() {
        CrossSellingRequestData request = new CrossSellingRequestData();

        RuntimeException exception = mock(RuntimeException.class);

        when(executor.executeCommand(any(GetCrossSellingInformationCommand.class), eq(null))).thenThrow(exception);

        CrossSellingResponseData actualPayload = esbCrossSellingService.getCrossSellingInformation(request);

        verify(executor).executeCommand(captor.capture(), eq(null));

        Assertions
                .assertThat(actualPayload)
                .isNull();

        GetCrossSellingInformationCommand command = captor.getValue();

        Assertions
                .assertThat(command)
                .isNotNull();

        Assertions
                .assertThat(command.getPayLoad())
                .isEqualTo(request);
    }

    @Test
    public void testGetCrossSellingInformation() {
        CrossSellingRequestData request = new CrossSellingRequestData();

        CrossSellingResponseData expectedPayload = new CrossSellingResponseData();

        Response<CrossSellingResponseData> response = new ResponseImpl<>();
        response.setHttpStatus(HttpStatus.OK);
        response.setPayLoad(expectedPayload);

        when(executor.executeCommand(any(GetCrossSellingInformationCommand.class), eq(null))).thenReturn(response);

        CrossSellingResponseData actualPayload = esbCrossSellingService.getCrossSellingInformation(request);

        verify(executor).executeCommand(captor.capture(), eq(null));

        Assertions
                .assertThat(actualPayload)
                .isEqualTo(expectedPayload);

        GetCrossSellingInformationCommand command = captor.getValue();

        Assertions
                .assertThat(command)
                .isNotNull();

        Assertions
                .assertThat(command.getPayLoad())
                .isEqualTo(request);
    }
}