package com.vanmarcke.services.job.maintenance;

import com.vanmarcke.services.job.maintenance.impl.VMKCleanupJobHistoryStrategyImpl;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.jobs.maintenance.MaintenanceCleanupStrategy;
import de.hybris.platform.servicelayer.internal.model.MaintenanceCleanupJobModel;
import de.hybris.platform.servicelayer.internal.service.AbstractBusinessService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.time.TimeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.google.common.base.Preconditions.checkArgument;
import static org.apache.commons.collections4.CollectionUtils.size;
import static org.apache.commons.lang3.time.DateUtils.addDays;

public abstract class AbstractVMKCleanupStrategy<T extends ItemModel> extends AbstractBusinessService implements MaintenanceCleanupStrategy<T, CronJobModel> {

    private static final Logger LOGGER = LoggerFactory.getLogger(VMKCleanupJobHistoryStrategyImpl.class);

    private static final int DEFAULT_DAYS_OLD_TRESHOLD = 14;

    private transient TimeService timeService;

    @Override
    public FlexibleSearchQuery createFetchQuery(final CronJobModel cjm) {
        checkArgument(cjm.getJob() instanceof MaintenanceCleanupJobModel, "job must be of type MaintenanceCleanupJobModel");

        final MaintenanceCleanupJobModel job = (MaintenanceCleanupJobModel) cjm.getJob();
        return new FlexibleSearchQuery(getQuery(), getParameters(job));
    }

    protected Map<String, Object> getParameters(final MaintenanceCleanupJobModel job) {
        final Map<String, Object> params = new HashMap<>();

        final int daysOld = job.getThreshold() != null && job.getThreshold() > 0 ? job.getThreshold() : DEFAULT_DAYS_OLD_TRESHOLD;
        final Date date = addDays(timeService.getCurrentTime(), -daysOld);
        params.put("date", date);

        return params;
    }

    protected abstract String getQuery();

    @Override
    public void process(final List<T> elements) {
        LOGGER.info("Removing {} entries.", size(elements));
        getModelService().removeAll(elements);
    }

    @Required
    public void setTimeService(final TimeService timeService) {
        this.timeService = timeService;
    }
}