package com.vanmarcke.services.action;

import com.vanmarcke.services.strategies.VMKCommercePaymentCaptureStrategy;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.task.RetryLaterException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import static de.hybris.platform.core.enums.OrderStatus.PAYMENT_CAPTURED;
import static de.hybris.platform.core.enums.OrderStatus.PAYMENT_NOT_CAPTURED;

/**
 * Action which will trigger the event for validating the payment capture status
 */
public class VMKValidatePaymentCaptureStatusWithRetryAction extends AbstractSimpleDecisionAction<OrderProcessModel> {

    private static final Logger LOGGER = LoggerFactory.getLogger(VMKValidatePaymentCaptureStatusWithRetryAction.class);

    private VMKCommercePaymentCaptureStrategy vmkCommercePaymentCaptureStrategy;

    /**
     * {@inheritDoc}
     */
    @Override
    public Transition executeAction(final OrderProcessModel process) throws RetryLaterException {
        final OrderModel order = process.getOrder();

        try {
            if (vmkCommercePaymentCaptureStrategy.isPaymentCapturedForOrder(order)) {
                setOrderStatus(order, PAYMENT_CAPTURED);
                return Transition.OK;
            }
            //try recapture
            PaymentTransactionEntryModel recapturedEntry = vmkCommercePaymentCaptureStrategy.capturePaymentAmount(order);
            if (vmkCommercePaymentCaptureStrategy.isPaymentCapturedForEntry(recapturedEntry)) {
                setOrderStatus(order, PAYMENT_CAPTURED);
                return Transition.OK;
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

        setOrderStatus(order, PAYMENT_NOT_CAPTURED);
        throw new RetryLaterException("Couldn't capture payment, attempting to recapture in 5 minutes");
    }

    @Required
    public void setVmkCommercePaymentCaptureStrategy(VMKCommercePaymentCaptureStrategy vmkCommercePaymentCaptureStrategy) {
        this.vmkCommercePaymentCaptureStrategy = vmkCommercePaymentCaptureStrategy;
    }
}