package com.vanmarcke.services.order.cart.impl;

import com.vanmarcke.core.model.VanMarckeVariantProductModel;
import com.vanmarcke.services.product.VMKVariantProductService;
import de.hybris.platform.commerceservices.order.impl.DefaultCommerceAddToCartStrategy;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

/**
 * Custom implementation for DefaultCommerceAddToCartStrategy
 *
 * @author Niels Raemaekers, Christiaan Janssen
 * @since 19-06-2019
 */
public class VMKCommerceAddToCartStrategyImpl extends DefaultCommerceAddToCartStrategy {

    private final VMKVariantProductService variantProductService;

    /**
     * Creates a new instance of the {@link VMKCommerceAddToCartStrategyImpl} class.
     *
     * @param variantProductService the variant product service
     */
    public VMKCommerceAddToCartStrategyImpl(VMKVariantProductService variantProductService) {
        this.variantProductService = variantProductService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Boolean isProductForCode(CommerceCartParameter parameter) {
        ProductModel product = parameter.getProduct();
        try {
            product = getProductService().getProductForCode(product.getCode());
        } catch (UnknownIdentifierException e) {
            return false;
        }
        return product instanceof VanMarckeVariantProductModel
                && variantProductService.isPurchasable((VanMarckeVariantProductModel) product);
    }
}
