package com.vanmarcke.services.product.price;

import de.hybris.platform.core.model.product.ProductModel;

import java.math.BigDecimal;

/**
 * The {@link VMKNetPriceLookupStrategy} interface defines the methods to lookup pricing for products.
 *
 * @author Joris Cryns, Niels Raemaekers, Christiaan Janssen
 * @since 03-07-2019
 */
public interface VMKNetPriceLookupStrategy {

    /**
     * Resolves the net price for the given product.
     *
     * @param product the product
     * @return the net price
     */
    BigDecimal getNetPriceForProduct(ProductModel product);
}
