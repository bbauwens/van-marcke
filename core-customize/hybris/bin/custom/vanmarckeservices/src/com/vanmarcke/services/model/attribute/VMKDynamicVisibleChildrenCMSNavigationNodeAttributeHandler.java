package com.vanmarcke.services.model.attribute;

import com.vanmarcke.services.strategies.VMKNavigationBuilderStrategy;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.cms2.model.navigation.CMSNavigationNodeModel;
import de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler;
import de.hybris.platform.site.BaseSiteService;

import java.util.List;

/**
 * Handler for the dynamic attribute "visibleChildren" on {@link CMSNavigationNodeModel}
 */
public class VMKDynamicVisibleChildrenCMSNavigationNodeAttributeHandler implements DynamicAttributeHandler<List<CMSNavigationNodeModel>, CMSNavigationNodeModel> {

    private final BaseSiteService baseSiteService;
    private final VMKNavigationBuilderStrategy navigationBuilderStrategy;

    /**
     * Creates a new instance of {@link VMKDynamicVisibleChildrenCMSNavigationNodeAttributeHandler}
     *
     * @param baseSiteService           the baseSiteService
     * @param navigationBuilderStrategy the navigationBuilderStrategy
     */
    public VMKDynamicVisibleChildrenCMSNavigationNodeAttributeHandler(BaseSiteService baseSiteService,
                                                                      VMKNavigationBuilderStrategy navigationBuilderStrategy) {
        this.baseSiteService = baseSiteService;
        this.navigationBuilderStrategy = navigationBuilderStrategy;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<CMSNavigationNodeModel> get(CMSNavigationNodeModel model) {
        final BaseSiteModel currentBaseSite = baseSiteService.getCurrentBaseSite();
        return currentBaseSite == null ? model.getChildren() : navigationBuilderStrategy.buildNavigation(currentBaseSite, model);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void set(CMSNavigationNodeModel model, List<CMSNavigationNodeModel> cmsNavigationNodeModels) {
        model.setChildren(cmsNavigationNodeModels);
    }
}