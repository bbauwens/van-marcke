package com.vanmarcke.services.product.impl;

import com.vanmarcke.core.product.VMKPagedVariantProductDao;
import com.vanmarcke.services.product.VMKProductImageInheritanceService;
import com.vanmarcke.services.strategies.VMKProductImageInheritanceUpdateStrategy;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.servicelayer.data.SearchPageData;
import de.hybris.platform.variants.model.VariantProductModel;
import org.springframework.beans.factory.annotation.Required;

import java.util.Date;

import static de.hybris.platform.servicelayer.search.paginated.util.PaginatedSearchUtils.createSearchPageDataWithPagination;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;
import static org.apache.commons.collections.CollectionUtils.isNotEmpty;

/**
 * Default implementation of the {@link VMKProductImageInheritanceService}
 */
public class VMKProductImageInheritanceServiceImpl implements VMKProductImageInheritanceService {

    private static final int PAGE_SIZE = 1000;

    private VMKPagedVariantProductDao pagedVariantProductDao;
    private VMKProductImageInheritanceUpdateStrategy productImageInheritanceUpdateStrategy;

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateProductImageInheritance(final CatalogVersionModel catalogVersion, final Date date) {
        validateParameterNotNull(catalogVersion, "catalog version cannot be null");
        int currentPage = 0, numberOfPages;
        do {
            final SearchPageData<VariantProductModel> searchPageData = createSearchPageDataWithPagination(PAGE_SIZE, currentPage, true);
            final SearchPageData<VariantProductModel> resultPageData = pagedVariantProductDao.findProductsByCatalogVersionAndDate(catalogVersion, date, searchPageData);

            if (isNotEmpty(resultPageData.getResults())) {
                resultPageData.getResults().stream().forEach(productImageInheritanceUpdateStrategy::updateProductImageInheritanceForProduct);
            }

            currentPage++;
            numberOfPages = resultPageData.getPagination().getNumberOfPages();
        } while (currentPage < numberOfPages);
    }

    /**
     * Sets the pagedVariantProductDao
     *
     * @param pagedVariantProductDao the pagedVariantProductDao
     */
    @Required
    public void setPagedVariantProductDao(VMKPagedVariantProductDao pagedVariantProductDao) {
        this.pagedVariantProductDao = pagedVariantProductDao;
    }

    /**
     * Sets the productImageInheritanceUpdateStrategy
     *
     * @param productImageInheritanceUpdateStrategy the productImageInheritanceUpdateStrategy
     */
    @Required
    public void setProductImageInheritanceUpdateStrategy(VMKProductImageInheritanceUpdateStrategy productImageInheritanceUpdateStrategy) {
        this.productImageInheritanceUpdateStrategy = productImageInheritanceUpdateStrategy;
    }
}