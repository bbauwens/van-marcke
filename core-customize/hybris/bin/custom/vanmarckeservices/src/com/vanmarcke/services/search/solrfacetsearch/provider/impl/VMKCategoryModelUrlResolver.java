package com.vanmarcke.services.search.solrfacetsearch.provider.impl;

import com.vanmarcke.services.search.solrfacetsearch.provider.VMKModelUrlResolver;
import com.vanmarcke.services.util.URIUtils;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.url.impl.DefaultCategoryModelUrlResolver;
import de.hybris.platform.core.model.ItemModel;

import java.util.List;
import java.util.Locale;

/**
 * Value resolver for category models.
 */
public class VMKCategoryModelUrlResolver extends DefaultCategoryModelUrlResolver implements VMKModelUrlResolver<CategoryModel> {

    /**
     * {@inheritDoc}
     */
    @Override
    protected String resolveInternal(final CategoryModel source) {
        return URIUtils.normalize(super.resolveInternal(source));
    }

    /**
     * Overloaded method that resolve the URL depending on a given locale
     *
     * @param source category
     * @param locale locale
     * @return URL that contains localized categories hierarchy
     * e.g /parent-category-name/child-category-name/c/category-code
     */
    public String resolveInternal(final CategoryModel source, Locale locale) {

        String url = getPattern();
        if (url.contains("{baseSite-uid}")) {
            url = url.replace("{baseSite-uid}", urlEncode(getBaseSiteUid().toString()));
        }
        if (url.contains("{category-path}")) {
            final String categoryPath = buildPathString(getCategoryPath(source), locale);
            url = url.replace("{category-path}", categoryPath);
        }
        if (url.contains("{category-code}")) {
            final String categoryCode = urlEncode(source.getCode()).replaceAll("\\+", "%20");
            url = url.replace("{category-code}", categoryCode);
        }
        if (url.contains("{catalog-id}")) {
            url = url.replace("{catalog-id}", urlEncode(source.getCatalogVersion().getCatalog().getId()));
        }
        if (url.contains("{catalogVersion}")) {
            url = url.replace("{catalogVersion}", urlEncode(source.getCatalogVersion().getVersion()));
        }

        return url;
    }

    /**
     * Overloaded method that creates localized path
     *
     * @param path   categories hierarchy
     * @param locale locale
     * @return localized categories hierarchy
     */
    private String buildPathString(final List<CategoryModel> path, Locale locale) {
        final StringBuilder result = new StringBuilder();

        for (int i = 0; i < path.size(); i++) {
            if (i != 0) {
                result.append('/');
            }
            result.append(urlSafe(path.get(i).getName(locale)));
        }

        return result.toString();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean canResolve(ItemModel item) {
        return item instanceof CategoryModel;
    }
}