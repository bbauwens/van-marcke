package com.vanmarcke.services.populators;

import com.google.common.base.Preconditions;
import com.vanmarcke.services.forms.delivery.DeliveryMethodForm;
import com.vanmarcke.services.storelocator.pos.VMKPointOfServiceService;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.commerceservices.delivery.DeliveryService;
import de.hybris.platform.commerceservices.model.PickUpDeliveryModeModel;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.*;
import java.util.stream.Collectors;

import static com.vanmarcke.core.enums.ReferenceConditionalType.MANDATORY;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.joda.time.format.DateTimeFormat.forPattern;

/**
 * Populates a {@link CartModel} with information from a {@link DeliveryMethodForm}.
 *
 * @author Tom van den Berg, Niels Raemaekers
 * @since 2-07-2020
 */
public class VMKDeliveryInformationCartPopulator implements Populator<DeliveryMethodForm, CartModel> {

    private static final String KEY_VALUE_SEPARATOR = "¤";
    private static final String SET_SEPARATOR = "¦";
    private static final String DATE_PATTERN = "yyyy-MM-dd";

    private final VMKPointOfServiceService pointOfServiceService;
    private final DeliveryService deliveryService;

    /**
     * Provides an instance of the {@link VMKDeliveryInformationCartPopulator}.
     *
     * @param pointOfServiceService the point of service service
     * @param deliveryService       the delivery service
     */
    public VMKDeliveryInformationCartPopulator(VMKPointOfServiceService pointOfServiceService, DeliveryService deliveryService) {
        this.pointOfServiceService = pointOfServiceService;
        this.deliveryService = deliveryService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void populate(DeliveryMethodForm source, CartModel target) {
        DeliveryModeModel deliveryMode = deliveryService.getDeliveryModeForCode(source.getDeliveryOption());

        target.setDeliveryDate(forPattern(DATE_PATTERN).withZoneUTC().parseDateTime(source.getActualDate()).toDate());
        target.setDeliveryMode(deliveryMode);
        target.setDeliveryPointOfService(getDeliveryPointOfService(deliveryMode, source.getTecUid()));
        target.setDeliveryComment(source.getDeliveryComment());

        setDeliveryDatesOnOrderEntries(source, target);

        final B2BCustomerModel currentCustomer = (B2BCustomerModel) target.getUser();

        target.setYardReference(source.getYardReference());
        target.setRequestSeparateInvoice(MANDATORY.equals(currentCustomer.getDefaultB2BUnit().getRef1()) || BooleanUtils.isTrue(source.getRequestSeparateInvoice()));
        target.setPurchaseOrderNumber(getCorrectPurchaseOrderNumber(target.getRequestSeparateInvoice(), source.getPurchaseOrderNumber()));

        resetValuesDependingOnDeliveryMethod(target);
    }

    /**
     * Set the delivery dates on the order entries
     *
     * @param source the information from the {@link DeliveryMethodForm}
     * @param target the cart to set the information on
     */
    protected void setDeliveryDatesOnOrderEntries(DeliveryMethodForm source, CartModel target) {
        Preconditions.checkNotNull(source.getProductEntriesDates());
        Map<String, String> productsWithDates = Arrays.stream(source.getProductEntriesDates().split(SET_SEPARATOR))
                .map(s -> s.split(KEY_VALUE_SEPARATOR, 2))
                .collect(Collectors.toMap(a -> a[0], a -> a.length > 1 ? a[1] : StringUtils.EMPTY));

        target.getEntries().forEach(e -> {
            e.setDeliveryDate(getDateFromProductEntriesDates(e, productsWithDates));
        });

        target.setIsSplitted(areRequestedDeliveryDatesDifferent(target));
    }

    private Date getDateFromProductEntriesDates(AbstractOrderEntryModel entry, Map<String, String> productsWithDates) {
        String dateForProduct = productsWithDates.get(entry.getProduct().getCode());
        if (Objects.isNull(dateForProduct)) {
            return null;
        }
        return forPattern(DATE_PATTERN).withZoneUTC().parseDateTime(dateForProduct).toDate();
    }

    /**
     * Get the correct purchase order number
     *
     * @param requestSeparateInvoice check to see if the customer requested a separate invoice
     * @param purchaseOrderNumber    the purchaseOrderNumber from the form
     * @return the correct purchaseOrderNumber
     */
    private String getCorrectPurchaseOrderNumber(Boolean requestSeparateInvoice, String purchaseOrderNumber) {
        return requestSeparateInvoice && StringUtils.isBlank(purchaseOrderNumber) ? "OUTIL" : purchaseOrderNumber;
    }

    /**
     * Retrieves the delivery point of service from the given {@link DeliveryMethodForm}.
     *
     * @param deliveryModeModel the delivery mode model
     * @param tecUid            the tec uid
     * @return the point of service model
     */
    protected PointOfServiceModel getDeliveryPointOfService(DeliveryModeModel deliveryModeModel, String tecUid) {
        if (deliveryModeModel instanceof PickUpDeliveryModeModel) {
            if (isNotBlank(tecUid)) {
                return pointOfServiceService.getPointOfServiceForName(tecUid);
            } else {
                return pointOfServiceService.getCurrentPointOfService();
            }
        }
        return null;
    }

    /**
     * Reset values depending on which delivery method is selected
     *
     * @param cart the cart
     */
    protected void resetValuesDependingOnDeliveryMethod(CartModel cart) {
        if (cart.getDeliveryMode() instanceof PickUpDeliveryModeModel) {
            cart.setDeliveryComment(StringUtils.EMPTY);
        }
    }

    /**
     * Check to see if the requested delivery dates on the entries are all the same date
     *
     * @param cart the cart
     * @return the decision
     */
    protected boolean areRequestedDeliveryDatesDifferent(CartModel cart) {
        List<Date> requestedDates = cart.getEntries().stream()
                .map(AbstractOrderEntryModel::getDeliveryDate)
                .distinct()
                .collect(Collectors.toList());
        return requestedDates.size() > 1;
    }
}
