package com.vanmarcke.services.job;

import be.elision.mandrillextension.service.MandrillService;
import com.vanmarcke.core.model.VMKSendFailedOrdersNotificationCronJobModel;
import com.vanmarcke.core.order.VMKOrderDao;
import com.vanmarcke.email.converters.impl.MandrillFailedOrdersConverter;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Job for sending the failed orders notification
 *
 * @author Niels Raemaekers
 * @since 19-05-2020
 */
public class VMKSendFailedOrdersNotificationJobPerformable extends AbstractJobPerformable<VMKSendFailedOrdersNotificationCronJobModel> {

    private static final Logger LOGGER = LoggerFactory.getLogger(VMKSendFailedOrdersNotificationJobPerformable.class);

    private final VMKOrderDao orderDao;
    private final MandrillService mandrillService;
    private final MandrillFailedOrdersConverter mandrillFailedOrdersConverter;
    private final String templateName;
    private final String emailAddress;

    /**
     * Constructor for {@link VMKSendFailedOrdersNotificationJobPerformable}
     *
     * @param orderDao                      the {@link VMKOrderDao}
     * @param mandrillService               the {@link MandrillService}
     * @param mandrillFailedOrdersConverter the {@link MandrillFailedOrdersConverter}
     * @param templateName                  the template name to use
     * @param emailAddress                  the email address to send to
     */
    public VMKSendFailedOrdersNotificationJobPerformable(VMKOrderDao orderDao, MandrillService mandrillService, MandrillFailedOrdersConverter mandrillFailedOrdersConverter, String templateName, String emailAddress) {
        this.orderDao = orderDao;
        this.mandrillService = mandrillService;
        this.mandrillFailedOrdersConverter = mandrillFailedOrdersConverter;
        this.templateName = templateName;
        this.emailAddress = emailAddress;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PerformResult perform(final VMKSendFailedOrdersNotificationCronJobModel cronJob) {
        List<OrderModel> failedOrders = orderDao.findFailedOrders();

        if (CollectionUtils.isNotEmpty(failedOrders)) {
            try {
                mandrillService.send(templateName, failedOrders, mandrillFailedOrdersConverter, emailAddress);
            } catch (Exception ex) {
                LOGGER.error(ex.getMessage(), ex);
                return new PerformResult(CronJobResult.ERROR, CronJobStatus.ABORTED);
            }
        }

        return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
    }
}