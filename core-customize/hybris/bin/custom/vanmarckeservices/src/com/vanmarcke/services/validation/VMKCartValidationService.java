package com.vanmarcke.services.validation;

import com.vanmarcke.services.MessageData;
import de.hybris.platform.core.model.order.CartModel;

/**
 * The {@code VMKCartValidationService} interface defines the methods that can be used to validate the shopping cart.
 *
 * @author Niels Raemaekers, Christiaan Janssen
 * @since 03-02-2020
 */
public interface VMKCartValidationService {

    /**
     * Checks whether the given {@code cart} is valid.
     * <p>
     * Returns an validation message code in case the cart is invalid or {@code null} otherwise.
     *
     * @param cart the shopping cart
     * @return the validation message code
     */
    MessageData isCartValid(CartModel cart);
}
