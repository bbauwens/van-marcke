package com.vanmarcke.services.strategies.impl;

import com.vanmarcke.core.model.CustomerInteractionModel;
import com.vanmarcke.services.event.events.SubmitCustomerInteractionEvent;
import com.vanmarcke.services.strategies.VMKSendCustomerInteractionEmailStrategy;
import de.hybris.platform.servicelayer.event.EventService;

/**
 * Implementation for VMKSendCustomerInteractionEmailStrategy.
 *
 * @author Niels Raemaekers
 * @since 18-12-2019
 */
public class VMKSendCustomerInteractionEmailStrategyImpl implements VMKSendCustomerInteractionEmailStrategy {

    private final EventService eventService;

    /**
     * Provides an instance of the DefaultVMKSendCustomerInteractionEmailStrategy.
     *
     * @param eventService the event service
     */
    public VMKSendCustomerInteractionEmailStrategyImpl(EventService eventService) {
        this.eventService = eventService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void sendCustomerInteractionForOrder(CustomerInteractionModel customerInteraction) {
        eventService.publishEvent(new SubmitCustomerInteractionEvent(customerInteraction));
    }
}
