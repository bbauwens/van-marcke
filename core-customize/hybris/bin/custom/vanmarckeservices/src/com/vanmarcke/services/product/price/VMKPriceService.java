package com.vanmarcke.services.product.price;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.commercefacades.product.data.PriceCalculationData;
import de.hybris.platform.variants.model.VariantProductModel;

import java.math.BigDecimal;

/**
 * The {@code BluePriceService} interface defines the methods to retrieve pricing information.
 *
 * @author Joris Cryns, Niels Raemaekers, Taki Korovessis, Christiaan Janssen
 * @since 27-06-2019
 */
public interface VMKPriceService {

    /**
     * Returns the price for the given {@code variant} and {@code b2bUnit}.
     *
     * @param variant the product variant
     * @param b2bUnitUid the B2B unit uid
     * @return the price data
     */
    PriceCalculationData getPrice(VariantProductModel variant, String b2bUnitUid);
}
