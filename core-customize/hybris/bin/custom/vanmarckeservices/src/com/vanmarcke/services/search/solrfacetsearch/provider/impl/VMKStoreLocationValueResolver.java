package com.vanmarcke.services.search.solrfacetsearch.provider.impl;

import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolver;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

/**
 * ValueResolver to index the latitude and longitude of the pointOfService as "latitude,longitude"
 */
public class VMKStoreLocationValueResolver extends AbstractValueResolver<PointOfServiceModel, Object, Object> {

    /**
     * {@inheritDoc}
     */
    @Override
    protected void addFieldValues(InputDocument document, IndexerBatchContext batchContext,
                                  IndexedProperty indexedProperty, PointOfServiceModel model,
                                  ValueResolverContext<Object, Object> resolverContext) throws FieldValueProviderException {
        Double latitude = model.getLatitude() == null ? 0 : model.getLatitude();
        Double longitude = model.getLongitude() == null ? 0 : model.getLongitude();

        String position = String.format("%s,%s", latitude, longitude);
        super.filterAndAddFieldValues(document, batchContext, indexedProperty, position, resolverContext.getFieldQualifier());

    }

}