package com.vanmarcke.services.b2b;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

/**
 * Interface for Users coming through SSO
 *
 * @author Niels Raemaekers
 * @since 23-06-2021
 */
public interface VMKSSOUserService {

    /**
     * Update the user's session store if it is not yet perstisted
     *
     * @param user the user
     * @return
     */
    void updateUserSessionStoreIfNotPresent(UserModel user);

    /**
     * Returns the nearest point of service for the given {@code user}.
     *
     * @param user the user
     * @return the point of service
     */
    PointOfServiceModel getNearestStore(B2BCustomerModel user);

    /**
     * Returns the nearest point of service for the given {@code user} and {@code baseStore}.
     *
     * @param user      the user
     * @param baseStore the base store
     * @return the point of service
     */
    PointOfServiceModel getNearestStore(B2BCustomerModel user, BaseStoreModel baseStore);
}
