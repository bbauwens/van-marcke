package com.vanmarcke.services.cronjob;

import com.vanmarcke.core.constants.VanmarckeCoreConstants;
import com.vanmarcke.core.model.ProductExportCronJobModel;
import com.vanmarcke.services.ftp.FTPClient;
import com.vanmarcke.services.product.VMKNonArchivedVariantsExportService;
import com.vanmarcke.services.util.VMKExportUtils;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Job that will trigger an export of the non-archived variants
 *
 * @author Niels Raemaekers
 * @since 12-05-2020
 */
public class VMKNonArchivedVariantsExportJobPerformable extends AbstractJobPerformable<ProductExportCronJobModel> {

    private static final Logger LOG = LoggerFactory.getLogger(VMKNonArchivedVariantsExportJobPerformable.class);
    private static final String HEADER_ROW = "Variant;Baseproduct;Category ID;category Name NL;Etim Class ID;Etim Class Name NL;GS1 Brick ID;GS1 Brick Name;De-averaging";

    private final VMKNonArchivedVariantsExportService vmkNonArchivedVariantsExportService;
    private final FTPClient ftpClient;

    /**
     * Constructor for {@link VMKNonArchivedVariantsExportJobPerformable}
     *
     * @param vmkNonArchivedVariantsExportService the {@link VMKNonArchivedVariantsExportService}
     * @param ftpClient
     */
    public VMKNonArchivedVariantsExportJobPerformable(VMKNonArchivedVariantsExportService vmkNonArchivedVariantsExportService, FTPClient ftpClient) {
        this.vmkNonArchivedVariantsExportService = vmkNonArchivedVariantsExportService;
        this.ftpClient = ftpClient;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PerformResult perform(ProductExportCronJobModel job) {
        List<String> exportLines = vmkNonArchivedVariantsExportService.generateNonArchivedVariantsExportFeed(job.getCatalogVersion());

        if (CollectionUtils.isNotEmpty(exportLines)) {
            exportLines.add(0, HEADER_ROW);

            if (send(exportLines)) {
                LOG.info("Successfully wrote {} variants to export file", exportLines.size());
                return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
            }
            return new PerformResult(CronJobResult.ERROR, CronJobStatus.ABORTED);
        }

        LOG.info("No export file created since there were no variants found.", exportLines.size());
        return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
    }

    /**
     * Send the file to the dedicated location
     *
     * @param exportLines the exportlines to send
     * @return verification if the sending worked
     */
    private boolean send(final List<String> exportLines) {
        final InputStream inputStream = convertFeedToInputStream(exportLines);
        if (inputStream != null) {
            try {
                ftpClient.send(inputStream, VMKExportUtils.getFormattedFileName(VanmarckeCoreConstants.Export.EXPORT_NON_ARCHIVED_FILE_NAME));
                return true;
            } catch (IOException ex) {
                LOG.error("Unable to write to FTP location with cause: ", ex);
            }
        }
        return false;
    }

    /**
     * Convert the Feed to an InputStream
     *
     * @param exportLines the feed
     * @return the inputStream
     */
    private InputStream convertFeedToInputStream(List<String> exportLines) {
        byte[] bytes = exportLines.stream()
                .collect(Collectors.joining("\n"))
                .getBytes();
        return new ByteArrayInputStream(bytes);
    }
}
