package com.vanmarcke.services.request.impl;

import com.vanmarcke.services.request.VMKYRequestKey;
import com.vanmarcke.services.request.VMKYRequestService;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Function;

/**
 * Service used to store additional attributes during processing requests. Always single thread / request specific and will be alive only during current logic execution.
 */
public class VMKYRequestServiceImpl implements VMKYRequestService {
    private final Map<VMKYRequestKey, Object> map = new HashMap<>();
    private final ReentrantLock lock = new ReentrantLock();

    @Override
    public <T> T getAttribute(VMKYRequestKey key) {
        return (T) map.get(key);
    }

    @Override
    public void setAttribute(VMKYRequestKey key, Object value) {
        map.put(key, value);
    }

    @Override
    public void removeAttribute(VMKYRequestKey key) {
        map.remove(key);
    }

    @Override
    public <T> T computeIfAbsent(VMKYRequestKey key, Function<VMKYRequestKey, T> function) {
        if (lock.isLocked()) {
            // map modification is not possible/not allowed in recursive call
            return function.apply(key);
        } else {
            try {
                lock.lock();
                return (T) map.computeIfAbsent(key, function);
            } finally {
                lock.unlock();
            }
        }
    }
}
