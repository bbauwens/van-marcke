package com.vanmarcke.services.product.price.impl;

import com.vanmarcke.services.b2b.VMKB2BUnitService;
import com.vanmarcke.services.product.price.VMKNetPriceLookupStrategy;
import com.vanmarcke.services.product.price.VMKPriceService;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.commercefacades.product.data.PriceCalculationData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.variants.model.VariantProductModel;

import java.math.BigDecimal;

/**
 * The {@link VMKNetPriceLookupStrategyImpl} class implements the business logic to lookup pricing for products.
 *
 * @author Joris Cryns, Niels Raemaekers, Christiaan Janssen
 * @since 03-07-2019
 */
public class VMKNetPriceLookupStrategyImpl implements VMKNetPriceLookupStrategy {

    private final VMKB2BUnitService vmkB2BUnitService;
    private final VMKPriceService vmkPriceService;

    /**
     * Creates a new instance of the {@link VMKNetPriceLookupStrategyImpl} class.
     *
     * @param vmkB2BUnitService the B2B unit service
     * @param vmkPriceService   the price service
     */
    public VMKNetPriceLookupStrategyImpl(VMKB2BUnitService vmkB2BUnitService, VMKPriceService vmkPriceService) {
        this.vmkB2BUnitService = vmkB2BUnitService;
        this.vmkPriceService = vmkPriceService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BigDecimal getNetPriceForProduct(ProductModel product) {
        if (product instanceof VariantProductModel) {
            VariantProductModel variant = (VariantProductModel) product;
            B2BUnitModel b2bUnit = vmkB2BUnitService.getB2BUnitModelForCurrentUser();
            if (b2bUnit != null) {
                PriceCalculationData priceCalculationData = vmkPriceService.getPrice(variant, b2bUnit.getUid());
                return priceCalculationData != null ? priceCalculationData.getPriceValue() : null;
            }
        }
        return null;
    }
}
