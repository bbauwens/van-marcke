package com.vanmarcke.services.cronjob;

import com.vanmarcke.core.model.ProductClassificationExportCronJobModel;
import com.vanmarcke.facades.data.ProductExportFeedResult;
import com.vanmarcke.services.ftp.FTPClient;
import com.vanmarcke.services.product.VMKProductClassificationExportService;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import static org.apache.commons.io.IOUtils.toInputStream;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;

/**
 * Job for exporting product classifications.
 *
 * @author Joris Cryns
 * @since 23-8-2019
 */
public class VMKProductClassificationExportJobPerformable extends AbstractJobPerformable<ProductClassificationExportCronJobModel> {

    private static final Logger LOG = LoggerFactory.getLogger(VMKProductClassificationExportJobPerformable.class);

    private VMKProductClassificationExportService productClassificationExportService;
    private FTPClient ftpClient;

    /**
     * {@inheritDoc}
     */
    @Override
    public PerformResult perform(final ProductClassificationExportCronJobModel cronJob) {
        try {
            final ProductExportFeedResult feedResult = productClassificationExportService.generateProductClassificationFeed(cronJob.getCatalogVersion(), cronJob.getSystemVersion(), cronJob.getLastModifiedTime());
            final String feedData = feedResult.getFeed();
            if (isNotEmpty(feedData)) {
                ftpClient.send(toInputStream(feedData), cronJob.getFeedName());
                if (feedResult.getLastModifiedTime() != null) {
                    cronJob.setLastModifiedTime(feedResult.getLastModifiedTime());
                    super.modelService.save(cronJob);
                }
            }
            return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
        } catch (final Exception e) {
            LOG.error("Exception occurred during the generation of the product classification feed", e);
            return new PerformResult(CronJobResult.ERROR, CronJobStatus.ABORTED);
        }
    }

    @Required
    public void setProductClassificationExportService(final VMKProductClassificationExportService productClassificationExportService) {
        this.productClassificationExportService = productClassificationExportService;
    }

    @Required
    public void setFtpClient(final FTPClient ftpClient) {
        this.ftpClient = ftpClient;
    }
}