package com.vanmarcke.services.order.hook.impl;

import com.vanmarcke.services.address.VMKPreferredDeliveryAddressLookupStrategy;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.model.ModelService;

/**
 * A hook that adds the delivery address to the cart if they are not present
 */
public class VMKPreferredDeliveryAddressCartCalculationMethodHookImpl extends AbstractVMKCartCalculationMethodHook {

    private final VMKPreferredDeliveryAddressLookupStrategy bluePreferredDeliveryAddressLookupStrategy;

    public VMKPreferredDeliveryAddressCartCalculationMethodHookImpl(final ModelService modelService, final VMKPreferredDeliveryAddressLookupStrategy bluePreferredDeliveryAddressLookupStrategy) {
        super(modelService);
        this.bluePreferredDeliveryAddressLookupStrategy = bluePreferredDeliveryAddressLookupStrategy;
    }

    @Override
    public void afterCalculate(final CommerceCartParameter parameter) {
        // No need to do anything after calculating
    }

    @Override
    public void beforeCalculate(final CommerceCartParameter parameter) {
        final CartModel cart = parameter.getCart();
        if (isB2BCart(cart) && cart.getDeliveryAddress() == null) {
            final AddressModel deliveryAddress = this.bluePreferredDeliveryAddressLookupStrategy.getPreferredDeliveryAddressForOrder(cart);
            if (deliveryAddress != null) {
                cart.setDeliveryAddress(deliveryAddress);
                getModelService().save(cart);
            }
        }
    }
}