package com.vanmarcke.services.search.solrfacetsearch.provider.impl;

import com.vanmarcke.services.storelocator.schedule.VMKOpeningScheduleService;
import de.hybris.platform.basecommerce.enums.WeekDay;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolver;
import de.hybris.platform.storelocator.model.OpeningDayModel;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import java.util.Date;

/**
 * ValueResolver to index the opening times from pointOfService's opening schedule
 */
public class VMKOpeningClosingTimeValueResolver extends AbstractValueResolver<PointOfServiceModel, Object, Object> {

    private static final String OPENING = "openingTime";
    private static final String CLOSING = "closingTime";

    private ModelService modelService;
    private VMKOpeningScheduleService openingScheduleService;

    /**
     * {@inheritDoc}
     */
    @Override
    protected void addFieldValues(InputDocument document, IndexerBatchContext batchContext,
                                  IndexedProperty indexedProperty, PointOfServiceModel model,
                                  ValueResolverContext<Object, Object> resolverContext) throws FieldValueProviderException {
        String name = indexedProperty.getName();

        Assert.isTrue(StringUtils.equals(OPENING, name) || StringUtils.equals(CLOSING, name), name + " is not a valid opening or closing value for " + model.getDescription());

        for (WeekDay weekDay : WeekDay.values()) {
            OpeningDayModel openingDayModel = openingScheduleService.getOpeningDayFromScheduleForGivenDay(model.getOpeningSchedule(), weekDay);
            if (openingDayModel != null) {
                Date attributeValue = modelService.getAttributeValue(openingDayModel, name);
                if (attributeValue != null) {
                    super.filterAndAddFieldValues(document, batchContext, indexedProperty, attributeValue, weekDay.getCode());
                }
            }
        }
    }

    /**
     * Injects VanmarckeOpeningScheduleService
     *
     * @param openingScheduleService
     */
    @Required
    public void setOpeningScheduleService(VMKOpeningScheduleService openingScheduleService) {
        this.openingScheduleService = openingScheduleService;
    }

    /**
     * Injects ModelService
     *
     * @param modelService
     */
    @Required
    public void setModelService(ModelService modelService) {
        this.modelService = modelService;
    }
}