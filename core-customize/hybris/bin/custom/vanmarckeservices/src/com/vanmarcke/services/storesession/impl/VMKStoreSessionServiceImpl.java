package com.vanmarcke.services.storesession.impl;

import com.vanmarcke.services.storelocator.pos.VMKPointOfServiceService;
import com.vanmarcke.services.storesession.VMKStoreSessionService;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.storesession.impl.DefaultStoreSessionService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import org.springframework.beans.factory.annotation.Required;

/**
 * Custom implementation for DefaultStoreSessionService
 */
public class VMKStoreSessionServiceImpl extends DefaultStoreSessionService implements VMKStoreSessionService {

    private VMKPointOfServiceService pointOfServiceService;
    private BaseSiteService baseSiteService;

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCurrentSite(final String uid) {
        final BaseSiteModel requestedBaseSite = baseSiteService.getBaseSiteForUID(uid);
        if (requestedBaseSite != null) {
            baseSiteService.setCurrentBaseSite(requestedBaseSite, true);
            setCurrentLanguage(requestedBaseSite.getDefaultLanguage().getIsocode());
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCurrentStore(final String uid) {
        final PointOfServiceModel requestedPointOfService = this.pointOfServiceService.getPointOfServiceForName(uid);
        if (requestedPointOfService != null) {
            pointOfServiceService.setCurrentPointOfService(requestedPointOfService);
        }
    }

    @Required
    public void setBaseSiteService(final BaseSiteService baseSiteService) {
        this.baseSiteService = baseSiteService;
    }

    @Required
    public void setPointOfServiceService(final VMKPointOfServiceService pointOfServiceService) {
        this.pointOfServiceService = pointOfServiceService;
    }

}