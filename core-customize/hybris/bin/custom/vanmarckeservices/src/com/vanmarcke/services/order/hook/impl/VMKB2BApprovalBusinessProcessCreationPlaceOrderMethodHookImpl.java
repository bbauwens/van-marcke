package com.vanmarcke.services.order.hook.impl;

import de.hybris.platform.commerceservices.order.hook.CommercePlaceOrderMethodHook;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.commerceservices.service.data.CommerceOrderResult;

/**
 * Disable the default SAP Commerce {@link de.hybris.platform.b2b.order.hooks.B2BApprovalBusinessProcessCreationPlaceOrderMethodHook}.
 */
public class VMKB2BApprovalBusinessProcessCreationPlaceOrderMethodHookImpl implements CommercePlaceOrderMethodHook {

    @Override
    public void afterPlaceOrder(final CommerceCheckoutParameter parameter, final CommerceOrderResult orderModel) {
        // disable B2B approval business process
    }

    @Override
    public void beforePlaceOrder(final CommerceCheckoutParameter parameter) {
        // disable B2B approval business process
    }

    @Override
    public void beforeSubmitOrder(final CommerceCheckoutParameter parameter, final CommerceOrderResult result) {
        // disable B2B approval business process
    }
}