package com.vanmarcke.services.search.solrfacetsearch.provider.impl;

import de.hybris.platform.commerceservices.url.UrlResolver;
import de.hybris.platform.commerceservices.url.impl.AbstractUrlResolver;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolver;
import de.hybris.platform.variants.model.VariantProductModel;
import org.springframework.beans.factory.annotation.Required;

/**
 * Custom Value Resolver for the url of the BaseProduct
 */
public class VMKBaseProductUrlValueResolver extends AbstractValueResolver<ProductModel, Object, Object> {

    private UrlResolver<ProductModel> urlResolver;

    /**
     * {@inheritDoc}
     */
    @Override
    protected void addFieldValues(InputDocument inputDocument,
                                  IndexerBatchContext indexerBatchContext,
                                  IndexedProperty indexedProperty,
                                  ProductModel productModel,
                                  ValueResolverContext<Object, Object> valueResolverContext) throws FieldValueProviderException {

        ProductModel baseProduct = productModel;
        if (productModel instanceof VariantProductModel) {
            baseProduct = ((VariantProductModel) productModel).getBaseProduct();
        }

        if (baseProduct != null) {
            inputDocument.addField(indexedProperty, urlResolver.resolve(baseProduct), valueResolverContext.getFieldQualifier());
        }
    }

    @Required
    public void setUrlResolver(AbstractUrlResolver<ProductModel> urlResolver) {
        this.urlResolver = urlResolver;
    }
}
