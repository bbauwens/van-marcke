package com.vanmarcke.services.odata.model;

import com.vanmarcke.services.constants.VanmarckeservicesConstants.OData;
import de.hybris.platform.integrationservices.model.AttributeValueGetter;
import de.hybris.platform.integrationservices.model.TypeAttributeDescriptor;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Locale;
import java.util.Map;

public class VMKAttributeValueGetter implements AttributeValueGetter {
    private final AttributeValueGetter delegateAttributeValueGetter;
    private final TypeAttributeDescriptor attributeDescriptor;
    private final ModelService modelService;

    public VMKAttributeValueGetter(AttributeValueGetter delegateAttributeValueGetter, TypeAttributeDescriptor attributeDescriptor, ModelService modelService) {
        this.delegateAttributeValueGetter = delegateAttributeValueGetter;
        this.attributeDescriptor = attributeDescriptor;
        this.modelService = modelService;
    }

    @Override
    public Object getValue(final Object model) {
        if (model != null && attributeDescriptor.isReadable() && attributeDescriptor.getQualifier().equalsIgnoreCase(OData.ITEMTYPE_QUALIFIER)) {
            return modelService.getAttributeValue(model, OData.ITEM_COMPOSED_TYPE_QUALIFIER);
        } else {
            return delegateAttributeValueGetter.getValue(model);
        }
    }

    @Override
    public Object getValue(final Object model, final Locale locale) {
        return delegateAttributeValueGetter.getValue(model, locale);
    }

    @Override
    public Map<Locale, Object> getValues(final Object model, final Locale... locales) {
        return delegateAttributeValueGetter.getValues(model, locales);
    }
}
