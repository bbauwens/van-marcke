package com.vanmarcke.services.search.solrfacetsearch.populators;

import com.vanmarcke.core.store.VMKBaseStoreDao;
import de.hybris.platform.catalog.model.CatalogModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.catalog.model.classification.ClassificationSystemVersionModel;
import de.hybris.platform.core.PK;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.converters.populator.DefaultIndexedTypePopulator;
import de.hybris.platform.solrfacetsearch.model.config.SolrIndexedTypeModel;
import de.hybris.platform.store.BaseStoreModel;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Extends default implementation {@link DefaultIndexedTypePopulator}
 *
 * @author Przemyslaw Mirowski
 * @since 26-11-2021
 */
public class VMKIndexedTypePopulator extends DefaultIndexedTypePopulator {

    private final VMKBaseStoreDao baseStoreDao;

    public VMKIndexedTypePopulator(VMKBaseStoreDao baseStoreDao) {
        this.baseStoreDao = baseStoreDao;
    }

    @Override
    public void populate(final SolrIndexedTypeModel source, final IndexedType target) {
        super.populate(source, target);

        final Map<Boolean, List<IndexedProperty>> facetIndexedPropertiesByClassificationStatus = target.getIndexedProperties().values().stream()
                .filter(IndexedProperty::isFacet)
                .collect(Collectors.partitioningBy(indexedProperty -> indexedProperty.getClassAttributeAssignment() != null));

        // if there are facets with classAttributeAssignment then group those by classification catalogs assigned to base stores
        if (facetIndexedPropertiesByClassificationStatus.containsKey(true)) {
            final Map<String, Set<String>> classificationSystemVersionPkByBaseStoreUid = baseStoreDao.findAllStoresWithSolrFacetSearchConfig().stream()
                    .collect(Collectors.toMap(BaseStoreModel::getUid, baseStore -> baseStore.getCatalogs().stream()
                            .map(CatalogModel::getActiveCatalogVersion)
                            .filter(ClassificationSystemVersionModel.class::isInstance)
                            .map(CatalogVersionModel::getPk)
                            .map(PK::toString)
                            .collect(Collectors.toSet())));

            final Map<String, List<IndexedProperty>> facetIndexedPropertiesByClassSystemVersionPK = facetIndexedPropertiesByClassificationStatus.get(true).stream()
                    .filter(IndexedProperty::isFacet)
                    .filter(indexedProperty -> indexedProperty.getClassAttributeAssignment() != null)
                    .collect(Collectors.groupingBy(indexedProperty -> indexedProperty.getClassAttributeAssignment().getSystemVersion().getCatalog().getActiveCatalogVersion().getPk().toString()));

            final Map<String, List<IndexedProperty>> facetIndexedPropertiesByBaseStoreUid = classificationSystemVersionPkByBaseStoreUid.entrySet().stream()
                    .collect(Collectors.toMap(Map.Entry::getKey, entry -> entry.getValue().stream()
                            .filter(facetIndexedPropertiesByClassSystemVersionPK::containsKey)
                            .map(facetIndexedPropertiesByClassSystemVersionPK::get)
                            .flatMap(List::stream)
                            .collect(Collectors.toList())));

            if (facetIndexedPropertiesByClassificationStatus.containsKey(false)) {
                final List<IndexedProperty> facetIndexedProperties = facetIndexedPropertiesByClassificationStatus.get(false);
                facetIndexedPropertiesByBaseStoreUid.values().forEach(facetsInStore -> facetsInStore.addAll(facetIndexedProperties));
            }
            target.setFacetsByBaseStore(facetIndexedPropertiesByBaseStoreUid);
        }
    }
}
