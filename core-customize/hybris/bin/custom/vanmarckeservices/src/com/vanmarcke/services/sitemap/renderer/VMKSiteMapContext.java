package com.vanmarcke.services.sitemap.renderer;

import de.hybris.platform.acceleratorservices.enums.SiteMapPageEnum;
import de.hybris.platform.acceleratorservices.sitemap.renderer.SiteMapContext;
import de.hybris.platform.cms2.model.site.CMSSiteModel;

/**
 * Extended from {@link SiteMapContext}, only one small change is done in the init method
 */
public class VMKSiteMapContext extends SiteMapContext {

    public static final String BASE_URL = "baseUrl";
    public static final String MEDIA_URL = "mediaUrl";

    /**
     * Only change here is setting the "secure" boolean for the getWebsiteUrlForSite() method to true so we process the jobs with https instead of http!
     */
    @Override
    public void init(final CMSSiteModel site, final SiteMapPageEnum siteMapPageEnum) {
        super.init(site, siteMapPageEnum);
        final String currentUrlEncodingPattern = getUrlEncoderService().getCurrentUrlEncodingPattern();
        this.put(BASE_URL, getSiteBaseUrlResolutionService().getWebsiteUrlForSite(site, currentUrlEncodingPattern, true, ""));
        this.put(MEDIA_URL, getSiteBaseUrlResolutionService().getMediaUrlForSite(site, true, ""));
    }
}
