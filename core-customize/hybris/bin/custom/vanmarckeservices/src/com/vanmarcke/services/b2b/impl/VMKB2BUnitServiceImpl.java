package com.vanmarcke.services.b2b.impl;

import com.vanmarcke.core.enums.ReferenceConditionalType;
import com.vanmarcke.services.b2b.VMKB2BUnitService;
import com.vanmarcke.services.basestore.VMKBaseStoreService;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2b.services.impl.DefaultB2BUnitService;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.core.model.enumeration.EnumerationValueModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import org.springframework.beans.factory.annotation.Required;

import java.util.Objects;

/**
 * Default implementation for BlueB2BUnitService
 *
 * @author Joris Cryns, Niels Raemaekers, Tom Martens, Tom van den Berg
 * @since 02-07-2019
 */
public class VMKB2BUnitServiceImpl extends DefaultB2BUnitService implements VMKB2BUnitService {

    private CMSSiteService cmsSiteService;
    private VMKBaseStoreService baseStoreService;

    /**
     * {@inheritDoc}
     */
    @Override
    public B2BUnitModel getB2BUnitModelForCurrentUser() {
        B2BUnitModel b2BUnitModel = null;
        final UserModel userModel = getUserService().getCurrentUser();
        if (userModel instanceof B2BCustomerModel) {
            b2BUnitModel = getParent((B2BCustomerModel) userModel);
        }
        return b2BUnitModel;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCustomerB2BUnitID() {
        B2BUnitModel b2BUnit = getB2BUnitModelForCurrentUser();
        if (Objects.nonNull(b2BUnit)) {
            return b2BUnit.getUid();
        }

        return getFallbackCustomerId();
    }

    @Override
    public String getCustomerB2BUnitID(UserModel user) {
        B2BUnitModel b2BUnitModel = null;
        if (user instanceof B2BCustomerModel) {
            b2BUnitModel = getParent((B2BCustomerModel) user);
        }

        return b2BUnitModel != null ? b2BUnitModel.getUid() : getCustomerB2BUnitID();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected EnumerationValueModel lookupPriceGroupFromClosestParent(final B2BUnitModel unitOfCustomer) {
        final CMSSiteModel cmsSiteModel = cmsSiteService.getCurrentSite();
        if (cmsSiteModel != null && cmsSiteModel.getCommerceGroup() != null && cmsSiteModel.getCommerceGroup().getUserPriceGroup() != null) {
            return getTypeService().getEnumerationValue(cmsSiteModel.getCommerceGroup().getUserPriceGroup());
        }
        return super.lookupPriceGroupFromClosestParent(unitOfCustomer);
    }

    /**
     * return fallback customer id for current base store
     */
    public String getFallbackCustomerId() {
        return baseStoreService.getCurrentBaseStore().getIbmConfiguration() != null ?
                baseStoreService.getCurrentBaseStore().getIbmConfiguration().getFallbackCustomerId() : "V99909";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setDefaultDeliveryAddressEntry(final B2BUnitModel b2BUnit, final AddressModel addressModel) {
        if (b2BUnit.getAddresses().contains(addressModel)) {
            b2BUnit.setShippingAddress(addressModel);
            getModelService().save(b2BUnit);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean mustAddReference() {
        return cmsSiteService.getCurrentSite().getChannel() != SiteChannel.DIY && ReferenceConditionalType.MANDATORY.equals(getB2BUnitModelForCurrentUser().getRef2());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean mandatorySeparateInvoiceReference() {
        return ReferenceConditionalType.MANDATORY.equals(getB2BUnitModelForCurrentUser().getRef1());
    }

    @Required
    public void setCmsSiteService(final CMSSiteService cmsSiteService) {
        this.cmsSiteService = cmsSiteService;
    }

    public void setBaseStoreService(VMKBaseStoreService baseStoreService) {
        this.baseStoreService = baseStoreService;
    }
}
