package com.vanmarcke.services.search.solrfacetsearch.impl;

import com.vanmarcke.services.search.solrfacetsearch.VMKSearchService;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.search.FacetSearchException;
import de.hybris.platform.solrfacetsearch.search.FacetSearchService;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import de.hybris.platform.solrfacetsearch.search.SearchResult;
import org.springframework.beans.factory.annotation.Required;

/**
 * Default implementation for {@link VMKSearchService}
 */
public class VMKSearchServiceImpl implements VMKSearchService {

    private FacetSearchService facetSearchService;
    private I18NService i18NService;

    /**
     * {@inheritDoc}
     */
    @Override
    public SearchResult search(SearchQuery searchQuery) throws FacetSearchException {
        return facetSearchService.search(searchQuery);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SearchQuery createTextSearchQuery(String term, FacetSearchConfig facetSearchConfig, IndexedType indexedType) {
        SearchQuery searchQuery = facetSearchService.createFreeTextSearchQuery(facetSearchConfig, indexedType, term);
        searchQuery.setLanguage(i18NService.getCurrentLocale().toString());
        return searchQuery;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SearchQuery createSearchQuery(FacetSearchConfig facetSearchConfig, IndexedType indexedType) {
        SearchQuery searchQuery = facetSearchService.createSearchQuery(facetSearchConfig, indexedType);
        searchQuery.setLanguage(i18NService.getCurrentLocale().toString());
        return searchQuery;
    }

    @Required
    public void setFacetSearchService(FacetSearchService facetSearchService) {
        this.facetSearchService = facetSearchService;
    }

    @Required
    public void setI18NService(I18NService i18NService) {
        this.i18NService = i18NService;
    }
}