package com.vanmarcke.services.storelocator.schedule;

import de.hybris.platform.basecommerce.enums.WeekDay;
import de.hybris.platform.storelocator.model.OpeningDayModel;
import de.hybris.platform.storelocator.model.OpeningScheduleModel;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.util.Date;
import java.util.Optional;

/**
 * Service related to openingSchedule
 */
public interface VMKOpeningScheduleService {

    /**
     * Returns the OpeningDay from the schedule for the given day
     *
     * @param openingSchedule the schedule
     * @param givenWeekDay    the given day
     * @return {@link OpeningDayModel}
     */
    OpeningDayModel getOpeningDayFromScheduleForGivenDay(OpeningScheduleModel openingSchedule, WeekDay givenWeekDay);

    boolean isStoreOpenOnGivenDate(String pointOfServiceName, Date date);

    /**
     * Checks whether the given {@code pos} is open at the given {@code date}.
     *
     * @param pos  the point of service
     * @param date the date
     * @return {@code true} in case the given {@code pos} is open at the given {@code date}, {@code false} otherwise
     */
    boolean isStoreOpenOnGivenDate(PointOfServiceModel pos, Date date);

    /**
     * Determines whether the given TEC store is open on saturday.
     *
     * @param pointOfService the TEC uid
     * @return true if open
     */
    boolean isStoreOpenOnSaturday(PointOfServiceModel pointOfService);

    /**
     * Determines whether the given point of service is open today.
     *
     * @param pos the point of service
     * @return true if open today
     */
    boolean isStoreOpenToday(PointOfServiceModel pos);

    /**
     * Finds first date when given point of service is open on or after given pickup date.
     *
     * @param pointOfService the point of service
     * @param pickupDate     the pickup date
     * @return pickup date when store is open if found
     */
    Optional<Date> findOpenPickupDateForPointOfService(PointOfServiceModel pointOfService, Date pickupDate);
}
