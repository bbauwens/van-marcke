package com.vanmarcke.services.order.cart.impl;

import de.hybris.platform.commerceservices.order.CommerceCartRestoration;
import de.hybris.platform.commerceservices.order.CommerceCartRestorationException;
import de.hybris.platform.commerceservices.order.CommerceSaveCartException;
import de.hybris.platform.commerceservices.order.impl.DefaultCommerceSaveCartService;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.commerceservices.service.data.CommerceSaveCartParameter;
import de.hybris.platform.commerceservices.service.data.CommerceSaveCartResult;
import de.hybris.platform.core.model.order.CartModel;

public class VMKCommerceSaveCartServiceImpl extends DefaultCommerceSaveCartService {

    @Override
    public CommerceSaveCartResult saveCart(final CommerceSaveCartParameter parameters) throws CommerceSaveCartException {
        final CommerceSaveCartResult saveCartResult = getCommerceSaveCartStrategy().saveCart(parameters);

        // when saving an existing saved cart after editing, the enable hooks is set to false
        if (parameters.isEnableHooks()) {
            final CommerceSaveCartResult clonedCartResult = getCommerceCloneSavedCartStrategy().cloneSavedCart(parameters);
            restoreCart(clonedCartResult.getSavedCart(), parameters.isEnableHooks());
        }

        return saveCartResult;
    }

    @Override
    public CommerceCartRestoration restoreSavedCart(final CommerceSaveCartParameter parameter) throws CommerceSaveCartException {
        final CommerceSaveCartResult clonedCartResult = getCommerceCloneSavedCartStrategy().cloneSavedCart(parameter);
        return restoreCart(clonedCartResult.getSavedCart(), parameter.isEnableHooks());
    }

    protected CommerceCartRestoration restoreCart(final CartModel cart, final boolean enableHooks) throws CommerceSaveCartException {
        final CommerceCartParameter commerceCartParameter = new CommerceCartParameter();
        commerceCartParameter.setCart(cart);
        commerceCartParameter.setEnableHooks(enableHooks);

        try {
            return getCommerceSaveCartRestorationStrategy().restoreCart(commerceCartParameter);
        } catch (final CommerceCartRestorationException e) {
            throw new CommerceSaveCartException(e.getMessage(), e);
        }
    }

}