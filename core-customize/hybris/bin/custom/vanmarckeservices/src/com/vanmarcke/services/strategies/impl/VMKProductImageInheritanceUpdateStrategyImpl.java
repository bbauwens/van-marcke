package com.vanmarcke.services.strategies.impl;

import com.vanmarcke.services.strategies.VMKProductImageInheritanceUpdateStrategy;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.variants.model.VariantProductModel;
import org.springframework.beans.factory.annotation.Required;

import java.util.List;
import java.util.stream.Collectors;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;
import static org.apache.commons.collections4.CollectionUtils.isEmpty;
import static org.apache.commons.lang3.StringUtils.startsWith;

/**
 * Default implementation for {@link VMKProductImageInheritanceUpdateStrategy}.
 */
public class VMKProductImageInheritanceUpdateStrategyImpl implements VMKProductImageInheritanceUpdateStrategy {

    private static final String VARIANT_PREFIX = "v-";

    private ModelService modelService;

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateProductImageInheritanceForProduct(final VariantProductModel variantProduct) {
        validateParameterNotNull(variantProduct, "variant product cannot be null");
        validateParameterNotNull(variantProduct.getBaseProduct(), "variant product's base product cannot be null");

        final String variantProductCode = variantProduct.getCode();
        final ProductModel baseProduct = variantProduct.getBaseProduct();

        if (isMediaForVariantProductUpdatable(variantProduct.getPicture(), variantProductCode)) {
            variantProduct.setPicture(baseProduct.getPicture());
        }

        if (isMediaForVariantProductUpdatable(variantProduct.getThumbnail(), variantProductCode)) {
            variantProduct.setThumbnail(baseProduct.getThumbnail());
        }

        final List<MediaContainerModel> variantGalleryImages = variantProduct.getGalleryImages().stream()
                .filter(e -> startsWith(e.getQualifier(), variantProductCode) || startsWith(e.getQualifier(), VARIANT_PREFIX))
                .collect(Collectors.toList());
        if (isEmpty(variantGalleryImages)) {
            variantProduct.setGalleryImages(baseProduct.getGalleryImages());
        } else {
            variantProduct.setGalleryImages(variantGalleryImages);
        }

        modelService.save(variantProduct);
    }

    private boolean isMediaForVariantProductUpdatable(final MediaModel media, final String variantProductCode) {
        return media == null || (!startsWith(media.getCode(), variantProductCode) && !startsWith(media.getCode(), VARIANT_PREFIX));
    }

    /**
     * Sets the modelService
     *
     * @param modelService the modelService
     */
    @Required
    public void setModelService(ModelService modelService) {
        this.modelService = modelService;
    }
}