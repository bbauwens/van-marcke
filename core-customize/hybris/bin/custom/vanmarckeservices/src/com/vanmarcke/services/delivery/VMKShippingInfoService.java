package com.vanmarcke.services.delivery;

import com.vanmarcke.cpi.data.order.ShippingDateInfoResponseData;
import de.hybris.platform.core.model.order.CartModel;

import java.util.Date;
import java.util.Optional;

/**
 * Defines methods for retrieving shipping information.
 *
 * @author Tom van den Berg
 * @since 30-12-2020
 */
public interface VMKShippingInfoService {

    /**
     * Determine whether shipping is possible for the given cart..
     *
     * @return true if shipping is possible
     */
    boolean isShippingPossible(CartModel cart);


    /**
     * Retrieve the first possible shipping date for the given cart.
     * <p>
     * It's possible that the date returned only applies to some of the products in the cart.
     *
     * @param cart the cart
     * @return the first possible shipping date
     */
    Optional<Date> getFirstPossibleShippingDate(CartModel cart);

    /**
     * Find first possible shipping date for given cart and shipping date response.
     *
     * @param cart     the cart
     * @param shippingDateInfoResponse the shipping date info response; must not be {@code null}
     * @return the first possible shipping date
     */
    Date getFirstPossibleShippingDate(CartModel cart, ShippingDateInfoResponseData shippingDateInfoResponse);

    /**
     * Get the last possible shipping date based on the response
     *
     * @param response the shipping response
     * @return the last possible date if found
     */
    Date getLastPossibleShippingDate(ShippingDateInfoResponseData response);

    /**
     * Returns the first possible shipping date for the given {@code cart}.
     *
     * @param cart the cart
     * @return the first possible shipping date
     */
    Date getFirstPossibleShippingDateForCompleteCart(final CartModel cart);
}
