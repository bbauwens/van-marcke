package com.vanmarcke.services.order.hook.impl;

import com.vanmarcke.services.strategies.VMKCommercePaymentCaptureStrategy;
import com.vanmarcke.services.strategies.VMKCommerceSendOrderStrategy;
import de.hybris.platform.commerceservices.order.hook.CommercePlaceOrderMethodHook;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.commerceservices.service.data.CommerceOrderResult;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.servicelayer.model.ModelService;

import static com.google.common.base.Preconditions.checkState;

/**
 * Method hook that takes care of the Send Order of the Place Order flow
 */
public class VMKSendOrderPlaceOrderMethodHookImpl implements CommercePlaceOrderMethodHook {

    private final VMKCommerceSendOrderStrategy vmkCommerceSendOrderStrategy;
    private final VMKCommercePaymentCaptureStrategy vmkCommercePaymentCaptureStrategy;
    private final ModelService modelService;

    /**
     * Creates a new instance of {@link VMKSendOrderPlaceOrderMethodHookImpl}
     *
     * @param vmkCommerceSendOrderStrategy      the commerce send order strategy
     * @param vmkCommercePaymentCaptureStrategy the payment capture strategy
     * @param modelService                      the model service
     */
    public VMKSendOrderPlaceOrderMethodHookImpl(VMKCommerceSendOrderStrategy vmkCommerceSendOrderStrategy,
                                                VMKCommercePaymentCaptureStrategy vmkCommercePaymentCaptureStrategy,
                                                ModelService modelService) {
        this.vmkCommerceSendOrderStrategy = vmkCommerceSendOrderStrategy;
        this.vmkCommercePaymentCaptureStrategy = vmkCommercePaymentCaptureStrategy;
        this.modelService = modelService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void afterPlaceOrder(final CommerceCheckoutParameter parameter, final CommerceOrderResult orderModel) {
        // nothing to do
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void beforePlaceOrder(final CommerceCheckoutParameter parameter) throws InvalidCartException {
        CartModel cart = parameter.getCart();
        if (cart != null) {
            cart.setStatus(OrderStatus.ORDER_CREATION_IN_PROGRESS);
            modelService.save(cart);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void beforeSubmitOrder(final CommerceCheckoutParameter parameter, final CommerceOrderResult result) throws InvalidCartException {
        checkState(result != null, "result cannot be null");
        checkState(result.getOrder() != null, "result.order cannot be null");

        if (vmkCommercePaymentCaptureStrategy.isPaymentCapturedForOrder(result.getOrder())) {
            vmkCommerceSendOrderStrategy.sendOrder(result.getOrder());
        }
    }
}