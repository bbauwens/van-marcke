package com.vanmarcke.services.order.payment.impl;

import com.vanmarcke.services.order.payment.VMKPaymentInfoFactory;
import de.hybris.platform.commerceservices.order.CommercePaymentProviderStrategy;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.payment.InvoicePaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.internal.service.AbstractBusinessService;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;
import org.springframework.beans.factory.annotation.Required;

import java.util.Map;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkState;
import static de.hybris.platform.b2bacceleratorservices.enums.CheckoutPaymentType.ACCOUNT;
import static org.apache.commons.collections4.MapUtils.getString;

public class VMKPaymentInfoFactoryImpl extends AbstractBusinessService implements VMKPaymentInfoFactory {

    private static final String DEFAULT_PAYMENT_INFO_TYPE_CODE = "PaymentInfo";

    private transient CommercePaymentProviderStrategy commercePaymentProviderStrategy;
    private transient KeyGenerator paymentInfoKeyGenerator;
    private transient Map<String, String> paymentInfoMap;

    @Override
    public PaymentInfoModel createPaymentInfoForOrder(final AbstractOrderModel order) {
        checkArgument(order != null, "parameter order can not be null");

        checkState(order.getPaymentType() != null, "order.paymentType cannot be null");

        PaymentInfoModel paymentInfo;
        if (ACCOUNT.equals(order.getPaymentType())) {
            paymentInfo = getModelService().create(InvoicePaymentInfoModel.class);
        } else {
            final String paymentInfoTypeCode = getString(paymentInfoMap, commercePaymentProviderStrategy.getPaymentProvider(), DEFAULT_PAYMENT_INFO_TYPE_CODE);
            paymentInfo = getModelService().create(paymentInfoTypeCode);
        }

        paymentInfo.setUser(order.getUser());
        paymentInfo.setCode(paymentInfoKeyGenerator.generateFor(order).toString());

        final AddressModel clonedPaymentAddress = getModelService().clone(order.getPaymentAddress());
        clonedPaymentAddress.setOwner(paymentInfo);
        paymentInfo.setBillingAddress(clonedPaymentAddress);

        getModelService().save(paymentInfo);

        return paymentInfo;
    }

    @Required
    public void setCommercePaymentProviderStrategy(final CommercePaymentProviderStrategy commercePaymentProviderStrategy) {
        this.commercePaymentProviderStrategy = commercePaymentProviderStrategy;
    }

    @Required
    public void setPaymentInfoKeyGenerator(final KeyGenerator paymentInfoKeyGenerator) {
        this.paymentInfoKeyGenerator = paymentInfoKeyGenerator;
    }

    @Required
    public void setPaymentInfoMap(final Map<String, String> paymentInfoMap) {
        this.paymentInfoMap = paymentInfoMap;
    }
}