package com.vanmarcke.services.model.attribute;

import com.vanmarcke.services.constants.VanmarckeservicesConstants;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler;
import de.hybris.platform.servicelayer.session.SessionService;

/**
 * Handler for the dynamic attribute "isOrderExport" on {@link AbstractOrderModel}
 */

public class VMKIsOrderExportAttributeHandler implements DynamicAttributeHandler<Boolean, AbstractOrderModel> {

    private final SessionService sessionService;

    /**
     * Creates a new instance of the {@link VMKIsOrderExportAttributeHandler} class.
     *
     * @param sessionService the session service
     */
    public VMKIsOrderExportAttributeHandler(SessionService sessionService) {
        this.sessionService = sessionService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Boolean get(AbstractOrderModel model) {
        Boolean isEnabledOnSession = sessionService.getAttribute(VanmarckeservicesConstants.IS_EXPORT_SESSION_ATTRIBUTE);
        return isEnabledOnSession != null && isEnabledOnSession;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void set(AbstractOrderModel model, Boolean aBoolean) {
        // Do nothing
    }
}
