package com.vanmarcke.services.odata.model;

import de.hybris.platform.core.model.type.AtomicTypeModel;
import de.hybris.platform.core.model.type.AttributeDescriptorModel;
import de.hybris.platform.core.model.type.CollectionTypeModel;
import de.hybris.platform.core.model.type.MapTypeModel;
import de.hybris.platform.core.model.type.TypeModel;
import de.hybris.platform.integrationservices.model.AttributeValueAccessor;
import de.hybris.platform.integrationservices.model.CollectionDescriptor;
import de.hybris.platform.integrationservices.model.DescriptorFactory;
import de.hybris.platform.integrationservices.model.IntegrationObjectItemAttributeModel;
import de.hybris.platform.integrationservices.model.IntegrationObjectItemModel;
import de.hybris.platform.integrationservices.model.MapDescriptor;
import de.hybris.platform.integrationservices.model.TypeAttributeDescriptor;
import de.hybris.platform.integrationservices.model.TypeDescriptor;
import de.hybris.platform.integrationservices.model.impl.DefaultDescriptorFactory;
import de.hybris.platform.integrationservices.model.impl.DefaultMapDescriptor;
import de.hybris.platform.integrationservices.model.impl.PrimitiveTypeDescriptor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Optional;

public class VMKTypeAttributeDescriptor implements TypeAttributeDescriptor {
    private static final Logger LOG = LogManager.getLogger();

    private final TypeAttributeDescriptor typeAttributeDescriptor;
    private final DescriptorFactory descriptorFactory;
    private final IntegrationObjectItemAttributeModel attributeModel;
    private final AttributeDescriptorModel attributeDescriptor;
    private TypeDescriptor attributeType;

    public VMKTypeAttributeDescriptor(TypeAttributeDescriptor typeAttributeDescriptor, IntegrationObjectItemAttributeModel attributeModel) {
        this(typeAttributeDescriptor, attributeModel, new DefaultDescriptorFactory());
    }

    public VMKTypeAttributeDescriptor(TypeAttributeDescriptor typeAttributeDescriptor, IntegrationObjectItemAttributeModel attributeModel, DescriptorFactory descriptorFactory) {
        this.typeAttributeDescriptor = typeAttributeDescriptor;
        this.descriptorFactory = descriptorFactory;
        this.attributeModel = attributeModel;
        this.attributeDescriptor = attributeModel.getAttributeDescriptor();
    }

    @Override
    public String getAttributeName() {
        return typeAttributeDescriptor.getAttributeName();
    }

    @Override
    public String getQualifier() {
        return typeAttributeDescriptor.getQualifier();
    }

    @Override
    public boolean isCollection() {
        return typeAttributeDescriptor.isCollection() || isLocalizedCollectionAttribute();
    }

    @Override
    public TypeDescriptor getAttributeType() {
        if (attributeType == null) {
            final IntegrationObjectItemModel referencedItemModel = attributeModel.getReturnIntegrationObjectItem();
            attributeType = referencedItemModel != null ? typeDescriptor(referencedItemModel) : createNonReferencedDescriptor();
        }
        return attributeType;
    }

    @Override
    public TypeDescriptor getTypeDescriptor() {
        return typeAttributeDescriptor.getTypeDescriptor();
    }

    @Override
    public Optional<TypeAttributeDescriptor> reverse() {
        return Optional.empty();
    }

    @Override
    public boolean isNullable() {
        return typeAttributeDescriptor.isNullable();
    }

    @Override
    public boolean isPartOf() {
        return typeAttributeDescriptor.isPartOf();
    }

    @Override
    public boolean isAutoCreate() {
        return typeAttributeDescriptor.isAutoCreate();
    }

    @Override
    public boolean isLocalized() {
        return typeAttributeDescriptor.isLocalized();
    }

    @Override
    public boolean isPrimitive() {
        return getAttributeType().isPrimitive();
    }

    @Override
    public boolean isMap() {
        return typeAttributeDescriptor.isMap();
    }

    @Override
    public boolean isWritable() {
        return typeAttributeDescriptor.isWritable();
    }

    @Override
    public boolean isInitializable() {
        return typeAttributeDescriptor.isInitializable();
    }

    @Override
    public boolean isKeyAttribute() {
        return typeAttributeDescriptor.isKeyAttribute();
    }

    @Override
    public CollectionDescriptor getCollectionDescriptor() {
        return typeAttributeDescriptor.getCollectionDescriptor();
    }

    @Override
    public Optional<MapDescriptor> getMapDescriptor() {
        try {
            if (isMap() && !isLocalized()) {
                return Optional.of(new DefaultMapDescriptor(attributeModel));
            }
        } catch (final IllegalArgumentException e) {
            LOG.warn("Failed to create a map descriptor for {}", this, e);
        }
        return Optional.empty();
    }

    private TypeDescriptor createNonReferencedDescriptor() {
        if (isMap() && !isLocalized()) {
            return VMKMapTypeDescriptor.create(attributeModel);
        } else {
            return PrimitiveTypeDescriptor.create(getTypeDescriptor().getIntegrationObjectCode(), derivePrimitiveTypeModel());
        }
    }

    private AtomicTypeModel derivePrimitiveTypeModel() {
        TypeModel typeModel = isCollectionAttribute() ? ((CollectionTypeModel) attributeDescriptor.getAttributeType()).getElementType() : attributeDescriptor.getAttributeType();

        if (isLocalized() && isMap(typeModel)) {
            typeModel = ((MapTypeModel) typeModel).getReturntype();
        }
        return getAtomicType(typeModel);
    }

    private boolean isLocalizedCollectionAttribute() {
        return isMap() && ((MapTypeModel) attributeDescriptor.getAttributeType()).getReturntype() instanceof CollectionTypeModel;
    }

    private boolean isCollectionAttribute() {
        return attributeDescriptor.getAttributeType() instanceof CollectionTypeModel;
    }

    private boolean isMap(final TypeModel typeModel) {
        return typeModel instanceof MapTypeModel;
    }

    private AtomicTypeModel getAtomicType(final TypeModel typeModel) {
        if (typeModel instanceof AtomicTypeModel) {
            return (AtomicTypeModel) typeModel;
        }
        throw new IllegalStateException("Modeling error: " + this + " is not a primitive attribute");
    }

    private TypeDescriptor typeDescriptor(final IntegrationObjectItemModel integrationObjectItem) {
        return descriptorFactory.createItemTypeDescriptor(integrationObjectItem);
    }

    @Override
    public AttributeValueAccessor accessor() {
        return typeAttributeDescriptor.accessor();
    }

    @Override
    public boolean isSettable(Object item) {
        return typeAttributeDescriptor.isSettable(item);
    }

    @Override
    public boolean isReadable() {
        return typeAttributeDescriptor.isReadable();
    }
}
