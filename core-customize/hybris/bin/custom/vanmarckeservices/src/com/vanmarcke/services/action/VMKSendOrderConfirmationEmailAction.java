package com.vanmarcke.services.action;

import be.elision.mandrillextension.service.MandrillService;
import com.vanmarcke.email.converters.impl.MandrillOrderConfirmationConverter;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import org.apache.commons.lang3.LocaleUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import java.util.Locale;

import static de.hybris.platform.core.enums.OrderStatus.CONFIRMATION_NOT_SENT;
import static de.hybris.platform.core.enums.OrderStatus.CONFIRMATION_SENT;

/**
 * Action which will trigger the event for sending an Email
 */
public class VMKSendOrderConfirmationEmailAction extends AbstractSimpleDecisionAction<OrderProcessModel> {

    private static final Logger LOGGER = LoggerFactory.getLogger(VMKSendOrderConfirmationEmailAction.class);

    private MandrillService mandrillService;
    private MandrillOrderConfirmationConverter mandrillOrderConfirmationConverter;

    /**
     * {@inheritDoc}
     */
    @Override
    public Transition executeAction(final OrderProcessModel process) {
        final OrderModel order = process.getOrder();
        try {
            Locale orderLocale = getLocale(order);
            final String templateName = order.getStore().getOrderConfirmationEmailTemplate(orderLocale);
            final String email = order.getUser().getUid();
            final String bcc = order.getSite().getChannel().equals(SiteChannel.DIY) ? order.getStore().getOrderManagerEmail() : null;

            String[] toArray = new String[]{email};
            String[] bccArray = StringUtils.isNotBlank(bcc) ? new String[]{bcc} : null;

            mandrillService.send(templateName, order, mandrillOrderConfirmationConverter, toArray, null, bccArray);
            setOrderStatus(order, CONFIRMATION_SENT);
            return Transition.OK;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

        setOrderStatus(order, CONFIRMATION_NOT_SENT);
        return Transition.NOK;
    }

    /**
     * Returns the locale for the given {@code order}.
     *
     * @param order the order
     * @return the locale
     */
    private Locale getLocale(OrderModel order) {
        String localeString = "en";
        LanguageModel language = order.getLanguage();
        if (language != null) {
            localeString = language.getIsocode().substring(0, 2);
        }
        return LocaleUtils.toLocale(localeString);
    }

    @Required
    public void setMandrillService(final MandrillService mandrillService) {
        this.mandrillService = mandrillService;
    }

    @Required
    public void setMandrillOrderConfirmationConverter(final MandrillOrderConfirmationConverter mandrillOrderConfirmationConverter) {
        this.mandrillOrderConfirmationConverter = mandrillOrderConfirmationConverter;
    }
}