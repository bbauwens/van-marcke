package com.vanmarcke.services.strategies.impl;

import com.vanmarcke.core.util.PaymentTransactionUtils;
import com.vanmarcke.services.strategies.VMKCommercePaymentCaptureStrategy;
import de.hybris.platform.b2bacceleratorservices.enums.CheckoutPaymentType;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.InvoicePaymentInfoModel;
import de.hybris.platform.payment.AdapterException;
import de.hybris.platform.payment.PaymentService;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.internal.service.AbstractBusinessService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import java.util.Date;
import java.util.Objects;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkState;
import static de.hybris.platform.payment.dto.TransactionStatus.ACCEPTED;
import static de.hybris.platform.payment.dto.TransactionStatus.REVIEW;
import static de.hybris.platform.payment.dto.TransactionStatusDetails.SUCCESFULL;
import static de.hybris.platform.payment.enums.PaymentTransactionType.AUTHORIZATION;
import static de.hybris.platform.payment.enums.PaymentTransactionType.CAPTURE;
import static java.lang.String.format;

/**
 * Strategy used for capturing payments.
 *
 * @author Taki Korovessis
 * @since 3-2-2020
 */
public class VMKCommercePaymentCaptureStrategyImpl extends AbstractBusinessService implements VMKCommercePaymentCaptureStrategy {

    private static final Logger LOGGER = LoggerFactory.getLogger(VMKCommercePaymentCaptureStrategyImpl.class);

    private transient PaymentService paymentService;

    /**
     * {@inheritDoc}
     */
    @Override
    public PaymentTransactionEntryModel capturePaymentAmount(AbstractOrderModel order) {
        checkArgument(order != null, "Parameter order cannot be null");

        checkState(order.getPaymentInfo() != null, "order.paymentinfo cannot be null");

        PaymentTransactionEntryModel paymentTransactionEntry = PaymentTransactionUtils.getCurrentPaymentTransactionEntry(order.getPaymentTransactions());
        checkState(paymentTransactionEntry != null, "order.paymentransaction.entry cannot be null");

        if (CAPTURE.equals(paymentTransactionEntry.getType()) && ACCEPTED.name().equals(paymentTransactionEntry.getTransactionStatus())) {
            LOGGER.debug("Order '{}' already captured", order.getCode());
            return paymentTransactionEntry;
        }

        if (CAPTURE.equals(paymentTransactionEntry.getType()) && REVIEW.name().equals(paymentTransactionEntry.getTransactionStatus())) {
            LOGGER.debug("Capture for order '{}' is pending", order.getCode());
            return paymentTransactionEntry;
        }

        if (!AUTHORIZATION.equals(paymentTransactionEntry.getType()) || !ACCEPTED.name().equals(paymentTransactionEntry.getTransactionStatus())) {
            LOGGER.warn("Could not capture order '{}' without authorization", order.getCode());
            return null;
        }

        PaymentTransactionModel paymentTransaction = paymentTransactionEntry.getPaymentTransaction();

        PaymentTransactionEntryModel entry = null;
        if (order.getPaymentInfo() instanceof InvoicePaymentInfoModel) {
            // create transaction entry
            String newEntryCode = paymentService.getNewPaymentTransactionEntryCode(paymentTransaction, CAPTURE);
            entry = getModelService().create(PaymentTransactionEntryModel.class);
            entry.setType(CAPTURE);
            entry.setCode(newEntryCode);
            entry.setAmount(paymentTransactionEntry.getAmount());
            entry.setCurrency(paymentTransactionEntry.getCurrency());
            entry.setTime(new Date());
            entry.setPaymentTransaction(paymentTransaction);
            entry.setTransactionStatus(ACCEPTED.toString());
            entry.setTransactionStatusDetails(SUCCESFULL.toString());
            getModelService().save(entry);
            // refresh transaction
            getModelService().refresh(paymentTransaction);
        } else {
            try {
                entry = paymentService.capture(paymentTransaction);
                // refresh transaction
                getModelService().refresh(paymentTransaction);
            } catch (AdapterException e) {
                LOGGER.warn(format("Could not capture order '%s'", order.getCode()), e);
            }
        }
        return entry;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isPaymentCapturedForOrder(OrderModel order) {
        return CheckoutPaymentType.ACCOUNT.equals(order.getPaymentType()) ||
                isPaymentCapturedForEntry(Objects.requireNonNull(
                        PaymentTransactionUtils.getCurrentPaymentTransactionEntry(order.getPaymentTransactions())));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isPaymentCapturedForEntry(PaymentTransactionEntryModel lastEntry) {
        return lastEntry.getType().equals(CAPTURE) && lastEntry.getTransactionStatus().equals(ACCEPTED.toString());
    }

    @Required
    public void setPaymentService(PaymentService paymentService) {
        this.paymentService = paymentService;
    }
}