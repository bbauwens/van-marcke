package com.vanmarcke.services.factories;

import de.hybris.platform.commerceservices.strategies.CartCleanStrategy;
import de.hybris.platform.commerceservices.strategies.CartValidationStrategy;

public interface VMKCartValidationStrategyFactory extends CartValidationStrategy, CartCleanStrategy {
}