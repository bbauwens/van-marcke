package com.vanmarcke.services.odata.model;

import de.hybris.platform.integrationservices.model.AbstractIntegrationObjectItemAttributeModel;
import de.hybris.platform.integrationservices.model.IntegrationObjectItemAttributeModel;
import de.hybris.platform.integrationservices.model.TypeAttributeDescriptor;
import de.hybris.platform.integrationservices.model.impl.DefaultDescriptorFactory;
import de.hybris.platform.integrationservices.model.impl.DefaultTypeAttributeDescriptor;

public class VMKDescriptorFactory extends DefaultDescriptorFactory {
    @Override
    public TypeAttributeDescriptor createTypeAttributeDescriptor(final AbstractIntegrationObjectItemAttributeModel itemAttribute) {
        final TypeAttributeDescriptor typeAttributeDescriptor = super.createTypeAttributeDescriptor(itemAttribute);

        if (typeAttributeDescriptor instanceof DefaultTypeAttributeDescriptor && itemAttribute instanceof IntegrationObjectItemAttributeModel) {
            return new VMKTypeAttributeDescriptor(typeAttributeDescriptor, (IntegrationObjectItemAttributeModel) itemAttribute, this);
        } else {
            return typeAttributeDescriptor;
        }
    }
}
