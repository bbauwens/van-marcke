package com.vanmarcke.services.xml;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.namespace.QName;
import java.util.Map;

/**
 * <p>
 * ref: https://dzone.com/articles/map-to-xml-dynamic-tag-names-with-jaxb
 * </p>
 */
public class MapAdapter extends XmlAdapter<MapWrapper, Map<String, String>> {

    @Override
    public Map<String, String> unmarshal(MapWrapper v) {
        return v.toMap();
    }

    @Override
    public MapWrapper marshal(Map<String, String> m) {
        MapWrapper wrapper = new MapWrapper();
        for (Map.Entry<String, String> entry : m.entrySet()) {
            wrapper.addEntry(new JAXBElement<>(new QName(entry.getKey()), String.class, entry.getValue()));
        }
        return wrapper;
    }
}