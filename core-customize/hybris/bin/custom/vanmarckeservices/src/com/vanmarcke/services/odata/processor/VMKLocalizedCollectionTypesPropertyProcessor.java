package com.vanmarcke.services.odata.processor;

import de.hybris.platform.integrationservices.service.IntegrationLocalizationService;
import de.hybris.platform.odata2services.odata.persistence.ModelEntityService;
import org.apache.commons.collections4.CollectionUtils;

import java.util.Collection;

public class VMKLocalizedCollectionTypesPropertyProcessor extends VMKLocalizedTypesPropertyProcessor {
    public VMKLocalizedCollectionTypesPropertyProcessor(IntegrationLocalizationService integrationLocalizationService, ModelEntityService modelEntityService) {
        super(integrationLocalizationService, modelEntityService);
    }

    @Override
    protected boolean canHandleEntityValue(final Object value) {
        return value instanceof Collection && CollectionUtils.isNotEmpty((Collection<?>) value);
    }
}
