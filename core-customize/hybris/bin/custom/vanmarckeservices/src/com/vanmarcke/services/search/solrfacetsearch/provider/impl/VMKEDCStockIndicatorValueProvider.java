package com.vanmarcke.services.search.solrfacetsearch.provider.impl;

import com.vanmarcke.core.enums.EDCStockStatus;
import com.vanmarcke.core.model.VanMarckeVariantProductModel;
import com.vanmarcke.services.product.VMKVariantProductService;
import de.hybris.platform.ordersplitting.WarehouseService;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;
import de.hybris.platform.solrfacetsearch.provider.FieldValueProvider;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractPropertyFieldValueProvider;
import de.hybris.platform.stock.impl.StockLevelDao;

import java.util.*;

/**
 * Provider to add the stock status of the product
 *
 * @author Niels Raemaekers
 * @since 02-07-2021
 */
public class VMKEDCStockIndicatorValueProvider extends AbstractPropertyFieldValueProvider implements FieldValueProvider {

    private static final String NOS_INDICATOR = "N";
    private static final String EDC_WAREHOUSE = "EDC";

    private final FieldNameProvider fieldNameProvider;
    private final StockLevelDao stockLevelDao;
    private final WarehouseService warehouseService;
    private final VMKVariantProductService vmkVariantProductService;

    /**
     * Constructor for {@link VMKEDCStockIndicatorValueProvider}
     *
     * @param fieldNameProvider        the {@link FieldNameProvider}
     * @param stockLevelDao            the {@link StockLevelDao}
     * @param warehouseService         the {@link WarehouseService}
     * @param vmkVariantProductService the {@link VMKVariantProductService}
     */
    public VMKEDCStockIndicatorValueProvider(FieldNameProvider fieldNameProvider, StockLevelDao stockLevelDao, WarehouseService warehouseService, VMKVariantProductService vmkVariantProductService) {
        this.fieldNameProvider = fieldNameProvider;
        this.stockLevelDao = stockLevelDao;
        this.warehouseService = warehouseService;
        this.vmkVariantProductService = vmkVariantProductService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<FieldValue> getFieldValues(final IndexConfig indexConfig, final IndexedProperty indexedProperty, final Object model) throws FieldValueProviderException {
        if (model instanceof VanMarckeVariantProductModel) {
            final VanMarckeVariantProductModel product = (VanMarckeVariantProductModel) model;
            final List<FieldValue> fieldValues = new ArrayList<>();

            String stockStatus = null;

            if (NOS_INDICATOR.equals(product.getDeliveryMethod())) {
                stockStatus = EDCStockStatus.NOSPRODUCT.getCode();
            } else {
                try {
                    StockLevelModel stockLevel = stockLevelDao.findStockLevel(product.getCode(), warehouseService.getWarehouseForCode(EDC_WAREHOUSE));
                    if (0 < stockLevel.getAvailable() && vmkVariantProductService.isPurchasable(product)) {
                        stockStatus = EDCStockStatus.STOCK.getCode();
                    } else {
                        stockStatus = EDCStockStatus.NOSTOCK.getCode();
                    }
                } catch (NullPointerException ex) {
                    // Do nothing
                }
            }

            if (Objects.isNull(stockStatus)) {
                return Collections.emptyList();
            }

            addFieldValues(fieldValues, indexedProperty, stockStatus);
            return fieldValues;
        } else {
            throw new FieldValueProviderException("Cannot get stock status of non-VMK Variant product item");
        }
    }

    private void addFieldValues(final List<FieldValue> fieldValues, final IndexedProperty indexedProperty, final Object value) {
        final Collection<String> fieldNames = fieldNameProvider.getFieldNames(indexedProperty, null);
        for (final String fieldName : fieldNames) {
            fieldValues.add(new FieldValue(fieldName, value));
        }
    }
}
