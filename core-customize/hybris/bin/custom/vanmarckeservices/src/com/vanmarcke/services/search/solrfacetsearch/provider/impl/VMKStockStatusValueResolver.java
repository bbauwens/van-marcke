package com.vanmarcke.services.search.solrfacetsearch.provider.impl;

import com.vanmarcke.services.product.VMKVariantProductService;
import de.hybris.platform.commercefacades.product.ProductStockStatus;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolver;
import de.hybris.platform.variants.model.VariantProductModel;

public class VMKStockStatusValueResolver extends AbstractValueResolver<ProductModel, Object, Object> {

    private VMKVariantProductService variantProductService;

    /**
     * {@inheritDoc}
     */
    @Override
    protected void addFieldValues(InputDocument inputDocument,
                                  IndexerBatchContext indexerBatchContext,
                                  IndexedProperty indexedProperty,
                                  ProductModel productModel,
                                  ValueResolverContext<Object, Object> valueResolverContext) throws FieldValueProviderException {
        if (productModel instanceof VariantProductModel) {
            String stockStatus = ProductStockStatus.IN_STOCK.toString();

            if (variantProductService.isNOSProduct((VariantProductModel) productModel)) {
                stockStatus = ProductStockStatus.NO_STOCK.toString();
            } else if (variantProductService.isDiscontinuedOutOfStockProduct((VariantProductModel) productModel)) {
                stockStatus = ProductStockStatus.LIMITED_STOCK.toString();
            } else if (variantProductService.isSoonInStockProduct((VariantProductModel) productModel)) {
                stockStatus = ProductStockStatus.SOON_IN_STOCK.toString();
            }

            addFieldValue(inputDocument, indexerBatchContext, indexedProperty,
                    stockStatus, valueResolverContext.getFieldQualifier());
        }
    }

    /**
     * Sets the variantProductService dependency
     * @param variantProductService the variantProductService instance
     */
    public void setVariantProductService(VMKVariantProductService variantProductService) {
        this.variantProductService = variantProductService;
    }
}
