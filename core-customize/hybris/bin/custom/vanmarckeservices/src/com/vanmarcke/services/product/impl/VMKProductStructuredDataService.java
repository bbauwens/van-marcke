package com.vanmarcke.services.product.impl;

import com.vanmarcke.services.product.VMKStructuredDataService;
import de.hybris.platform.acceleratorservices.urlresolver.SiteBaseUrlResolutionService;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.util.JsonUtilities;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Create product JSON for Google Structured Data
 *
 * @author Giani Ifrim
 * @since 27-07-2021
 */
public class VMKProductStructuredDataService implements VMKStructuredDataService<ProductData> {

    private static final Logger LOG = Logger.getLogger(VMKProductStructuredDataService.class);

    @Resource(name = "siteBaseUrlResolutionService")
    private SiteBaseUrlResolutionService siteBaseUrlResolutionService;

    @Resource(name = "baseSiteService")
    private BaseSiteService baseSiteService;

    /**
     * {@inheritDoc}
     */
    @Override
    public String createStructuredDataJson(ProductData productData) {
        String result = "";
        Map<String, Object> input = new LinkedHashMap<>();
        input.put("@context", "https://schema.org/");
        input.put("@type", "Product");

        if (!Objects.isNull(productData.getBrand()) && StringUtils.isNotBlank(productData.getBrand().getName())) {
            input.put("brand", productData.getBrand().getName());
        }

        if (StringUtils.isNotBlank(productData.getSuperCategory())) {
            input.put("category", productData.getSuperCategory());
        }

        if (StringUtils.isNotBlank(productData.getGtin())) {
            input.put("gtin", productData.getGtin());
        }

        if (StringUtils.isNotBlank(productData.getMpn())) {
            input.put("mpn", productData.getMpn());
        }

        if (StringUtils.isNotBlank(productData.getProductId())) {
            input.put("productId", productData.getProductId());
        }

        if (StringUtils.isNotBlank(productData.getCode())) {
            input.put("sku", productData.getCode());
        }

        if (StringUtils.isNotBlank(productData.getSummary())) {
            input.put("description", productData.getSummary());
        }

        if (StringUtils.isNotBlank(productData.getIdentifier())) {
            input.put("identifier", productData.getIdentifier());
        }

        if (StringUtils.isNotBlank(productData.getName())) {
            input.put("name", productData.getName());
        }

        if (StringUtils.isNotBlank(productData.getUrl())) {
            input.put("url", getSiteBaseUrlResolutionService()
                    .getWebsiteUrlForSite(getBaseSiteService().getCurrentBaseSite(), true, productData.getUrl()));
        }

        if (StringUtils.isNotBlank(productData.getThumbnail())) {
            input.put("image", productData.getThumbnail());
        }

        LinkedHashMap<String, Object> aggregateRating = new LinkedHashMap<>();

        aggregateRating.put("@type", "AggregateRating");
        aggregateRating.put("ratingValue", "5");
        aggregateRating.put("ratingCount", "1");
        aggregateRating.put("reviewCount", "1");

        input.put("aggregateRating", aggregateRating);

        try {
            result = JsonUtilities.toJson(input);
        } catch (IOException exception) {
            if (LOG.isDebugEnabled()) {
                LOG.debug(exception);
            }
        }

        return result;
    }

    public SiteBaseUrlResolutionService getSiteBaseUrlResolutionService() {
        return siteBaseUrlResolutionService;
    }

    public BaseSiteService getBaseSiteService() {
        return baseSiteService;
    }
}
