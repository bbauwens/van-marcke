package com.vanmarcke.services.action;

import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.processengine.model.BusinessProcessModel;

/**
 * TEMP ACTION
 */
public class VMKDummyAction extends AbstractSimpleDecisionAction {

    @Override
    public Transition executeAction(final BusinessProcessModel businessProcessModel) {
        return Transition.OK;
    }
}
