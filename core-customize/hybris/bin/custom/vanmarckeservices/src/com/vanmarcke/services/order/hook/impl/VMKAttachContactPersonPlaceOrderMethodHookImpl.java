package com.vanmarcke.services.order.hook.impl;

import com.vanmarcke.core.model.VanmarckeContactModel;
import com.vanmarcke.core.user.VMKContactPersonDao;
import de.hybris.platform.commerceservices.order.hook.CommercePlaceOrderMethodHook;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.commerceservices.service.data.CommerceOrderResult;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.servicelayer.model.ModelService;
import org.apache.commons.lang3.StringUtils;

/**
 * Method hook that takes care of attaching the return contact information
 *
 * @author Niels Raemaekers
 * @since 19-06-2020
 */
public class VMKAttachContactPersonPlaceOrderMethodHookImpl implements CommercePlaceOrderMethodHook {

    private final VMKContactPersonDao contactPersonDao;
    private final ModelService modelService;

    /**
     * Creates a new instance of {@link VMKAttachContactPersonPlaceOrderMethodHookImpl}
     *
     * @param contactPersonDao {@link VMKContactPersonDao}
     * @param modelService     {@link ModelService}
     */
    public VMKAttachContactPersonPlaceOrderMethodHookImpl(VMKContactPersonDao contactPersonDao, ModelService modelService) {
        this.contactPersonDao = contactPersonDao;
        this.modelService = modelService;
    }

    @Override
    public void afterPlaceOrder(final CommerceCheckoutParameter parameter, final CommerceOrderResult commerceOrderResult) throws InvalidCartException {
        //nothing to do
    }

    @Override
    public void beforePlaceOrder(final CommerceCheckoutParameter parameter) throws InvalidCartException {
        //nothing to do
    }

    @Override
    public void beforeSubmitOrder(final CommerceCheckoutParameter parameter, final CommerceOrderResult result) throws InvalidCartException {
        if (result.getOrder() != null) {
            OrderModel order = result.getOrder();
            order.setContactPerson(getReturnContact(order));
            modelService.save(order);
        }
    }

    /**
     * Get the return contact for the order
     *
     * @param order the order
     * @return the return contact
     */
    private VanmarckeContactModel getReturnContact(OrderModel order) {
        AddressModel billingAddress = order.getPaymentAddress();

        if (StringUtils.isNotEmpty(billingAddress.getPostalcode()) && billingAddress.getCountry() != null) {
            return contactPersonDao.findContactPersonForCountryAndPostalcode(billingAddress.getCountry(), billingAddress.getPostalcode());
        } else if (billingAddress.getCountry() != null) {
            return billingAddress.getCountry().getDefaultReturnContact();
        }

        return null;
    }
}