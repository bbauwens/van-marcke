package com.vanmarcke.services.address.impl;

import com.google.common.base.Preconditions;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2bacceleratorservices.strategies.impl.DefaultB2BDeliveryAddressesLookupStrategy;
import de.hybris.platform.commerceservices.strategies.DeliveryAddressesLookupStrategy;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.AddressModel;

import java.util.List;

import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang3.BooleanUtils.isTrue;

/**
 * Custom implementation for DefaultB2BDeliveryAddressesLookupStrategy
 */
public class VMKB2BDeliveryAddressesLookupStrategy extends DefaultB2BDeliveryAddressesLookupStrategy implements DeliveryAddressesLookupStrategy {

    @Override
    public List<AddressModel> getDeliveryAddressesForOrder(final AbstractOrderModel abstractOrder, final boolean visibleAddressesOnly) {
        Preconditions.checkNotNull(abstractOrder.getUnit(), "A B2BUnit must always be set before we can continue with the Checkout!");
        final B2BUnitModel b2BUnit = abstractOrder.getUnit();

        return b2BUnit.getAddresses().stream()
                .filter(address -> isTrue(address.getShippingAddress()))
                .filter(address -> visibleAddressesOnly ? isTrue(address.getVisibleInAddressBook()) : true)
                .collect(toList());
    }
}
