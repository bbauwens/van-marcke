package com.vanmarcke.services.job.maintenance.impl;

import com.vanmarcke.services.job.maintenance.AbstractVMKCleanupStrategy;
import de.hybris.platform.cronjob.model.CronJobHistoryModel;

/**
 * Cleans up all Cronjob Histories.
 */
public class VMKCleanupJobHistoryStrategyImpl extends AbstractVMKCleanupStrategy<CronJobHistoryModel> {

    private static final String QUERY = "SELECT {" + CronJobHistoryModel.PK + "}" +
            " FROM {" + CronJobHistoryModel._TYPECODE + "}" +
            " WHERE {" + CronJobHistoryModel.ENDTIME + "} < ?date";

    @Override
    protected final String getQuery() {
        return QUERY;
    }
}