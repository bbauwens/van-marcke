package com.vanmarcke.services.address.impl;

import com.google.common.base.Preconditions;
import com.vanmarcke.services.address.VMKPaymentAddressesLookupStrategy;
import com.vanmarcke.services.address.VMKPreferredPaymentAddressLookupStrategy;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import java.util.List;

/**
 * Implementation for BluePreferredPaymentAddressLookupStrategy
 */
public class VMKPreferredPaymentAddressLookupStrategyImpl implements VMKPreferredPaymentAddressLookupStrategy {

    private VMKPaymentAddressesLookupStrategy bluePaymentAddressesLookupStrategy;

    /**
     * {@inheritDoc}
     */
    @Override
    public AddressModel getPreferredPaymentAddressForOrder(final AbstractOrderModel abstractOrder) {

        Preconditions.checkNotNull(abstractOrder.getUnit(), "A B2BUnit must always be set before we can continue with the Checkout!");
        final B2BUnitModel b2BUnit = abstractOrder.getUnit();

        final AddressModel paymentAddress = b2BUnit.getBillingAddress();

        if (paymentAddress != null) {
            return paymentAddress;
        }

        final List<AddressModel> possiblePaymentAddresses = this.bluePaymentAddressesLookupStrategy.getPaymentAddressesForOrder(abstractOrder);
        if (CollectionUtils.isNotEmpty(possiblePaymentAddresses)) {
            return possiblePaymentAddresses.get(0);
        }

        return null;
    }

    @Required
    public void setBluePaymentAddressesLookupStrategy(final VMKPaymentAddressesLookupStrategy bluePaymentAddressesLookupStrategy) {
        this.bluePaymentAddressesLookupStrategy = bluePaymentAddressesLookupStrategy;
    }
}
