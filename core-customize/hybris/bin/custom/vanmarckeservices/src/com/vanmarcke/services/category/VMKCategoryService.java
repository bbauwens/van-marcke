package com.vanmarcke.services.category;

import com.vanmarcke.core.model.BrandCategoryModel;
import de.hybris.platform.core.model.media.MediaModel;

import java.util.Optional;

/**
 * Defines methods related to {@link de.hybris.platform.category.model.CategoryModel}
 * and {@link com.vanmarcke.core.model.BrandCategoryModel}
 *
 * @author Tom van den Berg
 * @since 25-08-2020
 */
public interface VMKCategoryService {

    /**
     * Retrieves a brand logo for the given media format, based on the current locale or its fallback locales.
     *
     * @param mediaFormat the media format string
     * @return an optional media model
     */
    Optional<MediaModel> getLocalizedBrandLogoForFormat(BrandCategoryModel brand, String mediaFormat);
}
