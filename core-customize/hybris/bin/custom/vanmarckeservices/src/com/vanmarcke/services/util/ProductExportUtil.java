package com.vanmarcke.services.util;

import org.jsoup.Jsoup;

/**
 * Util for the product export
 *
 * @author Niels Raemaekers
 * @since 18-05-2020
 */
public class ProductExportUtil {

    /**
     * Trim the HTML tags from the given string
     *
     * @param target the given string
     * @return the trimmed String
     */
    public static String trimHTMLTags(String target) {
        return Jsoup.parse(target).text();
    }
}
