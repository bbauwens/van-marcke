package com.vanmarcke.services.consent.impl;

import com.vanmarcke.core.cms.VMKConsentParagraphComponentDAO;
import com.vanmarcke.core.model.ConsentParagraphComponentModel;
import com.vanmarcke.services.consent.VMKConsentParagraphComponentService;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;

import java.util.List;

/**
 * The {@link VMKConsentParagraphComponentServiceImpl} class implements business logic concerning instances of the
 * {@link ConsentParagraphComponentModel} class.
 *
 * @author Christiaan Janssen
 * @since 16-07-2020
 */
public class VMKConsentParagraphComponentServiceImpl implements VMKConsentParagraphComponentService {

    private final VMKConsentParagraphComponentDAO consentParagraphComponentDAO;
    private final CatalogVersionService catalogVersionService;

    /**
     * Creates a new instance of the {@link VMKConsentParagraphComponentServiceImpl} class.
     *
     * @param consentParagraphComponentDAO the consent paragraph component
     * @param catalogVersionService        the catalog version service
     */
    public VMKConsentParagraphComponentServiceImpl(VMKConsentParagraphComponentDAO consentParagraphComponentDAO, CatalogVersionService catalogVersionService) {
        this.consentParagraphComponentDAO = consentParagraphComponentDAO;
        this.catalogVersionService = catalogVersionService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ConsentParagraphComponentModel> getConsentParagraphs() {
        CatalogVersionModel cv = catalogVersionService.getCatalogVersion("globalBlueContentCatalog", "Online");
        return consentParagraphComponentDAO.findAll(cv);
    }
}
