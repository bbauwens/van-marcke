package com.vanmarcke.services.interceptor;

import com.vanmarcke.core.model.BrandCategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;
import de.hybris.platform.variants.model.VariantProductModel;

import static org.apache.commons.collections4.IterableUtils.countMatches;

public class VMKProductBrandValidateInterceptor implements ValidateInterceptor<ProductModel> {

    @Override
    public void onValidate(final ProductModel product, final InterceptorContext ctx) throws InterceptorException {
        if (ctx.isModified(product, ProductModel.SUPERCATEGORIES)) {
            long count = countMatches(product.getSupercategories(), BrandCategoryModel.class::isInstance);

            if (product instanceof VariantProductModel) {
                if (count > 1) {
                    throw new InterceptorException("The number of brands for a variant product can not exceed 1");
                }
            } else if (count > 0) {
                throw new InterceptorException("Base products can not have any brands");
            }
        }
    }
}