package com.vanmarcke.services.order.cart.impl;

import com.vanmarcke.services.factories.VMKAbstractSiteChannelAwareFactory;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartFactory;

public class VMKCartFactoryFactoryImpl extends VMKAbstractSiteChannelAwareFactory<CartFactory> implements CartFactory {

    @Override
    public CartModel createCart() {
        return get().createCart();
    }
}
