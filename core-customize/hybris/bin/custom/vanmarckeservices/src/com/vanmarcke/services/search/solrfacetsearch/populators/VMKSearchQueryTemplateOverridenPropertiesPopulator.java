package com.vanmarcke.services.search.solrfacetsearch.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.solrfacetsearch.config.SearchQueryTemplate;
import de.hybris.platform.solrfacetsearch.model.SolrSearchQueryTemplateModel;

/**
 * The {@code VMKSearchQueryTemplateOverridenPropertiesPopulator} class is used to populate custom properties of the
 * solr search query template, specifically boolean properties that configure which of the template's properties override the default
 * configurations on the indexed type
 *
 * @author Cristi Stoica
 * @since 08-11-2021
 */
public class VMKSearchQueryTemplateOverridenPropertiesPopulator implements Populator<SolrSearchQueryTemplateModel, SearchQueryTemplate> {
    @Override
    public void populate(SolrSearchQueryTemplateModel source, SearchQueryTemplate target) throws ConversionException {
        target.setOverrideSearchQueryProperties(source.getOverrideSearchQueryProperties());
    }
}
