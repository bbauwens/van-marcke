package com.vanmarcke.services.order.cart.impl;

import com.vanmarcke.services.b2b.VMKB2BUnitService;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.order.impl.DefaultCartService;
import org.springframework.beans.factory.annotation.Required;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

/**
 * Custom implementation for DefaultCartService
 */
public class VMKCartServiceImpl extends DefaultCartService {

    private VMKB2BUnitService vmkB2BUnitService;

    /**
     * {@inheritDoc}
     */
    @Override
    public void changeCurrentCartUser(UserModel user) {
        validateParameterNotNull(user, "user must not be null!");
        if (hasSessionCart() && user instanceof B2BCustomerModel) {
            CartModel sessionCart = getSessionCart();
            sessionCart.setUser(user);
            sessionCart.setUnit(this.vmkB2BUnitService.getB2BUnitModelForCurrentUser());
            getModelService().save(sessionCart);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Required
    public void setVmkB2BUnitService(VMKB2BUnitService vmkB2BUnitService) {
        this.vmkB2BUnitService = vmkB2BUnitService;
    }
}
