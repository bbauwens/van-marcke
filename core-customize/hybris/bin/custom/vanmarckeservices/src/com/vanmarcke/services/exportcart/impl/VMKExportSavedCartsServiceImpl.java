package com.vanmarcke.services.exportcart.impl;

import com.google.common.collect.ImmutableList;
import com.lowagie.text.*;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.vanmarcke.facades.data.order.SavedCartData;
import com.vanmarcke.services.constants.VanmarckeservicesConstants.Export;
import com.vanmarcke.services.exportcart.VMKExportSavedCartsService;
import com.vanmarcke.services.util.PdfUtils;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.media.MediaService;
import org.apache.commons.io.IOUtils;
import org.springframework.http.MediaType;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author Cristi Stoica
 */
public class VMKExportSavedCartsServiceImpl implements VMKExportSavedCartsService {

    public static final String VANMARCKE_BLUE_HEADER_SAVEDCART_PDF_EXPORT_MEDIA = "vanmarcke-blue-wishlist-pdf-export";
    public static final String VANMARCKE_HEADER_SAVEDCART_PDF_EXPORT_MEDIA = "vanmarcke-wishlist-pdf-export";

    private final ProductService productService;
    private final MediaService mediaService;
    private final PdfUtils pdfUtils;
    private final CommerceCommonI18NService commerceCommonI18NService;
    private final CommonI18NService commonI18NService;

    public VMKExportSavedCartsServiceImpl(ProductService productService,
                                          MediaService mediaService,
                                          PdfUtils pdfUtils,
                                          CommerceCommonI18NService commerceCommonI18NService, CommonI18NService commonI18NService) {
        this.productService = productService;
        this.mediaService = mediaService;
        this.pdfUtils = pdfUtils;
        this.commerceCommonI18NService = commerceCommonI18NService;
        this.commonI18NService = commonI18NService;
    }

    @Override
    public byte[] generatePdf(List<String> headers, UserModel user, final SavedCartData savedCartData, boolean includeNet, boolean showImages, boolean includeTechnical, int emptyLines) {
        final Font boldFont = new Font(Font.COURIER, 10, Font.BOLD);
        final Font normalFont = new Font(Font.COURIER, 8, Font.NORMAL);
        final String currencySymbol = " ".concat(commerceCommonI18NService.getCurrentCurrency().getSymbol());

        // PDF generation code
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();

        final int totalColumns = 5 + (includeNet ? 1 : 0) + (showImages ? 1 : 0);

        try (Document document = new Document()) {
            double total = 0.00d;
            document.setPageSize(PageSize.A4.rotate());

            // setup the document itself
            // we create a writer that listens to the document and directs a PDF-stream to a file
            PdfWriter.getInstance(document, baos);
            document.open();

            // add empty lines
            addEmptyLines(document, emptyLines);

            // first the header with the images
            createPdfHeader(document, savedCartData, user);

            // print the wishlist information
            PdfPTable table = new PdfPTable(totalColumns);
            table.setWidthPercentage(100f);

            // include the table header
            headers.forEach(header -> {
                final Phrase phrase = new Phrase(header, boldFont);
                table.addCell(phrase);
            });

            for (OrderEntryData entry : savedCartData.getEntries()) {
                table.setWidths(getWidths(showImages, includeNet));

                // get the product instance for the image
                final ProductModel productModel = productService.getProductForCode(entry.getProduct().getCode());

                // 2. Product SKU
                final String productSku = entry.getProduct().getCode();

                // 3. The name
                final String productName = entry.getProduct().getName();

                // 3. Quantity
                final Long quantity = entry.getQuantity();

                // add them all
                addImage(table, showImages, productModel);
                table.addCell(new Phrase(productSku, normalFont));
                table.addCell(new Phrase(productName, normalFont));
                table.addCell(new Phrase(quantity.toString(), normalFont));
                table.addCell(new Phrase(entry.getBasePrice().getFormattedValue(), normalFont));
                if (includeNet) {
                    table.addCell(new Phrase(entry.getNetPriceData().getFormattedValue(), normalFont));
                }
                table.addCell(new Phrase(entry.getTotalPrice().getFormattedValue(), normalFont));
                total = total + entry.getTotalPrice().getValue().doubleValue();
            }

            PdfPCell cell = new PdfPCell(new Phrase());
            cell.setColspan(includeNet ? 6 : 5);
            table.addCell(cell);
            table.addCell(new Phrase(String.valueOf(commonI18NService.roundCurrency(total, Export.CURRENCY_DIGITS)).concat(currencySymbol), normalFont));

            document.add(table);

        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        // do we need to include the technical sheets?
        if (!includeTechnical) {
            // no we don't. Just return the current version
            return baos.toByteArray();
        }

        final List<InputStream> dataSheets = populateTechnicalDataSheets(savedCartData);

        // merge the generated PDF file, and the technical sheets
        final InputStream istream = mergeTechnicalDataSheets(baos, dataSheets);

        try {
            return IOUtils.toByteArray(istream);
        } catch (final IOException e) {
            return null;
        }
    }

    private void addEmptyLines(Document document, int emptyLines) {
        if (emptyLines > 0) {
            Paragraph paragraph = new Paragraph();
            paragraph.setSpacingAfter(10.f);
            for (int emptyLineCtr = 0; emptyLineCtr < emptyLines; emptyLineCtr++) {
                paragraph.add( Chunk.NEWLINE );
            }

            document.add(paragraph);
        }
    }

    private InputStream mergeTechnicalDataSheets(ByteArrayOutputStream baos, List<InputStream> dataSheets) {
        final InputStream istream = pdfUtils.mergePdfFiles(ImmutableList.<InputStream>builder()
                .add(new ByteArrayInputStream(baos.toByteArray()))
                .addAll(dataSheets)
                .build()
        );
        return istream;
    }

    protected List<InputStream> populateTechnicalDataSheets(SavedCartData savedCartData) {
        // create a list of PDF files, representing the technical sheets related to the products in the wishlist
        final List<InputStream> dataSheets = savedCartData.getEntries()
                .stream()
                .map(OrderEntryData::getProduct)
                .map(ProductData::getCode)
                .distinct()
                .map(productService::getProductForCode)
                .map(ProductModel::getTech_data_sheet)
                .filter(Objects::nonNull)
                .filter(m -> (MediaType.APPLICATION_PDF_VALUE).equals(m.getMime()))
                .map(this.mediaService::getStreamFromMedia)
                .collect(Collectors.toList());
        return dataSheets;
    }

    protected float[] getWidths(boolean showImages, boolean showNet) {
        final float[] widths;
        if (showImages && showNet) {
            widths = new float[]{1, 1, 3, 1, 1, 1, 1};
        } else if (showImages) {
            widths = new float[]{1, 1, 3, 1, 1, 1};
        } else if (showNet) {
            widths = new float[]{1, 3, 1, 1, 1, 1};
        } else {
            widths = new float[]{1, 3, 1, 1, 1};
        }
        return widths;
    }

    protected void createPdfHeader(final Document document, final SavedCartData savedCartData, final UserModel userModel) throws IOException {
        // fetch both the header images
        final MediaModel headerImgBlue = mediaService.getMedia(VANMARCKE_BLUE_HEADER_SAVEDCART_PDF_EXPORT_MEDIA);
        final MediaModel headerImg = mediaService.getMedia(VANMARCKE_HEADER_SAVEDCART_PDF_EXPORT_MEDIA);

        // add them to the images
        final Image imageVMKBlue = getImage(mediaService.getDataFromMedia(headerImgBlue));
        imageVMKBlue.scalePercent(40.0f);
        //image.setAbsolutePosition(90, 750);
        final Image imageVMK = getImage(mediaService.getDataFromMedia(headerImg));
        imageVMK.scalePercent(2.5f);

        // Create the paragraph containing all data
        Paragraph paragraph = new Paragraph();
        paragraph.setSpacingAfter(10.f);
        paragraph.setAlignment(ElementTags.ALIGN_CENTER);
        paragraph.add(new Chunk(imageVMKBlue, -320, 0, true));
        paragraph.add(new Chunk(imageVMK, 320, 0, true));
        document.add(paragraph);

        // create the paragraph with user information
        Paragraph userInfoParagraph = new Paragraph();
        userInfoParagraph.setSpacingAfter(10.f);
        userInfoParagraph.setAlignment(ElementTags.ALIGN_CENTER);
        userInfoParagraph.add(String.format("(%s) %s", userModel.getUid(), savedCartData.getName()));
        document.add(userInfoParagraph);
    }

    protected Image getImage(byte[] dataFromMedia) throws IOException {
        return Image.getInstance(dataFromMedia);
    }

    protected void addImage(PdfPTable table, final boolean showImages, ProductModel productModel) {
        if (showImages) {
            try {
                final MediaModel thumbnail = productModel.getThumbnail();

                if (thumbnail != null) {
                    final byte[] mediaData = mediaService.getDataFromMedia(thumbnail);
                    final Image image = getImage(mediaData);

                    PdfPCell cell = new PdfPCell();
                    cell.addElement(image);
                    table.addCell(cell);
                } else {
                    table.addCell("");
                }
            } catch (Exception e) {
                table.addCell("");
            }
        }
    }
}
