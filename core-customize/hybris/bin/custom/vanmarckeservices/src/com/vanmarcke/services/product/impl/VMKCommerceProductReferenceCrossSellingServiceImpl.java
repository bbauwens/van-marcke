package com.vanmarcke.services.product.impl;

import com.vanmarcke.services.product.VMKCrossSellingLookupService;
import de.hybris.platform.catalog.enums.ProductReferenceTypeEnum;
import de.hybris.platform.commerceservices.product.CommerceProductReferenceService;
import de.hybris.platform.commerceservices.product.data.ReferenceData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * The {@code DefaultVMKCommerceProductReferenceCrossSellingService} class is used to overwrite the
 * {@link CommerceProductReferenceService} class since we have different approach to retrieve cross-selling products.
 * <p>
 * Instead of storing these references in SAP Commerce, we'll perform a HTTP call to the ERP to retrieve the references
 * there.
 *
 * @author Niels Raemaekers, Christiaan Janssen
 * @since 21-11-2019
 */
public class VMKCommerceProductReferenceCrossSellingServiceImpl implements CommerceProductReferenceService<ProductReferenceTypeEnum, ProductModel> {

    private static final Logger LOG = Logger.getLogger(VMKCommerceProductReferenceCrossSellingServiceImpl.class);

    private final ProductService productService;
    private final VMKCrossSellingLookupService crossSellingLookupService;

    /**
     * Constructor for DefaultVMKCommerceProductReferenceCrossSellingService
     *
     * @param productService            the productService
     * @param crossSellingLookupService the cross selling lookup service
     */
    public VMKCommerceProductReferenceCrossSellingServiceImpl(ProductService productService,
                                                              VMKCrossSellingLookupService crossSellingLookupService) {
        this.crossSellingLookupService = crossSellingLookupService;
        this.productService = productService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ReferenceData<ProductReferenceTypeEnum, ProductModel>> getProductReferencesForCode(String code, ProductReferenceTypeEnum referenceType, Integer limit) {
        return getProductReferencesForCode(code, Collections.singletonList(referenceType), limit);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ReferenceData<ProductReferenceTypeEnum, ProductModel>> getProductReferencesForCode(String code, List<ProductReferenceTypeEnum> referenceTypes, Integer limit) {
        Iterator<String> i = crossSellingLookupService.getCrossSellingProducts(code).iterator();

        List<ReferenceData<ProductReferenceTypeEnum, ProductModel>> crossSellingReferences = new ArrayList<>();
        while ((limit == null || crossSellingReferences.size() < limit) && i.hasNext()) {
            String productCode = i.next();
            try {
                ProductModel product = productService.getProductForCode(productCode);
                crossSellingReferences.add(createCrossSellingReference(referenceTypes.get(0), product));
            } catch (UnknownIdentifierException e) {
                LOG.warn("Cross-selling product with code " + productCode + " was not found! Skipping...", e);
            }
        }
        return crossSellingReferences;
    }

    /**
     * Creates and returns a {@link ReferenceData} instance for the given {@code referenceType} and {@code product}.
     *
     * @param referenceType the type of product reference
     * @param product       the product
     * @return the {@link ReferenceData} instance
     */
    private ReferenceData<ProductReferenceTypeEnum, ProductModel> createCrossSellingReference(ProductReferenceTypeEnum referenceType, ProductModel product) {
        ReferenceData<ProductReferenceTypeEnum, ProductModel> referenceData = new ReferenceData<>();
        referenceData.setTarget(product);
        referenceData.setReferenceType(referenceType);
        return referenceData;
    }
}
