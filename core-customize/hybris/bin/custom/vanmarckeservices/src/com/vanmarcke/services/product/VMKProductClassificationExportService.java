package com.vanmarcke.services.product;

import com.vanmarcke.facades.data.ProductExportFeedResult;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.catalog.model.classification.ClassificationSystemVersionModel;

import java.util.Date;

@FunctionalInterface
public interface VMKProductClassificationExportService {

    /**
     * Generates a feed for the product classifications for the given {@link CatalogVersionModel} and {@link ClassificationSystemVersionModel}.
     *
     * @param catalogVersion   the product catalog version
     * @param systemVersion    the classification system version
     * @param lastModifiedTime the last modified time
     * @return result containing the product classification feed and updated last modified time
     */
    ProductExportFeedResult generateProductClassificationFeed(CatalogVersionModel catalogVersion, ClassificationSystemVersionModel systemVersion, Date lastModifiedTime);
}