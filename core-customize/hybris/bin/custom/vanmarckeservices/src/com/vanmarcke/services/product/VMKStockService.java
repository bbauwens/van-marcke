package com.vanmarcke.services.product;

import com.vanmarcke.facades.product.data.StockListData;
import de.hybris.platform.core.model.product.ProductModel;

/**
 * Service which retrieves stock.
 *
 * @author Tom van den Berg
 * @since 18-12-2020
 */
public interface VMKStockService {

    /**
     * Retrieve stock data for the given product and wareHouseIdentifiers.
     *
     * @param product              the product
     * @param warehouseIdentifiers the wareHouseIdentifiers
     * @return the store data
     */
    StockListData getStockForProductAndWarehouses(ProductModel product, String[] warehouseIdentifiers);
}
