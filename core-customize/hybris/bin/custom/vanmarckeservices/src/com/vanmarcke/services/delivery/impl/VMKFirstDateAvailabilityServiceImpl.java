package com.vanmarcke.services.delivery.impl;

import com.vanmarcke.services.basestore.VMKBaseStoreService;
import com.vanmarcke.services.delivery.VMKFirstDateAvailabilityService;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Implements methods from {@link VMKFirstDateAvailabilityService}
 *
 * @author Cristi Stoica
 * @since 29-09-2021
 */
public class VMKFirstDateAvailabilityServiceImpl implements VMKFirstDateAvailabilityService {
    private final VMKBaseStoreService baseStoreService;

    /**
     * Initializes fields for AbstractFirstDateAvailabilityService
     *
     * @param baseStoreService the base store service
     */
    public VMKFirstDateAvailabilityServiceImpl(VMKBaseStoreService baseStoreService) {
        this.baseStoreService = baseStoreService;
    }

    /**
     * Calculate and return the first possible delivery date equal or past a given first possible date
     *
     * @param firstPossibleShippingDate the attempted first possible date
     * @return the first possible date equal or past the attempted date
     */
    public Date getFirstValidNoClosingDate(Date firstPossibleShippingDate) {
        List<Date> closingDates = baseStoreService.getCurrentBaseStore().getClosingDates();
        List<LocalDate> closingLocalDates = closingDates.stream().map(date -> date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate())
                .collect(Collectors.toList());
        LocalDateTime localDate = firstPossibleShippingDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();

        while (true) {
            LocalDateTime newLocalDate = localDate.plusDays(1);

            if (closingLocalDates.contains(localDate.toLocalDate())) {
                localDate = newLocalDate;
                continue;
            }

            if (localDate.getDayOfWeek().equals(DayOfWeek.SATURDAY) || localDate.getDayOfWeek().equals(DayOfWeek.SUNDAY)) {
                localDate = newLocalDate;
                continue;
            }

            break;
        }

        return Date.from(localDate
                .atZone(ZoneId.systemDefault())
                .toInstant());
    }
}
