package com.vanmarcke.services.address;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.AddressModel;

/**
 * Interface for Preferred Delivery Addresses Lookup Strategy
 */
@FunctionalInterface
public interface VMKPreferredDeliveryAddressLookupStrategy {

    /**
     * Get the preferred delivery address for the current order
     *
     * @param abstractOrder the current order
     * @return
     */
    AddressModel getPreferredDeliveryAddressForOrder(final AbstractOrderModel abstractOrder);
}
