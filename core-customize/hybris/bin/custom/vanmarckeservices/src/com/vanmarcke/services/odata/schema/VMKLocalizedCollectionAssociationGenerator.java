package com.vanmarcke.services.odata.schema;

import de.hybris.platform.integrationservices.model.DescriptorFactory;
import de.hybris.platform.integrationservices.model.IntegrationObjectItemAttributeModel;
import de.hybris.platform.integrationservices.model.TypeAttributeDescriptor;
import de.hybris.platform.odata2services.odata.schema.association.CollectionAssociationGenerator;
import org.apache.olingo.odata2.api.edm.provider.Association;

public class VMKLocalizedCollectionAssociationGenerator extends CollectionAssociationGenerator {
    private final DescriptorFactory integrationServicesDescriptorFactory;

    public VMKLocalizedCollectionAssociationGenerator(DescriptorFactory integrationServicesDescriptorFactory) {
        this.integrationServicesDescriptorFactory = integrationServicesDescriptorFactory;
    }

    @Override
    public boolean isApplicable(final IntegrationObjectItemAttributeModel itemAttribute) {
        return isApplicable(integrationServicesDescriptorFactory.createTypeAttributeDescriptor(itemAttribute));
    }

    @Override
    public boolean isApplicable(final TypeAttributeDescriptor attributeDescriptor) {
        return attributeDescriptor != null && attributeDescriptor.isCollection() && attributeDescriptor.isLocalized();
    }

    @Override
    public Association generate(final IntegrationObjectItemAttributeModel itemAttribute) {
        return generate(integrationServicesDescriptorFactory.createTypeAttributeDescriptor(itemAttribute));
    }
}
