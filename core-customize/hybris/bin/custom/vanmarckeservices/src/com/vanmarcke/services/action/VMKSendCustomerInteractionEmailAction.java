package com.vanmarcke.services.action;

import be.elision.mandrillextension.service.MandrillService;
import com.vanmarcke.core.enums.CustomerInteractionStatus;
import com.vanmarcke.core.model.CustomerInteractionModel;
import com.vanmarcke.core.model.CustomerInteractionProcessModel;
import com.vanmarcke.core.model.OrderCustomerInteractionModel;
import com.vanmarcke.email.converters.impl.MandrillCustomerInteractionConverter;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.store.BaseStoreModel;
import org.apache.commons.lang3.LocaleUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import java.util.Locale;

import static com.vanmarcke.core.enums.CustomerInteractionStatus.NOT_SENT;
import static com.vanmarcke.core.enums.CustomerInteractionStatus.SENT;
import static com.vanmarcke.core.enums.CustomerInteractionType.CREDIT_CONTROLLER;
import static org.apache.commons.lang.StringUtils.defaultIfEmpty;

/**
 * Action which will trigger the event for sending an Email
 */
public class VMKSendCustomerInteractionEmailAction extends AbstractSimpleDecisionAction<CustomerInteractionProcessModel> {

    private static final Logger LOGGER = LoggerFactory.getLogger(VMKSendCustomerInteractionEmailAction.class);

    private MandrillService mandrillService;
    private MandrillCustomerInteractionConverter mandrillCustomerInteractionConverter;

    /**
     * {@inheritDoc}
     */
    @Override
    public Transition executeAction(CustomerInteractionProcessModel process) {
        CustomerInteractionModel customerInteraction = process.getCustomerInteraction();

        try {
            if (customerInteraction instanceof OrderCustomerInteractionModel) {
                OrderCustomerInteractionModel orderCustomerInteractionModel = (OrderCustomerInteractionModel) customerInteraction;
                OrderModel order = orderCustomerInteractionModel.getOrder();
                BaseStoreModel baseStore = order.getStore();

                String templateName;
                String serviceEmail;

                Locale currentLocale = getLocale(order);
                if (CREDIT_CONTROLLER.equals(customerInteraction.getType())) {
                    templateName = baseStore.getInvoiceEmailTemplate(currentLocale);
                    serviceEmail = baseStore.getInvoiceEmail();
                } else {
                    templateName = baseStore.getOrderManagerEmailTemplate(currentLocale);
                    serviceEmail = defaultIfEmpty(order.getContactPerson().getEmail(), baseStore.getOrderManagerEmail());
                }

                mandrillService.send(templateName, customerInteraction, mandrillCustomerInteractionConverter, serviceEmail);

                //send copy to the customer as well
                String customerEmail = order.getUser().getUid();
                mandrillService.send(templateName, customerInteraction, mandrillCustomerInteractionConverter, customerEmail);

                setCustomerInteractionStatus(customerInteraction, SENT);
                return Transition.OK;
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

        setCustomerInteractionStatus(customerInteraction, NOT_SENT);
        return Transition.NOK;
    }

    /**
     * Returns the locale for the given {@code order}.
     *
     * @param order the order
     * @return the locale
     */
    private Locale getLocale(OrderModel order) {
        String localeString = "en";
        LanguageModel language = order.getLanguage();
        if (language != null) {
            localeString = language.getIsocode().substring(0, 2);
        }
        return LocaleUtils.toLocale(localeString);
    }

    protected void setCustomerInteractionStatus(CustomerInteractionModel customerInteraction, CustomerInteractionStatus customerInteractionStatus) {
        customerInteraction.setStatus(customerInteractionStatus);
        this.modelService.save(customerInteraction);
    }

    @Required
    public void setMandrillService(MandrillService mandrillService) {
        this.mandrillService = mandrillService;
    }

    @Required
    public void setMandrillCustomerInteractionConverter(MandrillCustomerInteractionConverter mandrillCustomerInteractionConverter) {
        this.mandrillCustomerInteractionConverter = mandrillCustomerInteractionConverter;
    }
}