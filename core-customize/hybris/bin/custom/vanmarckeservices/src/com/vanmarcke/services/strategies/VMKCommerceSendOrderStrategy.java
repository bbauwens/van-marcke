package com.vanmarcke.services.strategies;

import de.hybris.platform.core.model.order.OrderModel;

/**
 * Strategy for Commerce Place Order
 */
@FunctionalInterface
public interface VMKCommerceSendOrderStrategy {

    /**
     * Send the order
     *
     * @param order the order to send
     * @return the created order
     */
    OrderModel sendOrder(final OrderModel order);
}
