package com.vanmarcke.services.address;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.AddressModel;

/**
 * Interface for Preferred Payment Addresses Lookup Strategy
 */
@FunctionalInterface
public interface VMKPreferredPaymentAddressLookupStrategy {

    /**
     * Get the preferred payment address for the current order
     *
     * @param abstractOrder the current order
     * @return
     */
    AddressModel getPreferredPaymentAddressForOrder(final AbstractOrderModel abstractOrder);
}
