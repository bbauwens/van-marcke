package com.vanmarcke.services.strategies;

import com.vanmarcke.core.model.CustomerInteractionModel;

/**
 * interface for VMKSendCustomerInteractionEmailStrategy
 */
public interface VMKSendCustomerInteractionEmailStrategy {

    /**
     * Sends the customer interaction email after an order when requested by the customer
     *
     * @param customerInteraction order to send an email for
     */
    void sendCustomerInteractionForOrder(CustomerInteractionModel customerInteraction);
}
