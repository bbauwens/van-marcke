package com.vanmarcke.services.odata.processor;

import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.integrationservices.model.TypeAttributeDescriptor;
import de.hybris.platform.integrationservices.model.TypeDescriptor;
import de.hybris.platform.integrationservices.service.IntegrationLocalizationService;
import de.hybris.platform.odata2services.odata.persistence.AbstractRequest;
import de.hybris.platform.odata2services.odata.persistence.ConversionOptions;
import de.hybris.platform.odata2services.odata.persistence.ItemConversionRequest;
import de.hybris.platform.odata2services.odata.persistence.ModelEntityService;
import de.hybris.platform.odata2services.odata.persistence.StorageRequest;
import de.hybris.platform.odata2services.odata.persistence.populator.processor.AbstractPropertyProcessor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.olingo.odata2.api.edm.EdmException;
import org.apache.olingo.odata2.api.edm.EdmNavigationProperty;
import org.apache.olingo.odata2.api.edm.EdmTyped;
import org.apache.olingo.odata2.api.ep.entry.ODataEntry;
import org.apache.olingo.odata2.api.ep.feed.ODataFeed;
import org.apache.olingo.odata2.core.ep.entry.EntryMetadataImpl;
import org.apache.olingo.odata2.core.ep.entry.MediaMetadataImpl;
import org.apache.olingo.odata2.core.ep.entry.ODataEntryImpl;
import org.apache.olingo.odata2.core.uri.ExpandSelectTreeNodeImpl;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static de.hybris.platform.odata2services.constants.Odata2servicesConstants.LANGUAGE_KEY_PROPERTY_NAME;
import static de.hybris.platform.odata2services.constants.Odata2servicesConstants.LOCALIZED_ATTRIBUTE_NAME;
import static de.hybris.platform.odata2services.odata.persistence.ODataFeedBuilder.oDataFeedBuilder;

public class VMKLocalizedTypesPropertyProcessor extends AbstractPropertyProcessor {
    private static final Logger LOG = LogManager.getLogger();

    private final IntegrationLocalizationService integrationLocalizationService;
    private final ModelEntityService modelEntityService;

    public VMKLocalizedTypesPropertyProcessor(IntegrationLocalizationService integrationLocalizationService, ModelEntityService modelEntityService) {
        this.integrationLocalizationService = integrationLocalizationService;
        this.modelEntityService = modelEntityService;
    }

    @Override
    public void processEntity(final ODataEntry oDataEntry, final ItemConversionRequest conversionRequest) throws EdmException {
        final EdmTyped localizedAttributes = conversionRequest.getEntityType().getProperty(LOCALIZED_ATTRIBUTE_NAME);

        if (localizedAttributes instanceof EdmNavigationProperty) {
            final TypeDescriptor typeDescriptor = getTypeDescriptor(conversionRequest);

            if (typeDescriptor != null) {
                final List<TypeAttributeDescriptor> localizedTypeDescriptors = getAttributeDescriptorsForConversion(typeDescriptor);
                processEntityInternal(oDataEntry, localizedTypeDescriptors, conversionRequest);
            }
        }
    }

    private void processEntityInternal(final ODataEntry oDataEntry, List<TypeAttributeDescriptor> localizedTypeDescriptors, final ItemConversionRequest conversionRequest) {
        final List<ODataEntry> entries = getDataEntriesForDescriptors(conversionRequest, localizedTypeDescriptors);

        if (oDataEntry.getProperties().containsKey(LOCALIZED_ATTRIBUTE_NAME)) {
            entries.forEach(newODataEntry -> {
                final String languageISO = (String) newODataEntry.getProperties().get(LANGUAGE_KEY_PROPERTY_NAME);

                findEntryByLanguage(((ODataFeed) oDataEntry.getProperties().get(LOCALIZED_ATTRIBUTE_NAME)).getEntries(), languageISO).ifPresentOrElse(
                        oldODataEntry -> oldODataEntry.getProperties().putAll(newODataEntry.getProperties()),
                        () -> ((ODataFeed) oDataEntry.getProperties().get(LOCALIZED_ATTRIBUTE_NAME)).getEntries().add(newODataEntry));
            });
        } else {
            oDataEntry.getProperties().put(LOCALIZED_ATTRIBUTE_NAME, oDataFeedBuilder().withEntries(entries).build());
        }
    }

    private List<ODataEntry> getDataEntriesForDescriptors(final ItemConversionRequest conversionRequest, final List<TypeAttributeDescriptor> localizedAttributeDescriptors) {
        final ConversionOptions options = conversionRequest.getOptions();

        if ((options.isExpandPresent() || options.isNavigationSegmentPresent())) {
            final Map<String, Map<Locale, Object>> translationDataForLocaleByAttributeName = localizedAttributeDescriptors.stream()
                    .collect(Collectors.toMap(TypeAttributeDescriptor::getAttributeName, attributeDescriptor -> deriveDataEntriesByLocaleForDescriptor(conversionRequest, attributeDescriptor)));

            return groupEntityTranslationsByLocale(translationDataForLocaleByAttributeName).entrySet().stream()
                    .map(this::createLocalizedODataEntry)
                    .collect(Collectors.toList());
        }
        return Collections.emptyList();
    }

    private Map<Locale, Object> deriveDataEntriesByLocaleForDescriptor(final ItemConversionRequest conversionRequest, final TypeAttributeDescriptor attributeDescriptor) {
        final Locale[] supportedLocales = integrationLocalizationService.getAllSupportedLocales().toArray(new Locale[0]);
        final Map<Locale, Object> valueByLocale = attributeDescriptor.accessor().getValues(conversionRequest.getValue(), supportedLocales);

        final Map<Locale, Object> result = new HashMap<>();
        valueByLocale.forEach((locale, o) -> {
            if (canHandleEntityValue(o)) {
                convertValue(o, conversionRequest, attributeDescriptor).ifPresent(oDataEntry -> result.put(locale, oDataEntry));
            }
        });
        return result;
    }

    private Optional<?> convertValue(final Object entityValue, final ItemConversionRequest conversionRequest, final TypeAttributeDescriptor attributeDescriptor) {
        final List<ODataEntry> result = new ArrayList<>();
        if (entityValue instanceof Collection) {
            final Collection<?> values = ((Collection<?>) entityValue);

            final List<ODataEntry> collect = values.stream()
                    .map(value -> convertObjectToODataEntry(value, conversionRequest, attributeDescriptor))
                    .flatMap(Optional::stream)
                    .collect(Collectors.toList());
            return Optional.of(oDataFeedBuilder().withEntries(collect).build());
        } else {
            return convertObjectToODataEntry(entityValue, conversionRequest, attributeDescriptor);
        }
    }

    private Stream<?> getValueAsStream(final Object entityValue) {
        if (entityValue instanceof Collection) {
            return ((Collection<?>) entityValue).stream();
        }
        return Stream.ofNullable(entityValue);
    }

    private Map<Locale, Map<String, Object>> groupEntityTranslationsByLocale(final Map<String, Map<Locale, Object>> dataEntryByLocaleByAttributeName) {
        return dataEntryByLocaleByAttributeName.entrySet().stream()
                .flatMap(entry -> entry.getValue().entrySet().stream()
                        .map(innerEntry -> new AbstractMap.SimpleEntry<>(entry.getKey(), innerEntry)))
                .collect(Collectors.toMap(
                        entry -> entry.getValue().getKey(),
                        entry -> {
                            final Map<String, Object> valueByAttributeName = new HashMap<>();
                            valueByAttributeName.put(entry.getKey(), entry.getValue().getValue());
                            valueByAttributeName.put(LANGUAGE_KEY_PROPERTY_NAME, entry.getValue().getKey().toString());
                            return valueByAttributeName;
                        },
                        (map1, map2) -> {
                            map1.putAll(map2);
                            return map1;
                        }));
    }

    private Optional<ODataEntry> convertObjectToODataEntry(final Object value, final ItemConversionRequest conversionRequest, final TypeAttributeDescriptor attributeDescriptor) {
        try {
            final ItemConversionRequest itemConversionRequest = VMKItemConversionRequest.requestBuilder()
                    .from(conversionRequest)
                    .build()
                    .propertyConversionRequest(attributeDescriptor.getAttributeType().getTypeCode(), attributeDescriptor.getAttributeName(), value);

            return Optional.ofNullable(modelEntityService.getODataEntry(itemConversionRequest));
        } catch (final EdmException e) {
            LOG.error("Failed to convert property value to target integration object [property name: {} | target object: {}]", attributeDescriptor::getAttributeName, attributeDescriptor.getAttributeType()::getTypeCode);

            return Optional.empty();
        }
    }

    private Optional<ODataEntry> findEntryByLanguage(final List<ODataEntry> entries, final String languageISO) {
        return entries.stream()
                .filter(oDataEntry -> oDataEntry.getProperties().containsKey(LANGUAGE_KEY_PROPERTY_NAME))
                .filter(oDataEntry -> oDataEntry.getProperties().get(LANGUAGE_KEY_PROPERTY_NAME).equals(languageISO))
                .findFirst();
    }

    @Override
    protected boolean isApplicable(final TypeAttributeDescriptor attributeDescriptor) {
        return !attributeDescriptor.isPrimitive() && attributeDescriptor.isLocalized() && attributeDescriptor.isReadable();
    }

    protected boolean canHandleEntityValue(final Object value) {
        return !(value instanceof Collection);
    }

    @Override
    protected void processEntityInternal(final ODataEntry oDataEntry, final String propertyName, final Object value, final ItemConversionRequest request) {
        //not supported
    }

    @Override
    protected void processItemInternal(final ItemModel item, final String entryPropertyName, final Object value, final StorageRequest request) {
        // not supported
    }

    private TypeDescriptor getTypeDescriptor(final AbstractRequest conversionRequest) throws EdmException {
        final String itemTypeCode = conversionRequest.getEntityType().getName();
        final String integrationObjectCode = conversionRequest.getIntegrationObjectCode();
        return getItemTypeDescriptorService().getTypeDescriptorByTypeCode(integrationObjectCode, itemTypeCode)
                .orElse(null);
    }

    private List<TypeAttributeDescriptor> getAttributeDescriptorsForConversion(final TypeDescriptor typeDescriptor) {
        return typeDescriptor.getAttributes().stream()
                .filter(this::isApplicable)
                .collect(Collectors.toList());
    }

    private ODataEntry createLocalizedODataEntry(final Map.Entry<Locale, Map<String, Object>> translation) {
        final ODataEntry entry = new ODataEntryImpl(new HashMap<>(), new MediaMetadataImpl(), new EntryMetadataImpl(), new ExpandSelectTreeNodeImpl());
        entry.getProperties().put(LANGUAGE_KEY_PROPERTY_NAME, translation.getKey().getLanguage());
        entry.getProperties().putAll(translation.getValue());
        return entry;
    }
}
