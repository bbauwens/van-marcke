package com.vanmarcke.services.search.solrfacetsearch.populators;

import de.hybris.platform.commerceservices.search.solrfacetsearch.populators.CommerceIndexedPropertyPopulator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.model.config.SolrIndexedPropertyModel;

/**
 * The {@code VMKCommerceIndexedPropertyPopulator} class is used to extend the OOTB
 * {@link CommerceIndexedPropertyPopulator} class.
 * <p>
 * It is used to populate the classification attribute unit on the indexed property, so that it can be used on the facet display name provider
 *
 * @author Cristian Stoica
 * @since 29-10-2021
 */
public class VMKCommerceIndexedPropertyPopulator extends CommerceIndexedPropertyPopulator {

    @Override
    public void populate(final SolrIndexedPropertyModel property, final IndexedProperty indexedProperty) throws ConversionException {
        super.populate(property, indexedProperty);
        if (property.getUnit() != null) {
            indexedProperty.setUnit(property.getUnit().getSymbol());
        }
    }
}
