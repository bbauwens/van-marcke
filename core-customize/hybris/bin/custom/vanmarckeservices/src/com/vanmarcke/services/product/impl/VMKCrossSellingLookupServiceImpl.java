package com.vanmarcke.services.product.impl;

import com.vanmarcke.core.model.CommerceGroupModel;
import com.vanmarcke.cpi.data.builder.CrossSellingRequestDataBuilder;
import com.vanmarcke.cpi.data.product.CrossSellingRequestData;
import com.vanmarcke.cpi.data.product.CrossSellingResponseData;
import com.vanmarcke.cpi.services.ESBCrossSellingService;
import com.vanmarcke.services.commercegroup.VMKCommerceGroupService;
import com.vanmarcke.services.product.VMKCrossSellingLookupService;
import org.springframework.cache.annotation.Cacheable;

import java.util.Collections;
import java.util.List;

/**
 * Implements the {@link VMKCrossSellingLookupService}.
 * It implements methods which handle the retrievel of cross selling products.
 *
 * @author Tom van den Berg
 * @since 11-06-2020
 */
public class VMKCrossSellingLookupServiceImpl implements VMKCrossSellingLookupService {

    private final ESBCrossSellingService esbCrossSellingService;
    private final VMKCommerceGroupService vmkCommerceGroupService;

    /**
     * Provides an instance of the {@code DefaultVMKCrossSellingLookupService}.
     *
     * @param esbCrossSellingService  the esb crosselling service
     * @param vmkCommerceGroupService tehe commerce group service
     */
    public VMKCrossSellingLookupServiceImpl(ESBCrossSellingService esbCrossSellingService, VMKCommerceGroupService vmkCommerceGroupService) {
        this.esbCrossSellingService = esbCrossSellingService;
        this.vmkCommerceGroupService = vmkCommerceGroupService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Cacheable(value = "crossSellInformation", key = "#code", cacheManager = "vanMarckeCacheManager")
    public List<String> getCrossSellingProducts(String code) {
        CommerceGroupModel commerceGroup = vmkCommerceGroupService.getCurrentCommerceGroup();

        CrossSellingRequestData request = CrossSellingRequestDataBuilder
                .aCrossSellingRequest(code)
                .withCountryISO(commerceGroup == null ? null : commerceGroup.getCountry().getIsocode())
                .build();

        CrossSellingResponseData response = esbCrossSellingService.getCrossSellingInformation(request);

        return (response != null && response.getCrossSellingProductCodes() != null) ?
                response.getCrossSellingProductCodes() : Collections.emptyList();
    }
}
