package com.vanmarcke.services.product.impl;

import com.google.common.collect.Iterables;
import com.vanmarcke.core.constants.VanmarckeCoreConstants;
import com.vanmarcke.core.product.VMKPagedVariantProductDao;
import com.vanmarcke.services.product.VMKNonArchivedVariantsExportService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.servicelayer.data.SearchPageData;
import de.hybris.platform.variants.model.VariantProductModel;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.LocaleUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import static de.hybris.platform.servicelayer.search.paginated.util.PaginatedSearchUtils.createSearchPageDataWithPagination;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;

/**
 * Default implementation of {@link VMKNonArchivedVariantsExportService}
 *
 * @author Niels Raemaekers
 * @since 12-05-2020
 */
public class VMKNonArchivedVariantsExportServiceImpl implements VMKNonArchivedVariantsExportService {

    private static final int PAGE_SIZE = 1000;

    private final VMKPagedVariantProductDao pagedVariantProductDao;

    /**
     * Constructor for {@link VMKNonArchivedVariantsExportServiceImpl}
     *
     * @param pagedVariantProductDao the {@link VMKPagedVariantProductDao}
     */
    public VMKNonArchivedVariantsExportServiceImpl(VMKPagedVariantProductDao pagedVariantProductDao) {
        this.pagedVariantProductDao = pagedVariantProductDao;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<String> generateNonArchivedVariantsExportFeed(CatalogVersionModel catalogVersion) {
        int currentPage = 0, numberOfPages;

        List<String> exportLines = new ArrayList<>();
        Locale localeNL = LocaleUtils.toLocale(VanmarckeCoreConstants.Languages.ISO_CODE_NL);
        do {
            final SearchPageData<VariantProductModel> searchPageData = createSearchPageDataWithPagination(PAGE_SIZE, currentPage, true);
            SearchPageData<VariantProductModel> nonArchivedProducts = pagedVariantProductDao.findNonArchivedProductsByCatalogVersion(catalogVersion, searchPageData);

            if (isNotEmpty(nonArchivedProducts.getResults())) {
                for (final VariantProductModel variant : nonArchivedProducts.getResults()) {
                    List<String> exportLine = new ArrayList<>();

                    exportLine.add(variant.getCode());
                    exportLine.add(resolveBaseProduct(variant));
                    addCategoryInformation(exportLine, variant, localeNL);
                    addClassificationInformation(exportLine, variant, localeNL);
                    if (variant.getDeaveraging() != null) {
                        exportLine.add(variant.getDeaveraging().getCode());
                    }
                    exportLines.add(exportLine.stream()
                            .collect(Collectors.joining(";")));
                }
            }

            currentPage++;
            numberOfPages = nonArchivedProducts.getPagination().getNumberOfPages();
        } while (currentPage < numberOfPages);

        return exportLines;
    }

    /**
     * Resolve the base product for a given variant
     *
     * @param variant the given variant
     * @return the baseProduct code if there is one, otherwise empty String
     */
    private String resolveBaseProduct(VariantProductModel variant) {
        if (variant.getBaseProduct() != null) {
            return variant.getBaseProduct().getCode();
        }
        return StringUtils.EMPTY;
    }

    /**
     * Add the category information to the exportLine for a given variant
     *
     * @param exportLine the exportLine to add to
     * @param variant    the given variant
     * @param localeNL   the given locale
     */
    private void addCategoryInformation(List<String> exportLine, VariantProductModel variant, Locale localeNL) {
        ProductModel baseProduct = variant.getBaseProduct();
        if (baseProduct != null && CollectionUtils.isNotEmpty(baseProduct.getSupercategories())) {
            Collection<CategoryModel> superCategories = baseProduct.getSupercategories();
            CategoryModel superCategory = Iterables.get(superCategories, 0);
            addCategoryInformation(exportLine, superCategory, localeNL);
        } else {
            addEmptyLinesForCategory(exportLine);
        }
    }

    /**
     * Add the classification information to the exportLine for a given variant
     *
     * @param exportLine the exportLine to add to
     * @param variant    the given variant
     * @param localeNL   the given locale
     */
    private void addClassificationInformation(List<String> exportLine, VariantProductModel variant, Locale localeNL) {
        List<ClassificationClassModel> classificationClasses = variant.getClassificationClasses();
        if (CollectionUtils.isNotEmpty(classificationClasses)) {
            ClassificationClassModel etimClassificationClass = classificationClasses.stream()
                    .filter(c -> isEtimClassification(c))
                    .findFirst()
                    .orElse(null);
            ClassificationClassModel gs1ClassificationClass = classificationClasses.stream()
                    .filter(c -> isGS1Classification(c))
                    .findFirst()
                    .orElse(null);

            if (etimClassificationClass != null) {
                addCategoryInformation(exportLine, etimClassificationClass, localeNL);
            } else {
                addEmptyLinesForCategory(exportLine);
            }

            if (gs1ClassificationClass != null) {
                addCategoryInformation(exportLine, gs1ClassificationClass, localeNL);
            } else {
                addEmptyLinesForCategory(exportLine);
            }
        } else {
            addEmptyLinesForCategory(exportLine);
            addEmptyLinesForCategory(exportLine);
        }
    }

    /**
     * Check if the classification class is an ETIM or not
     *
     * @param classificationClass the given classification class
     * @return the result of the check
     */
    private boolean isEtimClassification(ClassificationClassModel classificationClass) {
        return classificationClass.getCatalogVersion().getCatalog().getId().equals(VanmarckeCoreConstants.ETIM_CLASSIFICATION);
    }

    /**
     * Check if the classification class is an GS1 or not
     *
     * @param classificationClass the given classification class
     * @return the result of the check
     */
    private boolean isGS1Classification(ClassificationClassModel classificationClass) {
        return classificationClass.getCatalogVersion().getCatalog().getId().equals(VanmarckeCoreConstants.GS1_CLASSIFICATION);
    }

    /**
     * Add the Information for the given category to the export line
     *
     * @param exportLine the export line
     * @param category   the given category
     * @param localeNL   the given locale
     */
    private void addCategoryInformation(List<String> exportLine, CategoryModel category, Locale localeNL) {
        exportLine.add(category.getCode());
        exportLine.add(resolveNullValues(category.getName(localeNL)));
    }

    /**
     * Add two empty lines, one for Category ID and one for the localized Name
     *
     * @param exportLine the export line to add to
     */
    private void addEmptyLinesForCategory(List<String> exportLine) {
        exportLine.add(StringUtils.EMPTY);
        exportLine.add(StringUtils.EMPTY);
    }

    /**
     * Resolve null values
     *
     * @param value the value to resolve
     * @return empty in case of null value
     */
    private String resolveNullValues(String value) {
        return value == null ? StringUtils.EMPTY : value;
    }
}
