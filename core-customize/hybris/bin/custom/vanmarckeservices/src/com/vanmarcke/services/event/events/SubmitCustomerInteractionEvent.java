package com.vanmarcke.services.event.events;

import com.vanmarcke.core.model.CustomerInteractionModel;
import de.hybris.platform.servicelayer.event.events.AbstractEvent;

import java.io.Serializable;

/**
 * Event triggered when we want to send an email
 */
public class SubmitCustomerInteractionEvent extends AbstractEvent {

    private CustomerInteractionModel customerInteraction;

    public SubmitCustomerInteractionEvent() {
        super();
    }

    public SubmitCustomerInteractionEvent(final Serializable source) {
        super(source);

        // compatibility!
        if (source instanceof CustomerInteractionModel) {
            this.customerInteraction = (CustomerInteractionModel) source;
        }
    }

    public CustomerInteractionModel getCustomerInteraction() {
        return customerInteraction;
    }

    public void setCustomerInteraction(final CustomerInteractionModel customerInteraction) {
        this.customerInteraction = customerInteraction;
    }
}