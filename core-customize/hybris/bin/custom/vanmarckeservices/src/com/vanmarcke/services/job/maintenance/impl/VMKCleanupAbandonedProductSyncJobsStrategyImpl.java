package com.vanmarcke.services.job.maintenance.impl;

import com.vanmarcke.services.job.maintenance.AbstractVMKCleanupStrategy;
import de.hybris.platform.impex.model.ImpExMediaModel;

/**
 * Cleans up all abandoned Product Sync Jobs.
 * When a sync job hasn't been completed and a new one is started, the new one is set as "NEW" but will never trigger.
 * When these stay in the system, the platform will eventually start to abort new instances that should run as well.
 *
 * @author Niels Raemaekers
 * @since 18-05-2020
 */
public class VMKCleanupAbandonedProductSyncJobsStrategyImpl extends AbstractVMKCleanupStrategy<ImpExMediaModel> {

    private static final String QUERY = "SELECT {cv.pk} FROM {CatalogVersionSyncCronJob AS cv JOIN CronjobStatus AS c ON {c.pk} = {cv.status}} WHERE {c.code} = 'UNKNOWN' AND {cv.creationtime} < ?date";

    /**
     * {@inheritDoc}
     */
    @Override
    protected final String getQuery() {
        return QUERY;
    }
}