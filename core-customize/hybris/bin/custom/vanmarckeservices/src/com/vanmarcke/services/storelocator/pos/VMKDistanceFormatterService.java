package com.vanmarcke.services.storelocator.pos;

/**
 * Service to format a distance double into the correct locale
 */
@FunctionalInterface
public interface VMKDistanceFormatterService {

    /**
     * formats the distance into the correct locale
     *
     * @param distance the distance
     * @return a formatted string with the distance in the correct locale
     */
    String formatDistance(double distance);
}
