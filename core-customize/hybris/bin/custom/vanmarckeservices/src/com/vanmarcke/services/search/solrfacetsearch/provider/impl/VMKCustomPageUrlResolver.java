package com.vanmarcke.services.search.solrfacetsearch.provider.impl;

import com.vanmarcke.services.search.solrfacetsearch.provider.VMKModelUrlResolver;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commerceservices.url.impl.DefaulCustomPageUrlResolver;
import de.hybris.platform.core.model.ItemModel;

import java.util.Locale;

public class VMKCustomPageUrlResolver extends DefaulCustomPageUrlResolver implements VMKModelUrlResolver<ContentPageModel> {

    /**
     * {@inheritDoc}
     */
    @Override
    public String resolveInternal(ContentPageModel item, Locale locale) {
        return item.isHomepage() ? "/" : item.getUid();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean canResolve(ItemModel item) {
        return item instanceof ContentPageModel;
    }
}
