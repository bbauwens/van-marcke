package com.vanmarcke.services.order.credit;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;

/**
 * Custom CreditService Interface
 */
@FunctionalInterface
public interface VMKCreditCheckService {

    /**
     * Validates the creditworthiness for the current cart
     *
     * @param cart the current cart
     * @return <code>true</code> if the cart passed the creditworthiness check, otherwise <code>false</code>
     */
    boolean validateCreditworthinessForCart(AbstractOrderModel cart);
}