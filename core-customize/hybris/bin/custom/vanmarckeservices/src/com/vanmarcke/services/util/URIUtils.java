package com.vanmarcke.services.util;

import java.net.URI;

import static org.apache.commons.lang3.StringUtils.replaceAll;

public class URIUtils {

    private URIUtils() {
        // Hide public constructor
    }

    /**
     * Normalizes this URI's path.
     *
     * @param url the URI's path
     * @return A URI equivalent to this URI, but whose path is in normal form
     */
    public static String normalize(final String url) {
        final URI uri = URI.create(replaceAll(url, "/+", "/"));
        return uri.normalize().toString();
    }
}