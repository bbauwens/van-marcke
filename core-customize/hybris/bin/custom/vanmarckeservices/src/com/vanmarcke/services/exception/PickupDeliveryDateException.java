package com.vanmarcke.services.exception;

import com.vanmarcke.cpi.data.order.PickupDateInfoResponseData;
import com.vanmarcke.cpi.data.order.ShippingDateInfoResponseData;

/**
 * Exception for invalid dates:
 * <ul>
 *     <li>pickup date in {@link PickupDateInfoResponseData}</li>
 *     <li>delivery date in {@link ShippingDateInfoResponseData}</li>
 * </ul>
 *
 * @author Przemyslaw Mirowski
 * @since 22-11-2021
 */
public class PickupDeliveryDateException extends RuntimeException {
    public PickupDeliveryDateException() {
        super();
    }

    public PickupDeliveryDateException(final String message) {
        super(message);
    }

    public PickupDeliveryDateException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
