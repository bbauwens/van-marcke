package com.vanmarcke.services.search.solrfacetsearch.provider.impl;

import com.vanmarcke.core.model.VanMarckeVariantProductModel;
import com.vanmarcke.core.model.VanmarckeCountryChannelModel;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolver;

import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;

/**
 * Value resolver for determining if a product is available in a certain country channel.
 *
 * @author Joris Cryns
 * @since 1-7-2019
 */
public class VMKAvailabilityValueResolver extends AbstractValueResolver<ProductModel, Object, Object> {

    /**
     * {@inheritDoc}
     */
    @Override
    protected void addFieldValues(InputDocument inputDocument,
                                  IndexerBatchContext indexerBatchContext,
                                  IndexedProperty indexedProperty,
                                  ProductModel productModel,
                                  ValueResolverContext<Object, Object> valueResolverContext) throws FieldValueProviderException {

        if (productModel instanceof VanMarckeVariantProductModel) {
            VanMarckeVariantProductModel variantProduct = (VanMarckeVariantProductModel) productModel;
            for (VanmarckeCountryChannelModel vanmarckeCountryChannelModel : variantProduct.getVanmarckeCountryChannel()) {
                if (vanmarckeCountryChannelModel.getCountry() != null && isNotEmpty(vanmarckeCountryChannelModel.getChannels())) {
                    for (SiteChannel c : vanmarckeCountryChannelModel.getChannels()) {
                        addFieldValue(inputDocument, indexerBatchContext, indexedProperty, true, String.format("%s_%s", vanmarckeCountryChannelModel.getCountry().getIsocode().toLowerCase(), c.getCode().toLowerCase()));
                    }
                }
            }
        }
    }
}
