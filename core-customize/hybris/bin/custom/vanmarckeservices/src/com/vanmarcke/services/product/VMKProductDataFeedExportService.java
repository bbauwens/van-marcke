package com.vanmarcke.services.product;

import de.hybris.platform.core.model.product.ProductModel;
import org.w3c.dom.Document;

import javax.xml.parsers.ParserConfigurationException;
import java.util.List;

public interface VMKProductDataFeedExportService {

    /**
     * Generates the product xml datafeed for the given products and locale
     *
     * @param products products to be included in the datafeed
     * @return xml document containing the products
     * @throws ParserConfigurationException
     */
    Document generateProductXml(List<ProductModel> products) throws ParserConfigurationException;
}
