package com.vanmarcke.services.resolvers;

import de.hybris.platform.acceleratorservices.storefront.util.PageTitleResolver;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;

import static com.vanmarcke.services.constants.VanmarckeservicesConstants.PAGE_TITLE_SITE_NAME;

/**
 * Provides method to generate a SEO friendly version of category page title.
 *
 * @author Giani Ifrim
 * @since 08-07-2021
 */
public class VMKPageTitleResolver extends PageTitleResolver {

    /**
     * {@inheritDoc}
     */
    @Override
    public String resolveCategoryPageTitle(final CategoryModel category) {
        return StringEscapeUtils.escapeHtml(category.getName() + TITLE_WORD_SEPARATOR + PAGE_TITLE_SITE_NAME);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String resolveProductPageTitle(final ProductModel product) {
        return StringEscapeUtils.escapeHtml(getProductIdentifier(product) + TITLE_WORD_SEPARATOR + PAGE_TITLE_SITE_NAME);
    }

    /**
     * Returns the {@code product}'s identifier.
     * <p>
     * The product name is returned if available. The product code is returned otherwise.
     *
     * @param product the product
     * @return the product identifier
     */
    private String getProductIdentifier(final ProductModel product) {
        return StringUtils.isEmpty(product.getName()) ? product.getCode() : product.getName();
    }
}
