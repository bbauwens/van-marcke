package com.vanmarcke.services.wishlist2.impl;

public class UpdateWishlistException extends RuntimeException{
    public UpdateWishlistException() {
        super();
    }

    public UpdateWishlistException(String message) {
        super(message);
    }
}
