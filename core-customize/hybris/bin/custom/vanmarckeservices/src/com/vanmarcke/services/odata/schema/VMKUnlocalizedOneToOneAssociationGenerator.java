package com.vanmarcke.services.odata.schema;

import de.hybris.platform.integrationservices.model.DescriptorFactory;
import de.hybris.platform.integrationservices.model.IntegrationObjectItemAttributeModel;
import de.hybris.platform.integrationservices.model.TypeAttributeDescriptor;
import de.hybris.platform.odata2services.odata.schema.association.OneToOneAssociationGenerator;

public class VMKUnlocalizedOneToOneAssociationGenerator extends OneToOneAssociationGenerator {
    private final DescriptorFactory integrationServicesDescriptorFactory;

    public VMKUnlocalizedOneToOneAssociationGenerator(DescriptorFactory integrationServicesDescriptorFactory) {
        this.integrationServicesDescriptorFactory = integrationServicesDescriptorFactory;
    }

    @Override
    public boolean isApplicable(final IntegrationObjectItemAttributeModel itemAttribute) {
        return isApplicable(integrationServicesDescriptorFactory.createTypeAttributeDescriptor(itemAttribute));
    }

    @Override
    public boolean isApplicable(final TypeAttributeDescriptor attributeDescriptor) {
        return attributeDescriptor != null && !attributeDescriptor.isPrimitive() && hasOneUnlocalizedElement(attributeDescriptor);
    }

    @Override
    protected TypeAttributeDescriptor asDescriptor(final IntegrationObjectItemAttributeModel itemAttribute) {
        return integrationServicesDescriptorFactory.createTypeAttributeDescriptor(itemAttribute);
    }

    private boolean hasOneUnlocalizedElement(final TypeAttributeDescriptor attributeDescriptor) {
        return !attributeDescriptor.isCollection() && !attributeDescriptor.isLocalized();
    }
}
