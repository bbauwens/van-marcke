package com.vanmarcke.services.helper;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * The {@link StreamingMarshallerHelper} class can be used to marshall POJOs and write them to an XML file.
 *
 * @param <T> the type to marshall
 * @author Christiaan Janssen
 * @since 10/09/2020
 */
public class StreamingMarshallerHelper<T> {

    private final Marshaller marshaller;
    private final Class<T> type;

    private XMLStreamWriter xmlOut;

    /**
     * Creates a new instance of the {@link StreamingMarshallerHelper} class.
     *
     * @param type the class of the type to marshall
     * @throws JAXBException in case something goes wrong
     */
    public StreamingMarshallerHelper(Class<T> type) throws JAXBException {
        this.type = type;
        this.marshaller = getMarshaller(type);
    }

    /**
     * Creates a file with the given {@code fileName} to write to. An {@link XMLStreamWriter} instance is initialized to
     * write data to the file.
     * <p>
     * A root element is created for the given {@code rootTagName}.
     *
     * @param filename    the file name
     * @param rootTagName the name of the root tag
     * @throws XMLStreamException in case the {@link XMLStreamWriter} instance cannot be created
     * @throws IOException        in case the file cannot be created
     */
    public void open(String filename, String rootTagName) throws XMLStreamException, IOException {
        xmlOut = XMLOutputFactory
                .newFactory()
                .createXMLStreamWriter(new FileOutputStream(filename));
        xmlOut.writeStartDocument();
        xmlOut.writeStartElement(rootTagName);
    }

    /**
     * Writes the given object {@code object} to the file with the given {@code elementTagName}.
     *
     * @param object         the object to write
     * @param elementTagName the tag name to use
     * @throws JAXBException in case the object cannot be written
     */
    public void write(T object, String elementTagName) throws JAXBException {
        JAXBElement<T> element = new JAXBElement<T>(QName.valueOf(elementTagName), type, object);
        marshaller.marshal(element, xmlOut);
    }

    /**
     * Completes the XML file and closed the file.
     *
     * @throws XMLStreamException in case the file cannot be closed
     */
    public void close() throws XMLStreamException {
        xmlOut.writeEndDocument();
        xmlOut.close();
    }

    /**
     * Creates and returns the marshaller
     *
     * @param type the type to marshall
     * @return the marshaller
     * @throws JAXBException in case the marshaller cannot be created
     */
    private Marshaller getMarshaller(Class<T> type) throws JAXBException {
        Marshaller marshaller = JAXBContext.newInstance(type).createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
        return marshaller;
    }
}
