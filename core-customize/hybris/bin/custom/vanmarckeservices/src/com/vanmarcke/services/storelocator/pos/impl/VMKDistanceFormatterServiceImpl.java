package com.vanmarcke.services.storelocator.pos.impl;

import com.vanmarcke.services.storelocator.pos.VMKDistanceFormatterService;
import de.hybris.platform.servicelayer.i18n.I18NService;

import java.text.NumberFormat;
import java.util.Locale;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * Service to format a distance double into the correct locale
 */
public class VMKDistanceFormatterServiceImpl implements VMKDistanceFormatterService {

    private final ConcurrentMap<Locale, NumberFormat> numberFormats = new ConcurrentHashMap<>();

    private final I18NService i18NService;

    /**
     * Creates a new instance of {@link VMKDistanceFormatterServiceImpl}
     *
     * @param i18NService the i18n service
     */
    public VMKDistanceFormatterServiceImpl(final I18NService i18NService) {
        this.i18NService = i18NService;
    }

    @Override
    public String formatDistance(double distance) {
        final NumberFormat numberFormat = getNumberFormat(i18NService.getCurrentLocale());
        return numberFormat.format(distance);
    }

    private NumberFormat getNumberFormat(Locale locale) {
        NumberFormat numberFormat = numberFormats.get(locale);
        if (numberFormat == null) {
            numberFormat = NumberFormat.getNumberInstance(locale);
            numberFormat.setMinimumFractionDigits(2);
            numberFormat.setMaximumFractionDigits(2);
            numberFormats.put(locale, numberFormat);
        }
        return numberFormat;
    }
}