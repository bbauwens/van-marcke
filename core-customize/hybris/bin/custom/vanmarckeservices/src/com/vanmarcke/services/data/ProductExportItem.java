package com.vanmarcke.services.data;

import com.vanmarcke.services.xml.MapAdapter;
import com.vanmarcke.services.xml.MapWrapper;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@XmlRootElement(name = "item")
public class ProductExportItem extends MapWrapper {

    private String articleNumber;
    private Map<String, String> identifier;
    private String approval;
    private String baseProduct;
    private String calculatedStatus;
    private String deliveryMethod;
    private String sparePart;
    private Map<String, String> description;
    private Map<String, String> summary;
    private Map<String, String> name;
    private Map<String, String> keywords;
    private Map<String, String> compliancies;
    private Map<String, String> technicalDataSheet;
    private Map<String, String> baseProductSuperCategoryName;
    private Map<String, String> prices;
    private Map<String, String> brandCategoryName;
    private Map<String, String> urls;
    private String baseProductSuperCategory;
    private String brandCategory;
    private String countryChannels;
    private String productReferences;
    private String categories;
    private String manufacturer;
    private String vmArticleNumber_old;
    private String manufacturerArticleNumber_old;
    private String manufacturerArticleBarcode_old;
    private String vmWarrantyPeriod;
    private String energyClass;
    private String owner;
    private String vmBranch;
    private String technicalSpecification;
    private String brandType;
    private String brandSeries;
    private boolean catalog_VanMarcke_B2B;
    private boolean catalog_VanMarcke_B2C;
    private boolean catalog_DIY;
    private String thumbnailUrl;
    private String vendorItemNumber;
    private String vendorItemBarcode;
    private String vmItemBarcode;
    private String packaging;
    private String contentPackaging;
    private Collection<String> usp;
    private String smallImage;
    private String normalImage;
    private String largeImage;
    private String extraLargeImage;
    private String screenImage;
    private String creationTime;
    private String modifiedTime;
    private String smallTechnicalDrawingImage;
    private String normalTechnicalDrawingImage;
    private String largeTechnicalDrawingImage;
    private String extraLargeTechnicalDrawingImage;
    private String screenTechnicalDrawingImage;
    private String smallEcoLabelImage;
    private String normalEcoLabelImage;
    private String largeEcoLabelImage;
    private String extraLargeEcoLabelImage;
    private String screenEcoLabelImage;
    private Map<String, String> ecoDataSheet;
    private Map<String, String> safetyDataSheet;
    private Map<String, String> generalManual;

    private Map<String, String> normalisation;
    private Map<String, String> productSpecificationSheet;
    private Map<String, String> warranty;
    private Map<String, String> certification;
    private Map<String, String> dop;

    private Map<String, String> instructionManual;
    private Map<String, String> maintenanceManual;
    private Map<String, String> userManual;
    private Map<String, String> sparePartsList;

    private String smallIconImage;
    private String normalIconImage;
    private String largeIconImage;
    private String extraLargeIconImage;
    private String screenIconImage;

    private String smallProductLogoImage;
    private String normalProductLogoImage;
    private String largeProductLogoImage;
    private String extraLargeProductLogoImage;
    private String screenProductLogoImage;

    public ProductExportItem() {
        this.identifier = new HashMap<>();
        this.description = new HashMap<>();
        this.summary = new HashMap<>();
        this.name = new HashMap<>();
        this.keywords = new HashMap<>();
        this.compliancies = new HashMap<>();
        this.usp = new ArrayList<>();
        this.technicalDataSheet = new HashMap<>();
        this.ecoDataSheet = new HashMap<>();
        this.safetyDataSheet = new HashMap<>();
        this.normalisation = new HashMap<>();
        this.productSpecificationSheet = new HashMap<>();
        this.warranty = new HashMap<>();
        this.certification = new HashMap<>();
        this.dop = new HashMap<>();
        this.instructionManual = new HashMap<>();
        this.maintenanceManual = new HashMap<>();
        this.userManual = new HashMap<>();
        this.sparePartsList = new HashMap<>();
        this.generalManual = new HashMap<>();
        this.baseProductSuperCategoryName = new HashMap<>();
        this.prices = new HashMap<>();
        this.brandCategoryName = new HashMap<>();
        this.urls = new HashMap<>();
    }

    @XmlElementWrapper
    @XmlElement(name = "usps")
    public Collection<String> getUsp() {
        return this.usp;
    }

    public void setUsp(final Collection<String> usp) {
        this.usp = usp;
    }

    public String getArticleNumber() {
        return this.articleNumber;
    }

    public void setArticleNumber(final String articleNumber) {
        this.articleNumber = articleNumber;
    }

    @XmlJavaTypeAdapter(MapAdapter.class)
    public Map<String, String> getIdentifier() {
        return this.identifier;
    }

    public void setIdentifier(final Map<String, String> identifier) {
        this.identifier = identifier;
    }

    public String getApproval() {
        return this.approval;
    }

    public void setApproval(final String approval) {
        this.approval = approval;
    }

    public String getBaseProduct() {
        return this.baseProduct;
    }

    public void setBaseProduct(final String baseProduct) {
        this.baseProduct = baseProduct;
    }

    @XmlJavaTypeAdapter(MapAdapter.class)
    public Map<String, String> getDescription() {
        return this.description;
    }

    public void setDescription(final Map<String, String> description) {
        this.description = description;
    }

    @XmlJavaTypeAdapter(MapAdapter.class)
    public Map<String, String> getSummary() {
        return this.summary;
    }

    public void setSummary(final Map<String, String> summary) {
        this.summary = summary;
    }

    @XmlJavaTypeAdapter(MapAdapter.class)
    public Map<String, String> getName() {
        return this.name;
    }

    public void setName(final Map<String, String> name) {
        this.name = name;
    }

    @XmlJavaTypeAdapter(MapAdapter.class)
    public Map<String, String> getKeywords() {
        return this.keywords;
    }

    public void setKeywords(final Map<String, String> keywords) {
        this.keywords = keywords;
    }

    @XmlJavaTypeAdapter(MapAdapter.class)
    public Map<String, String> getCompliancies() {
        return this.compliancies;
    }

    public void setCompliancies(final Map<String, String> compliancies) {
        this.compliancies = compliancies;
    }

    public String getProductReferences() {
        return this.productReferences;
    }

    public void setProductReferences(final String productReferences) {
        this.productReferences = productReferences;
    }

    public String getCategories() {
        return this.categories;
    }

    public void setCategories(final String categories) {
        this.categories = categories;
    }

    public String getManufacturer() {
        return this.manufacturer;
    }

    public void setManufacturer(final String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getVmArticleNumber_old() {
        return this.vmArticleNumber_old;
    }

    public void setVmArticleNumber_old(final String vmArticleNumber_old) {
        this.vmArticleNumber_old = vmArticleNumber_old;
    }

    public String getManufacturerArticleNumber_old() {
        return this.manufacturerArticleNumber_old;
    }

    public void setManufacturerArticleNumber_old(final String manufacturerArticleNumber_old) {
        this.manufacturerArticleNumber_old = manufacturerArticleNumber_old;
    }

    public String getManufacturerArticleBarcode_old() {
        return this.manufacturerArticleBarcode_old;
    }

    public void setManufacturerArticleBarcode_old(final String manufacturerArticleBarcode_old) {
        this.manufacturerArticleBarcode_old = manufacturerArticleBarcode_old;
    }

    public String getVmWarrantyPeriod() {
        return this.vmWarrantyPeriod;
    }

    public void setVmWarrantyPeriod(final String vmWarrantyPeriod) {
        this.vmWarrantyPeriod = vmWarrantyPeriod;
    }

    public String getEnergyClass() {
        return this.energyClass;
    }

    public void setEnergyClass(final String energyClass) {
        this.energyClass = energyClass;
    }

    public String getOwner() {
        return this.owner;
    }

    public void setOwner(final String owner) {
        this.owner = owner;
    }

    public String getVmBranch() {
        return this.vmBranch;
    }

    public void setVmBranch(final String vmBranch) {
        this.vmBranch = vmBranch;
    }

    public String getTechnicalSpecification() {
        return this.technicalSpecification;
    }

    public void setTechnicalSpecification(final String technicalSpecification) {
        this.technicalSpecification = technicalSpecification;
    }

    public String getBrandType() {
        return this.brandType;
    }

    public void setBrandType(final String brandType) {
        this.brandType = brandType;
    }

    public String getBrandSeries() {
        return this.brandSeries;
    }

    public void setBrandSeries(final String brandSeries) {
        this.brandSeries = brandSeries;
    }

    public boolean isCatalog_VanMarcke_B2B() {
        return this.catalog_VanMarcke_B2B;
    }

    public void setCatalog_VanMarcke_B2B(final boolean catalog_VanMarcke_B2B) {
        this.catalog_VanMarcke_B2B = catalog_VanMarcke_B2B;
    }

    public boolean isCatalog_VanMarcke_B2C() {
        return this.catalog_VanMarcke_B2C;
    }

    public void setCatalog_VanMarcke_B2C(final boolean catalog_VanMarcke_B2C) {
        this.catalog_VanMarcke_B2C = catalog_VanMarcke_B2C;
    }

    public boolean isCatalog_DIY() {
        return this.catalog_DIY;
    }

    public void setCatalog_DIY(final boolean catalog_DIY) {
        this.catalog_DIY = catalog_DIY;
    }

    public String getThumbnailUrl() {
        return this.thumbnailUrl;
    }

    public void setThumbnailUrl(final String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public String getVendorItemNumber() {
        return this.vendorItemNumber;
    }

    public void setVendorItemNumber(final String vendorItemNumber) {
        this.vendorItemNumber = vendorItemNumber;
    }

    public String getVendorItemBarcode() {
        return this.vendorItemBarcode;
    }

    public void setVendorItemBarcode(final String vendorItemBarcode) {
        this.vendorItemBarcode = vendorItemBarcode;
    }

    public String getVmItemBarcode() {
        return this.vmItemBarcode;
    }

    public void setVmItemBarcode(final String vmItemBarcode) {
        this.vmItemBarcode = vmItemBarcode;
    }

    public String getPackaging() {
        return this.packaging;
    }

    public void setPackaging(final String packaging) {
        this.packaging = packaging;
    }

    public String getContentPackaging() {
        return this.contentPackaging;
    }

    public void setContentPackaging(final String contentPackaging) {
        this.contentPackaging = contentPackaging;
    }

    @XmlJavaTypeAdapter(MapAdapter.class)
    public Map<String, String> getTechnicalDataSheet() {
        return this.technicalDataSheet;
    }

    public void setTechnicalDataSheet(final Map<String, String> technicalDataSheet) {
        this.technicalDataSheet = technicalDataSheet;
    }

    public String getSmallImage() {
        return this.smallImage;
    }

    public void setSmallImage(final String smallImage) {
        this.smallImage = smallImage;
    }

    public String getNormalImage() {
        return this.normalImage;
    }

    public void setNormalImage(final String normalImage) {
        this.normalImage = normalImage;
    }

    public String getLargeImage() {
        return this.largeImage;
    }

    public void setLargeImage(final String largeImage) {
        this.largeImage = largeImage;
    }

    public String getExtraLargeImage() {
        return extraLargeImage;
    }

    public void setExtraLargeImage(String extraLargeImage) {
        this.extraLargeImage = extraLargeImage;
    }

    public String getScreenImage() {
        return screenImage;
    }

    public void setScreenImage(String screenImage) {
        this.screenImage = screenImage;
    }

    public String getCreationTime() {
        return this.creationTime;
    }

    public void setCreationTime(final String creationTime) {
        this.creationTime = creationTime;
    }

    public String getModifiedTime() {
        return this.modifiedTime;
    }

    public void setModifiedTime(final String modifiedTime) {
        this.modifiedTime = modifiedTime;
    }

    public String getSmallTechnicalDrawingImage() {
        return smallTechnicalDrawingImage;
    }

    public void setSmallTechnicalDrawingImage(final String smallTechnicalDrawingImage) {
        this.smallTechnicalDrawingImage = smallTechnicalDrawingImage;
    }

    public String getNormalTechnicalDrawingImage() {
        return normalTechnicalDrawingImage;
    }

    public void setNormalTechnicalDrawingImage(final String normalTechnicalDrawingImage) {
        this.normalTechnicalDrawingImage = normalTechnicalDrawingImage;
    }

    public String getLargeTechnicalDrawingImage() {
        return largeTechnicalDrawingImage;
    }

    public void setLargeTechnicalDrawingImage(final String largeTechnicalDrawingImage) {
        this.largeTechnicalDrawingImage = largeTechnicalDrawingImage;
    }

    public String getExtraLargeTechnicalDrawingImage() {
        return extraLargeTechnicalDrawingImage;
    }

    public void setExtraLargeTechnicalDrawingImage(String extraLargeTechnicalDrawingImage) {
        this.extraLargeTechnicalDrawingImage = extraLargeTechnicalDrawingImage;
    }

    public String getScreenTechnicalDrawingImage() {
        return screenTechnicalDrawingImage;
    }

    public void setScreenTechnicalDrawingImage(String screenTechnicalDrawingImage) {
        this.screenTechnicalDrawingImage = screenTechnicalDrawingImage;
    }

    public String getSmallEcoLabelImage() {
        return smallEcoLabelImage;
    }

    public void setSmallEcoLabelImage(final String smallEcoLabelImage) {
        this.smallEcoLabelImage = smallEcoLabelImage;
    }

    public String getNormalEcoLabelImage() {
        return normalEcoLabelImage;
    }

    public void setNormalEcoLabelImage(final String normalEcoLabelImage) {
        this.normalEcoLabelImage = normalEcoLabelImage;
    }

    public String getLargeEcoLabelImage() {
        return largeEcoLabelImage;
    }

    public void setLargeEcoLabelImage(final String largeEcoLabelImage) {
        this.largeEcoLabelImage = largeEcoLabelImage;
    }

    public String getExtraLargeEcoLabelImage() {
        return extraLargeEcoLabelImage;
    }

    public void setExtraLargeEcoLabelImage(String extraLargeEcoLabelImage) {
        this.extraLargeEcoLabelImage = extraLargeEcoLabelImage;
    }

    public String getScreenEcoLabelImage() {
        return screenEcoLabelImage;
    }

    public void setScreenEcoLabelImage(String screenEcoLabelImage) {
        this.screenEcoLabelImage = screenEcoLabelImage;
    }

    @XmlJavaTypeAdapter(MapAdapter.class)
    public Map<String, String> getEcoDataSheet() {
        return ecoDataSheet;
    }

    public void setEcoDataSheet(final Map<String, String> ecoDataSheet) {
        this.ecoDataSheet = ecoDataSheet;
    }

    @XmlJavaTypeAdapter(MapAdapter.class)
    public Map<String, String> getSafetyDataSheet() {
        return safetyDataSheet;
    }

    public void setSafetyDataSheet(final Map<String, String> safetyDataSheet) {
        this.safetyDataSheet = safetyDataSheet;
    }

    @XmlJavaTypeAdapter(MapAdapter.class)
    public Map<String, String> getNormalisation() {
        return normalisation;
    }

    public void setNormalisation(final Map<String, String> normalisation) {
        this.normalisation = normalisation;
    }

    @XmlJavaTypeAdapter(MapAdapter.class)
    public Map<String, String> getProductSpecificationSheet() {
        return productSpecificationSheet;
    }

    public void setProductSpecificationSheet(final Map<String, String> productSpecificationSheet) {
        this.productSpecificationSheet = productSpecificationSheet;
    }

    @XmlJavaTypeAdapter(MapAdapter.class)
    public Map<String, String> getWarranty() {
        return warranty;
    }

    @XmlJavaTypeAdapter(MapAdapter.class)
    public Map<String, String> getCertification() {
        return certification;
    }

    public void setWarranty(final Map<String, String> warranty) {
        this.warranty = warranty;
    }

    public void setCertification(final Map<String, String> certification) {
        this.certification = certification;
    }

    @XmlJavaTypeAdapter(MapAdapter.class)
    public Map<String, String> getDop() {
        return dop;
    }

    public void setDop(final Map<String, String> dop) {
        this.dop = dop;
    }

    @XmlJavaTypeAdapter(MapAdapter.class)
    public Map<String, String> getInstructionManual() {
        return instructionManual;
    }

    public void setInstructionManual(final Map<String, String> instructionManual) {
        this.instructionManual = instructionManual;
    }

    @XmlJavaTypeAdapter(MapAdapter.class)
    public Map<String, String> getMaintenanceManual() {
        return maintenanceManual;
    }

    public void setMaintenanceManual(final Map<String, String> maintenanceManual) {
        this.maintenanceManual = maintenanceManual;
    }

    @XmlJavaTypeAdapter(MapAdapter.class)
    public Map<String, String> getUserManual() {
        return userManual;
    }

    public void setUserManual(final Map<String, String> userManual) {
        this.userManual = userManual;
    }

    @XmlJavaTypeAdapter(MapAdapter.class)
    public Map<String, String> getSparePartsList() {
        return sparePartsList;
    }

    public void setSparePartsList(final Map<String, String> sparePartsList) {
        this.sparePartsList = sparePartsList;
    }

    public String getSmallIconImage() {
        return smallIconImage;
    }

    public void setSmallIconImage(final String smallIconImage) {
        this.smallIconImage = smallIconImage;
    }

    public String getNormalIconImage() {
        return normalIconImage;
    }

    public void setNormalIconImage(final String normalIconImage) {
        this.normalIconImage = normalIconImage;
    }

    public String getLargeIconImage() {
        return largeIconImage;
    }

    public void setLargeIconImage(final String largeIconImage) {
        this.largeIconImage = largeIconImage;
    }

    public String getExtraLargeIconImage() {
        return extraLargeIconImage;
    }

    public void setExtraLargeIconImage(String extraLargeIconImage) {
        this.extraLargeIconImage = extraLargeIconImage;
    }

    public String getScreenIconImage() {
        return screenIconImage;
    }

    public void setScreenIconImage(String screenIconImage) {
        this.screenIconImage = screenIconImage;
    }

    public String getSmallProductLogoImage() {
        return smallProductLogoImage;
    }

    public void setSmallProductLogoImage(final String smallProductLogoImage) {
        this.smallProductLogoImage = smallProductLogoImage;
    }

    public String getNormalProductLogoImage() {
        return normalProductLogoImage;
    }

    public void setNormalProductLogoImage(final String normalProductLogoImage) {
        this.normalProductLogoImage = normalProductLogoImage;
    }

    public String getLargeProductLogoImage() {
        return largeProductLogoImage;
    }

    public void setLargeProductLogoImage(final String largeProductLogoImage) {
        this.largeProductLogoImage = largeProductLogoImage;
    }

    public String getExtraLargeProductLogoImage() {
        return extraLargeProductLogoImage;
    }

    public void setExtraLargeProductLogoImage(String extraLargeProductLogoImage) {
        this.extraLargeProductLogoImage = extraLargeProductLogoImage;
    }

    public String getScreenProductLogoImage() {
        return screenProductLogoImage;
    }

    public void setScreenProductLogoImage(String screenProductLogoImage) {
        this.screenProductLogoImage = screenProductLogoImage;
    }

    @XmlJavaTypeAdapter(MapAdapter.class)
    public Map<String, String> getGeneralManual() {
        return generalManual;
    }

    public void setGeneralManual(final Map<String, String> generalManual) {
        this.generalManual = generalManual;
    }

    public String getCountryChannels() {
        return countryChannels;
    }

    public void setCountryChannels(String countryChannels) {
        this.countryChannels = countryChannels;
    }

    public String getCalculatedStatus() {
        return calculatedStatus;
    }

    public void setCalculatedStatus(String calculatedStatus) {
        this.calculatedStatus = calculatedStatus;
    }

    public String getDeliveryMethod() {
        return deliveryMethod;
    }

    public void setDeliveryMethod(String deliveryMethod) {
        this.deliveryMethod = deliveryMethod;
    }

    public String getSparePart() {
        return sparePart;
    }

    public void setSparePart(String sparePart) {
        this.sparePart = sparePart;
    }

    @XmlJavaTypeAdapter(MapAdapter.class)
    public Map<String, String> getBaseProductSuperCategoryName() {
        return baseProductSuperCategoryName;
    }

    public void setBaseProductSuperCategoryName(Map<String, String> baseProductSuperCategoryName) {
        this.baseProductSuperCategoryName = baseProductSuperCategoryName;
    }

    @XmlJavaTypeAdapter(MapAdapter.class)
    public Map<String, String> getPrices() {
        return prices;
    }

    public void setPrices(Map<String, String> prices) {
        this.prices = prices;
    }

    @XmlJavaTypeAdapter(MapAdapter.class)
    public Map<String, String> getBrandCategoryName() {
        return brandCategoryName;
    }

    public void setBrandCategoryName(Map<String, String> brandCategoryName) {
        this.brandCategoryName = brandCategoryName;
    }

    @XmlJavaTypeAdapter(MapAdapter.class)
    public Map<String, String> getUrls() {
        return urls;
    }

    public void setUrls(Map<String, String> urls) {
        this.urls = urls;
    }

    public String getBaseProductSuperCategory() {
        return baseProductSuperCategory;
    }

    public void setBaseProductSuperCategory(String baseProductSuperCategory) {
        this.baseProductSuperCategory = baseProductSuperCategory;
    }

    public String getBrandCategory() {
        return brandCategory;
    }

    public void setBrandCategory(String brandCategory) {
        this.brandCategory = brandCategory;
    }
}