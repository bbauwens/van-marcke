package com.vanmarcke.services.consent;

import com.vanmarcke.core.model.ConsentParagraphComponentModel;

import java.util.List;

/**
 * The {@link VMKConsentParagraphComponentService} interface exposes the business logic concerning instances of the
 * {@link ConsentParagraphComponentModel} class.
 *
 * @author Christiaan Janssen
 * @since 16-07-2020
 */
public interface VMKConsentParagraphComponentService {

    /**
     * Returns the {@link ConsentParagraphComponentModel} instances.
     *
     * @return the {@link ConsentParagraphComponentModel} instances
     */
    List<ConsentParagraphComponentModel> getConsentParagraphs();
}
