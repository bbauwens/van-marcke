package com.vanmarcke.services.product;

import de.hybris.platform.catalog.model.CatalogVersionModel;

import java.util.List;

/**
 * Service for exporting non-archived Variants
 *
 * @author Niels Raemaekers
 * @since 12-05-2020
 */
public interface VMKNonArchivedVariantsExportService {

    /**
     * Generate the export feed
     *
     * @param catalogVersion the given catalog version
     * @return the generated export feed
     */
    List<String> generateNonArchivedVariantsExportFeed(CatalogVersionModel catalogVersion);
}
