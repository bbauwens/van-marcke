package com.vanmarcke.services.order.cart.conversion;

import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.io.StringReader;

public interface VMKCsvExcelConversionService {

    /**
     * This method is used to export carts as an Excel file. It converts a cart from CSV format to a XLS file.
     * <p>
     * Converts the given {@link StringReader} to a {@code Byte Array}.
     *
     * @param csv the buffered csv
     * @return the byte array
     */
    byte[] convertCsvToXls(StringReader csv);

    /**
     * This method is used to export carts as an Excel file. It converts a cart from CSV format to a XLS file.
     * <p>
     * Converts the given {@link StringReader} to a {@code Byte Array}.
     *
     * @param csv the buffered csv
     * @param currencyColumns column indexes that need to be formatted as currency
     * @return the byte array
     */
    byte[] convertCsvToXls(StringReader csv, int[] currencyColumns);


    /**
     * This method is used to import a cart. It converts the imported excel file to a csv file.
     * <p>
     * It converts the given {@link MultipartFile} to an {@link InputStream}
     *
     * @param sheet the multi part file
     * @return the input stream
     */
    InputStream convertXlsToCsv(MultipartFile sheet);

}
