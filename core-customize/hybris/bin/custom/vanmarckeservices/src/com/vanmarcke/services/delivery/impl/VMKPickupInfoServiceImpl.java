package com.vanmarcke.services.delivery.impl;

import com.vanmarcke.core.helper.CheckoutDateHelper;
import com.vanmarcke.cpi.data.order.EntryInfoResponseData;
import com.vanmarcke.cpi.data.order.PickupDateInfoResponseData;
import com.vanmarcke.services.delivery.VMKDeliveryInfoService;
import com.vanmarcke.services.delivery.VMKFirstDateAvailabilityService;
import com.vanmarcke.services.delivery.VMKPickupInfoService;
import com.vanmarcke.services.storelocator.pos.VMKPointOfServiceService;
import com.vanmarcke.services.storelocator.schedule.VMKOpeningScheduleService;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import org.apache.commons.collections4.CollectionUtils;

import java.util.Date;
import java.util.Objects;
import java.util.Optional;

/**
 * Provides methods related to the pickup delivery method.
 *
 * @author Tom van den Berg
 * @since 30-12-2020
 */
public class VMKPickupInfoServiceImpl implements VMKPickupInfoService {

    private final VMKPointOfServiceService pointOfServiceService;
    private final VMKDeliveryInfoService deliveryInfoService;
    private final CheckoutDateHelper checkoutDateHelper;
    private final VMKFirstDateAvailabilityService vmkFirstDateAvailabilityService;
    private final VMKOpeningScheduleService openingScheduleService;


    /**
     * Provides an instance of the VMKPickupInfoServiceImpl.
     *
     * @param pointOfServiceService           the point of service service
     * @param deliveryInfoService             the delivery info service
     * @param vmkFirstDateAvailabilityService the first available day service
     * @param openingScheduleService          the opening schedule service
     */
    public VMKPickupInfoServiceImpl(VMKPointOfServiceService pointOfServiceService, VMKDeliveryInfoService deliveryInfoService, CheckoutDateHelper checkoutDateHelper, VMKFirstDateAvailabilityService vmkFirstDateAvailabilityService,
                                    VMKOpeningScheduleService openingScheduleService) {
        this.pointOfServiceService = pointOfServiceService;
        this.deliveryInfoService = deliveryInfoService;
        this.checkoutDateHelper = checkoutDateHelper;
        this.vmkFirstDateAvailabilityService = vmkFirstDateAvailabilityService;
        this.openingScheduleService = openingScheduleService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isPickupPossible(CartModel cart) {
        return hasPickupDate(cart)
                || isTecCollectPossible(cart)
                || hasAlternativeTecStores(cart);
    }

    /**
     * {@inheritDoc}
     *
     * @return
     */
    @Override
    public Optional<Date> getFirstPossiblePickupDate(final CartModel cart) {
        return deliveryInfoService.getPickupDateInformation(cart)
                .flatMap(pickupDateResponse -> getFirstPossiblePickupDate(cart, pickupDateResponse));
    }

    @Override
    public Optional<Date> getFirstPossiblePickupDate(final CartModel cart, final PickupDateInfoResponseData pickupDateResponse) {
        ServicesUtil.validateParameterNotNullStandardMessage("pickupDateResponse", pickupDateResponse);

        if (allProductsHavePickupDate(pickupDateResponse)) {
            return CollectionUtils.emptyIfNull(pickupDateResponse.getProducts()).stream()
                    .map(EntryInfoResponseData::getPickupDate)
                    .min(Date::compareTo)
                    .map(vmkFirstDateAvailabilityService::getFirstValidNoClosingDate)
                    .filter(checkoutDateHelper::isValidDate);
        }
        return Optional.empty();
    }

    @Override
    public Optional<Date> getLastPossiblePickupDate(final PickupDateInfoResponseData pickupDateResponse) {
        return pickupDateResponse.getProducts().stream()
                .map(EntryInfoResponseData::getPickupDate)
                .filter(Objects::nonNull)
                .max(Date::compareTo);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isTecCollectPossible(final CartModel cart) {
        return deliveryInfoService.getPickupDateInformation(cart)
                .map(pickupDateResponse -> isTecCollectPossible(cart, pickupDateResponse))
                .orElse(false);
    }

    @Override
    public boolean isTecCollectPossible(final CartModel cart, final PickupDateInfoResponseData pickupDateResponse) {
        final PointOfServiceModel pointOfService = cart.getDeliveryPointOfService();

        if (pickupDateResponse != null && pointOfServiceService != null) {
            final Date tecCollectDate = pickupDateResponse.getExpressPickupDate();
            return tecCollectDate != null && openingScheduleService.isStoreOpenOnGivenDate(pointOfService, tecCollectDate);
        }
        return false;
    }

    /**
     * Determine wheter pickup is possible for the given cart.
     *
     * @param cart the cart
     * @return true if pickup is possible
     */
    private boolean hasPickupDate(final CartModel cart) {
        return getFirstPossiblePickupDate(cart).isPresent();
    }

    /**
     * Determine whether there are alternative tec stores for the given cart.
     *
     * @param cart the cart
     * @return true if there are alternative tec stores
     */
    private boolean hasAlternativeTecStores(CartModel cart) {
        return CollectionUtils.isNotEmpty(pointOfServiceService.getAlternativeTECStoresWhichAreCurrentlyOpenForCart(cart));
    }

    private boolean allProductsHavePickupDate(final PickupDateInfoResponseData pickupDateResponse) {
        if (CollectionUtils.isNotEmpty(pickupDateResponse.getProducts())) {
            return pickupDateResponse.getProducts().stream()
                    .map(EntryInfoResponseData::getPickupDate)
                    .noneMatch(Objects::isNull);
        }
        return false;
    }
}
