package com.vanmarcke.services.exportcart;

import com.vanmarcke.facades.data.order.SavedCartData;
import de.hybris.platform.core.model.user.UserModel;

import java.util.List;

/**
 * Service for exporting saved baskets and wishlists to pdf or excel
 *
 * @author Cristi Stoica
 */
public interface VMKExportSavedCartsService {

    byte[] generatePdf(List<String> headers, UserModel user, SavedCartData savedCartData, boolean includeNet, boolean showImages, boolean includeTechnical, int emptyLines);
}
