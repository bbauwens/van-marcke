package com.vanmarcke.services.config;

import de.hybris.platform.servicelayer.config.ConfigurationService;

/**
 * The {@link VMKConfigurationService} interface defines the methods to read configuration properties.
 *
 * @author Christiaan Janssen
 * @since 14-07-2020
 */
public interface VMKConfigurationService extends ConfigurationService {

    /**
     * Returns the value of the configuration property with the given {@code key}.
     *
     * @param key the key
     * @return the value
     */
    String getString(String key, String defaultValue);

    /**
     * Returns the value of the configuration property with the given {@code key}.
     *
     * @param key the key
     * @return the value
     */
    Boolean getBoolean(String key, Boolean defaultValue);
}
