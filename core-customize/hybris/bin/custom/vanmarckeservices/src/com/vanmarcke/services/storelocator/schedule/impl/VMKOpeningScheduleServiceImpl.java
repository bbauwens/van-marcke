package com.vanmarcke.services.storelocator.schedule.impl;

import com.vanmarcke.core.pos.VMKPointOfServiceDao;
import com.vanmarcke.services.storelocator.schedule.VMKOpeningScheduleService;
import com.vanmarcke.services.util.VMKDateUtils;
import de.hybris.platform.basecommerce.enums.WeekDay;
import de.hybris.platform.storelocator.model.OpeningDayModel;
import de.hybris.platform.storelocator.model.OpeningScheduleModel;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import de.hybris.platform.storelocator.model.SpecialOpeningDayModel;
import de.hybris.platform.storelocator.model.WeekdayOpeningDayModel;
import org.apache.commons.lang.time.DateUtils;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Optional;

import static com.vanmarcke.services.constants.VanmarckeservicesConstants.OPEN_DAY_SEARCH_LIMIT;
import static com.vanmarcke.services.constants.VanmarckeservicesConstants.WEEK_DAYS;
import static java.util.Objects.isNull;

/**
 * Service related to openingSchedule
 */
public class VMKOpeningScheduleServiceImpl implements VMKOpeningScheduleService {

    private final VMKPointOfServiceDao pointOfServiceDao;

    public VMKOpeningScheduleServiceImpl(VMKPointOfServiceDao pointOfServiceDao) {
        this.pointOfServiceDao = pointOfServiceDao;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OpeningDayModel getOpeningDayFromScheduleForGivenDay(OpeningScheduleModel openingSchedule, WeekDay givenWeekDay) {
        if (openingSchedule != null) {
            return openingSchedule.getOpeningDays().stream()
                    .filter(WeekdayOpeningDayModel.class::isInstance)
                    .map(WeekdayOpeningDayModel.class::cast)
                    .filter(e -> givenWeekDay.equals(e.getDayOfWeek()))
                    .findFirst()
                    .orElse(null);
        }
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isStoreOpenOnSaturday(PointOfServiceModel pointOfService) {
        if (isNull(pointOfService)) {
            return false;
        }
        return pointOfService
                .getOpeningSchedule()
                .getOpeningDays()
                .stream()
                .filter(WeekdayOpeningDayModel.class::isInstance)
                .map(WeekdayOpeningDayModel.class::cast)
                .anyMatch(e -> WeekDay.SATURDAY.equals(e.getDayOfWeek()));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isStoreOpenToday(PointOfServiceModel pos) {
        return isStoreOpenOnGivenDate(pos, new Date());
    }


    @Override
    public boolean isStoreOpenOnGivenDate(final String pointOfServiceName, final Date date) {
        return isStoreOpenOnGivenDate(pointOfServiceDao.getPosByName(pointOfServiceName), date);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isStoreOpenOnGivenDate(PointOfServiceModel pos, Date date) {
        OpeningScheduleModel openingSchedule = pos.getOpeningSchedule();

        LocalTime time = null;
        if (DateUtils.isSameDay(date, VMKDateUtils.getTodaysDate())) {
            time = LocalTime.now();
        }

        SpecialOpeningDayModel specialOpeningDay = getSpecialOpeningDay(openingSchedule.getOpeningDays(), date);
        if (specialOpeningDay != null) {
            return isOpenForSpecialOpeningDay(specialOpeningDay, time);
        }

        WeekdayOpeningDayModel weekdayOpeningDay = getWeekOpeningDay(openingSchedule.getOpeningDays(), date);
        if (weekdayOpeningDay != null) {
            return isOpenForWeekOpeningDay(weekdayOpeningDay, time);
        }

        return false;
    }

    @Override
    public Optional<Date> findOpenPickupDateForPointOfService(final PointOfServiceModel pointOfService, final Date lastPossiblePickupDate) {
        int counter = 0;
        Date validPickupDate = lastPossiblePickupDate;

        while (validPickupDate != null && !isStoreOpenOnGivenDate(pointOfService, validPickupDate) && counter < OPEN_DAY_SEARCH_LIMIT) {
            validPickupDate = DateUtils.addDays(validPickupDate, 1);
            counter++;
        }
        return counter < OPEN_DAY_SEARCH_LIMIT ? Optional.ofNullable(validPickupDate) : Optional.empty();
    }

    /**
     * Returns the special opening day for the given {@code date}.
     * <p>
     * This methods returns {@code null} if no special opening day exists for the given date.
     *
     * @param openingDays the opening days
     * @param date        the date
     * @return the {@link SpecialOpeningDayModel} instance or {@code null}
     */
    private SpecialOpeningDayModel getSpecialOpeningDay(Collection<OpeningDayModel> openingDays, Date date) {
        for (OpeningDayModel openingDay : openingDays) {
            if (openingDay instanceof SpecialOpeningDayModel) {
                SpecialOpeningDayModel specialOpeningDay = (SpecialOpeningDayModel) openingDay;
                if (specialOpeningDay.getDate().compareTo(date) == 0) {
                    return specialOpeningDay;
                }
            }
        }
        return null;
    }

    /**
     * Returns the week opening day for the given {@code date}.
     * <p>
     * This methods returns {@code null} if no special opening day exists for the given date.
     *
     * @param openingDays the opening days
     * @param date        the date
     * @return the {@link WeekdayOpeningDayModel} instance or {@code null}
     */
    private WeekdayOpeningDayModel getWeekOpeningDay(Collection<OpeningDayModel> openingDays, Date date) {
        for (OpeningDayModel openingDay : openingDays) {
            if (openingDay instanceof WeekdayOpeningDayModel) {
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);

                WeekdayOpeningDayModel weekOpeningDay = (WeekdayOpeningDayModel) openingDay;
                if (weekOpeningDay.getDayOfWeek().equals(WEEK_DAYS[calendar.get(Calendar.DAY_OF_WEEK) - 1])) {
                    return weekOpeningDay;
                }
            }
        }
        return null;
    }

    /**
     * Checks whether the point of service is open on the given {@code openingDay} and {@code time}.
     *
     * @param openingDay the opening day
     * @param time       the time
     * @return {@code true} in case the point of service is open on the given {@code openingDay} and {@code time},
     * {@code false} otherwise
     */
    private boolean isOpenForSpecialOpeningDay(SpecialOpeningDayModel openingDay, LocalTime time) {
        return !openingDay.isClosed() && isBeforeClosingTime(openingDay, time);
    }

    /**
     * Checks whether the point of service is open on the given {@code openingDay} and {@code time}.
     *
     * @param openingDay the opening day
     * @param time       the time
     * @return {@code true} in case the point of service is open on the given {@code openingDay} and {@code time},
     * {@code false} otherwise
     */
    private boolean isOpenForWeekOpeningDay(WeekdayOpeningDayModel openingDay, LocalTime time) {
        return isBeforeClosingTime(openingDay, time);
    }

    /**
     * Checks whether the point of service is open on the given {@code openingDay} and {@code time}.
     *
     * @param openingDay the opening day
     * @param time       the time
     * @return {@code true} in case the point of service is open on the given {@code openingDay} and {@code time},
     * {@code false} otherwise
     */
    protected boolean isBeforeClosingTime(OpeningDayModel openingDay, LocalTime time) {
        Date closing = openingDay.getClosingTime();

        if (time == null) {
            return closing != null;
        }

        if (closing == null) {
            return false;
        }

        LocalTime closingTime = LocalDateTime
                .ofInstant(closing.toInstant(), ZoneId.systemDefault())
                .toLocalTime()
                .minusHours(1);

        return time.isBefore(closingTime);
    }
}
