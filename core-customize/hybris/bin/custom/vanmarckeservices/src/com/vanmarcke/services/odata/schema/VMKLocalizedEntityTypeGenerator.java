package com.vanmarcke.services.odata.schema;

import de.hybris.platform.integrationservices.model.DescriptorFactory;
import de.hybris.platform.integrationservices.model.IntegrationObjectItemModel;
import de.hybris.platform.integrationservices.model.TypeAttributeDescriptor;
import de.hybris.platform.integrationservices.model.TypeDescriptor;
import de.hybris.platform.odata2services.odata.schema.KeyGenerator;
import de.hybris.platform.odata2services.odata.schema.entity.EntityTypeGenerator;
import de.hybris.platform.odata2services.odata.schema.entity.LocalizedEntityTypeGenerator;
import de.hybris.platform.odata2services.odata.schema.navigation.NavigationPropertyListGeneratorRegistry;
import de.hybris.platform.odata2services.odata.schema.property.AbstractPropertyListGenerator;
import org.apache.olingo.odata2.api.edm.provider.EntityType;
import org.apache.olingo.odata2.api.edm.provider.NavigationProperty;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static de.hybris.platform.odata2services.constants.Odata2servicesConstants.LOCALIZED_ENTITY_TYPE_PREFIX;

public class VMKLocalizedEntityTypeGenerator extends LocalizedEntityTypeGenerator implements EntityTypeGenerator {
    private DescriptorFactory descriptorFactory;
    private KeyGenerator keyGenerator;
    private AbstractPropertyListGenerator propertiesGenerator;
    private NavigationPropertyListGeneratorRegistry registry;

    public VMKLocalizedEntityTypeGenerator(DescriptorFactory descriptorFactory, KeyGenerator keyGenerator, AbstractPropertyListGenerator propertiesGenerator, NavigationPropertyListGeneratorRegistry registry) {
        this.descriptorFactory = descriptorFactory;
        this.keyGenerator = keyGenerator;
        this.propertiesGenerator = propertiesGenerator;
        this.registry = registry;
    }

    @Override
    protected EntityType generateEntityType(final IntegrationObjectItemModel item) {
        final List<NavigationProperty> navigationProperties = registry.generate(item.getAttributes());
        final List<NavigationProperty> additionalNavigationProperties = registry.generate(descriptorFactory.createItemTypeDescriptor(item));

        return super.generateEntityType(item)
                .setNavigationProperties(combine(navigationProperties, additionalNavigationProperties));
    }

    @Override
    protected boolean isApplicable(final IntegrationObjectItemModel item) {
        return containsLocalizedAttributes(item);
    }

    @Override
    protected String generateEntityTypeName(final IntegrationObjectItemModel item) {
        return LOCALIZED_ENTITY_TYPE_PREFIX + item.getCode();
    }

    private List<NavigationProperty> combine(final List<NavigationProperty> navigationProperties, final List<NavigationProperty> additionalNavigationProperties) {
        final List<NavigationProperty> combinedNavigationProperties = new ArrayList<>(navigationProperties);
        final List<String> navigationPropertyNames = navigationProperties.stream().map(NavigationProperty::getName).collect(Collectors.toList());
        additionalNavigationProperties.stream()
                .filter(np -> !navigationPropertyNames.contains(np.getName()))
                .forEach(combinedNavigationProperties::add);
        return combinedNavigationProperties;
    }

    private boolean containsLocalizedAttributes(final IntegrationObjectItemModel item) {
        final TypeDescriptor typeDescriptor = descriptorFactory.createItemTypeDescriptor(item);
        return typeDescriptor.getAttributes().stream()
                .anyMatch(TypeAttributeDescriptor::isLocalized);
    }
}
