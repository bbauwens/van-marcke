package com.vanmarcke.services.order.cart.impl;

import com.elision.hybris.l10n.facades.LocalizedMessageFacade;
import com.vanmarcke.services.order.cart.VMKTecCollectReservationMessageService;
import com.vanmarcke.services.storelocator.schedule.VMKOpeningScheduleService;
import com.vanmarcke.services.util.VMKDateUtils;
import de.hybris.platform.core.model.order.CartModel;
import org.apache.commons.lang.StringUtils;

import java.time.LocalDate;
import java.util.Date;

import static java.time.DayOfWeek.SATURDAY;
import static java.time.DayOfWeek.SUNDAY;

/**
 * Provides methods which generate a Tec collect reservation message to be shown on the storefront in certain cases.
 *
 * @author Tom van den Berg
 * @since 30-12-2020
 */
public class VMKTecCollectReservationMessageServiceImpl implements VMKTecCollectReservationMessageService {

    private static final String TEC_COLLECT_MESSAGE_KEY = "checkout.tec.collect.message";

    private final LocalizedMessageFacade messageFacade;
    private final VMKOpeningScheduleService openingScheduleService;

    /**
     * Provides an instance of the VMKTecCollectReservationMessageServiceImpl.
     *
     * @param messageFacade          the message facade
     * @param openingScheduleService the opening schedule service
     */
    public VMKTecCollectReservationMessageServiceImpl(LocalizedMessageFacade messageFacade, VMKOpeningScheduleService openingScheduleService) {
        this.messageFacade = messageFacade;
        this.openingScheduleService = openingScheduleService;
    }

    @Override
    public String generateTecCollectReservationMessage(final CartModel cart, final Date pickupDate, final boolean tecCollectPossible) {
        if (tecCollectPossible && shouldShowTecCollectSuggestionMessage(cart, pickupDate)) {
            return messageFacade.getMessageForCodeAndCurrentLocale(TEC_COLLECT_MESSAGE_KEY);
        }
        return StringUtils.EMPTY;
    }

    /**
     * Determines whether whether TEC collect info message has to be shown for given cart and pickup date.
     *
     * @param cart       the cart
     * @param pickupDate the pickup date
     * @return {@code true} if message to be shown
     */
    private boolean shouldShowTecCollectSuggestionMessage(final CartModel cart, final Date pickupDate) {
        if (pickupDate != null) {
            final LocalDate nextOpeningDay = getNextOpeningDay(cart, getTecCollectDate());
            final LocalDate pickupLocalDate = VMKDateUtils.createLocalDate(pickupDate);

            return pickupLocalDate.isAfter(nextOpeningDay);
        }
        return false;
    }

    /**
     * Returns the next day (from today) when the store will be open, taking into account whether a specific store is open on saturday.
     *
     * @param tecCollectDate the TEC collect date
     * @param cart           the cart
     * @return the next opening day
     */
    protected LocalDate getNextOpeningDay(CartModel cart, LocalDate tecCollectDate) {
        LocalDate result = tecCollectDate.plusDays(1);

        if (SATURDAY.equals(result.getDayOfWeek())
                && !openingScheduleService.isStoreOpenOnSaturday(cart.getDeliveryPointOfService())) {
            return result.plusDays(2);
        }

        if (SUNDAY.equals(result.getDayOfWeek())) {
            return result.plusDays(1);
        }
        return result;
    }

    /**
     * Returns the date of today.
     *
     * @return localdate
     */
    protected LocalDate getTecCollectDate() {
        // This method was added for testing purposes
        return LocalDate.now();
    }
}
