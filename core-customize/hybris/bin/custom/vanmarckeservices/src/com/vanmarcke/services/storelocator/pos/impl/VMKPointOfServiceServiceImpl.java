package com.vanmarcke.services.storelocator.pos.impl;

import com.vanmarcke.core.pos.VMKPointOfServiceDao;
import com.vanmarcke.cpi.data.order.AlternativeTECRequestData;
import com.vanmarcke.cpi.data.order.AlternativeTECResponseData;
import com.vanmarcke.cpi.services.ESBAlternativeTECService;
import com.vanmarcke.services.storelocator.pos.VMKPointOfServiceService;
import com.vanmarcke.services.storelocator.schedule.VMKOpeningScheduleService;
import de.hybris.platform.commerceservices.store.data.GeoPoint;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import de.hybris.platform.storelocator.pos.impl.DefaultPointOfServiceService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.util.Assert;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Contains business logic related to {@link PointOfServiceModel} instances.
 *
 * @author Taki Korovessis
 * @since 19-8-2019
 */
public class VMKPointOfServiceServiceImpl extends DefaultPointOfServiceService implements VMKPointOfServiceService {

    private static final String CURRENT_POINT_OF_SERVICE = "currentPointOfService";

    private final SessionService sessionService;
    private final VMKOpeningScheduleService openingScheduleService;
    private final ESBAlternativeTECService esbAlternativeTECService;
    private final Converter<Map<String, Long>, AlternativeTECRequestData> alternativeTECRequestDataConverter;
    private final VMKPointOfServiceDao pointOfServiceDao;

    /**
     * Constructor for {@link VMKPointOfServiceServiceImpl}
     *
     * @param sessionService                     the session service
     * @param openingScheduleService             the opening schedule service
     * @param esbAlternativeTECService           the esb alternative TEC service
     * @param alternativeTECRequestDataConverter the alternative TEC request converter
     * @param pointOfServiceDao                  the point of service DAO
     */
    public VMKPointOfServiceServiceImpl(SessionService sessionService,
                                        VMKOpeningScheduleService openingScheduleService,
                                        ESBAlternativeTECService esbAlternativeTECService,
                                        Converter<Map<String, Long>, AlternativeTECRequestData> alternativeTECRequestDataConverter,
                                        VMKPointOfServiceDao pointOfServiceDao) {
        this.sessionService = sessionService;
        this.openingScheduleService = openingScheduleService;
        this.esbAlternativeTECService = esbAlternativeTECService;
        this.alternativeTECRequestDataConverter = alternativeTECRequestDataConverter;
        this.pointOfServiceDao = pointOfServiceDao;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCurrentPointOfService(PointOfServiceModel pointOfService) {
        sessionService.setAttribute(CURRENT_POINT_OF_SERVICE, pointOfService);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PointOfServiceModel getCurrentPointOfService() {
        return sessionService.getAttribute(CURRENT_POINT_OF_SERVICE);
    }

    @Override
    public PointOfServiceModel getCurrentPointOfService(final CartModel cart) {
        return Optional.ofNullable(cart.getDeliveryPointOfService())
                .orElseGet(this::getCurrentPointOfService);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PointOfServiceModel getPointOfServiceForName(String storeID) {
        List<PointOfServiceModel> pointOfServices =
                getPointOfServicesForStoreIDs(Collections.singletonList(storeID));
        return pointOfServices.stream().findFirst().orElse(null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<PointOfServiceModel> getPointOfServicesForStoreIDs(List<String> storeIDs) {
        Assert.notEmpty(storeIDs, "No TEC store id is present.");
        return pointOfServiceDao.getPointOfServicesForIDs(storeIDs);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Cacheable(cacheManager = "vanMarckeCacheManager", value = "alternativeTecStoresRequestCache", keyGenerator = "vmkDeliveryDateRequestKeyGenerator", unless = "#result.isEmpty()")
    public List<PointOfServiceModel> getAlternativeTECStoresWhichAreCurrentlyOpenForCart(final CartModel cart) {
        final Map<String, Long> productQuantitiesMap = cart.getEntries().stream()
                .collect(Collectors.toMap(e -> e.getProduct().getCode(), AbstractOrderEntryModel::getQuantity));

        return getAlternativeTECStoresWhichAreCurrentlyOpen(productQuantitiesMap);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<PointOfServiceModel> getAlternativeTECStoresWhichAreCurrentlyOpen(final Map<String, Long> productQuantitiesMap) {
        return getAlternativeTECStores(productQuantitiesMap).stream()
                .filter(openingScheduleService::isStoreOpenToday)
                .collect(Collectors.toList());
    }

    /**
     * Returns the alternative TEC stores for the given {@code cart}.
     *
     * @param productQuantitiesMap the map with products and their quantities
     * @return the alternative TEC stores
     */
    protected List<PointOfServiceModel> getAlternativeTECStores(final Map<String, Long> productQuantitiesMap) {
        if (MapUtils.isNotEmpty(productQuantitiesMap)) {
            final AlternativeTECRequestData request = alternativeTECRequestDataConverter.convert(productQuantitiesMap);
            final AlternativeTECResponseData response = esbAlternativeTECService.getAlternativeTECInformation(request);

            if (response != null && CollectionUtils.isNotEmpty(response.getAlternativeStoreIDs())) {
                return pointOfServiceDao.getPointOfServicesForIDs(response.getAlternativeStoreIDs());
            }
        }
        return Collections.emptyList();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isPointOfServiceOpenOnSaturday(PointOfServiceModel pointOfService) {
        return openingScheduleService.isStoreOpenOnSaturday(pointOfService);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public GeoPoint getGeoPoint(PointOfServiceModel source) {
        GeoPoint geoPoint = new GeoPoint();

        if (source.getLatitude() != null) {
            geoPoint.setLatitude(source.getLatitude());
        }
        if (source.getLongitude() != null) {
            geoPoint.setLongitude(source.getLongitude());
        }
        return geoPoint;
    }
}
