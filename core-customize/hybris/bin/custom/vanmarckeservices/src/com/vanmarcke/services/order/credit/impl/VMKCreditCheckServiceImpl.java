package com.vanmarcke.services.order.credit.impl;

import com.vanmarcke.cpi.data.order.CreditCheckResponseData;
import com.vanmarcke.cpi.data.order.OrderRequestData;
import com.vanmarcke.cpi.services.ESBCreditCheckService;
import com.vanmarcke.services.order.credit.VMKCreditCheckService;
import com.vanmarcke.services.util.VMKDateUtils;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Date;

/**
 * Default implementation of VMKCreditCheckService
 */
public class VMKCreditCheckServiceImpl implements VMKCreditCheckService {

    private final Converter<AbstractOrderModel, OrderRequestData> orderRequestConverter;
    private final ESBCreditCheckService esbCreditCheckService;
    private final ModelService modelService;

    /**
     * Creates a new instance of {@link VMKCreditCheckServiceImpl}
     *
     * @param orderRequestConverter the order request converter
     * @param esbCreditCheckService the ESB credit check service
     */
    public VMKCreditCheckServiceImpl(final Converter<AbstractOrderModel, OrderRequestData> orderRequestConverter,
                                     final ESBCreditCheckService esbCreditCheckService,
                                     final ModelService modelService) {
        this.orderRequestConverter = orderRequestConverter;
        this.esbCreditCheckService = esbCreditCheckService;
        this.modelService = modelService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean validateCreditworthinessForCart(final AbstractOrderModel cart) {
        final OrderRequestData request = orderRequestConverter.convert(cart);
        final CreditCheckResponseData response = this.esbCreditCheckService.getCreditCheckInformation(request);

        boolean result = false;
        cart.setCreditLimitWarning(false);
        cart.setCreditLimitExceeded(false);

        if (response != null && response.getData() != null && response.getData().getCreditworthy() != null) {
            switch (response.getData().getCreditworthy()) {
                case NOT_CREDIT_WORTHY:
                    break;
                case CREDIT_WORTHY:
                    result = true;
                    break;
                case CREDIT_LIMIT_EXCEEDED:
                    cart.setCreditLimitExceeded(true);
                    if (cart.getDeliveryMode() != null && cart.getDeliveryMode().getCode().contains("tec")) {
                        result = true;
                    } else if (cart.getDeliveryDate() != null && calculateWorkingDaysBetween(new Date(), cart.getDeliveryDate()) >= 1) {
                        result = true;
                        cart.setCreditLimitWarning(true);
                    }
            }
        }

        if (modelService.isModified(cart)) {
            modelService.save(cart);
        }

        return result;
    }

    protected long calculateWorkingDaysBetween(Date from, Date to) {
        return VMKDateUtils.getWorkingDaysBetween(from, to);
    }
}