package com.vanmarcke.services.order.cart.impl;

import com.vanmarcke.services.delivery.VMKShippingInfoService;
import com.vanmarcke.services.exception.PickupDeliveryDateException;
import com.vanmarcke.services.exception.UnsupportedDeliveryDateException;
import com.vanmarcke.services.order.cart.VMKCommerceCartService;
import com.vanmarcke.services.product.VMKVariantProductService;
import com.vanmarcke.services.strategies.VMKUpdateCartCheckoutModeStrategy;
import de.hybris.platform.commerceservices.order.CommerceCartMergingException;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartRestoration;
import de.hybris.platform.commerceservices.order.CommerceCartRestorationException;
import de.hybris.platform.commerceservices.order.impl.DefaultCommerceCartService;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.variants.model.VariantProductModel;
import org.springframework.beans.factory.annotation.Required;

import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;
import static java.util.Collections.emptyList;

/**
 * Custom implementation for the CommerceCartService
 */
public class VMKCommerceCartServiceImpl extends DefaultCommerceCartService implements VMKCommerceCartService {

    private VMKUpdateCartCheckoutModeStrategy updateCartCheckoutModeStrategy;
    private final VMKVariantProductService variantProductService;
    private final VMKShippingInfoService shippingInfoService;

    /**
     * Creates a new instance of the {@link VMKCommerceCartServiceImpl} class.
     *
     * @param variantProductService the variant product service
     * @param shippingInfoService   the shipping info service
     */
    public VMKCommerceCartServiceImpl(final VMKVariantProductService variantProductService,
                                      final VMKShippingInfoService shippingInfoService) {
        this.variantProductService = variantProductService;
        this.shippingInfoService = shippingInfoService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<CommerceCartModification> addToCart(final List<CommerceCartParameter> parameterList)
            throws CommerceCartMergingException {
        return getCommerceAddToCartStrategy().addToCart(removeNonPurchasableEntries(parameterList));
    }

    /**
     * Removes the non-purchasable products from the given {@code params}.
     *
     * @param params the add-to-cart parameters
     * @return the filtered add-to-cart parameters
     */
    private List<CommerceCartParameter> removeNonPurchasableEntries(List<CommerceCartParameter> params) {
        List<CommerceCartParameter> result = new ArrayList<>();
        for (CommerceCartParameter param : params) {
            ProductModel product = param.getProduct();
            if (product instanceof VariantProductModel && variantProductService.isPurchasable((VariantProductModel) product)) {
                result.add(param);
            }
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CommerceCartRestoration restoreCart(final CommerceCartParameter parameters) throws CommerceCartRestorationException {
        final CartModel cartModel = parameters.getCart();
        final List<CommerceCartModification> modifications = cartModel != null ? getCartValidationStrategy().validateCart(parameters) : emptyList();
        final CommerceCartRestoration restoration = super.restoreCart(parameters);
        restoration.getModifications().addAll(modifications);
        return restoration;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateCartCheckoutMode(final CommerceCartParameter parameter) {
        updateCartCheckoutModeStrategy.updateCartCheckoutMode(parameter);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setRequestedDeliveryDate(final CommerceCartParameter parameter, final Date requestedDeliveryDate) throws UnsupportedDeliveryDateException {
        validateParameterNotNullStandardMessage("requestedDeliveryDate", requestedDeliveryDate);
        final CartModel cartModel = parameter.getCart();
        validateParameterNotNull(cartModel, "Cart model cannot be null");

        final Date firstPossibleDeliveryDate = shippingInfoService.getFirstPossibleShippingDateForCompleteCart(cartModel);

        if (firstPossibleDeliveryDate == null) {
            throw new PickupDeliveryDateException(MessageFormat.format("Unable to validate requested delivery date, cannot determine first possible delivery date for products in cart {0}.",
                    parameter.getCart().getCode()));
        }

        final LocalDateTime requestedDate = requestedDeliveryDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        final LocalDateTime firstPossibleDate = firstPossibleDeliveryDate.toInstant().atZone(ZoneId.of("UTC")).toLocalDateTime();

        if (isValidRequestDeliveryDateForCompleteCart(firstPossibleDate, requestedDate)) {
            final Date actualDate = Date.from(requestedDate.atZone(ZoneId.of("UTC")).toInstant());
            cartModel.setDeliveryDate(actualDate);
            cartModel.getEntries().forEach(entry -> {
                entry.setDeliveryDate(actualDate);
                getModelService().save(entry);
            });
            getModelService().save(cartModel);
        } else {
            throw new UnsupportedDeliveryDateException(
                    MessageFormat.format("The requested delivery date is invalid. The first possible delivery date is: {0}.",
                            new SimpleDateFormat("yyyy-MM-dd").format(firstPossibleDeliveryDate)));
        }
    }

    /**
     * Checks whether the requested delivery date is valid.
     *
     * @param firstPossibleDeliveryDate the first possible delivery date
     * @param requestedDeliveryDate     the requested delivery date
     * @return {@code true} in case the requested delivery date is valid, {@code false} otherwise
     */
    private boolean isValidRequestDeliveryDateForCompleteCart(final LocalDateTime firstPossibleDeliveryDate,
                                                              final LocalDateTime requestedDeliveryDate) {
        return !requestedDeliveryDate.isBefore(firstPossibleDeliveryDate)
                && !requestedDeliveryDate.getDayOfWeek().equals(DayOfWeek.SATURDAY)
                && !requestedDeliveryDate.getDayOfWeek().equals(DayOfWeek.SUNDAY);
    }

    /**
     * Note: this dependency can not be wired via dependency injection.
     * This leads to a circular dependency while the application context is initializing.
     */
    @Required
    public void setUpdateCartCheckoutModeStrategy(VMKUpdateCartCheckoutModeStrategy updateCartCheckoutModeStrategy) {
        this.updateCartCheckoutModeStrategy = updateCartCheckoutModeStrategy;
    }
}