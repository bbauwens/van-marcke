package com.vanmarcke.services.cronjob;

import com.vanmarcke.core.model.VMKRemoveInactivatedProductReferencesCronJobModel;
import com.vanmarcke.core.references.VMKProductReferencesDao;
import de.hybris.platform.catalog.model.ProductReferenceModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.model.ModelService;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Cronjob that deals with cleaning up inactive product references
 *
 * @author Niels Raemaekers
 * @since 12-05-2020
 */
public class VMKRemoveInactivatedProductReferencesJobPerformable extends AbstractJobPerformable<VMKRemoveInactivatedProductReferencesCronJobModel> {

    private static final Logger LOG = LoggerFactory.getLogger(VMKRemoveInactivatedProductReferencesJobPerformable.class);

    private final VMKProductReferencesDao vmkProductReferencesDao;
    private final ModelService modelService;

    /**
     * Constructor for {@link VMKRemoveInactivatedProductReferencesJobPerformable}
     *
     * @param vmkProductReferencesDao the {@link VMKProductReferencesDao}
     * @param modelService            the {@link ModelService}
     */
    public VMKRemoveInactivatedProductReferencesJobPerformable(VMKProductReferencesDao vmkProductReferencesDao, ModelService modelService) {
        this.vmkProductReferencesDao = vmkProductReferencesDao;
        this.modelService = modelService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PerformResult perform(VMKRemoveInactivatedProductReferencesCronJobModel vmkRemoveInactivatedProductReferencesCronJob) {
        List<ProductReferenceModel> inactiveProductReferences = vmkProductReferencesDao.findAllInactiveProductReferences();

        if (CollectionUtils.isNotEmpty(inactiveProductReferences)) {
            LOG.info("Removing {} inactive product references", inactiveProductReferences.size());
            modelService.removeAll(inactiveProductReferences);
        }

        return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
    }
}
