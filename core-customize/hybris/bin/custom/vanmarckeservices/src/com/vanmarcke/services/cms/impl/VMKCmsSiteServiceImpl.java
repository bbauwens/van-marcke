package com.vanmarcke.services.cms.impl;

import com.vanmarcke.core.cms.VMKCMSSiteDao;
import com.vanmarcke.services.cms.VMKCmsSiteService;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.impl.DefaultCMSSiteService;
import de.hybris.platform.commerceservices.enums.SiteChannel;

import java.util.List;
import java.util.stream.Collectors;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;
import static org.apache.commons.lang3.BooleanUtils.isTrue;

/**
 * Custom implementation of DefaultCMSSiteService
 */
public class VMKCmsSiteServiceImpl extends DefaultCMSSiteService implements VMKCmsSiteService {

    /**
     * {@inheritDoc}
     */
    @Override
    public List<CMSSiteModel> getSupportedSites(final SiteChannel channel) {
        validateParameterNotNullStandardMessage("channel", channel);

        final List<CMSSiteModel> sites = ((VMKCMSSiteDao) getCmsSiteDao()).findAllSitesForChannel(channel);
        return sites.stream()
                .filter(e -> isTrue(e.getActive()))
                .filter(e -> isTrue(e.getEcommerceEnabled()))
                .collect(Collectors.toList());
    }
}