package com.vanmarcke.services.search.solrfacetsearch.provider.impl;

import de.hybris.platform.catalog.jalo.classification.ClassAttributeAssignment;
import de.hybris.platform.catalog.jalo.classification.util.Feature;
import de.hybris.platform.catalog.jalo.classification.util.FeatureContainer;
import de.hybris.platform.catalog.model.ProductFeatureModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.product.Product;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;
import de.hybris.platform.solrfacetsearch.provider.impl.ClassificationPropertyValueProvider;
import de.hybris.platform.solrfacetsearch.provider.impl.ValueProviderParameterUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class VMKClassificationProductFeatureValueProvider extends ClassificationPropertyValueProvider {
    private static final Logger LOG = LoggerFactory.getLogger(VMKClassificationProductFeatureValueProvider.class);

    @Override
    public Collection<FieldValue> getFieldValues(IndexConfig indexConfig, IndexedProperty indexedProperty, Object model)
            throws FieldValueProviderException {
        final String qualifierExpression = ValueProviderParameterUtils.getString(indexedProperty, "qualifierExpression", "");
        if (model instanceof ProductModel) {
            List<FieldValue> finalFieldValues = new ArrayList<>();
            final Product product = modelService.getSource(model);
            for (ProductFeatureModel featureModel : ((ProductModel) model).getFeatures()) {
                if (!qualifierExpression.isEmpty() && featureModel.getQualifier().matches(qualifierExpression)) {
                    final ClassAttributeAssignment classAttributeAssignment = modelService.getSource(featureModel.getClassificationAttributeAssignment());
                    final FeatureContainer cont = FeatureContainer.loadTyped(product, classAttributeAssignment);
                    if (cont.hasFeature(classAttributeAssignment)) {
                        Feature feature = cont.getFeature(classAttributeAssignment);
                        if (feature != null && !feature.isEmpty()) {
                            finalFieldValues.addAll(getFeaturesValues(indexConfig, feature, indexedProperty));
                        }
                    }
                }
            }

            finalFieldValues = finalFieldValues.stream()
                    .filter(distinctByKey(fieldValue -> fieldValue.getFieldName() + "_" + fieldValue.getValue()))
                    .collect(Collectors.toList());

            if (LOG.isDebugEnabled()) {
                LOG.debug(product.getPK() + ": " + finalFieldValues);
            }

            return finalFieldValues;
        }
        throw new FieldValueProviderException("Cannot provide product feature's property of non-product item");
    }

    private static <T> Predicate<T> distinctByKey(Function<? super T, Object> keyExtractor)
    {
        Map<Object, Boolean> map = new ConcurrentHashMap<>();
        return t -> map.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }
}
