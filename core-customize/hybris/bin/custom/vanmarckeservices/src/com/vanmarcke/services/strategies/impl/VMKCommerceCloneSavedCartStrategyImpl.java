package com.vanmarcke.services.strategies.impl;

import de.hybris.platform.commerceservices.order.CommerceSaveCartException;
import de.hybris.platform.commerceservices.order.impl.AbstractCommerceCloneSavedCartStrategy;
import de.hybris.platform.commerceservices.service.data.CommerceSaveCartParameter;
import de.hybris.platform.commerceservices.service.data.CommerceSaveCartResult;
import de.hybris.platform.core.model.order.CartModel;

/**
 * Custom strategy which clones saved carts.
 *
 * @author Taki Korovessis
 * @since 18-10-2019
 */
public class VMKCommerceCloneSavedCartStrategyImpl extends AbstractCommerceCloneSavedCartStrategy {

    /**
     * {@inheritDoc}
     */
    @Override
    public CommerceSaveCartResult cloneSavedCart(CommerceSaveCartParameter parameter) throws CommerceSaveCartException {
        CommerceSaveCartResult cloneCartResult = new CommerceSaveCartResult();

        this.beforeCloneSaveCart(parameter);

        CartModel clonedCart = getCartService().clone(null, null, parameter.getCart(), null);
        clonedCart.setPaymentTransactions(null);
        clonedCart.setCode(null);
        clonedCart.setName(null);
        clonedCart.setDescription(null);
        clonedCart.setSavedBy(null);
        clonedCart.setSaveTime(null);
        cloneCartResult.setSavedCart(clonedCart);
        getModelService().save(clonedCart);

        this.afterCloneSaveCart(parameter, cloneCartResult);

        return cloneCartResult;
    }
}