package com.vanmarcke.services.product;

import java.util.List;

/**
 * This interface defines methods to handle cross selling retrievals.
 *
 * @author Tom van den Berg
 * @since 11-06-2020
 */
public interface VMKCrossSellingLookupService {

    /**
     * Returns the cross-selling products for the given {@code code}.
     *
     * @param code the product code
     * @return the list of cross-selling products
     */
    List<String> getCrossSellingProducts(String code);
}
