package com.vanmarcke.services.factories;

import com.vanmarcke.services.product.VMKStockService;

/**
 * Factory interface which defines methods for retrieving {@link VMKStockService} instances.
 *
 * @author Tom van den Berg
 * @since 18-12-2020
 */
public interface VMKStockServiceFactory {

    /**
     * Retrieves the applicable {@link VMKStockService} for the current base store.
     *
     * @return the stock service
     */
    VMKStockService getStockService();
}
