package com.vanmarcke.services.consent.impl;

import com.vanmarcke.core.enums.ConsentType;
import com.vanmarcke.core.model.VMKConsentLogEntryModel;
import com.vanmarcke.core.model.VMKConsentModel;
import com.vanmarcke.services.consent.VMKConsentService;
import com.vanmarcke.services.util.ClientUtils;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import org.apache.commons.collections4.CollectionUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Implements the {@link VMKConsentService}.
 *
 * @author Tom van den Berg
 * @since 12-06-2020
 */
public class VMKConsentServiceImpl implements VMKConsentService {

    private final ModelService modelService;
    private final UserService userService;

    /**
     * Provides an instance of the {@code DefaultVMKConsentService}.
     *
     * @param modelService the model service
     * @param userService  the user service
     */
    public VMKConsentServiceImpl(ModelService modelService,
                                 UserService userService) {
        this.modelService = modelService;
        this.userService = userService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void processConsents(List<VMKConsentModel> consents, HttpServletRequest request) {
        UserModel currentUser = userService.getCurrentUser();
        if (userService.isAnonymousUser(currentUser)) {
            consents.forEach(consent -> createLogEntry(consent, request));
        } else {
            List<VMKConsentModel> existingConsents = ((CustomerModel) currentUser).getConsents();
            if (CollectionUtils.isEmpty(existingConsents)) {
                createConsents(consents, (CustomerModel) currentUser, request);
            } else {
                updateConsents(consents, existingConsents, (CustomerModel) currentUser, request);
            }
        }
    }

    /**
     * Creates the given {@code consents}.
     *
     * @param consents the consents
     * @param customer the customer
     * @param request  the request
     */
    private void createConsents(List<VMKConsentModel> consents, CustomerModel customer, HttpServletRequest request) {
        for (VMKConsentModel consent : consents) {
            consent.setCustomer(customer);
            modelService.save(consent);

            createLogEntry(consent, request);
        }
    }

    /**
     * Updates the {@code existingConsents} with the given {@code consents}.
     *
     * @param consents         the new consents
     * @param existingConsents the existing consents
     */
    private void updateConsents(List<VMKConsentModel> consents, List<VMKConsentModel> existingConsents, CustomerModel customer, HttpServletRequest request) {
        for (VMKConsentModel consent : consents) {
            boolean flag = false;
            for (VMKConsentModel existingConsent : existingConsents) {
                if (existingConsent.getConsentType().equals(consent.getConsentType())) {
                    existingConsent.setOptType(consent.getOptType());
                    modelService.save(existingConsent);

                    createLogEntry(existingConsent, request);

                    flag = true;
                    break;
                }
            }

            if (!flag) {
                createConsents(Collections.singletonList(consent), customer, request);
            }
        }
    }

    /**
     * Creates and stores a {@link VMKConsentLogEntryModel} instance for the given {@code consent} and {@code request}.
     *
     * @param consent the consent
     * @param request the request
     */
    private void createLogEntry(VMKConsentModel consent, HttpServletRequest request) {
        VMKConsentLogEntryModel logEntry = modelService.create(VMKConsentLogEntryModel.class);
        logEntry.setConsentDate(new Date());
        logEntry.setIpAddress(ClientUtils.getClientIp(request));
        logEntry.setBrowserUserAgent(request.getHeader("User-Agent"));
        logEntry.setJSessionID(request.getSession().getId());
        logEntry.setUrlConsentGiven(String.valueOf(request.getRequestURL()));
        logEntry.setConsentType(consent.getConsentType());
        logEntry.setOptType(consent.getOptType());
        modelService.save(logEntry);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<VMKConsentModel> getConsents() {
        UserModel currentUser = userService.getCurrentUser();
        if (!userService.isAnonymousUser(currentUser)) {
            return ((CustomerModel) currentUser).getConsents();
        }
        return Collections.emptyList();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<String> getConsentTypes() {
        return Arrays
                .stream(ConsentType.values())
                .map(ConsentType::getCode)
                .collect(Collectors.toList());
    }
}
