package com.vanmarcke.services.order.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import org.springframework.cache.interceptor.KeyGenerator;

import java.lang.reflect.Method;

import static java.lang.String.format;

/**
 * Generates keys for caching delivery/pickup date requests.
 *
 * @author Tom van den Berg
 * @since 3-07-2020
 */
public class VMKDeliveryDateRequestKeyGeneratorImpl implements KeyGenerator {

    private static final String KEY_FORMAT = "%s-%s-%s";

    private final UserService userService;
    private final SessionService sessionService;

    /**
     * Creates a new instance of the {@link VMKDeliveryDateRequestKeyGeneratorImpl} class.
     *
     * @param userService    the user service
     * @param sessionService the session server
     */
    public VMKDeliveryDateRequestKeyGeneratorImpl(UserService userService, SessionService sessionService) {
        this.userService = userService;
        this.sessionService = sessionService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object generate(Object object, Method method, Object... params) {
        if (params[0] instanceof CartModel) {
            CartModel cart = (CartModel) params[0];

            return format(KEY_FORMAT,
                    cart.getUser().getUid(),
                    formatSKUsAndQuantity(cart),
                    getPointOfService(cart));
        }
        throw new IllegalArgumentException("Parameter must be of type de.hybris.platform.core.model.order.CartModel");
    }

    /**
     * Returns the point of service code for the given {@code cart}.
     *
     * @param cart the cart
     * @return the point of service
     */
    private String getPointOfService(CartModel cart) {
        String pos;
        if (cart.getDeliveryPointOfService() != null) {
            pos = cart.getDeliveryPointOfService().getName();
        } else if (!userService.getCurrentUser().equals(userService.getAnonymousUser())) {
            pos = userService.getCurrentUser().getSessionStore().getName();
        } else {
            PointOfServiceModel pointOfService = sessionService.getCurrentSession().getAttribute("currentPointOfService");
            pos = pointOfService.getName();
        }
        return pos;
    }

    /**
     * Formats a string containing all the SKUs in the cart and their respective quantities.
     *
     * @param cart the cart
     * @return the formatted string
     */
    private String formatSKUsAndQuantity(CartModel cart) {
        StringBuilder result = new StringBuilder();
        for (AbstractOrderEntryModel entry : cart.getEntries()) {
            result.append(entry.getProduct().getCode())
                    .append(":")
                    .append(entry.getQuantity())
                    .append(";");
        }
        return result.toString();
    }
}
