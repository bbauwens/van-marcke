package com.vanmarcke.services.consent;

import com.vanmarcke.core.model.VMKConsentModel;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Defines methods related to {@link VMKConsentModel} instances.
 *
 * @author Tom van den Berg
 * @since 12-06-2020
 */
public interface VMKConsentService {

    /**
     * Processes the given {@code consents}.
     * <p>
     * Log entries are created for the given {@code consents} and {@code request}.
     * <p>
     * If the current user is a registered user, the consents will be linked to the account. This will make it possible
     * to manage the consents in the future.
     *
     * @param consents the consents
     * @param request  the HTTP request
     */
    void processConsents(List<VMKConsentModel> consents, HttpServletRequest request);

    /**
     * Returns the consents for the current user.
     *
     * @return the consents
     */
    List<VMKConsentModel> getConsents();

    /**
     * Returns the consent types.
     *
     * @return the consent types
     */
    List<String> getConsentTypes();
}
