package com.vanmarcke.services.factories.impl;

import com.vanmarcke.services.factories.VMKAbstractSiteChannelAwareFactory;
import com.vanmarcke.services.factories.VMKCartValidationStrategyFactory;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.CartModel;

import java.util.List;

public class VMKCartValidationStrategyFactoryImpl extends VMKAbstractSiteChannelAwareFactory<VMKCartValidationStrategyFactory> implements VMKCartValidationStrategyFactory {

    @Override
    public List<CommerceCartModification> validateCart(final CartModel cartModel) {
        return get().validateCart(cartModel);
    }

    @Override
    public List<CommerceCartModification> validateCart(final CommerceCartParameter parameter) {
        return get().validateCart(parameter);
    }

    @Override
    public void cleanCart(final CartModel cleanCart) {
        get().cleanCart(cleanCart);
    }
}