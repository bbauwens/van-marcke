package com.vanmarcke.services.basestore;

import com.vanmarcke.core.model.IBMConfigurationModel;
import de.hybris.platform.sap.core.configuration.model.SAPConfigurationModel;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

/**
 * Extends the {@link BaseStoreService} to define custom methods.
 *
 * @author Tom van den Berg
 * @since 16-10-2020
 */
public interface VMKBaseStoreService extends BaseStoreService {

    /**
     * Determines whether the given {@link BaseStoreModel}
     * uses {@link IBMConfigurationModel}.
     *
     * @return true if it uses IBM configuration
     */
    boolean isIBMBaseStore(BaseStoreModel baseStore);

    /**
     * Determines whether the given {@link BaseStoreModel}
     * uses {@link SAPConfigurationModel}.
     *
     * @return true if it uses IBM configuration
     */
    boolean isSAPBaseStore(BaseStoreModel baseStore);

    /**
     * Retrieves the base url for the external CPI server.
     *
     * @return the CPI base url
     */
    String getBaseUrl();

    /**
     * Retrieves the IBM A/S400 - CPI username credential
     *
     * @return the username
     */
    String getUsername();

    /**
     * Retrieves the IBM A/S400 - CPI password credential
     *
     * @return the password
     */
    String getPassword();

}
