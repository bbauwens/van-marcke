package com.vanmarcke.services.search.solrfacetsearch.provider.impl;

import com.vanmarcke.core.model.BrandCategoryModel;
import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.search.solrfacetsearch.provider.impl.DefaultCategorySource;
import org.apache.commons.lang3.StringUtils;

import java.util.Collection;
import java.util.Set;

import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;

/**
 * Source for categories.
 *
 * @author Cuypers
 * @since 6-9-2019
 */
public class VMKCategorySource extends DefaultCategorySource {

    private boolean includeBrandCategory;
    private boolean excludeRootCategory;

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isBlockedCategory(final CategoryModel category) {
        if (category instanceof ClassificationClassModel) {
            return !isIncludeClassificationClasses();
        } else if (category instanceof BrandCategoryModel) {
            return !isIncludeBrandCategory();
        } else {
            return false;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Collection<CategoryModel> getAllCategories(CategoryModel directCategory, Set<CategoryModel> rootCategories) {
        final Collection<CategoryModel> allCategories = super.getAllCategories(directCategory, rootCategories);
        if (excludeRootCategory && isNotEmpty(allCategories)) {
            allCategories.removeIf(c -> StringUtils.equals(c.getCode(), getRootCategory()));
        }
        return allCategories;
    }

    public boolean isIncludeBrandCategory() {
        return includeBrandCategory;
    }

    public void setIncludeBrandCategory(final boolean includeBrandCategory) {
        this.includeBrandCategory = includeBrandCategory;
    }

    public void setExcludeRootCategory(boolean excludeRootCategory) {
        this.excludeRootCategory = excludeRootCategory;
    }
}
