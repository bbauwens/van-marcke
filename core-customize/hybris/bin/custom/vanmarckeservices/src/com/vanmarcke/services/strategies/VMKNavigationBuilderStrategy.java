package com.vanmarcke.services.strategies;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.cms2.model.navigation.CMSNavigationNodeModel;

import java.util.List;

/**
 * A Strategy for building the navigation module
 */
public interface VMKNavigationBuilderStrategy {

    /**
     * Gets a list of {@link CMSNavigationNodeModel} which are relevant for the given site.
     * Empty {@link CMSNavigationNodeModel} are discarded.
     *
     * @param baseSite               the baseSite
     * @param cmsNavigationNodeModel the cmsNavigationNodeModel
     * @return a list of {@link CMSNavigationNodeModel}
     */
    List<CMSNavigationNodeModel> buildNavigation(final BaseSiteModel baseSite, final CMSNavigationNodeModel cmsNavigationNodeModel);
}