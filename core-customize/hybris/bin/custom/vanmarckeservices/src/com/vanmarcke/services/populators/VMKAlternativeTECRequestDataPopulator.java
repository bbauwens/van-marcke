package com.vanmarcke.services.populators;

import com.vanmarcke.cpi.data.order.AlternativeTECLineItemRequestData;
import com.vanmarcke.cpi.data.order.AlternativeTECRequestData;
import com.vanmarcke.services.b2b.VMKB2BUnitService;
import com.vanmarcke.services.basestore.VMKBaseStoreService;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static java.util.stream.Collectors.toList;

/**
 * Populates an {@link AlternativeTECRequestData} instance with from a {@link CartModel}.
 *
 * @author Niels Raemaekers
 * @since 10-3-2020
 */
public class VMKAlternativeTECRequestDataPopulator implements Populator<Map<String, Long>, AlternativeTECRequestData> {

    private final VMKBaseStoreService vmkBaseStoreService;
    private final VMKB2BUnitService b2bUnitService;

    /**
     * Constructor for {@link VMKAlternativeTECRequestDataPopulator}
     *
     * @param vmkBaseStoreService the base store service
     * @param b2bUnitService
     */
    public VMKAlternativeTECRequestDataPopulator(VMKBaseStoreService vmkBaseStoreService, VMKB2BUnitService b2bUnitService) {
        this.vmkBaseStoreService = vmkBaseStoreService;
        this.b2bUnitService = b2bUnitService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void populate(Map<String, Long> source, AlternativeTECRequestData target) {
        BaseStoreModel baseStore = vmkBaseStoreService.getCurrentBaseStore();

        if (Objects.nonNull(baseStore)) {
            List<PointOfServiceModel> pointOfServicesForCurrentCountry = baseStore.getPointsOfService();
            target.setStoreIDs(pointOfServicesForCurrentCountry.stream()
                    .map(PointOfServiceModel::getName)
                    .collect(toList()));
            target.setLineItems(getLineItemDataForCurrentBasket(source));
            target.setCustomerID(b2bUnitService.getCustomerB2BUnitID());
        }
    }

    /**
     * Get Line Item data for the given cart
     *
     * @param productQuantitiesMap the product and Quantities Map
     * @return a List with the AlternativeTECLineItemRequest Data
     */
    protected List<AlternativeTECLineItemRequestData> getLineItemDataForCurrentBasket(Map<String, Long> productQuantitiesMap) {
        List<AlternativeTECLineItemRequestData> allLineItems = new ArrayList<>();

        productQuantitiesMap.entrySet().stream()
                .forEach(e -> {
                    AlternativeTECLineItemRequestData lineItem = new AlternativeTECLineItemRequestData();
                    lineItem.setProductID(e.getKey());
                    lineItem.setQuantity(e.getValue());
                    allLineItems.add(lineItem);
                });

        return allLineItems;
    }
}
