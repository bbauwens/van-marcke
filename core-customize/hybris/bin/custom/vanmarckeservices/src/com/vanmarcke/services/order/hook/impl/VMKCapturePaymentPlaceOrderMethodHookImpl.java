package com.vanmarcke.services.order.hook.impl;

import com.vanmarcke.services.strategies.VMKCommercePaymentCaptureStrategy;
import de.hybris.platform.commerceservices.order.hook.CommercePlaceOrderMethodHook;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.commerceservices.service.data.CommerceOrderResult;
import de.hybris.platform.order.InvalidCartException;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkState;
import static de.hybris.platform.b2bacceleratorservices.enums.CheckoutPaymentType.CARD;

public class VMKCapturePaymentPlaceOrderMethodHookImpl implements CommercePlaceOrderMethodHook {

    private final VMKCommercePaymentCaptureStrategy commercePaymentCaptureStrategy;

    /**
     * Creates a new instance of {@link VMKCapturePaymentPlaceOrderMethodHookImpl}
     *
     * @param commercePaymentCaptureStrategy the commercePaymentCaptureStrategy
     */
    public VMKCapturePaymentPlaceOrderMethodHookImpl(final VMKCommercePaymentCaptureStrategy commercePaymentCaptureStrategy) {
        this.commercePaymentCaptureStrategy = commercePaymentCaptureStrategy;
    }

    @Override
    public void afterPlaceOrder(final CommerceCheckoutParameter parameter, final CommerceOrderResult orderModel) throws InvalidCartException {
        // nothing to do
    }

    @Override
    public void beforePlaceOrder(final CommerceCheckoutParameter parameter) throws InvalidCartException {
        checkArgument(parameter != null, "Parameter parameter cannot be null");

        checkState(parameter.getCart() != null, "parameter.cart cannot be null");

        if (parameter.getCart().getPaymentType() != null && CARD.getCode().equals(parameter.getCart().getPaymentType().getCode())) {
            commercePaymentCaptureStrategy.capturePaymentAmount(parameter.getCart());
        }
    }

    @Override
    public void beforeSubmitOrder(final CommerceCheckoutParameter parameter, final CommerceOrderResult result) throws InvalidCartException {
        // nothing to do
    }
}