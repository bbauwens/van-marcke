package com.vanmarcke.services.search.solrfacetsearch.provider;

import de.hybris.platform.core.model.ItemModel;

import java.util.Locale;

/**
 * Custom URL resolver for ItemModel type of objects
 * @param <T> ItemModel sub class
 */
public interface VMKModelUrlResolver<T extends ItemModel> {

    /**
     * @param item ItemModel to resolve
     * @return true if the resolver can process the ItemModel, false otherwise
     */
    boolean canResolve(ItemModel item);

    /**
     * Resolve the URL depending on a given locale
     * OOTB code will resolve URLs only in the current context locale
     * This method will resolve the URL for any locale
     *
     * @param item   ItemModel
     * @param locale locale
     * @return localized URL
     */
    String resolveInternal(T item, Locale locale);
}
