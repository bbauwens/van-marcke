package com.vanmarcke.services.strategies;

import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;

/**
 * Strategy which provides functionality around payment type defaults and validation.
 */
public interface VMKCommercePaymentTypeStrategy {

    /**
     * Validates the current payment type on the cart.
     *
     * @param parameter the parameter holding the cart
     */
    void validatePaymentType(CommerceCheckoutParameter parameter);
}