package com.vanmarcke.services.factories.impl;

import com.vanmarcke.services.basestore.VMKBaseStoreService;
import com.vanmarcke.services.factories.VMKStockServiceFactory;
import com.vanmarcke.services.product.VMKStockService;
import de.hybris.platform.store.BaseStoreModel;

import java.util.Map;

import static com.vanmarcke.services.constants.VanmarckeservicesConstants.ERPSystems.IBM;
import static com.vanmarcke.services.constants.VanmarckeservicesConstants.ERPSystems.S4HANA;

/**
 * Factory implementation for providing the required {@link VMKStockService}.
 *
 * @author Tom van den Berg
 * @since 18-12-2020
 */
public class VMKStockServiceFactoryImpl implements VMKStockServiceFactory {

    private final VMKBaseStoreService baseStoreService;
    private final Map<String, VMKStockService> factoryConfigurationMap;
    private final VMKStockService defaultStockService;

    /**
     * Provides an instance of the VMKStockServiceFactoryImpl.
     *
     * @param baseStoreService        the base store service
     * @param defaultStockService     the default stock service
     * @param factoryConfigurationMap the factory configuration map
     */
    public VMKStockServiceFactoryImpl(VMKBaseStoreService baseStoreService,
                                      Map<String, VMKStockService> factoryConfigurationMap,
                                      VMKStockService defaultStockService) {
        this.baseStoreService = baseStoreService;
        this.factoryConfigurationMap = factoryConfigurationMap;
        this.defaultStockService = defaultStockService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public VMKStockService getStockService() {
        BaseStoreModel baseStore = baseStoreService.getCurrentBaseStore();
        String systemType = baseStoreService.isSAPBaseStore(baseStore) ? S4HANA : IBM;
        return factoryConfigurationMap.getOrDefault(systemType, defaultStockService);
    }
}
