package com.vanmarcke.services.product.price;

import com.vanmarcke.cpi.data.price.ProductPriceResponseData;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.List;

/**
 * This interface defines methods which retrieve price information.
 *
 * @author Tom van den Berg
 * @since 10-06-2020
 */
public interface VMKPriceLookupService {

    /**
     * Returns the pricing for the given {@code product} and {@code b2bUnit}.
     *
     * @param product the base product
     * @param b2bUnitUid the B2B unit uid
     * @return the pricing
     */
    List<ProductPriceResponseData> getPricing(ProductModel product, String b2bUnitUid);
}
