package com.vanmarcke.services.helper;

import com.vanmarcke.services.search.solrfacetsearch.provider.VMKModelUrlResolver;
import de.hybris.platform.acceleratorservices.urlresolver.SiteBaseUrlResolutionService;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.site.BaseSiteService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Helper class for building hreflang links
 *
 * @author Giani Ifrim
 * @since 12-07-2021
 */
public class HreflangHelper {

    private final CommonI18NService commonI18NService;
    private final SiteBaseUrlResolutionService siteBaseUrlResolutionService;
    private final BaseSiteService baseSiteService;

    private Map<String, String> hreflangLocales;
    private List<VMKModelUrlResolver<ItemModel>> resolvers;

    public HreflangHelper(CommonI18NService commonI18NService, SiteBaseUrlResolutionService siteBaseUrlResolutionService,
                          BaseSiteService baseSiteService) {
        this.commonI18NService = commonI18NService;
        this.siteBaseUrlResolutionService = siteBaseUrlResolutionService;
        this.baseSiteService = baseSiteService;
    }

    /**
     * @param item model
     * @return map containing the hreflang links
     */
    public Map<String, String> generateHreflangMap(final ItemModel item) {
        String root = getRootUrl();
        final Map<String, String> results = new HashMap<>();
        hreflangLocales.forEach((k, v) -> results.put(k, buildLocalizedLink(item, v, root)));

        return results;
    }

    /**
     * @param item   model
     * @param locale
     * @return localized link for the given item
     */
    private String buildLocalizedLink(ItemModel item, String locale, String rootUrl) {
        String link;

        VMKModelUrlResolver<ItemModel> resolver = resolvers.stream().filter(r -> r.canResolve(item)).findFirst().orElseThrow();
        link = resolver.resolveInternal(item, getCommonI18NService().getLocaleForIsoCode(locale));

        return rootUrl.concat(locale).concat(link);
    }

    /**
     * This method will remove the siteId from the website URL.
     * e.g https://blue.vanmarcke.com/nl_BE/ -> https://blue.vanmarcke.com/
     * @return website URL without siteId
     */
    private String getRootUrl() {
        String urlWithSiteId = getSiteBaseUrlResolutionService()
                .getWebsiteUrlForSite(getBaseSiteService().getCurrentBaseSite(), true, "/");

        //get the position of first "/" after "https://"
        int position = urlWithSiteId.indexOf("/", 8);

        return urlWithSiteId.substring(0, position + 1);
    }

    /**
     * @return the commonI18NService
     */
    public CommonI18NService getCommonI18NService() {
        return commonI18NService;
    }

    /**
     * @return the siteBaseUrlResolutionService
     */
    public SiteBaseUrlResolutionService getSiteBaseUrlResolutionService() {
        return siteBaseUrlResolutionService;
    }

    /**
     * @return the baseSiteService
     */
    public BaseSiteService getBaseSiteService() {
        return baseSiteService;
    }

    /**
     * Sets the hreflangLocales
     * @param hreflangLocales map of locales
     *                        key -> locale as per Google documentation e.g nl-BE
     *                        value -> hybris locale e.g nl_BE
     */
    public void setHreflangLocales(Map<String, String> hreflangLocales) {
        this.hreflangLocales = hreflangLocales;
    }

    /**
     * Sets the resolvers
     * @param resolvers List of VMKModelUrlResolvers
     */
    public void setResolvers(List<VMKModelUrlResolver<ItemModel>> resolvers) {
        this.resolvers = resolvers;
    }


}
