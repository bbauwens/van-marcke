package com.vanmarcke.services.odata.schema;

import de.hybris.platform.integrationservices.model.DescriptorFactory;
import de.hybris.platform.integrationservices.model.IntegrationObjectItemAttributeModel;
import de.hybris.platform.integrationservices.model.TypeAttributeDescriptor;
import de.hybris.platform.odata2services.odata.NestedAbstractItemTypeCannotBeCreatedException;
import de.hybris.platform.odata2services.odata.UniqueCollectionNotAllowedException;
import de.hybris.platform.odata2services.odata.schema.SchemaElementGenerator;
import de.hybris.platform.odata2services.odata.schema.association.AssociationGeneratorRegistry;
import de.hybris.platform.odata2services.odata.schema.utils.SchemaUtils;
import org.apache.olingo.odata2.api.edm.FullQualifiedName;
import org.apache.olingo.odata2.api.edm.provider.AnnotationAttribute;
import org.apache.olingo.odata2.api.edm.provider.NavigationProperty;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class VMKNavigationPropertyGenerator implements SchemaElementGenerator<Optional<NavigationProperty>, IntegrationObjectItemAttributeModel> {
    private final DescriptorFactory integrationServicesDescriptorFactory;
    private final AssociationGeneratorRegistry associationGeneratorRegistry;
    private final SchemaElementGenerator<List<AnnotationAttribute>, IntegrationObjectItemAttributeModel> attributeListGenerator;

    public VMKNavigationPropertyGenerator(DescriptorFactory integrationServicesDescriptorFactory, AssociationGeneratorRegistry associationGeneratorRegistry, SchemaElementGenerator<List<AnnotationAttribute>, IntegrationObjectItemAttributeModel> attributeListGenerator) {
        this.integrationServicesDescriptorFactory = integrationServicesDescriptorFactory;
        this.associationGeneratorRegistry = associationGeneratorRegistry;
        this.attributeListGenerator = attributeListGenerator;
    }

    @Override
    public Optional<NavigationProperty> generate(final IntegrationObjectItemAttributeModel itemAttribute) {
        Objects.requireNonNull(itemAttribute);

        return associationGeneratorRegistry.getAssociationGenerator(itemAttribute)
                .map(associationGenerator -> {
                    validateNavigationProperty(integrationServicesDescriptorFactory.createTypeAttributeDescriptor(itemAttribute));
                    return new NavigationProperty()
                            .setName(itemAttribute.getAttributeName())
                            .setRelationship(new FullQualifiedName(SchemaUtils.NAMESPACE, associationGenerator.getAssociationName(itemAttribute)))
                            .setFromRole(associationGenerator.getSourceRole(itemAttribute))
                            .setToRole(associationGenerator.getTargetRole(itemAttribute))
                            .setAnnotationAttributes(attributeListGenerator.generate(itemAttribute));
                });
    }

    private void validateNavigationProperty(final TypeAttributeDescriptor attributeDescriptor) {
        if (attributeDescriptor.isAutoCreate() && attributeDescriptor.getAttributeType().isAbstract()) {
            throw new NestedAbstractItemTypeCannotBeCreatedException(attributeDescriptor);
        }
        if (attributeDescriptor.isKeyAttribute() && isCollection(attributeDescriptor)) {
            throw new UniqueCollectionNotAllowedException(attributeDescriptor);
        }
    }

    private boolean isCollection(final TypeAttributeDescriptor attributeDescriptor) {
        return attributeDescriptor.isCollection() || attributeDescriptor.isMap();
    }
}
