package com.vanmarcke.services.cronjob;

import com.vanmarcke.core.model.ProductImageInheritanceCronJobModel;
import com.vanmarcke.services.product.VMKProductImageInheritanceService;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.time.TimeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import java.util.Date;

/**
 * The job manages the inheritance of images between base products and variant products.
 * The job finds variant products which have been changed since the last successful execution time.
 */
public class VMKProductImageInheritanceJobPerformable extends AbstractJobPerformable<ProductImageInheritanceCronJobModel> {

    private static final Logger LOG = LoggerFactory.getLogger(VMKProductImageInheritanceJobPerformable.class);

    private VMKProductImageInheritanceService productImageInheritanceService;
    private TimeService timeService;

    /**
     * {@inheritDoc}
     */
    @Override
    public PerformResult perform(final ProductImageInheritanceCronJobModel productImageInheritanceCronJob) {
        try {
            final Date startDate = timeService.getCurrentTime();
            productImageInheritanceService.updateProductImageInheritance(productImageInheritanceCronJob.getCatalogVersion(), productImageInheritanceCronJob.getLastSuccessfulTime());
            updateLastSuccessfulTime(productImageInheritanceCronJob, startDate);
            return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
        } catch (Exception e) {
            LOG.error("Exception occurred during the image inheritance management", e);
            return new PerformResult(CronJobResult.ERROR, CronJobStatus.ABORTED);
        }
    }

    private void updateLastSuccessfulTime(final ProductImageInheritanceCronJobModel productImageInheritanceCronJob, final Date date) {
        productImageInheritanceCronJob.setLastSuccessfulTime(date);
        modelService.save(productImageInheritanceCronJob);
    }

    /**
     * Sets the productImageInheritanceService
     *
     * @param productImageInheritanceService the productImageInheritanceService
     */
    @Required
    public void setProductImageInheritanceService(VMKProductImageInheritanceService productImageInheritanceService) {
        this.productImageInheritanceService = productImageInheritanceService;
    }

    /**
     * Sets the timeService
     *
     * @param timeService the timeService
     */
    @Required
    public void setTimeService(TimeService timeService) {
        this.timeService = timeService;
    }
}