package com.vanmarcke.services.delivery;

import de.hybris.platform.commercefacades.order.data.DeliveryModeData;
import de.hybris.platform.core.model.order.CartModel;

import java.util.Map;

/**
 * This interface defines methods related to {@link DeliveryModeData}
 *
 * @author Tom van den Berg
 * @since 30-12-2020
 */
public interface VMKDeliveryMethodService {

    /**
     * Determines the actual available delivery methods based on the delivery methods that the current cart supports.
     *
     * @param cart the cart
     * @return the available delivery methods
     */
    Map<String, Boolean> getSupportedDeliveryMethods(CartModel cart);

    /**
     * Get the delivery method codes for the current country and supported delivery methods.
     *
     * @param cart the cart
     * @return the delivery method codes
     */
    Map<String, String> getDeliveryMethodCodes(CartModel cart);

}
