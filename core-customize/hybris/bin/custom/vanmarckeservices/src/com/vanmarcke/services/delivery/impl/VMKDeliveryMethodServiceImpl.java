package com.vanmarcke.services.delivery.impl;

import com.vanmarcke.services.delivery.VMKDeliveryMethodService;
import com.vanmarcke.services.delivery.VMKPickupInfoService;
import com.vanmarcke.services.delivery.VMKShippingInfoService;
import de.hybris.platform.commercefacades.order.data.DeliveryModeData;
import de.hybris.platform.commerceservices.delivery.DeliveryService;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeModel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.vanmarcke.core.constants.VanmarckeCoreConstants.DeliveryMethods.PICKUP;
import static com.vanmarcke.core.constants.VanmarckeCoreConstants.DeliveryMethods.SHIPPING;
import static org.apache.commons.collections.CollectionUtils.isNotEmpty;

/**
 * Implements methods related to {@link DeliveryModeData}.
 *
 * @author Tom van den Berg
 * @since 30-12-2020
 */
class VMKDeliveryMethodServiceImpl implements VMKDeliveryMethodService {

    private final VMKShippingInfoService shippingInfoService;
    private final VMKPickupInfoService pickupInfoService;
    private final DeliveryService deliveryService;

    /**
     * Provides an instance of the VMKDeliveryMethodServiceImpl.
     *
     * @param shippingInfoService the shipping info service
     * @param pickupInfoService   the pickup info service
     * @param deliveryService     the delivery service
     */
    public VMKDeliveryMethodServiceImpl(VMKShippingInfoService shippingInfoService,
                                        VMKPickupInfoService pickupInfoService,
                                        DeliveryService deliveryService) {
        this.shippingInfoService = shippingInfoService;
        this.pickupInfoService = pickupInfoService;
        this.deliveryService = deliveryService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public HashMap<String, Boolean> getSupportedDeliveryMethods(CartModel cart) {
        Map<Boolean, List<DeliveryModeModel>> filteredMethods = getSupportDeliveryMethodsForCart(cart);

        HashMap<String, Boolean> deliveryModes = new HashMap<>();
        deliveryModes.put(PICKUP, isNotEmpty(filteredMethods.get(Boolean.FALSE)) && pickupInfoService.isPickupPossible(cart));
        deliveryModes.put(SHIPPING, isNotEmpty(filteredMethods.get(Boolean.TRUE)) && shippingInfoService.isShippingPossible(cart));

        return deliveryModes;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<String, String> getDeliveryMethodCodes(CartModel cart) {
        Map<Boolean, List<DeliveryModeModel>> supportedDeliveryMethods = getSupportDeliveryMethodsForCart(cart);

        HashMap<String, String> mapping = new HashMap<>();
        mapping.put(PICKUP, supportedDeliveryMethods.get(Boolean.FALSE) != null ? supportedDeliveryMethods.get(Boolean.FALSE).get(0).getCode() : null);
        mapping.put(SHIPPING, supportedDeliveryMethods.get(Boolean.TRUE) != null ? supportedDeliveryMethods.get(Boolean.TRUE).get(0).getCode() : null);

        return mapping;
    }

    /**
     * Returns a map where the keys are booleans. The {code TRUE} value represents the SHIPPING delivery method, the {@code FALSE} value is PICKUP.
     *
     * @param cart the cart
     * @return the supported delivery methods
     */
    protected Map<Boolean, List<DeliveryModeModel>> getSupportDeliveryMethodsForCart(CartModel cart) {
        return deliveryService.getSupportedDeliveryModeListForOrder(cart)
                .stream()
                .collect(Collectors.groupingBy(ZoneDeliveryModeModel.class::isInstance));
    }
}
