package com.vanmarcke.services.strategies.impl;

import com.vanmarcke.core.enums.IBMOrderStatus;
import com.vanmarcke.core.model.IBMOrderModel;
import com.vanmarcke.cpi.data.order.OrderRequestData;
import com.vanmarcke.cpi.data.order.PlaceOrderInfoResponseData;
import com.vanmarcke.cpi.data.order.PlaceOrderResponseData;
import com.vanmarcke.cpi.services.ESBPlaceOrderService;
import com.vanmarcke.services.strategies.VMKCommerceSendOrderStrategy;
import de.hybris.platform.commerceservices.order.impl.DefaultCommercePlaceOrderStrategy;
import de.hybris.platform.core.enums.ExportStatus;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import static org.apache.commons.collections4.ListUtils.emptyIfNull;

/**
 * Custom Implementation of the {@link DefaultCommercePlaceOrderStrategy}
 * where the billing address on the cart's payment information is not cloned and set an the newly created order.
 *
 * @author Niels Raemaekers
 * @since 20-2-2020
 */
public class VMKCommerceSendOrderStrategyImpl implements VMKCommerceSendOrderStrategy {

    private final ESBPlaceOrderService esbPlaceOrderService;
    private final Converter<AbstractOrderModel, OrderRequestData> vmkPlaceOrderRequestConverter;
    private final Converter<IBMOrderModel, OrderRequestData> vmkPlaceSuborderRequestPopulator;
    private final ModelService modelService;

    /**
     * Constructor for {@link VMKCommerceSendOrderStrategyImpl}
     *
     * @param esbPlaceOrderService             the esbPlaceOrderService
     * @param vmkPlaceOrderRequestConverter    the vmkPlaceOrderRequestConverter
     * @param vmkPlaceSuborderRequestPopulator the suborder request converter
     * @param modelService                     the modelService
     */
    public VMKCommerceSendOrderStrategyImpl(final ESBPlaceOrderService esbPlaceOrderService,
                                            final Converter<AbstractOrderModel, OrderRequestData> vmkPlaceOrderRequestConverter,
                                            final Converter<IBMOrderModel, OrderRequestData> vmkPlaceSuborderRequestPopulator,
                                            final ModelService modelService) {
        this.esbPlaceOrderService = esbPlaceOrderService;
        this.vmkPlaceOrderRequestConverter = vmkPlaceOrderRequestConverter;
        this.vmkPlaceSuborderRequestPopulator = vmkPlaceSuborderRequestPopulator;
        this.modelService = modelService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OrderModel sendOrder(final OrderModel order) {
        // Check if the order is already exported.
        if (ExportStatus.EXPORTED.equals(order.getExportStatus())) {
            return order;
        }

        // Check of the order was already exported and failed.
        if (ExportStatus.PARTIALLY_EXPORTED.equals(order.getExportStatus())) {
            exportFailedSuborders(order);

            // Reassess the order status
            order.setExportStatus(getExportStatus(order));
            order.setStatus(ExportStatus.EXPORTED.equals(order.getExportStatus())
                    ? OrderStatus.COMPLETED : OrderStatus.PROCESSING_ERROR);

            modelService.save(order);
            return order;
        }

        final PlaceOrderResponseData response = esbPlaceOrderService.getPlaceOrderInformation(vmkPlaceOrderRequestConverter.convert(order));
        if (isValidResponse(response)) {
            splitOrder(order, response);

            final ExportStatus orderExportStatus = getExportStatus(order);
            order.setExternalCode(getExternalOrderCode(order));
            order.setExportStatus(orderExportStatus);
            order.setStatus(ExportStatus.EXPORTED.equals(orderExportStatus)
                    ? OrderStatus.COMPLETED : OrderStatus.PROCESSING_ERROR);
        } else {
            order.setExportStatus(ExportStatus.NOTEXPORTED);
            order.setStatus(OrderStatus.PROCESSING_ERROR);
        }

        modelService.save(order);
        modelService.refresh(order);
        return order;
    }

    /**
     * Exports the failed suborders of the given {@code order}.
     *
     * @param order the order
     */
    private void exportFailedSuborders(final OrderModel order) {
        order.getIbmOrders()
                .stream()
                .filter(suborder -> IBMOrderStatus.NOT_SENT_TO_ERP.equals(suborder.getOrderStatus()))
                .forEach(this::exportFailedSuborder);
    }

    /**
     * Reattempts to replicate the given {@code suborder}.
     *
     * @param suborder the suborder
     */
    private void exportFailedSuborder(final IBMOrderModel suborder) {
        final PlaceOrderResponseData response = esbPlaceOrderService.getPlaceOrderInformation(vmkPlaceSuborderRequestPopulator.convert(suborder));
        suborder.setOrderStatus(isValidResponse(response) && isReplicationSuccessful(response, suborder)
                ? IBMOrderStatus.SENT_TO_ERP : IBMOrderStatus.NOT_SENT_TO_ERP);
        modelService.save(suborder);
    }

    /**
     * Returns the external order code.
     *
     * @param order the order
     * @return the external order code
     */
    private String getExternalOrderCode(final OrderModel order) {
        return order.getIbmOrders()
                .stream()
                .findFirst()
                .map(IBMOrderModel::getCode)
                .orElse(null);
    }

    /**
     * Returns the export status for the given {@code order}.
     *
     * @param order the order
     * @return the export status
     */
    private ExportStatus getExportStatus(final OrderModel order) {
        return order.getIbmOrders()
                .stream()
                .anyMatch(suborder -> IBMOrderStatus.NOT_SENT_TO_ERP.equals(suborder.getOrderStatus()))
                ? ExportStatus.PARTIALLY_EXPORTED
                : ExportStatus.EXPORTED;
    }

    /**
     * Checks whether the given {@code response} is valid.
     * <p>
     * The response is invalid in case there is no data present.
     *
     * @param response the response to validate
     * @return {@code true} in case the response is valid, {@code false} otherwise
     */
    private boolean isValidResponse(final PlaceOrderResponseData response) {
        return response != null && response.getData() != null && CollectionUtils.isNotEmpty(response.getData().getOrders());
    }

    /**
     * Checks whether the given {@code suborder} is replicated successfully.
     *
     * @param response the response
     * @param suborder the suborder
     * @return {@code true} in case the given {@code suborder} is replicated successfully, {@code false} otherwise
     */
    private boolean isReplicationSuccessful(final PlaceOrderResponseData response, final IBMOrderModel suborder) {
        return response.getData()
                .getOrders()
                .stream()
                .filter(order -> suborder.getCode().equals(order.getId()))
                .findFirst()
                .filter(order -> StringUtils.isEmpty(order.getErrorCode()))
                .isPresent();
    }

    /**
     * Splits the given {@code order} into suborders.
     *
     * @param order    the order
     * @param response the order creation response
     */
    private void splitOrder(final OrderModel order, final PlaceOrderResponseData response) {
        order.setIbmOrders(response.getData().getOrders()
                .stream().map(suborderResponse -> createSuborder(suborderResponse, order))
                .collect(Collectors.toSet()));
    }

    /**
     * Creates a suborder for the given {@code response}.
     *
     * @param response    the order creation response
     * @param parentOrder the parent order
     * @return the suborder
     */
    private IBMOrderModel createSuborder(final PlaceOrderInfoResponseData response, final OrderModel parentOrder) {
        final IBMOrderModel suborder = modelService.create(IBMOrderModel.class);
        suborder.setCode(response.getId());
        suborder.setOrderStatus(getOrderStatus(response));
        suborder.setEntries(getEntries(response, parentOrder));
        suborder.setOrder(parentOrder);
        modelService.save(suborder);
        return suborder;
    }

    /**
     * Returns the appropriate suborder status code.
     *
     * @param response the order creation response
     * @return the suborder status
     */
    private IBMOrderStatus getOrderStatus(final PlaceOrderInfoResponseData response) {
        return response.isInError() ? IBMOrderStatus.NOT_SENT_TO_ERP : IBMOrderStatus.SENT_TO_ERP;
    }

    /**
     * Collects and returns the entries for the suborder.
     *
     * @param response    the order creation response
     * @param parentOrder the parent order which contains all entries
     * @return the entries for the suborder
     */
    private Set<AbstractOrderEntryModel> getEntries(final PlaceOrderInfoResponseData response, final OrderModel parentOrder) {
        final Set<AbstractOrderEntryModel> suborderEntries = new HashSet<>();
        emptyIfNull(response.getProducts())
                .forEach(responseEntry -> parentOrder.getEntries()
                        .stream()
                        .filter(entry -> responseEntry.getProductID().equals(entry.getProduct().getCode()))
                        .findFirst()
                        .ifPresent(suborderEntries::add));
        return suborderEntries;
    }
}