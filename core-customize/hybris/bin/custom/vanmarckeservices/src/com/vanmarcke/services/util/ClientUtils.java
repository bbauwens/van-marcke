package com.vanmarcke.services.util;

import javax.servlet.http.HttpServletRequest;

/**
 * Defines utility methods to retrieve information about the client.
 *
 * @author Tom van den Berg
 * @since 15-06-2020
 */
public class ClientUtils {

    /**
     * Returns the ip address from the given {@link HttpServletRequest}.
     *
     * @param request the http request
     * @return the ip address
     */
    public static String getClientIp(HttpServletRequest request) {
        String remoteAddr = "";
        if (request != null) {
            remoteAddr = request.getHeader("X-FORWARDED-FOR");
            if (remoteAddr == null || "".equals(remoteAddr)) {
                remoteAddr = request.getRemoteAddr();
            }
        }
        return remoteAddr;
    }
}
