package com.vanmarcke.services.strategies.impl;

import de.hybris.platform.commerceservices.strategies.impl.DefaultDeliveryModeLookupStrategy;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.user.AddressModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Custom implementation for DefaultBlueDeliveryModeLookupStrategy
 *
 * @author Niels Raemaekers, Taki Krovessis, Tom van den Berg
 * @since 07-07-2020
 */
public class VMKDeliveryModeLookupStrategyImpl extends DefaultDeliveryModeLookupStrategy {

    private static final String DIRECT_SUPPLY_CODE = "S";
    private static final String CROSS_DOC_CODE = "X";

    /**
     * {@inheritDoc}
     */
    @Override
    public List<DeliveryModeModel> getSelectableDeliveryModesForOrder(AbstractOrderModel abstractOrderModel) {
        List<DeliveryModeModel> deliveryModes = new ArrayList<>(getPickupDeliveryModeDao().findPickupDeliveryModesForAbstractOrder(abstractOrderModel));
        if (isDeliveryAvailableForOrder(abstractOrderModel)) {
            AddressModel deliveryAddress = abstractOrderModel.getDeliveryAddress();
            CurrencyModel currency = abstractOrderModel.getCurrency();
            if (currency != null && deliveryAddress != null && deliveryAddress.getCountry() != null) {
                deliveryModes.addAll(getCountryZoneDeliveryModeDao().findDeliveryModes(abstractOrderModel));
            }
        }
        return deliveryModes;
    }

    /**
     * Checks whether delivery is available for the given {@code order}.
     *
     * @param order the order
     * @return {@code true} in case delivery is available for the given {@code order}, {@code false} otherwise
     */
    private boolean isDeliveryAvailableForOrder(AbstractOrderModel order) {
        return order.getEntries()
                .stream()
                .noneMatch(entry -> DIRECT_SUPPLY_CODE.equalsIgnoreCase(entry.getProduct().getDeliveryMethod()) || CROSS_DOC_CODE.equalsIgnoreCase(entry.getProduct().getDeliveryMethod()));
    }
}
