package com.vanmarcke.services.product;

public interface VMKStructuredDataService<T> {

    /**
     *
     * @param o source item type
     * @return JSON as string
     */
    String createStructuredDataJson(T o);
}
