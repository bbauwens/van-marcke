package com.vanmarcke.services.config.impl;

import com.vanmarcke.services.config.VMKConfigurationService;
import de.hybris.platform.servicelayer.config.impl.DefaultConfigurationService;
import org.apache.commons.configuration.Configuration;

/**
 * The {@link VMKConfigurationServiceImpl} class implements business logic to read configuration properties.
 *
 * @author Christiaan Janssen
 * @since 14-07-2020
 */
public class VMKConfigurationServiceImpl extends DefaultConfigurationService implements VMKConfigurationService {

    /**
     * {@inheritDoc}
     */
    @Override
    public String getString(String key, String defaultValue) {
        return getConfiguration().getString(key, defaultValue);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Boolean getBoolean(String key, Boolean defaultValue) {
        return getConfiguration().getBoolean(key, defaultValue);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Configuration getConfiguration() {
        // This method has been overridden for testing purposes
        return super.getConfiguration();
    }
}
