package com.vanmarcke.services.delivery;

import com.vanmarcke.cpi.data.order.PickupDateInfoResponseData;
import com.vanmarcke.cpi.data.order.ShippingDateInfoResponseData;
import de.hybris.platform.core.model.order.CartModel;

import java.util.Optional;

/**
 * Defines methods for retrieving shipping and pickup information.
 *
 * @author Tom van den Berg
 * @since 14-01-2021
 */
public interface VMKDeliveryInfoService {

    /**
     * Retrieves the pickup info for the given cart.
     *
     * @param cart the cart
     * @return the pickup date info response data
     */
    Optional<PickupDateInfoResponseData> getPickupDateInformation(CartModel cart);

    /**
     * Retrieves the shipping info for the given cart.
     *
     * @param cart the cart
     * @return the shipping date info response data
     */
    Optional<ShippingDateInfoResponseData> getShippingDateInformation(CartModel cart);
}
