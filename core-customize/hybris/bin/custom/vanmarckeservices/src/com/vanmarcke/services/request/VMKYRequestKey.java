package com.vanmarcke.services.request;

import de.hybris.platform.core.model.order.CartModel;

import java.util.Objects;

public final class VMKYRequestKey {
    private final Object key;

    private VMKYRequestKey(Object key) {
        this.key = key;
    }

    /**
     * Generate yRequest pickup date information key for cart
     *
     * @param cart the cart
     * @return the yReqeust key
     */
    public static VMKYRequestKey pickupDateInformationKey(final CartModel cart) {
        Objects.requireNonNull(cart, "cart parameter must not be null");
        return new VMKYRequestKey("pickupDateInformationKey_" + cart.getCode());
    }

    /**
     * Generate yRequest shipping date information key for cart
     *
     * @param cart the cart
     * @return the yReqeust key
     */
    public static VMKYRequestKey shippingDateInformationKey(final CartModel cart) {
        Objects.requireNonNull(cart, "cart parameter must not be null");
        return new VMKYRequestKey("shippingDateInformationKey_" + cart.getCode());
    }

    public final Object getKey() {
        return key;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final VMKYRequestKey that = (VMKYRequestKey) o;
        return Objects.equals(key, that.key);
    }

    @Override
    public int hashCode() {
        return Objects.hash(key);
    }
}
