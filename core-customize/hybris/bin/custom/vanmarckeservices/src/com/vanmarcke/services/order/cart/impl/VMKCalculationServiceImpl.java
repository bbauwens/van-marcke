package com.vanmarcke.services.order.cart.impl;

import com.vanmarcke.core.model.CommerceGroupModel;
import com.vanmarcke.services.b2b.VMKB2BUnitService;
import com.vanmarcke.services.product.price.VMKPriceService;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.product.data.PriceCalculationData;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.order.impl.DefaultCalculationService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.util.PriceValue;
import de.hybris.platform.util.TaxValue;
import de.hybris.platform.variants.model.VariantProductModel;
import org.apache.commons.lang3.BooleanUtils;

import java.util.Collection;
import java.util.List;

/**
 * The {@code DefaultBlueCalculationService} class is used to overwrite the {@link DefaultCalculationService} class.
 * <p>
 * This is necessary, because price are personalized and stored in the ERP.
 *
 * @author Niels Raemaekers, Taki Korovessis, Christiaan Janssen
 * @since 26-04-2020
 */
public class VMKCalculationServiceImpl extends DefaultCalculationService {

    private final VMKPriceService priceService;
    private final UserService userService;
    private final VMKB2BUnitService b2bUnitService;
    private final CMSSiteService cmsSiteService;

    /**
     * Creates a new instance of the {@link VMKCalculationServiceImpl} class.
     * @param priceService the price service
     * @param userService
     * @param b2bUnitService
     * @param cmsSiteService
     */
    public VMKCalculationServiceImpl(VMKPriceService priceService, UserService userService, VMKB2BUnitService b2bUnitService, CMSSiteService cmsSiteService) {
        this.priceService = priceService;
        this.userService = userService;
        this.b2bUnitService = b2bUnitService;
        this.cmsSiteService = cmsSiteService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected PriceValue findBasePrice(AbstractOrderEntryModel entry) throws CalculationException {
        final UserModel currentUser = entry.getOrder().getUser();
        AbstractOrderModel order = entry.getOrder();
        if (!this.userService.isAnonymousUser(currentUser)
                && (currentUser.getShowNetPrice() || BooleanUtils.isTrue(order.getCheckoutMode()) || cmsSiteService.getCurrentSite().getChannel().equals(SiteChannel.DIY))) {
            Double value = getNetPrice((VariantProductModel) entry.getProduct(), order.getUnit());
            if (value != null) {
                return new PriceValue(order.getCurrency().getIsocode(), value, BooleanUtils.isTrue(order.getNet()));
            }
            return null;
        }
        return super.findBasePrice(entry);
    }

    /**
     * Returns the net price for the given {@code entry}.
     *
     * @param variant the product variant
     * @param b2bUnit the B2B unit
     * @return the net price
     */
    protected Double getNetPrice(VariantProductModel variant, B2BUnitModel b2bUnit) {
        PriceCalculationData price = priceService.getPrice(variant, b2bUnit.getUid());
        if (price != null) {
            return price.getPriceValue().doubleValue();
        }
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Collection<TaxValue> findTaxValues(final AbstractOrderEntryModel entry) {
        if (isZeroVAT(entry)) {
            return List.of(new TaxValue("VAT", 0, false, entry.getOrder().getCurrency().getIsocode()));
        }

        PriceCalculationData entryPriceData = priceService.getPrice((VariantProductModel) entry.getProduct(), b2bUnitService.getCustomerB2BUnitID(entry.getOrder().getUser()));
        return List.of(new TaxValue("VAT", entryPriceData.getVat(), false, entry.getOrder().getCurrency().getIsocode()));
    }

    /**
     * calculates whether we should apply VAT or not (VAT will only apply to purchases inside the current country or for extra-communitary purchases)
     * @param entry order entry
     * @return returns VAT value
     */
    protected boolean isZeroVAT(AbstractOrderEntryModel entry) {
        if (userService.isAnonymousUser(entry.getOrder().getUser())) {
            return false;
        }

        AddressModel addressModel = entry.getOrder().getDeliveryAddress() != null ?
                entry.getOrder().getDeliveryAddress() : b2bUnitService.getB2BUnitModelForCurrentUser().getShippingAddress();

        if (addressModel != null) {
            final CommerceGroupModel commerceGroupModel = cmsSiteService.getCurrentSite().getCommerceGroup();
            if (commerceGroupModel != null) {
                CountryModel siteCountry = commerceGroupModel.getCountry();
                return !siteCountry.getIsocode().equals(addressModel.getCountry().getIsocode());
            }
        }

        return false;
    }
}