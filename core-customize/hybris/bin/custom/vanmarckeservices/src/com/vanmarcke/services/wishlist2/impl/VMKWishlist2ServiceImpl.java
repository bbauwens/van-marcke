package com.vanmarcke.services.wishlist2.impl;

import com.vanmarcke.core.model.VanMarckeVariantProductModel;
import com.vanmarcke.core.wishlist2.VMKWishlist2Dao;
import com.vanmarcke.services.product.VMKVariantProductService;
import com.vanmarcke.services.wishlist2.VMKWishlist2Service;
import de.hybris.platform.core.Constants;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.core.servicelayer.data.PaginationData;
import de.hybris.platform.core.servicelayer.data.SearchPageData;
import de.hybris.platform.core.servicelayer.data.SortData;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.variants.model.VariantProductModel;
import de.hybris.platform.wishlist2.enums.Wishlist2EntryPriority;
import de.hybris.platform.wishlist2.impl.DefaultWishlist2Service;
import de.hybris.platform.wishlist2.model.Wishlist2EntryModel;
import de.hybris.platform.wishlist2.model.Wishlist2Model;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.lang.NonNull;

import java.text.MessageFormat;
import java.util.*;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

public class VMKWishlist2ServiceImpl extends DefaultWishlist2Service implements VMKWishlist2Service {


    private final ProductService productService;
    private final VMKVariantProductService variantProductService;
    /**
     * Creates a new instance of the {@link VMKWishlist2ServiceImpl} class.
     */
    public VMKWishlist2ServiceImpl(ProductService productService, VMKVariantProductService variantProductService) {
        this.productService = productService;
        this.variantProductService = variantProductService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SearchPageData<Wishlist2Model> getWishlists(@NonNull final PaginationData paginationData, @NonNull final SortData sortData) {
        return getWishlistDao().getWishlistsForUser(getCurrentUser(), paginationData, sortData);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Wishlist2Model getWishlistForCurrentUserAndName(String name) {
        return getWishlistDao().getWishlistByUserAndName(getCurrentUser(), name);
    }

    @Override
    public Wishlist2Model findWishListByUserAndName(UserModel user, String name) {
        return getWishlistDao().findWishListByUserAndName(user, name);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Wishlist2Model createWishlistForCurrentUserAndNameAndDescription(final String name, final String description) {
        return createWishlist(name, description);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeWishlistForCurrentUserAndName(String name) {
        getModelService().remove(getWishlistForCurrentUserAndName(name));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Wishlist2EntryModel getWishlistEntryForCurrentUserAndPK(String pk) {
        List<Wishlist2EntryModel> entries = getWishlistDao().findWishListEntriesByUserAndPK(getCurrentUser(), pk);
        ServicesUtil.validateIfSingleResult(entries,
                MessageFormat.format("No entry with given pk ''{0}'' was found.", pk),
                MessageFormat.format("More than one entry with given pk ''{0}'' was found.", pk));
        return entries.get(0);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeWishlistEntryForPK(String pk) {
        Wishlist2EntryModel entry = getWishlistEntryForCurrentUserAndPK(pk);
        getModelService().remove(entry);
    }

    @Override
    public Wishlist2Model convertCartToWishlist(String wishListName, String wishListDescription, AbstractOrderModel abstractOrderModel, UserModel userModel) {
        // TODO: does the wishlist exists?
        final Wishlist2Model target = getModelService().create(Wishlist2Model.class);
        addRequiredWishListFields(target, wishListName, userModel);
        if (!StringUtils.isEmpty(wishListDescription)) {
            target.setDescription(wishListDescription);
        }
        // convert all entries
        CollectionUtils.emptyIfNull(abstractOrderModel.getEntries()).forEach(entryModel -> {
            // create the new entry
            final Wishlist2EntryModel wishlist2EntryModel = getModelService().create(Wishlist2EntryModel.class);
            final ProductModel productModel = entryModel.getProduct();
            final int quantity = entryModel.getQuantity() != null ? entryModel.getQuantity().intValue() : 1;

            // fill all required fields
            addRequiredWishListEntryFields(wishlist2EntryModel, productModel, quantity);

            // and persist
            target.getEntries().add(wishlist2EntryModel);
        });

        // done
        getModelService().save(target);

        return target;
    }

    @Override
    public void addRequiredWishListFields(final Wishlist2Model wishlist2Model, final String wishListName, final UserModel userModel) {
        wishlist2Model.setName(wishListName);
        wishlist2Model.setUser(userModel);
        wishlist2Model.setEntries(new ArrayList<>());
    }

    @Override
    public void addRequiredWishListEntryFields(final Wishlist2EntryModel wishlist2EntryModel, final ProductModel productModel, final int quantity) {
        wishlist2EntryModel.setProduct(productModel);
        wishlist2EntryModel.setDesired(quantity);
        wishlist2EntryModel.setAddedDate(new Date());
        wishlist2EntryModel.setPriority(Wishlist2EntryPriority.HIGH);
    }

    @Override
    public void uploadProductToWishlist(String productCode, Long qty, final Wishlist2Model wishlist2Model) {
        try {
            ProductModel product = productService.getProductForCode(productCode);
            if (!(product instanceof VanMarckeVariantProductModel) || !variantProductService.isPurchasable((VariantProductModel) product)) {
                throw new UpdateWishlistException("Product is not purchasable");
            }
            Wishlist2EntryModel wishlist2EntryModel = getModelService().create(Wishlist2EntryModel._TYPECODE);
            addRequiredWishListEntryFields(wishlist2EntryModel, product, qty.intValue());
            wishlist2Model.getEntries().add(wishlist2EntryModel);
        } catch (final UnknownIdentifierException e) {
            throw new UpdateWishlistException("No product found");
        }
    }


    @Override
    public Integer getWishListsCountForUser(UserModel user) {
        validateParameterNotNull(user, "user cannot be null");

        final boolean anonymous = Constants.USER.ANONYMOUS_CUSTOMER.equals(user.getUid());

        return (anonymous) ? Integer.valueOf(0) : getWishlistDao().getSavedCartsCountForSiteAndUser(user);
    }

    @Override
    public void addWishlistEntry(Wishlist2Model wishlist, ProductModel product, Integer desired, Wishlist2EntryPriority priority, String comment) {
        // if there's already an entry with the same product, just add
        final Optional<Wishlist2EntryModel> entryContinaingProduct = Optional.ofNullable(wishlist.getEntries())
                .orElseGet(Collections::emptyList)
                .stream()
                .filter(entry -> product.equals(entry.getProduct()))
                .findFirst();

        if (entryContinaingProduct.isPresent()) {
            // add to this line
            final Wishlist2EntryModel entryModel = entryContinaingProduct.get();
            entryModel.setDesired(ObjectUtils.firstNonNull(entryModel.getDesired(), 0) + ObjectUtils.firstNonNull(desired, 0));
            getModelService().save(entryModel);
        } else {
            // just add a new line
            super.addWishlistEntry(wishlist, product, desired, priority, comment);
        }
    }

    public void setWishlistDao(VMKWishlist2Dao wishlistDao) {
        super.setWishlistDao(wishlistDao);
    }

    public VMKWishlist2Dao getWishlistDao() {
        return (VMKWishlist2Dao) super.wishlistDao;
    }

}