package com.vanmarcke.services.order;

import com.vanmarcke.services.forms.delivery.DeliveryMethodForm;
import de.hybris.platform.commerceservices.order.CommerceCheckoutService;

import java.util.List;

/**
 * Custom interface for the CommerceCheckoutService
 */
public interface VMKCommerceCheckoutService extends CommerceCheckoutService {

    /**
     * Sets the delivery details on the cart
     *
     * @param methodForm the delivery method form
     */
    void setDeliveryDetailsOnCart(DeliveryMethodForm methodForm);

    /**
     * If the TEC store uid is present, add to session cart.
     * <p>
     * If the TEC store uid is empty, add the current session store to session cart.
     *
     * @param tecUid the TEC store uid
     */
    void setFavoriteAlternativeStoreOnSessionCart(String tecUid);

    /**
     * Returns a list of cart entry codes which cannot be ordered.
     * <p>
     * The entries which cannot be ordered have the stock indication "L" and have no TEC collect date available.
     *
     * @return the list of product codes
     */
    List<String> getInvalidCartEntries();
}
