package com.vanmarcke.services.job.maintenance.impl;

import com.vanmarcke.services.job.maintenance.AbstractVMKCleanupStrategy;
import de.hybris.platform.processengine.enums.ProcessState;
import de.hybris.platform.processengine.model.BusinessProcessModel;
import de.hybris.platform.servicelayer.internal.model.MaintenanceCleanupJobModel;

import java.util.Map;

import static java.util.Collections.singleton;

/**
 * Cleans up all succeeded business processes.
 */
public class VMKCleanupBusinessProcessStrategyImpl extends AbstractVMKCleanupStrategy<BusinessProcessModel> {

    private static final String QUERY = "SELECT {" + BusinessProcessModel.PK + "}" +
            " FROM {" + BusinessProcessModel._TYPECODE + "}" +
            " WHERE {" + BusinessProcessModel.STATE + "} IN (?state)" +
            " AND {" + BusinessProcessModel.MODIFIEDTIME + "} < ?date";

    @Override
    protected final String getQuery() {
        return QUERY;
    }

    @Override
    protected Map<String, Object> getParameters(final MaintenanceCleanupJobModel job) {
        final Map<String, Object> params = super.getParameters(job);

        params.put("state", singleton(ProcessState.SUCCEEDED));

        return params;
    }
}