package com.vanmarcke.services.job.maintenance.impl;

import com.vanmarcke.services.job.maintenance.AbstractVMKCleanupStrategy;
import de.hybris.platform.impex.model.ImpExMediaModel;

/**
 * Cleans up all ImpexMedia.
 * <p>
 * Every ImpEx import or export generates at least one ImpexMedia.
 * These media stay in the system, the platform does not delete them when it deletes the ImpEx jobs they belonged to.
 * <p>
 * The retention time should be the same as for the cronjob cleanup.
 */
public class VMKCleanupImpexMediaStrategy extends AbstractVMKCleanupStrategy<ImpExMediaModel> {

    private static final String QUERY = "SELECT {" + ImpExMediaModel.PK + "}" +
            " FROM {" + ImpExMediaModel._TYPECODE + "}" +
            " WHERE {" + ImpExMediaModel.MODIFIEDTIME + "} < ?date";

    @Override
    protected final String getQuery() {
        return QUERY;
    }
}