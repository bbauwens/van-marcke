package com.vanmarcke.services.product.impl;

import com.vanmarcke.core.product.VMKPagedVariantProductDao;
import com.vanmarcke.services.data.ProductExportItem;
import com.vanmarcke.services.model.builder.VariantProductModelMockBuilder;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.servicelayer.data.PaginationData;
import de.hybris.platform.core.servicelayer.data.SearchPageData;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.variants.model.VariantProductModel;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.fest.assertions.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;

import static de.hybris.platform.catalog.enums.ArticleApprovalStatus.APPROVED;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * The {@link VMKProductExportServiceTest} class contains the unit tests for the {@link VMKProductExportServiceImpl}
 * class.
 *
 * @author Taki Korovessis, Christiaan Janssen
 * @since 27/08/2018
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKProductExportServiceTest {

    private static final String PATH = "test-product-feed-hybris.xml";
    private static final String ARTICLE_NUMBER = RandomStringUtils.randomAlphabetic(10);
    private static final String XML = "<?xml version='1.0' encoding='UTF-8'?><items><item><articleNumber>" + ARTICLE_NUMBER + "</articleNumber><catalog_DIY>false</catalog_DIY><catalog_VanMarcke_B2B>false</catalog_VanMarcke_B2B><catalog_VanMarcke_B2C>false</catalog_VanMarcke_B2C><certification/><compliancies/><description/><dop/><ecoDataSheet/><generalManual/><identifier/><instructionManual/><keywords/><maintenanceManual/><name/><normalisation/><productSpecificationSheet/><safetyDataSheet/><sparePartsList/><summary/><technicalDataSheet/><userManual/><usp/><warranty/></item></items>";

    @Mock
    private VMKPagedVariantProductDao pagedVariantProductDao;

    @Mock
    private Converter<VariantProductModel, ProductExportItem> productExportConverter;

    @InjectMocks
    private VMKProductExportServiceImpl defaultProductExportService;

    @Test
    public void testGenerateProductFeed() throws JAXBException, IOException, XMLStreamException {
        CatalogVersionModel catalogVersion = mock(CatalogVersionModel.class);

        VariantProductModel product = VariantProductModelMockBuilder
                .aVariantProduct(null, null)
                .build();

        ProductExportItem item = new ProductExportItem();
        item.setArticleNumber(ARTICLE_NUMBER);

        when(productExportConverter.convert(product)).thenReturn(item);

        PaginationData paginationData = new PaginationData();
        paginationData.setNumberOfPages(1);

        SearchPageData<VariantProductModel> searchResult = new SearchPageData<>();
        searchResult.setResults(Collections.singletonList(product));
        searchResult.setPagination(paginationData);

        when(pagedVariantProductDao.findProductsByCatalogVersionAndApprovalStatus(eq(catalogVersion), eq(APPROVED), any(SearchPageData.class))).thenReturn(searchResult);

        File file = defaultProductExportService.generateProductFeed(catalogVersion, PATH);

        Assertions
                .assertThat(file)
                .isNotNull();

        Assertions
                .assertThat(XML)
                .isEqualTo(new String(Files.readAllBytes(Paths.get(PATH))));

        verify(pagedVariantProductDao).findProductsByCatalogVersionAndApprovalStatus(eq(catalogVersion), eq(APPROVED), any(SearchPageData.class));
        verify(productExportConverter).convert(product);

        FileUtils.deleteQuietly(file);
    }
}