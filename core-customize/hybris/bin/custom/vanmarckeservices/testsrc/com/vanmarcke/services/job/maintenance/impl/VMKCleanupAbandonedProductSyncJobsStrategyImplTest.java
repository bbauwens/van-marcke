package com.vanmarcke.services.job.maintenance.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKCleanupAbandonedProductSyncJobsStrategyImplTest {

    private VMKCleanupAbandonedProductSyncJobsStrategyImpl cleanupStrategy = new VMKCleanupAbandonedProductSyncJobsStrategyImpl();

    @Test
    public void testGetQuery() {
        String result = cleanupStrategy.getQuery();
        assertThat(result).isEqualTo("SELECT {cv.pk} FROM {CatalogVersionSyncCronJob AS cv JOIN CronjobStatus AS c ON {c.pk} = {cv.status}} WHERE {c.code} = 'UNKNOWN' AND {cv.creationtime} < ?date");
    }
}