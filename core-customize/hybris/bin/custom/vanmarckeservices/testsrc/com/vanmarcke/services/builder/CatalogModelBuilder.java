package com.vanmarcke.services.builder;

import de.hybris.platform.catalog.model.CatalogModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;

import java.util.Set;

public class CatalogModelBuilder {
    private Set<CatalogVersionModel> catalogVersions;
    private CatalogVersionModel activeCatalogVersion;

    private CatalogModelBuilder() {
    }

    public static CatalogModelBuilder aCatalogModelBuilder() {
        return new CatalogModelBuilder();
    }

    public CatalogModelBuilder withCatalogVersions(final Set<CatalogVersionModel> catalogVersions) {
        this.catalogVersions = catalogVersions;
        return this;
    }

    public CatalogModelBuilder withActiveCatalogVersion(final CatalogVersionModel activeCatalogVersion) {
        this.activeCatalogVersion = activeCatalogVersion;
        return this;
    }

    public CatalogModel build() {
        final CatalogModel catalogModel = new CatalogModel();
        catalogModel.setActiveCatalogVersion(activeCatalogVersion);
        catalogModel.setCatalogVersions(catalogVersions);
        return catalogModel;
    }
}
