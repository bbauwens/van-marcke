package com.vanmarcke.services.address.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKB2BDeliveryAddressesLookupStrategyTest {

    @Mock
    private B2BUnitModel b2BUnit;

    @InjectMocks
    private VMKB2BDeliveryAddressesLookupStrategy vmkB2BDeliveryAddressesLookupStrategy;

    private AbstractOrderModel abstractOrder;

    @Before
    public void setUp() {
        this.abstractOrder = mock(AbstractOrderModel.class);
    }

    @Test
    public void testGetDeliveryAddressesForOrder() {
        final AddressModel shippingAddress = mock(AddressModel.class);
        final AddressModel shippingAddress2 = mock(AddressModel.class);
        final AddressModel billingAddress = mock(AddressModel.class);
        final List<AddressModel> allAddresses = new ArrayList<>();
        allAddresses.add(shippingAddress);
        allAddresses.add(shippingAddress2);
        allAddresses.add(billingAddress);

        when(this.abstractOrder.getUnit()).thenReturn(this.b2BUnit);
        when(this.b2BUnit.getAddresses()).thenReturn(allAddresses);
        when(this.b2BUnit.getBillingAddress()).thenReturn(billingAddress);
        when(shippingAddress.getShippingAddress()).thenReturn(true);
        when(shippingAddress2.getShippingAddress()).thenReturn(true);
        when(shippingAddress.getVisibleInAddressBook()).thenReturn(true);
        when(shippingAddress2.getVisibleInAddressBook()).thenReturn(false);
        when(billingAddress.getBillingAddress()).thenReturn(true);
        when(billingAddress.getVisibleInAddressBook()).thenReturn(true);
        final List<AddressModel> result = this.vmkB2BDeliveryAddressesLookupStrategy.getDeliveryAddressesForOrder(this.abstractOrder, false);

        assertThat(result).contains(shippingAddress).contains(shippingAddress2);
    }

    @Test
    public void testGetDeliveryAddressesForOrder_noAddresses() {
        when(this.abstractOrder.getUnit()).thenReturn(this.b2BUnit);
        final List<AddressModel> result = this.vmkB2BDeliveryAddressesLookupStrategy.getDeliveryAddressesForOrder(this.abstractOrder, true);

        assertThat(result).isNotNull().isEmpty();
    }

    @Test
    public void testGetDeliveryAddressesForOrder_onlyVisibleAddresses() {
        final AddressModel shippingAddress = mock(AddressModel.class);
        final AddressModel shippingAddress2 = mock(AddressModel.class);
        final AddressModel billingAddress = mock(AddressModel.class);
        final List<AddressModel> allAddresses = new ArrayList<>();
        allAddresses.add(shippingAddress);
        allAddresses.add(shippingAddress2);
        allAddresses.add(billingAddress);

        when(this.abstractOrder.getUnit()).thenReturn(this.b2BUnit);
        when(this.b2BUnit.getAddresses()).thenReturn(allAddresses);
        when(this.b2BUnit.getBillingAddress()).thenReturn(billingAddress);
        when(shippingAddress.getShippingAddress()).thenReturn(true);
        when(shippingAddress2.getShippingAddress()).thenReturn(true);
        when(shippingAddress.getVisibleInAddressBook()).thenReturn(true);
        when(shippingAddress2.getVisibleInAddressBook()).thenReturn(false);
        when(billingAddress.getBillingAddress()).thenReturn(true);
        when(billingAddress.getVisibleInAddressBook()).thenReturn(true);
        final List<AddressModel> result = this.vmkB2BDeliveryAddressesLookupStrategy.getDeliveryAddressesForOrder(this.abstractOrder, true);

        assertThat(result).containsExactly(shippingAddress);
    }
}