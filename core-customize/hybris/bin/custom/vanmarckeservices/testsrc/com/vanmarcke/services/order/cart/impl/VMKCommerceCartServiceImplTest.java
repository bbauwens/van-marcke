package com.vanmarcke.services.order.cart.impl;

import com.vanmarcke.services.delivery.VMKShippingInfoService;
import com.vanmarcke.services.exception.UnsupportedDeliveryDateException;
import com.vanmarcke.services.product.VMKVariantProductService;
import com.vanmarcke.services.strategies.VMKUpdateCartCheckoutModeStrategy;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.order.CommerceAddToCartStrategy;
import de.hybris.platform.commerceservices.order.CommerceCartMergingException;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartRestoration;
import de.hybris.platform.commerceservices.order.CommerceCartRestorationStrategy;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.commerceservices.strategies.CartValidationStrategy;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.variants.model.VariantProductModel;
import org.fest.assertions.Assertions;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static java.util.Collections.singletonList;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKCommerceCartServiceImplTest {

    @Mock
    private VMKUpdateCartCheckoutModeStrategy updateCartCheckoutModeStrategy;

    @Mock
    private VMKVariantProductService variantProductService;

    @Mock
    private CommerceAddToCartStrategy commerceAddToCartStrategy;

    @Mock
    private CartValidationStrategy cartValidationStrategy;

    @Mock
    private CommerceCartRestorationStrategy commerceCartRestorationStrategy;

    @Mock
    private VMKShippingInfoService shippingInfoService;

    @Mock
    private ModelService modelService;

    private VMKCommerceCartServiceImpl defaultCommerceCartService;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setUp() {
        initMocks(this);

        defaultCommerceCartService = new VMKCommerceCartServiceImpl(variantProductService, shippingInfoService);
        defaultCommerceCartService.setUpdateCartCheckoutModeStrategy(updateCartCheckoutModeStrategy);
        defaultCommerceCartService.setCommerceAddToCartStrategy(commerceAddToCartStrategy);
        defaultCommerceCartService.setCartValidationStrategy(cartValidationStrategy);
        defaultCommerceCartService.setCommerceCartRestorationStrategy(commerceCartRestorationStrategy);
        defaultCommerceCartService.setModelService(modelService);
    }

    @Test
    public void testAddToCart() throws Exception {
        defaultCommerceCartService.addToCart(anyListOf(CommerceCartParameter.class));

        verify(this.commerceAddToCartStrategy).addToCart(anyListOf(CommerceCartParameter.class));
    }

    @Test
    public void testAddToCarTWithNullValue() throws CommerceCartMergingException {
        CommerceCartParameter param = new CommerceCartParameter();

        CommerceCartModification expected = new CommerceCartModification();

        when(commerceAddToCartStrategy.addToCart(Collections.emptyList())).thenReturn(Collections.singletonList(expected));

        List<CommerceCartModification> actual = defaultCommerceCartService.addToCart(Collections.singletonList(param));

        Assertions
                .assertThat(actual)
                .containsExactly(expected);

        verify(commerceAddToCartStrategy).addToCart(Collections.emptyList());
    }

    @Test
    public void testAddToCarTWithoutVariant() throws CommerceCartMergingException {
        ProductModel product = new ProductModel();

        CommerceCartParameter param = new CommerceCartParameter();
        param.setProduct(product);

        CommerceCartModification expected = new CommerceCartModification();

        when(commerceAddToCartStrategy.addToCart(Collections.emptyList())).thenReturn(Collections.singletonList(expected));

        List<CommerceCartModification> actual = defaultCommerceCartService.addToCart(Collections.singletonList(param));

        Assertions
                .assertThat(actual)
                .containsExactly(expected);

        verify(commerceAddToCartStrategy).addToCart(Collections.emptyList());
    }

    @Test
    public void testAddToCarTWithoutPurchasableVariant() throws CommerceCartMergingException {
        VariantProductModel product = new VariantProductModel();

        when(variantProductService.isPurchasable(product)).thenReturn(false);

        CommerceCartParameter param = new CommerceCartParameter();
        param.setProduct(product);

        CommerceCartModification expected = new CommerceCartModification();

        when(commerceAddToCartStrategy.addToCart(Collections.emptyList())).thenReturn(Collections.singletonList(expected));

        List<CommerceCartModification> actual = defaultCommerceCartService.addToCart(Collections.singletonList(param));

        Assertions
                .assertThat(actual)
                .containsExactly(expected);

        verify(commerceAddToCartStrategy).addToCart(Collections.emptyList());
        verify(variantProductService).isPurchasable(product);
    }

    @Test
    public void testAddToCarTWithPurchasableVariant() throws CommerceCartMergingException {
        VariantProductModel product = new VariantProductModel();

        when(variantProductService.isPurchasable(product)).thenReturn(true);

        CommerceCartParameter param = new CommerceCartParameter();
        param.setProduct(product);

        CommerceCartModification expected = new CommerceCartModification();

        when(commerceAddToCartStrategy.addToCart(Collections.singletonList(param))).thenReturn(Collections.singletonList(expected));

        List<CommerceCartModification> actual = defaultCommerceCartService.addToCart(Collections.singletonList(param));

        Assertions
                .assertThat(actual)
                .containsExactly(expected);

        verify(commerceAddToCartStrategy).addToCart(Collections.singletonList(param));
        verify(variantProductService).isPurchasable(product);
    }

    @Test
    public void testUpdateCartCheckoutMode() {
        final CommerceCartParameter parameter = mock(CommerceCartParameter.class);

        defaultCommerceCartService.updateCartCheckoutMode(parameter);

        verify(updateCartCheckoutModeStrategy).updateCartCheckoutMode(parameter);
    }

    @Test
    public void testRestoreCart() throws Exception {
        CartModel cartModel = mock(CartModel.class);

        CommerceCartParameter parameter = mock(CommerceCartParameter.class);
        when(parameter.getCart()).thenReturn(cartModel);

        CommerceCartModification cartValidationModification = mock(CommerceCartModification.class);
        when(cartValidationStrategy.validateCart(parameter)).thenReturn(singletonList(cartValidationModification));

        CommerceCartRestoration restoration = buildCartRestoration();
        when(commerceCartRestorationStrategy.restoreCart(parameter)).thenReturn(restoration);

        CommerceCartRestoration result = defaultCommerceCartService.restoreCart(parameter);

        assertThat(result.getModifications()).contains(cartValidationModification);
    }

    @Test
    public void testRestoreCart_withoutCart() throws Exception {
        CommerceCartParameter parameter = mock(CommerceCartParameter.class);

        CommerceCartModification cartValidationModification = mock(CommerceCartModification.class);
        when(cartValidationStrategy.validateCart(parameter)).thenReturn(singletonList(cartValidationModification));

        CommerceCartRestoration restoration = buildCartRestoration();
        when(commerceCartRestorationStrategy.restoreCart(parameter)).thenReturn(restoration);

        CommerceCartRestoration result = defaultCommerceCartService.restoreCart(parameter);

        assertThat(result.getModifications()).isEmpty();

        verifyZeroInteractions(cartValidationStrategy);
    }

    private CommerceCartRestoration buildCartRestoration() {
        CommerceCartRestoration restoration = new CommerceCartRestoration();
        restoration.setModifications(new ArrayList<>());
        return restoration;
    }

    @Test
    public void testSetRequestedDeliveryDate_validDate_today() throws UnsupportedDeliveryDateException {
        // given
        final AbstractOrderEntryModel entry = new AbstractOrderEntryModel();

        final CartModel cart = new CartModel();
        cart.setEntries(Collections.singletonList(entry));

        final CommerceCartParameter parameter = new CommerceCartParameter();
        parameter.setCart(cart);

        final Date expectedDeliveryDate = getValidDate();

        when(shippingInfoService.getFirstPossibleShippingDateForCompleteCart(cart)).thenReturn(expectedDeliveryDate);

        // when
        defaultCommerceCartService.setRequestedDeliveryDate(parameter, expectedDeliveryDate);

        // then
        assertThat(cart.getDeliveryDate().getDay()).isEqualTo(expectedDeliveryDate.getDay());
        assertThat(cart.getDeliveryDate().getMonth()).isEqualTo(expectedDeliveryDate.getMonth());
        assertThat(cart.getDeliveryDate().getYear()).isEqualTo(expectedDeliveryDate.getYear());
        assertThat(cart.getEntries()).hasSize(1);
        assertThat(cart.getEntries().get(0).getDeliveryDate().getDay()).isEqualTo(expectedDeliveryDate.getDay());
        assertThat(cart.getEntries().get(0).getDeliveryDate().getMonth()).isEqualTo(expectedDeliveryDate.getMonth());
        assertThat(cart.getEntries().get(0).getDeliveryDate().getYear()).isEqualTo(expectedDeliveryDate.getYear());

        verify(modelService).save(cart);
        verify(modelService).save(entry);
    }

    @Test
    public void testSetRequestedDeliveryDate_validDate_tomorrow() throws UnsupportedDeliveryDateException {
        // given
        final AbstractOrderEntryModel entry = new AbstractOrderEntryModel();

        final CartModel cart = new CartModel();
        cart.setEntries(Collections.singletonList(entry));

        final CommerceCartParameter parameter = new CommerceCartParameter();
        parameter.setCart(cart);

        final Date firstPossibleDeliveryDate = getValidDate();

        final Calendar calendar = Calendar.getInstance();
        calendar.setTime(firstPossibleDeliveryDate);
        calendar.add(Calendar.DATE, 1);

        final Date expectedDeliveryDate = calendar.getTime();

        when(shippingInfoService.getFirstPossibleShippingDateForCompleteCart(cart)).thenReturn(firstPossibleDeliveryDate);

        // when
        defaultCommerceCartService.setRequestedDeliveryDate(parameter, expectedDeliveryDate);

        // then
        assertThat(cart.getDeliveryDate().getDay()).isEqualTo(expectedDeliveryDate.getDay());
        assertThat(cart.getDeliveryDate().getMonth()).isEqualTo(expectedDeliveryDate.getMonth());
        assertThat(cart.getDeliveryDate().getYear()).isEqualTo(expectedDeliveryDate.getYear());
        assertThat(cart.getEntries()).hasSize(1);
        assertThat(cart.getEntries().get(0).getDeliveryDate().getDay()).isEqualTo(expectedDeliveryDate.getDay());
        assertThat(cart.getEntries().get(0).getDeliveryDate().getMonth()).isEqualTo(expectedDeliveryDate.getMonth());
        assertThat(cart.getEntries().get(0).getDeliveryDate().getYear()).isEqualTo(expectedDeliveryDate.getYear());

        verify(modelService).save(cart);
        verify(modelService).save(entry);
    }

    @Test
    public void testSetRequestedDeliveryDate_invalidDate_yesterday() throws UnsupportedDeliveryDateException {
        // given
        final AbstractOrderEntryModel entry = new AbstractOrderEntryModel();

        final CartModel cart = new CartModel();
        cart.setEntries(Collections.singletonList(entry));

        final CommerceCartParameter parameter = new CommerceCartParameter();
        parameter.setCart(cart);

        final Date requestedDeliveryDate = getValidDate();

        final Calendar calendar = Calendar.getInstance();
        calendar.setTime(requestedDeliveryDate);
        calendar.add(Calendar.DATE, 1);

        final Date firstPossibleDeliveryDate = calendar.getTime();

        when(shippingInfoService.getFirstPossibleShippingDateForCompleteCart(cart)).thenReturn(firstPossibleDeliveryDate);

        // then
        thrown.expect(UnsupportedDeliveryDateException.class);

        // when
        defaultCommerceCartService.setRequestedDeliveryDate(parameter, requestedDeliveryDate);
    }

    @Test
    public void testSetRequestedDeliveryDate_invalidDate_saturday() throws UnsupportedDeliveryDateException {
        // given
        final AbstractOrderEntryModel entry = new AbstractOrderEntryModel();

        final CartModel cart = new CartModel();
        cart.setEntries(Collections.singletonList(entry));

        final CommerceCartParameter parameter = new CommerceCartParameter();
        parameter.setCart(cart);

        final Date firstPossibleDeliveryDate = getValidDate();

        final Calendar calendar = Calendar.getInstance();
        calendar.setTime(firstPossibleDeliveryDate);
        calendar.add(Calendar.DATE, Calendar.SATURDAY - calendar.get(Calendar.DAY_OF_WEEK));

        final Date requestedDeliveryDate = calendar.getTime();

        when(shippingInfoService.getFirstPossibleShippingDateForCompleteCart(cart)).thenReturn(firstPossibleDeliveryDate);

        // then
        thrown.expect(UnsupportedDeliveryDateException.class);

        // when
        defaultCommerceCartService.setRequestedDeliveryDate(parameter, requestedDeliveryDate);
    }

    @Test
    public void testSetRequestedDeliveryDate_invalidDate_sunday() throws UnsupportedDeliveryDateException {
        // given
        final AbstractOrderEntryModel entry = new AbstractOrderEntryModel();

        final CartModel cart = new CartModel();
        cart.setEntries(Collections.singletonList(entry));

        final CommerceCartParameter parameter = new CommerceCartParameter();
        parameter.setCart(cart);

        final Date firstPossibleDeliveryDate = getValidDate();

        final Calendar calendar = Calendar.getInstance();
        calendar.setTime(firstPossibleDeliveryDate);
        calendar.add(Calendar.DATE, -(calendar.get(Calendar.DAY_OF_WEEK) - Calendar.SUNDAY));

        final Date requestedDeliveryDate = calendar.getTime();

        when(shippingInfoService.getFirstPossibleShippingDateForCompleteCart(cart)).thenReturn(firstPossibleDeliveryDate);

        // then
        thrown.expect(UnsupportedDeliveryDateException.class);

        // when
        defaultCommerceCartService.setRequestedDeliveryDate(parameter, requestedDeliveryDate);
    }

    private Date getValidDate() {
        final Calendar calendar = Calendar.getInstance();
        while (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
            calendar.add(Calendar.DATE, 1);
        }
        return calendar.getTime();
    }
}