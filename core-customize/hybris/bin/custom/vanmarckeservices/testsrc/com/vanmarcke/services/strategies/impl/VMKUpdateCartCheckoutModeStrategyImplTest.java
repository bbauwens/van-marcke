package com.vanmarcke.services.strategies.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.order.CommerceCartCalculationStrategy;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.model.ModelService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

/**
 * The {@link VMKUpdateCartCheckoutModeStrategyImplTest} class contains the unit tests for the
 * {@link VMKUpdateCartCheckoutModeStrategyImpl} class.
 *
 * @author Taki Korovessis, Niels Raemaekers, Christiaan Janssen
 * @since 02-10-2019
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKUpdateCartCheckoutModeStrategyImplTest {

    @Mock
    private CommerceCartCalculationStrategy commerceCartCalculationStrategy;

    @Mock
    private ModelService modelService;

    @InjectMocks
    private VMKUpdateCartCheckoutModeStrategyImpl defaultVMKUpdateCartCheckoutModeStrategy;

    @Before
    public void setUp() {
        defaultVMKUpdateCartCheckoutModeStrategy.setModelService(modelService);
    }

    @Test
    public void testUpdateCartCheckoutMode() {
        final CommerceCartParameter parameter = mock(CommerceCartParameter.class);
        final CartModel cart = mock(CartModel.class);

        when(cart.getCheckoutMode()).thenReturn(false);

        when(parameter.getCart()).thenReturn(cart);
        when(parameter.getCheckoutMode()).thenReturn(true);

        defaultVMKUpdateCartCheckoutModeStrategy.updateCartCheckoutMode(parameter);

        verify(cart).setCheckoutMode(true);

        verify(modelService).save(cart);
        verify(modelService).refresh(cart);
        verify(commerceCartCalculationStrategy).recalculateCart(parameter);
    }

    @Test
    public void testUpdateCartCheckoutMode_NotInCheckout_CartNotCheckoutMode() {
        final CommerceCartParameter parameter = mock(CommerceCartParameter.class);
        final CartModel cart = mock(CartModel.class);

        when(cart.getCheckoutMode()).thenReturn(false);

        when(parameter.getCart()).thenReturn(cart);
        when(parameter.getCheckoutMode()).thenReturn(false);

        defaultVMKUpdateCartCheckoutModeStrategy.updateCartCheckoutMode(parameter);

        verify(cart).setDeliveryPointOfService(null);
        verify(cart, never()).setCheckoutMode(anyBoolean());

        verify(modelService).save(cart);
        verify(commerceCartCalculationStrategy).recalculateCart(parameter);
    }
}