package com.vanmarcke.services.storelocator.pos.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.i18n.I18NService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Locale;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKDistanceFormatterServiceImplTest {

    @Mock
    private I18NService i18NService;

    @InjectMocks
    private VMKDistanceFormatterServiceImpl defaultDistanceFormatterService;

    @Test
    public void test_formattedDistance_nl_BE_UsesComma() {
        Locale.Builder localeBuilder = new Locale.Builder();
        when(i18NService.getCurrentLocale()).thenReturn(localeBuilder.setLanguage("nl").setRegion("BE").build());
        String formatted = defaultDistanceFormatterService.formatDistance(1.1);
        assertThat(formatted).isEqualTo("1,10");
    }

    @Test
    public void test_formattedDistance_en_US_UsesDot() {
        Locale.Builder localeBuilder = new Locale.Builder();
        when(i18NService.getCurrentLocale()).thenReturn(localeBuilder.setLanguage("en").setRegion("US").build());
        String formatted = defaultDistanceFormatterService.formatDistance(1.1);
        assertThat(formatted).isEqualTo("1.10");
    }

}
