package com.vanmarcke.services.search.solrfacetsearch.provider.impl;

import com.vanmarcke.core.model.VanMarckeVariantProductModel;
import com.vanmarcke.services.product.VMKVariantProductService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordersplitting.WarehouseService;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;
import de.hybris.platform.stock.impl.StockLevelDao;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collection;
import java.util.Collections;
import java.util.Optional;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKEDCStockIndicatorValueProviderTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Mock
    private FieldNameProvider fieldNameProvider;
    @Mock
    private StockLevelDao stockLevelDao;
    @Mock
    private WarehouseService warehouseService;
    @Mock
    private VMKVariantProductService vmkVariantProductService;
    @Mock
    private IndexConfig indexConfig;
    @Mock
    private IndexedProperty indexedProperty;

    @InjectMocks
    private VMKEDCStockIndicatorValueProvider vmkEDCStockIndicatorValueProvider;

    @Test
    public void testGetFieldValues_wrongModel() throws Exception {
        ProductModel model = mock(ProductModel.class);

        thrown.expect(FieldValueProviderException.class);
        thrown.expectMessage("Cannot get stock status of non-VMK Variant product item");
        vmkEDCStockIndicatorValueProvider.getFieldValues(indexConfig, indexedProperty, model);

        verifyZeroInteractions(fieldNameProvider);
        verifyZeroInteractions(stockLevelDao);
        verifyZeroInteractions(warehouseService);
        verifyZeroInteractions(vmkVariantProductService);
    }

    @Test
    public void testGetFieldValues_NOSProduct() throws Exception {
        VanMarckeVariantProductModel model = mock(VanMarckeVariantProductModel.class);
        Collection<String> fieldNames = Collections.singleton("EDCStockIndicator");

        when(model.getDeliveryMethod()).thenReturn("N");
        when(fieldNameProvider.getFieldNames(indexedProperty, null)).thenReturn(fieldNames);
        Collection<FieldValue> result = vmkEDCStockIndicatorValueProvider.getFieldValues(indexConfig, indexedProperty, model);

        verify(fieldNameProvider).getFieldNames(indexedProperty, null);
        verifyZeroInteractions(stockLevelDao);
        verifyZeroInteractions(warehouseService);
        verifyZeroInteractions(vmkVariantProductService);
        assertThat(result).isNotEmpty();
        Optional<FieldValue> resultValue = result.stream().findFirst();
        assertThat(resultValue.get().getFieldName()).isEqualTo("EDCStockIndicator");
        assertThat(resultValue.get().getValue()).isEqualTo("NOSPRODUCT");
    }

    @Test
    public void testGetFieldValues_Stock() throws Exception {
        VanMarckeVariantProductModel model = mock(VanMarckeVariantProductModel.class);
        StockLevelModel stockLevel = mock(StockLevelModel.class);
        WarehouseModel warehouse = mock(WarehouseModel.class);
        Collection<String> fieldNames = Collections.singleton("EDCStockIndicator");

        when(model.getDeliveryMethod()).thenReturn("C");
        when(model.getCode()).thenReturn("01200");
        when(warehouseService.getWarehouseForCode("EDC")).thenReturn(warehouse);
        when(stockLevelDao.findStockLevel("01200", warehouse)).thenReturn(stockLevel);
        when(stockLevel.getAvailable()).thenReturn(100);
        when(vmkVariantProductService.isPurchasable(model)).thenReturn(true);
        when(fieldNameProvider.getFieldNames(indexedProperty, null)).thenReturn(fieldNames);
        Collection<FieldValue> result = vmkEDCStockIndicatorValueProvider.getFieldValues(indexConfig, indexedProperty, model);

        verify(fieldNameProvider).getFieldNames(indexedProperty, null);
        verify(warehouseService).getWarehouseForCode("EDC");
        verify(stockLevelDao).findStockLevel("01200", warehouse);
        verify(vmkVariantProductService).isPurchasable(model);
        assertThat(result).isNotEmpty();
        Optional<FieldValue> resultValue = result.stream().findFirst();
        assertThat(resultValue.get().getFieldName()).isEqualTo("EDCStockIndicator");
        assertThat(resultValue.get().getValue()).isEqualTo("STOCK");
    }

    @Test
    public void testGetFieldValues_OutOfStock() throws Exception {
        VanMarckeVariantProductModel model = mock(VanMarckeVariantProductModel.class);
        StockLevelModel stockLevel = mock(StockLevelModel.class);
        WarehouseModel warehouse = mock(WarehouseModel.class);
        Collection<String> fieldNames = Collections.singleton("EDCStockIndicator");

        when(model.getDeliveryMethod()).thenReturn("C");
        when(model.getCode()).thenReturn("01200");
        when(warehouseService.getWarehouseForCode("EDC")).thenReturn(warehouse);
        when(stockLevelDao.findStockLevel("01200", warehouse)).thenReturn(stockLevel);
        when(stockLevel.getAvailable()).thenReturn(0);
        when(vmkVariantProductService.isPurchasable(model)).thenReturn(false);
        when(fieldNameProvider.getFieldNames(indexedProperty, null)).thenReturn(fieldNames);
        Collection<FieldValue> result = vmkEDCStockIndicatorValueProvider.getFieldValues(indexConfig, indexedProperty, model);

        verify(fieldNameProvider).getFieldNames(indexedProperty, null);
        verify(warehouseService).getWarehouseForCode("EDC");
        verify(stockLevelDao).findStockLevel("01200", warehouse);
        verify(vmkVariantProductService, never()).isPotentiallyInStock(model);
        assertThat(result).isNotEmpty();
        Optional<FieldValue> resultValue = result.stream().findFirst();
        assertThat(resultValue.get().getFieldName()).isEqualTo("EDCStockIndicator");
        assertThat(resultValue.get().getValue()).isEqualTo("NOSTOCK");
    }

    @Test
    public void testGetFieldValues_StockLevelNotFound() throws Exception {
        VanMarckeVariantProductModel model = mock(VanMarckeVariantProductModel.class);
        WarehouseModel warehouse = mock(WarehouseModel.class);

        when(model.getDeliveryMethod()).thenReturn("C");
        when(model.getCode()).thenReturn("01200");
        when(warehouseService.getWarehouseForCode("EDC")).thenReturn(warehouse);
        when(stockLevelDao.findStockLevel("01200", warehouse)).thenThrow(NullPointerException.class);
        Collection<FieldValue> result = vmkEDCStockIndicatorValueProvider.getFieldValues(indexConfig, indexedProperty, model);

        verifyZeroInteractions(fieldNameProvider);
        verify(warehouseService).getWarehouseForCode("EDC");
        verify(stockLevelDao).findStockLevel("01200", warehouse);
        verifyZeroInteractions(vmkVariantProductService);
        assertThat(result).isEmpty();
    }
}
