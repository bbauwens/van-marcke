package com.vanmarcke.services.util;

import de.hybris.bootstrap.annotations.UnitTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDate;
import java.util.Date;
import java.util.Random;

import static org.fest.assertions.Assertions.assertThat;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKDateUtilsTest {
    private static final Random random = new Random();
    private static final int YEAR = 2001;
    private static final int MONTH = 7;
    private static final int DAY = 3;
    private static final String DATE_STRING = "2001-07-03";

    @Test
    public void testCreateDate() {
        Date result = VMKDateUtils.createDate(YEAR, MONTH, DAY);

        assertThat(result.getYear() + 1900).isEqualTo(YEAR);
        assertThat(result.getMonth() + 1).isEqualTo(MONTH);
        assertThat(result.getDay() + 1).isEqualTo(DAY);
        assertThat(result.getHours()).isEqualTo(0);
    }

    @Test
    public void testCreateDateStingFromInts() {
        String result = VMKDateUtils.createDateString(YEAR, MONTH, DAY);

        assertThat(result).isEqualTo(DATE_STRING);
    }

    @Test
    public void testCreateDateStingFromDate() {
        Date date = VMKDateUtils.createDate(YEAR, MONTH, DAY);

        String result = VMKDateUtils.createDateString(date);

        assertThat(result).isEqualTo(DATE_STRING);
    }

    @Test
    public void testCreateLocalDate() {
        Date date = VMKDateUtils.createDate(2020, 3, 5);

        LocalDate result = VMKDateUtils.createLocalDate(date);

        assertThat(result.getYear()).isEqualTo(2020);
        assertThat(result.getMonthValue()).isEqualTo(3);
        assertThat(result.getDayOfMonth()).isEqualTo(5);
    }

    @Test
    public void testCalendarDaysBetween_shouldReturn0ForSameDay() {
        Date from = createDay(1); //Monday
        Date to = createDay(1); //Monday

        long result = VMKDateUtils.calendarDaysBetween(from, to);

        assertThat(result).isEqualTo(0);
    }

    @Test
    public void testCalendarDaysBetween_shouldReturn0ForConsecutiveDays() {
        Date from = createDay(1); //Monday
        Date to = createDay(2); //Tuesday

        long result = VMKDateUtils.calendarDaysBetween(from, to);

        assertThat(result).isEqualTo(0);
    }

    @Test
    public void testCalendarDaysBetween_shouldReturn1For1DayBetween() {
        Date from = createDay(1); //Tuesday
        Date to = createDay(3); //Wednesday

        long result = VMKDateUtils.calendarDaysBetween(from, to);

        assertThat(result).isEqualTo(1);
    }

    @Test
    public void testCalendarDaysBetween_shouldReturn2For2DaysBetween() {
        Date from = createDay(1); //Monday
        Date to = createDay(4); //Thursday

        long result = VMKDateUtils.calendarDaysBetween(from, to);

        assertThat(result).isEqualTo(2);
    }

    @Test
    public void testIsWeekDay_shouldReturnTrueForMonday() {
        Date date = createDay(1); //Monday
        LocalDate localDate = VMKDateUtils.createLocalDate(date);

        boolean result = VMKDateUtils.isWeekDay(localDate);

        assertThat(result).isTrue();
    }

    @Test
    public void testIsWeekDay_shouldReturnFalseForSaturday() {
        Date date = createDay(6); //Saturday
        LocalDate localDate = VMKDateUtils.createLocalDate(date);

        boolean result = VMKDateUtils.isWeekDay(localDate);

        assertThat(result).isFalse();
    }

    @Test
    public void testGetWorkingDaysBetween_FridayMonday_shouldReturn_0() {
        Date startDate = createDay(5); //Friday
        Date endDAte = createDay(8); //Monday

        long result = VMKDateUtils.getWorkingDaysBetween(startDate, endDAte);

        assertThat(result).isEqualTo(0);
    }

    @Test
    public void testGetWorkingDaysBetween_SaturdayMonday_shouldReturn_0() {
        Date startDate = createDay(6); //Saturday
        Date endDate = createDay(8); //Monday

        long result = VMKDateUtils.getWorkingDaysBetween(startDate, endDate);

        assertThat(result).isEqualTo(0);
    }

    @Test
    public void testGetWorkingDaysBetween_FridayTuesday_shouldReturn_1() {
        Date startDate = createDay(5); //Friday
        Date endDate = createDay(9); //Tuesday

        long result = VMKDateUtils.getWorkingDaysBetween(startDate, endDate);

        assertThat(result).isEqualTo(1);
    }

    @Test
    public void testGetWorkingDaysBetween_SaturdayTuesday_shouldReturn_1() {
        Date startDate = createDay(6); //Saturday
        Date endDate = createDay(9); //Tuesday

        long result = VMKDateUtils.getWorkingDaysBetween(startDate, endDate);

        assertThat(result).isEqualTo(1);
    }

    @Test
    public void testGetWorkingDaysBetween_ThursdaySaturday_shouldReturn_1() {
        Date startDate = createDay(4); //Thursday
        Date endDate = createDay(6); //Saturday

        long result = VMKDateUtils.getWorkingDaysBetween(startDate, endDate);

        assertThat(result).isEqualTo(1);
    }

    @Test
    public void testGetWorkingDaysBetween_ThursdayThursday_shouldReturn_4() {
        Date startDate = createDay(4); //Thursday
        Date endDate = createDay(11); //Thursday (next)

        long result = VMKDateUtils.getWorkingDaysBetween(startDate, endDate);

        assertThat(result).isEqualTo(4);
    }

    private Date createDay(int dayNumber) {
        int sunday = 7;

        return VMKDateUtils.createDate(2021, 11, sunday + dayNumber);
    }
}