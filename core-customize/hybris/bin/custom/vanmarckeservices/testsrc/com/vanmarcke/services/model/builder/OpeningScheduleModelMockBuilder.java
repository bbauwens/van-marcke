package com.vanmarcke.services.model.builder;

import de.hybris.platform.storelocator.model.OpeningDayModel;
import de.hybris.platform.storelocator.model.OpeningScheduleModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * The {@code OpeningScheduleModelMockBuilder} class can be used to create a mock implementation of the
 * {@link OpeningScheduleModel} class.
 *
 * @author Christiaan Janssen
 * @since 07-07-2020
 */
public class OpeningScheduleModelMockBuilder {

    private List<OpeningDayModel> openingDays;


    /**
     * Creates a new instance of the {@link OpeningScheduleModelMockBuilder} class.
     */
    private OpeningScheduleModelMockBuilder() {
        openingDays = new ArrayList<>();
    }

    /**
     * The entry point to the {@link OpeningScheduleModelMockBuilder}.
     *
     * @return the {@link OpeningScheduleModelMockBuilder} instance for method chaining
     */
    public static OpeningScheduleModelMockBuilder anOpeningSchedule() {
        return new OpeningScheduleModelMockBuilder();
    }

    /**
     * Sets the {@code openingDays} on the current {@link OpeningScheduleModelMockBuilder} instance
     *
     * @param openingDays the opening days
     * @return the {@link OpeningScheduleModelMockBuilder} instance for method chaining
     */
    public OpeningScheduleModelMockBuilder withOpeningDays(OpeningDayModel... openingDays) {
        this.openingDays.addAll(Arrays.asList(openingDays));
        return this;
    }

    /**
     * Creates a mock implementation of the {@link OpeningScheduleModel} class.
     *
     * @return the {@link OpeningScheduleModel} instance
     */
    public OpeningScheduleModel build() {
        OpeningScheduleModel openingSchedule = mock(OpeningScheduleModel.class);
        when(openingSchedule.getOpeningDays()).thenReturn(openingDays);
        return openingSchedule;
    }
}
