package com.vanmarcke.services.category.impl;

import com.vanmarcke.core.model.BrandCategoryModel;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaFormatModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.servicelayer.media.MediaService;
import org.fest.assertions.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;
import java.util.Locale;
import java.util.Optional;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKCategoryServiceImplTest {

    private static final String MEDIA_FORMAT = "SMALL";

    private static final Locale LOCALE_NL = new Locale("nl");
    private static final Locale LOCALE_NL_BE = new Locale("nl_BE");
    private static final Locale LOCALE_EN = new Locale("en");
    private static final Locale[] FALLBACK_LOCALES = {LOCALE_NL, LOCALE_EN};

    @Mock
    private MediaService mediaService;

    @Mock
    private I18NService i18NService;

    @Spy
    @InjectMocks
    private VMKCategoryServiceImpl categoryService;

    @Test
    public void testGetLocalizedBrandLogoForForFormat_noBrandLogo() {
        BrandCategoryModel brand = mock(BrandCategoryModel.class);

        doReturn(emptyList()).when(categoryService).getLocalizedBrandLogo(brand);

        Optional<MediaModel> result = categoryService.getLocalizedBrandLogoForFormat(brand, MEDIA_FORMAT);

        Assertions
                .assertThat(result.isPresent())
                .isFalse();
    }

    @Test
    public void testGetLocalizedBrandLogoForFormat_success() {
        BrandCategoryModel brand = mock(BrandCategoryModel.class);
        MediaModel logo = mock(MediaModel.class);
        MediaContainerModel mediaContainer = mock(MediaContainerModel.class);

        doReturn(singletonList(mediaContainer)).when(categoryService).getLocalizedBrandLogo(brand);
        doReturn(Optional.of(logo)).when(categoryService).getBrandLogoForFormat(MEDIA_FORMAT, singletonList(mediaContainer));

        Optional<MediaModel> result = categoryService.getLocalizedBrandLogoForFormat(brand, MEDIA_FORMAT);

        Assertions
                .assertThat(result.isPresent())
                .isTrue();

        Assertions
                .assertThat(result.get())
                .isEqualTo(logo);
    }

    @Test
    public void getLocalizedBrandLogo_success() {
        BrandCategoryModel brand = mock(BrandCategoryModel.class);
        MediaContainerModel mediaContainer = mock(MediaContainerModel.class);

        when(i18NService.getCurrentLocale()).thenReturn(LOCALE_NL_BE);
        when(brand.getBrandLogo(LOCALE_NL_BE)).thenReturn(singletonList(mediaContainer));

        List<MediaContainerModel> result = categoryService.getLocalizedBrandLogo(brand);

        Assertions
                .assertThat(result)
                .containsExactly(mediaContainer);

        verify(categoryService, times(0)).getLocalizedBrandLogoWithFallbackLocale(brand);
        verify(i18NService).getCurrentLocale();
        verify(brand).getBrandLogo(LOCALE_NL_BE);
    }

    @Test
    public void getLocalizedBrandLogo_successWithFallBackLocale() {
        BrandCategoryModel brand = mock(BrandCategoryModel.class);
        MediaContainerModel mediaContainer = mock(MediaContainerModel.class);

        when(i18NService.getCurrentLocale()).thenReturn(LOCALE_NL_BE);
        when(brand.getBrandLogo(LOCALE_NL_BE)).thenReturn(emptyList());
        doReturn(singletonList(mediaContainer)).when(categoryService).getLocalizedBrandLogoWithFallbackLocale(brand);

        List<MediaContainerModel> result = categoryService.getLocalizedBrandLogo(brand);

        Assertions
                .assertThat(result)
                .containsExactly(mediaContainer);

        verify(categoryService).getLocalizedBrandLogoWithFallbackLocale(brand);
        verify(i18NService).getCurrentLocale();
        verify(brand).getBrandLogo(LOCALE_NL_BE);
    }

    @Test
    public void testGetLocalizedBrandLogoWithFallbackLocale_firstFallbackLocale() {
        BrandCategoryModel brand = mock(BrandCategoryModel.class);
        MediaContainerModel mediaContainer = mock(MediaContainerModel.class);

        when(i18NService.getCurrentLocale()).thenReturn(LOCALE_NL_BE);
        when(i18NService.getFallbackLocales(LOCALE_NL_BE)).thenReturn(FALLBACK_LOCALES);

        when(brand.getBrandLogo(LOCALE_NL)).thenReturn(singletonList(mediaContainer));

        List<MediaContainerModel> result = categoryService.getLocalizedBrandLogoWithFallbackLocale(brand);

        Assertions
                .assertThat(result)
                .containsExactly(mediaContainer);

        verify(i18NService).getCurrentLocale();
        verify(i18NService).getFallbackLocales(LOCALE_NL_BE);
        verify(brand).getBrandLogo(LOCALE_NL);
        verify(brand, never()).getBrandLogo(LOCALE_EN);
    }

    @Test
    public void testGetLocalizedBrandLogoWithFallbackLocale_secondFallbackLocale() {
        BrandCategoryModel brand = mock(BrandCategoryModel.class);
        MediaContainerModel mediaContainer = mock(MediaContainerModel.class);

        when(i18NService.getCurrentLocale()).thenReturn(LOCALE_NL_BE);
        when(i18NService.getFallbackLocales(LOCALE_NL_BE)).thenReturn(FALLBACK_LOCALES);

        when(brand.getBrandLogo(LOCALE_NL)).thenReturn(emptyList());
        when(brand.getBrandLogo(LOCALE_EN)).thenReturn(singletonList(mediaContainer));

        List<MediaContainerModel> result = categoryService.getLocalizedBrandLogoWithFallbackLocale(brand);

        Assertions
                .assertThat(result)
                .containsExactly(mediaContainer);

        verify(i18NService).getCurrentLocale();
        verify(i18NService).getFallbackLocales(LOCALE_NL_BE);
        verify(brand).getBrandLogo(LOCALE_NL);
        verify(brand).getBrandLogo(LOCALE_EN);
    }

    @Test
    public void testGetLocalizedBrandLogoWithFallbackLocale_emptyResult() {
        BrandCategoryModel brand = mock(BrandCategoryModel.class);

        when(i18NService.getCurrentLocale()).thenReturn(LOCALE_NL_BE);
        when(i18NService.getFallbackLocales(LOCALE_NL_BE)).thenReturn(FALLBACK_LOCALES);

        when(brand.getBrandLogo(LOCALE_NL)).thenReturn(emptyList());
        when(brand.getBrandLogo(LOCALE_EN)).thenReturn(emptyList());

        List<MediaContainerModel> result = categoryService.getLocalizedBrandLogoWithFallbackLocale(brand);

        Assertions
                .assertThat(result)
                .isEmpty();

        verify(i18NService).getCurrentLocale();
        verify(i18NService).getFallbackLocales(LOCALE_NL_BE);
        verify(brand).getBrandLogo(LOCALE_NL);
        verify(brand).getBrandLogo(LOCALE_EN);
    }

    @Test
    public void testGetBrandLogoForFormat_mediaServiceThrowsException() {
        MediaContainerModel mediaContainer = mock(MediaContainerModel.class);
        MediaFormatModel mediaFormat = mock(MediaFormatModel.class);

        when(mediaService.getFormat(MEDIA_FORMAT)).thenReturn(mediaFormat);
        when(mediaService.getMediaByFormat(mediaContainer, mediaFormat)).thenThrow(new ModelNotFoundException(""));

        Optional<MediaModel> result = categoryService.getBrandLogoForFormat(MEDIA_FORMAT, singletonList(mediaContainer));

        Assertions
                .assertThat(result.isPresent())
                .isFalse();
    }

    @Test
    public void testGetBrandLogoForFormat_success() {
        MediaContainerModel mediaContainer = mock(MediaContainerModel.class);
        MediaFormatModel mediaFormat = mock(MediaFormatModel.class);
        MediaModel logo = mock(MediaModel.class);

        when(mediaService.getFormat(MEDIA_FORMAT)).thenReturn(mediaFormat);
        when(mediaService.getMediaByFormat(mediaContainer, mediaFormat)).thenReturn(logo);

        Optional<MediaModel> result = categoryService.getBrandLogoForFormat(MEDIA_FORMAT, singletonList(mediaContainer));

        Assertions
                .assertThat(result.isPresent())
                .isTrue();

        Assertions
                .assertThat(result.get())
                .isEqualTo(logo);
    }
}