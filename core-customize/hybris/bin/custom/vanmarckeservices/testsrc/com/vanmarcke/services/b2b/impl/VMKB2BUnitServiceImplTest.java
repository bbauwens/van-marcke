package com.vanmarcke.services.b2b.impl;

import com.vanmarcke.core.enums.ReferenceConditionalType;
import com.vanmarcke.core.model.CommerceGroupModel;
import com.vanmarcke.core.model.IBMConfigurationModel;
import com.vanmarcke.services.basestore.VMKBaseStoreService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.europe1.enums.UserPriceGroup;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.type.TypeService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.store.BaseStoreModel;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Collection;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * The {@link VMKB2BUnitServiceImplTest} class contains the unit tests for the
 * {@link VMKB2BUnitServiceImpl} class.
 *
 * @author Joris Cryns, Niels Raemaekers, Tom Martens, Tom van den Berg, Cristi Stoica
 * @since 02-07-2019
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKB2BUnitServiceImplTest {

    @Mock
    private UserService userService;
    @Mock
    private CMSSiteService cmsSiteService;
    @Mock
    private TypeService typeService;
    @Mock
    private ModelService modelService;
    @Mock
    private VMKBaseStoreService baseStoreService;

    @Spy
    @InjectMocks
    private VMKB2BUnitServiceImpl vmkB2BUnitService;

    @Mock
    private B2BUnitModel parent;

    @Before
    public void setUp() throws Exception {
        vmkB2BUnitService.setUserService(userService);
        vmkB2BUnitService.setModelService(modelService);
        vmkB2BUnitService.setTypeService(typeService);
        vmkB2BUnitService.setCmsSiteService(cmsSiteService);
        vmkB2BUnitService.setBaseStoreService(baseStoreService);
        doReturn(parent).when(vmkB2BUnitService).getParent(any(B2BCustomerModel.class));
    }

    @Test
    public void testGetB2BUnitForCurrentUserWhenNoB2BCustomer() {
        final UserModel userModel = mock(UserModel.class);

        when(userService.getCurrentUser()).thenReturn(userModel);
        final B2BUnitModel result = vmkB2BUnitService.getB2BUnitModelForCurrentUser();

        assertThat(result).isNull();
    }

    @Test
    public void testGetB2BUnitForCurrentUserReturnsParent() {
        final B2BCustomerModel userModel = mock(B2BCustomerModel.class);

        when(userService.getCurrentUser()).thenReturn(userModel);
        final B2BUnitModel result = vmkB2BUnitService.getB2BUnitModelForCurrentUser();

        assertThat(result).isEqualTo(parent);
    }

    @Test
    public void testLookupPriceGroupFromClosestParent() {
        final B2BUnitModel b2BUnitModel = mock(B2BUnitModel.class);
        final CMSSiteModel cmsSiteModel = mock(CMSSiteModel.class);
        final CommerceGroupModel commerceGroupModel = mock(CommerceGroupModel.class);
        final UserPriceGroup userPriceGroup = mock(UserPriceGroup.class);

        when(cmsSiteService.getCurrentSite()).thenReturn(cmsSiteModel);
        when(cmsSiteModel.getCommerceGroup()).thenReturn(commerceGroupModel);
        when(commerceGroupModel.getUserPriceGroup()).thenReturn(userPriceGroup);

        vmkB2BUnitService.lookupPriceGroupFromClosestParent(b2BUnitModel);

        verify(typeService).getEnumerationValue(userPriceGroup);
    }

    @Test
    public void testSetDefaultDeliveryAddressEntry() {
        final B2BUnitModel b2BUnit = mock(B2BUnitModel.class);
        final AddressModel address = mock(AddressModel.class);
        final Collection<AddressModel> allAddresses = new ArrayList<>();
        allAddresses.add(address);

        when(b2BUnit.getAddresses()).thenReturn(allAddresses);
        vmkB2BUnitService.setDefaultDeliveryAddressEntry(b2BUnit, address);

        verify(b2BUnit).setShippingAddress(address);
        verify(modelService).save(b2BUnit);
    }

    @Test
    public void testSetDefaultDeliveryAddressEntry_notInAddressBook() {
        final B2BUnitModel b2BUnit = mock(B2BUnitModel.class);
        final AddressModel address = mock(AddressModel.class);
        final Collection<AddressModel> allAddresses = new ArrayList<>();

        when(b2BUnit.getAddresses()).thenReturn(allAddresses);
        vmkB2BUnitService.setDefaultDeliveryAddressEntry(b2BUnit, address);

        verify(b2BUnit, times(0)).setShippingAddress(address);
        verifyZeroInteractions(modelService);
    }

    @Test
    public void testMustAddReference_siteIsDIY() {
        final CMSSiteModel cmsSiteModel = mock(CMSSiteModel.class);

        when(cmsSiteService.getCurrentSite()).thenReturn(cmsSiteModel);
        when(cmsSiteModel.getChannel()).thenReturn(SiteChannel.DIY);

        boolean result = vmkB2BUnitService.mustAddReference();

        assertThat(result).isFalse();
    }

    @Test
    public void testMustAddReference_noReference() {
        final B2BCustomerModel user = mock(B2BCustomerModel.class);
        final CMSSiteModel cmsSiteModel = mock(CMSSiteModel.class);

        when(cmsSiteService.getCurrentSite()).thenReturn(cmsSiteModel);
        when(cmsSiteModel.getChannel()).thenReturn(SiteChannel.B2B);

        when(userService.getCurrentUser()).thenReturn(user);
        boolean result = vmkB2BUnitService.mustAddReference();

        verify(userService).getCurrentUser();
        assertThat(result).isFalse();
    }

    @Test
    public void testMustAddReference_MandatoryReference() {
        final B2BCustomerModel user = mock(B2BCustomerModel.class);
        final CMSSiteModel cmsSiteModel = mock(CMSSiteModel.class);

        when(cmsSiteService.getCurrentSite()).thenReturn(cmsSiteModel);
        when(cmsSiteModel.getChannel()).thenReturn(SiteChannel.B2B);

        when(userService.getCurrentUser()).thenReturn(user);
        when(parent.getRef2()).thenReturn(ReferenceConditionalType.MANDATORY);

        boolean result = vmkB2BUnitService.mustAddReference();

        verify(userService).getCurrentUser();
        assertThat(result).isTrue();
    }

    @Test
    public void testMustAddReference_OptionalReference() {
        final B2BCustomerModel user = mock(B2BCustomerModel.class);
        final CMSSiteModel cmsSiteModel = mock(CMSSiteModel.class);

        when(cmsSiteService.getCurrentSite()).thenReturn(cmsSiteModel);
        when(cmsSiteModel.getChannel()).thenReturn(SiteChannel.B2B);

        when(userService.getCurrentUser()).thenReturn(user);
        when(parent.getRef2()).thenReturn(ReferenceConditionalType.OPTIONAL);
        boolean result = vmkB2BUnitService.mustAddReference();

        verify(userService).getCurrentUser();
        assertThat(result).isFalse();
    }

    @Test
    public void testMustAddReference_ForbiddenReference() {
        final B2BCustomerModel user = mock(B2BCustomerModel.class);
        final CMSSiteModel cmsSiteModel = mock(CMSSiteModel.class);

        when(cmsSiteService.getCurrentSite()).thenReturn(cmsSiteModel);
        when(cmsSiteModel.getChannel()).thenReturn(SiteChannel.B2B);

        when(userService.getCurrentUser()).thenReturn(user);
        when(parent.getRef2()).thenReturn(ReferenceConditionalType.FORBIDDEN);
        boolean result = vmkB2BUnitService.mustAddReference();

        verify(userService).getCurrentUser();
        assertThat(result).isFalse();
    }

    @Test
    public void testMandatorySeparateInvoiceReference_noReference() {
        final B2BCustomerModel user = mock(B2BCustomerModel.class);

        when(userService.getCurrentUser()).thenReturn(user);
        boolean result = vmkB2BUnitService.mandatorySeparateInvoiceReference();

        verify(userService).getCurrentUser();
        assertThat(result).isFalse();
    }

    @Test
    public void testMandatorySeparateInvoiceReference_MandatoryReference() {
        final B2BCustomerModel user = mock(B2BCustomerModel.class);

        when(userService.getCurrentUser()).thenReturn(user);
        when(parent.getRef1()).thenReturn(ReferenceConditionalType.MANDATORY);
        boolean result = vmkB2BUnitService.mandatorySeparateInvoiceReference();

        verify(userService).getCurrentUser();
        assertThat(result).isTrue();
    }

    @Test
    public void testMandatorySeparateInvoiceReference_OptionalReference() {
        final B2BCustomerModel user = mock(B2BCustomerModel.class);

        when(userService.getCurrentUser()).thenReturn(user);
        when(parent.getRef1()).thenReturn(ReferenceConditionalType.OPTIONAL);
        boolean result = vmkB2BUnitService.mandatorySeparateInvoiceReference();

        verify(userService).getCurrentUser();
        assertThat(result).isFalse();
    }

    @Test
    public void testMandatorySeparateInvoiceReference_ForbiddenReference() {
        final B2BCustomerModel user = mock(B2BCustomerModel.class);

        when(userService.getCurrentUser()).thenReturn(user);
        when(parent.getRef1()).thenReturn(ReferenceConditionalType.FORBIDDEN);
        boolean result = vmkB2BUnitService.mandatorySeparateInvoiceReference();

        verify(userService).getCurrentUser();
        assertThat(result).isFalse();
    }

    @Test
    public void testGetCustomerB2BUnitID() {
        final B2BCustomerModel user = mock(B2BCustomerModel.class);

        when(userService.getCurrentUser()).thenReturn(user);
        when(parent.getUid()).thenReturn("V00001");
        String result = vmkB2BUnitService.getCustomerB2BUnitID();

        assertThat(result).isEqualTo("V00001");
        verify(userService).getCurrentUser();
    }

    @Test
    public void testGetCustomerB2BUnitID_fallback() {
        final CustomerModel user = mock(CustomerModel.class);
        BaseStoreModel baseStoreModel = mock(BaseStoreModel.class);
        IBMConfigurationModel ibmConfigurationModel = mock(IBMConfigurationModel.class);
        when(baseStoreService.getCurrentBaseStore()).thenReturn(baseStoreModel);
        when(baseStoreModel.getIbmConfiguration()).thenReturn(ibmConfigurationModel);
        when(ibmConfigurationModel.getFallbackCustomerId()).thenReturn("V112233");
        when(userService.getCurrentUser()).thenReturn(user);

        String result = vmkB2BUnitService.getCustomerB2BUnitID();

        assertThat(result).isEqualTo("V112233");
        verify(userService).getCurrentUser();
    }

    @Test
    public void testGetCustomerB2BUnitIdForUser() {
        final B2BCustomerModel user = mock(B2BCustomerModel.class);
        when(userService.getCurrentUser()).thenReturn(user);
        when(parent.getUid()).thenReturn("V00001");

        String result = vmkB2BUnitService.getCustomerB2BUnitID(user);
        assertThat(result).isEqualTo("V00001");
    }

    @Test
    public void testGetCustomerB2BUnitIdForUser_fallback() {
        final B2BCustomerModel user = mock(B2BCustomerModel.class);
        BaseStoreModel baseStoreModel = mock(BaseStoreModel.class);
        IBMConfigurationModel ibmConfigurationModel = mock(IBMConfigurationModel.class);
        when(baseStoreService.getCurrentBaseStore()).thenReturn(baseStoreModel);
        when(baseStoreModel.getIbmConfiguration()).thenReturn(ibmConfigurationModel);
        when(ibmConfigurationModel.getFallbackCustomerId()).thenReturn("V112233");
        doReturn(null).when(vmkB2BUnitService).getParent(user);

        String result = vmkB2BUnitService.getCustomerB2BUnitID(user);

        assertThat(result).isEqualTo("V112233");
        verify(userService).getCurrentUser();
    }

}
