package com.vanmarcke.services.util;

import de.hybris.bootstrap.annotations.UnitTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.fest.assertions.Assertions.assertThat;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ProductExportUtilTest {

    @Test
    public void trimHTMLTags() {
        String[] strHTMLTexts = {
                "<a href=\"#\">HTML Link</a>",
                "<table><tr><td>column1</td></tr></table>",
                "<br />< BR  >line break<bR/><br>",
                "<br /><BR>line break<bR/><br>",
                "<!-- html comment --><b>bold text</b>",
                "&lt;script&gt;",
                "<p>Vaillant. FelxoCompact exclusive. Water/water warmtepomp. ErP Ready.</p>"
        };

        List<String> result = new ArrayList<>();

        for (String str : strHTMLTexts) {
            result.add(ProductExportUtil.trimHTMLTags(str));
        }

        assertThat(result.get(0)).isEqualTo("HTML Link");
        assertThat(result.get(1)).isEqualTo("column1");
        assertThat(result.get(2)).isEqualTo("< BR >line break");
        assertThat(result.get(3)).isEqualTo("line break");
        assertThat(result.get(4)).isEqualTo("bold text");
        assertThat(result.get(5)).isEqualTo("<script>");
        assertThat(result.get(6)).isEqualTo("Vaillant. FelxoCompact exclusive. Water/water warmtepomp. ErP Ready.");
    }
}