package com.vanmarcke.services.action;

import com.vanmarcke.services.strategies.VMKCommerceSendOrderStrategy;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction.Transition;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.task.RetryLaterException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static de.hybris.platform.core.enums.ExportStatus.EXPORTED;
import static de.hybris.platform.core.enums.ExportStatus.NOTEXPORTED;
import static de.hybris.platform.core.enums.OrderStatus.COMPLETED;
import static de.hybris.platform.core.enums.OrderStatus.PROCESSING_ERROR;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKSendOrderToAS400ActionTest {

    @Mock
    private VMKCommerceSendOrderStrategy vmkCommerceSendOrderStrategy;
    @Mock
    private ModelService modelService;
    @InjectMocks
    private VMKSendOrderToAS400Action vmkSendOrderToAS400Action;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void testExecuteAction_ExportedOK() {
        OrderProcessModel process = mock(OrderProcessModel.class);
        OrderModel order = mock(OrderModel.class);

        when(process.getOrder()).thenReturn(order);
        when(order.getExportStatus()).thenReturn(EXPORTED);
        when(vmkCommerceSendOrderStrategy.sendOrder(order)).thenReturn(order);
        Transition result = vmkSendOrderToAS400Action.executeAction(process);

        verify(vmkCommerceSendOrderStrategy).sendOrder(order);
        assertThat(result).isEqualTo(Transition.OK);
    }

    @Test
    public void testExecuteAction_Resending() {
        OrderProcessModel process = mock(OrderProcessModel.class);
        OrderModel order = mock(OrderModel.class);

        when(process.getOrder()).thenReturn(order);
        when(order.getExternalCode()).thenReturn(null);
        when(order.getExportStatus()).thenReturn(NOTEXPORTED);
        when(vmkCommerceSendOrderStrategy.sendOrder(order)).thenReturn(order);
        expectedException.expect(RetryLaterException.class);
        vmkSendOrderToAS400Action.executeAction(process);

        verify(vmkCommerceSendOrderStrategy).sendOrder(order);
        verify(modelService).save(order);
    }

    @Test(expected = RetryLaterException.class)
    public void testExecuteAction_ResendingFailed() {
        OrderProcessModel process = mock(OrderProcessModel.class);
        OrderModel order = mock(OrderModel.class);

        when(process.getOrder()).thenReturn(order);
        when(order.getExternalCode()).thenReturn(null);
        when(order.getExportStatus()).thenReturn(NOTEXPORTED);
        when(vmkCommerceSendOrderStrategy.sendOrder(order)).thenReturn(order);
        vmkSendOrderToAS400Action.executeAction(process);

        verify(vmkCommerceSendOrderStrategy).sendOrder(order);
        verify(order).setStatus(PROCESSING_ERROR);
        verify(order).setExportStatus(NOTEXPORTED);
        verify(modelService).save(order);
    }
}