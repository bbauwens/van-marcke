package com.vanmarcke.services.search.solrfacetsearch.provider.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.url.UrlResolver;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolverTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;

import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKCategoryUrlsValueResolverTest extends AbstractValueResolverTest {

    @Mock
    private InputDocument inputDocument;
    @Mock
    private IndexerBatchContext indexerBatchContext;
    @Mock
    private IndexedProperty indexedProperty;
    @Mock
    private CategoryModel categoryModel;
    @Mock
    private UrlResolver<CategoryModel> urlResolver;

    @InjectMocks
    private VMKCategoryUrlsValueResolver valueResolver;

    @Test
    public void testAddFieldValuesWhenUrlEmpty() throws FieldValueProviderException {
        when(urlResolver.resolve(categoryModel)).thenReturn("");
        valueResolver.resolve(inputDocument, indexerBatchContext, Collections.singletonList(indexedProperty), categoryModel);
        verifyZeroInteractions(inputDocument);
    }

    @Test
    public void testAddFieldValuesWhenUrlNotEmpty() throws FieldValueProviderException {
        when(urlResolver.resolve(categoryModel)).thenReturn("URL");
        valueResolver.resolve(inputDocument, indexerBatchContext, Collections.singletonList(indexedProperty), categoryModel);
        verify(inputDocument).addField(indexedProperty, "URL", null);
    }
}