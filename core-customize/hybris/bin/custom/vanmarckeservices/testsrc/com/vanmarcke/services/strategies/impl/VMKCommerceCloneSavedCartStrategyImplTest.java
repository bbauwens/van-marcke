package com.vanmarcke.services.strategies.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.service.data.CommerceSaveCartParameter;
import de.hybris.platform.commerceservices.service.data.CommerceSaveCartResult;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.model.ModelService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKCommerceCloneSavedCartStrategyImplTest {

    @Mock
    private CartService cartService;
    @Mock
    private ModelService modelService;
    @InjectMocks
    private VMKCommerceCloneSavedCartStrategyImpl defaultVMKCommerceCloneSavedCartStrategy;

    @Test
    public void testCloneSavedCart() throws Exception {
        CartModel cart = mock(CartModel.class);
        CartModel clonedCart = mock(CartModel.class);

        when(cartService.clone(null, null, cart, null)).thenReturn(clonedCart);

        CommerceSaveCartParameter parameter = new CommerceSaveCartParameter();
        parameter.setEnableHooks(true);
        parameter.setCart(cart);

        CommerceSaveCartResult result = defaultVMKCommerceCloneSavedCartStrategy.cloneSavedCart(parameter);
        assertThat(result.getSavedCart()).isEqualTo(clonedCart);

        verify(clonedCart).setPaymentTransactions(null);
        verify(clonedCart).setCode(null);
        verify(clonedCart).setName(null);
        verify(clonedCart).setDescription(null);
        verify(clonedCart).setSavedBy(null);
        verify(clonedCart).setSaveTime(null);

        verify(modelService).save(clonedCart);
    }

}