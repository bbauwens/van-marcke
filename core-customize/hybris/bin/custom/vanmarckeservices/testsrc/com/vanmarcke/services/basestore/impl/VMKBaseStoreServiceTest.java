package com.vanmarcke.services.basestore.impl;

import com.vanmarcke.core.model.IBMConfigurationModel;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.sap.core.configuration.model.SAPConfigurationModel;
import de.hybris.platform.store.BaseStoreModel;
import org.apache.commons.lang.RandomStringUtils;
import org.fest.assertions.Assertions;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKBaseStoreServiceTest {

    private static final String BASE_URL = RandomStringUtils.randomAlphabetic(10);
    private static final String USERNAME = RandomStringUtils.randomAlphabetic(10);
    private static final String PASSWORD = RandomStringUtils.randomAlphabetic(10);

    @Spy
    @InjectMocks
    private VMKBaseStoreServiceImpl baseStoreService;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void testIsIBMBaseStore_true() {
        BaseStoreModel baseStore = mock(BaseStoreModel.class);
        IBMConfigurationModel ibmConfiguration = mock(IBMConfigurationModel.class);

        when(baseStore.getIbmConfiguration()).thenReturn(ibmConfiguration);

        Assertions
                .assertThat(baseStoreService.isIBMBaseStore(baseStore))
                .isTrue();
    }

    @Test
    public void testIsIBMBaseStore_false() {
        BaseStoreModel baseStore = mock(BaseStoreModel.class);

        Assertions
                .assertThat(baseStoreService.isIBMBaseStore(baseStore))
                .isFalse();
    }

    @Test
    public void testIsSAPBaseStore_true() {
        BaseStoreModel baseStore = mock(BaseStoreModel.class);
        SAPConfigurationModel sapConfiguration = mock(SAPConfigurationModel.class);

        when(baseStore.getSAPConfiguration()).thenReturn(sapConfiguration);

        Assertions
                .assertThat(baseStoreService.isSAPBaseStore(baseStore))
                .isTrue();
    }

    @Test
    public void testIsSAPBaseStore_false() {
        BaseStoreModel baseStore = mock(BaseStoreModel.class);

        Assertions
                .assertThat(baseStoreService.isSAPBaseStore(baseStore))
                .isFalse();
    }

    @Test
    public void testGetBaseUrl() {
        BaseStoreModel baseStore = mock(BaseStoreModel.class);
        IBMConfigurationModel ibmConfiguration = mock(IBMConfigurationModel.class);

        doReturn(baseStore).when(baseStoreService).getCurrentBaseStore();
        when(baseStore.getIbmConfiguration()).thenReturn(ibmConfiguration);
        when(ibmConfiguration.getBaseURL()).thenReturn(BASE_URL);

        Assertions
                .assertThat(baseStoreService.getBaseUrl())
                .isEqualTo(BASE_URL);
    }

    @Test
    public void testGetBaseUrl_configurationNull() {
        BaseStoreModel baseStore = mock(BaseStoreModel.class);
        IBMConfigurationModel ibmConfiguration = mock(IBMConfigurationModel.class);

        doReturn(baseStore).when(baseStoreService).getCurrentBaseStore();
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("IBM CPI base url not found");

        baseStoreService.getBaseUrl();
    }

    @Test
    public void testGetBaseUrl_baseUrlNull() {
        BaseStoreModel baseStore = mock(BaseStoreModel.class);
        IBMConfigurationModel ibmConfiguration = mock(IBMConfigurationModel.class);

        doReturn(baseStore).when(baseStoreService).getCurrentBaseStore();
        when(baseStore.getIbmConfiguration()).thenReturn(ibmConfiguration);

        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("IBM CPI base url not found");

        baseStoreService.getBaseUrl();
    }

    @Test
    public void testGetPassword() {
        BaseStoreModel baseStore = mock(BaseStoreModel.class);
        IBMConfigurationModel ibmConfiguration = mock(IBMConfigurationModel.class);

        doReturn(baseStore).when(baseStoreService).getCurrentBaseStore();
        when(baseStore.getIbmConfiguration()).thenReturn(ibmConfiguration);
        when(ibmConfiguration.getPassword()).thenReturn(PASSWORD);

        Assertions
                .assertThat(baseStoreService.getPassword())
                .isEqualTo(PASSWORD);
    }

    @Test
    public void testGetPassword_passwordNull() {
        BaseStoreModel baseStore = mock(BaseStoreModel.class);
        IBMConfigurationModel ibmConfiguration = mock(IBMConfigurationModel.class);

        doReturn(baseStore).when(baseStoreService).getCurrentBaseStore();
        when(baseStore.getIbmConfiguration()).thenReturn(ibmConfiguration);

        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("IBM CPI password not found");

        baseStoreService.getPassword();
    }

    @Test
    public void testGetPassword_configurationNull() {
        BaseStoreModel baseStore = mock(BaseStoreModel.class);
        IBMConfigurationModel ibmConfiguration = mock(IBMConfigurationModel.class);

        doReturn(baseStore).when(baseStoreService).getCurrentBaseStore();

        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("IBM CPI password not found");

        baseStoreService.getPassword();
    }

    @Test
    public void testGetUsername() {
        BaseStoreModel baseStore = mock(BaseStoreModel.class);
        IBMConfigurationModel ibmConfiguration = mock(IBMConfigurationModel.class);

        doReturn(baseStore).when(baseStoreService).getCurrentBaseStore();
        when(baseStore.getIbmConfiguration()).thenReturn(ibmConfiguration);
        when(ibmConfiguration.getUsername()).thenReturn(USERNAME);

        Assertions
                .assertThat(baseStoreService.getUsername())
                .isEqualTo(USERNAME);
    }

    @Test
    public void testGetUsername_usernameNull() {
        BaseStoreModel baseStore = mock(BaseStoreModel.class);
        IBMConfigurationModel ibmConfiguration = mock(IBMConfigurationModel.class);

        doReturn(baseStore).when(baseStoreService).getCurrentBaseStore();
        when(baseStore.getIbmConfiguration()).thenReturn(ibmConfiguration);

        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("IBM CPI username not found");

        baseStoreService.getUsername();
    }

    @Test
    public void testGetUsername_configurationNull() {
        BaseStoreModel baseStore = mock(BaseStoreModel.class);
        IBMConfigurationModel ibmConfiguration = mock(IBMConfigurationModel.class);

        doReturn(baseStore).when(baseStoreService).getCurrentBaseStore();

        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("IBM CPI username not found");

        baseStoreService.getUsername();
    }
}