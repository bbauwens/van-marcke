package com.vanmarcke.services.factories.impl;

import com.vanmarcke.services.factories.VMKCartValidationStrategyFactory;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.site.BaseSiteService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.HashMap;
import java.util.Map;

import static de.hybris.platform.commerceservices.enums.SiteChannel.B2B;
import static de.hybris.platform.commerceservices.enums.SiteChannel.B2C;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKCartValidationStrategyImplFactoryImplTest {

    @Mock
    private VMKCartValidationStrategyFactory defaultCartValidationStrategy;
    @Mock
    private VMKCartValidationStrategyFactory blueCartValidationStrategy;
    @Mock
    private BaseSiteService baseSiteService;

    @InjectMocks
    private VMKCartValidationStrategyFactoryImpl defaultCartValidationStrategyFactory;

    @Mock
    private BaseSiteModel siteModel;

    @Before
    public void setup() {
        Map<SiteChannel, VMKCartValidationStrategyFactory> factoryConfigurationMap = new HashMap<>();
        factoryConfigurationMap.put(B2B, blueCartValidationStrategy);
        defaultCartValidationStrategyFactory.setFactoryConfigurationMap(factoryConfigurationMap);
        defaultCartValidationStrategyFactory.setDefaultConfiguration(defaultCartValidationStrategy);

        when(baseSiteService.getCurrentBaseSite()).thenReturn(siteModel);
    }

    @Test
    public void testValidateCart_withModel() {
        CartModel cartModel = mock(CartModel.class);

        when(siteModel.getChannel()).thenReturn(B2C);

        defaultCartValidationStrategyFactory.validateCart(cartModel);

        verify(defaultCartValidationStrategy).validateCart(cartModel);
        verifyZeroInteractions(blueCartValidationStrategy);
    }

    @Test
    public void testValidateCart_withParameter() {
        CommerceCartParameter parameter = mock(CommerceCartParameter.class);

        when(siteModel.getChannel()).thenReturn(B2B);

        defaultCartValidationStrategyFactory.validateCart(parameter);

        verify(blueCartValidationStrategy).validateCart(parameter);
        verifyZeroInteractions(defaultCartValidationStrategy);
    }

    @Test
    public void testCleanCart() {
        CartModel cartModel = mock(CartModel.class);

        when(siteModel.getChannel()).thenReturn(B2C);

        defaultCartValidationStrategyFactory.cleanCart(cartModel);

        verify(defaultCartValidationStrategy).cleanCart(cartModel);
        verifyZeroInteractions(blueCartValidationStrategy);
    }

}