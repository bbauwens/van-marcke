package com.vanmarcke.services.cache.ehcache;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.tenant.TenantService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKTenantAwareEhCacheManagerFactoryBeanTest {

    @Mock
    private TenantService tenantService;
    private VMKTenantAwareEhCacheManagerFactoryBean vmkTenantAwareEhCacheManagerFactoryBean;

    @Before
    public void setUp() throws Exception {
        vmkTenantAwareEhCacheManagerFactoryBean = spy(new VMKTenantAwareEhCacheManagerFactoryBean(tenantService));

    }

    @Test
    public void afterPropertiesSet() {
        when(tenantService.getCurrentTenantId()).thenReturn("tenantId");

        vmkTenantAwareEhCacheManagerFactoryBean.afterPropertiesSet();

        verify(vmkTenantAwareEhCacheManagerFactoryBean).setCacheManagerName("vanMarckeCommonCache_tenantId");
        verify(vmkTenantAwareEhCacheManagerFactoryBean).superAfterPropertiesSet();
    }

}