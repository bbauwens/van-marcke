package com.vanmarcke.services.action;

import be.elision.mandrillextension.service.MandrillService;
import com.vanmarcke.core.model.VanmarckeContactModel;
import com.vanmarcke.email.converters.impl.MandrillTransportInformationConverter;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.model.PickUpDeliveryModeModel;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.store.BaseStoreModel;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Locale;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKSendTransportInformationEmailActionTest {

    private static final String TEMPLATE = RandomStringUtils.randomAlphabetic(10);
    private static final String DELIVERY_COMMENT = RandomStringUtils.randomAlphabetic(10);
    private static final String EMAIL = RandomStringUtils.randomAlphabetic(10);
    private static final String ISO_CODE = "en";

    @Mock
    private ModelService modelService;

    @Mock
    private MandrillService mandrillService;

    @Mock
    private MandrillTransportInformationConverter mandrillTransportInformationConverter;

    @InjectMocks
    private VMKSendTransportInformationEmailAction vmkSendTransportInformationEmailAction;

    @Test
    public void testExecuteAction() throws Exception {
        ZoneDeliveryModeModel shippingMode = mock(ZoneDeliveryModeModel.class);

        LanguageModel language = mock(LanguageModel.class);
        when(language.getIsocode()).thenReturn(ISO_CODE);

        BaseStoreModel store = mock(BaseStoreModel.class);
        when(store.getTransportInformationEmailTemplate(Locale.ENGLISH)).thenReturn(TEMPLATE);

        VanmarckeContactModel contactPerson = mock(VanmarckeContactModel.class);
        when(contactPerson.getEmail()).thenReturn(EMAIL);

        OrderModel order = mock(OrderModel.class);
        when(order.getDeliveryMode()).thenReturn(shippingMode);
        when(order.getLanguage()).thenReturn(language);
        when(order.getStore()).thenReturn(store);
        when(order.getContactPerson()).thenReturn(contactPerson);
        when(order.getDeliveryComment()).thenReturn(DELIVERY_COMMENT);

        OrderProcessModel process = mock(OrderProcessModel.class);
        when(process.getOrder()).thenReturn(order);

        AbstractSimpleDecisionAction.Transition result = vmkSendTransportInformationEmailAction.executeAction(process);

        assertThat(result).isEqualTo(AbstractSimpleDecisionAction.Transition.OK);

        verify(order).setStatus(OrderStatus.TRANSPORT_INFORMATION_SENT);
        verify(mandrillService).send(TEMPLATE, order, mandrillTransportInformationConverter, EMAIL);
        verify(modelService).save(order);
    }

    @Test
    public void testExecuteAction_fallbackToGeneralOrderManagerEmail() throws Exception {
        ZoneDeliveryModeModel shippingMode = mock(ZoneDeliveryModeModel.class);

        LanguageModel language = mock(LanguageModel.class);
        when(language.getIsocode()).thenReturn(ISO_CODE);

        BaseStoreModel store = mock(BaseStoreModel.class);
        when(store.getOrderManagerEmail()).thenReturn(EMAIL);
        when(store.getTransportInformationEmailTemplate(Locale.ENGLISH)).thenReturn(TEMPLATE);

        VanmarckeContactModel contactPerson = mock(VanmarckeContactModel.class);

        OrderModel order = mock(OrderModel.class);
        when(order.getDeliveryMode()).thenReturn(shippingMode);
        when(order.getLanguage()).thenReturn(language);
        when(order.getStore()).thenReturn(store);
        when(order.getContactPerson()).thenReturn(contactPerson);
        when(order.getDeliveryComment()).thenReturn(DELIVERY_COMMENT);

        OrderProcessModel process = mock(OrderProcessModel.class);
        when(process.getOrder()).thenReturn(order);

        AbstractSimpleDecisionAction.Transition result = vmkSendTransportInformationEmailAction.executeAction(process);

        assertThat(result).isEqualTo(AbstractSimpleDecisionAction.Transition.OK);

        verify(order).setStatus(OrderStatus.TRANSPORT_INFORMATION_SENT);
        verify(mandrillService).send(TEMPLATE, order, mandrillTransportInformationConverter, EMAIL);
        verify(modelService).save(order);
    }

    @Test
    public void testExecuteAction_noShipping() {
        PickUpDeliveryModeModel pickupMode = mock(PickUpDeliveryModeModel.class);

        OrderModel order = mock(OrderModel.class);
        when(order.getDeliveryMode()).thenReturn(pickupMode);
        when(order.getDeliveryComment()).thenReturn(DELIVERY_COMMENT);

        OrderProcessModel process = mock(OrderProcessModel.class);
        when(process.getOrder()).thenReturn(order);

        AbstractSimpleDecisionAction.Transition result = vmkSendTransportInformationEmailAction.executeAction(process);

        assertThat(result).isEqualTo(AbstractSimpleDecisionAction.Transition.OK);

        verifyZeroInteractions(mandrillService);
        verifyZeroInteractions(mandrillTransportInformationConverter);
        verify(modelService, never()).save(order);
    }

    @Test
    public void testExecuteAction_noDeliveryComment() {
        PickUpDeliveryModeModel pickupMode = mock(PickUpDeliveryModeModel.class);

        OrderModel order = mock(OrderModel.class);
        when(order.getDeliveryMode()).thenReturn(pickupMode);

        OrderProcessModel process = mock(OrderProcessModel.class);
        when(process.getOrder()).thenReturn(order);

        AbstractSimpleDecisionAction.Transition result = vmkSendTransportInformationEmailAction.executeAction(process);

        assertThat(result).isEqualTo(AbstractSimpleDecisionAction.Transition.OK);

        verifyZeroInteractions(mandrillService);
        verifyZeroInteractions(mandrillTransportInformationConverter);
        verify(modelService, never()).save(order);
    }

    @Test
    public void testExecuteAction_withFailure() throws Exception {
        ZoneDeliveryModeModel shippingMode = mock(ZoneDeliveryModeModel.class);

        BaseStoreModel store = mock(BaseStoreModel.class);
        when(store.getTransportInformationEmailTemplate(Locale.ENGLISH)).thenReturn(TEMPLATE);

        LanguageModel language = mock(LanguageModel.class);
        when(language.getIsocode()).thenReturn(ISO_CODE);

        VanmarckeContactModel contactPerson = mock(VanmarckeContactModel.class);
        when(contactPerson.getEmail()).thenReturn(EMAIL);

        OrderModel order = mock(OrderModel.class);
        when(order.getStore()).thenReturn(store);
        when(order.getDeliveryMode()).thenReturn(shippingMode);
        when(order.getLanguage()).thenReturn(language);
        when(order.getContactPerson()).thenReturn(contactPerson);
        when(order.getDeliveryComment()).thenReturn(DELIVERY_COMMENT);

        OrderProcessModel process = mock(OrderProcessModel.class);
        when(process.getOrder()).thenReturn(order);

        when(mandrillService.send(TEMPLATE, order, mandrillTransportInformationConverter, EMAIL)).thenThrow(Exception.class);

        AbstractSimpleDecisionAction.Transition result = vmkSendTransportInformationEmailAction.executeAction(process);

        assertThat(result).isEqualTo(AbstractSimpleDecisionAction.Transition.NOK);

        verify(order).setStatus(OrderStatus.TRANSPORT_INFORMATION_NOT_SENT);
        verify(modelService).save(order);
    }
}