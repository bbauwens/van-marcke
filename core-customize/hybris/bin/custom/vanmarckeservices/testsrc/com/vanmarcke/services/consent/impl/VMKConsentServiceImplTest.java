package com.vanmarcke.services.consent.impl;

import com.vanmarcke.core.enums.ConsentType;
import com.vanmarcke.core.enums.OptType;
import com.vanmarcke.core.model.VMKConsentLogEntryModel;
import com.vanmarcke.core.model.VMKConsentModel;
import com.vanmarcke.services.model.builder.CustomerModelMockBuilder;
import com.vanmarcke.services.model.builder.VMKConsentModelMockBuilder;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import org.apache.commons.lang.RandomStringUtils;
import org.assertj.core.util.Lists;
import org.fest.assertions.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKConsentServiceImplTest {

    private static final String IP = RandomStringUtils.randomAlphabetic(10);
    private static final String BROWSER_AGENT = RandomStringUtils.randomAlphabetic(10);
    private static final String ID = RandomStringUtils.randomAlphabetic(10);
    private static final String URL = RandomStringUtils.randomAlphabetic(10);

    @Mock
    private ModelService modelService;

    @Mock
    private UserService userService;

    @InjectMocks
    private VMKConsentServiceImpl service;

    @Test
    public void testProcessConsentsWithAnonymousUser() {
        VMKConsentModel consent = VMKConsentModelMockBuilder
                .aConsent()
                .withConsentType(ConsentType.ANALYTICAL_COOKIES)
                .withOptType(OptType.OPT_IN)
                .build();

        HttpSession session = mock(HttpSession.class);
        when(session.getId()).thenReturn(ID);

        StringBuffer url = new StringBuffer(URL);

        HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getHeader("X-FORWARDED-FOR")).thenReturn(IP);
        when(request.getHeader("User-Agent")).thenReturn(BROWSER_AGENT);
        when(request.getSession()).thenReturn(session);
        when(request.getRequestURL()).thenReturn(url);

        CustomerModel customer = mock(CustomerModel.class);

        when(userService.getCurrentUser()).thenReturn(customer);
        when(userService.isAnonymousUser(customer)).thenReturn(true);

        VMKConsentLogEntryModel logEntry = new VMKConsentLogEntryModel();

        when(modelService.create(VMKConsentLogEntryModel.class)).thenReturn(logEntry);

        service.processConsents(Collections.singletonList(consent), request);

        Assertions
                .assertThat(logEntry.getConsentDate())
                .isNotNull();

        Assertions
                .assertThat(logEntry.getIpAddress())
                .isEqualTo(IP);

        Assertions
                .assertThat(logEntry.getBrowserUserAgent())
                .isEqualTo(BROWSER_AGENT);

        Assertions
                .assertThat(logEntry.getJSessionID())
                .isEqualTo(ID);

        Assertions
                .assertThat(logEntry.getUrlConsentGiven())
                .isEqualTo(URL);

        Assertions
                .assertThat(logEntry.getConsentType())
                .isEqualTo(ConsentType.ANALYTICAL_COOKIES);

        Assertions
                .assertThat(logEntry.getOptType())
                .isEqualTo(OptType.OPT_IN);

        verify(userService).getCurrentUser();
        verify(userService).isAnonymousUser(customer);
        verify(modelService).create(VMKConsentLogEntryModel.class);
        verify(modelService).save(logEntry);
    }

    @Test
    public void testProcessConsentsWithNewConsents() {
        VMKConsentModel consent = new VMKConsentModel();
        consent.setConsentType(ConsentType.ANALYTICAL_COOKIES);
        consent.setOptType(OptType.OPT_IN);

        HttpSession session = mock(HttpSession.class);
        when(session.getId()).thenReturn(ID);

        StringBuffer url = new StringBuffer(URL);

        HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getHeader("X-FORWARDED-FOR")).thenReturn(IP);
        when(request.getHeader("User-Agent")).thenReturn(BROWSER_AGENT);
        when(request.getSession()).thenReturn(session);
        when(request.getRequestURL()).thenReturn(url);

        CustomerModel customer = CustomerModelMockBuilder
                .aCustomer()
                .build();

        when(userService.getCurrentUser()).thenReturn(customer);
        when(userService.isAnonymousUser(customer)).thenReturn(false);

        VMKConsentLogEntryModel logEntry = new VMKConsentLogEntryModel();

        when(modelService.create(VMKConsentLogEntryModel.class)).thenReturn(logEntry);

        service.processConsents(Collections.singletonList(consent), request);

        Assertions
                .assertThat(consent.getCustomer())
                .isEqualTo(customer);

        Assertions
                .assertThat(logEntry.getConsentDate())
                .isNotNull();

        Assertions
                .assertThat(logEntry.getIpAddress())
                .isEqualTo(IP);

        Assertions
                .assertThat(logEntry.getBrowserUserAgent())
                .isEqualTo(BROWSER_AGENT);

        Assertions
                .assertThat(logEntry.getJSessionID())
                .isEqualTo(ID);

        Assertions
                .assertThat(logEntry.getUrlConsentGiven())
                .isEqualTo(URL);

        Assertions
                .assertThat(logEntry.getConsentType())
                .isEqualTo(ConsentType.ANALYTICAL_COOKIES);

        Assertions
                .assertThat(logEntry.getOptType())
                .isEqualTo(OptType.OPT_IN);

        verify(userService).getCurrentUser();
        verify(userService).isAnonymousUser(customer);
        verify(modelService).save(consent);
        verify(modelService).create(VMKConsentLogEntryModel.class);
        verify(modelService).save(logEntry);
    }

    @Test
    public void testProcessConsentsWithExistingConsents() {
        VMKConsentModel consent = VMKConsentModelMockBuilder
                .aConsent()
                .withConsentType(ConsentType.ANALYTICAL_COOKIES)
                .withOptType(OptType.OPT_IN)
                .build();

        VMKConsentModel anotherConsent = new VMKConsentModel();
        anotherConsent.setConsentType(ConsentType.FUNCTIONAL_COOKIES);
        anotherConsent.setOptType(OptType.OPT_IN);

        HttpSession session = mock(HttpSession.class);
        when(session.getId()).thenReturn(ID);

        StringBuffer url = new StringBuffer(URL);

        HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getHeader("X-FORWARDED-FOR")).thenReturn(IP);
        when(request.getHeader("User-Agent")).thenReturn(BROWSER_AGENT);
        when(request.getSession()).thenReturn(session);
        when(request.getRequestURL()).thenReturn(url);

        VMKConsentModel existingConsent = new VMKConsentModel();
        existingConsent.setConsentType(ConsentType.ANALYTICAL_COOKIES);

        CustomerModel customer = CustomerModelMockBuilder
                .aCustomer()
                .withConsents(existingConsent)
                .build();

        when(userService.getCurrentUser()).thenReturn(customer);
        when(userService.isAnonymousUser(customer)).thenReturn(false);

        VMKConsentLogEntryModel logEntry = new VMKConsentLogEntryModel();
        VMKConsentLogEntryModel anotherLogEntry = new VMKConsentLogEntryModel();

        when(modelService.create(VMKConsentLogEntryModel.class))
                .thenReturn(logEntry)
                .thenReturn(anotherLogEntry);

        service.processConsents(Lists.newArrayList(consent, anotherConsent), request);

        Assertions
                .assertThat(existingConsent.getOptType())
                .isEqualTo(OptType.OPT_IN);

        Assertions
                .assertThat(anotherConsent.getCustomer())
                .isEqualTo(customer);

        Assertions
                .assertThat(logEntry.getConsentDate())
                .isNotNull();

        Assertions
                .assertThat(logEntry.getIpAddress())
                .isEqualTo(IP);

        Assertions
                .assertThat(logEntry.getBrowserUserAgent())
                .isEqualTo(BROWSER_AGENT);

        Assertions
                .assertThat(logEntry.getJSessionID())
                .isEqualTo(ID);

        Assertions
                .assertThat(logEntry.getUrlConsentGiven())
                .isEqualTo(URL);

        Assertions
                .assertThat(logEntry.getConsentType())
                .isEqualTo(ConsentType.ANALYTICAL_COOKIES);

        Assertions
                .assertThat(logEntry.getOptType())
                .isEqualTo(OptType.OPT_IN);

        Assertions
                .assertThat(anotherLogEntry.getConsentDate())
                .isNotNull();

        Assertions
                .assertThat(anotherLogEntry.getIpAddress())
                .isEqualTo(IP);

        Assertions
                .assertThat(anotherLogEntry.getBrowserUserAgent())
                .isEqualTo(BROWSER_AGENT);

        Assertions
                .assertThat(anotherLogEntry.getJSessionID())
                .isEqualTo(ID);

        Assertions
                .assertThat(anotherLogEntry.getUrlConsentGiven())
                .isEqualTo(URL);

        Assertions
                .assertThat(anotherLogEntry.getConsentType())
                .isEqualTo(ConsentType.FUNCTIONAL_COOKIES);

        Assertions
                .assertThat(anotherLogEntry.getOptType())
                .isEqualTo(OptType.OPT_IN);

        verify(userService).getCurrentUser();
        verify(userService).isAnonymousUser(customer);
        verify(modelService).save(existingConsent);
        verify(modelService).save(anotherConsent);
        verify(modelService, times(2)).create(VMKConsentLogEntryModel.class);
        verify(modelService).save(logEntry);
        verify(modelService).save(anotherLogEntry);
    }

    @Test
    public void testGetConsentsWithAnonymousUser() {
        CustomerModel customer = CustomerModelMockBuilder
                .aCustomer()
                .build();

        when(userService.getCurrentUser()).thenReturn(customer);
        when(userService.isAnonymousUser(customer)).thenReturn(true);

        List<VMKConsentModel> actualConsents = service.getConsents();

        Assertions
                .assertThat(actualConsents)
                .isEmpty();

        verify(userService).getCurrentUser();
        verify(userService).isAnonymousUser(customer);
    }

    @Test
    public void testGetConsentsWithLoggedInUser() {
        VMKConsentModel expectedConsent = VMKConsentModelMockBuilder
                .aConsent()
                .withConsentType(ConsentType.ANALYTICAL_COOKIES)
                .withOptType(OptType.OPT_IN)
                .build();

        CustomerModel customer = CustomerModelMockBuilder
                .aCustomer()
                .withConsents(expectedConsent)
                .build();

        when(userService.getCurrentUser()).thenReturn(customer);
        when(userService.isAnonymousUser(customer)).thenReturn(false);

        List<VMKConsentModel> actualConsents = service.getConsents();

        Assertions
                .assertThat(actualConsents)
                .containsExactly(expectedConsent);

        verify(userService).getCurrentUser();
        verify(userService).isAnonymousUser(customer);
    }

    @Test
    public void testGetConsentTypes() {
        List<String> consentTypes = service.getConsentTypes();

        Assertions
                .assertThat(consentTypes)
                .contains("ANALYTICAL_COOKIES", "FUNCTIONAL_COOKIES");
    }
}