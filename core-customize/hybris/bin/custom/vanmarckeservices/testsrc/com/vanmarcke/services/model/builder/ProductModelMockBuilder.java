package com.vanmarcke.services.model.builder;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.europe1.model.TaxRowModel;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.*;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * The {@code ProductModelBuilder} class can be used to create a mock implementation of the
 * {@link ProductModel} class.
 *
 * @author Christiaan Janssen
 * @since 29-04-2020
 */
public class ProductModelMockBuilder {

    private final String code;
    private final Locale locale;
    private final Map<Locale, String> name;
    private final List<VariantProductModel> variants;
    private final List<CategoryModel> supercategories;
    private final List<TaxRowModel> europe1Taxes;
    private final Map<Locale, List<MediaModel>> instructionManuals;
    private final Map<Locale, List<MediaModel>> generalManuals;
    private final Map<Locale, List<MediaModel>> userManuals;
    private final Map<Locale, List<MediaModel>> maintenanceManuals;
    private final Map<Locale, List<MediaModel>> certificates;
    private final Map<Locale, MediaModel> DOP;
    private final Map<Locale, MediaModel> normalisation;
    private final Map<Locale, MediaModel> techDataSheet;
    private final Map<Locale, MediaModel> safetyDataSheet;
    private final Map<Locale, MediaModel> productSpecificationSheet;
    private final Map<Locale, MediaModel> warranty;
    private final Map<Locale, MediaModel> sparePartsList;
    private final Map<Locale, MediaModel> ecoDataSheet;
    private final Map<Locale, List<CategoryModel>> brandCatalogs;

    private String brandSeries;
    private String brandType;
    private String deliveryMethod;
    private String vanmarckeItemBarcode;
    private String vendorItemBarcode_current;

    private List<MediaContainerModel> techDrawings;
    private List<MediaContainerModel> galleryImages;

    /**
     * Creates a new instance of the {@link ProductModelMockBuilder} class.
     *
     * @param code   the product code
     * @param locale the locale
     */
    private ProductModelMockBuilder(String code, Locale locale) {
        this.code = code;
        this.locale = locale;
        this.name = new HashMap<>();
        this.variants = new ArrayList<>();
        this.supercategories = new ArrayList<>();
        this.europe1Taxes = new ArrayList<>();
        this.instructionManuals = new HashMap<>();
        this.generalManuals = new HashMap<>();
        this.userManuals = new HashMap<>();
        this.maintenanceManuals = new HashMap<>();
        this.certificates = new HashMap<>();
        this.DOP = new HashMap<>();
        this.normalisation = new HashMap<>();
        this.techDataSheet = new HashMap<>();
        this.safetyDataSheet = new HashMap<>();
        this.productSpecificationSheet = new HashMap<>();
        this.warranty = new HashMap<>();
        this.sparePartsList = new HashMap<>();
        this.ecoDataSheet = new HashMap<>();
        this.brandCatalogs = new HashMap<>();
    }

    /**
     * The entry point to the {@link ProductModelMockBuilder}.
     *
     * @param code   the product code
     * @param locale the locale
     * @return the {@link ProductModelMockBuilder} instance for method chaining
     */
    public static ProductModelMockBuilder aBaseProduct(String code, Locale locale) {
        return new ProductModelMockBuilder(code, locale);
    }

    /**
     * Sets the given {@code name} on the current {@link ProductModelMockBuilder} instance.
     *
     * @param name   the name
     * @param locale the locale
     * @return the {@link ProductModelMockBuilder} instance for method chaining
     */
    public ProductModelMockBuilder withName(String name, Locale locale) {
        this.name.put(locale, name);
        return this;
    }

    /**
     * Sets the given {@code deliveryMethod} on the current {@link ProductModelMockBuilder} instance.
     *
     * @param deliveryMethod the delivery method
     * @return the {@link ProductModelMockBuilder} instance for method chaining
     */
    public ProductModelMockBuilder withDeliveryMethod(String deliveryMethod) {
        this.deliveryMethod = deliveryMethod;
        return this;
    }

    /**
     * Sets the given {@code vanmarckeItemBarcode} on the current {@link ProductModelMockBuilder} instance.
     *
     * @param vanmarckeItemBarcode the barcode
     * @return the {@link ProductModelMockBuilder} instance for method chaining
     */
    public ProductModelMockBuilder withVanmarckeItemBarcode(String vanmarckeItemBarcode) {
        this.vanmarckeItemBarcode = vanmarckeItemBarcode;
        return this;
    }

    /**
     * Sets the given {@code vendorItemBarcode_current} on the current {@link ProductModelMockBuilder} instance.
     *
     * @param vendorItemBarcode_current the vendor barcode
     * @return the {@link ProductModelMockBuilder} instance for method chaining
     */
    public ProductModelMockBuilder withVendorItemBarcode(String vendorItemBarcode_current) {
        this.vendorItemBarcode_current = vendorItemBarcode_current;
        return this;
    }

    /**
     * Sets the given {@code brandSeries} on the current {@link ProductModelMockBuilder} instance.
     *
     * @param brandSeries the brand series
     * @return the {@link ProductModelMockBuilder} instance for method chaining
     */
    public ProductModelMockBuilder withBrandSeries(String brandSeries) {
        this.brandSeries = brandSeries;
        return this;
    }

    /**
     * Sets the given {@code brandType} on the current {@link ProductModelMockBuilder} instance.
     *
     * @param brandType the brand type
     * @return the {@link ProductModelMockBuilder} instance for method chaining
     */
    public ProductModelMockBuilder withBrandType(String brandType) {
        this.brandType = brandType;
        return this;
    }

    /**
     * Sets the given {@code variants} on the current {@link ProductModelMockBuilder} instance.
     *
     * @param variants the variants
     * @return the {@link ProductModelMockBuilder} instance for method chaining
     */
    public ProductModelMockBuilder withVariants(VariantProductModel... variants) {
        this.variants.addAll(Arrays.asList(variants));
        return this;
    }

    /**
     * Sets the given {@code supercategories} on the current {@link ProductModelMockBuilder} instance.
     *
     * @param supercategories the super categories
     * @return the {@link ProductModelMockBuilder} instance for method chaining
     */
    public ProductModelMockBuilder withSupercategories(CategoryModel... supercategories) {
        this.supercategories.addAll(Arrays.asList(supercategories));
        return this;
    }

    /**
     * Sets the given {@code europe1Taxes} on the current {@link ProductModelMockBuilder} instance.
     *
     * @param europe1Taxes the tax rows
     * @return the {@link ProductModelMockBuilder} instance for method chaining
     */
    public ProductModelMockBuilder withEurope1Taxes(TaxRowModel... europe1Taxes) {
        this.europe1Taxes.addAll(Arrays.asList(europe1Taxes));
        return this;
    }

    /**
     * Sets the given gallery images on the current {@link ProductModelMockBuilder} instance.
     *
     * @param galleryImages the gallery images
     * @return the {@link ProductModelMockBuilder} instance for method chaining
     */
    public ProductModelMockBuilder withGalleryImages(List<MediaContainerModel> galleryImages) {
        this.galleryImages = galleryImages;
        return this;
    }

    /**
     * Sets the given technical drawings on the current {@link ProductModelMockBuilder} instance.
     *
     * @param techDrawings the technical drawings
     * @return the {@link ProductModelMockBuilder} instance for method chaining
     */
    public ProductModelMockBuilder withTechDrawings(List<MediaContainerModel> techDrawings) {
        this.techDrawings = techDrawings;
        return this;
    }

    /**
     * Sets the given {@code instruction manuals} on the current {@link ProductModelMockBuilder} instance.
     *
     * @param instructionManuals the instruction manuals
     * @return the {@link ProductModelMockBuilder} instance for method chaining
     */
    public ProductModelMockBuilder withInstructionManuals(List<MediaModel> instructionManuals) {
        this.instructionManuals.put(locale, instructionManuals);
        return this;
    }

    /**
     * Sets the given {@code general manuals} on the current {@link ProductModelMockBuilder} instance.
     *
     * @param generalManuals the general manuals
     * @return the {@link ProductModelMockBuilder} instance for method chaining
     */
    public ProductModelMockBuilder withGeneralManuals(List<MediaModel> generalManuals) {
        this.generalManuals.put(locale, generalManuals);
        return this;
    }

    /**
     * Sets the given {@code certificates} on the current {@link ProductModelMockBuilder} instance.
     *
     * @param certificates the certificates
     * @return the {@link ProductModelMockBuilder} instance for method chaining
     */
    public ProductModelMockBuilder withCertificates(List<MediaModel> certificates) {
        this.certificates.put(locale, certificates);
        return this;
    }

    /**
     * Sets the given {@code user manuals} on the current {@link ProductModelMockBuilder} instance.
     *
     * @param userManuals the user manuals
     * @return the {@link ProductModelMockBuilder} instance for method chaining
     */
    public ProductModelMockBuilder withUserManuals(List<MediaModel> userManuals) {
        this.userManuals.put(locale, userManuals);
        return this;
    }

    /**
     * Sets the given {@code maintenance manuals} on the current {@link ProductModelMockBuilder} instance.
     *
     * @param maintenanceManuals the maintenance manuals
     * @return the {@link ProductModelMockBuilder} instance for method chaining
     */
    public ProductModelMockBuilder withMaintenanceManuals(List<MediaModel> maintenanceManuals) {
        this.maintenanceManuals.put(locale, maintenanceManuals);
        return this;
    }

    /**
     * Sets the given {@code DOP} on the current {@link ProductModelMockBuilder} instance.
     *
     * @param DOP the DOP
     * @return the {@link ProductModelMockBuilder} instance for method chaining
     */
    public ProductModelMockBuilder withDOP(MediaModel DOP) {
        this.DOP.put(locale, DOP);
        return this;
    }

    /**
     * Sets the given {@code normalisation} on the current {@link ProductModelMockBuilder} instance.
     *
     * @param normalisation the normalisation
     * @return the {@link ProductModelMockBuilder} instance for method chaining
     */
    public ProductModelMockBuilder withNormalisation(MediaModel normalisation) {
        this.normalisation.put(locale, normalisation);
        return this;
    }

    /**
     * Sets the given {@code techDataSheet} on the current {@link ProductModelMockBuilder} instance.
     *
     * @param techDataSheet the techDataSheet
     * @return the {@link ProductModelMockBuilder} instance for method chaining
     */
    public ProductModelMockBuilder withTechDataSheet(MediaModel techDataSheet) {
        this.techDataSheet.put(locale, techDataSheet);
        return this;
    }

    /**
     * Sets the given {@code safetyDataSheet} on the current {@link ProductModelMockBuilder} instance.
     *
     * @param safetyDataSheet the safetyDataSheet
     * @return the {@link ProductModelMockBuilder} instance for method chaining
     */
    public ProductModelMockBuilder withSafetyDataSheet(MediaModel safetyDataSheet) {
        this.safetyDataSheet.put(locale, safetyDataSheet);
        return this;
    }

    /**
     * Sets the given {@code productSpecificationSheet} on the current {@link ProductModelMockBuilder} instance.
     *
     * @param productSpecificationSheet the productSpecificationSheet
     * @return the {@link ProductModelMockBuilder} instance for method chaining
     */
    public ProductModelMockBuilder withProductSpecificationSheet(MediaModel productSpecificationSheet) {
        this.productSpecificationSheet.put(locale, productSpecificationSheet);
        return this;
    }

    /**
     * Sets the given {@code warranty} on the current {@link ProductModelMockBuilder} instance.
     *
     * @param warranty the warranty
     * @return the {@link ProductModelMockBuilder} instance for method chaining
     */
    public ProductModelMockBuilder withWarranty(MediaModel warranty) {
        this.warranty.put(locale, warranty);
        return this;
    }

    /**
     * Sets the given {@code sparePartsList} on the current {@link ProductModelMockBuilder} instance.
     *
     * @param sparePartsList the sparePartsList
     * @return the {@link ProductModelMockBuilder} instance for method chaining
     */
    public ProductModelMockBuilder withSparePartsList(MediaModel sparePartsList) {
        this.sparePartsList.put(locale, sparePartsList);
        return this;
    }

    /**
     * Sets the given {@code ecoDataSheet} on the current {@link ProductModelMockBuilder} instance.
     *
     * @param ecoDataSheet the ecoDataSheet
     * @return the {@link ProductModelMockBuilder} instance for method chaining
     */
    public ProductModelMockBuilder withEcoDataSheet(MediaModel ecoDataSheet) {
        this.ecoDataSheet.put(locale, ecoDataSheet);
        return this;
    }

    /**
     * Sets the given {@code brandCatalogs} on the current {@link ProductModelMockBuilder} instance.
     *
     * @param brandCatalogs the brandCatalogs
     * @return the {@link ProductModelMockBuilder} instance for method chaining
     */
    public ProductModelMockBuilder withBrandCatalogs(List<CategoryModel> brandCatalogs) {
        this.brandCatalogs.put(locale, brandCatalogs);
        return this;
    }

    /**
     * Creates a mock implementation of the {@link ProductModel} class.
     *
     * @return the {@link ProductModel} instance
     */
    public ProductModel build() {
        ProductModel product = mock(ProductModel.class);
        when(product.getCode()).thenReturn(code);
        when(product.getName()).thenReturn(name.get(locale));
        when(product.getDeliveryMethod()).thenReturn(deliveryMethod);
        when(product.getVanmarckeItemBarcode()).thenReturn(vanmarckeItemBarcode);
        when(product.getVendorItemBarcode_current()).thenReturn(vendorItemBarcode_current);
        when(product.getBrandSeries()).thenReturn(brandSeries);
        when(product.getBrandType()).thenReturn(brandType);
        when(product.getVariants()).thenReturn(variants);
        when(product.getSupercategories()).thenReturn(supercategories);
        when(product.getEurope1Taxes()).thenReturn(europe1Taxes);
        when(product.getGalleryImages()).thenReturn(galleryImages);
        when(product.getTech_drawing()).thenReturn(techDrawings);
        when(product.getInstructionManuals()).thenReturn(instructionManuals.get(locale));
        when(product.getMaintenanceManuals()).thenReturn(maintenanceManuals.get(locale));
        when(product.getGeneralManuals()).thenReturn(generalManuals.get(locale));
        when(product.getUserManuals()).thenReturn(userManuals.get(locale));
        when(product.getCertificates()).thenReturn(certificates.get(locale));
        when(product.getDOP()).thenReturn(DOP.get(locale));
        when(product.getNormalisation()).thenReturn(normalisation.get(locale));
        when(product.getTech_data_sheet()).thenReturn(techDataSheet.get(locale));
        when(product.getSafety_data_sheet()).thenReturn(safetyDataSheet.get(locale));
        when(product.getProduct_specification_sheet()).thenReturn(productSpecificationSheet.get(locale));
        when(product.getWarranty()).thenReturn(warranty.get(locale));
        when(product.getSpare_parts_list()).thenReturn(sparePartsList.get(locale));
        when(product.getEco_data_sheet()).thenReturn(ecoDataSheet.get(locale));

        for (Map.Entry<Locale, String> entry : name.entrySet()) {
            when(product.getName(entry.getKey())).thenReturn(entry.getValue());
        }

        return product;
    }
}
