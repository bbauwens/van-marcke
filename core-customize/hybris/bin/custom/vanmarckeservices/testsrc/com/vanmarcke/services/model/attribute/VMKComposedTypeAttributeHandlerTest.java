package com.vanmarcke.services.model.attribute;

import com.vanmarcke.services.model.builder.VariantProductModelMockBuilder;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.type.ComposedTypeModel;
import de.hybris.platform.servicelayer.type.TypeService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKComposedTypeAttributeHandlerTest {
    @Mock
    private TypeService typeService;
    @InjectMocks
    private VMKComposedTypeAttributeHandler vmkComposedTypeAttributeHandler;

    @Test
    public void testGet_returnsRuntimeComposedTypeCode() {
        String vanMarckeVariantItemType = "VanMarckeVariantProduct";
        ProductModel vanMarckeVariant = VariantProductModelMockBuilder
                .aVariantProduct()
                .withItemType(vanMarckeVariantItemType)
                .build();
        ComposedTypeModel vmVariantComposedType = new ComposedTypeModel();

        when(typeService.getComposedTypeForCode(vanMarckeVariantItemType)).thenReturn(vmVariantComposedType);

        ComposedTypeModel result = vmkComposedTypeAttributeHandler.get(vanMarckeVariant);

        assertThat(result).isSameAs(vmVariantComposedType);
    }
}
