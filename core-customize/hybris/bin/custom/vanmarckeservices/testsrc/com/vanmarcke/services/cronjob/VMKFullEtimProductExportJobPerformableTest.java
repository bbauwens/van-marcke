package com.vanmarcke.services.cronjob;

import com.vanmarcke.core.model.FullEtimProductExportCronJobModel;
import com.vanmarcke.facades.data.ProductExportFeedResult;
import com.vanmarcke.services.ftp.FTPClient;
import com.vanmarcke.services.product.price.VMKFullEtimProductExportService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

import static de.hybris.platform.util.CSVConstants.HYBRIS_LINE_SEPARATOR;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKFullEtimProductExportJobPerformableTest {

    private static final String exportHeader = "sku;AS400 description nl;AS400 description fr;etim class code;etim class name nl;etim class name fr;" +
            "etim feature code;etim feature name nl;etim feature name fr;etim feature index;etim value;etim value name nl;etim value name fr;";
    private static final String exportLine = "somevaluesHere";

    @Mock
    private VMKFullEtimProductExportService vmkFullEtimProductExportService;

    @Mock
    private FTPClient ftpClient;

    @InjectMocks
    private VMKFullEtimProductExportJobPerformable vmkFullEtimProductExportJobPerformable;


    @Test
    public void testPerform() throws Exception {
        final FullEtimProductExportCronJobModel fullEtimProductExportCronJob = mock(FullEtimProductExportCronJobModel.class);
        final CatalogVersionModel catalogVersion = mock(CatalogVersionModel.class);
        final Date startDate = mock(Date.class);

        String exportResult = exportHeader.concat(HYBRIS_LINE_SEPARATOR).concat(exportLine).concat(HYBRIS_LINE_SEPARATOR);
        ProductExportFeedResult productExportFeedResult = new ProductExportFeedResult();
        productExportFeedResult.setFeed(exportResult);

        when(fullEtimProductExportCronJob.getCatalogVersion()).thenReturn(catalogVersion);
        when(fullEtimProductExportCronJob.getLastSuccessfulTime()).thenReturn(startDate);

        when(vmkFullEtimProductExportService.createFullEtimProductExportLines(fullEtimProductExportCronJob.getCatalogVersion()))
                .thenReturn(productExportFeedResult);

        PerformResult result = vmkFullEtimProductExportJobPerformable.perform(fullEtimProductExportCronJob);

        verify(ftpClient).send(any(InputStream.class), any(String.class));
        assertThat(result.getResult()).isEqualTo(CronJobResult.SUCCESS);
        assertThat(result.getStatus()).isEqualTo(CronJobStatus.FINISHED);
    }

    @Test
    public void testPerform_noClassifications() {
        final FullEtimProductExportCronJobModel fullEtimProductExportCronJob = mock(FullEtimProductExportCronJobModel.class);
        final CatalogVersionModel catalogVersion = mock(CatalogVersionModel.class);
        final Date startDate = mock(Date.class);

        String exportResult = "";
        ProductExportFeedResult productExportFeedResult = new ProductExportFeedResult();
        productExportFeedResult.setFeed(exportResult);

        when(fullEtimProductExportCronJob.getCatalogVersion()).thenReturn(catalogVersion);
        when(fullEtimProductExportCronJob.getLastSuccessfulTime()).thenReturn(startDate);

        when(vmkFullEtimProductExportService.createFullEtimProductExportLines(fullEtimProductExportCronJob.getCatalogVersion()))
                .thenReturn(productExportFeedResult);

        PerformResult result = vmkFullEtimProductExportJobPerformable.perform(fullEtimProductExportCronJob);

        verifyZeroInteractions(ftpClient);
        assertThat(result.getResult()).isEqualTo(CronJobResult.SUCCESS);
        assertThat(result.getStatus()).isEqualTo(CronJobStatus.FINISHED);
    }

    @Test
    public void testPerform_ftpError() throws Exception {
        final FullEtimProductExportCronJobModel fullEtimProductExportCronJob = mock(FullEtimProductExportCronJobModel.class);
        final CatalogVersionModel catalogVersion = mock(CatalogVersionModel.class);
        final Date startDate = mock(Date.class);

        String exportResult = exportHeader.concat(HYBRIS_LINE_SEPARATOR).concat(exportLine).concat(HYBRIS_LINE_SEPARATOR);
        ProductExportFeedResult productExportFeedResult = new ProductExportFeedResult();
        productExportFeedResult.setFeed(exportResult);

        when(fullEtimProductExportCronJob.getCatalogVersion()).thenReturn(catalogVersion);
        when(fullEtimProductExportCronJob.getLastSuccessfulTime()).thenReturn(startDate);

        when(vmkFullEtimProductExportService.createFullEtimProductExportLines(fullEtimProductExportCronJob.getCatalogVersion()))
                .thenReturn(productExportFeedResult);
        doThrow(IOException.class).when(ftpClient).send(any(InputStream.class), any(String.class));

        PerformResult result = vmkFullEtimProductExportJobPerformable.perform(fullEtimProductExportCronJob);

        verify(ftpClient).send(any(InputStream.class), any(String.class));
        assertThat(result.getResult()).isEqualTo(CronJobResult.ERROR);
        assertThat(result.getStatus()).isEqualTo(CronJobStatus.ABORTED);
    }


}
