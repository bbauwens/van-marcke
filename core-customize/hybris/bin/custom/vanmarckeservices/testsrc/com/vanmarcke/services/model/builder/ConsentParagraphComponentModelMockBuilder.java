package com.vanmarcke.services.model.builder;

import com.vanmarcke.core.enums.ConsentType;
import com.vanmarcke.core.model.ConsentParagraphComponentModel;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * The {@link ConsentParagraphComponentModelMockBuilder} class can be used to create a mock implementation of the
 * {@link ConsentParagraphComponentModel} class.
 *
 * @author Christiaan Janssen
 * @since 29-04-2020
 */
public class ConsentParagraphComponentModelMockBuilder {

    private boolean editable;
    private ConsentType consentType;
    private String title;
    private String content;

    /**
     * Creates a new instance of the {@link ConsentParagraphComponentModelMockBuilder} class.
     */
    private ConsentParagraphComponentModelMockBuilder() {
    }

    /**
     * The entry point to the {@link ConsentParagraphComponentModelMockBuilder}.
     *
     * @return the {@link ConsentParagraphComponentModelMockBuilder} instance for method chaining
     */
    public static ConsentParagraphComponentModelMockBuilder aContentParagraph() {
        return new ConsentParagraphComponentModelMockBuilder();
    }

    /**
     * Sets the given {@code consentType} on the current {@link ConsentParagraphComponentModelMockBuilder} instance.
     *
     * @param consentType the consent type
     * @return the {@link ConsentParagraphComponentModelMockBuilder} instance for method chaining
     */
    public ConsentParagraphComponentModelMockBuilder withConsentType(ConsentType consentType) {
        this.consentType = consentType;
        return this;
    }

    /**
     * Sets the given {@code title} on the current {@link ConsentParagraphComponentModelMockBuilder} instance.
     *
     * @param title the title
     * @return the {@link ConsentParagraphComponentModelMockBuilder} instance for method chaining
     */
    public ConsentParagraphComponentModelMockBuilder withTitle(String title) {
        this.title = title;
        return this;
    }

    /**
     * Sets the given {@code content} on the current {@link ConsentParagraphComponentModelMockBuilder} instance.
     *
     * @param content the content
     * @return the {@link ConsentParagraphComponentModelMockBuilder} instance for method chaining
     */
    public ConsentParagraphComponentModelMockBuilder withContent(String content) {
        this.content = content;
        return this;
    }

    /**
     * Sets the given {@code editable} on the current {@link ConsentParagraphComponentModelMockBuilder} instance.
     *
     * @param editable the editable flag
     * @return the {@link ConsentParagraphComponentModelMockBuilder} instance for method chaining
     */
    public ConsentParagraphComponentModelMockBuilder withEditable(boolean editable) {
        this.editable = editable;
        return this;
    }

    /**
     * Creates a mock implementation of the {@link ConsentParagraphComponentModel} class.
     *
     * @return the {@link ConsentParagraphComponentModel} instance
     */
    public ConsentParagraphComponentModel build() {
        ConsentParagraphComponentModel paragraph = mock(ConsentParagraphComponentModel.class);
        when(paragraph.getEditable()).thenReturn(editable);
        when(paragraph.getTitle()).thenReturn(title);
        when(paragraph.getContent()).thenReturn(content);
        when(paragraph.getConsentType()).thenReturn(consentType);
        return paragraph;
    }
}
