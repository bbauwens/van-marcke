package com.vanmarcke.services.model.builder;

import com.vanmarcke.core.model.VMKConsentModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * The {@code CustomerModelMockBuilder} class can be used to create a mock implementation of the
 * {@link CustomerModel} class.
 *
 * @author Christiaan Janssen
 * @since 07-07-2020
 */
public class CustomerModelMockBuilder extends UserModelMockBuilder<CustomerModelMockBuilder, CustomerModel> {

    private final List<VMKConsentModel> consents;

    /**
     * Creates a new instance of the {@link CustomerModelMockBuilder} class.
     */
    private CustomerModelMockBuilder() {
        super(CustomerModelMockBuilder.class);
        this.consents = new ArrayList<>();
    }

    /**
     * The entry point to the {@link CustomerModelMockBuilder}.
     *
     * @return the {@link CustomerModelMockBuilder} instance for method chaining
     */
    public static CustomerModelMockBuilder aCustomer() {
        return new CustomerModelMockBuilder();
    }

    /**
     * Sets the {@code consents} on the current {@link CustomerModelMockBuilder} instance
     *
     * @param consents the consents
     * @return the {@link CustomerModelMockBuilder} instance for method chaining
     */
    public CustomerModelMockBuilder withConsents(VMKConsentModel... consents) {
        this.consents.addAll(Arrays.asList(consents));
        return this;
    }

    /**
     * Creates a mock implementation of the {@link AddressModel} class.
     *
     * @return the {@link AddressModel} instance
     */
    @Override
    public CustomerModel build() {
        CustomerModel customer = mock(CustomerModel.class);
        when(customer.getConsents()).thenReturn(consents);
        return customer;
    }
}
