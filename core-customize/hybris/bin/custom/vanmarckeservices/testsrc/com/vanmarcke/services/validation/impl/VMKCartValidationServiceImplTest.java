package com.vanmarcke.services.validation.impl;

import com.vanmarcke.cpi.data.order.*;
import com.vanmarcke.cpi.services.ESBCartValidationService;
import com.vanmarcke.services.MessageData;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import jersey.repackaged.com.google.common.collect.Lists;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * The {@code DefaultVMKCartValidationServiceTest} class contains the unit tests for the
 * {@link VMKCartValidationServiceImpl} class.
 *
 * @author Christiaan Janssen
 * @since 03-02-2020
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKCartValidationServiceImplTest {

    private static final String PRODUCT_CODE = RandomStringUtils.random(10);

    @Mock
    private Converter<AbstractOrderModel, OrderRequestData> vmkCartValidationRequestConverter;

    @Mock
    private ESBCartValidationService esbCartValidationService;

    @InjectMocks
    private VMKCartValidationServiceImpl defaultVMKCartValidationService;

    @Test
    public void testIsCartValid_withoutResponse() {
        // given
        CartModel cart = mock(CartModel.class);

        OrderRequestData request = new OrderRequestData();

        when(vmkCartValidationRequestConverter.convert(cart)).thenReturn(request);

        // when
        MessageData message = defaultVMKCartValidationService.isCartValid(cart);

        // then
        assertThat(message).isNotNull();
        assertThat(message.getKey()).isEqualTo("cart.validation-error.failed");

        verify(vmkCartValidationRequestConverter).convert(cart);
        verify(esbCartValidationService).getCartValidationInformation(request);
    }

    @Test
    public void testIsCartValid_withoutResponseData() {
        // given
        CartModel cart = mock(CartModel.class);

        OrderRequestData request = new OrderRequestData();

        when(vmkCartValidationRequestConverter.convert(cart)).thenReturn(request);

        CartValidationResponseData response = new CartValidationResponseData();

        when(esbCartValidationService.getCartValidationInformation(request)).thenReturn(response);

        // when
        MessageData message = defaultVMKCartValidationService.isCartValid(cart);

        // then
        assertThat(message).isNotNull();
        assertThat(message.getKey()).isEqualTo("cart.validation-error.failed");

        verify(vmkCartValidationRequestConverter).convert(cart);
        verify(esbCartValidationService).getCartValidationInformation(request);
    }

    @Test
    public void testIsCartValid_withErrorOnHeaderLevel() {
        // given
        CartModel cart = mock(CartModel.class);

        OrderRequestData request = new OrderRequestData();

        when(vmkCartValidationRequestConverter.convert(cart)).thenReturn(request);

        CartValidationInternalResponseOrderData validationOrderResult = new CartValidationInternalResponseOrderData();
        CartValidationInternalResponseData validationResult = new CartValidationInternalResponseData();
        validationResult.setInError(true);
        validationResult.setErrorCode(RandomStringUtils.random(10));

        CartValidationResponseData response = new CartValidationResponseData();
        response.setData(validationOrderResult);
        validationOrderResult.setOrders(Collections.singletonList(validationResult));

        when(esbCartValidationService.getCartValidationInformation(request)).thenReturn(response);

        // when
        MessageData message = defaultVMKCartValidationService.isCartValid(cart);

        // then
        assertThat(message).isNotNull();
        assertThat(message.getKey()).isEqualTo("cart.validation-error.failed");

        verify(vmkCartValidationRequestConverter).convert(cart);
        verify(esbCartValidationService).getCartValidationInformation(request);
    }

    @Test
    public void testIsCartValid_withErrorOnItemLevel() {
        // given
        CartModel cart = mock(CartModel.class);

        OrderRequestData request = new OrderRequestData();

        when(vmkCartValidationRequestConverter.convert(cart)).thenReturn(request);

        CartValidationEntryResponseData validEntry = new CartValidationEntryResponseData();
        validEntry.setProductID(RandomStringUtils.random(10));

        CartValidationEntryResponseData invalidEntry = new CartValidationEntryResponseData();
        invalidEntry.setProductID(RandomStringUtils.random(10));
        invalidEntry.setErrorCode(RandomStringUtils.random(10));

        CartValidationInternalResponseOrderData validationOrderResult = new CartValidationInternalResponseOrderData();
        CartValidationInternalResponseData validationResult = new CartValidationInternalResponseData();
        validationResult.setInError(true);
        validationResult.setProducts(Lists.newArrayList(validEntry, invalidEntry));

        CartValidationResponseData response = new CartValidationResponseData();
        response.setData(validationOrderResult);
        validationOrderResult.setOrders(Collections.singletonList(validationResult));

        when(esbCartValidationService.getCartValidationInformation(request)).thenReturn(response);

        // when
        MessageData message = defaultVMKCartValidationService.isCartValid(cart);

        // then
        assertThat(message).isNotNull();
        assertThat(message.getKey()).isEqualTo("cart.validation-error.failed");

        verify(vmkCartValidationRequestConverter).convert(cart);
        verify(esbCartValidationService).getCartValidationInformation(request);
    }

    @Test
    public void testIsCartValid_withValidResponse() {
        // given
        CartModel cart = mock(CartModel.class);

        OrderRequestData request = new OrderRequestData();

        when(vmkCartValidationRequestConverter.convert(cart)).thenReturn(request);

        CartValidationInternalResponseOrderData validationOrderResult = new CartValidationInternalResponseOrderData();
        CartValidationInternalResponseData validationResult = new CartValidationInternalResponseData();
        validationResult.setInError(false);

        CartValidationResponseData response = new CartValidationResponseData();
        response.setData(validationOrderResult);
        validationOrderResult.setOrders(Collections.singletonList(validationResult));

        when(esbCartValidationService.getCartValidationInformation(request)).thenReturn(response);

        // when
        MessageData message = defaultVMKCartValidationService.isCartValid(cart);

        // then
        assertThat(message).isNull();

        verify(vmkCartValidationRequestConverter).convert(cart);
        verify(esbCartValidationService).getCartValidationInformation(request);
    }

    @Test
    public void testGetErrorMessage_with001() {
        // when
        MessageData message = defaultVMKCartValidationService.getErrorMessage("001", null);

        // then
        assertThat(message).isNotNull();
        assertThat(message.getKey()).isEqualTo("cart.validation-error.representative-not-allowed");
    }

    @Test
    public void testGetErrorMessage_with002() {
        // when
        MessageData message = defaultVMKCartValidationService.getErrorMessage("002", null);

        // then
        assertThat(message).isNotNull();
        assertThat(message.getKey()).isEqualTo("cart.validation-error.customer-not-allowed");
    }

    @Test
    public void testGetErrorMessage_with016() {
        // when
        MessageData message = defaultVMKCartValidationService.getErrorMessage("016", PRODUCT_CODE);

        // then
        assertThat(message).isNotNull();
        assertThat(message.getKey()).isEqualTo("cart.validation-error.article-not-allowed");
        assertThat(message.getAttributes()).containsExactly(PRODUCT_CODE);
    }

    @Test
    public void testGetErrorMessage_with018() {
        // when
        MessageData message = defaultVMKCartValidationService.getErrorMessage("018", PRODUCT_CODE);

        // then
        assertThat(message).isNotNull();
        assertThat(message.getKey()).isEqualTo("cart.validation-error.invalid-price");
        assertThat(message.getAttributes()).containsExactly(PRODUCT_CODE);
    }

    @Test
    public void testGetErrorMessage_with019() {
        // when
        MessageData message = defaultVMKCartValidationService.getErrorMessage("019", PRODUCT_CODE);

        // then
        assertThat(message).isNotNull();
        assertThat(message.getKey()).isEqualTo("cart.validation-error.quantity-invalid");
        assertThat(message.getAttributes()).containsExactly(PRODUCT_CODE);
    }

    @Test
    public void testGetErrorMessage_with026() {
        // when
        MessageData message = defaultVMKCartValidationService.getErrorMessage("026", null);

        // then
        assertThat(message).isNotNull();
        assertThat(message.getKey()).isEqualTo("cart.validation-error.no-confirmation");
    }

    @Test
    public void testGetErrorMessage_with027() {
        // when
        MessageData message = defaultVMKCartValidationService.getErrorMessage("027", null);

        // then
        assertThat(message).isNotNull();
        assertThat(message.getKey()).isEqualTo("cart.validation-error.no-vam");
    }

    @Test
    public void testGetErrorMessage_with028() {
        // when
        MessageData message = defaultVMKCartValidationService.getErrorMessage("028", null);

        // then
        assertThat(message).isNotNull();
        assertThat(message.getKey()).isEqualTo("cart.validation-error.shipping-address-different-from-vam");
    }

    @Test
    public void testGetErrorMessage_with029() {
        // when
        MessageData message = defaultVMKCartValidationService.getErrorMessage("029", null);

        // then
        assertThat(message).isNotNull();
        assertThat(message.getKey()).isEqualTo("cart.validation-error.pos-not-allowed");
    }

    @Test
    public void testGetErrorMessage_with030() {
        // when
        MessageData message = defaultVMKCartValidationService.getErrorMessage("030", null);

        // then
        assertThat(message).isNotNull();
        assertThat(message.getKey()).isEqualTo("cart.validation-error.pos-missing");
    }

    @Test
    public void testGetErrorMessage_with031() {
        // when
        MessageData message = defaultVMKCartValidationService.getErrorMessage("031", null);

        // then
        assertThat(message).isNotNull();
        assertThat(message.getKey()).isEqualTo("cart.validation-error.yard-reference-missing");
    }

    @Test
    public void testGetErrorMessage_with032() {
        // when
        MessageData message = defaultVMKCartValidationService.getErrorMessage("032", null);

        // then
        assertThat(message).isNotNull();
        assertThat(message.getKey()).isEqualTo("cart.validation-error.order-amount-exceeded");
    }

    @Test
    public void testGetErrorMessage_with033() {
        // when
        MessageData message = defaultVMKCartValidationService.getErrorMessage("033", null);

        // then
        assertThat(message).isNotNull();
        assertThat(message.getKey()).isEqualTo("cart.validation-error.credit-line-exceeded");
    }

    @Test
    public void testGetErrorMessage_with034() {
        // when
        MessageData message = defaultVMKCartValidationService.getErrorMessage("034", null);

        // then
        assertThat(message).isNotNull();
        assertThat(message.getKey()).isEqualTo("cart.validation-error.cash-on-delivery-not-allowed");
    }

    @Test
    public void testAddOrderSplitInformationOnCart() {
        CartValidationResponseData response = mock(CartValidationResponseData.class);
        CartModel cart = mock(CartModel.class);

//        TODO: COMPLETE
//        defaultVMKCartValidationService.addOrderSplitInformationOnCart(response, cart);
    }
}
