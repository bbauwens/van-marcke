package com.vanmarcke.services.product.price.impl;

import com.vanmarcke.services.b2b.VMKB2BUnitService;
import com.vanmarcke.services.model.builder.B2BUnitModelMockBuilder;
import com.vanmarcke.services.model.builder.VariantProductModelMockBuilder;
import com.vanmarcke.services.product.price.VMKPriceService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.commercefacades.product.data.PriceCalculationData;
import de.hybris.platform.variants.model.VariantProductModel;
import org.apache.commons.lang3.RandomStringUtils;
import org.fest.assertions.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.Locale;

import static org.mockito.Mockito.*;

/**
 * The {@code VMKNetPriceLookupStrategyImplTest} class contains the unit tests for the
 * {@link VMKNetPriceLookupStrategyImpl} class.
 *
 * @author Niels Raemaekers, Christiaan Janssen
 * @since 12-07-2019
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKNetPriceLookupStrategyImplTest {

    @Mock
    private VMKB2BUnitService vmkB2BUnitService;

    @Mock
    private VMKPriceService vmkPriceService;

    @InjectMocks
    private VMKNetPriceLookupStrategyImpl vmkNetPriceLookupStrategy;

    @Test
    public void testGetNetPriceForProductWhenVariantNull() {
        B2BUnitModel b2bUnit = B2BUnitModelMockBuilder
                .aB2BUnit(RandomStringUtils.random(10))
                .build();

        when(vmkB2BUnitService.getB2BUnitModelForCurrentUser()).thenReturn(b2bUnit);

        BigDecimal expectedNetPrice = vmkNetPriceLookupStrategy.getNetPriceForProduct(null);

        Assertions
                .assertThat(expectedNetPrice)
                .isNull();

        verifyZeroInteractions(vmkPriceService);
    }

    @Test
    public void testGetNetPriceForProductWhenB2BUnitNull() {
        VariantProductModel variant = VariantProductModelMockBuilder
                .aVariantProduct(RandomStringUtils.random(10), Locale.CANADA)
                .build();

        when(vmkB2BUnitService.getB2BUnitModelForCurrentUser()).thenReturn(null);

        BigDecimal expectedNetPrice = vmkNetPriceLookupStrategy.getNetPriceForProduct(variant);

        Assertions
                .assertThat(expectedNetPrice)
                .isNull();

        verifyZeroInteractions(vmkPriceService);
    }

    @Test
    public void testGetNetPriceForProduct() {
        VariantProductModel variant = VariantProductModelMockBuilder
                .aVariantProduct(RandomStringUtils.random(10), Locale.CANADA)
                .build();

        B2BUnitModel b2bUnit = B2BUnitModelMockBuilder
                .aB2BUnit(RandomStringUtils.random(10))
                .build();

        PriceCalculationData priceCalculationData = new PriceCalculationData();
        priceCalculationData.setPriceValue(BigDecimal.TEN);
        when(vmkB2BUnitService.getB2BUnitModelForCurrentUser()).thenReturn(b2bUnit);


        when(vmkPriceService.getPrice(variant, b2bUnit.getUid())).thenReturn(priceCalculationData);

        BigDecimal expectedNetPrice = vmkNetPriceLookupStrategy.getNetPriceForProduct(variant);

        Assertions
                .assertThat(expectedNetPrice)
                .isEqualTo(BigDecimal.TEN);

        verify(vmkB2BUnitService).getB2BUnitModelForCurrentUser();
        verify(vmkPriceService).getPrice(variant, b2bUnit.getUid());
    }
}