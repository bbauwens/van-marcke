package com.vanmarcke.services.builder;

import de.hybris.platform.catalog.model.CatalogModel;
import de.hybris.platform.store.BaseStoreModel;

import java.util.List;

public class BaseStoreModelBuilder {
    private String uid;
    private List<CatalogModel> catalogs;

    private BaseStoreModelBuilder() {
    }

    public static BaseStoreModelBuilder aBaseStoreModel() {
        return new BaseStoreModelBuilder();
    }

    public BaseStoreModelBuilder withUid(final String uid) {
        this.uid = uid;
        return this;
    }

    public BaseStoreModelBuilder withCatalogs(final List<CatalogModel> catalogs) {
        this.catalogs = catalogs;
        return this;
    }

    public BaseStoreModel build() {
        final BaseStoreModel baseStore = new BaseStoreModel();
        baseStore.setUid(uid);
        baseStore.setCatalogs(catalogs);
        return baseStore;
    }
}
