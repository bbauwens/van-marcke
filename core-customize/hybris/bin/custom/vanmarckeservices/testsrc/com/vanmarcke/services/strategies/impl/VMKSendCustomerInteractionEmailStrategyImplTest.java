package com.vanmarcke.services.strategies.impl;

import com.vanmarcke.core.model.CustomerInteractionModel;
import com.vanmarcke.services.event.events.SubmitCustomerInteractionEvent;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.event.EventService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKSendCustomerInteractionEmailStrategyImplTest {

    @Mock
    private EventService eventService;

    @InjectMocks
    private VMKSendCustomerInteractionEmailStrategyImpl defaultVMKSendCustomerInteractionEmailStrategy;

    @Captor
    private ArgumentCaptor<SubmitCustomerInteractionEvent> submitCustomerInteractionEventCaptor;

    @Test
    public void sendCustomerInteractionForOrder() {
        CustomerInteractionModel expected = mock(CustomerInteractionModel.class);
        defaultVMKSendCustomerInteractionEmailStrategy.sendCustomerInteractionForOrder(expected);

        verify(eventService).publishEvent(submitCustomerInteractionEventCaptor.capture());

        assertThat(submitCustomerInteractionEventCaptor.getValue().getCustomerInteraction()).isEqualTo(expected);
    }
}