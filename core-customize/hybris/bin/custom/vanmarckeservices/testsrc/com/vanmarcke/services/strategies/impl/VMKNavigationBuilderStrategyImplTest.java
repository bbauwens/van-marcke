package com.vanmarcke.services.strategies.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.model.contents.components.CMSLinkComponentModel;
import de.hybris.platform.cms2.model.navigation.CMSNavigationEntryModel;
import de.hybris.platform.cms2.model.navigation.CMSNavigationNodeModel;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.product.ProductService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static java.util.Collections.emptyList;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKNavigationBuilderStrategyImplTest {

    @Mock
    private ProductService productService;
    @InjectMocks
    public VMKNavigationBuilderStrategyImpl defaultVMKNavigationBuilderStrategy;

    @Test
    public void testGet_noChildren() {
        BaseSiteModel baseSiteModel = mock(BaseSiteModel.class);
        CMSNavigationNodeModel navigationNode = mock(CMSNavigationNodeModel.class);

        when(navigationNode.getChildren()).thenReturn(emptyList());
        List<CMSNavigationNodeModel> result = defaultVMKNavigationBuilderStrategy.buildNavigation(baseSiteModel, navigationNode);

        assertThat(result).isEqualTo(emptyList());
        verifyZeroInteractions(productService);
    }

    @Test
    public void testGet_withChildren_noEntries() {
        BaseSiteModel baseSiteModel = mock(BaseSiteModel.class);
        CMSNavigationNodeModel navigationNode = mock(CMSNavigationNodeModel.class);
        CMSNavigationNodeModel child1 = mock(CMSNavigationNodeModel.class);
        CMSNavigationNodeModel child2 = mock(CMSNavigationNodeModel.class);
        List<CMSNavigationNodeModel> children = new ArrayList<>();
        children.add(child1);
        children.add(child2);

        when(navigationNode.getChildren()).thenReturn(children);
        when(child1.getEntries()).thenReturn(emptyList());
        when(child2.getEntries()).thenReturn(emptyList());
        List<CMSNavigationNodeModel> result = defaultVMKNavigationBuilderStrategy.buildNavigation(baseSiteModel, navigationNode);

        assertThat(result).isEqualTo(emptyList());
        verifyZeroInteractions(productService);
    }

    @Test
    public void testGet_withChildren_withEntries_child1HasProductsSomewhere() {
        BaseSiteModel baseSiteModel = mock(BaseSiteModel.class);
        CMSNavigationNodeModel navigationNode = mock(CMSNavigationNodeModel.class);
        CMSNavigationNodeModel child1 = mock(CMSNavigationNodeModel.class);
        CMSNavigationNodeModel child2 = mock(CMSNavigationNodeModel.class);
        List<CMSNavigationNodeModel> children = new ArrayList<>();
        children.add(child1);
        children.add(child2);
        List<CMSNavigationEntryModel> navigationEntries1 = new ArrayList<>();
        List<CMSNavigationEntryModel> navigationEntries2 = new ArrayList<>();
        CMSNavigationEntryModel entry1 = mock(CMSNavigationEntryModel.class);
        CMSNavigationEntryModel entry2 = mock(CMSNavigationEntryModel.class);
        CMSNavigationEntryModel entry3 = mock(CMSNavigationEntryModel.class);
        navigationEntries1.add(entry1);
        navigationEntries1.add(entry2);
        navigationEntries2.add(entry3);
        ItemModel item1 = mock(ItemModel.class);
        CMSLinkComponentModel item2 = mock(CMSLinkComponentModel.class);
        CMSLinkComponentModel item3 = mock(CMSLinkComponentModel.class);
        CategoryModel cat1 = mock(CategoryModel.class);
        CategoryModel cat2 = mock(CategoryModel.class);

        when(navigationNode.getChildren()).thenReturn(children);
        when(child1.getEntries()).thenReturn(navigationEntries1);
        when(child2.getEntries()).thenReturn(navigationEntries2);
        when(entry1.getItem()).thenReturn(item1);
        when(entry2.getItem()).thenReturn(item2);
        when(entry3.getItem()).thenReturn(item3);
        when(item2.getCategory()).thenReturn(cat1);
        when(productService.containsProductsForCategory(cat1)).thenReturn(false);
        when(item3.getCategory()).thenReturn(cat2);
        when(productService.containsProductsForCategory(cat2)).thenReturn(true);
        List<CMSNavigationNodeModel> result = defaultVMKNavigationBuilderStrategy.buildNavigation(baseSiteModel, navigationNode);

        assertThat(result).isNotEmpty();
        assertThat(result).contains(child2);
        verify(productService, times(2)).containsProductsForCategory(any(CategoryModel.class));
    }

}