package com.vanmarcke.services.search.solrfacetsearch.provider.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.category.CommerceCategoryService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Locale;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKCategoryModelUrlResolverTest {

    @Mock
    private CommerceCategoryService commerceCategoryService;
    @InjectMocks
    private VMKCategoryModelUrlResolver vmkCategoryModelUrlResolver;

    @Before
    public void setup() {
        vmkCategoryModelUrlResolver.setPattern("/{category-path}/c/{category-code}");
    }

    @Test
    public void testResolveInternal() {
        CategoryModel categoryModel = mock(CategoryModel.class);
        CategoryModel categoryModel1 = mock(CategoryModel.class);
        CategoryModel categoryModel2 = mock(CategoryModel.class);

        when(categoryModel.getCode()).thenReturn("category-code");

        when(categoryModel1.getName()).thenReturn("parent-name-1");
        when(categoryModel2.getName()).thenReturn("parent-name-2");

        when(commerceCategoryService.getPathsForCategory(categoryModel)).thenReturn(singletonList(asList(categoryModel1, categoryModel2)));

        String result = vmkCategoryModelUrlResolver.resolveInternal(categoryModel);

        assertThat(result).isEqualTo("/parent-name-1/parent-name-2/c/category-code");
    }

    @Test
    public void testResolveInternal_withEmptyCategory1() {
        CategoryModel categoryModel = mock(CategoryModel.class);
        CategoryModel categoryModel1 = mock(CategoryModel.class);
        CategoryModel categoryModel2 = mock(CategoryModel.class);

        when(categoryModel.getCode()).thenReturn("category-code");

        when(categoryModel2.getName()).thenReturn("parent-name-2");

        when(commerceCategoryService.getPathsForCategory(categoryModel)).thenReturn(singletonList(asList(categoryModel1, categoryModel2)));

        String result = vmkCategoryModelUrlResolver.resolveInternal(categoryModel);

        assertThat(result).isEqualTo("/parent-name-2/c/category-code");
    }

    @Test
    public void testResolveInternal_withEmptyCategory2() {
        CategoryModel categoryModel = mock(CategoryModel.class);
        CategoryModel categoryModel1 = mock(CategoryModel.class);
        CategoryModel categoryModel2 = mock(CategoryModel.class);

        when(categoryModel.getCode()).thenReturn("category-code");

        when(categoryModel1.getName()).thenReturn("parent-name-1");

        when(commerceCategoryService.getPathsForCategory(categoryModel)).thenReturn(singletonList(asList(categoryModel1, categoryModel2)));

        String result = vmkCategoryModelUrlResolver.resolveInternal(categoryModel);

        assertThat(result).isEqualTo("/parent-name-1/c/category-code");
    }

    @Test
    public void testResolveInternal_withEmptyCategoryNames() {
        CategoryModel categoryModel = mock(CategoryModel.class);
        CategoryModel categoryModel1 = mock(CategoryModel.class);
        CategoryModel categoryModel2 = mock(CategoryModel.class);

        when(categoryModel.getCode()).thenReturn("category-code");

        when(commerceCategoryService.getPathsForCategory(categoryModel)).thenReturn(singletonList(asList(categoryModel1, categoryModel2)));

        String result = vmkCategoryModelUrlResolver.resolveInternal(categoryModel);

        assertThat(result).isEqualTo("/c/category-code");
    }

    @Test
    public void testResolveInternal_withoutCategories() {
        CategoryModel categoryModel = mock(CategoryModel.class);

        when(categoryModel.getCode()).thenReturn("category-code");

        when(commerceCategoryService.getPathsForCategory(categoryModel)).thenReturn(singletonList(emptyList()));

        String result = vmkCategoryModelUrlResolver.resolveInternal(categoryModel);

        assertThat(result).isEqualTo("/c/category-code");
    }

    @Test
    public void testResolveInternalLocalized() {
        CategoryModel categoryModel = mock(CategoryModel.class);
        CategoryModel categoryModel1 = mock(CategoryModel.class);
        CategoryModel categoryModel2 = mock(CategoryModel.class);

        when(categoryModel.getCode()).thenReturn("category-code");

        when(categoryModel1.getName(Locale.ENGLISH)).thenReturn("parent-name-1-en");
        when(categoryModel2.getName(Locale.ENGLISH)).thenReturn("parent-name-2-en");

        when(commerceCategoryService.getPathsForCategory(categoryModel)).thenReturn(singletonList(asList(categoryModel1, categoryModel2)));

        String result = vmkCategoryModelUrlResolver.resolveInternal(categoryModel, Locale.ENGLISH);

        assertThat(result).isEqualTo("/parent-name-1-en/parent-name-2-en/c/category-code");
    }
}