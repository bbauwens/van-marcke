package com.vanmarcke.services.builder;

import de.hybris.platform.catalog.model.CatalogModel;
import de.hybris.platform.catalog.model.classification.ClassificationSystemModel;
import de.hybris.platform.catalog.model.classification.ClassificationSystemVersionModel;
import de.hybris.platform.core.PK;

public class ClassificationSystemVersionModelBuilder {

    private final ClassificationSystemVersionModel classificationSystemVersion = new ClassificationSystemVersionModel();

    /**
     * Private constructor to hide the implicit default one.
     */
    private ClassificationSystemVersionModelBuilder() {
    }

    /**
     * A new instance of the {@link ClassificationSystemVersionModelBuilder} is created and returned
     *
     * @return the created {@link ClassificationSystemVersionModelBuilder} instance
     */
    public static ClassificationSystemVersionModelBuilder aClassificationSystemVersionModel() {
        return new ClassificationSystemVersionModelBuilder();
    }

    /**
     * Sets the given {@code catalog} on the current {@link ClassificationSystemModel} instance.
     *
     * @param catalog the catalog
     * @return the current {@link ClassificationSystemVersionModelBuilder} instance for method chaining
     */
    public ClassificationSystemVersionModelBuilder withCatalog(CatalogModel catalog) {
        classificationSystemVersion.setCatalog(catalog);
        return this;
    }

    /**
     * Returns the current {@link ClassificationSystemModel} instance
     *
     * @return the current {@link ClassificationSystemModel} instance
     */
    public ClassificationSystemVersionModel build() {
        return classificationSystemVersion;
    }
}
