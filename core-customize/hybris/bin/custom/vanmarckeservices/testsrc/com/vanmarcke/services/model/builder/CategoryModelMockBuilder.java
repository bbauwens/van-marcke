package com.vanmarcke.services.model.builder;

import de.hybris.platform.category.model.CategoryModel;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * The {@code CategoryModelMockBuilder} class can be used to create a mock implementation of the
 * {@link CategoryModel} class.
 *
 * @author Christiaan Janssen
 * @since 20-04-2020
 */
public class CategoryModelMockBuilder {

    private String code;

    /**
     * Creates a new instance of the {@link CategoryModelMockBuilder} class.
     */
    private CategoryModelMockBuilder() {
    }

    /**
     * The entry point to the {@link CategoryModelMockBuilder}.
     *
     * @return the {@link CategoryModelMockBuilder} instance for method chaining
     */
    public static CategoryModelMockBuilder aCategory() {
        return new CategoryModelMockBuilder();
    }

    /**
     * Sets the {@code code} on the current {@link CategoryModelMockBuilder} instance
     *
     * @param code the category's unique identifier
     * @return the {@link CategoryModelMockBuilder} instance for method chaining
     */
    public CategoryModelMockBuilder withCode(String code) {
        this.code = code;
        return this;
    }

    /**
     * Creates a mock implementation of the {@link CategoryModel} class.
     *
     * @return the {@link CategoryModel} instance
     */
    public CategoryModel build() {
        CategoryModel category = mock(CategoryModel.class);
        when(category.getCode()).thenReturn(code);
        return category;
    }
}
