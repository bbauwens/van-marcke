package com.vanmarcke.services.builder;

import de.hybris.platform.catalog.model.classification.ClassificationAttributeModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.StubLocaleProvider;
import de.hybris.platform.servicelayer.internal.model.impl.LocaleProvider;
import de.hybris.platform.servicelayer.model.ItemModelContextImpl;

import java.util.List;
import java.util.Locale;

public class ProductModelBuilder {

    private String name;
    private List<CategoryModel> superCategories;
    private String code;

    private ProductModelBuilder() {
    }

    public static ProductModelBuilder aProductModel() {
        return new ProductModelBuilder();
    }

    public ProductModelBuilder withName(String name) {
        this.name = name;
        return this;
    }

    public ProductModelBuilder withSuperCategories(List<CategoryModel> superCategories) {
        this.superCategories = superCategories;
        return this;
    }

    public ProductModelBuilder withCode(String code) {
        this.code = code;
        return this;
    }

    public ProductModel build() {
        ClassificationAttributeModel classificationAttribute = new ClassificationAttributeModel();
        LocaleProvider localeProvider = new StubLocaleProvider(Locale.ENGLISH);

        ItemModelContextImpl ctx = (ItemModelContextImpl) classificationAttribute.getItemModelContext();
        ctx.setLocaleProvider(localeProvider);
        ProductModel product = new ProductModel(ctx);

        product.setName(name);
        product.setSupercategories(superCategories);
        product.setCode(code);

        return product;
    }
}
