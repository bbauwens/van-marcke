package com.vanmarcke.services.model.builder;

import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.core.model.security.PrincipalGroupModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.UserModel;

import java.util.*;

/**
 * The {@link AbstractUserModelMockBuilder} class serves as starting point for other builders that are used to create
 * instances that extend the {@link UserModel} class.
 *
 * @param <S> the type of the builder class
 * @param <T> the type of the class to build
 * @author Christiaan Janssen
 * @since 23-10-2020
 */
abstract class AbstractUserModelMockBuilder<S, T extends UserModel> {

    protected final S myself;
    protected final Set<PrincipalGroupModel> groups;
    protected final List<AddressModel> addresses;

    protected String uid;
    protected String customerID;
    protected String name;
    protected Boolean active;
    protected CustomerType type;

    /**
     * Serves as starting point for classes that extend the {@link AbstractUserModelMockBuilder} class.
     *
     * @param type the class type
     */
    protected AbstractUserModelMockBuilder(Class<S> type) {
        myself = type.cast(this);
        groups = new HashSet<>();
        addresses = new ArrayList<>();
    }

    /**
     * Sets the {@code uid} on the current {@link S} instance.
     *
     * @param uid the user's unique identifier
     * @return the {@link S} instance for method chaining
     */
    public S withUID(String uid) {
        this.uid = uid;
        return myself;
    }

    /**
     * Sets the {@code customerID} on the current {@link S} instance.
     *
     * @param customerID the user's SAP customer ID
     * @return the {@link S} instance for method chaining
     */
    public S withCustomerID(String customerID) {
        this.customerID = customerID;
        return myself;
    }

    /**
     * Sets the {@code name} on the current {@link S} instance.
     *
     * @param name the user's first and last name
     * @return the {@link S} instance for method chaining
     */
    public S withName(String name) {
        this.name = name;
        return myself;
    }

    /**
     * Sets the {@code active} on the current {@link S} instance.
     *
     * @param active flag that indicates whether the use is active or not
     * @return the {@link S} instance for method chaining
     */
    public S withActive(boolean active) {
        this.active = active;
        return myself;
    }

    /**
     * Adds the given {@code groups} to the current {@link S} instance.
     *
     * @param groups the user groups
     * @return the {@link S} instance for method chaining
     */
    public S withGroups(PrincipalGroupModel... groups) {
        this.groups.addAll(Arrays.asList(groups));
        return myself;
    }

    /**
     * Adds the given {@code type} to the current {@link S} instance.
     *
     * @param type the customer type
     * @return the {@link S} instance for method chaining
     */
    public S withType(CustomerType type) {
        this.type = type;
        return myself;
    }

    /**
     * Adds the given {@code addresses} to the current {@link S} instance.
     *
     * @param addresses the addresses
     * @return the {@link S} instance for method chaining
     */
    public S withAddresses(AddressModel... addresses) {
        this.addresses.addAll(Arrays.asList(addresses));
        return myself;
    }
}
