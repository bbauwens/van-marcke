package com.vanmarcke.services.storelocator.pos.impl;

import com.vanmarcke.core.pos.VMKPointOfServiceDao;
import com.vanmarcke.cpi.data.order.AlternativeTECRequestData;
import com.vanmarcke.cpi.data.order.AlternativeTECResponseData;
import com.vanmarcke.cpi.services.ESBAlternativeTECService;
import com.vanmarcke.services.model.builder.CartModelMockBuilder;
import com.vanmarcke.services.model.builder.PointOfServiceModelMockBuilder;
import com.vanmarcke.services.storelocator.schedule.VMKOpeningScheduleService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.store.data.GeoPoint;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.*;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static java.util.Collections.singletonMap;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

/**
 * The {@link VMKPointOfServiceServiceImplTest} class contains the unit tests for the
 * {@link VMKPointOfServiceServiceImpl} class.
 *
 * @author Taki Korovessis, Niels Raemaekers, Christiaan Janssen
 * @since 02-10-2019
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKPointOfServiceServiceImplTest {

    private static final String TEC_UID = RandomStringUtils.randomAlphabetic(10);
    private static final boolean IS_OPEN = RandomUtils.nextBoolean();
    private static final Date FUTURE_DATE = DateTime.now().plusDays(3).toDate();
    private static final double LATITUDE = RandomUtils.nextDouble();
    private static final double LONGITUDE = RandomUtils.nextDouble();

    private CartModel cart;
    private PointOfServiceModel pointOfService1;
    private PointOfServiceModel pointOfService2;

    private AlternativeTECResponseData responseData;
    private AlternativeTECRequestData requestData;


    @Mock
    private SessionService sessionService;

    @Mock
    private VMKPointOfServiceDao pointOfServiceDao;

    @Mock
    private ESBAlternativeTECService esbAlternativeTECService;

    @Mock
    private Converter<Map<String, Long>, AlternativeTECRequestData> alternativeTECRequestDataConverter;

    @Mock
    private VMKOpeningScheduleService openingScheduleService;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();


    @Spy
    @InjectMocks
    private VMKPointOfServiceServiceImpl pointOfServiceService;

    @Before
    public void setUp() {
        pointOfService1 = PointOfServiceModelMockBuilder.aPointOfService().build();
        pointOfService2 = PointOfServiceModelMockBuilder.aPointOfService().build();
        cart = CartModelMockBuilder.aCart().build();
        responseData = new AlternativeTECResponseData();
        requestData = new AlternativeTECRequestData();
    }

    @Test
    public void testSetCurrentPointOfService() {
        pointOfServiceService.setCurrentPointOfService(pointOfService1);

        verify(sessionService).setAttribute("currentPointOfService", pointOfService1);
    }

    @Test
    public void testGetCurrentPointOfService() {
        when(sessionService.getAttribute("currentPointOfService")).thenReturn(pointOfService1);

        PointOfServiceModel result = pointOfServiceService.getCurrentPointOfService();
        assertThat(result).isEqualTo(pointOfService1);
    }

    @Test
    public void testGetPointOfServiceForName() {
        PointOfServiceModel pos = mock(PointOfServiceModel.class);
        doReturn(singletonList(pos))
                .when(pointOfServiceService)
                .getPointOfServicesForStoreIDs(singletonList(TEC_UID));

        assertThat(pointOfServiceService.getPointOfServiceForName(TEC_UID))
                .isEqualTo(pos);
    }

    @Test
    public void testGetPointOfServiceForName_null() {
        doReturn(emptyList())
                .when(pointOfServiceService)
                .getPointOfServicesForStoreIDs(singletonList(TEC_UID));

        assertThat(pointOfServiceService.getPointOfServiceForName(TEC_UID))
                .isNull();
    }

    @Test
    public void testGetPointOfServicesForIDs() {
        String alternativeTECid = "EA";
        when(pointOfServiceDao.getPointOfServicesForIDs(singletonList(alternativeTECid)))
                .thenReturn(singletonList(pointOfService1));
        List<PointOfServiceModel> result = pointOfServiceService.getPointOfServicesForStoreIDs(singletonList(alternativeTECid));

        verify(pointOfServiceDao).getPointOfServicesForIDs(singletonList(alternativeTECid));
        assertThat(result).isNotEmpty().contains(pointOfService1);
    }

    @Test
    public void testGetPointOfServicesForIDs_emptyList() {
        expectedException.expectMessage("No TEC store id is present.");
        expectedException.expect(IllegalArgumentException.class);
        pointOfServiceService.getPointOfServicesForStoreIDs(emptyList());
        verifyZeroInteractions(pointOfServiceDao);
    }

    @Test
    public void testGetAlternativeTECStoresWhichAreCurrentlyOpen() {
        List<PointOfServiceModel> alternativeStores = Arrays.asList(pointOfService1, pointOfService2);
        AbstractOrderEntryModel orderEntry = mock(AbstractOrderEntryModel.class);
        ProductModel product = mock(ProductModel.class);

        when(cart.getEntries()).thenReturn(singletonList(orderEntry));
        when(orderEntry.getProduct()).thenReturn(product);
        when(product.getCode()).thenReturn("1234");
        when(orderEntry.getQuantity()).thenReturn(1L);
        doReturn(alternativeStores).when(pointOfServiceService).getAlternativeTECStores(singletonMap("1234", 1L));
        when(openingScheduleService.isStoreOpenToday(pointOfService1)).thenReturn(false);
        when(openingScheduleService.isStoreOpenToday(pointOfService2)).thenReturn(true);

        List<PointOfServiceModel> result = pointOfServiceService.getAlternativeTECStoresWhichAreCurrentlyOpenForCart(cart);
        assertThat(result).containsExactly(pointOfService2);
        verify(openingScheduleService).isStoreOpenToday(pointOfService1);
        verify(openingScheduleService).isStoreOpenToday(pointOfService2);
    }

    @Test
    public void testGetAlternativeTECStores() {
        List<String> alternativeTECStoreCodes = Arrays.asList("EF", "GB", "JP");
        responseData.setAlternativeStoreIDs(alternativeTECStoreCodes);
        Map<String, Long> productQuantitiesMap = mock(Map.class);

        List<PointOfServiceModel> posList = new ArrayList<>();
        posList.add(pointOfService1);
        posList.add(null);
        posList.add(pointOfService2);

        when(esbAlternativeTECService.getAlternativeTECInformation(any(AlternativeTECRequestData.class))).thenReturn(responseData);
        when(alternativeTECRequestDataConverter.convert(productQuantitiesMap)).thenReturn(requestData);
        when(pointOfServiceDao.getPointOfServicesForIDs(alternativeTECStoreCodes)).thenReturn(posList);

        List<PointOfServiceModel> result = pointOfServiceService.getAlternativeTECStores(productQuantitiesMap);

        assertThat(result).isNotEmpty();
        assertThat(result).containsExactly(pointOfService1, pointOfService2);
        verify(esbAlternativeTECService).getAlternativeTECInformation(requestData);
        verify(alternativeTECRequestDataConverter).convert(productQuantitiesMap);
        verify(pointOfServiceDao).getPointOfServicesForIDs(alternativeTECStoreCodes);
    }

    @Test
    public void testGetAlternativeTECStores_noResults() {
        Map<String, Long> productQuantitiesMap = mock(Map.class);

        when(alternativeTECRequestDataConverter.convert(productQuantitiesMap)).thenReturn(requestData);
        when(this.esbAlternativeTECService.getAlternativeTECInformation(any(AlternativeTECRequestData.class)))
                .thenReturn(responseData);

        List<PointOfServiceModel> result = pointOfServiceService.getAlternativeTECStores(productQuantitiesMap);

        assertThat(result).isEmpty();
        verify(esbAlternativeTECService).getAlternativeTECInformation(requestData);
        verify(alternativeTECRequestDataConverter).convert(productQuantitiesMap);
        verifyZeroInteractions(pointOfServiceDao);
    }

    @Test
    public void testIsPointOfServiceOpenOnSaturday_closed() {
        when(openingScheduleService.isStoreOpenOnSaturday(pointOfService1)).thenReturn(false);

        assertThat(pointOfServiceService.isPointOfServiceOpenOnSaturday(pointOfService1)).isFalse();

        verify(openingScheduleService).isStoreOpenOnSaturday(pointOfService1);
    }

    @Test
    public void testIsPointOfServiceOpenOnSaturday_open() {
        when(openingScheduleService.isStoreOpenOnSaturday(pointOfService1)).thenReturn(true);

        assertThat(pointOfServiceService.isPointOfServiceOpenOnSaturday(pointOfService1)).isTrue();

        verify(openingScheduleService).isStoreOpenOnSaturday(pointOfService1);
    }

    @Test
    public void testIsPointOfServiceOpenOnGivenDate() {
        when(openingScheduleService.isStoreOpenOnGivenDate(pointOfService1, FUTURE_DATE)).thenReturn(IS_OPEN);

        assertThat(openingScheduleService.isStoreOpenOnGivenDate(pointOfService1, FUTURE_DATE))
                .isEqualTo(IS_OPEN);

        verify(openingScheduleService).isStoreOpenOnGivenDate(pointOfService1, FUTURE_DATE);
    }

    @Test
    public void getGeoPoint_latitudeNull() {
        when(pointOfService1.getLongitude()).thenReturn(LONGITUDE);

        GeoPoint result = pointOfServiceService.getGeoPoint(pointOfService1);
        assertThat(result.getLongitude()).isEqualTo(LONGITUDE);
        assertThat(result.getLatitude()).isEqualTo(0.0);
    }

    @Test
    public void getGeoPoint_longitudeNull() {
        when(pointOfService1.getLatitude()).thenReturn(LATITUDE);

        GeoPoint result = pointOfServiceService.getGeoPoint(pointOfService1);
        assertThat(result.getLongitude()).isEqualTo(0.0);
        assertThat(result.getLatitude()).isEqualTo(LATITUDE);
    }

    @Test
    public void getGeoPoint() {
        when(pointOfService1.getLongitude()).thenReturn(LONGITUDE);
        when(pointOfService1.getLatitude()).thenReturn(LATITUDE);

        GeoPoint result = pointOfServiceService.getGeoPoint(pointOfService1);
        assertThat(result.getLongitude()).isEqualTo(LONGITUDE);
        assertThat(result.getLatitude()).isEqualTo(LATITUDE);
    }
}
