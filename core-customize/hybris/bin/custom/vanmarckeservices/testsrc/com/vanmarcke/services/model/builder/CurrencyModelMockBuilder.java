package com.vanmarcke.services.model.builder;

import de.hybris.platform.core.model.c2l.CurrencyModel;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * The {@code CurrencyModelBuilder} class can be used to create a mock implementation of the
 * {@link CurrencyModel} class.
 *
 * @author Christiaan Janssen
 * @since 29-04-2020
 */
public class CurrencyModelMockBuilder {

    protected final String isoCode;

    /**
     * Creates a new instance of the {@link CurrencyModelMockBuilder} class.
     *
     * @param isoCode the ISO code
     */
    private CurrencyModelMockBuilder(String isoCode) {
        this.isoCode = isoCode;
    }

    /**
     * The entry point to the {@link CurrencyModelMockBuilder}.
     *
     * @param isoCode the ISO code
     * @return the {@link CurrencyModelMockBuilder} instance for method chaining
     */
    public static CurrencyModelMockBuilder aCurrency(String isoCode) {
        return new CurrencyModelMockBuilder(isoCode);
    }

    /**
     * Creates a mock implementation of the {@link CurrencyModel} class.
     *
     * @return the {@link CurrencyModel} instance
     */
    public CurrencyModel build() {
        CurrencyModel currency = mock(CurrencyModel.class);
        when(currency.getIsocode()).thenReturn(isoCode);
        return currency;
    }
}
