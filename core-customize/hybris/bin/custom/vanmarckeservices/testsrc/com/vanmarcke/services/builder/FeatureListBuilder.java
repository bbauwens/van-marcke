package com.vanmarcke.services.builder;

import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.classification.features.FeatureList;

import java.util.ArrayList;
import java.util.List;

public class FeatureListBuilder {

    private List<Feature> features = new ArrayList<>();

    /**
     * Private constructor to hide the implicit default one.
     */
    private FeatureListBuilder() {
    }

    /**
     * A new instance of the {@link FeatureListBuilder} is created and returned
     *
     * @return the created {@link FeatureListBuilder} instance
     */
    public static FeatureListBuilder aFeatureList() {
        return new FeatureListBuilder();
    }

    /**
     * Sets the given {@code code} on the current {@link FeatureList} instance.
     *
     * @param features the features
     * @return the current {@link FeatureListBuilder} instance for method chaining
     */
    public FeatureListBuilder withFeatures(List<Feature> features) {
        this.features = features;
        return this;
    }

    /**
     * Returns the current {@link FeatureList} instance
     *
     * @return the current {@link FeatureList} instance
     */
    public FeatureList build() {
        return new FeatureList(features);
    }
}
