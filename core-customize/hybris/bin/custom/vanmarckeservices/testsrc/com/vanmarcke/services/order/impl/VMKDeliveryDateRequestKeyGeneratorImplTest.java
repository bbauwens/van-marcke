package com.vanmarcke.services.order.impl;

import com.vanmarcke.services.model.builder.*;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.fest.assertions.Assertions;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.text.MessageFormat;
import java.util.Locale;

import static org.mockito.Mockito.when;

/**
 * The {@link VMKDeliveryDateRequestKeyGeneratorImplTest} class contains the unit tests for the
 * {@link VMKDeliveryDateRequestKeyGeneratorImpl} class.
 *
 * @author Christiaan Janssen
 * @since 08-07-2020
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKDeliveryDateRequestKeyGeneratorImplTest {

    private static final String POS_NAME = RandomStringUtils.randomAlphabetic(10);
    private static final String USER_UID = RandomStringUtils.randomAlphabetic(10);

    private static final String FIRST_PRODUCT_CODE = RandomStringUtils.randomAlphabetic(10);
    private static final Long FIRST_QUANTITY = RandomUtils.nextLong(0, 100);

    private static final String SECOND_PRODUCT_CODE = RandomStringUtils.randomAlphabetic(10);
    private static final Long SECOND_QUANTITY = RandomUtils.nextLong(0, 100);

    @Mock
    private UserService userService;

    @InjectMocks
    private VMKDeliveryDateRequestKeyGeneratorImpl generator;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void testGenerateWithCartAndDeliveryPos() {
        PointOfServiceModel pos = PointOfServiceModelMockBuilder
                .aPointOfService()
                .withName(POS_NAME)
                .build();

        UserModel user = UserModelMockBuilder
                .aUser()
                .withUID(USER_UID)
                .build();

        ProductModel firstProduct = ProductModelMockBuilder
                .aBaseProduct(FIRST_PRODUCT_CODE, Locale.US)
                .build();

        CartEntryModel firstEntry = CartEntryModelMockBuilder
                .anEntry()
                .withQuantity(FIRST_QUANTITY)
                .withProduct(firstProduct)
                .build();

        ProductModel secondProduct = ProductModelMockBuilder
                .aBaseProduct(SECOND_PRODUCT_CODE, Locale.US)
                .build();

        CartEntryModel secondEntry = CartEntryModelMockBuilder
                .anEntry()
                .withQuantity(SECOND_QUANTITY)
                .withProduct(secondProduct)
                .build();

        CartModel cart = CartModelMockBuilder
                .aCart()
                .withDeliveryPointOfService(pos)
                .withUser(user)
                .withEntries(firstEntry, secondEntry)
                .build();

        String actualKey = (String) generator.generate(null, null, cart);

        Assertions
                .assertThat(actualKey)
                .isEqualTo(MessageFormat.format("{0}-{1}:{2};{3}:{4};-{5}",
                        USER_UID,
                        FIRST_PRODUCT_CODE,
                        FIRST_QUANTITY,
                        SECOND_PRODUCT_CODE,
                        SECOND_QUANTITY,
                        POS_NAME));
    }

    @Test
    public void testGenerateWithCartWithoutDeliveryPos() {
        PointOfServiceModel pos = PointOfServiceModelMockBuilder
                .aPointOfService()
                .withName(POS_NAME)
                .build();

        UserModel user = UserModelMockBuilder
                .aUser()
                .withUID(USER_UID)
                .withSessionStore(pos)
                .build();

        when(userService.getCurrentUser()).thenReturn(user);

        ProductModel firstProduct = ProductModelMockBuilder
                .aBaseProduct(FIRST_PRODUCT_CODE, Locale.US)
                .build();

        CartEntryModel firstEntry = CartEntryModelMockBuilder
                .anEntry()
                .withQuantity(FIRST_QUANTITY)
                .withProduct(firstProduct)
                .build();

        ProductModel secondProduct = ProductModelMockBuilder
                .aBaseProduct(SECOND_PRODUCT_CODE, Locale.US)
                .build();

        CartEntryModel secondEntry = CartEntryModelMockBuilder
                .anEntry()
                .withQuantity(SECOND_QUANTITY)
                .withProduct(secondProduct)
                .build();

        CartModel cart = CartModelMockBuilder
                .aCart()
                .withUser(user)
                .withEntries(firstEntry, secondEntry)
                .build();

        String actualKey = (String) generator.generate(null, null, cart);

        Assertions
                .assertThat(actualKey)
                .isEqualTo(MessageFormat.format("{0}-{1}:{2};{3}:{4};-{5}",
                        USER_UID,
                        FIRST_PRODUCT_CODE,
                        FIRST_QUANTITY,
                        SECOND_PRODUCT_CODE,
                        SECOND_QUANTITY,
                        POS_NAME));
    }

    @Test
    public void testGenerateWithIllegalArgument() {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Parameter must be of type de.hybris.platform.core.model.order.CartModel");

        generator.generate(null, null, RandomStringUtils.random(10));
    }
}