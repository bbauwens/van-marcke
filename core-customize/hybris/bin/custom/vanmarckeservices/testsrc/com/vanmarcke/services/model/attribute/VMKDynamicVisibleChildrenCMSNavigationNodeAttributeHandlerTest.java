package com.vanmarcke.services.model.attribute;

import com.vanmarcke.services.strategies.VMKNavigationBuilderStrategy;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.cms2.model.navigation.CMSNavigationNodeModel;
import de.hybris.platform.site.BaseSiteService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static java.util.Collections.singletonList;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKDynamicVisibleChildrenCMSNavigationNodeAttributeHandlerTest {

    @Mock
    private BaseSiteService baseSiteService;

    @Mock
    private VMKNavigationBuilderStrategy navigationBuilderStrategy;

    @InjectMocks
    public VMKDynamicVisibleChildrenCMSNavigationNodeAttributeHandler handler;

    @Test
    public void testGet_withoutCurrentBaseSite() {
        CMSNavigationNodeModel navigationNode = mock(CMSNavigationNodeModel.class);
        CMSNavigationNodeModel childNode = mock(CMSNavigationNodeModel.class);

        when(navigationNode.getChildren()).thenReturn(singletonList(childNode));

        List<CMSNavigationNodeModel> result = handler.get(navigationNode);

        assertThat(result).containsExactly(childNode);

        verifyZeroInteractions(navigationBuilderStrategy);
    }

    @Test
    public void testGet_withCurrentBaseSite() {
        BaseSiteModel baseSiteModel = mock(BaseSiteModel.class);
        CMSNavigationNodeModel parentNode = mock(CMSNavigationNodeModel.class);
        CMSNavigationNodeModel childNode = mock(CMSNavigationNodeModel.class);

        when(baseSiteService.getCurrentBaseSite()).thenReturn(baseSiteModel);

        when(navigationBuilderStrategy.buildNavigation(baseSiteModel, parentNode)).thenReturn(singletonList(childNode));

        List<CMSNavigationNodeModel> result = handler.get(parentNode);
        assertThat(result).containsExactly(childNode);
    }

    @Test
    public void testSet() {
        CMSNavigationNodeModel model = mock(CMSNavigationNodeModel.class);
        List<CMSNavigationNodeModel> models = new ArrayList<>();

        handler.set(model, models);

        verify(model).setChildren(models);
    }
}