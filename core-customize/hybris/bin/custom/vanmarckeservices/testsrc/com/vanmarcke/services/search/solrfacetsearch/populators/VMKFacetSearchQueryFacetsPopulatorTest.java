package com.vanmarcke.services.search.solrfacetsearch.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfig;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import de.hybris.platform.solrfacetsearch.search.impl.SearchQueryConverterData;
import org.apache.commons.lang3.RandomUtils;
import org.apache.solr.client.solrj.SolrQuery;
import org.fest.assertions.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

/**
 * The {@code VMKFacetSearchQueryFacetsPopulatorTest} class contains the unit tests for the
 * {@link VMKFacetSearchQueryFacetsPopulator} class.
 *
 * @author Christiaan Janssen
 * @since 02-04-2020
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKFacetSearchQueryFacetsPopulatorTest {

    private static final Integer FACET_LIMIT = RandomUtils.nextInt();

    @InjectMocks
    private VMKFacetSearchQueryFacetsPopulator facetSearchQueryFacetsPopulator = new VMKFacetSearchQueryFacetsPopulator(FACET_LIMIT);

    @Test
    public void testPopulate() {
        FacetSearchConfig cfg = new FacetSearchConfig();

        SearchQuery searchQuery = new SearchQuery(cfg, null);

        SearchQueryConverterData source = new SearchQueryConverterData();
        source.setSearchQuery(searchQuery);

        SolrQuery target = new SolrQuery();

        facetSearchQueryFacetsPopulator.populate(source, target);

        Assertions
                .assertThat(target.getFacetLimit())
                .isEqualTo(FACET_LIMIT);
    }
}