package com.vanmarcke.services.search.solrfacetsearch.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeUnitModel;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.model.config.SolrIndexedPropertyModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKCommerceIndexedPropertyPopulatorTest {

    private VMKCommerceIndexedPropertyPopulator vmkCommerceIndexedPropertyPopulator = new VMKCommerceIndexedPropertyPopulator();

    @Test
    public void testUnitSet() {
        // given
        SolrIndexedPropertyModel solrIndexedProperty = mock(SolrIndexedPropertyModel.class);
        ClassificationAttributeUnitModel unitModel = mock(ClassificationAttributeUnitModel.class);
        IndexedProperty indexedProperty = mock(IndexedProperty.class);
        when(solrIndexedProperty.getUnit()).thenReturn(unitModel);
        when(unitModel.getSymbol()).thenReturn("m");

        // when
        vmkCommerceIndexedPropertyPopulator.populate(solrIndexedProperty, indexedProperty);

        // then
        verify(indexedProperty).setUnit("m");
    }
}