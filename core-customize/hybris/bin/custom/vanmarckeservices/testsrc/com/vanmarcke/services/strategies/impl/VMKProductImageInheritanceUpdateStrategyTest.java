package com.vanmarcke.services.strategies.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.variants.model.VariantProductModel;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
@UnitTest
public class VMKProductImageInheritanceUpdateStrategyTest {

    @Mock
    private ModelService modelService;

    @InjectMocks
    private VMKProductImageInheritanceUpdateStrategyImpl defaultProductImageInheritanceUpdateStrategy;

    @Mock
    private ProductModel product;
    @Mock
    private VariantProductModel variantProduct;

    @Before
    public void setUp() throws Exception {
        product = mock(ProductModel.class);
        when(product.getCode()).thenReturn("productcode");

        variantProduct = mock(VariantProductModel.class);
        when(variantProduct.getCode()).thenReturn("variantcode");
        when(variantProduct.getBaseProduct()).thenReturn(product);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testUpdateProductImageInheritanceForProduct_withoutVariantProduct() throws Exception {
        defaultProductImageInheritanceUpdateStrategy.updateProductImageInheritanceForProduct(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testUpdateProductImageInheritanceForProduct_withoutBaseProduct() throws Exception {
        VariantProductModel variantProduct = mock(VariantProductModel.class);
        defaultProductImageInheritanceUpdateStrategy.updateProductImageInheritanceForProduct(variantProduct);
    }

    @Test
    public void testUpdateProductImageInheritanceForProduct_withoutImageInheritanceFromBaseProduct() throws Exception {
        defaultProductImageInheritanceUpdateStrategy.updateProductImageInheritanceForProduct(variantProduct);

        verify(variantProduct).setPicture(null);
        verify(variantProduct).setThumbnail(null);
        verify(variantProduct).setGalleryImages(emptyList());

        verify(modelService).save(variantProduct);
    }

    @Test
    public void testUpdateProductImageInheritanceForProduct_withImageInheritanceFromBaseProduct() throws Exception {
        MediaModel previewMedia = mock(MediaModel.class);
        when(previewMedia.getCode()).thenReturn("productcode-PREVIEW");
        MediaModel thumbnailMedia = mock(MediaModel.class);
        when(thumbnailMedia.getCode()).thenReturn("productcode-THUMBNAIL");
        MediaContainerModel galleryMediaContainer = mock(MediaContainerModel.class);
        when(galleryMediaContainer.getQualifier()).thenReturn("productcode_0");
        List<MediaContainerModel> galleryMediaContainers = singletonList(galleryMediaContainer);

        when(product.getPicture()).thenReturn(previewMedia);
        when(product.getThumbnail()).thenReturn(thumbnailMedia);
        when(product.getGalleryImages()).thenReturn(galleryMediaContainers);

        defaultProductImageInheritanceUpdateStrategy.updateProductImageInheritanceForProduct(variantProduct);

        verify(variantProduct).setPicture(previewMedia);
        verify(variantProduct).setThumbnail(thumbnailMedia);
        verify(variantProduct).setGalleryImages(galleryMediaContainers);

        verify(modelService).save(variantProduct);
    }

    @Test
    public void testUpdateProductImageInheritanceForProduct_withRemoveImageInheritanceFromBaseProduct() throws Exception {
        MediaModel previewMedia = mock(MediaModel.class);
        when(previewMedia.getCode()).thenReturn("productcode-PREVIEW");
        MediaModel thumbnailMedia = mock(MediaModel.class);
        when(thumbnailMedia.getCode()).thenReturn("productcode-THUMBNAIL");
        MediaContainerModel galleryMediaContainer = mock(MediaContainerModel.class);
        when(galleryMediaContainer.getQualifier()).thenReturn("productcode_0");
        List<MediaContainerModel> galleryMediaContainers = singletonList(galleryMediaContainer);

        when(product.getPicture()).thenReturn(previewMedia);
        when(product.getThumbnail()).thenReturn(thumbnailMedia);
        when(product.getGalleryImages()).thenReturn(galleryMediaContainers);

        MediaModel previewVariantMedia = mock(MediaModel.class);
        when(previewVariantMedia.getCode()).thenReturn("variantcode-PREVIEW");
        MediaModel thumbnailvariantMedia = mock(MediaModel.class);
        when(thumbnailvariantMedia.getCode()).thenReturn("variantcode-THUMBNAIL");
        MediaContainerModel galleryVariantMediaContainer = mock(MediaContainerModel.class);
        when(galleryVariantMediaContainer.getQualifier()).thenReturn("variantcode_0");
        List<MediaContainerModel> galleryVariantMediaContainers = asList(galleryMediaContainer, galleryVariantMediaContainer);

        when(variantProduct.getPicture()).thenReturn(previewVariantMedia);
        when(variantProduct.getThumbnail()).thenReturn(thumbnailvariantMedia);
        when(variantProduct.getGalleryImages()).thenReturn(galleryVariantMediaContainers);

        defaultProductImageInheritanceUpdateStrategy.updateProductImageInheritanceForProduct(variantProduct);

        verify(variantProduct, never()).setPicture(any());
        verify(variantProduct, never()).setThumbnail(any());
        verify(variantProduct).setGalleryImages(singletonList(galleryVariantMediaContainer));

        verify(modelService).save(variantProduct);
    }

    @Test
    public void testUpdateProductImageInheritanceForProduct_withRemoveImageInheritanceFromBaseProductAndWithPrefix() throws Exception {
        MediaModel previewMedia = mock(MediaModel.class);
        when(previewMedia.getCode()).thenReturn("productcode-PREVIEW");
        MediaModel thumbnailMedia = mock(MediaModel.class);
        when(thumbnailMedia.getCode()).thenReturn("productcode-THUMBNAIL");
        MediaContainerModel galleryMediaContainer = mock(MediaContainerModel.class);
        when(galleryMediaContainer.getQualifier()).thenReturn("productcode_0");
        List<MediaContainerModel> galleryMediaContainers = singletonList(galleryMediaContainer);

        when(product.getPicture()).thenReturn(previewMedia);
        when(product.getThumbnail()).thenReturn(thumbnailMedia);
        when(product.getGalleryImages()).thenReturn(galleryMediaContainers);

        MediaModel previewVariantMedia = mock(MediaModel.class);
        when(previewVariantMedia.getCode()).thenReturn("v-code_0-PREVIEW");
        MediaModel thumbnailvariantMedia = mock(MediaModel.class);
        when(thumbnailvariantMedia.getCode()).thenReturn("v-code_0-THUMBNAIL");
        MediaContainerModel galleryVariantMediaContainer = mock(MediaContainerModel.class);
        when(galleryVariantMediaContainer.getQualifier()).thenReturn("v-code_0");
        List<MediaContainerModel> galleryVariantMediaContainers = asList(galleryMediaContainer, galleryVariantMediaContainer);

        when(variantProduct.getPicture()).thenReturn(previewVariantMedia);
        when(variantProduct.getThumbnail()).thenReturn(thumbnailvariantMedia);
        when(variantProduct.getGalleryImages()).thenReturn(galleryVariantMediaContainers);

        defaultProductImageInheritanceUpdateStrategy.updateProductImageInheritanceForProduct(variantProduct);

        verify(variantProduct, never()).setPicture(any());
        verify(variantProduct, never()).setThumbnail(any());
        verify(variantProduct).setGalleryImages(singletonList(galleryVariantMediaContainer));

        verify(modelService).save(variantProduct);
    }

    @Test
    public void testUpdateProductImageInheritanceForProduct_withIgnoreImageInheritanceFromBaseProduct() throws Exception {
        MediaModel previewMedia = mock(MediaModel.class);
        when(previewMedia.getCode()).thenReturn("productcode-PREVIEW");
        MediaModel thumbnailMedia = mock(MediaModel.class);
        when(thumbnailMedia.getCode()).thenReturn("productcode-THUMBNAIL");
        MediaContainerModel galleryMediaContainer = mock(MediaContainerModel.class);
        when(galleryMediaContainer.getQualifier()).thenReturn("productcode_0");
        List<MediaContainerModel> galleryMediaContainers = singletonList(galleryMediaContainer);

        when(product.getPicture()).thenReturn(previewMedia);
        when(product.getThumbnail()).thenReturn(thumbnailMedia);
        when(product.getGalleryImages()).thenReturn(galleryMediaContainers);

        MediaModel previewVariantMedia = mock(MediaModel.class);
        when(previewVariantMedia.getCode()).thenReturn("variantcode-PREVIEW");
        MediaModel thumbnailvariantMedia = mock(MediaModel.class);
        when(thumbnailvariantMedia.getCode()).thenReturn("variantcode-THUMBNAIL");
        MediaContainerModel galleryVariantMediaContainer = mock(MediaContainerModel.class);
        when(galleryVariantMediaContainer.getQualifier()).thenReturn("variantcode_0");
        List<MediaContainerModel> galleryVariantMediaContainers = asList(galleryVariantMediaContainer);

        when(variantProduct.getPicture()).thenReturn(previewVariantMedia);
        when(variantProduct.getThumbnail()).thenReturn(thumbnailvariantMedia);
        when(variantProduct.getGalleryImages()).thenReturn(galleryVariantMediaContainers);

        defaultProductImageInheritanceUpdateStrategy.updateProductImageInheritanceForProduct(variantProduct);

        verify(variantProduct, never()).setPicture(any());
        verify(variantProduct, never()).setThumbnail(any());
        verify(variantProduct).setGalleryImages(singletonList(galleryVariantMediaContainer));

        verify(modelService).save(variantProduct);
    }

    @Test
    public void testUpdateProductImageInheritanceForProduct_withChangedImageInheritanceForBaseProduct() throws Exception {
        MediaModel previewMedia = mock(MediaModel.class);
        when(previewMedia.getCode()).thenReturn("productcode-PREVIEW");
        MediaModel thumbnailMedia = mock(MediaModel.class);
        when(thumbnailMedia.getCode()).thenReturn("productcode-THUMBNIAL");
        MediaContainerModel galleryMediaContainer0 = mock(MediaContainerModel.class);
        when(galleryMediaContainer0.getQualifier()).thenReturn("productcode_0");
        MediaContainerModel galleryMediaContainer2 = mock(MediaContainerModel.class);
        when(galleryMediaContainer0.getQualifier()).thenReturn("productcode_2");
        List<MediaContainerModel> galleryMediaContainers = asList(galleryMediaContainer0, galleryMediaContainer2);

        when(product.getPicture()).thenReturn(previewMedia);
        when(product.getThumbnail()).thenReturn(thumbnailMedia);
        when(product.getGalleryImages()).thenReturn(galleryMediaContainers);

        MediaContainerModel galleryVariantMediaContainer1 = mock(MediaContainerModel.class);
        when(galleryVariantMediaContainer1.getQualifier()).thenReturn("productcode_1");
        List<MediaContainerModel> galleryVariantMediaContainers = asList(galleryMediaContainer0, galleryVariantMediaContainer1);

        when(variantProduct.getPicture()).thenReturn(previewMedia);
        when(variantProduct.getThumbnail()).thenReturn(thumbnailMedia);
        when(variantProduct.getGalleryImages()).thenReturn(galleryVariantMediaContainers);

        defaultProductImageInheritanceUpdateStrategy.updateProductImageInheritanceForProduct(variantProduct);

        verify(variantProduct).setPicture(previewMedia);
        verify(variantProduct).setThumbnail(thumbnailMedia);
        verify(variantProduct).setGalleryImages(asList(galleryMediaContainer0, galleryMediaContainer2));

        verify(modelService).save(variantProduct);
    }
}