package com.vanmarcke.services.order.cart.conversion.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.util.CSVReader;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.poi.ss.usermodel.*;
import org.fest.assertions.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.StringReader;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;

import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKCsvExcelConversionServiceImplTest {

    private static final String STRING_VALUE = RandomStringUtils.randomAlphabetic(10);
    private static final double NUMERIC_VALUE = new Random().nextDouble();

    @Spy
    @InjectMocks
    private VMKCsvExcelConversionServiceImpl conversionService;

    @Test
    public void testConvertCsvToXls() {
        ArgumentCaptor<Sheet> sheetArgumentCaptor = ArgumentCaptor.forClass(Sheet.class);

        String csv = RandomStringUtils.randomAlphabetic(10);
        StringReader stringReader = new StringReader(csv);

        CSVReader csvReader = mock(CSVReader.class);
        doReturn(csvReader).when(conversionService).getCSVReader(stringReader);

        byte[] result = conversionService.convertCsvToXls(stringReader);

        verify(conversionService).processCsvRows(eq(stringReader), sheetArgumentCaptor.capture());

        Assertions
                .assertThat(sheetArgumentCaptor.getValue().getSheetName())
                .isEqualTo("cart");

        Assertions
                .assertThat(result)
                .isNotEmpty();
    }

    @Test
    public void testConvertCsvToXlsWithCurrency() {
        ArgumentCaptor<Sheet> sheetArgumentCaptor = ArgumentCaptor.forClass(Sheet.class);

        String csv = RandomStringUtils.randomAlphabetic(10);
        StringReader stringReader = new StringReader(csv);

        CSVReader csvReader = mock(CSVReader.class);
        doReturn(csvReader).when(conversionService).getCSVReader(stringReader);
        int[] currencyColumns = new int[] {};
        byte[] result = conversionService.convertCsvToXls(stringReader, currencyColumns);

        verify(conversionService).processCsvRows(eq(stringReader), sheetArgumentCaptor.capture());
        verify(conversionService).processCurrencyCell(eq(currencyColumns), any(Workbook.class), sheetArgumentCaptor.capture());

        Assertions
                .assertThat(sheetArgumentCaptor.getValue().getSheetName())
                .isEqualTo("cart");

        Assertions
                .assertThat(result)
                .isNotEmpty();
    }
    @Test
    public void testProcessCurrencyCells() {
        Sheet sheet = mock(Sheet.class);
        Row row = mock(Row.class);
        Cell cell = mock(Cell.class);
        Workbook workbook = mock(Workbook.class);
        Iterator<Row> rowIterator = mock(Iterator.class);
        CellStyle cs = mock(CellStyle.class);
        DataFormat df = mock(DataFormat.class);
        when(workbook.createCellStyle()).thenReturn(cs);
        when(workbook.createDataFormat()).thenReturn(df);
        when(df.getFormat(anyString())).thenReturn((short)1);

        when(sheet.createRow(0)).thenReturn(row);
        when(sheet.createRow(1)).thenReturn(row);
        when(sheet.iterator()).thenReturn(rowIterator);
        when(rowIterator.hasNext()).thenReturn(Boolean.TRUE, Boolean.TRUE, Boolean.FALSE);
        when(rowIterator.next()).thenReturn(row);
        when(row.getCell(0)).thenReturn(cell);
        when(cell.getStringCellValue()).thenReturn(Double.toString(new Random().nextDouble()));

        conversionService.processCurrencyCell(new int[]{0}, workbook, sheet);

        ArgumentCaptor<CellStyle> cellStyleArgumentCaptor = ArgumentCaptor.forClass(CellStyle.class);

        verify(cell, times(1)).setCellValue(anyDouble());
        verify(cell, times(1)).setCellStyle(cellStyleArgumentCaptor.capture());
        Assert.assertEquals(cellStyleArgumentCaptor.getValue(), cs);
    }


    @Test
    public void testProcessCsvRowsNoNextLine() {
        String csv = RandomStringUtils.randomAlphabetic(10);
        StringReader stringReader = new StringReader(csv);
        Sheet sheet = mock(Sheet.class);
        CSVReader csvReader = mock(CSVReader.class);

        doReturn(csvReader).when(conversionService).getCSVReader(stringReader);
        doReturn(false).when(csvReader).readNextLine();

        conversionService.processCsvRows(stringReader, sheet);

        verify(conversionService, times(0)).processCsvRow(sheet, csvReader, 1);
    }

    @Test
    public void testProcessCsvRow() {
        Sheet sheet = mock(Sheet.class);
        CSVReader csvReader = mock(CSVReader.class);
        Row row = mock(Row.class);
        Cell cell = mock(Cell.class);
        Map line = mock(Map.class);

        when(sheet.createRow(1)).thenReturn(row);
        when(csvReader.getLine()).thenReturn(line);
        when(line.size()).thenReturn(1);
        when(row.createCell(0)).thenReturn(cell);
        when(line.get(0)).thenReturn(STRING_VALUE);

        conversionService.processCsvRow(sheet, csvReader, 1);

        verify(sheet).createRow(1);
        verify(csvReader).getLine();
        verify(row).createCell(0);
        verify(cell).setCellValue(STRING_VALUE);
        verify(line).get(0);
    }


    @Test
    public void testProcessRows() {
        Workbook workbook = mock(Workbook.class);
        Sheet sheet = mock(Sheet.class);
        Row row = mock(Row.class);
        Iterator<Cell> cellIterator = mock(Iterator.class);
        Iterator<Row> rowIterator = mock(Iterator.class);
        StringBuilder stringBuilder = new StringBuilder();

        when(workbook.getSheetAt(0)).thenReturn(sheet);
        when(sheet.iterator()).thenReturn(rowIterator);
        when(rowIterator.hasNext()).thenReturn(Boolean.TRUE, Boolean.FALSE);
        when(rowIterator.next()).thenReturn(row);
        when(row.cellIterator()).thenReturn(cellIterator);

        conversionService.processRows(stringBuilder, workbook);

        verify(conversionService).processCells(stringBuilder, cellIterator);

        Assertions
                .assertThat(stringBuilder.toString())
                .contains("\n");
    }

    @Test
    public void testProcessCells() {
        StringBuilder stringBuilder = new StringBuilder();
        Iterator<Cell> cellIterator = mock(Iterator.class);
        Cell cell = mock(Cell.class);

        when(cellIterator.hasNext()).thenReturn(Boolean.TRUE, Boolean.FALSE);
        when(cellIterator.next()).thenReturn(cell);

        conversionService.processCells(stringBuilder, cellIterator);

        verify(conversionService).processCell(stringBuilder, cell);
    }

    @Test
    public void testProcessCellTypeBoolean() {
        StringBuilder stringBuilder = new StringBuilder();
        Cell cell = mock(Cell.class);

        when(cell.getCellTypeEnum()).thenReturn(CellType.BOOLEAN);
        when(cell.getBooleanCellValue()).thenReturn(Boolean.TRUE);

        conversionService.processCell(stringBuilder, cell);

        Assertions
                .assertThat(stringBuilder.toString())
                .isEqualTo("true;");
    }

    @Test
    public void testProcessCellTypeNumeric() {
        StringBuilder stringBuilder = new StringBuilder();
        Cell cell = mock(Cell.class);

        when(cell.getCellTypeEnum()).thenReturn(CellType.NUMERIC);
        when(cell.getNumericCellValue()).thenReturn(NUMERIC_VALUE);

        conversionService.processCell(stringBuilder, cell);

        Assertions
                .assertThat(stringBuilder.toString())
                .isEqualTo((int) NUMERIC_VALUE + ";");
    }

    @Test
    public void testProcessCellTypeString() {
        StringBuilder stringBuilder = new StringBuilder();
        Cell cell = mock(Cell.class);

        when(cell.getCellTypeEnum()).thenReturn(CellType.STRING);
        when(cell.getStringCellValue()).thenReturn(STRING_VALUE);

        conversionService.processCell(stringBuilder, cell);

        Assertions
                .assertThat(stringBuilder.toString())
                .isEqualTo(STRING_VALUE + ";");
    }

    @Test
    public void testProcessCellTypeBlank() {
        StringBuilder stringBuilder = new StringBuilder();
        Cell cell = mock(Cell.class);

        when(cell.getCellTypeEnum()).thenReturn(CellType.BLANK);

        conversionService.processCell(stringBuilder, cell);

        Assertions
                .assertThat(stringBuilder.toString())
                .isEqualTo(";");
    }
}