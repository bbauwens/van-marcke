package com.vanmarcke.services.model.builder;

import de.hybris.platform.storelocator.model.OpeningDayModel;

import java.util.Date;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * The {@code OpeningDayModelMockBuilder} class can be used to create a mock implementation of the
 * {@link OpeningDayModel} class.
 *
 * @author Christiaan Janssen
 * @since 07-07-2020
 */
public class OpeningDayModelMockBuilder<S extends OpeningDayModelMockBuilder, T extends OpeningDayModel> {

    protected final S myself;

    protected Date openingTime;
    protected Date closingTime;

    /**
     * Creates a new instance of the {@link OpeningDayModelMockBuilder} class.
     */
    protected OpeningDayModelMockBuilder(Class<S> type) {
        myself = type.cast(this);
    }

    /**
     * Sets the {@code openingTime} on the current {@link OpeningDayModelMockBuilder} instance
     *
     * @param openingTime the opening time
     * @return the {@link OpeningDayModelMockBuilder} instance for method chaining
     */
    public S withOpeningTime(Date openingTime) {
        this.openingTime = openingTime;
        return myself;
    }

    /**
     * Sets the {@code closingTime} on the current {@link OpeningDayModelMockBuilder} instance
     *
     * @param closingTime the closing time
     * @return the {@link OpeningDayModelMockBuilder} instance for method chaining
     */
    public S withClosingTime(Date closingTime) {
        this.closingTime = closingTime;
        return myself;
    }

    /**
     * Creates a mock implementation of the {@link OpeningDayModel} class.
     *
     * @return the {@link OpeningDayModel} instance
     */
    public OpeningDayModel build() {
        OpeningDayModel openingDay = mock(OpeningDayModel.class);
        when(openingDay.getOpeningTime()).thenReturn(openingTime);
        when(openingDay.getClosingTime()).thenReturn(closingTime);
        return openingDay;
    }
}
