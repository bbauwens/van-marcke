package com.vanmarcke.services.search.solrfacetsearch.provider.impl;

import org.fest.assertions.CollectionAssert;

import java.util.Arrays;
import java.util.Collection;
import java.util.function.BiPredicate;

/**
 * Allows a BiPredicate<T, T> as inputparameter to serve as an override for the equals method.
 * Very handy to test object equality for objects that don't have a custom implementation of .equals, such as de.hybris.platform.solrfacetsearch.provider.FieldValue
 *
 * @param <T> The type of the Collection<T>.
 */
public class VMKCollectionAssert<T> extends CollectionAssert {

    private final BiPredicate<T, T> equalsPredicate;

    public static <T> VMKCollectionAssert<T> assertThat(Collection<?> actual, BiPredicate<T, T> equalsPredicate) {
        return new VMKCollectionAssert<>(actual, equalsPredicate);
    }

    public VMKCollectionAssert(Collection actual, BiPredicate<T, T> equalsPredicate) {
        super(actual);
        this.equalsPredicate = equalsPredicate;
    }

    public VMKCollectionAssert<T> containsAll(T... expectedValues) {
        Arrays.stream(expectedValues).forEach(this::contains);
        return this;
    }

    public VMKCollectionAssert<T> contains(T expectedValue) {
        if (this.actual.stream().noneMatch(x -> equalsPredicate.test((T) x, expectedValue))) {
            throw new AssertionError(String.format("%s was not found", expectedValue));
        }
        return this;
    }

}
