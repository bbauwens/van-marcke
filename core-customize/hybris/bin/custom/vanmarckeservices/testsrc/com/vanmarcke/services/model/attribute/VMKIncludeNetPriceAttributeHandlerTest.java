package com.vanmarcke.services.model.attribute;

import com.vanmarcke.services.constants.VanmarckeservicesConstants;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.session.SessionService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKIncludeNetPriceAttributeHandlerTest {

    @Mock
    SessionService sessionService;

    @InjectMocks
    VMKIncludeNetPriceAttributeHandler vmkIncludeNetPriceAttributeHandler;

    @Test
    public void testGet_enabledOnSession() {
        when(sessionService.getAttribute(VanmarckeservicesConstants.IS_EXPORT_NET_PRICE_ATTRIBUTE)).thenReturn(true);
        Assert.assertTrue(vmkIncludeNetPriceAttributeHandler.get(mock(AbstractOrderModel.class)));
    }

    @Test
    public void testGet_notOnSession() {
        when(sessionService.getAttribute(VanmarckeservicesConstants.IS_EXPORT_NET_PRICE_ATTRIBUTE)).thenReturn(null);
        Assert.assertFalse(vmkIncludeNetPriceAttributeHandler.get(mock(AbstractOrderModel.class)));
    }
}