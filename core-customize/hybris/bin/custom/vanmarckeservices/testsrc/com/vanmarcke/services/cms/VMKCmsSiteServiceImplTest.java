package com.vanmarcke.services.cms;

import com.vanmarcke.core.cms.VMKCMSSiteDao;
import com.vanmarcke.services.cms.impl.VMKCmsSiteServiceImpl;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKCmsSiteServiceImplTest {

    @Mock
    private VMKCMSSiteDao cmsSiteDao;

    @InjectMocks
    private VMKCmsSiteServiceImpl vmkCMSSiteService;

    @Test
    public void testGetAllActiveCMSSitesForChannel() throws Exception {
        CMSSiteModel cmsSite1 = mock(CMSSiteModel.class);
        CMSSiteModel cmsSite2 = mock(CMSSiteModel.class);
        CMSSiteModel cmsSite3 = mock(CMSSiteModel.class);
        CMSSiteModel cmsSite4 = mock(CMSSiteModel.class);

        when(cmsSite1.getActive()).thenReturn(true);
        when(cmsSite1.getEcommerceEnabled()).thenReturn(false);

        when(cmsSite2.getActive()).thenReturn(true);
        when(cmsSite2.getEcommerceEnabled()).thenReturn(true);

        when(cmsSite3.getActive()).thenReturn(false);
        when(cmsSite3.getEcommerceEnabled()).thenReturn(true);

        when(cmsSite4.getActive()).thenReturn(false);
        when(cmsSite4.getEcommerceEnabled()).thenReturn(null);

        when(cmsSiteDao.findAllSitesForChannel(SiteChannel.B2B)).thenReturn(Arrays.asList(cmsSite1, cmsSite2, cmsSite3, cmsSite4));

        List<CMSSiteModel> result = vmkCMSSiteService.getSupportedSites(SiteChannel.B2B);
        assertThat(result).containsExactly(cmsSite2);
    }

}