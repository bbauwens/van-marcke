package com.vanmarcke.services.address.impl;

import com.vanmarcke.services.address.VMKPaymentAddressesLookupStrategy;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKPreferredPaymentAddressLookupStrategyTest {

    @Mock
    private B2BUnitModel b2BUnit;
    @Mock
    private VMKPaymentAddressesLookupStrategy bluePaymentAddressesLookupStrategy;

    @InjectMocks
    private VMKPreferredPaymentAddressLookupStrategyImpl vmkPreferredPaymentAddressLookupStrategy;

    private AbstractOrderModel abstractOrder;

    @Before
    public void setUp() {
        this.abstractOrder = mock(AbstractOrderModel.class);
    }

    @Test
    public void testGetDefaultPaymentAddressForOrder_defaultUnitAddress() {
        final AddressModel billingAddress = mock(AddressModel.class);
        final CountryModel country = mock(CountryModel.class);

        when(this.abstractOrder.getUnit()).thenReturn(this.b2BUnit);
        when(this.b2BUnit.getBillingAddress()).thenReturn(billingAddress);
        when(billingAddress.getCountry()).thenReturn(country);
        final AddressModel result = this.vmkPreferredPaymentAddressLookupStrategy.getPreferredPaymentAddressForOrder(this.abstractOrder);

        assertThat(result).isEqualTo(billingAddress);
    }

    @Test
    public void testGetDefaultPaymentAddressForOrder_fallbackAddresses() {
        final AddressModel billingAddress = mock(AddressModel.class);
        final CountryModel country = mock(CountryModel.class);
        final List<AddressModel> billingAddresses = new ArrayList<>();
        billingAddresses.add(billingAddress);

        when(this.abstractOrder.getUnit()).thenReturn(this.b2BUnit);
        when(this.bluePaymentAddressesLookupStrategy.getPaymentAddressesForOrder(this.abstractOrder)).thenReturn(billingAddresses);
        when(billingAddress.getCountry()).thenReturn(country);
        final AddressModel result = this.vmkPreferredPaymentAddressLookupStrategy.getPreferredPaymentAddressForOrder(this.abstractOrder);

        verify(this.bluePaymentAddressesLookupStrategy).getPaymentAddressesForOrder(this.abstractOrder);
        assertThat(result).isEqualTo(billingAddress);
    }

    @Test
    public void testGetDefaultPaymentAddressForOrder_noAddressFound() {
        when(this.abstractOrder.getUnit()).thenReturn(this.b2BUnit);
        when(this.bluePaymentAddressesLookupStrategy.getPaymentAddressesForOrder(this.abstractOrder)).thenReturn(null);
        final AddressModel result = this.vmkPreferredPaymentAddressLookupStrategy.getPreferredPaymentAddressForOrder(this.abstractOrder);

        verify(this.bluePaymentAddressesLookupStrategy).getPaymentAddressesForOrder(this.abstractOrder);
        assertThat(result).isNull();
    }
}