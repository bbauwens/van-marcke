package com.vanmarcke.services.search.solrfacetsearch.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.solrfacetsearch.config.SearchQueryTemplate;
import de.hybris.platform.solrfacetsearch.model.SolrSearchQueryTemplateModel;
import junit.framework.TestCase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKSearchQueryTemplateOverridenPropertiesPopulatorTest extends TestCase {

    @Test
    public void testPopulateOverridenProperties() {
        VMKSearchQueryTemplateOverridenPropertiesPopulator vmkSearchQueryTemplateOverridenPropertiesPopulator = new VMKSearchQueryTemplateOverridenPropertiesPopulator();
        SolrSearchQueryTemplateModel source = mock(SolrSearchQueryTemplateModel.class);
        when(source.getOverrideSearchQueryProperties()).thenReturn(true);
        SearchQueryTemplate target = mock(SearchQueryTemplate.class);

        vmkSearchQueryTemplateOverridenPropertiesPopulator.populate(source, target);

        verify(source).getOverrideSearchQueryProperties();
        verify(target).setOverrideSearchQueryProperties(true);
    }
}