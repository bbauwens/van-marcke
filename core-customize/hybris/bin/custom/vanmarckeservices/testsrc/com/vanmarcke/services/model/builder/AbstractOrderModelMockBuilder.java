package com.vanmarcke.services.model.builder;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.basecommerce.jalo.site.BaseSite;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.UserModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * The {@code AbstractOrderModelMockBuilder} class can be used to create a mock implementation of the
 * {@link AbstractOrderModel} class.
 *
 * @author Christiaan Janssen
 * @since 29-04-2020
 */
public class AbstractOrderModelMockBuilder<S extends AbstractOrderModelMockBuilder, T extends AbstractOrderModel> {

    protected final S myself;

    protected String code;
    protected String guid;
    protected UserModel user;
    protected CurrencyModel currency;
    protected B2BUnitModel unit;
    protected boolean net;
    protected AddressModel deliveryAddress;
    protected AddressModel paymentAddress;
    protected List<AbstractOrderEntryModel> entries;
    protected BaseSiteModel site;

    /**
     * Creates a new instance of the {@link AbstractOrderModelMockBuilder} class.
     *
     * @param type the type
     */
    protected AbstractOrderModelMockBuilder(Class<S> type) {
        myself = type.cast(this);
        entries = new ArrayList<>();
    }

    /**
     * The entry point to the {@link AbstractOrderModelMockBuilder}.
     *
     * @return the {@link AbstractOrderModelMockBuilder} instance for method chaining
     */
    public static AbstractOrderModelMockBuilder anAbstractOrder() {
        return new AbstractOrderModelMockBuilder(AbstractOrderModelMockBuilder.class);
    }

    /**
     * Sets the {@code code} on the current {@link AbstractOrderModelMockBuilder} instance
     *
     * @param code the abstract order's unique identifier
     * @return the {@link AbstractOrderModelMockBuilder} instance for method chaining
     */
    public S withCode(String code) {
        this.code = code;
        return myself;
    }

    /**
     * Sets the {@code guid} on the current {@link AbstractOrderModelMockBuilder} instance
     *
     * @param guid the unique identifier
     * @return the {@link AbstractOrderModelMockBuilder} instance for method chaining
     */
    public S withGUID(String guid) {
        this.guid = guid;
        return myself;
    }

    /**
     * Sets the {@code user} on the current {@link AbstractOrderModelMockBuilder} instance.
     *
     * @param user the user
     * @return the {@link AbstractOrderModelMockBuilder} instance for method chaining
     */
    public S withUser(UserModel user) {
        this.user = user;
        return myself;
    }

    /**
     * Sets the {@code currency} on the current {@link AbstractOrderModelMockBuilder} instance.
     *
     * @param currency the currency
     * @return the {@link AbstractOrderModelMockBuilder} instance for method chaining
     */
    public S withCurrency(CurrencyModel currency) {
        this.currency = currency;
        return myself;
    }

    /**
     * Sets the {@code unit} on the current {@link AbstractOrderModelMockBuilder} instance.
     *
     * @param unit the unit
     * @return the {@link AbstractOrderModelMockBuilder} instance for method chaining
     */
    public S withUnit(B2BUnitModel unit) {
        this.unit = unit;
        return myself;
    }

    /**
     * Sets the {@code net} flag on the current {@link AbstractOrderModelMockBuilder} instance.
     *
     * @return the {@link AbstractOrderModelMockBuilder} instance for method chaining
     */
    public S withNet() {
        this.net = true;
        return myself;
    }

    /**
     * Sets the {@code deliveryAddress} on the current {@link AbstractOrderModelMockBuilder} instance.
     *
     * @param deliveryAddress the delivery address
     * @return the {@link AbstractOrderModelMockBuilder} instance for method chaining
     */
    public S withDeliveryAddress(AddressModel deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
        return myself;
    }

    /**
     * Sets the {@code paymentAddress} on the current {@link AbstractOrderModelMockBuilder} instance.
     *
     * @param paymentAddress the payment address
     * @return the {@link AbstractOrderModelMockBuilder} instance for method chaining
     */
    public S withPaymentAddress(AddressModel paymentAddress) {
        this.paymentAddress = paymentAddress;
        return myself;
    }

    /**
     * Sets the {@code net} flag on the current {@link AbstractOrderModelMockBuilder} instance.
     *
     * @return the {@link AbstractOrderModelMockBuilder} instance for method chaining
     */
    public S withEntries(AbstractOrderEntryModel... entries) {
        this.entries.addAll(Arrays.asList(entries));
        return myself;
    }

    /**
     * Sets the {@code site} on the current {@link AbstractOrderModelMockBuilder} instance.
     *
     * @return the {@link AbstractOrderModelMockBuilder} instance for method chaining
     */
    public S withSite(BaseSiteModel site) {
        this.site = site;
        return myself;
    }


    /**
     * Creates a mock implementation of the {@link AbstractOrderModel} class.
     *
     * @return the {@link AbstractOrderModel} instance
     */
    public AbstractOrderModel build() {
        AbstractOrderModel order = mock(AbstractOrderModel.class);
        when(order.getCode()).thenReturn(code);
        when(order.getGuid()).thenReturn(guid);
        when(order.getUser()).thenReturn(user);
        when(order.getCurrency()).thenReturn(currency);
        when(order.getUnit()).thenReturn(unit);
        when(order.getNet()).thenReturn(net);
        when(order.getDeliveryAddress()).thenReturn(deliveryAddress);
        when(order.getPaymentAddress()).thenReturn(paymentAddress);
        when(order.getEntries()).thenReturn(entries);
        when(order.getSite()).thenReturn(site);
        return order;
    }
}
