package com.vanmarcke.services.job.maintenance.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cronjob.model.CronJobHistoryModel;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.internal.model.MaintenanceCleanupJobModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.time.TimeService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Date;

import static java.util.Collections.singletonList;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKCleanupJobHistoryStrategyImplTest {

    @Mock
    private ModelService modelService;
    @Mock
    private TimeService timeService;
    @InjectMocks
    private VMKCleanupJobHistoryStrategyImpl cleanupStrategy;

    @Test
    public void testCreateFetchQuery() {
        CronJobModel cronJobModel = mock(CronJobModel.class);
        MaintenanceCleanupJobModel jobModel = mock(MaintenanceCleanupJobModel.class);

        when(cronJobModel.getJob()).thenReturn(jobModel);

        Date date = Date.from(LocalDateTime.of(2019, 8, 4, 20, 45, 16).toInstant(ZoneOffset.of("+02:00")));
        when(timeService.getCurrentTime()).thenReturn(date);

        FlexibleSearchQuery result = cleanupStrategy.createFetchQuery(cronJobModel);

        assertThat(result.getQuery()).isEqualTo("SELECT {pk} FROM {CronJobHistory} WHERE {endTime} < ?date");
        assertThat(result.getQueryParameters()).hasSize(1);
        assertThat(result.getQueryParameters().containsKey("date")).isTrue();

        Date paramDate = (Date) result.getQueryParameters().get("date");
        assertThat((date.getTime() - paramDate.getTime()) / (1000 * 60 * 60 * 24)).isEqualTo(14);
    }

    @Test
    public void testCreateFetchQuery_withTreshold() {
        CronJobModel cronJobModel = mock(CronJobModel.class);
        MaintenanceCleanupJobModel jobModel = mock(MaintenanceCleanupJobModel.class);

        when(cronJobModel.getJob()).thenReturn(jobModel);

        when(jobModel.getThreshold()).thenReturn(5);

        Date date = Date.from(LocalDateTime.of(2019, 8, 4, 20, 45, 16).toInstant(ZoneOffset.of("+02:00")));
        when(timeService.getCurrentTime()).thenReturn(date);

        FlexibleSearchQuery result = cleanupStrategy.createFetchQuery(cronJobModel);

        assertThat(result.getQuery()).isEqualTo("SELECT {pk} FROM {CronJobHistory} WHERE {endTime} < ?date");
        assertThat(result.getQueryParameters()).hasSize(1);
        assertThat(result.getQueryParameters().containsKey("date")).isTrue();

        Date paramDate = (Date) result.getQueryParameters().get("date");
        assertThat((date.getTime() - paramDate.getTime()) / (1000 * 60 * 60 * 24)).isEqualTo(5);
    }

    @Test
    public void testProcess() {
        CronJobHistoryModel cronJobHistoryModel = mock(CronJobHistoryModel.class);

        cleanupStrategy.process(singletonList(cronJobHistoryModel));

        verify(modelService).removeAll(singletonList(cronJobHistoryModel));
    }

}