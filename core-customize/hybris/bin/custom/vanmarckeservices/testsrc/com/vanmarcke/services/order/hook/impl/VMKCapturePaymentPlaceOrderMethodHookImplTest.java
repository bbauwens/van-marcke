package com.vanmarcke.services.order.hook.impl;

import com.vanmarcke.services.strategies.VMKCommercePaymentCaptureStrategy;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.core.model.order.CartModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKCapturePaymentPlaceOrderMethodHookImplTest {

    @Mock
    private VMKCommercePaymentCaptureStrategy commercePaymentCaptureStrategy;
    @InjectMocks
    private VMKCapturePaymentPlaceOrderMethodHookImpl defaultVMKCapturePaymentPlaceOrderMethodHook;

    @Test
    public void testBeforePlaceOrder() throws Exception {
        CartModel cartModel = mock(CartModel.class);

        CommerceCheckoutParameter parameter = new CommerceCheckoutParameter();
        parameter.setCart(cartModel);
        defaultVMKCapturePaymentPlaceOrderMethodHook.beforePlaceOrder(parameter);

        verify(commercePaymentCaptureStrategy).capturePaymentAmount(cartModel);
    }
}