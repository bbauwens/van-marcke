package com.vanmarcke.services.search.solrfacetsearch.provider.impl;

import com.vanmarcke.core.model.BrandCategoryModel;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.variants.model.VariantCategoryModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collection;

import static java.util.Collections.singleton;
import static java.util.Collections.singletonList;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@UnitTest
public class VMKCategorySourceTest {

    @InjectMocks
    private VMKCategorySource vmkCategorySource;

    @Mock
    private BrandCategoryModel brandCategoryModel;
    @Mock
    private ClassificationClassModel classificationClassModel;
    @Mock
    private VariantCategoryModel variantCategoryModel;

    @Test
    public void isBlockedCategoryForBrandCategory() {
        final Boolean result = vmkCategorySource.isBlockedCategory(brandCategoryModel);
        assertThat(result).isTrue();
    }

    @Test
    public void isBlockedCategoryForClassificationClasses() {
        final Boolean result = vmkCategorySource.isBlockedCategory(classificationClassModel);
        assertThat(result).isTrue();
    }

    @Test
    public void isNotABlockedCategory() {
        final Boolean result = vmkCategorySource.isBlockedCategory(variantCategoryModel);
        assertThat(result).isFalse();
    }

    @Test
    public void testGetAllCategories() throws Exception {
        vmkCategorySource.setRootCategory("root");
        CategoryModel directCategory = mock(CategoryModel.class);
        CategoryModel rootCategory = mock(CategoryModel.class);
        when(directCategory.getCode()).thenReturn("direct");
        when(directCategory.getSupercategories()).thenReturn(singletonList(rootCategory));
        when(rootCategory.getCode()).thenReturn("root");
        Collection<CategoryModel> result = vmkCategorySource.getAllCategories(directCategory, singleton(rootCategory));
        assertThat(result).containsOnly(rootCategory, directCategory);
    }

    @Test
    public void testGetAllCategories_withExcludeRootCategory() throws Exception {
        vmkCategorySource.setRootCategory("root");
        vmkCategorySource.setExcludeRootCategory(true);
        CategoryModel directCategory = mock(CategoryModel.class);
        CategoryModel rootCategory = mock(CategoryModel.class);
        when(directCategory.getCode()).thenReturn("direct");
        when(directCategory.getSupercategories()).thenReturn(singletonList(rootCategory));
        when(rootCategory.getCode()).thenReturn("root");
        Collection<CategoryModel> result = vmkCategorySource.getAllCategories(directCategory, singleton(rootCategory));
        assertThat(result).containsOnly(directCategory);
    }
}