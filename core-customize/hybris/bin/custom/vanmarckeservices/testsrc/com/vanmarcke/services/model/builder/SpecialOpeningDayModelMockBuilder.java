package com.vanmarcke.services.model.builder;

import de.hybris.platform.storelocator.model.SpecialOpeningDayModel;

import java.util.Date;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * The {@code SpecialOpeningDayModelMockBuilder} class can be used to create a mock implementation of the
 * {@link SpecialOpeningDayModel} class.
 *
 * @author Christiaan Janssen
 * @since 07-07-2020
 */
public class SpecialOpeningDayModelMockBuilder extends OpeningDayModelMockBuilder<SpecialOpeningDayModelMockBuilder, SpecialOpeningDayModel> {

    private Date date;
    private boolean closed;

    /**
     * Creates a new instance of the {@link SpecialOpeningDayModelMockBuilder} class.
     */
    private SpecialOpeningDayModelMockBuilder() {
        super(SpecialOpeningDayModelMockBuilder.class);
    }

    /**
     * The entry point to the {@link SpecialOpeningDayModelMockBuilder}.
     *
     * @return the {@link SpecialOpeningDayModelMockBuilder} instance for method chaining
     */
    public static SpecialOpeningDayModelMockBuilder aSpecialOpeningDay() {
        return new SpecialOpeningDayModelMockBuilder();
    }

    /**
     * Sets the {@code date} on the current {@link SpecialOpeningDayModelMockBuilder} instance
     *
     * @param date the date
     * @return the {@link SpecialOpeningDayModelMockBuilder} instance for method chaining
     */
    public SpecialOpeningDayModelMockBuilder withDate(Date date) {
        this.date = date;
        return this;
    }

    /**
     * Sets the {@code closed} on the current {@link SpecialOpeningDayModelMockBuilder} instance
     *
     * @param closed flag to indicate whether the store is closed
     * @return the {@link SpecialOpeningDayModelMockBuilder} instance for method chaining
     */
    public SpecialOpeningDayModelMockBuilder withClosed(boolean closed) {
        this.closed = closed;
        return this;
    }

    /**
     * Creates a mock implementation of the {@link SpecialOpeningDayModel} class.
     *
     * @return the {@link SpecialOpeningDayModel} instance
     */
    public SpecialOpeningDayModel build() {
        SpecialOpeningDayModel openingDay = mock(SpecialOpeningDayModel.class);
        when(openingDay.getDate()).thenReturn(date);
        when(openingDay.isClosed()).thenReturn(closed);
        when(openingDay.getOpeningTime()).thenReturn(openingTime);
        when(openingDay.getClosingTime()).thenReturn(closingTime);
        return openingDay;
    }
}
