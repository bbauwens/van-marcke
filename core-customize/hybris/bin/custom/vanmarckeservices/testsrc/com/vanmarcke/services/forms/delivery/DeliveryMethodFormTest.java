package com.vanmarcke.services.forms.delivery;

import de.hybris.bootstrap.annotations.UnitTest;
import org.apache.commons.lang.RandomStringUtils;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DeliveryMethodFormTest {

    private static final String SHIPPING_DATE = RandomStringUtils.randomAlphabetic(10);
    private static final String SHIPPING = "DELIVERY";
    private static final String PICKUP_DATE = RandomStringUtils.randomAlphabetic(10);
    private static final String PICKUP = "PICKUP";

    @Test
    public void getActualDate_pickup() {
        DeliveryMethodForm form = new DeliveryMethodForm();
        form.setSelectedPickupDate(PICKUP_DATE);
        form.setSelectedShippingDate(SHIPPING_DATE);
        form.setDeliveryMethod(PICKUP);

        Assertions
                .assertThat(form.getActualDate())
                .isEqualTo(PICKUP_DATE);
    }

    @Test
    public void getActualDate_shipping() {
        DeliveryMethodForm form = new DeliveryMethodForm();
        form.setSelectedPickupDate(PICKUP_DATE);
        form.setSelectedShippingDate(SHIPPING_DATE);
        form.setDeliveryMethod(SHIPPING);

        Assertions
                .assertThat(form.getActualDate())
                .isEqualTo(SHIPPING_DATE);
    }

    @Test
    public void getActualDate_null() {
        DeliveryMethodForm form = new DeliveryMethodForm();
        form.setSelectedPickupDate(PICKUP_DATE);
        form.setSelectedShippingDate(SHIPPING_DATE);
        form.setDeliveryMethod(null);

        Assertions
                .assertThat(form.getActualDate())
                .isEqualTo(null);
    }

}