package com.vanmarcke.services.order.hook.impl;

import com.vanmarcke.services.address.VMKPreferredDeliveryAddressLookupStrategy;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.model.ModelService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKPreferredDeliveryAddressCartCalculationMethodHookImplTest {

    @Mock
    private VMKPreferredDeliveryAddressLookupStrategy bluePreferredDeliveryAddressLookupStrategy;
    @Mock
    private ModelService modelService;

    @InjectMocks
    private VMKPreferredDeliveryAddressCartCalculationMethodHookImpl bluePreferredDeliveryAddressCartCalculationMethodHook;

    @Test
    public void testBeforeCalculate_isB2BCart_withoutDeliveryAddress() {
        final CommerceCartParameter parameter = mock(CommerceCartParameter.class);
        final CartModel cart = mock(CartModel.class);
        final B2BCustomerModel customer = mock(B2BCustomerModel.class);
        final AddressModel addressModel = mock(AddressModel.class);

        when(parameter.getCart()).thenReturn(cart);

        when(cart.getUser()).thenReturn(customer);

        when(this.bluePreferredDeliveryAddressLookupStrategy.getPreferredDeliveryAddressForOrder(cart)).thenReturn(addressModel);

        this.bluePreferredDeliveryAddressCartCalculationMethodHook.beforeCalculate(parameter);

        verify(cart).setDeliveryAddress(addressModel);

        verify(this.modelService).save(cart);
    }

    @Test
    public void testBeforeCalculate_isB2BCart_withoutDeliveryAddress_withoutDefaultDeliveryAddress() {
        final CommerceCartParameter parameter = mock(CommerceCartParameter.class);
        final CartModel cart = mock(CartModel.class);
        final B2BCustomerModel customer = mock(B2BCustomerModel.class);

        when(parameter.getCart()).thenReturn(cart);

        when(cart.getUser()).thenReturn(customer);

        when(this.bluePreferredDeliveryAddressLookupStrategy.getPreferredDeliveryAddressForOrder(cart)).thenReturn(null);

        this.bluePreferredDeliveryAddressCartCalculationMethodHook.beforeCalculate(parameter);

        verify(cart, never()).setDeliveryAddress(any(AddressModel.class));
        verifyZeroInteractions(this.modelService);
    }

    @Test
    public void testBeforeCalculate_notB2BCart() {
        final CommerceCartParameter parameter = mock(CommerceCartParameter.class);
        final CartModel cart = mock(CartModel.class);
        final CustomerModel anonymous = mock(CustomerModel.class);

        when(parameter.getCart()).thenReturn(cart);
        when(cart.getUser()).thenReturn(anonymous);
        this.bluePreferredDeliveryAddressCartCalculationMethodHook.beforeCalculate(parameter);

        Mockito.verifyZeroInteractions(this.bluePreferredDeliveryAddressLookupStrategy);
        verifyZeroInteractions(this.modelService);
    }
}