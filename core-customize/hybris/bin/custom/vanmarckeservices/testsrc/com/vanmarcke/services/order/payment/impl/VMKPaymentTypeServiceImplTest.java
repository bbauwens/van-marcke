package com.vanmarcke.services.order.payment.impl;

import com.vanmarcke.services.order.credit.VMKCreditCheckService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2bacceleratorservices.enums.CheckoutPaymentType;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.enumeration.EnumerationService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static de.hybris.platform.b2bacceleratorservices.enums.CheckoutPaymentType.ACCOUNT;
import static de.hybris.platform.b2bacceleratorservices.enums.CheckoutPaymentType.CARD;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

/**
 * The {@link VMKPaymentTypeServiceImplTest} class contains the unit tests for the {@link VMKPaymentTypeServiceImpl}
 * class.
 *
 * @author Taki Korovessis, Christiaan Janssen
 * @since 21-01-2020
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKPaymentTypeServiceImplTest {

    @Mock
    private EnumerationService enumerationService;

    @Mock
    private VMKCreditCheckService vmkCreditCheckService;

    @InjectMocks
    private VMKPaymentTypeServiceImpl defaultVMKPaymentTypeService;

    @Test
    public void testGetSupportedPaymentTypeListForOrder_creditworthy() {
        // given
        final CartModel cart = mock(CartModel.class);

        when(enumerationService.getEnumerationValues("CheckoutPaymentType")).thenReturn(newArrayList(ACCOUNT, CARD));
        when(vmkCreditCheckService.validateCreditworthinessForCart(cart)).thenReturn(true);

        // when
        final List<CheckoutPaymentType> result = defaultVMKPaymentTypeService.getSupportedPaymentTypeListForOrder(cart);

        //then
        assertThat(result).containsExactly(ACCOUNT, CARD);

        verify(enumerationService).getEnumerationValues("CheckoutPaymentType");
        verify(vmkCreditCheckService).validateCreditworthinessForCart(cart);
    }

    @Test
    public void testGetSupportedPaymentTypeListForOrder_notCreditworthy() {
        // given
        final CartModel cart = mock(CartModel.class);

        when(enumerationService.getEnumerationValues("CheckoutPaymentType")).thenReturn(newArrayList(ACCOUNT, CARD));
        when(vmkCreditCheckService.validateCreditworthinessForCart(cart)).thenReturn(false);

        // when
        final List<CheckoutPaymentType> result = defaultVMKPaymentTypeService.getSupportedPaymentTypeListForOrder(cart);

        //then
        assertThat(result).containsExactly(CARD);

        verify(enumerationService).getEnumerationValues("CheckoutPaymentType");
        verify(vmkCreditCheckService).validateCreditworthinessForCart(cart);
    }
}
