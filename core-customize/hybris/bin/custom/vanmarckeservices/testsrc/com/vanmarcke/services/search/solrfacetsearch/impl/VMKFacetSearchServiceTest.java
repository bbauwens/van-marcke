package com.vanmarcke.services.search.solrfacetsearch.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.solrfacetsearch.config.*;
import de.hybris.platform.solrfacetsearch.search.FacetField;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import de.hybris.platform.store.services.BaseStoreService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKFacetSearchServiceTest {

    @Mock
    private BaseStoreService baseStoreService;
    @InjectMocks
    private VMKFacetSearchService facetSearchService;

    @Test
    public void testPopulateFacetFields_defaultSearchProperties() {
        // given
        FacetSearchConfig facetSearchConfig = mock(FacetSearchConfig.class);
        IndexedType indexedType = mock(IndexedType.class);
        SearchQueryTemplate searchQueryTemplate = mock(SearchQueryTemplate.class);
        SearchQuery searchQuery = mock(SearchQuery.class);
        IndexedProperty indexedProperty = mock(IndexedProperty.class);
        when(indexedType.getIndexedProperties()).thenReturn(Map.of("sample_facet", indexedProperty));
        when(indexedProperty.isFacet()).thenReturn(true);
        when(indexedProperty.getFacetType()).thenReturn(FacetType.MULTISELECTOR);
        when(indexedProperty.getName()).thenReturn("sample_property");
        when(indexedProperty.getPriority()).thenReturn(0);
        when(indexedProperty.getFacetDisplayNameProvider()).thenReturn("displayNameProvider");
        when(indexedProperty.getFacetSortProvider()).thenReturn("facetSortProvider");
        when(indexedProperty.getTopValuesProvider()).thenReturn("topValuesProvider");
        when(searchQueryTemplate.getSearchQueryProperties()).thenReturn(null);
        when(searchQueryTemplate.getOverrideSearchQueryProperties()).thenReturn(false);
        when(searchQueryTemplate.isShowFacets()).thenReturn(true);

        // when
        facetSearchService.populateFacetFields(facetSearchConfig, indexedType, searchQueryTemplate, searchQuery);

        // then
        ArgumentCaptor<FacetField> facetFieldArgumentCaptor = ArgumentCaptor.forClass(FacetField.class);
        verify(searchQuery).addFacet(facetFieldArgumentCaptor.capture());
        assertEquals("sample_property", facetFieldArgumentCaptor.getValue().getField());
        assertEquals(FacetType.MULTISELECTOR, facetFieldArgumentCaptor.getValue().getFacetType());
        assertEquals(0, facetFieldArgumentCaptor.getValue().getPriority().intValue());
        assertEquals("displayNameProvider", facetFieldArgumentCaptor.getValue().getDisplayNameProvider());
        assertEquals("facetSortProvider", facetFieldArgumentCaptor.getValue().getSortProvider());
        assertEquals("topValuesProvider", facetFieldArgumentCaptor.getValue().getTopValuesProvider());
    }

    @Test
    public void testPopulateFacetFields_templateWithSearchQueryProperties() {
        // given
        FacetSearchConfig facetSearchConfig = mock(FacetSearchConfig.class);
        IndexedType indexedType = mock(IndexedType.class);
        SearchQueryTemplate searchQueryTemplate = mock(SearchQueryTemplate.class);
        SearchQuery searchQuery = mock(SearchQuery.class);
        SearchQueryProperty searchQueryProperty = mock(SearchQueryProperty.class);
        when(searchQueryTemplate.isShowFacets()).thenReturn(true);
        when(searchQueryTemplate.getOverrideSearchQueryProperties()).thenReturn(true);
        when(searchQueryTemplate.getSearchQueryProperties()).thenReturn(Map.of("searchQueryProperty", searchQueryProperty));
        when(searchQueryProperty.isFacet()).thenReturn(true);
        when(searchQueryProperty.getIndexedProperty()).thenReturn("sample_property");
        when(searchQueryProperty.getFacetType()).thenReturn(FacetType.MULTISELECTOR);
        when(searchQueryProperty.getPriority()).thenReturn(0);
        when(searchQueryProperty.getFacetDisplayNameProvider()).thenReturn("displayNameProvider");
        when(searchQueryProperty.getFacetSortProvider()).thenReturn("facetSortProvider");
        when(searchQueryProperty.getFacetTopValuesProvider()).thenReturn("topValuesProvider");

        // when
        facetSearchService.populateFacetFields(facetSearchConfig, indexedType, searchQueryTemplate, searchQuery);

        // then
        ArgumentCaptor<FacetField> facetFieldArgumentCaptor = ArgumentCaptor.forClass(FacetField.class);
        verify(searchQuery).addFacet(facetFieldArgumentCaptor.capture());
        assertEquals(0, facetFieldArgumentCaptor.getValue().getPriority().intValue());
        assertEquals("sample_property", facetFieldArgumentCaptor.getValue().getField());
        assertEquals(FacetType.MULTISELECTOR, facetFieldArgumentCaptor.getValue().getFacetType());
        assertEquals("displayNameProvider", facetFieldArgumentCaptor.getValue().getDisplayNameProvider());
        assertEquals("facetSortProvider", facetFieldArgumentCaptor.getValue().getSortProvider());
        assertEquals("topValuesProvider", facetFieldArgumentCaptor.getValue().getTopValuesProvider());
    }


    @Test
    public void testPopulateFreeTextQuery_defaultSearchProperties() {
        // given
        FacetSearchConfig facetSearchConfig = mock(FacetSearchConfig.class);
        IndexedType indexedType = mock(IndexedType.class);
        SearchQueryTemplate searchQueryTemplate = mock(SearchQueryTemplate.class);
        SearchQuery searchQuery = mock(SearchQuery.class);
        String userQuery = "sample_query";

        when(searchQueryTemplate.getSearchQueryProperties()).thenReturn(null);
        when(searchQueryTemplate.getOverrideSearchQueryProperties()).thenReturn(false);
        when(indexedType.getFtsQueryBuilder()).thenReturn("defaultFtsBuilder");
        // when
        facetSearchService.populateFreeTextQuery(facetSearchConfig, indexedType, searchQueryTemplate, searchQuery, userQuery);

        // then
        verify(indexedType).getFtsQueryBuilder();
        verify(searchQuery).setFreeTextQueryBuilder("defaultFtsBuilder");
        verify(searchQuery).setUserQuery("sample_query");
    }

    @Test
    public void testPopulateFreeTextQuery_templateWithSearchQueryProperties() {
        // given
        FacetSearchConfig facetSearchConfig = mock(FacetSearchConfig.class);
        IndexedType indexedType = mock(IndexedType.class);
        SearchQueryTemplate searchQueryTemplate = mock(SearchQueryTemplate.class);
        SearchQuery searchQuery = mock(SearchQuery.class);
        String userQuery = "sample_query";
        when(searchQueryTemplate.getSearchQueryProperties()).thenReturn(null);
        when(searchQueryTemplate.getOverrideSearchQueryProperties()).thenReturn(true);
        when(searchQueryTemplate.getFtsQueryBuilder()).thenReturn("defaultFtsBuilder");
        when(searchQueryTemplate.getSearchQueryProperties()).thenReturn(Collections.emptyMap());

        // when
        facetSearchService.populateFreeTextQuery(facetSearchConfig, indexedType, searchQueryTemplate, searchQuery, userQuery);

        // then
        verify(searchQueryTemplate).getFtsQueryBuilder();
        verify(searchQuery).setFreeTextQueryBuilder("defaultFtsBuilder");
        verify(searchQuery).setUserQuery("sample_query");
    }
}
