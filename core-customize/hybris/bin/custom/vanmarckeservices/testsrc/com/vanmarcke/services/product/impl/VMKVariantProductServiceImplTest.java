package com.vanmarcke.services.product.impl;

import com.vanmarcke.facades.product.data.StockListData;
import com.vanmarcke.services.constants.VITCalculatedStatusConstants;
import com.vanmarcke.services.constants.VITDeliveryMethodConstants;
import com.vanmarcke.services.model.builder.VariantProductModelMockBuilder;
import com.vanmarcke.services.product.VMKStockService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.StockData;
import de.hybris.platform.variants.model.VariantProductModel;
import org.apache.commons.lang3.RandomStringUtils;
import org.fest.assertions.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKVariantProductServiceImplTest {

    private static final String RANDOM_STRING = RandomStringUtils.randomAlphabetic(10);

    @Mock
    private VMKStockService stockService;

    @Spy
    @InjectMocks
    private VMKVariantProductServiceImpl defaultVMKVariantProductService;


    @Test
    public void testIsPurchasable_XDockProduct() {
        VariantProductModel product = mock(VariantProductModel.class);

        when(product.getDeliveryMethod()).thenReturn(VITDeliveryMethodConstants.CROSS_DOCK);

        assertThat(defaultVMKVariantProductService.isPotentiallyInStock(product))
                .isFalse();

        verify(product).getDeliveryMethod();
        verifyNoMoreInteractions(product);
    }

    @Test
    public void testIsPurchasable_NoStockProductAndNotAvailable() {
        VariantProductModel product = mock(VariantProductModel.class);

        when(product.getDeliveryMethod()).thenReturn(VITDeliveryMethodConstants.NO_STOCK);
        when(product.getCalculatedStatus()).thenReturn(VITCalculatedStatusConstants.NOT_AVAILABLE);

        Assertions
                .assertThat(defaultVMKVariantProductService.isPotentiallyInStock(product))
                .isFalse();

        verify(product, times(2)).getDeliveryMethod();
        verify(product).getCalculatedStatus();
        verifyNoMoreInteractions(product);
    }

    @Test
    public void testIsPurchasable_NoStockProductAndAvailable() {
        VariantProductModel product = mock(VariantProductModel.class);

        when(product.getDeliveryMethod()).thenReturn(VITDeliveryMethodConstants.NO_STOCK);
        when(product.getCalculatedStatus()).thenReturn(RANDOM_STRING);

        Assertions
                .assertThat(defaultVMKVariantProductService.isPotentiallyInStock(product))
                .isTrue();

        verify(product, times(2)).getDeliveryMethod();
        verify(product).getCalculatedStatus();
        verifyNoMoreInteractions(product);
    }

    @Test
    public void testIsNosProduct_sellingOffTecStock() {
        VariantProductModel product = mock(VariantProductModel.class);
        doReturn(true).when(defaultVMKVariantProductService).isPotentiallyInStock(product);
        when(product.getDeliveryMethod()).thenReturn(VITDeliveryMethodConstants.NO_STOCK);
        when(product.getCalculatedStatus()).thenReturn(VITCalculatedStatusConstants.SELLING_OFF);
        doReturn(0L).when(defaultVMKVariantProductService).getAvailableEDCStock(product);

        Assertions
                .assertThat(defaultVMKVariantProductService.isNOSProduct(product))
                .isTrue();

        verify(product).getDeliveryMethod();
        verify(product).getCalculatedStatus();
        verify(defaultVMKVariantProductService).getAvailableEDCStock(product);
    }

    @Test
    public void testIsNosProduct_soldOff() {
        VariantProductModel product = mock(VariantProductModel.class);
        doReturn(false).when(defaultVMKVariantProductService).isPotentiallyInStock(product);

        Assertions
                .assertThat(defaultVMKVariantProductService.isNOSProduct(product))
                .isTrue();

        verify(product, times(0)).getDeliveryMethod();
        verify(product, times(0)).getCalculatedStatus();
        verify(defaultVMKVariantProductService, times(0)).getAvailableEDCStock(product);
    }

    @Test
    public void testIsNoNOSProduct() {
        VariantProductModel product = mock(VariantProductModel.class);
        doReturn(true).when(defaultVMKVariantProductService).isPotentiallyInStock(product);
        doReturn(1L).when(defaultVMKVariantProductService).getAvailableEDCStock(product);
        Assertions
                .assertThat(defaultVMKVariantProductService.isNOSProduct(product))
                .isFalse();
    }

    @Test
    public void testIsSellingOff() {
        VariantProductModel product = mock(VariantProductModel.class);
        when(product.getDeliveryMethod()).thenReturn(VITDeliveryMethodConstants.NO_STOCK);
        when(product.getCalculatedStatus()).thenReturn(VITCalculatedStatusConstants.SELLING_OFF);
        Assertions
                .assertThat(defaultVMKVariantProductService.isSellingOffAlmostNOS(product))
                .isTrue();
    }

    @Test
    public void testIsNotSellingOff_noDeliveryMethod() {
        VariantProductModel product = mock(VariantProductModel.class);
        when(product.getDeliveryMethod()).thenReturn(RANDOM_STRING);
        when(product.getCalculatedStatus()).thenReturn(VITCalculatedStatusConstants.SELLING_OFF);
        Assertions
                .assertThat(defaultVMKVariantProductService.isSellingOffAlmostNOS(product))
                .isFalse();
    }

    @Test
    public void testIsNotSellingOff_noCalculatedStatus() {
        VariantProductModel product = mock(VariantProductModel.class);
        when(product.getDeliveryMethod()).thenReturn(VITDeliveryMethodConstants.NO_STOCK);
        when(product.getCalculatedStatus()).thenReturn(RANDOM_STRING);
        Assertions
                .assertThat(defaultVMKVariantProductService.isSellingOffAlmostNOS(product))
                .isFalse();
    }




    @Test
    public void testIsLimitedStock() {
        VariantProductModel product = mock(VariantProductModel.class);
        when(product.getDeliveryMethod()).thenReturn(null);
        when(product.getCalculatedStatus()).thenReturn(VITCalculatedStatusConstants.NOT_AVAILABLE);

        Assertions
                .assertThat(defaultVMKVariantProductService.isDiscontinuedProduct(product))
                .isTrue();
        verify(product).getDeliveryMethod();
        verify(product).getCalculatedStatus();
    }

    @Test
    public void testIsNotLimitedStock_DeliveryMethod() {
        VariantProductModel product = mock(VariantProductModel.class);
        String deliveryMethod = "standard";
        when(product.getDeliveryMethod()).thenReturn(deliveryMethod);
        when(product.getCalculatedStatus()).thenReturn(VITCalculatedStatusConstants.NOT_AVAILABLE);

        Assertions
                .assertThat(defaultVMKVariantProductService.isDiscontinuedProduct(product))
                .isFalse();
        verify(product).getDeliveryMethod();
        verify(product, times(0)).getCalculatedStatus();
    }

    @Test
    public void isDiscontinuedProduct_OutOfStock() {
        VariantProductModel product = VariantProductModelMockBuilder.aVariantProduct("product_code", null).build();
        doReturn(0L).when(defaultVMKVariantProductService).getAvailableEDCStock(product);
        doReturn(true).when(defaultVMKVariantProductService).isDiscontinuedProduct(product);
        Assertions.assertThat(defaultVMKVariantProductService.isDiscontinuedOutOfStockProduct(product)).isTrue();
    }

    @Test
    public void testProductIsSoonInStock() {
        VariantProductModel product = mock(VariantProductModel.class);
        when(product.getAvailabilityDate()).thenReturn(null);
        Assertions.assertThat(defaultVMKVariantProductService.isSoonInStockProduct(product)).isTrue();
    }

    @Test
    public void testProductIsNotSoonInStock() {
        VariantProductModel product = mock(VariantProductModel.class);
        when(product.getAvailabilityDate()).thenReturn("01/01/2022");
        Assertions.assertThat(defaultVMKVariantProductService.isSoonInStockProduct(product)).isFalse();
    }


    @Test
    public void testProductInvalidCartEntry_notPurchasable() {
        VariantProductModel product = mock(VariantProductModel.class);
        doReturn(true).when(defaultVMKVariantProductService).isNOSProduct(product);
        doReturn(false).when(defaultVMKVariantProductService).isSoonInStockProduct(product);
        doReturn(false).when(defaultVMKVariantProductService).isDiscontinuedOutOfStockProduct(product);
        Assertions.assertThat(defaultVMKVariantProductService.isPurchasable(product)).isFalse();
    }

    @Test
    public void testProductInvalidCartEntry_soonInStock() {
        VariantProductModel product = mock(VariantProductModel.class);
        doReturn(false).when(defaultVMKVariantProductService).isNOSProduct(product);
        doReturn(true).when(defaultVMKVariantProductService).isSoonInStockProduct(product);
        doReturn(false).when(defaultVMKVariantProductService).isDiscontinuedOutOfStockProduct(product);
        Assertions.assertThat(defaultVMKVariantProductService.isPurchasable(product)).isFalse();
    }

    @Test
    public void testProductInvalidCartEntry_discontinuedOutOfStock() {
        VariantProductModel product = mock(VariantProductModel.class);
        doReturn(false).when(defaultVMKVariantProductService).isNOSProduct(product);
        doReturn(false).when(defaultVMKVariantProductService).isSoonInStockProduct(product);
        doReturn(true).when(defaultVMKVariantProductService).isDiscontinuedOutOfStockProduct(product);
        Assertions.assertThat(defaultVMKVariantProductService.isPurchasable(product)).isFalse();
    }


    @Test
    public void testProductInvalidCartEntry_isNotInvalid() {
        VariantProductModel product = mock(VariantProductModel.class);
        doReturn(false).when(defaultVMKVariantProductService).isNOSProduct(product);
        doReturn(false).when(defaultVMKVariantProductService).isSoonInStockProduct(product);
        doReturn(false).when(defaultVMKVariantProductService).isDiscontinuedOutOfStockProduct(product);
        Assertions.assertThat(defaultVMKVariantProductService.isPurchasable(product)).isTrue();
    }

    @Test
    public void testGetAvailableEDCStock() {
        VariantProductModel productModel = VariantProductModelMockBuilder.aVariantProduct("product_code", null).build();
        StockData stockData = mock(StockData.class);
        when(stockData.getWareHouse()).thenReturn("EDC");
        when(stockData.getStockLevel()).thenReturn(10L);
        StockListData stockListData = mock(StockListData.class);
        when(stockListData.getStock()).thenReturn(List.of(stockData));
        when(stockService.getStockForProductAndWarehouses(productModel, new String[]{})).thenReturn(stockListData);


        // then
        Assert.assertEquals(10, defaultVMKVariantProductService.getAvailableEDCStock(productModel));
    }

    @Test
    public void testGetAvailableEDCStock_nullStockLevel() {
        VariantProductModel productModel = VariantProductModelMockBuilder.aVariantProduct("product_code", null).build();
        StockListData stockListData = mock(StockListData.class);
        when(stockListData.getStock()).thenReturn(Collections.emptyList());

        Assert.assertEquals(0L, defaultVMKVariantProductService.getAvailableEDCStock(productModel));
    }
}