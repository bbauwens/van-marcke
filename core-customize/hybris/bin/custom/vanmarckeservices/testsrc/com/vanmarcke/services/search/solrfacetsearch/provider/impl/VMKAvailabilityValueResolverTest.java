package com.vanmarcke.services.search.solrfacetsearch.provider.impl;

import com.vanmarcke.core.model.VanMarckeVariantProductModel;
import com.vanmarcke.core.model.VanmarckeCountryChannelModel;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolverTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;

import static java.util.Collections.singleton;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKAvailabilityValueResolverTest extends AbstractValueResolverTest {

    @Mock
    private InputDocument inputDocument;
    @Mock
    private IndexerBatchContext indexerBatchContext;
    @Mock
    private IndexedProperty indexedProperty;
    @Mock
    private VanMarckeVariantProductModel vanMarckeVariantProductModel;

    @InjectMocks
    private VMKAvailabilityValueResolver valueResolver;

    @Test
    public void testAddFieldValuesWhenNotVanMarckeVariant() throws FieldValueProviderException {
        ProductModel productModel = mock(ProductModel.class);
        valueResolver.resolve(inputDocument, indexerBatchContext, Collections.singletonList(indexedProperty), productModel);
        verifyZeroInteractions(inputDocument);
    }

    @Test
    public void testAddFieldValuesWhenEmptyCountryChannels() throws FieldValueProviderException {
        when(vanMarckeVariantProductModel.getVanmarckeCountryChannel()).thenReturn(Collections.emptySet());
        valueResolver.resolve(inputDocument, indexerBatchContext, Collections.singletonList(indexedProperty), vanMarckeVariantProductModel);
        verifyZeroInteractions(inputDocument);
    }

    @Test
    public void testAddFieldValuesWhenNoCountry() throws FieldValueProviderException {
        VanmarckeCountryChannelModel countryChannelModel = mock(VanmarckeCountryChannelModel.class);
        when(vanMarckeVariantProductModel.getVanmarckeCountryChannel()).thenReturn(singleton(countryChannelModel));
        valueResolver.resolve(inputDocument, indexerBatchContext, Collections.singletonList(indexedProperty), vanMarckeVariantProductModel);
        verifyZeroInteractions(inputDocument);
    }

    @Test
    public void testAddFieldValuesWhenNoActiveChannels() throws FieldValueProviderException {
        VanmarckeCountryChannelModel countryChannelModel = mock(VanmarckeCountryChannelModel.class);
        CountryModel countryModel = mock(CountryModel.class);
        when(vanMarckeVariantProductModel.getVanmarckeCountryChannel()).thenReturn(singleton(countryChannelModel));
        when(countryChannelModel.getCountry()).thenReturn(countryModel);
        valueResolver.resolve(inputDocument, indexerBatchContext, Collections.singletonList(indexedProperty), vanMarckeVariantProductModel);
        verifyZeroInteractions(inputDocument);
    }

    @Test
    public void testAddFieldValuesWhenActiveB2BChannel() throws FieldValueProviderException {
        VanmarckeCountryChannelModel countryChannelModel = mock(VanmarckeCountryChannelModel.class);
        CountryModel countryModel = mock(CountryModel.class);
        when(vanMarckeVariantProductModel.getVanmarckeCountryChannel()).thenReturn(singleton(countryChannelModel));
        when(countryChannelModel.getCountry()).thenReturn(countryModel);
        when(countryModel.getIsocode()).thenReturn("BE");
        when(countryChannelModel.getChannels()).thenReturn(singleton(SiteChannel.B2B));
        valueResolver.resolve(inputDocument, indexerBatchContext, Collections.singletonList(indexedProperty), vanMarckeVariantProductModel);
        verify(inputDocument).addField(indexedProperty, true, "be_b2b");
    }
}