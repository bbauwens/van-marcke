package com.vanmarcke.services.factories.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.commerceservices.strategies.DeliveryAddressesLookupStrategy;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.site.BaseSiteService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.HashMap;
import java.util.Map;

import static de.hybris.platform.commerceservices.enums.SiteChannel.B2B;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKDeliveryAddressLookupStrategyFactoryImplTest {

    @Mock
    private DeliveryAddressesLookupStrategy defaultDeliveryAddressesLookupStrategy;
    @Mock
    private DeliveryAddressesLookupStrategy blueDeliveryAddressesLookupStrategy;
    @Mock
    private BaseSiteService baseSiteService;
    @Mock
    private BaseSiteModel siteModel;

    @InjectMocks
    private VMKDeliveryAddressLookupStrategyFactoryImpl defaultDeliveryAddressLookupStrategyFactory;

    @Before
    public void setup() {
        final Map<SiteChannel, DeliveryAddressesLookupStrategy> factoryConfigurationMap = new HashMap<>();
        factoryConfigurationMap.put(B2B, this.blueDeliveryAddressesLookupStrategy);
        this.defaultDeliveryAddressLookupStrategyFactory.setFactoryConfigurationMap(factoryConfigurationMap);
        this.defaultDeliveryAddressLookupStrategyFactory.setDefaultConfiguration(defaultDeliveryAddressesLookupStrategy);

        when(this.baseSiteService.getCurrentBaseSite()).thenReturn(this.siteModel);
    }

    @Test
    public void testGetDeliveryAddressesForOrder() {
        final AbstractOrderModel abstractOrder = mock(AbstractOrderModel.class);

        when(this.siteModel.getChannel()).thenReturn(B2B);

        this.defaultDeliveryAddressLookupStrategyFactory.getDeliveryAddressesForOrder(abstractOrder, true);

        verify(this.blueDeliveryAddressesLookupStrategy).getDeliveryAddressesForOrder(abstractOrder, true);
        verifyZeroInteractions(this.defaultDeliveryAddressesLookupStrategy);
    }
}