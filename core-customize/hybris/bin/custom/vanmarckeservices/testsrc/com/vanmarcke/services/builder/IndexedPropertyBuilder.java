package com.vanmarcke.services.builder;

import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;

public class IndexedPropertyBuilder {

    private String name;
    private boolean facet;
    private ClassAttributeAssignmentModel classAttributeAssignment;

    private IndexedPropertyBuilder() {}

    public static IndexedPropertyBuilder anIndexedProperty() {
        return new IndexedPropertyBuilder();
    }

    public IndexedPropertyBuilder withName(final String name) {
        this.name = name;
        return this;
    }

    public IndexedPropertyBuilder withFacet(final boolean facet) {
        this.facet = facet;
        return this;
    }

    public IndexedPropertyBuilder withClassAttributeAssignment(final ClassAttributeAssignmentModel classAttributeAssignment) {
        this.classAttributeAssignment = classAttributeAssignment;
        return this;
    }

    public IndexedProperty build() {
        final IndexedProperty indexedProperty = new IndexedProperty();
        indexedProperty.setName(name);
        indexedProperty.setFacet(facet);
        indexedProperty.setClassAttributeAssignment(classAttributeAssignment);
        return indexedProperty;
    }
}
