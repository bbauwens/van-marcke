package com.vanmarcke.services.job;

import be.elision.mandrillextension.service.MandrillService;
import com.vanmarcke.core.model.VMKSendFailedOrdersNotificationCronJobModel;
import com.vanmarcke.core.order.VMKOrderDao;
import com.vanmarcke.email.converters.impl.MandrillFailedOrdersConverter;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import org.junit.Before;
import org.junit.Test;

import java.util.Collections;
import java.util.List;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@UnitTest
public class VMKSendFailedOrdersNotificationJobPerformableTest {

    private VMKSendFailedOrdersNotificationJobPerformable vmkSendFailedOrdersNotificationJobPerformable;

    private VMKOrderDao orderDao;
    private MandrillService mandrillService;
    private MandrillFailedOrdersConverter mandrillFailedOrdersConverter;
    private String templateName;
    private String emailAddress;

    @Before
    public void setUp() {
        orderDao = mock(VMKOrderDao.class);
        mandrillService = mock(MandrillService.class);
        mandrillFailedOrdersConverter = mock(MandrillFailedOrdersConverter.class);
        templateName = "template";
        emailAddress = "email";
        vmkSendFailedOrdersNotificationJobPerformable = new VMKSendFailedOrdersNotificationJobPerformable(orderDao, mandrillService, mandrillFailedOrdersConverter, templateName, emailAddress);
    }

    @Test
    public void performDoesNotSendEmailWhenThereAreNoFailedOrders() {
        when(orderDao.findFailedOrders()).thenReturn(Collections.emptyList());

        PerformResult performResult = vmkSendFailedOrdersNotificationJobPerformable.perform(mock(VMKSendFailedOrdersNotificationCronJobModel.class));

        verifyZeroInteractions(mandrillService);
        assertThat(performResult.getResult()).isEqualTo(CronJobResult.SUCCESS);
        assertThat(performResult.getStatus()).isEqualTo(CronJobStatus.FINISHED);
    }

    @Test
    public void performSendsEmailForFailedOrders() throws Exception {
        List<OrderModel> order = mock(List.class);
        when(orderDao.findFailedOrders()).thenReturn(order);

        PerformResult performResult = vmkSendFailedOrdersNotificationJobPerformable.perform(mock(VMKSendFailedOrdersNotificationCronJobModel.class));

        verify(mandrillService).send(templateName, order, mandrillFailedOrdersConverter, emailAddress);
        assertThat(performResult.getResult()).isEqualTo(CronJobResult.SUCCESS);
        assertThat(performResult.getStatus()).isEqualTo(CronJobStatus.FINISHED);
    }

    @Test
    public void performReturnsErrorResult() throws Exception {
        List<OrderModel> order = mock(List.class);
        when(orderDao.findFailedOrders()).thenReturn(order);

        doThrow(new RuntimeException()).when(mandrillService).send(templateName, order, mandrillFailedOrdersConverter, emailAddress);
        PerformResult performResult = vmkSendFailedOrdersNotificationJobPerformable.perform(mock(VMKSendFailedOrdersNotificationCronJobModel.class));

        assertThat(performResult.getResult()).isEqualTo(CronJobResult.ERROR);
        assertThat(performResult.getStatus()).isEqualTo(CronJobStatus.ABORTED);
    }
}