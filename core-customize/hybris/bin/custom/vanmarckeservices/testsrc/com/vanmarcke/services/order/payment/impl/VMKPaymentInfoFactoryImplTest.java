package com.vanmarcke.services.order.payment.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.order.CommercePaymentProviderStrategy;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.payment.InvoicePaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;
import de.hybris.platform.servicelayer.model.ModelService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static de.hybris.platform.b2bacceleratorservices.enums.CheckoutPaymentType.ACCOUNT;
import static de.hybris.platform.b2bacceleratorservices.enums.CheckoutPaymentType.CARD;
import static java.util.Collections.singletonMap;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKPaymentInfoFactoryImplTest {

    @Mock
    private CommercePaymentProviderStrategy commercePaymentProviderStrategy;
    @Mock
    private KeyGenerator paymentInfoKeyGenerator;
    @Mock
    private ModelService modelService;

    @InjectMocks
    private VMKPaymentInfoFactoryImpl defaultPaymentInfoFactory;

    @Before
    public void setup() {
        defaultPaymentInfoFactory.setPaymentInfoMap(singletonMap("Saferpay", "SaferpayPaymentInfo"));
    }

    @Test
    public void testCreatePaymentInfoForOrder_withAccount() {
        AddressModel addressModel = mock(AddressModel.class);
        AddressModel clonedAddressModel = mock(AddressModel.class);
        CartModel cartModel = mock(CartModel.class);
        UserModel userModel = mock(UserModel.class);
        InvoicePaymentInfoModel paymentInfoModel = mock(InvoicePaymentInfoModel.class);

        when(cartModel.getPaymentType()).thenReturn(ACCOUNT);
        when(cartModel.getUser()).thenReturn(userModel);
        when(cartModel.getPaymentAddress()).thenReturn(addressModel);

        when(paymentInfoKeyGenerator.generateFor(cartModel)).thenReturn("paymentinfo-key");

        when(modelService.create(InvoicePaymentInfoModel.class)).thenReturn(paymentInfoModel);

        when(modelService.clone(addressModel)).thenReturn(clonedAddressModel);

        PaymentInfoModel result = defaultPaymentInfoFactory.createPaymentInfoForOrder(cartModel);
        assertThat(result).isEqualTo(paymentInfoModel);

        verify(clonedAddressModel).setOwner(paymentInfoModel);

        verify(paymentInfoModel).setUser(userModel);
        verify(paymentInfoModel).setCode("paymentinfo-key");
        verify(paymentInfoModel).setBillingAddress(clonedAddressModel);

        verify(modelService).save(paymentInfoModel);

        verifyZeroInteractions(commercePaymentProviderStrategy);
    }

    @Test
    public void testCreatePaymentInfoForOrder_withCardAndPaymentProvider() {
        AddressModel addressModel = mock(AddressModel.class);
        AddressModel clonedAddressModel = mock(AddressModel.class);
        CartModel cartModel = mock(CartModel.class);
        UserModel userModel = mock(UserModel.class);
        PaymentInfoModel paymentInfoModel = mock(PaymentInfoModel.class);

        when(cartModel.getPaymentType()).thenReturn(CARD);
        when(cartModel.getUser()).thenReturn(userModel);
        when(cartModel.getPaymentAddress()).thenReturn(addressModel);

        when(commercePaymentProviderStrategy.getPaymentProvider()).thenReturn("Saferpay");

        when(paymentInfoKeyGenerator.generateFor(cartModel)).thenReturn("paymentinfo-key");

        when(modelService.create("SaferpayPaymentInfo")).thenReturn(paymentInfoModel);

        when(modelService.clone(addressModel)).thenReturn(clonedAddressModel);

        PaymentInfoModel result = defaultPaymentInfoFactory.createPaymentInfoForOrder(cartModel);
        assertThat(result).isEqualTo(paymentInfoModel);

        verify(clonedAddressModel).setOwner(paymentInfoModel);

        verify(paymentInfoModel).setUser(userModel);
        verify(paymentInfoModel).setCode("paymentinfo-key");
        verify(paymentInfoModel).setBillingAddress(clonedAddressModel);

        verify(modelService).save(paymentInfoModel);
    }

    @Test
    public void testCreatePaymentInfoForOrder_withCardAndDefaultPaymentInfo() {
        AddressModel addressModel = mock(AddressModel.class);
        AddressModel clonedAddressModel = mock(AddressModel.class);
        CartModel cartModel = mock(CartModel.class);
        UserModel userModel = mock(UserModel.class);
        PaymentInfoModel paymentInfoModel = mock(PaymentInfoModel.class);

        when(cartModel.getPaymentType()).thenReturn(CARD);
        when(cartModel.getUser()).thenReturn(userModel);
        when(cartModel.getPaymentAddress()).thenReturn(addressModel);

        when(commercePaymentProviderStrategy.getPaymentProvider()).thenReturn("Mockup");

        when(paymentInfoKeyGenerator.generateFor(cartModel)).thenReturn("paymentinfo-key");

        when(modelService.create("PaymentInfo")).thenReturn(paymentInfoModel);

        when(modelService.clone(addressModel)).thenReturn(clonedAddressModel);

        PaymentInfoModel result = defaultPaymentInfoFactory.createPaymentInfoForOrder(cartModel);
        assertThat(result).isEqualTo(paymentInfoModel);

        verify(clonedAddressModel).setOwner(paymentInfoModel);

        verify(paymentInfoModel).setUser(userModel);
        verify(paymentInfoModel).setCode("paymentinfo-key");
        verify(paymentInfoModel).setBillingAddress(clonedAddressModel);

        verify(modelService).save(paymentInfoModel);
    }

}