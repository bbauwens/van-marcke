package com.vanmarcke.services.commercegroup.impl;

import com.vanmarcke.core.model.CommerceGroupModel;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@UnitTest
public class VMKCommerceGroupServiceImplTest {

    @Mock
    private CMSSiteService cmsSiteService;

    @InjectMocks
    private VMKCommerceGroupServiceImpl defaultVMKCommerceGroupService;

    @Test
    public void testGetCurrentCommerceGroup_withoutCurrentSite() throws Exception {
        CommerceGroupModel result = defaultVMKCommerceGroupService.getCurrentCommerceGroup();

        assertThat(result).isNull();
    }

    @Test
    public void testGetCurrentCommerceGroup() throws Exception {
        CMSSiteModel cmsSiteModel = mock(CMSSiteModel.class);
        CommerceGroupModel commerceGroupModel = mock(CommerceGroupModel.class);

        when(cmsSiteModel.getCommerceGroup()).thenReturn(commerceGroupModel);

        when(cmsSiteService.getCurrentSite()).thenReturn(cmsSiteModel);

        CommerceGroupModel result = defaultVMKCommerceGroupService.getCurrentCommerceGroup();

        assertThat(result).isEqualTo(commerceGroupModel);
    }

}