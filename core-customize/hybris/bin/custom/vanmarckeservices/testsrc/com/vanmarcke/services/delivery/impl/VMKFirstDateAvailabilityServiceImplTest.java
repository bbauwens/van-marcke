package com.vanmarcke.services.delivery.impl;

import com.vanmarcke.services.basestore.VMKBaseStoreService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.store.BaseStoreModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKFirstDateAvailabilityServiceImplTest {

    @Mock
    private VMKBaseStoreService baseStoreService;

    @InjectMocks
    VMKFirstDateAvailabilityServiceImpl vmkFirstDateAvailabilityService;

    @Test
    public void testGetFirstPossibleDate() throws ParseException {
        // given
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date date = format.parse("2021-12-31");
        BaseStoreModel baseStoreModel = mock(BaseStoreModel.class);
        when(baseStoreService.getCurrentBaseStore()).thenReturn(baseStoreModel);
        when(baseStoreModel.getClosingDates()).thenReturn(List.of(date));

        // when
        Date newProposedDate = vmkFirstDateAvailabilityService.getFirstValidNoClosingDate(date);

        // then
        assertThat(newProposedDate).isEqualTo(format.parse("2022-01-03"));
    }
}