package com.vanmarcke.services.cronjob;

import com.vanmarcke.core.model.ProductExportCronJobModel;
import com.vanmarcke.services.ftp.FTPClient;
import com.vanmarcke.services.product.VMKNonArchivedVariantsExportService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import static java.util.Collections.emptyList;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKNonArchivedVariantsExportJobPerformableTest {

    @Mock
    private VMKNonArchivedVariantsExportService vmkNonArchivedVariantsExportService;
    @Mock
    private FTPClient ftpClient;
    @InjectMocks
    private VMKNonArchivedVariantsExportJobPerformable vmkNonArchivedVariantsExportJobPerformable;

    @Test
    public void testPerform_NoVariants() {
        ProductExportCronJobModel job = mock(ProductExportCronJobModel.class);
        CatalogVersionModel catalogVersion = mock(CatalogVersionModel.class);

        when(job.getCatalogVersion()).thenReturn(catalogVersion);
        when(vmkNonArchivedVariantsExportService.generateNonArchivedVariantsExportFeed(catalogVersion)).thenReturn(emptyList());

        PerformResult result = vmkNonArchivedVariantsExportJobPerformable.perform(job);

        verify(vmkNonArchivedVariantsExportService).generateNonArchivedVariantsExportFeed(catalogVersion);
        verifyZeroInteractions(ftpClient);
        assertThat(result.getResult()).isEqualTo(CronJobResult.SUCCESS);
        assertThat(result.getStatus()).isEqualTo(CronJobStatus.FINISHED);
    }

    @Test
    public void testPerform_IssueSendingToFTP() throws Exception {
        ProductExportCronJobModel job = mock(ProductExportCronJobModel.class);
        CatalogVersionModel catalogVersion = mock(CatalogVersionModel.class);
        List<String> exportLines = new ArrayList<>();
        String exportLine = "variantCode;baseProductCode;superCatCode;Nederlandse supercategorie naam;E0001;Nederlandse Etim Classificatie naam;;";
        exportLines.add(exportLine);

        when(job.getCatalogVersion()).thenReturn(catalogVersion);
        when(vmkNonArchivedVariantsExportService.generateNonArchivedVariantsExportFeed(catalogVersion)).thenReturn(exportLines);
        doThrow(IOException.class).when(ftpClient).send(any(InputStream.class), any(String.class));

        PerformResult result = vmkNonArchivedVariantsExportJobPerformable.perform(job);

        verify(vmkNonArchivedVariantsExportService).generateNonArchivedVariantsExportFeed(catalogVersion);
        verify(ftpClient).send(any(InputStream.class), any(String.class));
        assertThat(result.getResult()).isEqualTo(CronJobResult.ERROR);
        assertThat(result.getStatus()).isEqualTo(CronJobStatus.ABORTED);
    }

    @Test
    public void testPerform() throws Exception {
        ProductExportCronJobModel job = mock(ProductExportCronJobModel.class);
        CatalogVersionModel catalogVersion = mock(CatalogVersionModel.class);
        List<String> exportLines = new ArrayList<>();
        String exportLine = "variantCode;baseProductCode;superCatCode;Nederlandse supercategorie naam;E0001;Nederlandse Etim Classificatie naam;;";
        exportLines.add(exportLine);

        when(job.getCatalogVersion()).thenReturn(catalogVersion);
        when(vmkNonArchivedVariantsExportService.generateNonArchivedVariantsExportFeed(catalogVersion)).thenReturn(exportLines);

        PerformResult result = vmkNonArchivedVariantsExportJobPerformable.perform(job);

        verify(vmkNonArchivedVariantsExportService).generateNonArchivedVariantsExportFeed(catalogVersion);
        verify(ftpClient).send(any(InputStream.class), any(String.class));
        assertThat(result.getResult()).isEqualTo(CronJobResult.SUCCESS);
        assertThat(result.getStatus()).isEqualTo(CronJobStatus.FINISHED);
    }
}