package com.vanmarcke.services.search.solrfacetsearch.populators;


import com.vanmarcke.core.store.VMKBaseStoreDao;
import com.vanmarcke.services.builder.ClassAttributeAssignmentModelBuilder;
import com.vanmarcke.services.builder.IndexedPropertyBuilder;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;
import de.hybris.platform.catalog.model.classification.ClassificationSystemModel;
import de.hybris.platform.catalog.model.classification.ClassificationSystemVersionModel;
import de.hybris.platform.core.PK;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.config.IndexedTypeSort;
import de.hybris.platform.solrfacetsearch.config.SearchQueryTemplate;
import de.hybris.platform.solrfacetsearch.model.SolrSearchQueryTemplateModel;
import de.hybris.platform.solrfacetsearch.model.SolrSortModel;
import de.hybris.platform.solrfacetsearch.model.config.SolrIndexedPropertyModel;
import de.hybris.platform.solrfacetsearch.model.config.SolrIndexedTypeModel;
import de.hybris.platform.store.BaseStoreModel;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static com.vanmarcke.services.builder.BaseStoreModelBuilder.aBaseStoreModel;
import static com.vanmarcke.services.builder.ClassificationSystemModelBuilder.aCatalogModel;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * The {@code VMKIndexedTypePopulatorTest} class contains the unit tests for the
 * {@link VMKIndexedTypePopulator} class.
 *
 * @author Przemyslaw Mirowski
 * @since 26-11-2021
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKIndexedTypePopulatorTest {
    private static final String STORE_1 = "store1";
    private static final String STORE_2 = "store2";

    @Mock
    private VMKBaseStoreDao baseStoreDao;
    @Mock
    private Converter<SolrIndexedPropertyModel, IndexedProperty> indexedPropertyConverter;
    @Mock
    private Converter<SolrSortModel, IndexedTypeSort> indexedTypeSortConverter;
    @Mock
    private Converter<SolrSearchQueryTemplateModel, SearchQueryTemplate> solrSearchQueryTemplateConverter;

    private VMKIndexedTypePopulator indexedTypePopulator;

    @Before
    public void setUp() {
        indexedTypePopulator = new VMKIndexedTypePopulator(baseStoreDao);
        indexedTypePopulator.setIndexedTypeSortConverter(indexedTypeSortConverter);
        indexedTypePopulator.setIndexedPropertyConverter(indexedPropertyConverter);
        indexedTypePopulator.setSolrSearchQueryTemplateConverter(solrSearchQueryTemplateConverter);
    }

    @Test
    public void testPopulateFacetsByBaseStore() {
        //given
        SolrIndexedTypeModel solrIndexedTypeModel = mock(SolrIndexedTypeModel.class);
        ClassificationSystemVersionModel classification1 = mock(ClassificationSystemVersionModel.class);
        ClassificationSystemVersionModel classification2 = mock(ClassificationSystemVersionModel.class);
        PK classification1Pk = PK.fromLong(1L);
        PK classification2Pk = PK.fromLong(2L);
        ClassificationSystemModel catalog1 = aCatalogModel()
                .withActiveCatalogVersion(classification1)
                .build();
        ClassificationSystemModel catalog2 = aCatalogModel()
                .withActiveCatalogVersion(classification2)
                .build();
        ClassAttributeAssignmentModel classAttributeAssignment1 = ClassAttributeAssignmentModelBuilder.aClassAttributeAssignmentModel()
                .withSystemVersion(classification1)
                .build();
        ClassAttributeAssignmentModel classAttributeAssignment2 = ClassAttributeAssignmentModelBuilder.aClassAttributeAssignmentModel()
                .withSystemVersion(classification2)
                .build();
        IndexedProperty indexedProperty1 = IndexedPropertyBuilder.anIndexedProperty()
                .withName("indexedProperty1")
                .withFacet(true)
                .withClassAttributeAssignment(classAttributeAssignment1)
                .build();
        IndexedProperty indexedProperty2 = IndexedPropertyBuilder.anIndexedProperty()
                .withName("indexedProperty2")
                .withFacet(true)
                .withClassAttributeAssignment(classAttributeAssignment2)
                .build();
        IndexedProperty indexedProperty3 = IndexedPropertyBuilder.anIndexedProperty()
                .withName("indexedProperty3")
                .withFacet(true)
                .build();
        SolrIndexedPropertyModel indexedPropertyModel1 = new SolrIndexedPropertyModel();
        SolrIndexedPropertyModel indexedPropertyModel2 = new SolrIndexedPropertyModel();
        SolrIndexedPropertyModel indexedPropertyModel3 = new SolrIndexedPropertyModel();
        BaseStoreModel store1 = aBaseStoreModel()
                .withUid(STORE_1)
                .withCatalogs(List.of(catalog1, catalog2))
                .build();
        BaseStoreModel store2 = aBaseStoreModel()
                .withUid(STORE_2)
                .withCatalogs(List.of(catalog2))
                .build();
        when(baseStoreDao.findAllStoresWithSolrFacetSearchConfig()).thenReturn(List.of(store1, store2));
        when(indexedPropertyConverter.convert(indexedPropertyModel1)).thenReturn(indexedProperty1);
        when(indexedPropertyConverter.convert(indexedPropertyModel2)).thenReturn(indexedProperty2);
        when(indexedPropertyConverter.convert(indexedPropertyModel3)).thenReturn(indexedProperty3);
        when(classification1.getPk()).thenReturn(classification1Pk);
        when(classification2.getPk()).thenReturn(classification2Pk);
        when(classification1.getCatalog()).thenReturn(catalog1);
        when(classification2.getCatalog()).thenReturn(catalog2);
        when(solrIndexedTypeModel.getSolrIndexedProperties()).thenReturn(List.of(indexedPropertyModel1, indexedPropertyModel2, indexedPropertyModel3));
        when(solrIndexedTypeModel.getSearchQueryTemplates()).thenReturn(Collections.emptyList());
        IndexedType indexedType = new IndexedType();
        indexedType.setIndexedProperties(Map.of(
                "indexedProperty1", indexedProperty1,
                "indexedProperty2", indexedProperty2,
                "indexedProperty3", indexedProperty3));

        //when
        indexedTypePopulator.populate(solrIndexedTypeModel, indexedType);

        //then
        assertThat(indexedType.getFacetsByBaseStore())
                .isNotEmpty()
                .hasSize(2)
                .containsKey(STORE_1)
                .containsKey(STORE_2)
                .containsValue(List.of(indexedProperty1, indexedProperty2, indexedProperty3))
                .containsValue(List.of(indexedProperty2, indexedProperty3));
    }
}
