package com.vanmarcke.services.order.cart.impl;

import com.vanmarcke.core.model.CommerceGroupModel;
import com.vanmarcke.services.b2b.VMKB2BUnitService;
import com.vanmarcke.services.model.builder.*;
import com.vanmarcke.services.product.price.VMKPriceService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.product.data.PriceCalculationData;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.order.strategies.calculation.FindPriceStrategy;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.util.PriceValue;
import de.hybris.platform.util.TaxValue;
import de.hybris.platform.variants.model.VariantProductModel;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.fest.assertions.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.List;
import java.util.Locale;

import static org.mockito.Mockito.*;

/**
 * The {@code VMKCalculationServiceImplTest} class contains the unit tests for the
 * {@link VMKCalculationServiceImpl} class.
 * <p>
 * This is necessary, because price are personalized and stored in the ERP.
 *
 * @author Niels Raemaekers, Taki Korovessis, Christiaan Janssen
 * @since 26-04-2020
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKCalculationServiceImplTest {

    private static final String CURRENCY_ISO = RandomStringUtils.random(10);
    private static final String PRODUCT_CODE = RandomStringUtils.random(10);
    private static final String B2B_UNIT_UID = RandomStringUtils.random(10);
    private static final String ANONYMOUS_B2B_UNIT_UID = "V99909";
    private static final BigDecimal PRICE = BigDecimal.valueOf(RandomUtils.nextDouble());
    private static final BigDecimal STANDARD_VAT = BigDecimal.valueOf(20);

    @Mock
    private VMKPriceService bluePriceService;

    @Mock
    private UserService userService;

    @Mock
    private VMKB2BUnitService b2bUnitService;

    @Mock
    private CMSSiteService cmsSiteService;

    @Mock
    private FindPriceStrategy findPriceStrategy;

    @Spy
    @InjectMocks
    private VMKCalculationServiceImpl vmkCalculationService;

    @Test
    public void testFindBasePriceWithoutPrice() throws CalculationException {
        CurrencyModel currency = CurrencyModelMockBuilder
                .aCurrency(CURRENCY_ISO)
                .build();

        B2BUnitModel b2bUnit = B2BUnitModelMockBuilder
                .aB2BUnit(B2B_UNIT_UID)
                .build();

        UserModel userModel = UserModelMockBuilder
                .aUser()
                .withShowNetPrice(true)
                .build();

        AbstractOrderModel order = AbstractOrderModelMockBuilder
                .anAbstractOrder()
                .withCurrency(currency)
                .withUnit(b2bUnit)
                .withUser(userModel)
                .withNet()
                .build();

        VariantProductModel variant = VariantProductModelMockBuilder
                .aVariantProduct(PRODUCT_CODE, Locale.CANADA)
                .build();

        AbstractOrderEntryModel entry = AbstractOrderEntryModelMockBuilder
                .anAbstractOrderEntry()
                .withOrder(order)
                .withProduct(variant)
                .build();


        when(userService.getCurrentUser()).thenReturn(userModel);
        when(userService.isAnonymousUser(userModel)).thenReturn(false);

        PriceValue expectedPrice = vmkCalculationService.findBasePrice(entry);

        Assertions
                .assertThat(expectedPrice)
                .isNull();

        verify(bluePriceService).getPrice(variant, B2B_UNIT_UID);
    }

    @Test
    public void testFindBasePriceWithPrice() throws CalculationException {
        CurrencyModel currency = CurrencyModelMockBuilder
                .aCurrency(CURRENCY_ISO)
                .build();

        B2BUnitModel b2bUnit = B2BUnitModelMockBuilder
                .aB2BUnit(B2B_UNIT_UID)
                .build();

        UserModel userModel = UserModelMockBuilder
                .aUser()
                .withShowNetPrice(true)
                .build();

        AbstractOrderModel order = AbstractOrderModelMockBuilder
                .anAbstractOrder()
                .withCurrency(currency)
                .withUnit(b2bUnit)
                .withUser(userModel)
                .withNet()
                .build();

        VariantProductModel variant = VariantProductModelMockBuilder
                .aVariantProduct(PRODUCT_CODE, Locale.CANADA)
                .build();

        AbstractOrderEntryModel entry = AbstractOrderEntryModelMockBuilder
                .anAbstractOrderEntry()
                .withOrder(order)
                .withProduct(variant)
                .build();

        PriceCalculationData priceCalculationData = mock(PriceCalculationData.class);
        when(priceCalculationData.getPriceValue()).thenReturn(PRICE);
        when(bluePriceService.getPrice(variant, B2B_UNIT_UID)).thenReturn(priceCalculationData);

        PriceValue expectedPrice = vmkCalculationService.findBasePrice(entry);

        Assertions
                .assertThat(expectedPrice)
                .isNotNull();

        Assertions
                .assertThat(expectedPrice.getCurrencyIso())
                .isEqualTo(CURRENCY_ISO);

        Assertions
                .assertThat(expectedPrice.getValue())
                .isEqualTo(PRICE.doubleValue());

        Assertions
                .assertThat(expectedPrice.isNet())
                .isTrue();

        verify(bluePriceService).getPrice(variant, B2B_UNIT_UID);
    }

    @Test
    public void testFindBasePriceWithPrice_isDIY() throws CalculationException {
        CurrencyModel currency = CurrencyModelMockBuilder
                .aCurrency(CURRENCY_ISO)
                .build();

        B2BUnitModel b2bUnit = B2BUnitModelMockBuilder
                .aB2BUnit(B2B_UNIT_UID)
                .build();

        UserModel userModel = UserModelMockBuilder
                .aUser()
                .withShowNetPrice(false)
                .build();

        AbstractOrderModel order = AbstractOrderModelMockBuilder
                .anAbstractOrder()
                .withCurrency(currency)
                .withUnit(b2bUnit)
                .withUser(userModel)
                .withNet()
                .build();

        VariantProductModel variant = VariantProductModelMockBuilder
                .aVariantProduct(PRODUCT_CODE, Locale.CANADA)
                .build();

        AbstractOrderEntryModel entry = AbstractOrderEntryModelMockBuilder
                .anAbstractOrderEntry()
                .withOrder(order)
                .withProduct(variant)
                .build();

        PriceCalculationData priceCalculationData = mock(PriceCalculationData.class);
        when(priceCalculationData.getPriceValue()).thenReturn(PRICE);
        when(bluePriceService.getPrice(variant, B2B_UNIT_UID)).thenReturn(priceCalculationData);

        CMSSiteModel cmsSiteModel = mock(CMSSiteModel.class);
        when(cmsSiteModel.getChannel()).thenReturn(SiteChannel.DIY);
        when(cmsSiteService.getCurrentSite()).thenReturn(cmsSiteModel);

        PriceValue expectedPrice = vmkCalculationService.findBasePrice(entry);

        Assertions
                .assertThat(expectedPrice)
                .isNotNull();

        Assertions
                .assertThat(expectedPrice.getCurrencyIso())
                .isEqualTo(CURRENCY_ISO);

        Assertions
                .assertThat(expectedPrice.getValue())
                .isEqualTo(PRICE.doubleValue());

        Assertions
                .assertThat(expectedPrice.isNet())
                .isTrue();

        verify(bluePriceService).getPrice(variant, B2B_UNIT_UID);
    }

    @Test
    public void testFindBasePriceAnonymous() {
        UserModel userModel = UserModelMockBuilder
                .aUser()
                .build();

        CurrencyModel currency = CurrencyModelMockBuilder
                .aCurrency(CURRENCY_ISO)
                .build();

        AbstractOrderModel order = AbstractOrderModelMockBuilder
                .anAbstractOrder()
                .withUser(userModel)
                .withCurrency(currency)
                .build();

        AbstractOrderEntryModel entry = AbstractOrderEntryModelMockBuilder
                .anAbstractOrderEntry()
                .withOrder(order)
                .build();

        vmkCalculationService.setFindPriceStrategy(findPriceStrategy);

        when(userService.isAnonymousUser(userModel)).thenReturn(true);

        boolean isZeroVat = vmkCalculationService.isZeroVAT(entry);
        Assert.assertEquals(false, isZeroVat);
    }

    @Test
    public void testNonZeroVAT() {
        CurrencyModel currency = CurrencyModelMockBuilder
                .aCurrency(CURRENCY_ISO)
                .build();

        CountryModel countryModel = CountryModelMockBuilder
                .aCountry()
                .withISOCode("BE")
                .build();

        AddressModel deliveryAddress = AddressModelMockBuilder
                .anAddress()
                .withCountry(countryModel)
                .build();

        AbstractOrderModel order = AbstractOrderModelMockBuilder
                .anAbstractOrder()
                .withCurrency(currency)
                .withDeliveryAddress(deliveryAddress)
                .build();

        AbstractOrderEntryModel entry = AbstractOrderEntryModelMockBuilder
                .anAbstractOrderEntry()
                .withOrder(order)
                .build();

        CMSSiteModel cmsSite = mock(CMSSiteModel.class);
        CommerceGroupModel commerceGroupModel = mock(CommerceGroupModel.class);
        when(commerceGroupModel.getCountry()).thenReturn(countryModel);
        when(cmsSiteService.getCurrentSite()).thenReturn(cmsSite);
        when(cmsSite.getCommerceGroup()).thenReturn(commerceGroupModel);

        boolean isZeroVat = vmkCalculationService.isZeroVAT(entry);
        Assert.assertEquals(false, isZeroVat);
    }

    @Test
    public void testZeroVAT_DefaultShippingAddress() {
        CurrencyModel currency = CurrencyModelMockBuilder
                .aCurrency(CURRENCY_ISO)
                .build();

        UserModel userModel = UserModelMockBuilder
                .aUser()
                .build();

        CountryModel countryModel = CountryModelMockBuilder
                .aCountry()
                .withISOCode("BE")
                .build();

        CountryModel countryModel2 = CountryModelMockBuilder
                .aCountry()
                .withISOCode("FR")
                .build();


        AddressModel deliveryAddress = AddressModelMockBuilder
                .anAddress()
                .withCountry(countryModel)
                .build();

        AbstractOrderModel order = AbstractOrderModelMockBuilder
                .anAbstractOrder()
                .withCurrency(currency)
                .withUser(userModel)
                .withDeliveryAddress(null)
                .build();

        AbstractOrderEntryModel entry = AbstractOrderEntryModelMockBuilder
                .anAbstractOrderEntry()
                .withOrder(order)
                .build();

        B2BUnitModel b2bUnit = B2BUnitModelMockBuilder
                .aB2BUnit(B2B_UNIT_UID)
                .withShippingAddress(deliveryAddress)
                .build();




        CMSSiteModel cmsSite = mock(CMSSiteModel.class);
        CommerceGroupModel commerceGroupModel = mock(CommerceGroupModel.class);
        when(commerceGroupModel.getCountry()).thenReturn(countryModel2);
        when(cmsSiteService.getCurrentSite()).thenReturn(cmsSite);
        when(cmsSite.getCommerceGroup()).thenReturn(commerceGroupModel);
        when(b2bUnitService.getB2BUnitModelForCurrentUser()).thenReturn(b2bUnit);

        boolean isZeroVat = vmkCalculationService.isZeroVAT(entry);
        Assert.assertEquals(true, isZeroVat);
    }

    @Test
    public void testNonZeroVAT_DefaultShippingAddress() {
        CurrencyModel currency = CurrencyModelMockBuilder
                .aCurrency(CURRENCY_ISO)
                .build();

        CountryModel countryModel = CountryModelMockBuilder
                .aCountry()
                .withISOCode("BE")
                .build();

        UserModel userModel = UserModelMockBuilder
                .aUser()
                .build();

        AddressModel deliveryAddress = AddressModelMockBuilder
                .anAddress()
                .withCountry(countryModel)
                .build();

        AbstractOrderModel order = AbstractOrderModelMockBuilder
                .anAbstractOrder()
                .withCurrency(currency)
                .withUser(userModel)
                .withDeliveryAddress(null)
                .build();

        AbstractOrderEntryModel entry = AbstractOrderEntryModelMockBuilder
                .anAbstractOrderEntry()
                .withOrder(order)
                .build();

        B2BUnitModel b2bUnit = B2BUnitModelMockBuilder
                .aB2BUnit(B2B_UNIT_UID)
                .withShippingAddress(deliveryAddress)
                .build();


        CMSSiteModel cmsSite = mock(CMSSiteModel.class);
        when(cmsSiteService.getCurrentSite()).thenReturn(cmsSite);
        CommerceGroupModel commerceGroupModel = mock(CommerceGroupModel.class);
        when(commerceGroupModel.getCountry()).thenReturn(countryModel);
        when(cmsSiteService.getCurrentSite()).thenReturn(cmsSite);
        when(cmsSite.getCommerceGroup()).thenReturn(commerceGroupModel);

        when(b2bUnitService.getB2BUnitModelForCurrentUser()).thenReturn(b2bUnit);

        boolean isZeroVat = vmkCalculationService.isZeroVAT(entry);
        Assert.assertFalse(isZeroVat);
    }

    @Test
    public void testFindTaxValues_ZeroVAT() {
        CurrencyModel currency = CurrencyModelMockBuilder
                .aCurrency(CURRENCY_ISO)
                .build();

        AbstractOrderModel order = AbstractOrderModelMockBuilder
                .anAbstractOrder()
                .withCurrency(currency)
                .build();

        AbstractOrderEntryModel entry = AbstractOrderEntryModelMockBuilder
                .anAbstractOrderEntry()
                .withOrder(order)
                .build();

        doReturn(true).when(vmkCalculationService).isZeroVAT(entry);
        List<TaxValue> taxValues = (List<TaxValue>) vmkCalculationService.findTaxValues(entry);

        Assert.assertEquals(1, taxValues.size());
        Assert.assertEquals(0, (int) taxValues.get(0).getValue());
        Assert.assertFalse(taxValues.get(0).isAbsolute());
        Assert.assertEquals("VAT", taxValues.get(0).getCode());
        Assert.assertEquals(CURRENCY_ISO, taxValues.get(0).getCurrencyIsoCode());
    }

    @Test
    public void testFindTaxValues_NonZeroVAT() {
        CurrencyModel currency = CurrencyModelMockBuilder
                .aCurrency(CURRENCY_ISO)
                .build();

        B2BUnitModel b2bUnit = B2BUnitModelMockBuilder
                .aB2BUnit(B2B_UNIT_UID)
                .build();

        UserModel userModel = UserModelMockBuilder
                .aUser()
                .build();

        AbstractOrderModel order = AbstractOrderModelMockBuilder
                .anAbstractOrder()
                .withCurrency(currency)
                .withUnit(b2bUnit)
                .withNet()
                .withUser(userModel)
                .build();

        VariantProductModel variant = VariantProductModelMockBuilder
                .aVariantProduct(PRODUCT_CODE, Locale.CANADA)
                .build();

        AbstractOrderEntryModel entry = AbstractOrderEntryModelMockBuilder
                .anAbstractOrderEntry()
                .withOrder(order)
                .withProduct(variant)
                .build();


        PriceCalculationData priceCalculationData = new PriceCalculationData();
        priceCalculationData.setVat(STANDARD_VAT.intValue());

        when(b2bUnitService.getCustomerB2BUnitID(userModel)).thenReturn(B2B_UNIT_UID);
        when(bluePriceService.getPrice(variant, B2B_UNIT_UID)).thenReturn(priceCalculationData);
        doReturn(false).when(vmkCalculationService).isZeroVAT(entry);
        List<TaxValue> taxValues = (List<TaxValue>) vmkCalculationService.findTaxValues(entry);

        Assert.assertEquals(1, taxValues.size());
        Assert.assertEquals(0, BigDecimal.valueOf(taxValues.get(0).getValue()).compareTo(STANDARD_VAT));
        Assert.assertFalse(taxValues.get(0).isAbsolute());
        Assert.assertEquals("VAT", taxValues.get(0).getCode());
        Assert.assertEquals(CURRENCY_ISO, taxValues.get(0).getCurrencyIsoCode());
    }

    @Test
    public void testFindTaxValues_NonZeroVAT_AnonymousUser() {
        UserModel userModel = UserModelMockBuilder
                .aUser()
                .build();

        CurrencyModel currency = CurrencyModelMockBuilder
                .aCurrency(CURRENCY_ISO)
                .build();

        AbstractOrderModel order = AbstractOrderModelMockBuilder
                .anAbstractOrder()
                .withCurrency(currency)
                .withUser(userModel)
                .build();

        VariantProductModel variant = VariantProductModelMockBuilder
                .aVariantProduct(PRODUCT_CODE, Locale.CANADA)
                .build();

        AbstractOrderEntryModel entry = AbstractOrderEntryModelMockBuilder
                .anAbstractOrderEntry()
                .withOrder(order)
                .withProduct(variant)
                .build();

        when(userService.getCurrentUser()).thenReturn(userModel);
        when(userService.isAnonymousUser(userModel)).thenReturn(true);
        when(b2bUnitService.getCustomerB2BUnitID(userModel)).thenReturn(ANONYMOUS_B2B_UNIT_UID);
        PriceCalculationData priceCalculationData = mock(PriceCalculationData.class);
        when(priceCalculationData.getPriceValue()).thenReturn(PRICE);
        when(bluePriceService.getPrice(variant, ANONYMOUS_B2B_UNIT_UID)).thenReturn(priceCalculationData);

        vmkCalculationService.findTaxValues(entry);
        verify(vmkCalculationService).isZeroVAT(entry);
        verify(bluePriceService).getPrice(variant, ANONYMOUS_B2B_UNIT_UID);
    }
}