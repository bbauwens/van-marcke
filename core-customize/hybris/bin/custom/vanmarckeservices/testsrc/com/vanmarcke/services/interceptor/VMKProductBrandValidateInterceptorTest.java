package com.vanmarcke.services.interceptor;

import com.vanmarcke.core.model.BrandCategoryModel;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.variants.model.VariantProductModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKProductBrandValidateInterceptorTest {

    @InjectMocks
    private VMKProductBrandValidateInterceptor vmkProductBrandValidateInterceptor;

    @Test
    public void testOnValidate_withoutModificationsOnSuperCategoriesField() throws Exception {
        ProductModel productModel = mock(ProductModel.class);
        InterceptorContext ctx = mock(InterceptorContext.class);

        when(ctx.isModified(productModel, ProductModel.SUPERCATEGORIES)).thenReturn(false);

        vmkProductBrandValidateInterceptor.onValidate(productModel, ctx);

        verifyZeroInteractions(productModel);
    }

    @Test
    public void testOnValidate_withBaseProductWithoutBrands() throws Exception {
        ProductModel productModel = mock(ProductModel.class);
        InterceptorContext ctx = mock(InterceptorContext.class);

        when(ctx.isModified(productModel, ProductModel.SUPERCATEGORIES)).thenReturn(true);

        vmkProductBrandValidateInterceptor.onValidate(productModel, ctx);
    }

    @Test(expected = InterceptorException.class)
    public void testOnValidate_withBaseProductWithBrand() throws Exception {
        BrandCategoryModel brandCategoryModel = mock(BrandCategoryModel.class);
        ProductModel productModel = mock(ProductModel.class);
        InterceptorContext ctx = mock(InterceptorContext.class);

        when(ctx.isModified(productModel, ProductModel.SUPERCATEGORIES)).thenReturn(true);

        when(productModel.getSupercategories()).thenReturn(singletonList(brandCategoryModel));

        vmkProductBrandValidateInterceptor.onValidate(productModel, ctx);
    }

    @Test
    public void testOnValidate_withVariantProductWithoutBrands() throws Exception {
        VariantProductModel productModel = mock(VariantProductModel.class);
        InterceptorContext ctx = mock(InterceptorContext.class);

        when(ctx.isModified(productModel, ProductModel.SUPERCATEGORIES)).thenReturn(true);

        vmkProductBrandValidateInterceptor.onValidate(productModel, ctx);
    }

    @Test
    public void testOnValidate_withVariantProductWithBrand() throws Exception {
        BrandCategoryModel brandCategoryModel = mock(BrandCategoryModel.class);
        CategoryModel categoryModel = mock(CategoryModel.class);
        VariantProductModel productModel = mock(VariantProductModel.class);
        InterceptorContext ctx = mock(InterceptorContext.class);

        when(ctx.isModified(productModel, ProductModel.SUPERCATEGORIES)).thenReturn(true);

        when(productModel.getSupercategories()).thenReturn(asList(brandCategoryModel, categoryModel));

        vmkProductBrandValidateInterceptor.onValidate(productModel, ctx);
    }

    @Test(expected = InterceptorException.class)
    public void testOnValidate_withVariantProductWithMultipleBrands() throws Exception {
        BrandCategoryModel brandCategoryModel1 = mock(BrandCategoryModel.class);
        BrandCategoryModel brandCategoryModel2 = mock(BrandCategoryModel.class);
        VariantProductModel productModel = mock(VariantProductModel.class);
        InterceptorContext ctx = mock(InterceptorContext.class);

        when(ctx.isModified(productModel, ProductModel.SUPERCATEGORIES)).thenReturn(true);

        when(productModel.getSupercategories()).thenReturn(asList(brandCategoryModel1, brandCategoryModel2));

        vmkProductBrandValidateInterceptor.onValidate(productModel, ctx);
    }

}