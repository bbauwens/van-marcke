package com.vanmarcke.services.sitemap.renderer;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.enums.SiteMapPageEnum;
import de.hybris.platform.acceleratorservices.model.SiteMapConfigModel;
import de.hybris.platform.acceleratorservices.urlencoder.UrlEncoderService;
import de.hybris.platform.acceleratorservices.urlresolver.SiteBaseUrlResolutionService;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKSiteMapContextTest {

    @Mock
    private UrlEncoderService urlEncoderService;
    @Mock
    private SiteBaseUrlResolutionService siteBaseUrlResolutionService;
    @InjectMocks
    private VMKSiteMapContext vmkSiteMapContext;

    @Test
    public void test() {
        final CMSSiteModel site = mock(CMSSiteModel.class);
        final SiteMapConfigModel config = mock(SiteMapConfigModel.class);
        final SiteMapPageEnum siteMapPageEnum = SiteMapPageEnum.PRODUCT;
        final String currentUrlEncodingPattern = "/nl_BE";

        when(urlEncoderService.getCurrentUrlEncodingPattern()).thenReturn(currentUrlEncodingPattern);
        when(siteBaseUrlResolutionService.getWebsiteUrlForSite(site, currentUrlEncodingPattern, true, "")).thenReturn("https://blue.vmk.com:9002/nl_BE");
        when(siteBaseUrlResolutionService.getMediaUrlForSite(site, true, "")).thenReturn("https://blue.vmk.com:9002");
        when(site.getSiteMapConfig()).thenReturn(config);
        when(config.getSiteMapPages()).thenReturn(Collections.emptyList());
        vmkSiteMapContext.init(site, siteMapPageEnum);
        
        assertThat(vmkSiteMapContext.get("baseUrl").toString()).isEqualTo("https://blue.vmk.com:9002/nl_BE");
        assertThat(vmkSiteMapContext.get("mediaUrl").toString()).isEqualTo("https://blue.vmk.com:9002");
    }
}
