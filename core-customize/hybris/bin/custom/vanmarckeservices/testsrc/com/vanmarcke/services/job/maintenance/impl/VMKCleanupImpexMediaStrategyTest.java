package com.vanmarcke.services.job.maintenance.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKCleanupImpexMediaStrategyTest {

    private VMKCleanupImpexMediaStrategy cleanupStrategy = new VMKCleanupImpexMediaStrategy();

    @Test
    public void testGetQuery() {
        String result = cleanupStrategy.getQuery();
        assertThat(result).isEqualTo("SELECT {pk} FROM {ImpExMedia} WHERE {modifiedtime} < ?date");
    }

}