package com.vanmarcke.services.order.hook.impl;

import com.vanmarcke.services.address.VMKPreferredPaymentAddressLookupStrategy;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.model.ModelService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKPreferredPaymentAddressCartCalculationMethodHookImplTest {

    @Mock
    private VMKPreferredPaymentAddressLookupStrategy bluePreferredPaymentAddressLookupStrategy;
    @Mock
    private ModelService modelService;

    @InjectMocks
    private VMKPreferredPaymentAddressCartCalculationMethodHookImpl bluePreferredPaymentAddressCartCalculationMethodHook;

    @Test
    public void testBeforeCalculate_isB2BCart() {
        final CommerceCartParameter parameter = mock(CommerceCartParameter.class);
        final CartModel cart = mock(CartModel.class);
        final B2BCustomerModel customer = mock(B2BCustomerModel.class);

        when(parameter.getCart()).thenReturn(cart);
        when(cart.getUser()).thenReturn(customer);
        when(this.bluePreferredPaymentAddressLookupStrategy.getPreferredPaymentAddressForOrder(cart)).thenReturn(mock(AddressModel.class));
        this.bluePreferredPaymentAddressCartCalculationMethodHook.beforeCalculate(parameter);

        verify(this.bluePreferredPaymentAddressLookupStrategy).getPreferredPaymentAddressForOrder(cart);
        verify(this.modelService).save(cart);
    }

    @Test
    public void testBeforeCalculate_isB2BCartButNoAddressFound() {
        final CommerceCartParameter parameter = mock(CommerceCartParameter.class);
        final CartModel cart = mock(CartModel.class);
        final B2BCustomerModel customer = mock(B2BCustomerModel.class);

        when(parameter.getCart()).thenReturn(cart);
        when(cart.getUser()).thenReturn(customer);
        this.bluePreferredPaymentAddressCartCalculationMethodHook.beforeCalculate(parameter);

        verify(this.bluePreferredPaymentAddressLookupStrategy).getPreferredPaymentAddressForOrder(cart);
        verifyZeroInteractions(this.modelService);
    }

    @Test
    public void testBeforeCalculate_notB2BCart() {
        final CommerceCartParameter parameter = mock(CommerceCartParameter.class);
        final CartModel cart = mock(CartModel.class);
        final CustomerModel anonymous = mock(CustomerModel.class);

        when(parameter.getCart()).thenReturn(cart);
        when(cart.getUser()).thenReturn(anonymous);
        this.bluePreferredPaymentAddressCartCalculationMethodHook.beforeCalculate(parameter);

        Mockito.verifyZeroInteractions(this.bluePreferredPaymentAddressLookupStrategy);
        verifyZeroInteractions(this.modelService);
    }
}