package com.vanmarcke.services.search.solrfacetsearch.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.search.FacetSearchException;
import de.hybris.platform.solrfacetsearch.search.FacetSearchService;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import de.hybris.platform.solrfacetsearch.search.SearchResult;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Locale;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKSearchServiceImplTest {

    @Mock
    private FacetSearchService facetSearchService;
    @Mock
    private I18NService i18NService;
    @InjectMocks
    private VMKSearchServiceImpl defaultVMKSearchService;

    @Test
    public void testSearch() throws FacetSearchException {
        SearchQuery searchQuery = mock(SearchQuery.class);

        defaultVMKSearchService.search(searchQuery);

        verify(facetSearchService).search(searchQuery);
    }

    @Test
    public void testSearchWhenNull() throws FacetSearchException {
        SearchResult result = defaultVMKSearchService.search(null);

        assertThat(result).isNull();
    }

    @Test
    public void createTextSearchQuery() {
        FacetSearchConfig facetSearchConfig = mock(FacetSearchConfig.class);
        IndexedType indexedType = mock(IndexedType.class);
        SearchQuery searchQuery = mock(SearchQuery.class);
        Locale locale = new Locale("nl", "BE");

        when(facetSearchService.createFreeTextSearchQuery(facetSearchConfig, indexedType, "toilet")).thenReturn(searchQuery);
        when(i18NService.getCurrentLocale()).thenReturn(locale);

        SearchQuery result = defaultVMKSearchService.createTextSearchQuery("toilet", facetSearchConfig, indexedType);

        assertThat(result).isEqualTo(searchQuery);
    }

    @Test
    public void createSearchQuery() {
        FacetSearchConfig facetSearchConfig = mock(FacetSearchConfig.class);
        IndexedType indexedType = mock(IndexedType.class);
        SearchQuery searchQuery = mock(SearchQuery.class);
        Locale locale = new Locale("nl", "BE");

        when(facetSearchService.createSearchQuery(facetSearchConfig, indexedType)).thenReturn(searchQuery);
        when(i18NService.getCurrentLocale()).thenReturn(locale);

        SearchQuery result = defaultVMKSearchService.createSearchQuery(facetSearchConfig, indexedType);

        assertThat(result).isEqualTo(searchQuery);
    }
}