package com.vanmarcke.services.event;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.order.events.SubmitOrderEvent;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.store.BaseStoreModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SubmitOrderEventListenerTest {

    @Mock
    private ModelService modelService;
    @Mock
    private BusinessProcessService businessProcessService;
    @InjectMocks
    private SubmitOrderEventListener submitOrderEventListener;

    @Test
    public void onSiteEvent() {
        OrderProcessModel orderProcessModel = mock(OrderProcessModel.class);
        OrderModel orderModel = mock(OrderModel.class);
        BaseStoreModel baseStore = mock(BaseStoreModel.class);
        SubmitOrderEvent submitOrderEvent = new SubmitOrderEvent();
        submitOrderEvent.setOrder(orderModel);
        when(orderModel.getStore()).thenReturn(baseStore);
        when(orderModel.getCode()).thenReturn("orderCode");
        when(baseStore.getSubmitOrderProcessCode()).thenReturn("submitOrderProcessCode");
        when(businessProcessService.createProcess(anyString(), anyString())).thenReturn(orderProcessModel);

        submitOrderEventListener.onSiteEvent(submitOrderEvent);

        verify(orderProcessModel).setOrder(orderModel);
        verify(modelService).save(orderProcessModel);
        verify(businessProcessService).startProcess(orderProcessModel);
    }

    @Test
    public void getSiteChannelForEvent() {
        OrderModel orderModel = mock(OrderModel.class);
        BaseSiteModel baseSiteModel = mock(BaseSiteModel.class);
        SubmitOrderEvent submitOrderEvent = new SubmitOrderEvent();
        submitOrderEvent.setOrder(orderModel);

        when(orderModel.getSite()).thenReturn(baseSiteModel);
        when(baseSiteModel.getChannel()).thenReturn(SiteChannel.B2C);

        SiteChannel actual = submitOrderEventListener.getSiteChannelForEvent(submitOrderEvent);

        assertThat(actual).isEqualTo(SiteChannel.B2C);
    }
}