package com.vanmarcke.services.strategies.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.externaltax.ExternalTaxesService;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.commerceservices.service.data.CommerceOrderResult;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.order.CalculationService;
import de.hybris.platform.order.OrderService;
import de.hybris.platform.promotions.PromotionsService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.time.TimeService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Date;

import static de.hybris.platform.commerceservices.enums.SalesApplication.WEB;
import static java.util.Collections.emptySet;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKCommercePlaceOrderStrategyImplTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Mock
    private CalculationService calculationService;
    @Mock
    private OrderService orderService;
    @Mock
    private TimeService timeService;
    @Mock
    private BaseSiteService baseSiteService;
    @Mock
    private BaseStoreService baseStoreService;
    @Mock
    private CommonI18NService commonI18NService;
    @Mock
    private ModelService modelService;
    @Mock
    private PromotionsService promotionsService;
    @Mock
    private ExternalTaxesService externalTaxesService;

    @InjectMocks
    private VMKCommercePlaceOrderStrategyImpl defaultVMKCommercePlaceOrderStrategy;

    @Test
    public void testPlaceOrder() throws Exception {
        CustomerModel customerModel = mock(CustomerModel.class);
        CartModel cartModel = mock(CartModel.class);
        OrderModel orderModel = mock(OrderModel.class);
        Date currentTime = mock(Date.class);
        BaseSiteModel baseSiteModel = mock(BaseSiteModel.class);
        BaseStoreModel baseStoreModel = mock(BaseStoreModel.class);
        LanguageModel languageModel = mock(LanguageModel.class);

        when(cartModel.getUser()).thenReturn(customerModel);

        when(orderService.createOrderFromCart(cartModel)).thenReturn(orderModel);

        when(timeService.getCurrentTime()).thenReturn(currentTime);

        when(baseSiteService.getCurrentBaseSite()).thenReturn(baseSiteModel);

        when(baseStoreService.getCurrentBaseStore()).thenReturn(baseStoreModel);

        when(commonI18NService.getCurrentLanguage()).thenReturn(languageModel);

        CommerceCheckoutParameter parameter = new CommerceCheckoutParameter();
        parameter.setCart(cartModel);
        parameter.setSalesApplication(WEB);
        CommerceOrderResult result = defaultVMKCommercePlaceOrderStrategy.placeOrder(parameter);

        assertThat(result.getOrder()).isEqualTo(orderModel);

        verify(orderModel).setDate(currentTime);
        verify(orderModel).setSite(baseSiteModel);
        verify(orderModel).setStore(baseStoreModel);
        verify(orderModel).setLanguage(languageModel);
        verify(orderModel).setSalesApplication(WEB);
        verify(orderModel).setAllPromotionResults(emptySet());

        verify(modelService).saveAll(customerModel, orderModel);
        verify(modelService).refresh(orderModel);
        verify(modelService).refresh(customerModel);

        verify(promotionsService).transferPromotionsToOrder(cartModel, orderModel, false);

        verify(calculationService).calculateTotals(orderModel, false);

        verify(externalTaxesService).calculateExternalTaxes(orderModel);
        verify(externalTaxesService).clearSessionTaxDocument();

        verify(orderService).submitOrder(orderModel);
    }

    @Test
    public void testPlaceOrder_withOrderCreationFailure() throws Exception {
        thrown.expect(IllegalArgumentException.class);

        CustomerModel customerModel = mock(CustomerModel.class);
        CartModel cartModel = mock(CartModel.class);

        when(cartModel.getUser()).thenReturn(customerModel);

        CommerceCheckoutParameter parameter = new CommerceCheckoutParameter();
        parameter.setCart(cartModel);
        defaultVMKCommercePlaceOrderStrategy.placeOrder(parameter);

        verifyZeroInteractions(modelService);
        verifyZeroInteractions(promotionsService);
    }

}