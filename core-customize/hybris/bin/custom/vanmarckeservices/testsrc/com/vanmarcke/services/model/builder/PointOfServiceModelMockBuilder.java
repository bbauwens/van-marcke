package com.vanmarcke.services.model.builder;

import de.hybris.platform.storelocator.model.OpeningScheduleModel;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * The {@code PointOfServiceModelMockBuilder} class can be used to create a mock implementation of the
 * {@link PointOfServiceModel} class.
 *
 * @author Christiaan Janssen
 * @since 06-07-2020
 */
public class PointOfServiceModelMockBuilder {

    private String name;
    private OpeningScheduleModel openingSchedule;

    /**
     * Creates a new instance of the {@link PointOfServiceModelMockBuilder} class.
     */
    private PointOfServiceModelMockBuilder() {
    }

    /**
     * The entry point to the {@link PointOfServiceModelMockBuilder}.
     *
     * @return the {@link PointOfServiceModelMockBuilder} instance for method chaining
     */
    public static PointOfServiceModelMockBuilder aPointOfService() {
        return new PointOfServiceModelMockBuilder();
    }

    /**
     * Sets the {@code name} on the current {@link PointOfServiceModelMockBuilder} instance
     *
     * @param name the point of service's unique identifier
     * @return the {@link PointOfServiceModelMockBuilder} instance for method chaining
     */
    public PointOfServiceModelMockBuilder withName(String name) {
        this.name = name;
        return this;
    }

    /**
     * Sets the {@code openingSchedule} on the current {@link PointOfServiceModelMockBuilder} instance
     *
     * @param openingSchedule the opening schedule
     * @return the {@link PointOfServiceModelMockBuilder} instance for method chaining
     */
    public PointOfServiceModelMockBuilder withOpeningSchedule(OpeningScheduleModel openingSchedule) {
        this.openingSchedule = openingSchedule;
        return this;
    }

    /**
     * Creates a mock implementation of the {@link PointOfServiceModel} class.
     *
     * @return the {@link PointOfServiceModel} instance
     */
    public PointOfServiceModel build() {
        PointOfServiceModel pos = mock(PointOfServiceModel.class);
        when(pos.getName()).thenReturn(name);
        when(pos.getOpeningSchedule()).thenReturn(openingSchedule);
        return pos;
    }
}
