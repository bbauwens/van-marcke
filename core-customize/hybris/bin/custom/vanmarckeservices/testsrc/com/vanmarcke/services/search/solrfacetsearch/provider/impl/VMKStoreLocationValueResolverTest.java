package com.vanmarcke.services.search.solrfacetsearch.provider.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolverTest;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKStoreLocationValueResolverTest extends AbstractValueResolverTest {

    @InjectMocks
    VMKStoreLocationValueResolver storeLocationValueResolver;
    @Mock
    private InputDocument inputDocument;
    @Mock
    private IndexerBatchContext indexerBatchContext;
    @Mock
    private IndexedProperty indexedProperty;
    @Mock
    private PointOfServiceModel posModel;

    @Test
    public void test_latitudeAndLongitudeFilled() throws FieldValueProviderException {
        when(indexedProperty.getName()).thenReturn("latitude");
        when(posModel.getLatitude()).thenReturn(5d);
        when(posModel.getLongitude()).thenReturn(10d);
        storeLocationValueResolver.resolve(inputDocument, indexerBatchContext, Collections.singletonList(indexedProperty), posModel);
        verify(inputDocument).addField(indexedProperty, "5.0,10.0", null);
    }

    @Test
    public void test_latitudeFilledAndLongitudeNot() throws FieldValueProviderException {
        when(indexedProperty.getName()).thenReturn("latitude");
        when(posModel.getLatitude()).thenReturn(5d);
        when(posModel.getLongitude()).thenReturn(null);
        storeLocationValueResolver.resolve(inputDocument, indexerBatchContext, Collections.singletonList(indexedProperty), posModel);
        verify(inputDocument).addField(indexedProperty, "5.0,0.0", null);
    }

    @Test
    public void test_latitudeNotAndLongitudeFilled() throws FieldValueProviderException {
        when(indexedProperty.getName()).thenReturn("latitude");
        when(posModel.getLatitude()).thenReturn(null);
        when(posModel.getLongitude()).thenReturn(10d);
        storeLocationValueResolver.resolve(inputDocument, indexerBatchContext, Collections.singletonList(indexedProperty), posModel);
        verify(inputDocument).addField(indexedProperty, "0.0,10.0", null);
    }

    @Test
    public void test_latitudeAndLongitudeNot() throws FieldValueProviderException {
        when(indexedProperty.getName()).thenReturn("latitude");
        when(posModel.getLatitude()).thenReturn(null);
        when(posModel.getLongitude()).thenReturn(null);
        storeLocationValueResolver.resolve(inputDocument, indexerBatchContext, Collections.singletonList(indexedProperty), posModel);
        verify(inputDocument).addField(indexedProperty, "0.0,0.0", null);
    }

}
