package com.vanmarcke.services.builder;

import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;
import de.hybris.platform.catalog.model.classification.ClassificationSystemVersionModel;

public class ClassAttributeAssignmentModelBuilder {

    private final ClassAttributeAssignmentModel classAttributeAssignment = new ClassAttributeAssignmentModel();

    /**
     * Private constructor to hide the implicit default one.
     */
    private ClassAttributeAssignmentModelBuilder() {
    }

    /**
     * A new instance of the {@link ClassAttributeAssignmentModelBuilder} is created and returned
     *
     * @return the created {@link ClassAttributeAssignmentModelBuilder} instance
     */
    public static ClassAttributeAssignmentModelBuilder aClassAttributeAssignmentModel() {
        return new ClassAttributeAssignmentModelBuilder();
    }

    /**
     * Sets the given {@code systemVersion} on the current {@link ClassAttributeAssignmentModel} instance.
     *
     * @param systemVersion the systemVersion
     * @return the current {@link ClassAttributeAssignmentModelBuilder} instance for method chaining
     */
    public ClassAttributeAssignmentModelBuilder withSystemVersion(ClassificationSystemVersionModel systemVersion) {
        classAttributeAssignment.setSystemVersion(systemVersion);
        return this;
    }

    /**
     * Returns the current {@link ClassAttributeAssignmentModel} instance
     *
     * @return the current {@link ClassAttributeAssignmentModel} instance
     */
    public ClassAttributeAssignmentModel build() {
        return classAttributeAssignment;
    }
}
