package com.vanmarcke.services.config.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.fest.assertions.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

/**
 * The {@link VMKConfigurationServiceImplTest} class contains the unit tests for the
 * {@link VMKConfigurationServiceImpl} class.
 *
 * @author Christiaan Janssen
 * @since 14-07-2020
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKConfigurationServiceImplTest {

    public static final String KEY = RandomStringUtils.random(10);
    public static final String STRING_VALUE = RandomStringUtils.random(10);
    public static final String STRING_DEFAULT_VALUE = RandomStringUtils.random(10);
    public static final Boolean BOOLEAN_VALUE = RandomUtils.nextBoolean();
    public static final Boolean BOOLEAN_DEFAULT_VALUE = RandomUtils.nextBoolean();

    @Mock
    private Configuration cfg;

    @Spy
    @InjectMocks
    private VMKConfigurationServiceImpl configurationService;

    @Test
    public void testGetString() {
        Configuration configuration = mock(Configuration.class);
        when(configuration.getString(KEY, STRING_DEFAULT_VALUE)).thenReturn(STRING_VALUE);

        doReturn(configuration).when(configurationService).getConfiguration();

        String actualValue = configurationService.getString(KEY, STRING_DEFAULT_VALUE);

        Assertions
                .assertThat(actualValue)
                .isEqualTo(STRING_VALUE);
    }

    @Test
    public void testGetBoolean() {
        Configuration configuration = mock(Configuration.class);
        when(configuration.getBoolean(KEY, BOOLEAN_DEFAULT_VALUE)).thenReturn(BOOLEAN_VALUE);

        doReturn(configuration).when(configurationService).getConfiguration();

        Boolean actualValue = configurationService.getBoolean(KEY, BOOLEAN_DEFAULT_VALUE);

        Assertions
                .assertThat(actualValue)
                .isEqualTo(BOOLEAN_VALUE);
    }

    @Test
    public void testGetConfiguration() {
        // This method is only here to max out the test coverage.
        configurationService.getConfiguration();
    }
}