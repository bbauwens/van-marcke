package com.vanmarcke.services.address.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKPaymentAddressesLookupStrategyTest {

    @Mock
    private B2BUnitModel b2BUnit;

    @InjectMocks
    private VMKPaymentAddressesLookupStrategyImpl vmkB2BPaymentAddressesLookupStrategy;

    private AbstractOrderModel abstractOrder;

    @Before
    public void setUp() {
        this.abstractOrder = mock(AbstractOrderModel.class);
    }

    @Test
    public void testGetDeliveryAddressesForOrder() {
        final AddressModel shippingAddress = mock(AddressModel.class);
        final AddressModel billingAddress = mock(AddressModel.class);
        final CountryModel country = mock(CountryModel.class);
        final List<AddressModel> allAddresses = new ArrayList<>();
        allAddresses.add(shippingAddress);
        allAddresses.add(billingAddress);

        when(this.abstractOrder.getUnit()).thenReturn(this.b2BUnit);
        when(this.b2BUnit.getAddresses()).thenReturn(allAddresses);
        when(shippingAddress.getShippingAddress()).thenReturn(true);
        when(shippingAddress.getVisibleInAddressBook()).thenReturn(true);
        when(billingAddress.getBillingAddress()).thenReturn(true);
        when(billingAddress.getVisibleInAddressBook()).thenReturn(true);
        when(shippingAddress.getCountry()).thenReturn(country);
        when(billingAddress.getCountry()).thenReturn(country);
        final List<AddressModel> result = this.vmkB2BPaymentAddressesLookupStrategy.getPaymentAddressesForOrder(this.abstractOrder);

        assertThat(result).isNotNull().isNotEmpty().containsExactly(billingAddress);
    }

    @Test
    public void testGetDeliveryAddressesForOrder_noAddresses() {
        when(this.abstractOrder.getUnit()).thenReturn(this.b2BUnit);
        final List<AddressModel> result = this.vmkB2BPaymentAddressesLookupStrategy.getPaymentAddressesForOrder(this.abstractOrder);

        assertThat(result).isNotNull().isEmpty();
    }
}