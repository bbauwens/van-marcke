package com.vanmarcke.services.impersonation;

import com.vanmarcke.core.model.CommerceGroupModel;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.commerceservices.impersonation.ImpersonationContext;
import de.hybris.platform.europe1.enums.UserPriceGroup;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.services.BaseStoreService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static de.hybris.platform.europe1.constants.Europe1Constants.PARAMS.UPG;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKImpersonationServiceImplTest {

    @Mock
    private BaseSiteService baseSiteService;
    @Mock
    private BaseStoreService baseStoreService;
    @Mock
    private CommonI18NService i18nService;
    @Mock
    private SessionService sessionService;
    @Mock
    private UserService userService;

    @InjectMocks
    private VMKImpersonationServiceImpl defaultVMKImpersonationService;

    @Test
    public void testConfigureSession() {
        ImpersonationContext impersonationContext = mock(ImpersonationContext.class);
        CMSSiteModel cmsSiteModel = mock(CMSSiteModel.class);
        CommerceGroupModel commerceGroupModel = mock(CommerceGroupModel.class);
        UserPriceGroup userPriceGroup = mock(UserPriceGroup.class);

        when(impersonationContext.getSite()).thenReturn(cmsSiteModel);

        when(cmsSiteModel.getCommerceGroup()).thenReturn(commerceGroupModel);

        when(commerceGroupModel.getUserPriceGroup()).thenReturn(userPriceGroup);

        defaultVMKImpersonationService.configureSession(impersonationContext);

        verify(sessionService).setAttribute(UPG, userPriceGroup);
    }

    @Test
    public void testConfigureSession_withoutCommerceGroup() {
        ImpersonationContext impersonationContext = mock(ImpersonationContext.class);
        CMSSiteModel cmsSiteModel = mock(CMSSiteModel.class);

        when(impersonationContext.getSite()).thenReturn(cmsSiteModel);

        defaultVMKImpersonationService.configureSession(impersonationContext);

        verify(sessionService, never()).setAttribute(eq(UPG), any());
    }

}