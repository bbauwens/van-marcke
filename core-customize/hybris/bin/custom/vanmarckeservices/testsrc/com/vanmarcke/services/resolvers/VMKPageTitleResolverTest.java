package com.vanmarcke.services.resolvers;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * The {@link VMKPageTitleResolverTest} contains the unit tests for the {@link VMKPageTitleResolver} class.
 *
 * @author Giani Ifrim
 * @since 08-07-2021
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKPageTitleResolverTest {

    @InjectMocks
    private VMKPageTitleResolver vmkPageTitleResolver;

    @Test
    public void testResolveCategoryPageTitle() {
        CategoryModel categoryModelMock = mock(CategoryModel.class);

        String demoCategoryName = "Demo Category";
        String demoPageTitleSiteName = "Van Marcke Blue";

        when(categoryModelMock.getName()).thenReturn(demoCategoryName);
        String result = vmkPageTitleResolver.resolveCategoryPageTitle(categoryModelMock);

        assertThat(result).isEqualTo(demoCategoryName + " | " + demoPageTitleSiteName);
    }

    @Test
    public void testResolveProductPageTitle() {
        ProductModel productModelMock = mock(ProductModel.class);

        String demoProductName = "Demo Product";
        String demoPageTitleSiteName = "Van Marcke Blue";

        when(productModelMock.getName()).thenReturn(demoProductName);
        String result = vmkPageTitleResolver.resolveProductPageTitle(productModelMock);

        assertThat(result).isEqualTo(demoProductName + " | " + demoPageTitleSiteName);
    }

    @Test
    public void testResolveProductPageTitleForEmptyProductName() {
        ProductModel productModelMock = mock(ProductModel.class);

        String emptyDemoProductName = "";
        String demoProductCode = "ABC123";
        String demoPageTitleSiteName = "Van Marcke Blue";

        when(productModelMock.getName()).thenReturn(emptyDemoProductName);
        when(productModelMock.getCode()).thenReturn(demoProductCode);
        String result = vmkPageTitleResolver.resolveProductPageTitle(productModelMock);

        assertThat(result).isEqualTo(demoProductCode + " | " + demoPageTitleSiteName);
    }
}
