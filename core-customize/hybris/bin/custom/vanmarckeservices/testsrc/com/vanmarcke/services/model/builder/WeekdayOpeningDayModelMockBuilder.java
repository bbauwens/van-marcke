package com.vanmarcke.services.model.builder;

import de.hybris.platform.basecommerce.enums.WeekDay;
import de.hybris.platform.storelocator.model.WeekdayOpeningDayModel;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * The {@code WeekdayOpeningDayModelMockBuilder} class can be used to create a mock implementation of the
 * {@link WeekdayOpeningDayModel} class.
 *
 * @author Christiaan Janssen
 * @since 07-07-2020
 */
public class WeekdayOpeningDayModelMockBuilder extends OpeningDayModelMockBuilder<WeekdayOpeningDayModelMockBuilder, WeekdayOpeningDayModel> {

    private WeekDay dayOfWeek;

    /**
     * Creates a new instance of the {@link WeekdayOpeningDayModelMockBuilder} class.
     */
    private WeekdayOpeningDayModelMockBuilder() {
        super(WeekdayOpeningDayModelMockBuilder.class);
    }

    /**
     * The entry point to the {@link WeekdayOpeningDayModelMockBuilder}.
     *
     * @return the {@link WeekdayOpeningDayModelMockBuilder} instance for method chaining
     */
    public static WeekdayOpeningDayModelMockBuilder aWeekdayOpeningDay() {
        return new WeekdayOpeningDayModelMockBuilder();
    }

    /**
     * Sets the {@code dayOfWeek} on the current {@link WeekdayOpeningDayModelMockBuilder} instance
     *
     * @param dayOfWeek the day of the week
     * @return the {@link WeekdayOpeningDayModelMockBuilder} instance for method chaining
     */
    public WeekdayOpeningDayModelMockBuilder withDayOfWeek(WeekDay dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
        return this;
    }

    /**
     * Creates a mock implementation of the {@link WeekdayOpeningDayModel} class.
     *
     * @return the {@link WeekdayOpeningDayModel} instance
     */
    public WeekdayOpeningDayModel build() {
        WeekdayOpeningDayModel openingDay = mock(WeekdayOpeningDayModel.class);
        when(openingDay.getDayOfWeek()).thenReturn(dayOfWeek);
        when(openingDay.getOpeningTime()).thenReturn(openingTime);
        when(openingDay.getClosingTime()).thenReturn(closingTime);
        return openingDay;
    }
}
