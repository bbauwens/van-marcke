package com.vanmarcke.services.search.solrfacetsearch.provider.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolverTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;

import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKCategoryThumbnailValueResolverTest extends AbstractValueResolverTest {

    @Mock
    private InputDocument inputDocument;
    @Mock
    private IndexerBatchContext indexerBatchContext;
    @Mock
    private IndexedProperty indexedProperty;
    @Mock
    private CategoryModel categoryModel;

    @InjectMocks
    private VMKCategoryThumbnailValueResolver valueResolver;

    @Test
    public void testAddFieldValuesWhenNoThumbnailFound() throws FieldValueProviderException {
        valueResolver.resolve(inputDocument, indexerBatchContext, Collections.singletonList(indexedProperty), categoryModel);
        verifyZeroInteractions(inputDocument);
    }

    @Test
    public void testAddFieldValuesWhenThumbnailUrlBlank() throws FieldValueProviderException {
        MediaModel mediaModel = mock(MediaModel.class);
        when(categoryModel.getThumbnail()).thenReturn(mediaModel);
        valueResolver.resolve(inputDocument, indexerBatchContext, Collections.singletonList(indexedProperty), categoryModel);
        verifyZeroInteractions(inputDocument);
    }

    @Test
    public void testAddFieldValues() throws FieldValueProviderException {
        MediaModel mediaModel = mock(MediaModel.class);
        when(categoryModel.getThumbnail()).thenReturn(mediaModel);
        when(mediaModel.getURL()).thenReturn("URL");
        valueResolver.resolve(inputDocument, indexerBatchContext, Collections.singletonList(indexedProperty), categoryModel);
        verify(inputDocument).addField(indexedProperty, "URL");
    }
}