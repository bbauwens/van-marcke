package com.vanmarcke.services.helper;

import de.hybris.bootstrap.annotations.UnitTest;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.fest.assertions.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.stream.XMLStreamException;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * The {@link StreamingMarshallerHelperTest} class contains the unit tests for the {@link StreamingMarshallerHelper}
 * class.
 *
 * @author Christiaan Janssen
 * @since 10/09/2020
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class StreamingMarshallerHelperTest {

    private static final String ONE = RandomStringUtils.randomAlphabetic(10);
    private static final String TWO = RandomStringUtils.randomAlphabetic(10);
    private static final String THREE = RandomStringUtils.randomAlphabetic(10);
    private static final String XML = "<?xml version='1.0' encoding='UTF-8'?><root><test><one>" + ONE + "</one><two>" + TWO + "</two><three>" + THREE + "</three></test></root>";

    @Test
    public void testMarshaller() throws IOException, XMLStreamException, JAXBException {
        TestNode test = new TestNode(ONE, TWO, THREE);

        StreamingMarshallerHelper<TestNode> marshaller = new StreamingMarshallerHelper<>(TestNode.class);
        marshaller.open("test-marshaller.xml", "root");
        marshaller.write(test, "test");
        marshaller.close();

        File file = new File("test-marshaller.xml");

        Assertions
                .assertThat(file)
                .isNotNull();

        Assertions
                .assertThat(XML)
                .isEqualTo(new String(Files.readAllBytes(Paths.get("test-marshaller.xml"))));

        FileUtils.deleteQuietly(file);
    }

    @XmlRootElement(name = "test")
    @XmlType(propOrder = {"one", "two", "three"})
    public static class TestNode {

        private String one;
        private String two;
        private String three;

        public TestNode() {
        }

        public TestNode(String one, String two, String three) {
            this.one = one;
            this.two = two;
            this.three = three;
        }

        public String getOne() {
            return one;
        }

        public void setOne(String one) {
            this.one = one;
        }

        public String getTwo() {
            return two;
        }

        public void setTwo(String two) {
            this.two = two;
        }

        public String getThree() {
            return three;
        }

        public void setThree(String three) {
            this.three = three;
        }
    }
}