package com.vanmarcke.services.b2b.impl;

import com.vanmarcke.services.model.builder.AddressModelMockBuilder;
import com.vanmarcke.services.model.builder.B2BUnitModelMockBuilder;
import com.vanmarcke.services.model.builder.CountryModelMockBuilder;
import com.vanmarcke.services.model.builder.PointOfServiceModelMockBuilder;
import com.vanmarcke.services.storelocator.pos.VMKPointOfServiceService;
import com.vanmarcke.services.storesession.VMKStoreSessionService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.store.data.GeoPoint;
import de.hybris.platform.commerceservices.storefinder.StoreFinderService;
import de.hybris.platform.commerceservices.storefinder.data.PointOfServiceDistanceData;
import de.hybris.platform.commerceservices.storefinder.data.StoreFinderSearchPageData;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKSSOUserServiceImplTest {

    private static final String ISO = RandomStringUtils.random(10);
    private static final String TOWN = RandomStringUtils.random(10);

    @Mock
    private StoreFinderService<PointOfServiceDistanceData, StoreFinderSearchPageData<PointOfServiceDistanceData>> storeFinderService;
    @Mock
    private VMKStoreSessionService storeSessionService;
    @Mock
    private BaseStoreService baseStoreService;
    @Mock
    private ModelService modelService;
    @Mock
    private VMKPointOfServiceService pointOfServiceService;

    @Spy
    @InjectMocks
    private VMKSSOUserServiceImpl vmkssoUserService;

    @Test
    public void testUpdateUserSessionStoreIfNotPresent() {
        B2BCustomerModel currentUser = mock(B2BCustomerModel.class);
        BaseStoreModel store = mock(BaseStoreModel.class);

        CountryModel country = CountryModelMockBuilder
                .aCountry()
                .withISOCode(ISO)
                .build();

        AddressModel address = AddressModelMockBuilder
                .anAddress()
                .withTown(TOWN)
                .withCountry(country)
                .build();

        B2BUnitModel b2bUnit = B2BUnitModelMockBuilder
                .aB2BUnit(RandomStringUtils.random(10))
                .withShippingAddress(address)
                .withBillingAddress(address)
                .build();

        PointOfServiceModel pos = PointOfServiceModelMockBuilder
                .aPointOfService()
                .build();

        when(currentUser.getDefaultB2BUnit()).thenReturn(b2bUnit);
        when(baseStoreService.getBaseStoreForUid("blue-" + ISO.toLowerCase())).thenReturn(store);
        PointOfServiceDistanceData posWrapper = new PointOfServiceDistanceData();
        posWrapper.setPointOfService(pos);

        StoreFinderSearchPageData<PointOfServiceDistanceData> searchPageData = new StoreFinderSearchPageData<>();
        searchPageData.setResults(Collections.singletonList(posWrapper));

        when(storeFinderService.locationSearch(eq(store), eq(TOWN), any(PageableData.class))).thenReturn(searchPageData);

        vmkssoUserService.updateUserSessionStoreIfNotPresent(currentUser);

        verify(baseStoreService, times(2)).getBaseStoreForUid("blue-" + ISO.toLowerCase());
        verify(storeFinderService).locationSearch(eq(store), eq(TOWN), any(PageableData.class));
        verify(modelService).save(currentUser);
        verify(storeSessionService).setCurrentStore(pos.getName());
    }

    @Test
    public void testGetNearestStore() {
        B2BCustomerModel currentUser = mock(B2BCustomerModel.class);
        BaseStoreModel store = mock(BaseStoreModel.class);
        BaseStoreModel userLocatedStore = mock(BaseStoreModel.class);
        GeoPoint geoPoint = mock(GeoPoint.class);

        CountryModel country = CountryModelMockBuilder
                .aCountry()
                .withISOCode(ISO)
                .build();

        AddressModel address = AddressModelMockBuilder
                .anAddress()
                .withTown(TOWN)
                .withCountry(country)
                .build();

        PointOfServiceModel pos = PointOfServiceModelMockBuilder
                .aPointOfService()
                .build();

        PointOfServiceModel sessionPos = PointOfServiceModelMockBuilder
                .aPointOfService()
                .build();

        B2BUnitModel b2bUnit = B2BUnitModelMockBuilder
                .aB2BUnit(RandomStringUtils.random(10))
                .withShippingAddress(address)
                .withBillingAddress(address)
                .build();

        when(currentUser.getDefaultB2BUnit()).thenReturn(b2bUnit);
        when(baseStoreService.getBaseStoreForUid("blue-" + ISO.toLowerCase())).thenReturn(userLocatedStore);
        when(currentUser.getSessionStore()).thenReturn(sessionPos);
        when(pointOfServiceService.getGeoPoint(sessionPos)).thenReturn(geoPoint);
        PointOfServiceDistanceData posWrapper = new PointOfServiceDistanceData();
        posWrapper.setPointOfService(pos);

        StoreFinderSearchPageData<PointOfServiceDistanceData> searchPageData = new StoreFinderSearchPageData<>();
        searchPageData.setResults(Collections.singletonList(posWrapper));

        when(storeFinderService.positionSearch(eq(store), eq(geoPoint), any(PageableData.class))).thenReturn(searchPageData);

        vmkssoUserService.getNearestStore(currentUser, store);

        verify(baseStoreService).getBaseStoreForUid("blue-" + ISO.toLowerCase());
        verify(storeFinderService).positionSearch(eq(store), eq(geoPoint), any(PageableData.class));
        verify(pointOfServiceService).getGeoPoint(sessionPos);
    }

    @Test
    public void testGetNearestStore_nullNearestStore() {
        B2BCustomerModel currentUser = mock(B2BCustomerModel.class);
        BaseStoreModel store = mock(BaseStoreModel.class);
        BaseStoreModel userLocatedStore = mock(BaseStoreModel.class);

        CountryModel country = CountryModelMockBuilder
                .aCountry()
                .withISOCode(ISO)
                .build();

        AddressModel address = AddressModelMockBuilder
                .anAddress()
                .withTown(TOWN)
                .withCountry(country)
                .build();

        PointOfServiceModel sessionPos = PointOfServiceModelMockBuilder
                .aPointOfService()
                .build();

        B2BUnitModel b2bUnit = B2BUnitModelMockBuilder
                .aB2BUnit(RandomStringUtils.random(10))
                .withShippingAddress(address)
                .withBillingAddress(address)
                .build();

        when(currentUser.getDefaultB2BUnit()).thenReturn(b2bUnit);
        when(baseStoreService.getBaseStoreForUid("blue-" + ISO.toLowerCase())).thenReturn(userLocatedStore);
        when(currentUser.getSessionStore()).thenReturn(sessionPos);

        doReturn(null).when(vmkssoUserService).findNearestStoreForBaseStore(store, TOWN);
        vmkssoUserService.getNearestStore(currentUser, store);
        verify(vmkssoUserService).getFallbackStore(store, address);
    }


    @Test
    public void testGetFallbackPos() {
        BaseStoreModel baseStoreModel = mock(BaseStoreModel.class);
        AddressModel addressModel = mock(AddressModel.class);
        CountryModel countryModel = mock(CountryModel.class);
        PointOfServiceModel pointOfServiceModel = mock(PointOfServiceModel.class);
        when(addressModel.getCountry()).thenReturn(countryModel);
        when(countryModel.getIsocode()).thenReturn("be");
        when(storeFinderService.getAllPosForCountry("be", baseStoreModel)).thenReturn(List.of(pointOfServiceModel));

        Assert.assertEquals(pointOfServiceModel, vmkssoUserService.getFallbackStore(baseStoreModel, addressModel));
    }
}
