package com.vanmarcke.services.product.impl;

import com.vanmarcke.core.model.CommerceGroupModel;
import com.vanmarcke.cpi.data.builder.CrossSellingRequestDataBuilder;
import com.vanmarcke.cpi.data.product.CrossSellingRequestData;
import com.vanmarcke.cpi.data.product.CrossSellingResponseData;
import com.vanmarcke.cpi.services.ESBCrossSellingService;
import com.vanmarcke.services.commercegroup.VMKCommerceGroupService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CountryModel;
import org.apache.commons.lang.RandomStringUtils;
import org.fest.assertions.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKCrossSellingLookupServiceImplTest {

    private static final String CODE = RandomStringUtils.randomAlphabetic(10);
    private static final String ISO_CODE = RandomStringUtils.randomAlphabetic(10);

    @Mock
    private ESBCrossSellingService esbCrossSellingService;

    @Mock
    private VMKCommerceGroupService vmkCommerceGroupService;

    @Captor
    private ArgumentCaptor<CrossSellingRequestData> captor;

    @InjectMocks
    private VMKCrossSellingLookupServiceImpl lookupService;

    @Test
    public void testGetCrossSellingProducts_Succes() {
        CommerceGroupModel commerceGroup = mock(CommerceGroupModel.class);
        CountryModel country = mock(CountryModel.class);

        when(commerceGroup.getCountry()).thenReturn(country);
        when(country.getIsocode()).thenReturn(ISO_CODE);
        when(vmkCommerceGroupService.getCurrentCommerceGroup()).thenReturn(commerceGroup);

        List<String> crossSellingProductCodes = Collections.singletonList(CODE);

        CrossSellingResponseData response = new CrossSellingResponseData();
        response.setCrossSellingProductCodes(crossSellingProductCodes);

        when(esbCrossSellingService.getCrossSellingInformation(captor.capture())).thenReturn(response);

        List<String> actual = lookupService.getCrossSellingProducts(CODE);

        Assertions
                .assertThat(actual)
                .isEqualTo(crossSellingProductCodes);

        Assertions
                .assertThat(captor.getValue().getProductId())
                .isEqualTo(CODE);

        Assertions
                .assertThat(captor.getValue().getCountry())
                .isEqualTo(ISO_CODE);

        verify(vmkCommerceGroupService).getCurrentCommerceGroup();
        verify(esbCrossSellingService).getCrossSellingInformation(captor.getValue());
    }

    @Test
    public void testGetCrossSellingProducts_NoCommerceGroup() {
        when(vmkCommerceGroupService.getCurrentCommerceGroup()).thenReturn(null);

        List<String> crossSellingProductCodes = Collections.singletonList(CODE);
        CrossSellingRequestData request = CrossSellingRequestDataBuilder
                .aCrossSellingRequest(CODE)
                .withCountryISO(ISO_CODE)
                .build();

        CrossSellingResponseData response = new CrossSellingResponseData();
        response.setCrossSellingProductCodes(crossSellingProductCodes);

        when(esbCrossSellingService.getCrossSellingInformation(captor.capture())).thenReturn(response);

        List<String> actual = lookupService.getCrossSellingProducts(CODE);

        Assertions
                .assertThat(actual)
                .isEqualTo(crossSellingProductCodes);

        Assertions
                .assertThat(captor.getValue().getProductId())
                .isEqualTo(CODE);

        Assertions
                .assertThat(captor.getValue().getCountry())
                .isNull();

        verify(vmkCommerceGroupService).getCurrentCommerceGroup();
        verify(esbCrossSellingService).getCrossSellingInformation(captor.getValue());
    }

    @Test
    public void testGetCrossSellingProducts_NoProductCodes() {
        CommerceGroupModel commerceGroup = mock(CommerceGroupModel.class);
        CountryModel country = mock(CountryModel.class);

        when(commerceGroup.getCountry()).thenReturn(country);
        when(country.getIsocode()).thenReturn(ISO_CODE);
        when(vmkCommerceGroupService.getCurrentCommerceGroup()).thenReturn(commerceGroup);

        CrossSellingResponseData response = new CrossSellingResponseData();

        when(esbCrossSellingService.getCrossSellingInformation(captor.capture())).thenReturn(response);

        List<String> actual = lookupService.getCrossSellingProducts(CODE);

        Assertions
                .assertThat(actual)
                .isEmpty();

        Assertions
                .assertThat(captor.getValue().getProductId())
                .isEqualTo(CODE);

        Assertions
                .assertThat(captor.getValue().getCountry())
                .isEqualTo(ISO_CODE);

        verify(vmkCommerceGroupService).getCurrentCommerceGroup();
        verify(esbCrossSellingService).getCrossSellingInformation(captor.getValue());
    }

    @Test
    public void testGetCrossSellingProducts_NoResponse() {
        CommerceGroupModel commerceGroup = mock(CommerceGroupModel.class);
        CountryModel country = mock(CountryModel.class);

        when(commerceGroup.getCountry()).thenReturn(country);
        when(country.getIsocode()).thenReturn(ISO_CODE);
        when(vmkCommerceGroupService.getCurrentCommerceGroup()).thenReturn(commerceGroup);

        when(esbCrossSellingService.getCrossSellingInformation(captor.capture())).thenReturn(null);

        List<String> actual = lookupService.getCrossSellingProducts(CODE);

        Assertions
                .assertThat(actual)
                .isEmpty();

        Assertions
                .assertThat(captor.getValue().getProductId())
                .isEqualTo(CODE);

        Assertions
                .assertThat(captor.getValue().getCountry())
                .isEqualTo(ISO_CODE);

        verify(vmkCommerceGroupService).getCurrentCommerceGroup();
        verify(esbCrossSellingService).getCrossSellingInformation(captor.getValue());
    }
}