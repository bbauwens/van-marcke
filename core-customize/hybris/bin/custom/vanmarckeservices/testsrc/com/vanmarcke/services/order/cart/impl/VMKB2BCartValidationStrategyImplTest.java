package com.vanmarcke.services.order.cart.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.model.ModelService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKB2BCartValidationStrategyImplTest {

    @Mock
    private ModelService modelService;

    @InjectMocks
    private VMKB2BCartValidationStrategyImpl vmkB2BCartValidationStrategy;

    @Test
    public void testGetStockLevel() {
        final CartEntryModel cartEntryModel = mock(CartEntryModel.class);

        final Long result = this.vmkB2BCartValidationStrategy.getStockLevel(cartEntryModel);
        assertThat(result).isEqualTo(Long.MAX_VALUE);
    }

    @Test
    public void testValidateDelivery_isB2BCustomerWithCorrectAddress() {
        final AddressModel deliveryAddress = mock(AddressModel.class);
        final B2BCustomerModel user = mock(B2BCustomerModel.class);
        final B2BUnitModel b2bUnit = mock(B2BUnitModel.class);
        final CartModel cart = mock(CartModel.class);

        when(cart.getDeliveryAddress()).thenReturn(deliveryAddress);
        when(cart.getUser()).thenReturn(user);
        when(cart.getUnit()).thenReturn(b2bUnit);

        when(deliveryAddress.getOwner()).thenReturn(b2bUnit);
        this.vmkB2BCartValidationStrategy.validateDelivery(cart);

        assertThat(cart.getDeliveryAddress()).isNotNull();
        verifyZeroInteractions(this.modelService);
    }

    @Test
    public void testValidateDelivery_isB2BCustomerWithIncorrectAddress() {
        final AddressModel deliveryAddress = mock(AddressModel.class);
        final B2BCustomerModel user = mock(B2BCustomerModel.class);
        final B2BUnitModel b2bUnit = mock(B2BUnitModel.class);
        final CartModel cart = mock(CartModel.class);

        when(cart.getDeliveryAddress()).thenReturn(deliveryAddress);
        when(cart.getUser()).thenReturn(user);
        when(cart.getUnit()).thenReturn(b2bUnit);

        when(deliveryAddress.getOwner()).thenReturn(mock(B2BUnitModel.class));
        this.vmkB2BCartValidationStrategy.validateDelivery(cart);

        verify(cart).setDeliveryAddress(null);
        verify(this.modelService).save(cart);
    }

    @Test
    public void testValidateDelivery_isAnonymous() {
        final AddressModel deliveryAddress = mock(AddressModel.class);
        final CustomerModel anonymous = mock(CustomerModel.class);
        final B2BUnitModel b2bUnit = mock(B2BUnitModel.class);
        final CartModel cart = mock(CartModel.class);

        when(cart.getDeliveryAddress()).thenReturn(deliveryAddress);
        when(cart.getUser()).thenReturn(anonymous);
        when(cart.getUnit()).thenReturn(b2bUnit);

        this.vmkB2BCartValidationStrategy.validateDelivery(cart);

        verify(cart).setDeliveryAddress(null);
        verify(this.modelService).save(cart);
    }
}