package com.vanmarcke.services.product.impl;

import com.vanmarcke.core.product.VMKPagedVariantProductDao;
import com.vanmarcke.services.strategies.VMKProductImageInheritanceUpdateStrategy;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.servicelayer.data.PaginationData;
import de.hybris.platform.core.servicelayer.data.SearchPageData;
import de.hybris.platform.variants.model.VariantProductModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Date;
import java.util.List;

import static java.util.Collections.singletonList;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isNull;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
@UnitTest
public class VMKProductImageInheritanceServiceTest {

    @Mock
    private VMKPagedVariantProductDao pagedVariantProductDao;

    @Mock
    private VMKProductImageInheritanceUpdateStrategy productImageInheritanceUpdateStrategy;

    @Captor
    private ArgumentCaptor<SearchPageData<VariantProductModel>> searchPageDataArgumentCaptor;

    @InjectMocks
    private VMKProductImageInheritanceServiceImpl defaultProductImageInheritanceService;

    @Test(expected = IllegalArgumentException.class)
    public void testUpdateProductImageInheritance_withoutCatalogVersion() throws Exception {
        defaultProductImageInheritanceService.updateProductImageInheritance(null, null);
    }

    @Test
    public void testUpdateProductImageInheritance() throws Exception {
        CatalogVersionModel catalogVersion = mock(CatalogVersionModel.class);
        Date date = mock(Date.class);
        VariantProductModel variantProduct = mock(VariantProductModel.class);

        PaginationData resultPaginationData = mock(PaginationData.class);
        when(resultPaginationData.getNumberOfPages()).thenReturn(1);

        SearchPageData<VariantProductModel> resultSearchPageData = mock(SearchPageData.class);
        when(resultSearchPageData.getPagination()).thenReturn(resultPaginationData);
        when(resultSearchPageData.getResults()).thenReturn(singletonList(variantProduct));

        when(pagedVariantProductDao.findProductsByCatalogVersionAndDate(eq(catalogVersion), eq(date), searchPageDataArgumentCaptor.capture())).thenReturn(resultSearchPageData);

        defaultProductImageInheritanceService.updateProductImageInheritance(catalogVersion, date);

        verify(productImageInheritanceUpdateStrategy).updateProductImageInheritanceForProduct(variantProduct);

        SearchPageData<VariantProductModel> searchPageData = searchPageDataArgumentCaptor.getValue();
        assertThat(searchPageData).isNotNull();
        assertThat(searchPageData.getPagination()).isNotNull();
        assertThat(searchPageData.getPagination().isNeedsTotal()).isTrue();
        assertThat(searchPageData.getPagination().getPageSize()).isEqualTo(1000);
        assertThat(searchPageData.getPagination().getCurrentPage()).isEqualTo(0);
    }

    @Test
    public void testUpdateProductImageInheritance_withMultiplePages() throws Exception {
        CatalogVersionModel catalogVersion = mock(CatalogVersionModel.class);
        Date date = mock(Date.class);
        VariantProductModel variantProduct1 = mock(VariantProductModel.class);
        VariantProductModel variantProduct2 = mock(VariantProductModel.class);

        PaginationData resultPaginationData = mock(PaginationData.class);
        when(resultPaginationData.getNumberOfPages()).thenReturn(2);

        SearchPageData<VariantProductModel> resultSearchPageData1 = mock(SearchPageData.class);
        when(resultSearchPageData1.getPagination()).thenReturn(resultPaginationData);
        when(resultSearchPageData1.getResults()).thenReturn(singletonList(variantProduct1));

        SearchPageData<VariantProductModel> resultSearchPageData2 = mock(SearchPageData.class);
        when(resultSearchPageData2.getPagination()).thenReturn(resultPaginationData);
        when(resultSearchPageData2.getResults()).thenReturn(singletonList(variantProduct2));

        when(pagedVariantProductDao.findProductsByCatalogVersionAndDate(eq(catalogVersion), eq(date), searchPageDataArgumentCaptor.capture())).thenReturn(resultSearchPageData1, resultSearchPageData2);

        defaultProductImageInheritanceService.updateProductImageInheritance(catalogVersion, date);

        verify(productImageInheritanceUpdateStrategy).updateProductImageInheritanceForProduct(variantProduct1);
        verify(productImageInheritanceUpdateStrategy).updateProductImageInheritanceForProduct(variantProduct2);

        List<SearchPageData<VariantProductModel>> searchPageDatas = searchPageDataArgumentCaptor.getAllValues();
        assertThat(searchPageDatas).hasSize(2);

        SearchPageData<VariantProductModel> searchPageData1 = searchPageDatas.get(0);
        assertThat(searchPageData1.getPagination()).isNotNull();
        assertThat(searchPageData1.getPagination().isNeedsTotal()).isTrue();
        assertThat(searchPageData1.getPagination().getPageSize()).isEqualTo(1000);
        assertThat(searchPageData1.getPagination().getCurrentPage()).isEqualTo(0);

        SearchPageData<VariantProductModel> searchPageData2 = searchPageDatas.get(1);
        assertThat(searchPageData2.getPagination()).isNotNull();
        assertThat(searchPageData2.getPagination().isNeedsTotal()).isTrue();
        assertThat(searchPageData2.getPagination().getPageSize()).isEqualTo(1000);
        assertThat(searchPageData2.getPagination().getCurrentPage()).isEqualTo(1);
    }

    @Test
    public void testUpdateProductImageInheritance_withoutDate() throws Exception {
        CatalogVersionModel catalogVersion = mock(CatalogVersionModel.class);
        VariantProductModel variantProduct = mock(VariantProductModel.class);

        PaginationData resultPaginationData = mock(PaginationData.class);
        when(resultPaginationData.getNumberOfPages()).thenReturn(1);

        SearchPageData<VariantProductModel> resultSearchPageData = mock(SearchPageData.class);
        when(resultSearchPageData.getPagination()).thenReturn(resultPaginationData);
        when(resultSearchPageData.getResults()).thenReturn(singletonList(variantProduct));

        when(pagedVariantProductDao.findProductsByCatalogVersionAndDate(eq(catalogVersion), isNull(Date.class), searchPageDataArgumentCaptor.capture())).thenReturn(resultSearchPageData);

        defaultProductImageInheritanceService.updateProductImageInheritance(catalogVersion, null);

        verify(productImageInheritanceUpdateStrategy).updateProductImageInheritanceForProduct(variantProduct);

        SearchPageData<VariantProductModel> searchPageData = searchPageDataArgumentCaptor.getValue();
        assertThat(searchPageData).isNotNull();
        assertThat(searchPageData.getPagination()).isNotNull();
        assertThat(searchPageData.getPagination().isNeedsTotal()).isTrue();
        assertThat(searchPageData.getPagination().getPageSize()).isEqualTo(1000);
        assertThat(searchPageData.getPagination().getCurrentPage()).isEqualTo(0);
    }

}