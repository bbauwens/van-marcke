package com.vanmarcke.services.model.builder;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * The {@link B2BCustomerModelMockBuilder} class can be used to create a mock implementation of the {@link B2BCustomerModel}
 * class.
 * <p>
 * TODO: VERPLAATS ALLE MOCK BUILDERS NAAR DE CORE
 *
 * @author Christiaan Janssen
 * @since 23-10-2020
 */
public class B2BCustomerModelMockBuilder extends AbstractUserModelMockBuilder<B2BCustomerModelMockBuilder, B2BCustomerModel> {

    private B2BUnitModel defaultB2BUnit;

    /**
     * Private constructor hides the default implicit one.
     */
    private B2BCustomerModelMockBuilder() {
        super(B2BCustomerModelMockBuilder.class);
    }

    /**
     * The entry point to the {@link B2BCustomerModelMockBuilder}.
     *
     * @return the {@link B2BCustomerModelMockBuilder} instance for method chaining
     */
    public static B2BCustomerModelMockBuilder aB2BCustomer() {
        return new B2BCustomerModelMockBuilder();
    }

    /**
     * Sets the {@code defaultB2BUnit} on the current {@link B2BCustomerModelMockBuilder} instance.
     *
     * @param defaultB2BUnit the user's default B2B unit
     * @return the {@link B2BCustomerModelMockBuilder} instance for method chaining
     */
    public B2BCustomerModelMockBuilder withDefaultB2BUnit(final B2BUnitModel defaultB2BUnit) {
        this.defaultB2BUnit = defaultB2BUnit;
        return this;
    }

    /**
     * Creates a mock implementation of the {@link B2BCustomerModel} class.
     *
     * @return the {@link B2BCustomerModel} instance
     */
    public B2BCustomerModel build() {
        final B2BCustomerModel customer = mock(B2BCustomerModel.class);
        when(customer.getUid()).thenReturn(uid);
        when(customer.getCustomerID()).thenReturn(customerID);
        when(customer.getName()).thenReturn(name);
        when(customer.getActive()).thenReturn(active);
        when(customer.getType()).thenReturn(type);
        when(customer.getDefaultB2BUnit()).thenReturn(defaultB2BUnit);
        when(customer.getGroups()).thenReturn(groups);
        when(customer.getAddresses()).thenReturn(addresses);
        return customer;
    }
}
