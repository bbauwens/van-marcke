package com.vanmarcke.services.strategies.impl;

import com.vanmarcke.services.model.builder.*;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.delivery.dao.CountryZoneDeliveryModeDao;
import de.hybris.platform.commerceservices.delivery.dao.PickupDeliveryModeDao;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import org.fest.assertions.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;
import java.util.Locale;

import static org.mockito.Mockito.*;

/**
 * The {@link VMKDeliveryModeLookupStrategyImplTest} class contains the unit test for the
 * {@link VMKDeliveryModeLookupStrategyImpl} class.
 *
 * @author Christiaan Janssen
 * @since 07-07-2020
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKDeliveryModeLookupStrategyImplTest {

    @Mock
    private PickupDeliveryModeDao pickupDeliveryModeDao;

    @Mock
    private CountryZoneDeliveryModeDao countryZoneDeliveryModeDao;

    @InjectMocks
    VMKDeliveryModeLookupStrategyImpl deliveryModeLookupStrategy;

    @Test
    public void testGetSelectableDeliveryModesForOrderWithValidProduct() {
        ProductModel product = ProductModelMockBuilder
                .aBaseProduct(null, Locale.ENGLISH)
                .withDeliveryMethod("A")
                .build();

        AbstractOrderEntryModel entry = AbstractOrderEntryModelMockBuilder
                .anAbstractOrderEntry()
                .withProduct(product)
                .build();

        CountryModel country = CountryModelMockBuilder
                .aCountry()
                .build();

        AddressModel address = AddressModelMockBuilder
                .anAddress()
                .withCountry(country)
                .build();

        CurrencyModel currency = CurrencyModelMockBuilder
                .aCurrency(null)
                .build();

        AbstractOrderModel order = AbstractOrderModelMockBuilder
                .anAbstractOrder()
                .withEntries(entry)
                .withDeliveryAddress(address)
                .withCurrency(currency)
                .build();

        when(pickupDeliveryModeDao.findPickupDeliveryModesForAbstractOrder(order)).thenReturn(Collections.emptyList());

        DeliveryModeModel deliveryMode = mock(DeliveryModeModel.class);

        when(countryZoneDeliveryModeDao.findDeliveryModes(order)).thenReturn(Collections.singletonList(deliveryMode));

        List<DeliveryModeModel> actualDeliveryModes = deliveryModeLookupStrategy.getSelectableDeliveryModesForOrder(order);

        Assertions
                .assertThat(actualDeliveryModes)
                .containsExactly(deliveryMode);

        verify(pickupDeliveryModeDao).findPickupDeliveryModesForAbstractOrder(order);
        verify(countryZoneDeliveryModeDao).findDeliveryModes(order);
    }

    @Test
    public void testGetSelectableDeliveryModesForOrderWithDirectSupplyProduct() {
        ProductModel product = ProductModelMockBuilder
                .aBaseProduct(null, Locale.ENGLISH)
                .withDeliveryMethod("S")
                .build();

        AbstractOrderEntryModel entry = AbstractOrderEntryModelMockBuilder
                .anAbstractOrderEntry()
                .withProduct(product)
                .build();

        CountryModel country = CountryModelMockBuilder
                .aCountry()
                .build();

        AddressModel address = AddressModelMockBuilder
                .anAddress()
                .withCountry(country)
                .build();

        CurrencyModel currency = CurrencyModelMockBuilder
                .aCurrency(null)
                .build();

        AbstractOrderModel order = AbstractOrderModelMockBuilder
                .anAbstractOrder()
                .withEntries(entry)
                .withDeliveryAddress(address)
                .withCurrency(currency)
                .build();

        when(pickupDeliveryModeDao.findPickupDeliveryModesForAbstractOrder(order)).thenReturn(Collections.emptyList());

        List<DeliveryModeModel> actualDeliveryModes = deliveryModeLookupStrategy.getSelectableDeliveryModesForOrder(order);

        Assertions
                .assertThat(actualDeliveryModes)
                .isEmpty();

        verify(pickupDeliveryModeDao).findPickupDeliveryModesForAbstractOrder(order);
        verify(countryZoneDeliveryModeDao, never()).findDeliveryModes(order);
    }

    @Test
    public void testGetSelectableDeliveryModesForOrderWithCrossDocProduct() {
        ProductModel product = ProductModelMockBuilder
                .aBaseProduct(null, Locale.ENGLISH)
                .withDeliveryMethod("X")
                .build();

        AbstractOrderEntryModel entry = AbstractOrderEntryModelMockBuilder
                .anAbstractOrderEntry()
                .withProduct(product)
                .build();

        CountryModel country = CountryModelMockBuilder
                .aCountry()
                .build();

        AddressModel address = AddressModelMockBuilder
                .anAddress()
                .withCountry(country)
                .build();

        CurrencyModel currency = CurrencyModelMockBuilder
                .aCurrency(null)
                .build();

        AbstractOrderModel order = AbstractOrderModelMockBuilder
                .anAbstractOrder()
                .withEntries(entry)
                .withDeliveryAddress(address)
                .withCurrency(currency)
                .build();

        when(pickupDeliveryModeDao.findPickupDeliveryModesForAbstractOrder(order)).thenReturn(Collections.emptyList());

        List<DeliveryModeModel> actualDeliveryModes = deliveryModeLookupStrategy.getSelectableDeliveryModesForOrder(order);

        Assertions
                .assertThat(actualDeliveryModes)
                .isEmpty();

        verify(pickupDeliveryModeDao).findPickupDeliveryModesForAbstractOrder(order);
        verify(countryZoneDeliveryModeDao, never()).findDeliveryModes(order);
    }
}
