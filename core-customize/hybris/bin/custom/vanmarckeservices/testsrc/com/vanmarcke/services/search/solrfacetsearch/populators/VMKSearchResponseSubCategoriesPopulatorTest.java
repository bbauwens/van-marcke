package com.vanmarcke.services.search.solrfacetsearch.populators;

import com.vanmarcke.services.model.builder.CategoryModelMockBuilder;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.category.CommerceCategoryService;
import de.hybris.platform.solrfacetsearch.search.Facet;
import de.hybris.platform.solrfacetsearch.search.FacetValue;
import de.hybris.platform.solrfacetsearch.search.impl.SolrSearchResult;
import org.apache.commons.lang3.RandomStringUtils;
import org.assertj.core.util.Lists;
import org.fest.assertions.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

/**
 * The {@code VMKSearchResponseSubCategoriesPopulatorTest} class contains the unit tests for the
 * {@link VMKSearchResponseSubCategoriesPopulator} class.
 *
 * @author Christiaan Janssen
 * @since 20-04-2020
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKSearchResponseSubCategoriesPopulatorTest {

    private static final String CATEGORY_CODE = RandomStringUtils.randomAlphabetic(10);

    private static final String SUBCATEGORY_A_CODE = "001";
    private static final String SUBCATEGORY_B_CODE = "003";
    private static final String SUBCATEGORY_C_CODE = "002";

    private static final String PATH_PREFIX = "/" + CATEGORY_CODE + "/";
    private static final String SUBCATEGORY_A_PATH = PATH_PREFIX + SUBCATEGORY_A_CODE;
    private static final String SUBCATEGORY_B_PATH = PATH_PREFIX + SUBCATEGORY_B_CODE;
    private static final String SUBCATEGORY_C_PATH = PATH_PREFIX + SUBCATEGORY_C_CODE;

    @Mock
    private CommerceCategoryService commerceCategoryService;

    @InjectMocks
    private VMKSearchResponseSubCategoriesPopulator populator;

    @Test
    public void testBuildSubcategoriesWithoutSearchResult() {
        List<CategoryModel> expectedSubcategories = populator.buildSubCategories(CATEGORY_CODE, null);

        Assertions
                .assertThat(expectedSubcategories)
                .isNull();

        verifyZeroInteractions(commerceCategoryService);
    }

    @Test
    public void testBuildSubcategoriesWithEmptySearchResult() {
        SolrSearchResult searchResult = new SolrSearchResult();

        List<CategoryModel> expectedSubcategories = populator.buildSubCategories(CATEGORY_CODE, searchResult);

        Assertions
                .assertThat(expectedSubcategories)
                .isNull();

        verifyZeroInteractions(commerceCategoryService);
    }

    @Test
    public void testBuildSubcategoriesWithoutCategoryCode() {
        SolrSearchResult searchResult = new SolrSearchResult();
        searchResult.setNumberOfResults(1);

        List<CategoryModel> expectedSubcategories = populator.buildSubCategories(null, searchResult);

        Assertions
                .assertThat(expectedSubcategories)
                .isNull();

        verifyZeroInteractions(commerceCategoryService);
    }

    @Test
    public void testBuildSubcategoriesWithoutCategoryPathFacet() {
        Map<String, Facet> facets = new HashMap<>();

        SolrSearchResult searchResult = new SolrSearchResult();
        searchResult.setNumberOfResults(1);
        searchResult.setFacetsMap(facets);

        List<CategoryModel> expectedSubcategories = populator.buildSubCategories(CATEGORY_CODE, searchResult);

        Assertions
                .assertThat(expectedSubcategories)
                .isNull();

        verifyZeroInteractions(commerceCategoryService);
    }

    @Test
    public void testBuildSubcategoriesWithoutCategory() {
        FacetValue valueA = new FacetValue(SUBCATEGORY_A_PATH, 1, true);
        FacetValue valueB = new FacetValue(SUBCATEGORY_B_PATH, 2, false);
        FacetValue valueC = new FacetValue(SUBCATEGORY_C_PATH, 3, false);

        Facet facet = new Facet("categoryPath", Lists.newArrayList(valueA, valueB, valueC));

        Map<String, Facet> facets = new HashMap<>();
        facets.put("categoryPath", facet);

        SolrSearchResult searchResult = new SolrSearchResult();
        searchResult.setNumberOfResults(1);
        searchResult.setFacetsMap(facets);

        List<CategoryModel> expectedSubcategories = populator.buildSubCategories(CATEGORY_CODE, searchResult);

        Assertions
                .assertThat(expectedSubcategories)
                .isNull();

        verify(commerceCategoryService).getCategoryForCode(CATEGORY_CODE);
    }

    @Test
    public void testBuildSubcategories() {
        FacetValue valueA = new FacetValue(SUBCATEGORY_A_PATH, 1, true);
        FacetValue valueB = new FacetValue(SUBCATEGORY_B_PATH, 2, false);
        FacetValue valueC = new FacetValue(SUBCATEGORY_C_PATH, 3, false);

        Facet facet = new Facet("categoryPath", Lists.newArrayList(valueA, valueB, valueC));

        Map<String, Facet> facets = new HashMap<>();
        facets.put("categoryPath", facet);

        SolrSearchResult searchResult = new SolrSearchResult();
        searchResult.setNumberOfResults(1);
        searchResult.setFacetsMap(facets);

        CategoryModel category = CategoryModelMockBuilder
                .aCategory()
                .withCode(CATEGORY_CODE)
                .build();

        when(commerceCategoryService.getCategoryForCode(CATEGORY_CODE)).thenReturn(category);
        when(commerceCategoryService.getPathsForCategory(category)).thenReturn(Collections.singletonList(Collections.singletonList(category)));

        CategoryModel subcategoryA = CategoryModelMockBuilder
                .aCategory()
                .withCode(SUBCATEGORY_A_CODE)
                .build();

        when(commerceCategoryService.getCategoryForCode(SUBCATEGORY_A_CODE)).thenReturn(subcategoryA);

        CategoryModel subcategoryB = CategoryModelMockBuilder
                .aCategory()
                .withCode(SUBCATEGORY_B_CODE)
                .build();

        when(commerceCategoryService.getCategoryForCode(SUBCATEGORY_B_CODE)).thenReturn(subcategoryB);

        CategoryModel subcategoryC = CategoryModelMockBuilder
                .aCategory()
                .withCode(SUBCATEGORY_C_CODE)
                .build();

        when(commerceCategoryService.getCategoryForCode(SUBCATEGORY_C_CODE)).thenReturn(subcategoryC);

        List<CategoryModel> expectedSubcategories = populator.buildSubCategories(CATEGORY_CODE, searchResult);

        Assertions
                .assertThat(expectedSubcategories)
                .containsExactly(subcategoryA, subcategoryC, subcategoryB);

        verify(commerceCategoryService).getCategoryForCode(CATEGORY_CODE);
        verify(commerceCategoryService).getPathsForCategory(category);
        verify(commerceCategoryService).getCategoryForCode(SUBCATEGORY_A_CODE);
        verify(commerceCategoryService).getCategoryForCode(SUBCATEGORY_B_CODE);
        verify(commerceCategoryService).getCategoryForCode(SUBCATEGORY_C_CODE);
    }
}