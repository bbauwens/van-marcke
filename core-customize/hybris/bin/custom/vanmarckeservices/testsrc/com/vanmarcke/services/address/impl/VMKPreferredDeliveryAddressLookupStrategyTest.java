package com.vanmarcke.services.address.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.commerceservices.strategies.DeliveryAddressesLookupStrategy;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKPreferredDeliveryAddressLookupStrategyTest {

    @Mock
    private B2BUnitModel b2BUnit;
    @Mock
    private DeliveryAddressesLookupStrategy deliveryAddressesLookupStrategy;

    @InjectMocks
    private VMKPreferredDeliveryAddressLookupStrategyImpl vmkPreferredDeliveryAddressLookupStrategy;

    private AbstractOrderModel abstractOrder;

    @Before
    public void setUp() {
        this.abstractOrder = mock(AbstractOrderModel.class);
    }

    @Test
    public void testGetDefaultDeliveryAddressForOrder_defaultUnitAddress() {
        final AddressModel shippingAddress = mock(AddressModel.class);
        final AddressModel defaultBillingAddress = mock(AddressModel.class);
        final CountryModel country = mock(CountryModel.class);

        when(this.abstractOrder.getUnit()).thenReturn(this.b2BUnit);
        when(this.b2BUnit.getShippingAddress()).thenReturn(shippingAddress);
        when(this.b2BUnit.getBillingAddress()).thenReturn(defaultBillingAddress);
        when(shippingAddress.getCountry()).thenReturn(country);
        when(defaultBillingAddress.getCountry()).thenReturn(country);

        final AddressModel result = this.vmkPreferredDeliveryAddressLookupStrategy.getPreferredDeliveryAddressForOrder(this.abstractOrder);

        assertThat(result).isEqualTo(shippingAddress);
    }

    @Test
    public void testGetDefaultDeliveryAddressForOrder_fallbackAddresses() {
        final AddressModel shippingAddress = mock(AddressModel.class);
        final AddressModel defaultBillingAddress = mock(AddressModel.class);
        final CountryModel country = mock(CountryModel.class);
        final List<AddressModel> shippingAddresses = new ArrayList<>();
        shippingAddresses.add(shippingAddress);

        when(this.abstractOrder.getUnit()).thenReturn(this.b2BUnit);
        when(this.b2BUnit.getBillingAddress()).thenReturn(defaultBillingAddress);
        when(this.deliveryAddressesLookupStrategy.getDeliveryAddressesForOrder(this.abstractOrder, true)).thenReturn(shippingAddresses);
        when(shippingAddress.getCountry()).thenReturn(country);
        when(defaultBillingAddress.getCountry()).thenReturn(country);
        final AddressModel result = this.vmkPreferredDeliveryAddressLookupStrategy.getPreferredDeliveryAddressForOrder(this.abstractOrder);

        verify(this.deliveryAddressesLookupStrategy).getDeliveryAddressesForOrder(this.abstractOrder, true);
        assertThat(result).isEqualTo(shippingAddress);
    }

    @Test
    public void testGetDefaultDeliveryAddressForOrder_noAddressFound() {
        when(this.abstractOrder.getUnit()).thenReturn(this.b2BUnit);
        when(this.deliveryAddressesLookupStrategy.getDeliveryAddressesForOrder(this.abstractOrder, true)).thenReturn(null);
        final AddressModel result = this.vmkPreferredDeliveryAddressLookupStrategy.getPreferredDeliveryAddressForOrder(this.abstractOrder);

        verify(this.deliveryAddressesLookupStrategy).getDeliveryAddressesForOrder(this.abstractOrder, true);
        assertThat(result).isNull();
    }
}