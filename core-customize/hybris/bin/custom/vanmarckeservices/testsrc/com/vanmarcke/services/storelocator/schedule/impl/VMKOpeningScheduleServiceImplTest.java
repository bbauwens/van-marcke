package com.vanmarcke.services.storelocator.schedule.impl;

import com.vanmarcke.services.model.builder.OpeningScheduleModelMockBuilder;
import com.vanmarcke.services.model.builder.PointOfServiceModelMockBuilder;
import com.vanmarcke.services.model.builder.SpecialOpeningDayModelMockBuilder;
import com.vanmarcke.services.model.builder.WeekdayOpeningDayModelMockBuilder;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.WeekDay;
import de.hybris.platform.storelocator.model.*;
import org.fest.assertions.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;

import static com.vanmarcke.services.constants.VanmarckeservicesConstants.WEEK_DAYS;
import static java.util.Collections.singletonList;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKOpeningScheduleServiceImplTest {

    private Date date;

    @Spy
    @InjectMocks
    private VMKOpeningScheduleServiceImpl openingScheduleService;

    @Before
    public void setUp() {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);

        date = c.getTime();
    }

    @Test
    public void testGetOpeningDayFromScheduleForGivenDay_withoutOpeningSchedule() {
        OpeningDayModel result = openingScheduleService.getOpeningDayFromScheduleForGivenDay(null, WeekDay.TUESDAY);

        assertThat(result).isNull();
    }

    @Test
    public void testGetOpeningDayFromScheduleForGivenDay() {
        OpeningScheduleModel openingScheduleModel = mock(OpeningScheduleModel.class);
        WeekdayOpeningDayModel weekdayOpeningDayModel = mock(WeekdayOpeningDayModel.class);

        when(weekdayOpeningDayModel.getDayOfWeek()).thenReturn(WeekDay.TUESDAY);

        when(openingScheduleModel.getOpeningDays()).thenReturn(singletonList(weekdayOpeningDayModel));

        OpeningDayModel result = openingScheduleService.getOpeningDayFromScheduleForGivenDay(openingScheduleModel, WeekDay.TUESDAY);

        assertThat(result).isEqualTo(weekdayOpeningDayModel);
    }

    @Test
    public void testGetOpeningDayFromScheduleForGivenDay_notFound() {
        OpeningScheduleModel openingScheduleModel = mock(OpeningScheduleModel.class);
        WeekdayOpeningDayModel weekdayOpeningDayModel = mock(WeekdayOpeningDayModel.class);

        when(weekdayOpeningDayModel.getDayOfWeek()).thenReturn(WeekDay.WEDNESDAY);

        when(openingScheduleModel.getOpeningDays()).thenReturn(singletonList(weekdayOpeningDayModel));

        OpeningDayModel result = openingScheduleService.getOpeningDayFromScheduleForGivenDay(openingScheduleModel, WeekDay.TUESDAY);

        assertThat(result).isNull();
    }

    @Test
    public void testIsOpenWithoutOpeningDays() {
        OpeningScheduleModel openingSchedule = OpeningScheduleModelMockBuilder
                .anOpeningSchedule()
                .build();

        PointOfServiceModel pos = PointOfServiceModelMockBuilder
                .aPointOfService()
                .withOpeningSchedule(openingSchedule)
                .build();

        boolean actualResult = openingScheduleService.isStoreOpenOnGivenDate(pos, date);

        Assertions
                .assertThat(actualResult)
                .isFalse();
    }

    @Test
    public void testIsOpenWithoutOpeningDaysWithSpecialOpeningDay() {
        SpecialOpeningDayModel openingDay = SpecialOpeningDayModelMockBuilder
                .aSpecialOpeningDay()
                .withDate(date)
                .withClosed(false)
                .build();

        OpeningScheduleModel openingSchedule = OpeningScheduleModelMockBuilder
                .anOpeningSchedule()
                .withOpeningDays(openingDay)
                .build();

        PointOfServiceModel pos = PointOfServiceModelMockBuilder
                .aPointOfService()
                .withOpeningSchedule(openingSchedule)
                .build();

        doReturn(true).when(openingScheduleService).isBeforeClosingTime(eq(openingDay), any(LocalTime.class));

        boolean actualResult = openingScheduleService.isStoreOpenOnGivenDate(pos, date);

        Assertions
                .assertThat(actualResult)
                .isTrue();
    }

    @Test
    public void testIsOpenWithoutOpeningDaysWithWeekdayOpeningDay() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        WeekdayOpeningDayModel openingDay = WeekdayOpeningDayModelMockBuilder
                .aWeekdayOpeningDay()
                .withDayOfWeek(WEEK_DAYS[calendar.get(Calendar.DAY_OF_WEEK) - 1])
                .build();

        OpeningScheduleModel openingSchedule = OpeningScheduleModelMockBuilder
                .anOpeningSchedule()
                .withOpeningDays(openingDay)
                .build();

        PointOfServiceModel pos = PointOfServiceModelMockBuilder
                .aPointOfService()
                .withOpeningSchedule(openingSchedule)
                .build();

        doReturn(true).when(openingScheduleService).isBeforeClosingTime(eq(openingDay), any());

        boolean actualResult = openingScheduleService.isStoreOpenOnGivenDate(pos, date);

        Assertions
                .assertThat(actualResult)
                .isTrue();
    }

    @Test
    public void testIsOpenWithTimeInBetweenOpeningAndClosingTimes() {
        Date openingTime = Date.from(LocalTime.of(9, 0)
                .atDate(LocalDate.now())
                .atZone(ZoneId.systemDefault()).toInstant());

        Date closingTime = Date.from(LocalTime.of(17, 0)
                .atDate(LocalDate.now())
                .atZone(ZoneId.systemDefault()).toInstant());

        LocalTime currentTime = LocalTime.of(10, 0);

        WeekdayOpeningDayModel openingDay = WeekdayOpeningDayModelMockBuilder
                .aWeekdayOpeningDay()
                .withOpeningTime(openingTime)
                .withClosingTime(closingTime)
                .build();

        boolean actualResult = openingScheduleService.isBeforeClosingTime(openingDay, currentTime);

        Assertions
                .assertThat(actualResult)
                .isTrue();
    }

    @Test
    public void testIsOpenWithoutTimeAndInvalidOpeningHours() {
        Date closingTime = Date.from(LocalTime.of(17, 0)
                .atDate(LocalDate.now())
                .atZone(ZoneId.systemDefault()).toInstant());

        WeekdayOpeningDayModel openingDay = WeekdayOpeningDayModelMockBuilder
                .aWeekdayOpeningDay()
                .withClosingTime(closingTime)
                .build();

        boolean actualResult = openingScheduleService.isBeforeClosingTime(openingDay, null);

        Assertions
                .assertThat(actualResult)
                .isTrue();
    }

    @Test
    public void testIsOpenWithoutTimeAndValidOpeningHours() {
        Date openingTime = Date.from(LocalTime.of(9, 0)
                .atDate(LocalDate.now())
                .atZone(ZoneId.systemDefault()).toInstant());

        Date closingTime = Date.from(LocalTime.of(17, 0)
                .atDate(LocalDate.now())
                .atZone(ZoneId.systemDefault()).toInstant());

        WeekdayOpeningDayModel openingDay = WeekdayOpeningDayModelMockBuilder
                .aWeekdayOpeningDay()
                .withOpeningTime(openingTime)
                .withClosingTime(closingTime)
                .build();

        boolean actualResult = openingScheduleService.isBeforeClosingTime(openingDay, null);

        Assertions
                .assertThat(actualResult)
                .isTrue();
    }

    @Test
    public void testIsOpenWithTimeEqualToOpeningTime() {
        Date openingTime = Date.from(LocalTime.of(9, 0)
                .atDate(LocalDate.now())
                .atZone(ZoneId.systemDefault()).toInstant());

        Date closingTime = Date.from(LocalTime.of(17, 0)
                .atDate(LocalDate.now())
                .atZone(ZoneId.systemDefault()).toInstant());

        LocalTime currentTime = LocalTime.of(9, 0);

        WeekdayOpeningDayModel openingDay = WeekdayOpeningDayModelMockBuilder
                .aWeekdayOpeningDay()
                .withOpeningTime(openingTime)
                .withClosingTime(closingTime)
                .build();

        boolean actualResult = openingScheduleService.isBeforeClosingTime(openingDay, currentTime);

        Assertions
                .assertThat(actualResult)
                .isTrue();
    }

    @Test
    public void testIsOpenWithTimeBeforeOpeningTime() {
        Date openingTime = Date.from(LocalTime.of(9, 0)
                .atDate(LocalDate.now())
                .atZone(ZoneId.systemDefault()).toInstant());

        Date closingTime = Date.from(LocalTime.of(17, 0)
                .atDate(LocalDate.now())
                .atZone(ZoneId.systemDefault()).toInstant());

        LocalTime currentTime = LocalTime.of(8, 0);

        WeekdayOpeningDayModel openingDay = WeekdayOpeningDayModelMockBuilder
                .aWeekdayOpeningDay()
                .withOpeningTime(openingTime)
                .withClosingTime(closingTime)
                .build();

        boolean actualResult = openingScheduleService.isBeforeClosingTime(openingDay, currentTime);

        Assertions
                .assertThat(actualResult)
                .isTrue();
    }

    @Test
    public void testIsOpenWithTimeEqualToClosingTime() {
        Date openingTime = Date.from(LocalTime.of(9, 0)
                .atDate(LocalDate.now())
                .atZone(ZoneId.systemDefault()).toInstant());

        Date closingTime = Date.from(LocalTime.of(17, 0)
                .atDate(LocalDate.now())
                .atZone(ZoneId.systemDefault()).toInstant());

        LocalTime currentTime = LocalTime.of(17, 0);

        WeekdayOpeningDayModel openingDay = WeekdayOpeningDayModelMockBuilder
                .aWeekdayOpeningDay()
                .withOpeningTime(openingTime)
                .withClosingTime(closingTime)
                .build();

        boolean actualResult = openingScheduleService.isBeforeClosingTime(openingDay, currentTime);

        Assertions
                .assertThat(actualResult)
                .isFalse();
    }

    @Test
    public void testIsOpenWithTimeAfterClosingTime() {
        Date openingTime = Date.from(LocalTime.of(9, 0)
                .atDate(LocalDate.now())
                .atZone(ZoneId.systemDefault()).toInstant());

        Date closingTime = Date.from(LocalTime.of(17, 0)
                .atDate(LocalDate.now())
                .atZone(ZoneId.systemDefault()).toInstant());

        LocalTime currentTime = LocalTime.of(18, 0);

        WeekdayOpeningDayModel openingDay = WeekdayOpeningDayModelMockBuilder
                .aWeekdayOpeningDay()
                .withOpeningTime(openingTime)
                .withClosingTime(closingTime)
                .build();

        boolean actualResult = openingScheduleService.isBeforeClosingTime(openingDay, currentTime);

        Assertions
                .assertThat(actualResult)
                .isFalse();
    }

}