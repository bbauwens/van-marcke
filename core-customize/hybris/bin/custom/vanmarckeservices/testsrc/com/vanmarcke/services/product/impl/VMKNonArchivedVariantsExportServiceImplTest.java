package com.vanmarcke.services.product.impl;

import com.vanmarcke.core.enums.DeaveragingStatus;
import com.vanmarcke.core.product.VMKPagedVariantProductDao;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.catalog.model.classification.ClassificationSystemModel;
import de.hybris.platform.catalog.model.classification.ClassificationSystemVersionModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.servicelayer.data.PaginationData;
import de.hybris.platform.core.servicelayer.data.SearchPageData;
import de.hybris.platform.variants.model.VariantProductModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;
import java.util.Locale;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKNonArchivedVariantsExportServiceImplTest {

    @Mock
    private VMKPagedVariantProductDao pagedVariantProductDao;
    @InjectMocks
    public VMKNonArchivedVariantsExportServiceImpl defaultVMKNonArchivedVariantsExportService;

    @Test
    public void testGenerateNonArchivedVariantsExportFeed_Etim() {
        CatalogVersionModel catalogVersion = mock(CatalogVersionModel.class);
        Locale locale = new Locale("nl");
        SearchPageData searchPageData = mock(SearchPageData.class);
        PaginationData paginationData = mock(PaginationData.class);
        VariantProductModel variant = mock(VariantProductModel.class);
        ProductModel baseProduct = mock(ProductModel.class);
        CategoryModel superCategory = mock(CategoryModel.class);
        ClassificationClassModel etimClassification = mock(ClassificationClassModel.class);
        ClassificationSystemVersionModel classificationCatalogVersion = mock(ClassificationSystemVersionModel.class);
        ClassificationSystemModel classificationCatalog = mock(ClassificationSystemModel.class);

        when(pagedVariantProductDao.findNonArchivedProductsByCatalogVersion(eq(catalogVersion), any(SearchPageData.class))).thenReturn(searchPageData);
        when(searchPageData.getResults()).thenReturn(Collections.singletonList(variant));
        when(searchPageData.getPagination()).thenReturn(paginationData);
        when(paginationData.getNumberOfPages()).thenReturn(1);
        when(variant.getCode()).thenReturn("variantCode");
        when(variant.getBaseProduct()).thenReturn(baseProduct);
        when(variant.getDeaveraging()).thenReturn(DeaveragingStatus.DEVELOP);
        when(baseProduct.getCode()).thenReturn("baseProductCode");
        when(baseProduct.getSupercategories()).thenReturn(Collections.singletonList(superCategory));
        when(superCategory.getCode()).thenReturn("superCatCode");
        when(superCategory.getName(locale)).thenReturn("Nederlandse supercategorie naam");
        when(variant.getClassificationClasses()).thenReturn(Collections.singletonList(etimClassification));
        when(etimClassification.getCatalogVersion()).thenReturn(classificationCatalogVersion);
        when(classificationCatalogVersion.getCatalog()).thenReturn(classificationCatalog);
        when(classificationCatalog.getId()).thenReturn("EtimClassification");
        when(etimClassification.getCode()).thenReturn("E0001");
        when(etimClassification.getName(locale)).thenReturn("Nederlandse Etim Classificatie naam");
        List<String> result = defaultVMKNonArchivedVariantsExportService.generateNonArchivedVariantsExportFeed(catalogVersion);

        verify(pagedVariantProductDao).findNonArchivedProductsByCatalogVersion(eq(catalogVersion), any(SearchPageData.class));
        assertThat(result).isNotEmpty();
        assertThat(result.get(0)).isEqualTo("variantCode;baseProductCode;superCatCode;Nederlandse supercategorie naam;E0001;Nederlandse Etim Classificatie naam;;;DEVELOP");
    }

    @Test
    public void testGenerateNonArchivedVariantsExportFeed_GS1() {
        CatalogVersionModel catalogVersion = mock(CatalogVersionModel.class);
        Locale locale = new Locale("nl");
        SearchPageData searchPageData = mock(SearchPageData.class);
        PaginationData paginationData = mock(PaginationData.class);
        VariantProductModel variant = mock(VariantProductModel.class);
        ProductModel baseProduct = mock(ProductModel.class);
        CategoryModel superCategory = mock(CategoryModel.class);
        ClassificationClassModel gs1Classification = mock(ClassificationClassModel.class);
        ClassificationSystemVersionModel classificationCatalogVersion = mock(ClassificationSystemVersionModel.class);
        ClassificationSystemModel classificationCatalog = mock(ClassificationSystemModel.class);

        when(pagedVariantProductDao.findNonArchivedProductsByCatalogVersion(eq(catalogVersion), any(SearchPageData.class))).thenReturn(searchPageData);
        when(searchPageData.getResults()).thenReturn(Collections.singletonList(variant));
        when(searchPageData.getPagination()).thenReturn(paginationData);
        when(paginationData.getNumberOfPages()).thenReturn(1);
        when(variant.getCode()).thenReturn("variantCode");
        when(variant.getBaseProduct()).thenReturn(baseProduct);
        when(variant.getDeaveraging()).thenReturn(DeaveragingStatus.DEVELOP);
        when(baseProduct.getCode()).thenReturn("baseProductCode");
        when(baseProduct.getSupercategories()).thenReturn(Collections.singletonList(superCategory));
        when(superCategory.getCode()).thenReturn("superCatCode");
        when(superCategory.getName(locale)).thenReturn("Nederlandse supercategorie naam");
        when(variant.getClassificationClasses()).thenReturn(Collections.singletonList(gs1Classification));
        when(gs1Classification.getCatalogVersion()).thenReturn(classificationCatalogVersion);
        when(classificationCatalogVersion.getCatalog()).thenReturn(classificationCatalog);
        when(classificationCatalog.getId()).thenReturn("GS1Classification");
        when(gs1Classification.getCode()).thenReturn("GS1Code");
        when(gs1Classification.getName(locale)).thenReturn("Nederlandse GS1 Classificatie naam");
        List<String> result = defaultVMKNonArchivedVariantsExportService.generateNonArchivedVariantsExportFeed(catalogVersion);

        verify(pagedVariantProductDao).findNonArchivedProductsByCatalogVersion(eq(catalogVersion), any(SearchPageData.class));
        assertThat(result).isNotEmpty();
        assertThat(result.get(0)).isEqualTo("variantCode;baseProductCode;superCatCode;Nederlandse supercategorie naam;;;GS1Code;Nederlandse GS1 Classificatie naam;DEVELOP");
    }
}