package com.vanmarcke.services.job.maintenance.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKCleanupBusinessProcessStrategyImplTest {

    private VMKCleanupBusinessProcessStrategyImpl cleanupStrategy = new VMKCleanupBusinessProcessStrategyImpl();

    @Test
    public void testGetQuery() {
        String result = cleanupStrategy.getQuery();
        assertThat(result).isEqualTo("SELECT {pk} FROM {BusinessProcess} WHERE {state} IN (?state) AND {modifiedtime} < ?date");
    }

}