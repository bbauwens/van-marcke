package com.vanmarcke.services.order.cart.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2b.services.B2BUnitService;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKB2BCartFactoryImplTest {

    @Mock
    private B2BUnitService<B2BUnitModel, B2BCustomerModel> b2bUnitService;
    @Mock
    private ModelService modelService;
    @Mock
    private I18NService i18nService;

    @InjectMocks
    private VMKB2BCartFactoryImpl vmkB2BCartFactory;

    @Test
    public void isB2BCartReturnsTrueWhenUserIsB2BCustomer() {
        CartModel cart = mock(CartModel.class);
        when(cart.getUser()).thenReturn(mock(B2BCustomerModel.class));

        boolean actual = vmkB2BCartFactory.isB2BCart(cart);

        assertThat(actual).isTrue();
    }

    @Test
    public void isB2BCartReturnsTrueWhenUserIsNull() {
        CartModel cart = mock(CartModel.class);
        when(cart.getUser()).thenReturn(null);

        boolean actual = vmkB2BCartFactory.isB2BCart(cart);

        assertThat(actual).isFalse();
    }

    @Test
    public void isB2BCartReturnsTrueWhenUserIsNotAB2BCustomer() {
        CartModel cart = mock(CartModel.class);
        when(cart.getUser()).thenReturn(mock(CustomerModel.class));

        boolean actual = vmkB2BCartFactory.isB2BCart(cart);

        assertThat(actual).isFalse();
    }
}