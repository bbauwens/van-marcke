package com.vanmarcke.services.search.solrfacetsearch.provider.impl;

import com.vanmarcke.services.storelocator.schedule.VMKOpeningScheduleService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.WeekDay;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolverTest;
import de.hybris.platform.storelocator.model.OpeningScheduleModel;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import de.hybris.platform.storelocator.model.WeekdayOpeningDayModel;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;
import java.util.Date;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKOpeningClosingTimeValueResolverTest extends AbstractValueResolverTest {

    @InjectMocks
    VMKOpeningClosingTimeValueResolver openingClosingTimeValueResolver;

    @Mock
    private ModelService modelService;
    @Mock
    private InputDocument inputDocument;
    @Mock
    private IndexerBatchContext indexerBatchContext;
    @Mock
    private PointOfServiceModel posModel;
    @Mock
    private IndexedProperty indexedProperty;
    @Mock
    private VMKOpeningScheduleService openingScheduleService;
    @Mock
    OpeningScheduleModel openingScheduleModel;
    @Mock
    WeekdayOpeningDayModel weekdayOpeningDayModel;

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void testClosingTimeGetsAddedProperly() throws FieldValueProviderException {
        when(indexedProperty.getName()).thenReturn("closingTime");
        when(posModel.getDescription()).thenReturn("modelDescription");
        when(posModel.getOpeningSchedule()).thenReturn(openingScheduleModel);
        when(weekdayOpeningDayModel.getClosingTime()).thenReturn(new Date(100));
        when(openingScheduleService.getOpeningDayFromScheduleForGivenDay(openingScheduleModel, WeekDay.MONDAY)).thenReturn(weekdayOpeningDayModel);
        when(modelService.getAttributeValue(weekdayOpeningDayModel, "closingTime")).thenReturn(new Date(150));

        openingClosingTimeValueResolver.resolve(inputDocument, indexerBatchContext, Collections.singletonList(indexedProperty), posModel);

        verify(inputDocument).addField(indexedProperty, new Date(150), "MONDAY");
    }

    @Test
    public void testInvalidNameThrowsIllegalArgumentException() throws FieldValueProviderException {

        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("invalidTime is not a valid opening or closing value for modelDescription");

        when(indexedProperty.getName()).thenReturn("invalidTime");
        when(posModel.getDescription()).thenReturn("modelDescription");

        openingClosingTimeValueResolver.resolve(inputDocument, indexerBatchContext, Collections.singletonList(indexedProperty), posModel);
    }
}

