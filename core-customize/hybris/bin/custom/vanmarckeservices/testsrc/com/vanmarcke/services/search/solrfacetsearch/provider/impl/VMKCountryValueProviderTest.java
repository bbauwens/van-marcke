package com.vanmarcke.services.search.solrfacetsearch.provider.impl;

import com.vanmarcke.core.model.VanMarckeVariantProductModel;
import com.vanmarcke.core.model.VanmarckeCountryChannelModel;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collection;
import java.util.Collections;
import java.util.function.BiPredicate;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKCountryValueProviderTest {

    @Mock
    private FieldNameProvider fieldNameProvider;
    @InjectMocks
    private VMKCountryValueProvider countryValueProvider;

    @Test
    public void getFieldValuesReturnsEmptyListForProductModel() {
        ProductModel product = mock(ProductModel.class);

        Collection<FieldValue> actual = countryValueProvider.getFieldValues(mock(IndexConfig.class), mock(IndexedProperty.class), product);

        assertThat(actual).isEmpty();
    }

    @Test
    public void getFieldValues() {
        IndexedProperty indexedProperty = mock(IndexedProperty.class);
        VanMarckeVariantProductModel product = mock(VanMarckeVariantProductModel.class);
        VanmarckeCountryChannelModel countryChannel = mock(VanmarckeCountryChannelModel.class);
        when(product.getVanmarckeCountryChannel()).thenReturn(Collections.singleton(countryChannel));
        CountryModel country = mock(CountryModel.class);
        when(country.getIsocode()).thenReturn("BE");
        when(countryChannel.getCountry()).thenReturn(country);
        when(fieldNameProvider.getFieldNames(indexedProperty, null)).thenReturn(Collections.singletonList("country"));
        FieldValue expected = new FieldValue("country", "BE");

        Collection<FieldValue> actual = countryValueProvider.getFieldValues(mock(IndexConfig.class), indexedProperty, product);
        VMKCollectionAssert.assertThat(actual, isFieldValueEqual()).contains(expected);
    }

    private BiPredicate<FieldValue, FieldValue> isFieldValueEqual() {
        return (x, y) -> x.getValue().equals(y.getValue()) && x.getFieldName().equals(y.getFieldName());
    }
}