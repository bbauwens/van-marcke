package com.vanmarcke.services.product.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.enums.ProductReferenceTypeEnum;
import de.hybris.platform.commerceservices.product.impl.DefaultCommerceProductReferenceService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKCommerceProductReferenceServiceFactoryTest {

    @Mock
    private DefaultCommerceProductReferenceService defaultCommerceProductReferenceService;
    @Mock
    private VMKCommerceProductReferenceServiceFactory vmkCrossSellCommerceProductReferenceService;
    @InjectMocks
    VMKCommerceProductReferenceServiceFactory vmkCommerceProductReferenceServiceFactory = new VMKCommerceProductReferenceServiceFactory(this.defaultCommerceProductReferenceService, this.vmkCrossSellCommerceProductReferenceService);

    @Test
    public void testGetProductReferencesForCode_singleReferencePassed_default() {
        final String productCode = "ABC";
        final ProductReferenceTypeEnum productRef = ProductReferenceTypeEnum.CONSISTS_OF;
        final int limit = 5;

        this.vmkCommerceProductReferenceServiceFactory.getProductReferencesForCode(productCode, productRef, limit);

        verify(this.defaultCommerceProductReferenceService).getProductReferencesForCode(productCode, productRef, limit);
        verifyZeroInteractions(this.vmkCrossSellCommerceProductReferenceService);
    }

    @Test
    public void testGetProductReferencesForCode_singleReferencePassed_crossSell() {
        final String productCode = "ABC";
        final ProductReferenceTypeEnum productRef = ProductReferenceTypeEnum.CROSSELLING;
        final int limit = 5;

        this.vmkCommerceProductReferenceServiceFactory.getProductReferencesForCode(productCode, productRef, limit);

        verify(this.vmkCrossSellCommerceProductReferenceService).getProductReferencesForCode(productCode, productRef, limit);
        verifyZeroInteractions(this.defaultCommerceProductReferenceService);
    }

    @Test
    public void testGetProductReferencesForCode_multipleReferencePassed_default() {
        final String productCode = "ABC";
        final ProductReferenceTypeEnum productRef = ProductReferenceTypeEnum.CONSISTS_OF;
        final int limit = 5;

        this.vmkCommerceProductReferenceServiceFactory.getProductReferencesForCode(productCode, Collections.singletonList(productRef), limit);

        verify(this.defaultCommerceProductReferenceService).getProductReferencesForCode(productCode, Collections.singletonList(productRef), limit);
        verifyZeroInteractions(this.vmkCrossSellCommerceProductReferenceService);
    }

    @Test
    public void testGetProductReferencesForCode_multipleReferencePassed_crossSell() {
        final String productCode = "ABC";
        final ProductReferenceTypeEnum productRef = ProductReferenceTypeEnum.CROSSELLING;
        final int limit = 5;

        this.vmkCommerceProductReferenceServiceFactory.getProductReferencesForCode(productCode, Collections.singletonList(productRef), limit);

        verify(this.vmkCrossSellCommerceProductReferenceService).getProductReferencesForCode(productCode, Collections.singletonList(productRef), limit);
        verifyZeroInteractions(this.defaultCommerceProductReferenceService);
    }
}