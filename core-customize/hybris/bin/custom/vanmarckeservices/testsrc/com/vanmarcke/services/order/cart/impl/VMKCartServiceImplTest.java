package com.vanmarcke.services.order.cart.impl;

import com.vanmarcke.services.b2b.VMKB2BUnitService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static de.hybris.platform.order.impl.DefaultCartService.SESSION_CART_PARAMETER_NAME;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKCartServiceImplTest {

    @Mock
    private VMKB2BUnitService blueB2BUnitService;
    @Mock
    private SessionService sessionService;
    @Mock
    private ModelService modelService;

    @InjectMocks
    private final MockVMKCartServiceImpl vmkCartService = new MockVMKCartServiceImpl();

    @Mock
    private CartModel cart;

    @Test
    public void testChangeCurrentCartUser() {
        final B2BCustomerModel user = mock(B2BCustomerModel.class);
        final B2BUnitModel b2bUnit = mock(B2BUnitModel.class);

        when(this.sessionService.getAttribute(SESSION_CART_PARAMETER_NAME)).thenReturn("some_cart_name");
        when(this.blueB2BUnitService.getB2BUnitModelForCurrentUser()).thenReturn(b2bUnit);
        this.vmkCartService.changeCurrentCartUser(user);

        verify(cart).setUser(user);
        verify(cart).setUnit(b2bUnit);
        verify(this.modelService).save(this.cart);
    }

    private class MockVMKCartServiceImpl extends VMKCartServiceImpl {

        @Override
        public CartModel getSessionCart() {
            return VMKCartServiceImplTest.this.cart;
        }
    }
}