package com.vanmarcke.services.search.solrfacetsearch.provider.impl;

import com.elision.hybris.l10n.model.LocalizedMessageModel;
import com.elision.hybris.l10n.services.LocalizedMessageService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKBooleanFacetValueDisplayNameProviderTest {

    @Mock
    private LocalizedMessageService localizedMessageService;
    @InjectMocks
    VMKBooleanFacetValueDisplayNameProvider vmkBooleanFacetValueDisplayNameProvider;

    @Test
    public void testGetDisplayName_true() {
        SearchQuery searchQuery = mock(SearchQuery.class);
        IndexedProperty indexedProperty = mock(IndexedProperty.class);
        String originalValue = "true";
        LocalizedMessageModel localizedMessageTrue = mock(LocalizedMessageModel.class);
        LocalizedMessageModel localizedMessageFalse = mock(LocalizedMessageModel.class);

        when(localizedMessageService.getCachedLocalizedMessageForKey("facet.boolean.true")).thenReturn(localizedMessageTrue);
        when(localizedMessageService.getCachedLocalizedMessageForKey("facet.boolean.false")).thenReturn(localizedMessageFalse);
        when(localizedMessageTrue.getMessage()).thenReturn("Ja");
        when(localizedMessageFalse.getMessage()).thenReturn("Nee");
        String result = vmkBooleanFacetValueDisplayNameProvider.getDisplayName(searchQuery, indexedProperty, originalValue);

        verify(localizedMessageService).getCachedLocalizedMessageForKey("facet.boolean.true");
        verify(localizedMessageService, never()).getCachedLocalizedMessageForKey("facet.boolean.false");
        assertThat(result).isEqualTo("Ja");
    }

    @Test
    public void testGetDisplayName_false() {
        SearchQuery searchQuery = mock(SearchQuery.class);
        IndexedProperty indexedProperty = mock(IndexedProperty.class);
        String originalValue = "false";
        LocalizedMessageModel localizedMessageTrue = mock(LocalizedMessageModel.class);
        LocalizedMessageModel localizedMessageFalse = mock(LocalizedMessageModel.class);

        when(localizedMessageService.getCachedLocalizedMessageForKey("facet.boolean.true")).thenReturn(localizedMessageTrue);
        when(localizedMessageService.getCachedLocalizedMessageForKey("facet.boolean.false")).thenReturn(localizedMessageFalse);
        when(localizedMessageTrue.getMessage()).thenReturn("Ja");
        when(localizedMessageFalse.getMessage()).thenReturn("Nee");
        String result = vmkBooleanFacetValueDisplayNameProvider.getDisplayName(searchQuery, indexedProperty, originalValue);

        verify(localizedMessageService, never()).getCachedLocalizedMessageForKey("facet.boolean.true");
        verify(localizedMessageService).getCachedLocalizedMessageForKey("facet.boolean.false");
        assertThat(result).isEqualTo("Nee");
    }
}