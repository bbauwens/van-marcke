package com.vanmarcke.services.populators;

import com.vanmarcke.core.enums.ReferenceConditionalType;
import com.vanmarcke.services.forms.delivery.DeliveryMethodForm;
import com.vanmarcke.services.storelocator.pos.VMKPointOfServiceService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.commerceservices.delivery.DeliveryService;
import de.hybris.platform.commerceservices.model.PickUpDeliveryModeModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeModel;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;

import static org.fest.assertions.Assertions.assertThat;
import static org.joda.time.format.DateTimeFormat.forPattern;
import static org.mockito.Mockito.*;

/**
 * The {@link VMKDeliveryInformationCartPopulatorTest} class contains the unit tests for the
 * {@link VMKDeliveryInformationCartPopulator} class.
 *
 * @author Tom van den Berg, Niels Raemaekers
 * @since 2-07-2020
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKDeliveryInformationCartPopulatorTest {

    private static final String DATE_PATTERN = "yyyy-MM-dd";
    private static final String DATE_STRING = "2000-02-03";
    private static final String PICKUP = "PICKUP";
    private static final String DELIVERY_OPTION = RandomStringUtils.randomAlphabetic(10);
    private static final String YARD_REFERENCE = RandomStringUtils.randomAlphabetic(10);
    private static final String DELIVERY_COMMENT = RandomStringUtils.randomAlphabetic(10);
    private static final String TEC_UID = RandomStringUtils.randomAlphabetic(10);

    @Mock
    private VMKPointOfServiceService pointOfServiceService;
    @Mock
    private DeliveryService deliveryService;
    @Spy
    @InjectMocks
    private VMKDeliveryInformationCartPopulator populator;

    @Test
    public void testPopulate() {
        final B2BUnitModel b2bUnit = mock(B2BUnitModel.class);
        when(b2bUnit.getRef1()).thenReturn(ReferenceConditionalType.MANDATORY);

        final B2BCustomerModel b2bCustomer = mock(B2BCustomerModel.class);
        when(b2bCustomer.getDefaultB2BUnit()).thenReturn(b2bUnit);

        CartModel target = new CartModel();
        target.setUser(b2bCustomer);
        target.setUnit(b2bUnit);

        DeliveryMethodForm source = mock(DeliveryMethodForm.class);
        ZoneDeliveryModeModel deliveryModeModel = mock(ZoneDeliveryModeModel.class);
        PointOfServiceModel pointOfServiceModel = mock(PointOfServiceModel.class);

        AbstractOrderEntryModel entry1 = new AbstractOrderEntryModel();
        AbstractOrderEntryModel entry2 = new AbstractOrderEntryModel();
        AbstractOrderEntryModel entry3 = new AbstractOrderEntryModel();
        AbstractOrderEntryModel entry4 = new AbstractOrderEntryModel();
        List<AbstractOrderEntryModel> entries = Arrays.asList(entry1, entry2, entry3, entry4);
        target.setEntries(entries);

        ProductModel product1 = mock(ProductModel.class);
        ProductModel product2 = mock(ProductModel.class);
        ProductModel product3 = mock(ProductModel.class);
        ProductModel product4 = mock(ProductModel.class);
        entry1.setProduct(product1);
        when(product1.getCode()).thenReturn("389200");
        entry2.setProduct(product2);
        when(product2.getCode()).thenReturn("389201");
        entry3.setProduct(product3);
        when(product3.getCode()).thenReturn("524000");
        entry4.setProduct(product4);
        when(product4.getCode()).thenReturn("524010");

        when(source.getDeliveryMethod()).thenReturn(PICKUP);
        when(source.getSelectedPickupDate()).thenReturn(DATE_STRING);
        when(source.getDeliveryOption()).thenReturn(DELIVERY_OPTION);
        when(source.getYardReference()).thenReturn(YARD_REFERENCE);
        when(source.getDeliveryComment()).thenReturn(DELIVERY_COMMENT);
        when(source.getTecUid()).thenReturn(TEC_UID);
        when(source.getRequestSeparateInvoice()).thenReturn(true);
        when(source.getActualDate()).thenReturn(DATE_STRING);
        when(source.getProductEntriesDates()).thenReturn("389200¤2021-05-03¦389201¤2021-05-03¦524000¤2021-05-03¦524010¤2021-05-07");

        when(deliveryService.getDeliveryModeForCode(DELIVERY_OPTION)).thenReturn(deliveryModeModel);
        doReturn(pointOfServiceModel).when(populator).getDeliveryPointOfService(deliveryModeModel, TEC_UID);

        populator.populate(source, target);

        verify(deliveryService).getDeliveryModeForCode(DELIVERY_OPTION);
        verify(populator).getDeliveryPointOfService(deliveryModeModel, TEC_UID);
        verify(populator).resetValuesDependingOnDeliveryMethod(target);

        assertThat(target.getDeliveryDate()).isEqualTo(forPattern("yyyy-MM-dd")
                .withZoneUTC()
                .parseDateTime(DATE_STRING).toDate());
        assertThat(target.getDeliveryMode()).isEqualTo(deliveryModeModel);
        assertThat(target.getYardReference()).isEqualTo(YARD_REFERENCE);
        assertThat(target.getDeliveryComment()).isEqualTo(DELIVERY_COMMENT);
        assertThat(target.getDeliveryPointOfService()).isEqualTo(pointOfServiceModel);
        assertThat(target.getRequestSeparateInvoice()).isEqualTo(true);
        assertThat(target.getEntries()).isNotNull().isEqualTo(entries);
        assertThat(target.getIsSplitted()).isTrue();

        DateFormat df = new SimpleDateFormat(DATE_PATTERN);
        List<AbstractOrderEntryModel> resultEntries = target.getEntries();
        assertThat(resultEntries.get(0).getProduct().getCode()).isEqualTo("389200");
        assertThat(df.format(resultEntries.get(0).getDeliveryDate())).isEqualTo("2021-05-03");
        assertThat(resultEntries.get(1).getProduct().getCode()).isEqualTo("389201");
        assertThat(df.format(resultEntries.get(1).getDeliveryDate())).isEqualTo("2021-05-03");
        assertThat(resultEntries.get(2).getProduct().getCode()).isEqualTo("524000");
        assertThat(df.format(resultEntries.get(2).getDeliveryDate())).isEqualTo("2021-05-03");
        assertThat(resultEntries.get(3).getProduct().getCode()).isEqualTo("524010");
        assertThat(df.format(resultEntries.get(3).getDeliveryDate())).isEqualTo("2021-05-07");
    }

    @Test
    public void getDeliveryPointOfService_notPickup() {
        DeliveryModeModel deliveryModeModel = mock(DeliveryModeModel.class);

        PointOfServiceModel result = populator.getDeliveryPointOfService(deliveryModeModel, TEC_UID);
        assertThat(result)
                .isNull();
    }

    @Test
    public void getDeliveryPointOfService_pickup() {
        DeliveryModeModel deliveryModeModel = mock(PickUpDeliveryModeModel.class);
        PointOfServiceModel pointOfServiceModel = mock(PointOfServiceModel.class);

        when(pointOfServiceService.getPointOfServiceForName(TEC_UID)).thenReturn(pointOfServiceModel);

        PointOfServiceModel result = populator.getDeliveryPointOfService(deliveryModeModel, TEC_UID);

        assertThat(result)
                .isEqualTo(pointOfServiceModel);

        verify(pointOfServiceService).getPointOfServiceForName(TEC_UID);
        verify(pointOfServiceService, times(0)).getCurrentPointOfService();
    }

    @Test
    public void getDeliveryPointOfService_pickup_tecUidNull() {
        DeliveryModeModel deliveryModeModel = mock(PickUpDeliveryModeModel.class);
        PointOfServiceModel pointOfServiceModel = mock(PointOfServiceModel.class);

        when(pointOfServiceService.getCurrentPointOfService()).thenReturn(pointOfServiceModel);

        PointOfServiceModel result = populator.getDeliveryPointOfService(deliveryModeModel, null);

        assertThat(result)
                .isEqualTo(pointOfServiceModel);

        verify(pointOfServiceService, times(0)).getPointOfServiceForName(TEC_UID);
        verify(pointOfServiceService).getCurrentPointOfService();
    }

    @Test
    public void resetValuesDependingOnDeliveryMethod_pickup() {
        CartModel cart = new CartModel();
        cart.setYardReference(YARD_REFERENCE);
        cart.setDeliveryComment(DELIVERY_COMMENT);
        DeliveryModeModel deliveryModeModel = mock(PickUpDeliveryModeModel.class);

        cart.setDeliveryMode(deliveryModeModel);
        populator.resetValuesDependingOnDeliveryMethod(cart);

        assertThat(cart.getYardReference()).isEqualTo(YARD_REFERENCE);
        assertThat(cart.getDeliveryComment()).isEmpty();
    }

    @Test
    public void resetValuesDependingOnDeliveryMethod() {
        CartModel cart = new CartModel();
        cart.setDeliveryComment(DELIVERY_COMMENT);
        DeliveryModeModel deliveryModeModel = mock(DeliveryModeModel.class);

        cart.setDeliveryMode(deliveryModeModel);

        populator.resetValuesDependingOnDeliveryMethod(cart);

        assertThat(cart.getDeliveryComment())
                .isEqualTo(DELIVERY_COMMENT);
    }
}
