package com.vanmarcke.services.builder;

import com.vanmarcke.services.data.ProductExportItem;

import java.util.Map;

public class ProductExportItemBuilder {

    private final ProductExportItem productExportItem = new ProductExportItem();

    /**
     * Private constructor to hide the implicit default one.
     */
    private ProductExportItemBuilder() {
    }

    /**
     * A new instance of the {@link ProductExportItemBuilder} is created and returned
     *
     * @return the created {@link ProductExportItemBuilder} instance
     */
    public static ProductExportItemBuilder aProductExportItem() {
        return new ProductExportItemBuilder();
    }

    /**
     * Sets the given {@code identifier} on the current {@link ProductExportItem} instance.
     *
     * @param identifier the identifier
     * @return the current {@link ProductExportItemBuilder} instance for method chaining
     */
    public ProductExportItemBuilder withIdentifier(Map<String, String> identifier) {
        productExportItem.setIdentifier(identifier);
        return this;
    }

    /**
     * Returns the current {@link ProductExportItem} instance
     *
     * @return the current {@link ProductExportItem} instance
     */
    public ProductExportItem build() {
        return productExportItem;
    }
}
