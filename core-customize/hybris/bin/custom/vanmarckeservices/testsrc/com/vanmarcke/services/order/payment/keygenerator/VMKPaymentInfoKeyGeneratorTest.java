package com.vanmarcke.services.order.payment.keygenerator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.CartModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKPaymentInfoKeyGeneratorTest {

    @InjectMocks
    private VMKPaymentInfoKeyGenerator defaultPaymentInfoKeyGenerator;

    @Test
    public void testGenerate() {
        String result = (String) defaultPaymentInfoKeyGenerator.generate();
        assertThat(result).isNotNull().isNotEmpty();
    }

    @Test
    public void testGenerateForObject() {
        CartModel cartModel = mock(CartModel.class);

        when(cartModel.getCode()).thenReturn("cart-code");

        String result = (String) defaultPaymentInfoKeyGenerator.generateFor(cartModel);
        assertThat(result).startsWith("cart-code-");
    }


    @Test(expected = UnsupportedOperationException.class)
    public void testReset() {
        defaultPaymentInfoKeyGenerator.reset();
    }

}