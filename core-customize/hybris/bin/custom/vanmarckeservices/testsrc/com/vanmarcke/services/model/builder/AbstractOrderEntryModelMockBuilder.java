package com.vanmarcke.services.model.builder;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.product.ProductModel;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * The {@code AbstractOrderEntryModelMockBuilder} class can be used to create a mock implementation of the
 * {@link AbstractOrderEntryModel} class.
 *
 * @author Christiaan Janssen
 * @since 29-04-2020
 */
public class AbstractOrderEntryModelMockBuilder<S extends AbstractOrderEntryModelMockBuilder, T extends AbstractOrderEntryModel> {

    protected final S myself;

    protected Long quantity;
    protected AbstractOrderModel order;
    protected ProductModel product;

    /**
     * Creates a new instance of the {@link AbstractOrderEntryModelMockBuilder} class.
     *
     * @param type the type
     */
    protected AbstractOrderEntryModelMockBuilder(Class<S> type) {
        myself = type.cast(this);
    }

    /**
     * The entry point to the {@link AbstractOrderEntryModelMockBuilder}.
     *
     * @return the {@link AbstractOrderEntryModelMockBuilder} instance for method chaining
     */
    public static AbstractOrderEntryModelMockBuilder anAbstractOrderEntry() {
        return new AbstractOrderEntryModelMockBuilder(AbstractOrderEntryModelMockBuilder.class);
    }

    /**
     * Sets the {@code quantity} on the current {@link AbstractOrderEntryModelMockBuilder} instance.
     *
     * @param quantity the quantity
     * @return the {@link AbstractOrderEntryModelMockBuilder} instance for method chaining
     */
    public S withQuantity(Long quantity) {
        this.quantity = quantity;
        return myself;
    }

    /**
     * Sets the {@code order} on the current {@link AbstractOrderEntryModelMockBuilder} instance.
     *
     * @param order the order
     * @return the {@link AbstractOrderEntryModelMockBuilder} instance for method chaining
     */
    public S withOrder(AbstractOrderModel order) {
        this.order = order;
        return myself;
    }

    /**
     * Sets the {@code order} on the current {@link AbstractOrderEntryModelMockBuilder} instance.
     *
     * @param product the product
     * @return the {@link AbstractOrderEntryModelMockBuilder} instance for method chaining
     */
    public S withProduct(ProductModel product) {
        this.product = product;
        return myself;
    }

    /**
     * Creates a mock implementation of the {@link AbstractOrderEntryModel} class.
     *
     * @return the {@link AbstractOrderEntryModel} instance
     */
    public AbstractOrderEntryModel build() {
        AbstractOrderEntryModel entry = mock(AbstractOrderEntryModel.class);
        when(entry.getQuantity()).thenReturn(quantity);
        when(entry.getOrder()).thenReturn(order);
        when(entry.getProduct()).thenReturn(product);
        return entry;
    }
}
