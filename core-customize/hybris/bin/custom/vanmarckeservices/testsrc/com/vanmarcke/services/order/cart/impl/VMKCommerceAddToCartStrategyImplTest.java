package com.vanmarcke.services.order.cart.impl;

import com.vanmarcke.core.model.VanMarckeVariantProductModel;
import com.vanmarcke.services.model.builder.ProductModelMockBuilder;
import com.vanmarcke.services.product.VMKVariantProductService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import org.apache.commons.lang3.RandomStringUtils;
import org.fest.assertions.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Locale;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * The {@link VMKCommerceAddToCartStrategyImplTest} class contains the unit tests for the
 * {@link VMKCommerceAddToCartStrategyImpl} class.
 *
 * @author Niels Raemaekers, Christiaan Janssen
 * @since 19-06-2019
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKCommerceAddToCartStrategyImplTest {

    private static final String PRODUCT_CODE = RandomStringUtils.random(10);

    @Mock
    private ProductService productService;

    @Mock
    private VMKVariantProductService variantProductService;

    @InjectMocks
    private VMKCommerceAddToCartStrategyImpl commerceAddToCartStrategy;

    @Before
    public void setUp() {
        commerceAddToCartStrategy.setProductService(productService);
    }

    @Test
    public void testIsProductForCodeWithUnknownIdentifier() {
        ProductModel product = ProductModelMockBuilder
                .aBaseProduct(PRODUCT_CODE, Locale.US)
                .build();

        CommerceCartParameter parameter = new CommerceCartParameter();
        parameter.setProduct(product);

        UnknownIdentifierException exception = mock(UnknownIdentifierException.class);

        when(productService.getProductForCode(PRODUCT_CODE)).thenThrow(exception);

        boolean actualResult = commerceAddToCartStrategy.isProductForCode(parameter);

        Assertions
                .assertThat(actualResult)
                .isFalse();

        verify(productService).getProductForCode(PRODUCT_CODE);
        verify(variantProductService, never()).isPurchasable(any());
    }

    @Test
    public void testIsProductForCodeWithBaseProduct() {
        ProductModel product = ProductModelMockBuilder
                .aBaseProduct(PRODUCT_CODE, Locale.US)
                .build();

        CommerceCartParameter parameter = new CommerceCartParameter();
        parameter.setProduct(product);

        when(productService.getProductForCode(PRODUCT_CODE)).thenReturn(product);

        boolean actualResult = commerceAddToCartStrategy.isProductForCode(parameter);

        Assertions
                .assertThat(actualResult)
                .isFalse();

        verify(productService).getProductForCode(PRODUCT_CODE);
        verify(variantProductService, never()).isPurchasable(any());
    }

    @Test
    public void testIsProductForCodeWithNonPurchasableProduct() {
        VanMarckeVariantProductModel product = mock(VanMarckeVariantProductModel.class);
        when(product.getCode()).thenReturn(PRODUCT_CODE);

        CommerceCartParameter parameter = new CommerceCartParameter();
        parameter.setProduct(product);

        when(productService.getProductForCode(PRODUCT_CODE)).thenReturn(product);
        when(variantProductService.isPurchasable(product)).thenReturn(false);

        boolean actualResult = commerceAddToCartStrategy.isProductForCode(parameter);

        Assertions
                .assertThat(actualResult)
                .isFalse();

        verify(productService).getProductForCode(PRODUCT_CODE);
        verify(variantProductService).isPurchasable(product);
    }

    @Test
    public void testIsProductForCodeWithPurchasableProduct() {
        VanMarckeVariantProductModel product = mock(VanMarckeVariantProductModel.class);
        when(product.getCode()).thenReturn(PRODUCT_CODE);

        CommerceCartParameter parameter = new CommerceCartParameter();
        parameter.setProduct(product);

        when(productService.getProductForCode(PRODUCT_CODE)).thenReturn(product);
        when(variantProductService.isPurchasable(product)).thenReturn(true);

        boolean actualResult = commerceAddToCartStrategy.isProductForCode(parameter);

        Assertions
                .assertThat(actualResult)
                .isTrue();

        verify(productService).getProductForCode(PRODUCT_CODE);
        verify(variantProductService).isPurchasable(product);
    }
}