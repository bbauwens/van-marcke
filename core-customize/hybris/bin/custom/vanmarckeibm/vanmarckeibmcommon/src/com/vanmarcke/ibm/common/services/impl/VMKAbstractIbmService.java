package com.vanmarcke.ibm.common.services.impl;

import com.vanmarcke.ibm.common.resttemplate.impl.VMKRestTemplate;
import com.vanmarcke.ibm.common.services.VMKIbmService;
import com.vanmarcke.services.basestore.VMKBaseStoreService;

/**
 * Defines and provides general methods related to the IBM A/S400.
 *
 * @author Tom van den Berg
 * @since 28-10-2020
 */
public abstract class VMKAbstractIbmService implements VMKIbmService {

    protected final VMKBaseStoreService baseStoreService;
    protected final VMKRestTemplate restTemplate;

    /**
     * Provides an implementation of the VMKAbstractIbmService.
     *
     * @param baseStoreService the base store service
     * @param restTemplate     the rest template
     */
    protected VMKAbstractIbmService(VMKBaseStoreService baseStoreService, VMKRestTemplate restTemplate) {
        this.baseStoreService = baseStoreService;
        this.restTemplate = restTemplate;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getUrl() {
        return baseStoreService.getBaseUrl();
    }
}
