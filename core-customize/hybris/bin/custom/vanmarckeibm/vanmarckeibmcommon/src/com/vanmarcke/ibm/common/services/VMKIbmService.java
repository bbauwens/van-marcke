package com.vanmarcke.ibm.common.services;


/**
 * Defines methods to provide additional business logic for interacting with IBM A/S400.
 *
 * @author Tom van den Berg
 * @since 28-10-2020
 */
public interface VMKIbmService {

    /**
     * Retrieve the CPI endpoint url for the current base store.
     *
     * @return the CPI endpoint
     */
    String getUrl();
}
