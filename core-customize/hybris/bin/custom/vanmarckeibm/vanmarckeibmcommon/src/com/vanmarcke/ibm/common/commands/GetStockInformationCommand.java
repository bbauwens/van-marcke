package com.vanmarcke.ibm.common.commands;

import com.vanmarcke.cpi.data.stock.StockRequestData;
import com.vanmarcke.cpi.data.stock.StockResponseData;
import eu.elision.integration.command.configuration.AuthenticationType;
import eu.elision.integration.command.configuration.annotation.CommandConfig;
import org.springframework.http.HttpMethod;

/**
 * Command which gets the StockInformation from the backend (via the ESB)
 */
@CommandConfig(serviceName = "stockInformationService", authenticationType = AuthenticationType.BASIC, httpMethod = HttpMethod.POST, url = "/stock")
public class GetStockInformationCommand extends AbstractVMKCPICommand<StockResponseData> {

    /**
     * Constructor which sets the stock request object to the payload.
     */
    public GetStockInformationCommand(StockRequestData stockRequest) {
        setPayLoad(stockRequest);
    }
}
