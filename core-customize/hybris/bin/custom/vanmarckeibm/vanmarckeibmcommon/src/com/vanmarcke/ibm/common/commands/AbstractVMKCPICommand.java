package com.vanmarcke.ibm.common.commands;

import eu.elision.integration.command.AbstractRESTCommand;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.List;

import static com.google.common.base.Preconditions.checkState;
import static java.util.Collections.singletonList;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;
import static org.springframework.http.HttpHeaders.COOKIE;
import static org.springframework.http.HttpHeaders.SET_COOKIE;
import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.MediaType.ALL;

/**
 * Abstract command for retrieving the required CSRF token and cookies for further communication with CPI.
 */
public abstract class AbstractVMKCPICommand<T> extends AbstractRESTCommand<T> {

    private static final String HEADER_X_CSRF_TOKEN = "X-CSRF-Token";

    /**
     * {@inheritDoc}
     */
    @Override
    protected void beforeExecute(RestTemplate restTemplate, String url) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setAccept(singletonList(ALL));
        httpHeaders.add(HEADER_X_CSRF_TOKEN, "Fetch");

        HttpEntity request = new HttpEntity<String>(httpHeaders);
        ResponseEntity<String> response = restTemplate.exchange(url, GET, request, String.class);

        String csrfToken = response.getHeaders().getFirst(HEADER_X_CSRF_TOKEN);
        checkState(isNotEmpty(csrfToken), "The CSRF Token must not be empty");

        addHttpHeader(HEADER_X_CSRF_TOKEN, response.getHeaders().getFirst(HEADER_X_CSRF_TOKEN));

        List<String> setCookie = response.getHeaders().get(SET_COOKIE);
        checkState(isNotEmpty(setCookie), "The Set-Cookie must not be empty");

        setCookie.forEach(e -> addHttpHeader(COOKIE, e));
    }
}