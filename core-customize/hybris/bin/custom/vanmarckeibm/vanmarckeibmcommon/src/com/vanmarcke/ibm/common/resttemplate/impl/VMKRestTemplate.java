package com.vanmarcke.ibm.common.resttemplate.impl;

import com.google.common.collect.Lists;
import com.vanmarcke.services.basestore.VMKBaseStoreService;
import eu.elision.integration.interceptors.LogClientHttpRequestInterceptor;
import org.springframework.http.*;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static com.google.common.base.Preconditions.checkState;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.springframework.http.HttpHeaders.COOKIE;
import static org.springframework.http.HttpHeaders.SET_COOKIE;

/**
 * Extends the default {@link RestTemplate} to provide custom methods.
 *
 * @author Tom van den Berg
 * @since 27-10-2020
 */
public class VMKRestTemplate extends RestTemplate {

    private static final String X_CSRF_TOKEN = "X-CSRF-Token";
    private static final String X_CSRF_TOKEN_FETCH = "fetch";

    private final VMKBaseStoreService baseStoreService;

    /**
     * Provides an implementation of the VMKRestTemplate.
     *
     * @param baseStoreService the base store service
     */
    public VMKRestTemplate(VMKBaseStoreService baseStoreService) {
        super(new BufferingClientHttpRequestFactory(new SimpleClientHttpRequestFactory()));
        this.baseStoreService = baseStoreService;
    }

    /**
     * Determine whether the given {@link ResponseEntity} is valid.
     *
     * @param response the response entity
     * @return true if valid
     */
    public boolean isSuccessFul(ResponseEntity<?> response) {
        return response != null && HttpStatus.OK.equals(response.getStatusCode()) && response.getBody() != null;
    }

    /**
     * Retrieve a {@link HttpHeaders} instance with the required parameters.
     *
     * @param endpoint the endpoint for retrieving the X-CSRF token
     * @return the http headers
     */
    public HttpHeaders getHttpHeaders(String endpoint) {
        initInterceptors();


        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN));
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        //todo: uncomment this once this issue is resolved
//        HttpHeaders csrfResponseHeaders = getCsrfHeaders(endpoint);

//        httpHeaders.set(X_CSRF_TOKEN, getCsrfToken(csrfResponseHeaders));
//
//        for (String cookie : getCookies(csrfResponseHeaders)) {
//            httpHeaders.add(COOKIE, cookie);
//        }

        return httpHeaders;
    }

    /**
     * Initialize the interceptors for the current {@link VMKRestTemplate} instance.
     */
    private void initInterceptors() {
        List<ClientHttpRequestInterceptor> interceptors = this.getInterceptors();
        boolean init;
        if (CollectionUtils.isEmpty(interceptors)) {
            init = true;
        } else {
            init = interceptors.stream()
                    .noneMatch(interceptor -> interceptor instanceof LogClientHttpRequestInterceptor);
        }
        if (init) {
            setInterceptors(Lists.newArrayList(
                    new LogClientHttpRequestInterceptor(), new BasicAuthorizationInterceptor(
                            baseStoreService.getUsername(),
                            baseStoreService.getPassword())));
        }
    }

    /**
     * Retrieve and set the X-CSRF token and the Set-Cookie on the given {@link HttpHeaders}.
     *
     * @param endpoint the endpoint
     * @return the http headers
     */
    protected HttpHeaders getCsrfHeaders(String endpoint) {
        ResponseEntity<Map> response = exchange(
                baseStoreService.getBaseUrl() + endpoint,
                HttpMethod.GET,
                new HttpEntity(getHeadersForTokenFetching()),
                Map.class);

        return response.getHeaders();
    }

    /**
     * Retrieve the X-CSRF token from the given response headers.
     *
     * @param csrfResponseHeaders the CSRF response headers
     * @return the X-CSRF token
     */
    protected String getCsrfToken(HttpHeaders csrfResponseHeaders) {
        String xcsrfToken = csrfResponseHeaders.getFirst(X_CSRF_TOKEN);
        checkState(isNotBlank(xcsrfToken), "The X-CSRF token must not be empty");
        return xcsrfToken;
    }

    /**
     * Retrieve Set-Cookie from the given response headers.
     *
     * @param csrfResponseHeaders the CSRF response headers
     * @return a list of cookie values
     */
    protected List<String> getCookies(HttpHeaders csrfResponseHeaders) {
        List<String> cookies = csrfResponseHeaders.get(SET_COOKIE);
        checkState(isNotEmpty(cookies), "The Set-Cookie must not be empty");
        return cookies;
    }

    /**
     * Retrieve a {@link HttpHeaders} instance with the required parameters for fetching the X-CSRF token.
     *
     * @return the http headers
     */
    protected HttpHeaders getHeadersForTokenFetching() {
        HttpHeaders headers = new HttpHeaders();
        headers.set(X_CSRF_TOKEN, X_CSRF_TOKEN_FETCH);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        return headers;
    }

}
