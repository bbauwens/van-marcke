package com.vanmarcke.ibm.common.commands;

import com.vanmarcke.cpi.data.stock.StockRequestData;
import com.vanmarcke.cpi.data.stock.StockResponseData;
import de.hybris.bootstrap.annotations.UnitTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpMethod;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class GetStockInformationCommandTest {

    private GetStockInformationCommand command;

    @Test
    public void testConstructor() {
        StockRequestData stockRequestData = mock(StockRequestData.class);

        command = new GetStockInformationCommand(stockRequestData);

        assertThat(command.getHttpMethod()).isEqualTo(HttpMethod.POST);
        assertThat(command.getServiceName()).isEqualTo("stockInformationService");
        assertThat(command.getUrl()).isEqualTo("/stock");
        assertThat(command.getPayLoad()).isEqualTo(stockRequestData);
        assertThat(command.getResponseType()).isEqualTo(StockResponseData.class);
    }

}