package com.vanmarcke.ibm.common.resttemplate.impl;

import com.vanmarcke.services.basestore.impl.VMKBaseStoreServiceImpl;
import de.hybris.bootstrap.annotations.UnitTest;
import eu.elision.integration.interceptors.LogClientHttpRequestInterceptor;
import org.apache.commons.lang.RandomStringUtils;
import org.assertj.core.util.Lists;
import org.fest.assertions.Assertions;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.*;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;

import java.util.Collections;
import java.util.Map;

import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKRestTemplateTest {

    private static final String X_CSRF_TOKEN = "X-CSRF-Token";
    private static final String X_CSRF_TOKEN_FETCH = "fetch";
    private static final String CSRF_TOKEN = RandomStringUtils.randomAlphabetic(10);
    private static final String COOKIE1 = RandomStringUtils.randomAlphabetic(10);
    private static final String COOKIE2 = RandomStringUtils.randomAlphabetic(10);
    private static final String URL = RandomStringUtils.randomAlphabetic(10);
    private static final String USERNAME = RandomStringUtils.randomAlphabetic(10);
    private static final String PASSWORD = RandomStringUtils.randomAlphabetic(10);
    private static final String ENDPOINT = RandomStringUtils.randomAlphabetic(10);

    @Mock
    private VMKBaseStoreServiceImpl baseStoreService;

    @Captor
    private ArgumentCaptor<HttpEntity> httpEntityArgumentCaptor;

    @Spy
    @InjectMocks
    private VMKRestTemplate restTemplate;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void testIsSuccessFul_responseNull() {
        Assertions
                .assertThat(restTemplate.isSuccessFul(null))
                .isFalse();
    }

    @Test
    public void testIsSuccessFul_HttpStatusNotOk() {
        ResponseEntity responseEntity = mock(ResponseEntity.class);
        when(responseEntity.getStatusCode()).thenReturn(HttpStatus.CONFLICT);

        Assertions
                .assertThat(restTemplate.isSuccessFul(responseEntity))
                .isFalse();
    }

    @Test
    public void testIsSuccessFul_ResponseBodyEmpty() {
        ResponseEntity responseEntity = mock(ResponseEntity.class);
        when(responseEntity.getStatusCode()).thenReturn(HttpStatus.OK);

        Assertions
                .assertThat(restTemplate.isSuccessFul(responseEntity))
                .isFalse();
    }

    @Test
    public void testIsSuccessFul_Ok() {
        ResponseEntity responseEntity = mock(ResponseEntity.class);
        when(responseEntity.getStatusCode()).thenReturn(HttpStatus.OK);
        when(responseEntity.getBody()).thenReturn(new Object());

        Assertions
                .assertThat(restTemplate.isSuccessFul(responseEntity))
                .isTrue();
    }

    @Test
    public void testGethttpHeaders() {
        HttpHeaders csrfResponseHeaders = new HttpHeaders();
        doReturn(csrfResponseHeaders).when(restTemplate).getCsrfHeaders(ENDPOINT);
        doReturn(CSRF_TOKEN).when(restTemplate).getCsrfToken(csrfResponseHeaders);
        doReturn(Lists.newArrayList(COOKIE1, COOKIE2)).when(restTemplate).getCookies(csrfResponseHeaders);

        when(baseStoreService.getUsername()).thenReturn(USERNAME);
        when(baseStoreService.getPassword()).thenReturn(PASSWORD);

        HttpHeaders result = restTemplate.getHttpHeaders(ENDPOINT);

        Assertions
                .assertThat(restTemplate.getInterceptors())
                .hasSize(2);

        Assertions
                .assertThat(restTemplate.getInterceptors().get(0))
                .isInstanceOf(LogClientHttpRequestInterceptor.class);

        Assertions
                .assertThat(restTemplate.getInterceptors().get(1))
                .isInstanceOf(BasicAuthorizationInterceptor.class);

        Assertions
                .assertThat(result.getAccept())
                .contains(MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN);

        Assertions
                .assertThat(result.getContentType())
                .isEqualTo(MediaType.APPLICATION_JSON);

        Assertions
                .assertThat(result.getFirst(X_CSRF_TOKEN))
                .isEqualTo(CSRF_TOKEN);

        Assertions
                .assertThat(result.get(HttpHeaders.COOKIE))
                .contains(COOKIE1, COOKIE2);
    }

    @Test
    public void testGetCsrfHeaders() {
        ResponseEntity<Map> responseEntity = mock(ResponseEntity.class);
        HttpHeaders expectedHeaders = new HttpHeaders();
        when(responseEntity.getHeaders()).thenReturn(expectedHeaders);

        when(baseStoreService.getBaseUrl()).thenReturn(URL);

        HttpHeaders csrfResponseHeaders = new HttpHeaders();
        doReturn(csrfResponseHeaders).when(restTemplate).getHeadersForTokenFetching();

        doReturn(responseEntity)
                .when(restTemplate).exchange(
                eq(URL + ENDPOINT),
                eq(HttpMethod.GET),
                httpEntityArgumentCaptor.capture(),
                eq(Map.class));

        HttpHeaders actualHeaders = restTemplate.getCsrfHeaders(ENDPOINT);

        HttpHeaders actualCsrfHeaders = httpEntityArgumentCaptor.getValue().getHeaders();

        Assertions
                .assertThat(actualCsrfHeaders)
                .isEqualTo(csrfResponseHeaders);

        Assertions
                .assertThat(actualHeaders)
                .isEqualTo(expectedHeaders);
    }

    @Test
    public void testGetCsrfToken() {
        HttpHeaders headers = new HttpHeaders();
        headers.set(X_CSRF_TOKEN, CSRF_TOKEN);

        Assertions
                .assertThat(restTemplate.getCsrfToken(headers))
                .isEqualTo(CSRF_TOKEN);
    }

    @Test
    public void testGetCsrfToken_blankToken() {
        HttpHeaders headers = new HttpHeaders();
        headers.set(X_CSRF_TOKEN, "");

        thrown.expect(IllegalStateException.class);
        thrown.expectMessage("The X-CSRF token must not be empty");

        restTemplate.getCsrfToken(headers);
    }

    @Test
    public void testGetCookie() {
        HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.SET_COOKIE, COOKIE1);

        Assertions
                .assertThat(restTemplate.getCookies(headers))
                .containsExactly(COOKIE1);
    }

    @Test
    public void testGetCookie_emptyCookies() {
        HttpHeaders headers = mock(HttpHeaders.class);
        when(headers.get(HttpHeaders.SET_COOKIE)).thenReturn(Collections.emptyList());

        thrown.expect(IllegalStateException.class);
        thrown.expectMessage("The Set-Cookie must not be empty");

        restTemplate.getCookies(headers);
    }

    @Test
    public void testGetHeadersForTokenFetching() {
        HttpHeaders result = restTemplate.getHeadersForTokenFetching();

        Assertions
                .assertThat(result.getFirst(X_CSRF_TOKEN))
                .isEqualTo(X_CSRF_TOKEN_FETCH);

        Assertions
                .assertThat(result.getAccept())
                .containsExactly(MediaType.APPLICATION_JSON);
    }
}