package com.vanmarcke.ibm.productavailability.exceptions;

import com.vanmarcke.ibm.common.exception.IBMException;

/**
 * Custom IBM stock exception.
 *
 * @author Tom van den Berg
 * @since 28-10-2020
 */
public class IBMStockException extends IBMException {

    /**
     * {@inheritDoc}
     */
    public IBMStockException(String message) {
        super(message);
    }
}
