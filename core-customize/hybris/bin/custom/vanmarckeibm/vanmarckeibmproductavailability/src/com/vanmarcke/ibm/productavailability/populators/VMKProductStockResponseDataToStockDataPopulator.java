package com.vanmarcke.ibm.productavailability.populators;

import com.vanmarcke.cpi.data.stock.ProductStockResponseData;
import com.vanmarcke.cpi.data.stock.WareHouseStockResponseData;
import com.vanmarcke.facades.product.data.StockListData;
import de.hybris.platform.commercefacades.product.data.StockData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.List;

import static de.hybris.platform.basecommerce.enums.StockLevelStatus.INSTOCK;
import static de.hybris.platform.basecommerce.enums.StockLevelStatus.OUTOFSTOCK;
import static org.apache.commons.lang3.BooleanUtils.isTrue;

public class VMKProductStockResponseDataToStockDataPopulator implements Populator<ProductStockResponseData, StockListData> {

    private static final String DISTRIBUTION_CENTER = "EDC";

    private final Converter<WareHouseStockResponseData, StockData> vmkWareHouseStockResponseDataToStockDataConverter;

    /**
     * Constructor
     *
     * @param vmkWareHouseStockResponseDataToStockDataConverter converts WareHouseStockResponseData into StockData
     */
    public VMKProductStockResponseDataToStockDataPopulator(Converter<WareHouseStockResponseData, StockData> vmkWareHouseStockResponseDataToStockDataConverter) {
        this.vmkWareHouseStockResponseDataToStockDataConverter = vmkWareHouseStockResponseDataToStockDataConverter;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void populate(ProductStockResponseData source, StockListData target) throws ConversionException {
        target.setStock(getStockData(source));
    }

    /**
     * Creates StockData for EDC and converts all the objects the List<WareHouseStockResponseData> into StockData
     *
     * @param source ProductStockResponseData The ProductStockResponseData
     * @return StockData for the warehouses and EDC
     */
    protected List<StockData> getStockData(ProductStockResponseData source) {
        List<StockData> stockDataList = new ArrayList<>();
        if (source != null) {
            stockDataList.add(createDistributionCenterStockData(source));
            stockDataList.addAll(vmkWareHouseStockResponseDataToStockDataConverter.convertAll(source.getWarehouses()));
        }
        return stockDataList;
    }

    /**
     * Creates StockData for the EDC center
     *
     * @param source The ProductStockResponseData
     * @return StockData for EDC
     */
    protected StockData createDistributionCenterStockData(ProductStockResponseData source) {
        StockData stockData = new StockData();
        stockData.setWareHouse(DISTRIBUTION_CENTER);
        stockData.setStockLevelStatus(isTrue(source.getAvailable()) ? INSTOCK : OUTOFSTOCK);
        stockData.setStockLevel(source.getAvailableAmountEDC() != null ? source.getAvailableAmountEDC() : 0);
        return stockData;
    }
}
