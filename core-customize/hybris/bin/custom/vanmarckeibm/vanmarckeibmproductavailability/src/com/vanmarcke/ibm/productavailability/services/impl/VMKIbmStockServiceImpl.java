package com.vanmarcke.ibm.productavailability.services.impl;

import com.vanmarcke.cpi.data.stock.ProductStockResponseData;
import com.vanmarcke.cpi.data.stock.StockRequestData;
import com.vanmarcke.cpi.data.stock.StockResponseData;
import com.vanmarcke.facades.product.data.StockListData;
import com.vanmarcke.ibm.common.resttemplate.impl.VMKRestTemplate;
import com.vanmarcke.ibm.common.services.impl.VMKAbstractIbmService;
import com.vanmarcke.ibm.productavailability.builders.StockRequestDataBuilder;
import com.vanmarcke.ibm.productavailability.exceptions.IBMStockException;
import com.vanmarcke.services.b2b.VMKB2BUnitService;
import com.vanmarcke.services.basestore.impl.VMKBaseStoreServiceImpl;
import com.vanmarcke.services.product.VMKStockService;
import de.hybris.platform.commercefacades.product.data.StockData;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.site.BaseSiteService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static com.vanmarcke.ibm.common.constants.VanmarckeibmcommonConstants.STOCK;

/**
 * Implements methods for retrieving stock information from the IBM A/S400 mainframe.
 *
 * @author Tom van den Berg
 * @since 19-10-2020
 */
public class VMKIbmStockServiceImpl extends VMKAbstractIbmService implements VMKStockService {

    private static final Logger LOG = Logger.getLogger(VMKIbmStockServiceImpl.class);

    private final Converter<ProductStockResponseData, StockListData> vmkStockListDataConverter;
    private final VMKB2BUnitService b2bUnitService;
    private final BaseSiteService baseSiteService;


    /**
     * Provides an implementation of the VMKIbmStockServiceImpl.
     *
     * @param baseStoreService          the base store service
     * @param restTemplate              the VMK rest template
     * @param vmkStockListDataConverter the stock list data converter
     */
    public VMKIbmStockServiceImpl(VMKBaseStoreServiceImpl baseStoreService,
                                  VMKRestTemplate restTemplate,
                                  Converter<ProductStockResponseData, StockListData> vmkStockListDataConverter,
                                  VMKB2BUnitService b2bUnitService,
                                  BaseSiteService baseSiteService) {
        super(baseStoreService, restTemplate);
        this.vmkStockListDataConverter = vmkStockListDataConverter;
        this.b2bUnitService = b2bUnitService;
        this.baseSiteService = baseSiteService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getUrl() {
        return super.getUrl() + STOCK;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Cacheable(value = "ibmStockInformation", keyGenerator = "vmkIbmStockInformationCacheKeyGenerator",
            cacheManager = "vanMarckeCacheManager", unless = "#result == null")
    public StockListData getStockForProductAndWarehouses(ProductModel product, String[] warehouseIdentifiers) {
        StockListData stockData = null;
        try {
            StockResponseData stockResponse = retrieveStock(product, b2bUnitService.getCustomerB2BUnitID(), warehouseIdentifiers);

            if (CollectionUtils.isNotEmpty(stockResponse.getStockLevels())) {
                ProductStockResponseData productStockResponseData = stockResponse.getStockLevels().get(0);
                stockData = vmkStockListDataConverter.convert(productStockResponseData);
            }

        } catch (IBMStockException e) {
            LOG.warn(e.getMessage());
        }
        return stockData;
    }

    /**
     * {@inheritDoc}
     */
    protected StockResponseData retrieveStock(ProductModel product, String customerID, String[] wareHouseIdentifiers) throws IBMStockException {
        Assert.notNull(product, "Product cannot be null.");
        Assert.notNull(product.getCode(), "Product code cannot be null.");
        Assert.notNull(wareHouseIdentifiers, "Store ID cannot be null.");

        StockRequestData request = StockRequestDataBuilder
                .aStockRequest(product.getCode(), customerID, wareHouseIdentifiers)
                .build();

        if (request.getStoreIDs().size() == 1 && request.getStoreIDs().get(0).isEmpty() && baseSiteService.getCurrentBaseSite().getChannel() == SiteChannel.DIY) {
            request.setStoreIDs(Collections.emptyList());
        }

        StockResponseData stock = getStockInformation(request);
        stock.setStockLevels(getValidStockLevels(stock.getStockLevels(), product.getCode()));
        return stock;
    }

    /**
     * Perform a stock data request to the A/S400 mainframe with the given {@link StockRequestData}.
     *
     * @param requestBody the stock request body
     * @return the stock response body
     */
    protected StockResponseData getStockInformation(StockRequestData requestBody) throws IBMStockException {
        HttpEntity<StockRequestData> request = new HttpEntity<>(requestBody, restTemplate.getHttpHeaders(STOCK));

        ResponseEntity<StockResponseData> response = restTemplate.exchange(
                getUrl(), HttpMethod.POST, request, StockResponseData.class);

        if (!restTemplate.isSuccessFul(response)) {
            throw new IBMStockException(String.format("Error while retrieving stock information for product with code %s", requestBody.getProductIDs().get(0)));
        }
        return response.getBody();
    }

    /**
     * Removes the invalid stock levels from the given list of {@link ProductStockResponseData}.
     *
     * @param stockLevels the initial stock levels
     * @return a list of {@link ProductStockResponseData}
     */
    protected List<ProductStockResponseData> getValidStockLevels(List<ProductStockResponseData> stockLevels, String productCode) throws IBMStockException {
        if (CollectionUtils.isEmpty(stockLevels)) {
            throw new IBMStockException(String.format("No stock levels found for product with code %s", productCode));
        }
        return stockLevels
                .stream()
                .filter(stockLevel -> productCode.equals(stockLevel.getProductCode()))
                .collect(Collectors.toList());
    }
}
