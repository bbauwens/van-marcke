package com.vanmarcke.ibm.productavailability.cache;

import de.hybris.platform.core.model.product.ProductModel;
import org.springframework.cache.interceptor.KeyGenerator;

import java.lang.reflect.Method;
import java.util.Arrays;

/**
 * Creates a cache key for a given product and an array of store ids.
 */
public class VMKIbmStockInformationCacheKeyGenerator implements KeyGenerator {

    /**
     * {@inheritDoc}
     */
    @Override
    public String generate(Object o, Method method, Object... objects) {
        String productCode = ((ProductModel) objects[0]).getCode();
        String suffix;
        String[] stores = (String[]) objects[1];
        if (stores != null && stores.length > 0) {
            Arrays.sort(stores);
            suffix = Arrays.toString(stores);
        } else {
            suffix = "EDC";
        }
        return String.format("%s_%s", productCode, suffix);
    }
}
