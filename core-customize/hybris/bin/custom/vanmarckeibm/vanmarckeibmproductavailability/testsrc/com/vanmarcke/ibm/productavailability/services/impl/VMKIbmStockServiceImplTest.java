package com.vanmarcke.ibm.productavailability.services.impl;

import com.vanmarcke.cpi.data.stock.ProductStockResponseData;
import com.vanmarcke.cpi.data.stock.StockRequestData;
import com.vanmarcke.cpi.data.stock.StockResponseData;
import com.vanmarcke.facades.product.data.StockListData;
import com.vanmarcke.ibm.common.constants.VanmarckeibmcommonConstants;
import com.vanmarcke.ibm.common.resttemplate.impl.VMKRestTemplate;
import com.vanmarcke.ibm.productavailability.exceptions.IBMStockException;
import com.vanmarcke.services.b2b.VMKB2BUnitService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.apache.commons.lang.RandomStringUtils;
import org.fest.assertions.Assertions;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.apache.commons.lang3.ArrayUtils.toArray;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKIbmStockServiceImplTest {

    private static final String STORE_ID = RandomStringUtils.randomAlphabetic(10);
    private static final String PRODUCT_CODE1 = RandomStringUtils.randomAlphabetic(10);
    private static final String PRODUCT_CODE2 = RandomStringUtils.randomAlphabetic(10);
    private static final String URL = RandomStringUtils.randomAlphabetic(10);
    private static final String CUSTOMER_ID = "V99909";

    @Mock
    private VMKRestTemplate restTemplate;

    @Mock
    private Converter<ProductStockResponseData, StockListData> vmkStockListDataConverter;

    @Mock
    private VMKB2BUnitService b2bUnitService;

    @Captor
    private ArgumentCaptor<StockRequestData> stockRequestDataCaptor;

    @Captor
    private ArgumentCaptor<HttpEntity<StockRequestData>> stockRequestHttpEntityCaptor;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Spy
    @InjectMocks
    private VMKIbmStockServiceImpl ibmStockService;

    @Before
    public void setup() {
        when(b2bUnitService.getCustomerB2BUnitID()).thenReturn(CUSTOMER_ID);
    }

    @Test
    public void testRetrieveStock_productNull() throws IBMStockException {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Product cannot be null.");
        ibmStockService.retrieveStock(null, CUSTOMER_ID, null);
    }

    @Test
    public void testRetrieveStock_productCodeNull() throws IBMStockException {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Product code cannot be null.");
        ibmStockService.retrieveStock(mock(ProductModel.class), CUSTOMER_ID, null);
    }

    @Test
    public void testRetrieveStock_storeIdNull() throws IBMStockException {
        ProductModel product = mock(ProductModel.class);
        when(product.getCode()).thenReturn(PRODUCT_CODE1);
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Store ID cannot be null.");
        ibmStockService.retrieveStock(product, CUSTOMER_ID, null);
    }

    @Test
    public void testGetStockForProductAndWarehouses_catchIBMException() throws IBMStockException {
        ProductModel product = mock(ProductModel.class);
        when(product.getCode()).thenReturn(PRODUCT_CODE1);

        doThrow(new IBMStockException("exception"))
                .when(ibmStockService)
                .retrieveStock(product, CUSTOMER_ID, toArray((STORE_ID)));

        StockListData result = ibmStockService.getStockForProductAndWarehouses(product, toArray((STORE_ID)));

        Assertions
                .assertThat(result)
                .isNull();

        verify(ibmStockService).retrieveStock(product, CUSTOMER_ID, toArray((STORE_ID)));
        verifyZeroInteractions(restTemplate, vmkStockListDataConverter);
    }

    @Test
    public void testGetStockForProductAndWarehouses() throws IBMStockException {
        ProductModel product = mock(ProductModel.class);
        when(product.getCode()).thenReturn(PRODUCT_CODE1);

        ProductStockResponseData productStockResponse1 = new ProductStockResponseData();
        productStockResponse1.setProductCode(PRODUCT_CODE1);
        ProductStockResponseData productStockResponse2 = new ProductStockResponseData();
        productStockResponse2.setProductCode(PRODUCT_CODE2);

        List<ProductStockResponseData> stockLevels =
                Arrays.asList(productStockResponse1, productStockResponse2);

        StockResponseData stockResponse = new StockResponseData();
        stockResponse.setStockLevels(stockLevels);

        doReturn(stockResponse).when(ibmStockService).retrieveStock(product, CUSTOMER_ID, toArray(STORE_ID));

        StockListData stockListData1 = new StockListData();
        StockListData stockListData2 = new StockListData();

        when(vmkStockListDataConverter.convert(productStockResponse1)).thenReturn(stockListData1);

        StockListData result = ibmStockService.getStockForProductAndWarehouses(product, toArray((STORE_ID)));

        Assertions
                .assertThat(result)
                .isEqualTo(stockListData1);

        Assertions
                .assertThat(result)
                .isNotEqualTo(stockListData2);

        verify(ibmStockService).retrieveStock(product, CUSTOMER_ID, toArray((STORE_ID)));
        verify(vmkStockListDataConverter).convert(productStockResponse1);
        verify(vmkStockListDataConverter, never()).convert(productStockResponse2);

    }

    @Test
    public void testRetrieveStock_success() throws IBMStockException {
        ProductModel product = mock(ProductModel.class);
        when(product.getCode()).thenReturn(PRODUCT_CODE1);

        HttpHeaders httpHeaders = new HttpHeaders();
        when(restTemplate.getHttpHeaders(VanmarckeibmcommonConstants.STOCK)).thenReturn(httpHeaders);

        ResponseEntity<StockResponseData> response = mock(ResponseEntity.class);

        StockResponseData stockResponseData = new StockResponseData();
        when(response.getBody()).thenReturn(stockResponseData);

        ProductStockResponseData productStockResponseData = new ProductStockResponseData();
        productStockResponseData.setProductCode(PRODUCT_CODE1);
        List<ProductStockResponseData> productStockResponseDataList = singletonList(productStockResponseData);
        stockResponseData.setStockLevels(productStockResponseDataList);

        doReturn(URL).when(ibmStockService).getUrl();

        when(restTemplate.exchange(
                eq(URL),
                eq(HttpMethod.POST),
                stockRequestHttpEntityCaptor.capture(),
                eq(StockResponseData.class))).thenReturn(response);

        when(restTemplate.isSuccessFul(response)).thenReturn(true);

        StockResponseData result = ibmStockService.retrieveStock(product, CUSTOMER_ID, toArray((STORE_ID)));

        verify(ibmStockService).getStockInformation(stockRequestDataCaptor.capture());

        StockRequestData capturedStockRequestData = stockRequestDataCaptor.getValue();
        HttpEntity<StockRequestData> capturedStockRequestHttpEntity = stockRequestHttpEntityCaptor.getValue();

        Assertions
                .assertThat(result)
                .isEqualTo(stockResponseData);

        Assertions
                .assertThat(result.getStockLevels())
                .containsExactly(productStockResponseData);

        Assertions
                .assertThat(capturedStockRequestHttpEntity.getBody())
                .isEqualTo(capturedStockRequestData);

        Assertions
                .assertThat(capturedStockRequestHttpEntity.getHeaders())
                .isEqualTo(httpHeaders);

        verify(restTemplate).getHttpHeaders(VanmarckeibmcommonConstants.STOCK);
        verify(restTemplate).exchange(URL, HttpMethod.POST, capturedStockRequestHttpEntity, StockResponseData.class);
        verify(restTemplate).isSuccessFul(response);
        verify(ibmStockService).getValidStockLevels(productStockResponseDataList, PRODUCT_CODE1);
        verify(ibmStockService).getStockInformation(capturedStockRequestData);
        verify(response).getBody();
    }

    @Test
    public void testRetrieveStock_invalidResponse() throws IBMStockException {
        ProductModel product = mock(ProductModel.class);
        when(product.getCode()).thenReturn(PRODUCT_CODE1);

        HttpHeaders httpHeaders = new HttpHeaders();
        when(restTemplate.getHttpHeaders(VanmarckeibmcommonConstants.STOCK)).thenReturn(httpHeaders);

        ResponseEntity<StockResponseData> response = mock(ResponseEntity.class);

        StockResponseData stockResponseData = new StockResponseData();
        when(response.getBody()).thenReturn(stockResponseData);

        ProductStockResponseData productStockResponseData = new ProductStockResponseData();
        productStockResponseData.setProductCode(PRODUCT_CODE1);
        List<ProductStockResponseData> productStockResponseDataList = singletonList(productStockResponseData);
        stockResponseData.setStockLevels(productStockResponseDataList);

        doReturn(URL).when(ibmStockService).getUrl();

        when(restTemplate.exchange(
                eq(URL),
                eq(HttpMethod.POST),
                stockRequestHttpEntityCaptor.capture(),
                eq(StockResponseData.class))).thenReturn(response);

        when(restTemplate.isSuccessFul(response)).thenReturn(false);

        thrown.expect(IBMStockException.class);
        thrown.expectMessage(String.format("Error while retrieving stock information for product with code %s", PRODUCT_CODE1));

        ibmStockService.retrieveStock(product, CUSTOMER_ID, toArray((STORE_ID)));

    }

    @Test
    public void testRetrieveStock_InvalidStockLevels() throws IBMStockException {
        ProductModel product = mock(ProductModel.class);
        when(product.getCode()).thenReturn(PRODUCT_CODE1);

        HttpHeaders httpHeaders = new HttpHeaders();
        when(restTemplate.getHttpHeaders(VanmarckeibmcommonConstants.STOCK)).thenReturn(httpHeaders);

        ResponseEntity<StockResponseData> response = mock(ResponseEntity.class);

        StockResponseData stockResponseData = new StockResponseData();
        when(response.getBody()).thenReturn(stockResponseData);

        stockResponseData.setStockLevels(emptyList());

        doReturn(URL).when(ibmStockService).getUrl();

        when(restTemplate.exchange(
                eq(URL),
                eq(HttpMethod.POST),
                stockRequestHttpEntityCaptor.capture(),
                eq(StockResponseData.class))).thenReturn(response);

        when(restTemplate.isSuccessFul(response)).thenReturn(true);

        thrown.expect(IBMStockException.class);
        thrown.expectMessage(String.format("No stock levels found for product with code %s", PRODUCT_CODE1));

        ibmStockService.retrieveStock(product, CUSTOMER_ID, toArray((STORE_ID)));
    }
}
