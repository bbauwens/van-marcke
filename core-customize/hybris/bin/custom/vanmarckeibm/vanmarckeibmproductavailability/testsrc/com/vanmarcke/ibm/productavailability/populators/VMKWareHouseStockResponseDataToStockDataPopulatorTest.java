package com.vanmarcke.ibm.productavailability.populators;

import com.vanmarcke.cpi.data.stock.WareHouseStockResponseData;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.StockData;
import de.hybris.platform.converters.Populator;
import org.junit.Before;
import org.junit.Test;

import static de.hybris.platform.basecommerce.enums.StockLevelStatus.INSTOCK;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@UnitTest
public class VMKWareHouseStockResponseDataToStockDataPopulatorTest {

    private Populator<WareHouseStockResponseData, StockData> vmkWareHouseStockResponseDataToStockDataPopulator;

    @Before
    public void setUp() {
        vmkWareHouseStockResponseDataToStockDataPopulator = new VMKWareHouseStockResponseDataToStockDataPopulator();
    }

    @Test
    public void populate() {
        WareHouseStockResponseData source = mock(WareHouseStockResponseData.class);
        when(source.getAvailable()).thenReturn(5L);
        when(source.getCode()).thenReturn("code");
        StockData target = mock(StockData.class);

        vmkWareHouseStockResponseDataToStockDataPopulator.populate(source, target);

        verify(target).setStockLevelStatus(INSTOCK);
        verify(target).setWareHouse("code");
        verify(target).setStockLevel(5L);
    }
}