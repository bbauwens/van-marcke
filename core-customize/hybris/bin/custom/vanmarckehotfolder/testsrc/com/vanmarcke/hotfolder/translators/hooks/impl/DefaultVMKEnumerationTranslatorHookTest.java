package com.vanmarcke.hotfolder.translators.hooks.impl;

import com.vanmarcke.core.enums.VMBranchType;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.enumeration.EnumerationService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.internal.util.reflection.Whitebox;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultVMKEnumerationTranslatorHookTest {

    @Mock
    private EnumerationService enumerationService;

    @InjectMocks
    private DefaultVMKEnumerationTranslatorHook vmkEnumerationTranslator;

    @Before
    public void setup() {
        Whitebox.setInternalState(vmkEnumerationTranslator, "enumerationCode", "VMBranchType");
    }

    @Test
    public void testTranslate_withExistingEnum() {
        when(enumerationService.getEnumerationValue("VMBranchType", "BEFORE")).thenReturn(VMBranchType.BEFORE);

        String result = vmkEnumerationTranslator.translate("BEFORE", null);
        assertThat(result).isEqualTo("BEFORE");
    }

    @Test
    public void testTranslate_withExistingEnumAndWhiteSpaces() {
        when(enumerationService.getEnumerationValue("VMBranchType", "TRAV_PUB")).thenReturn(VMBranchType.TRAV_PUB);

        String result = vmkEnumerationTranslator.translate("TRAV PUB", null);
        assertThat(result).isEqualTo("TRAV_PUB");
    }

    @Test
    public void testTranslate_withoutExistingEnum() {
        String result = vmkEnumerationTranslator.translate("UNKNOWN", null);
        assertThat(result).isEqualTo("<ignore>");
    }

    @Test
    public void testTranslate_withBlankValue() {
        String result = vmkEnumerationTranslator.translate(" ", null);
        assertThat(result).isEqualTo("<ignore>");
    }

}