package com.vanmarcke.hotfolder.dataimport.batch.tax.populator;

import com.vanmarcke.hotfolder.data.TaxDTO;
import com.vanmarcke.services.model.builder.CurrencyModelMockBuilder;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.europe1.enums.UserTaxGroup;
import de.hybris.platform.europe1.model.TaxRowModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

/**
 * The {@link VMKTaxRowReversePopulatorTest} class contains the unit tests for the {@link VMKTaxRowReversePopulator}
 * class.
 *
 * @author Christiaan Janssen
 * @since 07-04-2021
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKTaxRowReversePopulatorTest {

    private static final String COUNTRY_ISO = RandomStringUtils.random(10);
    private static final String CURRENCY_ISO = RandomStringUtils.random(10);
    private static final double VALUE = RandomUtils.nextDouble();

    @Mock
    private CommonI18NService commonI18NService;

    @InjectMocks
    private VMKTaxRowReversePopulator taxRowReversePopulator;

    @Test
    public void testPopulate_withoutValues() {
        // given
        TaxDTO taxDTO = new TaxDTO();

        TaxRowModel taxRowModel = new TaxRowModel();

        // when
        taxRowReversePopulator.populate(taxDTO, taxRowModel);

        // then
        assertThat(taxRowModel.getCurrency()).isNull();
        assertThat(taxRowModel.getUg()).isNull();
        assertThat(taxRowModel.getValue()).isEqualTo(0);

        verify(commonI18NService, never()).getCurrency(anyString());
    }

    @Test
    public void testPopulate_withValues() {
        // given
        TaxDTO taxDTO = new TaxDTO();
        taxDTO.setCountryIsoCode(COUNTRY_ISO);
        taxDTO.setCurrencyIsoCode(CURRENCY_ISO);
        taxDTO.setValue(VALUE);

        CurrencyModel currencyModel = CurrencyModelMockBuilder
                .aCurrency(CURRENCY_ISO)
                .build();

        when(commonI18NService.getCurrency(CURRENCY_ISO)).thenReturn(currencyModel);

        TaxRowModel taxRowModel = new TaxRowModel();

        // when
        taxRowReversePopulator.populate(taxDTO, taxRowModel);

        // then
        assertThat(taxRowModel.getCurrency()).isEqualTo(currencyModel);
        assertThat(taxRowModel.getUg()).isEqualTo(UserTaxGroup.valueOf(COUNTRY_ISO));
        assertThat(taxRowModel.getValue()).isEqualTo(VALUE);

        verify(commonI18NService).getCurrency(CURRENCY_ISO);
    }
}