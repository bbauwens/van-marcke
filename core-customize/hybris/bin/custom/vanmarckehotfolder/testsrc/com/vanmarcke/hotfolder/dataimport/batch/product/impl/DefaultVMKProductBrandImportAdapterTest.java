package com.vanmarcke.hotfolder.dataimport.batch.product.impl;

import com.vanmarcke.core.model.BrandCategoryModel;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.servicelayer.model.ModelService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static java.util.Arrays.asList;
import static java.util.Collections.singleton;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.internal.util.collections.Sets.newSet;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultVMKProductBrandImportAdapterTest {

    @Mock
    private ModelService modelService;
    @Mock
    private CategoryService categoryService;
    @InjectMocks
    private DefaultVMKProductBrandImportAdapter defaultVMKProductBrandImportAdapter;

    @Test
    public void testPerformImport_withRemoveBrand() {
        Item processedItem = mock(Item.class);
        ProductModel productModel = mock(ProductModel.class);
        CategoryModel categoryModel = mock(CategoryModel.class);
        BrandCategoryModel brandCategoryModel = mock(BrandCategoryModel.class);

        when(categoryModel.getCode()).thenReturn("category");

        when(brandCategoryModel.getCode()).thenReturn("brand");

        when(productModel.getSupercategories()).thenReturn(asList(categoryModel, brandCategoryModel));

        when(modelService.get(processedItem)).thenReturn(productModel);

        defaultVMKProductBrandImportAdapter.performImport("", processedItem);

        verify(productModel).setSupercategories(singleton(categoryModel));

        verify(modelService).save(productModel);
        verifyZeroInteractions(categoryService);
    }

    @Test
    public void testPerformImport_withNewBrand() {
        Item processedItem = mock(Item.class);
        CatalogVersionModel catalogVersionModel = mock(CatalogVersionModel.class);
        ProductModel productModel = mock(ProductModel.class);
        CategoryModel categoryModel = mock(CategoryModel.class);
        BrandCategoryModel brandCategoryModel = mock(BrandCategoryModel.class);
        BrandCategoryModel newBrandCategoryModel = mock(BrandCategoryModel.class);

        when(categoryModel.getCode()).thenReturn("category");

        when(brandCategoryModel.getCode()).thenReturn("brand");

        when(productModel.getCatalogVersion()).thenReturn(catalogVersionModel);
        when(productModel.getSupercategories()).thenReturn(asList(categoryModel, brandCategoryModel));

        when(modelService.get(processedItem)).thenReturn(productModel);

        when(categoryService.getCategoryForCode(catalogVersionModel, "new-brand")).thenReturn(newBrandCategoryModel);

        defaultVMKProductBrandImportAdapter.performImport("new-brand", processedItem);

        verify(productModel).setSupercategories(newSet(categoryModel, newBrandCategoryModel));

        verify(modelService).save(productModel);
    }

    @Test
    public void testPerformImport_withExistingBrand() {
        Item processedItem = mock(Item.class);
        ProductModel productModel = mock(ProductModel.class);
        CategoryModel categoryModel = mock(CategoryModel.class);
        BrandCategoryModel brandCategoryModel = mock(BrandCategoryModel.class);

        when(categoryModel.getCode()).thenReturn("category");

        when(brandCategoryModel.getCode()).thenReturn("brand");

        when(productModel.getSupercategories()).thenReturn(asList(categoryModel, brandCategoryModel));

        when(modelService.get(processedItem)).thenReturn(productModel);

        defaultVMKProductBrandImportAdapter.performImport("brand", processedItem);

        verify(modelService, never()).save(productModel);
        verifyZeroInteractions(categoryService);
    }

    @Test
    public void testPerformImport_withMultipleExistingBrands() {
        Item processedItem = mock(Item.class);
        CatalogVersionModel catalogVersionModel = mock(CatalogVersionModel.class);
        ProductModel productModel = mock(ProductModel.class);
        CategoryModel categoryModel = mock(CategoryModel.class);
        BrandCategoryModel brandCategoryModel1 = mock(BrandCategoryModel.class);
        BrandCategoryModel brandCategoryModel2 = mock(BrandCategoryModel.class);

        when(categoryModel.getCode()).thenReturn("category");

        when(brandCategoryModel1.getCode()).thenReturn("brand1");
        when(brandCategoryModel2.getCode()).thenReturn("brand2");

        when(productModel.getCatalogVersion()).thenReturn(catalogVersionModel);
        when(productModel.getSupercategories()).thenReturn(asList(categoryModel, brandCategoryModel1, brandCategoryModel2));

        when(modelService.get(processedItem)).thenReturn(productModel);

        when(categoryService.getCategoryForCode(catalogVersionModel, "brand1")).thenReturn(brandCategoryModel1);

        defaultVMKProductBrandImportAdapter.performImport("brand1", processedItem);

        verify(productModel).setSupercategories(newSet(categoryModel, brandCategoryModel1));

        verify(modelService).save(productModel);
    }

}