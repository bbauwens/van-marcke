package com.vanmarcke.hotfolder.resourcespace.api;

import de.hybris.bootstrap.annotations.UnitTest;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpMethod;

import static org.fest.assertions.Assertions.assertThat;
import static org.fest.assertions.MapAssert.entry;

/**
 * The {@code VMKDeleteResourceCommandTest} class contains the unit tests for the {@link VMKDeleteResourceCommandTest} class.
 *
 * @author Christiaan Janssen
 * @since 16-04-2020
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKDeleteResourceCommandTest {

    private static final String RESOURCE_ID = RandomStringUtils.random(10);

    private VMKDeleteResourceCommand command;

    @Test
    public void testConstruct() {
        command = new VMKDeleteResourceCommand(RESOURCE_ID);

        assertThat(command.getHttpMethod()).isEqualTo(HttpMethod.GET);
        assertThat(command.getServiceName()).isEqualTo("vmkResourceSpaceService");
        assertThat(command.getUrl()).isEqualTo("/");
        assertThat(command.getRequestParams()).hasSize(2)
                .includes(entry("function", "delete_resource"),
                        entry("param1", RESOURCE_ID));
        assertThat(command.getResponseType()).isEqualTo(String.class);
    }
}