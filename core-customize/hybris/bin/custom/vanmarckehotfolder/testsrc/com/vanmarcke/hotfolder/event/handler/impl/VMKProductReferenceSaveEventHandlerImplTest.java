package com.vanmarcke.hotfolder.event.handler.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.ProductReferenceModel;
import de.hybris.platform.core.PK;
import de.hybris.platform.servicelayer.exceptions.ModelLoadingException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.tx.AfterSaveEvent;
import org.apache.commons.lang3.RandomUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;

import static org.mockito.Mockito.*;

/**
 * The {@link VMKProductReferenceSaveEventHandlerImplTest} class contains the unit tests for the
 * {@link VMKProductReferenceSaveEventHandlerImplTest} class.
 *
 * @author Christiaan Janssen
 * @since 31-08-2020
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKProductReferenceSaveEventHandlerImplTest {

    private static final PK PRIMARY_KEY = PK.fromLong(RandomUtils.nextLong());

    @Mock
    private ModelService modelService;

    @InjectMocks
    private VMKProductReferenceSaveEventHandlerImpl productReferenceSaveEventHandler;

    @Test
    public void testHandleCreateEvent() {
        AfterSaveEvent event = new AfterSaveEvent(PRIMARY_KEY, AfterSaveEvent.CREATE);

        ProductReferenceModel productReference = mock(ProductReferenceModel.class);
        when(productReference.getActive()).thenReturn(true);

        when(modelService.get(PRIMARY_KEY)).thenReturn(productReference);

        productReferenceSaveEventHandler.afterSave(Collections.singletonList(event));

        verify(modelService).get(PRIMARY_KEY);
        verify(modelService, never()).remove(any());
    }

    @Test
    public void testHandleUpdateEvent() {
        AfterSaveEvent event = new AfterSaveEvent(PRIMARY_KEY, AfterSaveEvent.UPDATE);

        ProductReferenceModel productReference = mock(ProductReferenceModel.class);
        when(productReference.getActive()).thenReturn(false);

        when(modelService.get(PRIMARY_KEY)).thenReturn(productReference);

        productReferenceSaveEventHandler.afterSave(Collections.singletonList(event));

        verify(modelService).get(PRIMARY_KEY);
        verify(modelService).remove(productReference);
    }

    @Test
    public void testHandleRemoveEvent() {
        AfterSaveEvent event = new AfterSaveEvent(PRIMARY_KEY, AfterSaveEvent.REMOVE);

        ProductReferenceModel productReference = mock(ProductReferenceModel.class);
        when(productReference.getActive()).thenReturn(true);

        when(modelService.get(PRIMARY_KEY)).thenReturn(productReference);

        productReferenceSaveEventHandler.afterSave(Collections.singletonList(event));

        verify(modelService, never()).get(anyString());
        verify(modelService, never()).remove(any());
    }

    @Test
    public void testHandleEventWithException() {
        AfterSaveEvent event = new AfterSaveEvent(PRIMARY_KEY, AfterSaveEvent.CREATE);

        ModelLoadingException exception = mock(ModelLoadingException.class);

        when(modelService.get(PRIMARY_KEY)).thenReturn(exception);

        productReferenceSaveEventHandler.afterSave(Collections.singletonList(event));

        verify(modelService).get(PRIMARY_KEY);
        verify(modelService, never()).remove(any());
    }
}