package com.vanmarcke.hotfolder.dataimport.batch.tax;

import com.vanmarcke.hotfolder.dataimport.batch.tax.adapter.VMKTaxImportAdapter;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.impex.jalo.header.AbstractDescriptor;
import de.hybris.platform.impex.jalo.header.SpecialColumnDescriptor;
import de.hybris.platform.jalo.Item;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

/**
 * The {@link VMKTaxTranslatorTest} class contains the unit tests for the {@link VMKTaxTranslator} class.
 *
 * @author Christiaan Janssen
 * @since 12-04-2021
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKTaxTranslatorTest {

    private static final String BEAN_NAME = RandomStringUtils.random(10);
    private static final String CELL_VALUE = RandomStringUtils.random(10);

    @Mock
    private VMKTaxImportAdapter vmkTaxImportAdapter;

    @InjectMocks
    private VMKTaxTranslator taxTranslator;

    @Test
    public void testGetImportAdapterBeanName_withBeanName() {
        // given
        AbstractDescriptor.DescriptorParams descriptorParams = mock(AbstractDescriptor.DescriptorParams.class);
        when(descriptorParams.getModifier("adapter")).thenReturn(BEAN_NAME);

        SpecialColumnDescriptor columnDescriptor = mock(SpecialColumnDescriptor.class);
        when(columnDescriptor.getDescriptorData()).thenReturn(descriptorParams);

        // when
        String actualBeanName = taxTranslator.getImportAdapterBeanName(columnDescriptor);

        // then
        assertThat(actualBeanName).isEqualTo(BEAN_NAME);
    }

    @Test
    public void testGetImportAdapterBeanName_withFallbackBeanName() {
        // given
        AbstractDescriptor.DescriptorParams descriptorParams = mock(AbstractDescriptor.DescriptorParams.class);

        SpecialColumnDescriptor columnDescriptor = mock(SpecialColumnDescriptor.class);
        when(columnDescriptor.getDescriptorData()).thenReturn(descriptorParams);

        // when
        String actualBeanName = taxTranslator.getImportAdapterBeanName(columnDescriptor);

        // then
        assertThat(actualBeanName).isEqualTo("taxTaxImportAdapter");
    }

    @Test
    public void testPerformImport() {
        // given
        Item item = mock(Item.class);

        // when
        taxTranslator.performImport(CELL_VALUE, item);

        // then
        verify(vmkTaxImportAdapter).performImport(CELL_VALUE, item);
    }
}