package com.vanmarcke.hotfolder.translators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Set;

import static de.hybris.platform.commerceservices.enums.SiteChannel.*;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.internal.util.collections.Sets.newSet;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKCountryMarketSiteChannelTranslatorTest {

    @InjectMocks
    private VMKCountryMarketSiteChannelTranslator vmkCountryMarketSiteChannelTranslator;

    @Test
    public void testImportValue_B2B_DIY_B2C() {
        Object result = vmkCountryMarketSiteChannelTranslator.importValue("Y,Y,Y", null);

        assertThat(result).isInstanceOf(Set.class);

        Set<SiteChannel> channels = (Set<SiteChannel>) result;
        assertThat(channels).containsOnly(B2B, DIY, B2C);
    }

    @Test
    public void testImportValue_B2B_DIY() {
        Object result = vmkCountryMarketSiteChannelTranslator.importValue("Y,Y,N", null);

        assertThat(result).isInstanceOf(Set.class);

        Set<SiteChannel> channels = (Set<SiteChannel>) result;
        assertThat(channels).containsOnly(B2B, DIY);
    }

    @Test
    public void testImportValue_B2B_B2C() {
        Object result = vmkCountryMarketSiteChannelTranslator.importValue("Y,N,Y", null);

        assertThat(result).isInstanceOf(Set.class);

        Set<SiteChannel> channels = (Set<SiteChannel>) result;
        assertThat(channels).containsOnly(B2B, B2C);
    }

    @Test
    public void testImportValue_DIY_B2C() {
        Object result = vmkCountryMarketSiteChannelTranslator.importValue("N,Y,Y", null);

        assertThat(result).isInstanceOf(Set.class);

        Set<SiteChannel> channels = (Set<SiteChannel>) result;
        assertThat(channels).containsOnly(DIY, B2C);
    }

    @Test
    public void testImportValue_withoutChannels() {
        Object result = vmkCountryMarketSiteChannelTranslator.importValue("N,N,N", null);

        assertThat(result).isNull();
    }

    @Test
    public void testExportValue_withChannels() {
        String result = vmkCountryMarketSiteChannelTranslator.exportValue(newSet(B2B, DIY, B2C));

        assertThat(result).isEqualTo("B2B, DIY, B2C");
    }

    @Test
    public void testExportValue_withoutChannels() {
        String result = vmkCountryMarketSiteChannelTranslator.exportValue(null);

        assertThat(result).isEmpty();
    }

}