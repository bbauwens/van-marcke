package com.vanmarcke.hotfolder.utils;

import com.vanmarcke.core.model.BrandCategoryModel;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.exceptions.SystemException;
import de.hybris.platform.variants.model.VariantProductModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static java.util.Collections.emptySet;
import static java.util.Collections.singleton;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.internal.util.collections.Sets.newSet;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKMediaImportUtilsTest {

    @Test(expected = IndexOutOfBoundsException.class)
    public void testGetMediaPrefix_withoutElements() {
        VMKMediaImportUtils.getMediaPrefix(emptySet());
    }

    @Test
    public void testGetMediaPrefix_withBrands() {
        BrandCategoryModel brandCategoryModel1 = mock(BrandCategoryModel.class);
        BrandCategoryModel brandCategoryModel2 = mock(BrandCategoryModel.class);

        String result = VMKMediaImportUtils.getMediaPrefix(newSet(brandCategoryModel1, brandCategoryModel2));
        assertThat(result).isEqualTo("b");
    }

    @Test
    public void testGetMediaPrefix_withCategories() {
        CategoryModel categoryModel1 = mock(CategoryModel.class);
        CategoryModel categoryModel2 = mock(CategoryModel.class);

        String result = VMKMediaImportUtils.getMediaPrefix(newSet(categoryModel1, categoryModel2));
        assertThat(result).isEqualTo("c");
    }

    @Test
    public void testGetMediaPrefix_withProducts() {
        ProductModel productModel1 = mock(ProductModel.class);
        ProductModel productModel2 = mock(ProductModel.class);

        String result = VMKMediaImportUtils.getMediaPrefix(newSet(productModel1, productModel2));
        assertThat(result).isEqualTo("p");
    }

    @Test
    public void testGetMediaPrefix_withVariants() {
        VariantProductModel variantProductModel1 = mock(VariantProductModel.class);
        VariantProductModel variantProductModel2 = mock(VariantProductModel.class);

        String result = VMKMediaImportUtils.getMediaPrefix(newSet(variantProductModel1, variantProductModel2));
        assertThat(result).isEqualTo("v");
    }

    @Test(expected = SystemException.class)
    public void testGetMediaPrefix_withUnsupportedItemType() {
        CustomerModel customerModel = mock(CustomerModel.class);

        VMKMediaImportUtils.getMediaPrefix(singleton(customerModel));
    }

}