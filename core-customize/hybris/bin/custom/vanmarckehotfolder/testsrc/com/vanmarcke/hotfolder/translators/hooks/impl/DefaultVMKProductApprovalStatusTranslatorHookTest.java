package com.vanmarcke.hotfolder.translators.hooks.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.product.ProductModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultVMKProductApprovalStatusTranslatorHookTest {

    @InjectMocks
    private DefaultVMKProductApprovalStatusTranslatorHook vmkProductApprovalStatusTranslator;

    @Test
    public void testTranslate_withNewProduct() {
        String result = vmkProductApprovalStatusTranslator.translate(null, null);
        assertThat(result).isEqualTo("unapproved");
    }

    @Test
    public void testTranslate_withExistingProduct() {
        ProductModel productModel = mock(ProductModel.class);
        String result = vmkProductApprovalStatusTranslator.translate(null, productModel);
        assertThat(result).isEqualTo("<ignore>");
    }

}