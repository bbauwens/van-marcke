package com.vanmarcke.hotfolder.resourcespace.api;

import de.hybris.bootstrap.annotations.UnitTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpMethod;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import static org.fest.assertions.Assertions.assertThat;
import static org.fest.assertions.MapAssert.entry;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKUpdateFieldCommandTest {

    private VMKUpdateFieldCommand command;

    @Test
    public void testConstructor() throws UnsupportedEncodingException {
        command = new VMKUpdateFieldCommand("resource", "field", "value");

        assertThat(command.getHttpMethod()).isEqualTo(HttpMethod.GET);
        assertThat(command.getServiceName()).isEqualTo("vmkResourceSpaceService");
        assertThat(command.getUrl()).isEqualTo("/");
        assertThat(command.getRequestParams()).hasSize(4)
                .includes(entry("function", "update_field"),
                        entry("param1", "resource"),
                        entry("param2", "field"),
                        entry("param3", "value"));
        assertThat(command.getResponseType()).isEqualTo(String.class);
    }

    @Test
    public void testBuildUri() throws UnsupportedEncodingException {
        Map<String, Object> properties = new HashMap<>();
        properties.put("baseUrl", "https://localhost:9002");
        properties.put("user", "admin");
        properties.put("privateKey", "27b6f4ebee4848a87da46b123f44aa7e02c4605002ea3d1416c159a0f8c03217");

        command = new VMKUpdateFieldCommand("resource", "field", "value");

        String result = command.buildUri(properties);

        assertThat(result).isEqualTo("https://localhost:9002/?user=admin&function=update_field&param1=resource&param2=field&param3=value&sign=9d967cb044c2d4480d23cecda1612ccb9905f42a6024da866bf1e8c7086a2aa6");
    }
}
