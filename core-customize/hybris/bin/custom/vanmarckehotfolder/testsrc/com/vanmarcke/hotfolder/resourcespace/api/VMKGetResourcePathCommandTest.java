package com.vanmarcke.hotfolder.resourcespace.api;

import de.hybris.bootstrap.annotations.UnitTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpMethod;

import java.util.HashMap;
import java.util.Map;

import static org.fest.assertions.Assertions.assertThat;
import static org.fest.assertions.MapAssert.entry;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKGetResourcePathCommandTest {

    private VMKGetResourcePathCommand command;

    @Test
    public void testConstructor() {
        command = new VMKGetResourcePathCommand("resource", "size", "extension");

        assertThat(command.getHttpMethod()).isEqualTo(HttpMethod.GET);
        assertThat(command.getServiceName()).isEqualTo("vmkResourceSpaceService");
        assertThat(command.getUrl()).isEqualTo("/");
        assertThat(command.getRequestParams()).hasSize(6)
                .includes(entry("function", "get_resource_path"),
                        entry("param1", "resource"),
                        entry("param2", "false"),
                        entry("param3", "size"),
                        entry("param4", "false"),
                        entry("param5", "extension"));
        assertThat(command.getResponseType()).isEqualTo(String.class);
    }

    @Test
    public void testConstructor_withoutSize() {
        command = new VMKGetResourcePathCommand("resource", null, "extension");

        assertThat(command.getHttpMethod()).isEqualTo(HttpMethod.GET);
        assertThat(command.getServiceName()).isEqualTo("vmkResourceSpaceService");
        assertThat(command.getUrl()).isEqualTo("/");
        assertThat(command.getRequestParams()).hasSize(6)
                .includes(entry("function", "get_resource_path"),
                        entry("param1", "resource"),
                        entry("param2", "false"),
                        entry("param3", ""),
                        entry("param4", "false"),
                        entry("param5", "extension"));
        assertThat(command.getResponseType()).isEqualTo(String.class);
    }

    @Test
    public void testBuildUri() {
        Map<String, Object> properties = new HashMap<>();
        properties.put("baseUrl", "https://localhost:9002");
        properties.put("user", "admin");
        properties.put("privateKey", "27b6f4ebee4848a87da46b123f44aa7e02c4605002ea3d1416c159a0f8c03217");

        command = new VMKGetResourcePathCommand("resource", "size", "extension");

        String result = command.buildUri(properties);

        assertThat(result).isEqualTo("https://localhost:9002/?user=admin&function=get_resource_path&param1=resource&param2=false&param3=size&param4=false&param5=extension&sign=662275d85bfa6eb7234c1054166256eb74d33d7ecc4f1f66f1f21da232eff71b");
    }

}