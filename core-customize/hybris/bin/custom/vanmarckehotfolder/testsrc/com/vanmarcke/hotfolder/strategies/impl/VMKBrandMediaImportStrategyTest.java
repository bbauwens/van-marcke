package com.vanmarcke.hotfolder.strategies.impl;

import com.google.common.collect.Sets;
import com.vanmarcke.core.enums.VMBranchType;
import com.vanmarcke.core.model.BrandCategoryModel;
import com.vanmarcke.hotfolder.data.MediaData;
import com.vanmarcke.hotfolder.providers.VMKItemProvider;
import com.vanmarcke.hotfolder.resourcespace.enums.MetaDataField;
import com.vanmarcke.hotfolder.resourcespace.enums.ResourceType;
import com.vanmarcke.hotfolder.resourcespace.service.VMKResourceSpaceService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import org.apache.commons.lang3.RandomStringUtils;
import org.assertj.core.api.Assertions;
import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Locale;
import java.util.Set;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

/**
 * The {@code AbstractVMKBrandMediaImportStrategyTest} class contains the unit test for the
 * {@link VMKBrandMediaImportStrategy} class.
 *
 * @author Taki Korovessis, Christiaan Janssen
 * @since 19-10-2019
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKBrandMediaImportStrategyTest {

    private static final String MEDIA_TYPE = RandomStringUtils.randomAlphabetic(10);

    private static final String BRAND_A = RandomStringUtils.randomAlphabetic(10);
    private static final String MANUFACTURER_A = RandomStringUtils.randomAlphabetic(10);

    private static final String BRAND_B = RandomStringUtils.randomAlphabetic(10);
    private static final String MANUFACTURER_B = RandomStringUtils.randomAlphabetic(10);

    private static final String EXTERNAL_ID = RandomStringUtils.randomAlphabetic(10);

    @Mock
    private VMKItemProvider<BrandCategoryModel> itemProvider;

    @Mock
    private VMKResourceSpaceService integrationService;

    @InjectMocks
    private VMKBrandMediaImportStrategy brandMediaImportStrategy;

    @Test
    public void testGetItems() {
        CatalogVersionModel catalogVersion = new CatalogVersionModel();

        MediaData media = new MediaData();
        media.setBrands(Lists.newArrayList(BRAND_A, BRAND_B));

        BrandCategoryModel brandA = new BrandCategoryModel();
        BrandCategoryModel brandB = new BrandCategoryModel();

        when(itemProvider.get(catalogVersion, Lists.newArrayList(BRAND_A, BRAND_B))).thenReturn(Sets.newLinkedHashSet(Lists.newArrayList(brandA, brandB)));

        Set<BrandCategoryModel> actualBrands = brandMediaImportStrategy.getItems(catalogVersion, media);

        Assertions
                .assertThat(actualBrands)
                .contains(brandA, brandB);
    }

    @Test
    public void testUpdateMetadataWithPhoto() {
        brandMediaImportStrategy.setMediaType(MEDIA_TYPE);
        brandMediaImportStrategy.setResourceType(ResourceType.PHOTO);

        BrandCategoryModel brandA = new BrandCategoryModel();
        brandA.setCode(BRAND_A);
        brandA.setManufacturer(MANUFACTURER_A);

        BrandCategoryModel brandB = new BrandCategoryModel();
        brandB.setCode(BRAND_B);
        brandB.setManufacturer(MANUFACTURER_B);

        brandMediaImportStrategy.updateMetaData(EXTERNAL_ID, Sets.newLinkedHashSet(Lists.newArrayList(Locale.US, Locale.UK)), Sets.newLinkedHashSet(Lists.newArrayList(brandA, brandB)));

        verify(integrationService).updateField(EXTERNAL_ID, MetaDataField.PHOTO_TYPE, MEDIA_TYPE);
        verify(integrationService).updateField(EXTERNAL_ID, MetaDataField.MANUFACTURER, MANUFACTURER_A + "," + MANUFACTURER_B);
        verify(integrationService).updateField(EXTERNAL_ID, MetaDataField.SKU, BRAND_A + "," + BRAND_B);
        verify(integrationService).updateField(EXTERNAL_ID, MetaDataField.BRANDNAME, BRAND_A + "," + BRAND_B);
        verifyNoMoreInteractions(integrationService);
    }

    @Test
    public void testUpdateMetadataWithDocument() {
        brandMediaImportStrategy.setMediaType(MEDIA_TYPE);
        brandMediaImportStrategy.setResourceType(ResourceType.DOCUMENT);

        BrandCategoryModel brandA = new BrandCategoryModel();
        brandA.setCode(BRAND_A);
        brandA.setManufacturer(MANUFACTURER_A);
        brandA.setVmBranch(VMBranchType.SERVICES);

        BrandCategoryModel brandB = new BrandCategoryModel();
        brandB.setCode(BRAND_B);
        brandB.setManufacturer(MANUFACTURER_B);
        brandB.setVmBranch(VMBranchType.HVAC);

        brandMediaImportStrategy.updateMetaData(EXTERNAL_ID, Sets.newLinkedHashSet(Lists.newArrayList(Locale.US, Locale.UK)), Sets.newLinkedHashSet(Lists.newArrayList(brandA, brandB)));

        verify(integrationService).updateField(EXTERNAL_ID, MetaDataField.DOCUMENT_TYPE, MEDIA_TYPE);
        verify(integrationService).updateField(EXTERNAL_ID, MetaDataField.MANUFACTURER, MANUFACTURER_A + "," + MANUFACTURER_B);
        verify(integrationService).updateField(EXTERNAL_ID, MetaDataField.SKU, BRAND_A + "," + BRAND_B);
        verify(integrationService).updateField(EXTERNAL_ID, MetaDataField.BRANDNAME, BRAND_A + "," + BRAND_B);
        verify(integrationService).updateField(EXTERNAL_ID, MetaDataField.DOCUMENT_LANGUAGE, Locale.US.toString() + "," + Locale.UK.toString());
        verify(integrationService).updateField(EXTERNAL_ID, MetaDataField.DOCUMENT_BRANCHE, VMBranchType.SERVICES.getCode() + "," + VMBranchType.HVAC.getCode());
        verifyNoMoreInteractions(integrationService);
    }

    @Test
    public void testUpdateMetadataWithOtherResourceType() {
        BrandCategoryModel brandA = new BrandCategoryModel();
        brandA.setCode(BRAND_A);
        brandA.setManufacturer(MANUFACTURER_A);
        brandA.setVmBranch(VMBranchType.SERVICES);

        BrandCategoryModel brandB = new BrandCategoryModel();
        brandB.setCode(BRAND_B);
        brandB.setManufacturer(MANUFACTURER_B);
        brandA.setVmBranch(VMBranchType.HVAC);

        brandMediaImportStrategy.updateMetaData(EXTERNAL_ID, Sets.newLinkedHashSet(Lists.newArrayList(Locale.US, Locale.UK)), Sets.newLinkedHashSet(Lists.newArrayList(brandA, brandB)));

        verify(integrationService).updateField(EXTERNAL_ID, MetaDataField.MANUFACTURER, MANUFACTURER_A + "," + MANUFACTURER_B);
        verify(integrationService).updateField(EXTERNAL_ID, MetaDataField.SKU, BRAND_A + "," + BRAND_B);
        verify(integrationService).updateField(EXTERNAL_ID, MetaDataField.BRANDNAME, BRAND_A + "," + BRAND_B);
        verifyNoMoreInteractions(integrationService);
    }
}