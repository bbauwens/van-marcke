package com.vanmarcke.hotfolder.services.dataimport.batch.account.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static java.util.Collections.singleton;
import static java.util.Collections.singletonList;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultVMKAccountImportAddressAdapterTest {

    @Mock
    private ModelService modelService;

    @Mock
    private CommonI18NService commonI18NService;

    @InjectMocks
    private DefaultVMKAccountImportAddressAdapter defaultVMKAccountImportAddressAdapter;

    @Test
    public void testPerformImport_withExistingBillingAddress() {
        final String cellValue = "ext-ID:Y:N:Testline:1:A:town:3500:BE:001223456:00654321:email:Name:smt";
        final Item b2bUnit = mock(Item.class);
        final AddressModel addressModel = mock(AddressModel.class);
        final CountryModel countryModel = mock(CountryModel.class);
        final B2BUnitModel b2BUnitModel = mock(B2BUnitModel.class);

        when(addressModel.getShippingAddress()).thenReturn(true);
        when(addressModel.getVisibleInAddressBook()).thenReturn(true);

        when(b2BUnitModel.getBillingAddresses()).thenReturn(singletonList(addressModel));

        when(modelService.get(b2bUnit)).thenReturn(b2BUnitModel);
        when(commonI18NService.getCountry("BE")).thenReturn(countryModel);

        defaultVMKAccountImportAddressAdapter.performImport(cellValue, b2bUnit);

        verify(modelService).save(addressModel);
        verify(modelService).save(b2BUnitModel);

        verify(addressModel, never()).setOwner(b2BUnitModel);
        verify(addressModel).setExternalID("ext-ID");
        verify(addressModel).setCompany("Name");
        verify(addressModel).setStreetname("Testline");
        verify(addressModel).setStreetnumber("1");
        verify(addressModel).setAppartment("A");
        verify(addressModel).setTown("town");
        verify(addressModel).setPostalcode("3500");
        verify(addressModel).setCountry(countryModel);
        verify(addressModel).setPhone1("001223456");
        verify(addressModel).setPhone2("00654321");
        verify(addressModel).setEmail("email");
        verify(addressModel).setBillingAddress(true);
        verify(addressModel).setShippingAddress(true);
        verify(addressModel).setVisibleInAddressBook(true);

        verify(b2BUnitModel, never()).setAddresses(anyCollection());
    }

    @Test
    public void testPerformImport_withNewBillingAddress() {
        final String cellValue = "ext-ID:Y:N:Testline:1:A:town:3500:BE:001223456:00654321:email:Name:smt";
        final Item b2bUnit = mock(Item.class);
        final AddressModel addressModel = mock(AddressModel.class);
        final CountryModel countryModel = mock(CountryModel.class);
        final B2BUnitModel b2BUnitModel = mock(B2BUnitModel.class);

        when(modelService.get(b2bUnit)).thenReturn(b2BUnitModel);
        when(modelService.create(AddressModel.class)).thenReturn(addressModel);
        when(commonI18NService.getCountry("BE")).thenReturn(countryModel);

        defaultVMKAccountImportAddressAdapter.performImport(cellValue, b2bUnit);

        verify(modelService).create(AddressModel.class);
        verify(modelService).save(addressModel);
        verify(modelService).save(b2BUnitModel);

        verify(addressModel).setOwner(b2BUnitModel);
        verify(addressModel).setExternalID("ext-ID");
        verify(addressModel).setCompany("Name");
        verify(addressModel).setStreetname("Testline");
        verify(addressModel).setStreetnumber("1");
        verify(addressModel).setAppartment("A");
        verify(addressModel).setTown("town");
        verify(addressModel).setPostalcode("3500");
        verify(addressModel).setCountry(countryModel);
        verify(addressModel).setPhone1("001223456");
        verify(addressModel).setPhone2("00654321");
        verify(addressModel).setEmail("email");
        verify(addressModel).setBillingAddress(true);
        verify(addressModel).setShippingAddress(true);
        verify(addressModel).setVisibleInAddressBook(true);

        verify(b2BUnitModel).setAddresses(singleton(addressModel));
    }
}