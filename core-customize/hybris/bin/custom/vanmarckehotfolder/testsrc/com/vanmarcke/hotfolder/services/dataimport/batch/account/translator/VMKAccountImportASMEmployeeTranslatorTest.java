package com.vanmarcke.hotfolder.services.dataimport.batch.account.translator;

import com.vanmarcke.hotfolder.services.dataimport.batch.account.VMKAccountImportASMEmployeeAdapter;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.jalo.Item;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.verify;

@UnitTest
public class VMKAccountImportASMEmployeeTranslatorTest {

    private static final String TEST_IMPORT = "testuser:test@test.com";

    private VMKAccountImportASMEmployeeTranslator translator;
    @Mock
    private VMKAccountImportASMEmployeeAdapter vmkAccountImportASMEmployeeAdapter;
    @Mock
    private Item item;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        (this.translator = new VMKAccountImportASMEmployeeTranslator()).setVMKAccountImportASMEmployeeAdapter(this.vmkAccountImportASMEmployeeAdapter);
    }

    @Test
    public void test() {
        this.translator.performImport(TEST_IMPORT, this.item);
        ((VMKAccountImportASMEmployeeAdapter) verify((Object) this.vmkAccountImportASMEmployeeAdapter)).performImport(TEST_IMPORT, this.item);
    }
}