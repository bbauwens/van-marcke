package com.vanmarcke.hotfolder.translators.hooks.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.product.ProductModel;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.internal.util.reflection.Whitebox;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultVMKProductBaseProductTranslatorHookTest {

    @InjectMocks
    private DefaultVMKProductBaseProductTranslatorHook vmkProductBaseProductTranslator;

    @Before
    public void setup() {
        Whitebox.setInternalState(vmkProductBaseProductTranslator, "baseProduct", "dummy");
    }

    @Test
    public void testTranslate_withNewProduct() {
        String result = vmkProductBaseProductTranslator.translate(null, null);
        assertThat(result).isEqualTo("dummy");
    }

    @Test
    public void testTranslate_withExistingProduct() {
        ProductModel productModel = mock(ProductModel.class);
        String result = vmkProductBaseProductTranslator.translate(null, productModel);
        assertThat(result).isEqualTo("<ignore>");
    }

}