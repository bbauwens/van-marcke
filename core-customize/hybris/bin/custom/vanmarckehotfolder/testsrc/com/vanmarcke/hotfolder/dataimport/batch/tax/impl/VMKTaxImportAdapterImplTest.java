package com.vanmarcke.hotfolder.dataimport.batch.tax.impl;

import com.vanmarcke.hotfolder.data.TaxDTO;
import com.vanmarcke.hotfolder.dataimport.batch.tax.adapter.impl.VMKTaxImportAdapterImpl;
import com.vanmarcke.hotfolder.dataimport.batch.tax.service.VMKTaxService;
import com.vanmarcke.services.model.builder.ProductModelMockBuilder;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.servicelayer.model.ModelService;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

/**
 * The {@link VMKTaxImportAdapterImplTest} class contains the unit tests for the {@link VMKTaxImportAdapterImpl} class.
 *
 * @author Christiaan Janssen
 * @since 07-04-2021
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKTaxImportAdapterImplTest {

    private static final String ACTIVE_TAX_INFORMATION = "BE:0.041:EUR:RECUPEL:true";
    private static final String INACTIVE_TAX_INFORMATION = "BE:0.041:EUR:RECUPEL:false";
    private static final String PRODUCT_CODE = RandomStringUtils.random(10);

    @Mock
    private ModelService modelService;

    @Mock
    private VMKTaxService taxService;

    @InjectMocks
    private VMKTaxImportAdapterImpl taxImportAdapter;

    @Captor
    private ArgumentCaptor<TaxDTO> captor;

    @Test
    public void testImport_withInactiveTaxInformation() {
        // given
        Item item = mock(Item.class);

        ProductModel productModel = ProductModelMockBuilder
                .aBaseProduct(PRODUCT_CODE, null)
                .build();

        when(modelService.get(item)).thenReturn(productModel);

        // when
        taxImportAdapter.performImport(INACTIVE_TAX_INFORMATION, item);

        // then
        verify(modelService).get(item);
        verify(taxService).remove(PRODUCT_CODE + "-BE-RECUPEL", productModel);
        verify(taxService, never()).createOrUpdate(any(TaxDTO.class), any(ProductModel.class));
    }

    @Test
    public void testImport_withActiveTaxInformation() {
        // given
        Item item = mock(Item.class);

        ProductModel productModel = ProductModelMockBuilder
                .aBaseProduct(PRODUCT_CODE, null)
                .build();

        when(modelService.get(item)).thenReturn(productModel);

        // when
        taxImportAdapter.performImport(ACTIVE_TAX_INFORMATION, item);

        // then
        verify(modelService).get(item);
        verify(taxService, never()).remove(anyString(), any(ProductModel.class));
        verify(taxService).createOrUpdate(captor.capture(), eq(productModel));

        TaxDTO taxDTO = captor.getValue();

        assertThat(taxDTO.getCode()).isEqualTo(PRODUCT_CODE + "-BE-RECUPEL");
        assertThat(taxDTO.getCountryIsoCode()).isEqualTo("BE");
        assertThat(taxDTO.getValue()).isEqualTo(0.041);
        assertThat(taxDTO.getCurrencyIsoCode()).isEqualTo("EUR");
        assertThat(taxDTO.getType()).isEqualTo("RECUPEL");
        assertThat(taxDTO.isActive()).isTrue();
    }
}