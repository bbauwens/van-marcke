package com.vanmarcke.hotfolder.services.impl;

import com.vanmarcke.hotfolder.data.MediaData;
import com.vanmarcke.hotfolder.strategies.VMKMediaImportStrategy;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cloud.hotfolder.dataimport.batch.zip.ZipBatchHeader;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.File;
import java.util.List;
import java.util.regex.Pattern;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultVMKMediaImportServiceTest {

    private Pattern allowedFileNamePattern = Pattern.compile("^(.*)(?i)(\\.pdf)");
    @Mock
    private Converter<File, MediaData> createMediaConverter;
    @Mock
    private VMKMediaImportStrategy importStrategy;

    private DefaultVMKMediaImportService defaultVMKMediaImportService;

    @Before
    public void setup() {
        defaultVMKMediaImportService = new DefaultVMKMediaImportService(allowedFileNamePattern, createMediaConverter, importStrategy);
    }

    @Test
    public void testExecute() {
        ZipBatchHeader zipBatchHeader = mock(ZipBatchHeader.class);
        File metaFile = new File("unzipped/a.meta");
        File mediaFile = new File("unzipped/a.pdf");

        when(zipBatchHeader.getUnzippedFiles()).thenReturn(asList(metaFile, mediaFile));

        MediaData mediaData = mock(MediaData.class);
        when(mediaData.getQualifier()).thenReturn("a");
        doAnswer(invocation -> {
            when(mediaData.getFiles()).thenReturn(invocation.getArgumentAt(0, List.class));
            return null;
        }).when(mediaData).setFiles(anyList());

        when(createMediaConverter.convert(metaFile)).thenReturn(mediaData);

        defaultVMKMediaImportService.execute(zipBatchHeader);

        verify(mediaData).setFiles(singletonList(mediaFile));
        verify(importStrategy).execute(mediaData);
    }

    @Test
    public void testExecute_withMultipleFiles() {
        ZipBatchHeader zipBatchHeader = mock(ZipBatchHeader.class);
        File metaFile = new File("unzipped/a.meta");
        File mediaFile1 = new File("unzipped/a.pdf");
        File mediaFile2 = new File("unzipped/a$9.pdf");
        File mediaFile3 = new File("unzipped/a$10.pdf");

        when(zipBatchHeader.getUnzippedFiles()).thenReturn(asList(metaFile, mediaFile1, mediaFile3, mediaFile2));

        MediaData mediaData = mock(MediaData.class);
        when(mediaData.getQualifier()).thenReturn("a");
        doAnswer(invocation -> {
            when(mediaData.getFiles()).thenReturn(invocation.getArgumentAt(0, List.class));
            return null;
        }).when(mediaData).setFiles(anyList());

        when(createMediaConverter.convert(metaFile)).thenReturn(mediaData);

        defaultVMKMediaImportService.execute(zipBatchHeader);

        verify(mediaData).setFiles(asList(mediaFile1, mediaFile2, mediaFile3));
        verify(importStrategy).execute(mediaData);
    }

    @Test
    public void testExecute_withMultipleFilesAndUnderscoreInFileName() {
        ZipBatchHeader zipBatchHeader = mock(ZipBatchHeader.class);
        File metaFile = new File("unzipped/a$b.meta");
        File mediaFile1 = new File("unzipped/a$b.pdf");
        File mediaFile2 = new File("unzipped/a$b$1.pdf");

        when(zipBatchHeader.getUnzippedFiles()).thenReturn(asList(metaFile, mediaFile1, mediaFile2));

        MediaData mediaData = mock(MediaData.class);
        when(mediaData.getQualifier()).thenReturn("a");
        doAnswer(invocation -> {
            when(mediaData.getFiles()).thenReturn(invocation.getArgumentAt(0, List.class));
            return null;
        }).when(mediaData).setFiles(anyList());

        when(createMediaConverter.convert(metaFile)).thenReturn(mediaData);

        defaultVMKMediaImportService.execute(zipBatchHeader);

        verify(mediaData).setFiles(asList(mediaFile1, mediaFile2));
        verify(importStrategy).execute(mediaData);
    }

    @Test
    public void testExecute_withUnsupportedFile() {
        ZipBatchHeader zipBatchHeader = mock(ZipBatchHeader.class);
        File metaFile = new File("unzipped/a.meta");
        File mediaFile = new File("unzipped/a.txt");

        when(zipBatchHeader.getUnzippedFiles()).thenReturn(asList(metaFile, mediaFile));

        MediaData mediaData = mock(MediaData.class);
        when(mediaData.getQualifier()).thenReturn("a");

        when(createMediaConverter.convert(metaFile)).thenReturn(mediaData);

        defaultVMKMediaImportService.execute(zipBatchHeader);

        verify(mediaData).setFiles(emptyList());
        verify(importStrategy).execute(mediaData);
    }

}