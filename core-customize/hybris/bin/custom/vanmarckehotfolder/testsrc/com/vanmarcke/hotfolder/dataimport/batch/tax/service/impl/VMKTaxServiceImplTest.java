package com.vanmarcke.hotfolder.dataimport.batch.tax.service.impl;

import com.vanmarcke.hotfolder.data.TaxDTO;
import com.vanmarcke.services.model.builder.ProductModelMockBuilder;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.order.price.TaxModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.europe1.model.TaxRowModel;
import de.hybris.platform.order.TaxService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Optional;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

/**
 * The {@link VMKTaxServiceImplTest} class contains the unit tests for the {@link VMKTaxServiceImpl} class.
 *
 * @author Christiaan Janssen
 * @since 07-04-2021
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKTaxServiceImplTest {

    private static final String TAX_CODE = RandomStringUtils.random(10);

    @Mock
    private TaxService taxService;

    @Mock
    private ModelService modelService;

    @Mock
    private Converter<TaxDTO, TaxModel> taxConverter;

    @Mock
    private Converter<TaxDTO, TaxRowModel> taxRowConverter;

    private VMKTaxServiceImpl vmkTaxService;

    @Before
    public void setUp() {
        vmkTaxService = spy(new VMKTaxServiceImpl(taxService, modelService, taxConverter, taxRowConverter));
    }

    @Test
    public void testGetTax_withoutResult() {
        // given
        UnknownIdentifierException exception = mock(UnknownIdentifierException.class);

        when(taxService.getTaxForCode(TAX_CODE)).thenThrow(exception);

        // when
        Optional<TaxModel> actualTaxModel = vmkTaxService.getTax(TAX_CODE);

        // then
        assertThat(actualTaxModel.isPresent()).isFalse();

        verify(taxService).getTaxForCode(TAX_CODE);
    }

    @Test
    public void testGetTax_withResult() {
        // given
        TaxModel expectedTaxModel = mock(TaxModel.class);

        when(taxService.getTaxForCode(TAX_CODE)).thenReturn(expectedTaxModel);

        // when
        Optional<TaxModel> actualTaxModel = vmkTaxService.getTax(TAX_CODE);

        // then
        assertThat(actualTaxModel.isPresent()).isTrue();
        assertThat(actualTaxModel.get()).isEqualTo(expectedTaxModel);

        verify(taxService).getTaxForCode(TAX_CODE);
    }

    @Test
    public void testGetTaxRow_withoutResult_noTaxRows() {
        // given
        ProductModel productModel = ProductModelMockBuilder
                .aBaseProduct(RandomStringUtils.random(10), null)
                .build();

        // when
        Optional<TaxRowModel> actualTaxRowModel = vmkTaxService.getTaxRow(TAX_CODE, productModel);

        // then
        assertThat(actualTaxRowModel.isPresent()).isFalse();
    }

    @Test
    public void testGetTaxRow_withoutResult_missingTaxModel() {
        // given
        TaxRowModel expectedTaxRowModel = mock(TaxRowModel.class);

        ProductModel productModel = ProductModelMockBuilder
                .aBaseProduct(RandomStringUtils.random(10), null)
                .withEurope1Taxes(expectedTaxRowModel)
                .build();

        // when
        Optional<TaxRowModel> actualTaxRowModel = vmkTaxService.getTaxRow(TAX_CODE, productModel);

        // then
        assertThat(actualTaxRowModel.isPresent()).isFalse();
    }

    @Test
    public void testGetTaxRow_withoutResult_differentTaxModel() {
        // given
        TaxModel taxModel = mock(TaxModel.class);
        when(taxModel.getCode()).thenReturn(RandomStringUtils.random(10));

        TaxRowModel expectedTaxRowModel = mock(TaxRowModel.class);
        when(expectedTaxRowModel.getTax()).thenReturn(taxModel);

        ProductModel productModel = ProductModelMockBuilder
                .aBaseProduct(RandomStringUtils.random(10), null)
                .withEurope1Taxes(expectedTaxRowModel)
                .build();

        // when
        Optional<TaxRowModel> actualTaxRowModel = vmkTaxService.getTaxRow(TAX_CODE, productModel);

        // then
        assertThat(actualTaxRowModel.isPresent()).isFalse();
    }

    @Test
    public void testGetTaxRow_withResult() {
        // given
        TaxModel taxModel = mock(TaxModel.class);
        when(taxModel.getCode()).thenReturn(TAX_CODE);

        TaxRowModel expectedTaxRowModel = mock(TaxRowModel.class);
        when(expectedTaxRowModel.getTax()).thenReturn(taxModel);

        ProductModel productModel = ProductModelMockBuilder
                .aBaseProduct(RandomStringUtils.random(10), null)
                .withEurope1Taxes(expectedTaxRowModel)
                .build();

        // when
        Optional<TaxRowModel> actualTaxRowModel = vmkTaxService.getTaxRow(TAX_CODE, productModel);

        // then
        assertThat(actualTaxRowModel.isPresent()).isTrue();
        assertThat(actualTaxRowModel.get()).isEqualTo(expectedTaxRowModel);
    }

    @Test
    public void testRemove_withoutEntriesToRemove() {
        // given
        ProductModel productModel = ProductModelMockBuilder
                .aBaseProduct(RandomStringUtils.random(10), null)
                .build();

        doReturn(Optional.empty()).when(vmkTaxService).getTax(TAX_CODE);
        doReturn(Optional.empty()).when(vmkTaxService).getTaxRow(TAX_CODE, productModel);

        // when
        vmkTaxService.remove(TAX_CODE, productModel);

        // then
        verify(modelService, never()).remove(any());
    }

    @Test
    public void testRemove_withEntriesToRemove() {
        // given
        ProductModel productModel = ProductModelMockBuilder
                .aBaseProduct(RandomStringUtils.random(10), null)
                .build();

        TaxModel taxModel = mock(TaxModel.class);

        doReturn(Optional.of(taxModel)).when(vmkTaxService).getTax(TAX_CODE);

        TaxRowModel taxRowModel = mock(TaxRowModel.class);

        doReturn(Optional.of(taxRowModel)).when(vmkTaxService).getTaxRow(TAX_CODE, productModel);

        // when
        vmkTaxService.remove(TAX_CODE, productModel);

        // then
        verify(vmkTaxService).getTax(TAX_CODE);
        verify(modelService).remove(taxModel);
        verify(vmkTaxService).getTaxRow(TAX_CODE, productModel);
        verify(modelService).remove(taxRowModel);
    }

    @Test
    public void testCreateOrUpdate_withNewEntries() {
        // given
        TaxDTO taxDTO = new TaxDTO();
        taxDTO.setCode(TAX_CODE);

        CatalogVersionModel catalogVersionModel = mock(CatalogVersionModel.class);

        ProductModel productModel = mock(ProductModel.class);
        when(productModel.getCatalogVersion()).thenReturn(catalogVersionModel);

        doReturn(Optional.empty()).when(vmkTaxService).getTax(TAX_CODE);

        TaxModel taxModel = new TaxModel();

        when(modelService.create(TaxModel.class)).thenReturn(taxModel);

        doReturn(Optional.empty()).when(vmkTaxService).getTaxRow(TAX_CODE, productModel);

        TaxRowModel taxRowModel = new TaxRowModel();

        when(modelService.create(TaxRowModel.class)).thenReturn(taxRowModel);

        // when
        vmkTaxService.createOrUpdate(taxDTO, productModel);

        // then
        assertThat(taxModel.getCode()).isEqualTo(TAX_CODE);
        assertThat(taxRowModel.getProduct()).isEqualTo(productModel);
        assertThat(taxRowModel.getCatalogVersion()).isEqualTo(catalogVersionModel);
        assertThat(taxRowModel.getTax()).isEqualTo(taxModel);

        verify(vmkTaxService).getTax(TAX_CODE);
        verify(modelService).create(TaxModel.class);
        verify(taxConverter).convert(taxDTO, taxModel);
        verify(modelService).save(taxModel);
        verify(vmkTaxService).getTaxRow(TAX_CODE, productModel);
        verify(modelService).create(TaxRowModel.class);
        verify(taxRowConverter).convert(taxDTO, taxRowModel);
        verify(modelService).save(taxRowModel);
    }

    @Test
    public void testCreateOrUpdate_withExistingEntries() {
        // given
        TaxDTO taxDTO = new TaxDTO();
        taxDTO.setCode(TAX_CODE);

        CatalogVersionModel catalogVersionModel = mock(CatalogVersionModel.class);

        ProductModel productModel = mock(ProductModel.class);
        when(productModel.getCatalogVersion()).thenReturn(catalogVersionModel);

        TaxModel taxModel = mock(TaxModel.class);

        doReturn(Optional.of(taxModel)).when(vmkTaxService).getTax(TAX_CODE);

        when(modelService.create(TaxModel.class)).thenReturn(mock(TaxModel.class));

        TaxRowModel taxRowModel = new TaxRowModel();

        doReturn(Optional.of(taxRowModel)).when(vmkTaxService).getTaxRow(TAX_CODE, productModel);

        when(modelService.create(TaxRowModel.class)).thenReturn(mock(TaxRowModel.class));

        // when
        vmkTaxService.createOrUpdate(taxDTO, productModel);

        // then
        assertThat(taxRowModel.getProduct()).isNull();
        assertThat(taxRowModel.getCatalogVersion()).isNull();
        assertThat(taxRowModel.getTax()).isNull();

        verify(vmkTaxService).getTax(TAX_CODE);
        verify(modelService).create(TaxModel.class);
        verify(taxConverter).convert(taxDTO, taxModel);
        verify(modelService).save(taxModel);
        verify(vmkTaxService).getTaxRow(TAX_CODE, productModel);
        verify(modelService).create(TaxRowModel.class);
        verify(taxRowConverter).convert(taxDTO, taxRowModel);
        verify(modelService).save(taxRowModel);
    }
}
