package com.vanmarcke.hotfolder.resourcespace.api;

import de.hybris.bootstrap.annotations.UnitTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.net.URI;

import static java.util.Collections.emptyMap;
import static org.fest.assertions.Assertions.assertThat;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultVMKUriTemplateHandlerTest {

    private DefaultVMKUriTemplateHandler defaultVMKUriTemplateHandler = new DefaultVMKUriTemplateHandler();

    @Test
    public void testExpand_withUriVariablesMap() {
        URI result = defaultVMKUriTemplateHandler.expand("https://localhost:9002/?user=admin&function=create_resource&param1=type&param2=0&param3=https%3A%2F%2Flocalhost%3A9002%2Fmedias%2Fvanmarcke.pdf%3Fcontext%3Dblue&param4=&param5=&param6=false&param7=&sign=271b8dbe82da01f30b88c69eb97eb2b8c19c1d06b254d69e55841bc70e90288c", emptyMap());

        assertThat(result.toString()).isEqualTo("https://localhost:9002/?user=admin&function=create_resource&param1=type&param2=0&param3=https%3A%2F%2Flocalhost%3A9002%2Fmedias%2Fvanmarcke.pdf%3Fcontext%3Dblue&param4=&param5=&param6=false&param7=&sign=271b8dbe82da01f30b88c69eb97eb2b8c19c1d06b254d69e55841bc70e90288c");
    }

    @Test
    public void testExpand_withUriVariablesList() {
        URI result = defaultVMKUriTemplateHandler.expand("https://localhost:9002/?user=admin&function=create_resource&param1=type&param2=0&param3=https%3A%2F%2Flocalhost%3A9002%2Fmedias%2Fvanmarcke.pdf%3Fcontext%3Dblue&param4=&param5=&param6=false&param7=&sign=271b8dbe82da01f30b88c69eb97eb2b8c19c1d06b254d69e55841bc70e90288c", new Object[0]);

        assertThat(result.toString()).isEqualTo("https://localhost:9002/?user=admin&function=create_resource&param1=type&param2=0&param3=https%3A%2F%2Flocalhost%3A9002%2Fmedias%2Fvanmarcke.pdf%3Fcontext%3Dblue&param4=&param5=&param6=false&param7=&sign=271b8dbe82da01f30b88c69eb97eb2b8c19c1d06b254d69e55841bc70e90288c");
    }

    @Test(expected = IllegalStateException.class)
    public void testExpand_withIllegalStateException() {
        defaultVMKUriTemplateHandler.expand("http://\\/foo/bar", new Object[0]);
    }

}