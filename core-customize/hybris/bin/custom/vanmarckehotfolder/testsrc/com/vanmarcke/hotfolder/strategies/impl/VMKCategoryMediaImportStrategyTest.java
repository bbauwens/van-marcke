package com.vanmarcke.hotfolder.strategies.impl;

import com.google.common.collect.Sets;
import com.vanmarcke.hotfolder.data.MediaAttributeType;
import com.vanmarcke.hotfolder.data.MediaData;
import com.vanmarcke.hotfolder.providers.VMKItemProvider;
import com.vanmarcke.hotfolder.resourcespace.enums.Size;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaFormatModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.model.ModelService;
import org.apache.commons.lang3.RandomStringUtils;
import org.assertj.core.api.Assertions;
import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * The {@code AbstractVMKCategoryMediaImportStrategyTest} class contains the unit test for the
 * {@link VMKCategoryMediaImportStrategy} class.
 *
 * @author Christiaan Janssen
 * @since 15-04-2020
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKCategoryMediaImportStrategyTest {

    private static final String CATEGORY_A = RandomStringUtils.randomAlphabetic(10);
    private static final String CATEGORY_B = RandomStringUtils.randomAlphabetic(10);
    private static final String ATTRIBUTE = RandomStringUtils.randomAlphabetic(10);
    private static final String CODE = RandomStringUtils.randomAlphabetic(10);
    private static final String QUALIFIER = RandomStringUtils.randomAlphabetic(10);

    @Mock
    private ModelService modelService;

    @Mock
    private VMKItemProvider<CategoryModel> itemProvider;

    @Spy
    @InjectMocks
    private VMKCategoryMediaImportStrategy categoryMediaImportStrategy;

    @Test
    public void testGetItems() {
        CatalogVersionModel catalogVersion = new CatalogVersionModel();

        MediaData media = new MediaData();
        media.setCategories(Lists.newArrayList(CATEGORY_A, CATEGORY_B));

        CategoryModel categoryA = new CategoryModel();
        CategoryModel categoryB = new CategoryModel();

        when(itemProvider.get(catalogVersion, Lists.newArrayList(CATEGORY_A, CATEGORY_B))).thenReturn(Sets.newLinkedHashSet(Lists.newArrayList(categoryA, categoryB)));

        Set<CategoryModel> actualCategories = categoryMediaImportStrategy.getItems(catalogVersion, media);

        Assertions
                .assertThat(actualCategories)
                .contains(categoryA, categoryB);
    }

    @Test
    public void testUpdate() {
        CategoryModel category = new CategoryModel();

        MediaContainerModel container = new MediaContainerModel();
        container.setQualifier(QUALIFIER);

        MediaModel mediaThumbnail = new MediaModel();
        MediaModel mediaPreview = new MediaModel();

        MediaFormatModel mediaFormatThumbnail = new MediaFormatModel();
        MediaFormatModel mediaFormatPreview = new MediaFormatModel();

        mediaFormatThumbnail.setQualifier(Size.THUMBNAIL_VANMARCKE.name());
        mediaFormatPreview.setQualifier(Size.PREVIEW.name());

        mediaThumbnail.setMediaFormat(mediaFormatThumbnail);
        mediaPreview.setMediaFormat(mediaFormatPreview);

        Collection<MediaModel> medias = new ArrayList<>();
        medias.add(mediaThumbnail);
        medias.add(mediaPreview);
        container.setMedias(medias);

        categoryMediaImportStrategy.update(container, category, ATTRIBUTE, MediaAttributeType.PACKSHOT);

        verify(modelService).setAttributeValue(category, ProductModel.PICTURE, mediaPreview);
        verify(modelService).setAttributeValue(category, ProductModel.LOGO, Collections.singletonList(mediaPreview));
        verify(modelService).setAttributeValue(category, ProductModel.THUMBNAIL, mediaThumbnail);
    }
}