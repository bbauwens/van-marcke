package com.vanmarcke.hotfolder.dataimport.batch.converter.impl;


import com.vanmarcke.hotfolder.translators.hooks.VMKImpexTranslatorHook;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.exceptions.SystemException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.variants.model.VariantProductModel;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.HashMap;
import java.util.Map;

import static java.util.Collections.singletonMap;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultVMKProductImpexConverterTest {

    @Mock
    private CatalogVersionService catalogVersionService;
    @Mock
    private ProductService productService;
    @Mock
    private VMKImpexTranslatorHook aTranslatorHook;
    @Mock
    private VMKImpexTranslatorHook bTranslatorHook;
    @Mock
    private VMKImpexTranslatorHook e4TranslatorHook;

    @InjectMocks
    private DefaultVMKProductImpexConverter defaultVMKProductImpexConverter;

    @Before
    public void setup() {
        defaultVMKProductImpexConverter.setCatalog("testCatalog");
        defaultVMKProductImpexConverter.setImpexRow(";{+1};;{A};{B};{E3};{2};{S}");

        Map<String, VMKImpexTranslatorHook> translatorHooks = new HashMap<>();
        translatorHooks.put("A", aTranslatorHook);
        translatorHooks.put("B", bTranslatorHook);
        translatorHooks.put("E3", e4TranslatorHook);
        defaultVMKProductImpexConverter.setTranslatorHooks(translatorHooks);
    }

    @Test
    public void testConvert_withoutRowAndSequenceId() {
        String result = defaultVMKProductImpexConverter.convert(null, null);
        assertThat(result).isEmpty();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testConvert_withoutProductCode() {
        Map<Integer, String> row = singletonMap(0, "");

        defaultVMKProductImpexConverter.convert(row, null);
    }

    @Test(expected = SystemException.class)
    public void testConvert_withInvalidRowSyntax() {
        Map<Integer, String> row = singletonMap(1, "product-code");

        defaultVMKProductImpexConverter.setImpexRow(";{+1");

        defaultVMKProductImpexConverter.convert(row, null);
    }

    @Test
    public void testConvert_withNewProduct() {
        Map<Integer, String> row = new HashMap<>();
        row.put(1, "product-code");
        row.put(2, "product-attribute");
        row.put(3, "enum-code");

        CatalogVersionModel catalogVersionModel = mock(CatalogVersionModel.class);

        when(catalogVersionService.getCatalogVersion("testCatalog", "Staged")).thenReturn(catalogVersionModel);

        when(productService.getProductForCode(catalogVersionModel, "product-code")).thenThrow(UnknownIdentifierException.class);

        when(aTranslatorHook.translate(null, null)).thenReturn("translatedA");
        when(bTranslatorHook.translate(null, null)).thenReturn("translatedB");
        when(e4TranslatorHook.translate("enum-code", null)).thenReturn("translatedE4");

        String result = defaultVMKProductImpexConverter.convert(row, 16L);

        assertThat(result).isEqualTo(";product-code;;translatedA;translatedB;translatedE4;product-attribute;16");
    }

    @Test
    public void testConvert_withExistingProduct() {
        Map<Integer, String> row = new HashMap<>();
        row.put(1, "product-code");
        row.put(2, "product-attribute");
        row.put(3, "enum-code");

        VariantProductModel variantProductModel = mock(VariantProductModel.class);
        CatalogVersionModel catalogVersionModel = mock(CatalogVersionModel.class);

        when(catalogVersionService.getCatalogVersion("testCatalog", "Staged")).thenReturn(catalogVersionModel);

        when(productService.getProductForCode(catalogVersionModel, "product-code")).thenReturn(variantProductModel);

        when(aTranslatorHook.translate(null, variantProductModel)).thenReturn("translatedA");
        when(bTranslatorHook.translate(null, variantProductModel)).thenReturn("translatedB");
        when(e4TranslatorHook.translate("enum-code", variantProductModel)).thenReturn("translatedE4");

        String result = defaultVMKProductImpexConverter.convert(row, 2L);

        assertThat(result).isEqualTo(";product-code;;translatedA;translatedB;translatedE4;product-attribute;2");
    }

}