package com.vanmarcke.hotfolder.strategies;

import com.vanmarcke.hotfolder.data.MediaAttributeType;
import com.vanmarcke.hotfolder.data.MediaData;
import com.vanmarcke.hotfolder.data.MediaFactoryParameter;
import com.vanmarcke.hotfolder.providers.VMKItemProvider;
import com.vanmarcke.hotfolder.resourcespace.enums.MetaDataField;
import com.vanmarcke.hotfolder.resourcespace.enums.ResourceType;
import com.vanmarcke.hotfolder.resourcespace.service.VMKResourceSpaceService;
import com.vanmarcke.hotfolder.strategies.media.VMKMediaFactoryStrategy;
import com.vanmarcke.services.model.builder.ProductModelMockBuilder;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.EmployeeModel;
import de.hybris.platform.servicelayer.exceptions.SystemException;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import org.apache.commons.lang3.RandomStringUtils;
import org.assertj.core.util.Maps;
import org.assertj.core.util.Sets;
import org.fest.assertions.Assertions;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.File;
import java.util.*;

import static java.util.Collections.singletonList;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * The {@code AbstractVMKMediaImportStrategyTest} class contains the unit tests for the
 * {@link AbstractVMKMediaImportStrategy} class.
 *
 * @author Taki Korovessis, Christiaan Janssen
 * @since 29-10-2019
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AbstractVMKMediaImportStrategyTest {

    private static final String CODE = RandomStringUtils.randomAlphabetic(10);
    private static final String CATALOG = "vanmarckeProductCatalog";
    private static final String MEDIA_TYPE = RandomStringUtils.randomAlphabetic(10);
    private static final String QUALIFIER = RandomStringUtils.randomAlphabetic(10);
    private static final String EXTERNAL_ID = RandomStringUtils.randomAlphabetic(10);
    private static final String LOCALIZED_NAME = RandomStringUtils.randomAlphabetic(10);
    private static final String ATTRIBUTE = RandomStringUtils.randomAlphabetic(10);
    private static final MediaAttributeType MEDIA_ATTRIBUTE_TYPE = MediaAttributeType.LIST;
    private static final String STAGED = "Staged";

    private MockVMKMediaImportStrategy mediaImportStrategy;

    @Mock
    protected ModelService modelService;

    @Mock
    private VMKResourceSpaceService integrationService;

    @Mock
    protected VMKItemProvider<ProductModel> itemProvider;

    @Mock
    private I18NService i18NService;

    @Mock
    private SessionService sessionService;

    @Mock
    private UserService userService;

    @Mock
    private CatalogVersionService catalogVersionService;

    @Mock
    private VMKMediaFactoryStrategy<MediaModel> mediaFactoryStrategy;

    @Mock
    private VMKMediaFactoryStrategy<MediaContainerModel> mediaContainerFactoryStrategy;

    @Captor
    private ArgumentCaptor<MediaFactoryParameter> captor;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setUp() {
        initMocks(this);

        mediaImportStrategy = spy(new MockVMKMediaImportStrategy());

        mediaImportStrategy.setModelService(modelService);
        mediaImportStrategy.setIntegrationService(integrationService);
        mediaImportStrategy.setItemProvider(itemProvider);
        mediaImportStrategy.setI18NService(i18NService);
        mediaImportStrategy.setSessionService(sessionService);
        mediaImportStrategy.setUserService(userService);
        mediaImportStrategy.setCatalogVersionService(catalogVersionService);
        mediaImportStrategy.setMediaFactoryStrategy(mediaFactoryStrategy);
        mediaImportStrategy.setMediaContainerFactoryStrategy(mediaContainerFactoryStrategy);
        mediaImportStrategy.setAttribute(ATTRIBUTE);
        mediaImportStrategy.setMediaAttributeType(MEDIA_ATTRIBUTE_TYPE);
        mediaImportStrategy.setCatalog(CATALOG);

    }

    @Test
    public void testExecuteWithoutMediaDTO() {
        mediaImportStrategy.execute(null);
    }

    @Test
    public void testExecuteWithoutFiles() {
        MediaData mediaDTO = new MediaData();

        mediaImportStrategy.execute(mediaDTO);
    }

    @Test
    public void testExecuteWithUnknownResourceType() {
        mediaImportStrategy.setResourceType(ResourceType.GLOBAL);

        File file = mock(File.class);

        MediaData mediaDTO = new MediaData();
        mediaDTO.setFiles(singletonList(file));

        EmployeeModel employee = new EmployeeModel();

        when(userService.getAdminUser()).thenReturn(employee);

        when(sessionService.executeInLocalView(any(SessionExecutionBody.class), eq(employee)))
                .thenAnswer(invocation -> ((SessionExecutionBody) invocation.getArguments()[0]).execute());

        CatalogVersionModel catalogVersion = new CatalogVersionModel();

        when(catalogVersionService.getCatalogVersion(CATALOG, STAGED)).thenReturn(catalogVersion);

        thrown.expect(SystemException.class);
        thrown.expectMessage("Resource type 'GLOBAL' is not supported.");

        mediaImportStrategy.execute(mediaDTO);
    }

    @Test
    public void testExecuteWithDocumentButWithoutItems() {
        mediaImportStrategy.setResourceType(ResourceType.DOCUMENT);

        File file = mock(File.class);

        MediaData mediaDTO = new MediaData();
        mediaDTO.setFiles(singletonList(file));

        EmployeeModel employee = new EmployeeModel();

        when(userService.getAdminUser()).thenReturn(employee);

        when(sessionService.executeInLocalView(any(SessionExecutionBody.class), eq(employee)))
                .thenAnswer(invocation -> ((SessionExecutionBody) invocation.getArguments()[0]).execute());

        CatalogVersionModel catalogVersion = new CatalogVersionModel();

        when(catalogVersionService.getCatalogVersion(CATALOG, STAGED)).thenReturn(catalogVersion);

        doReturn(Sets.newLinkedHashSet()).when(mediaImportStrategy).getItems(catalogVersion, mediaDTO);

        mediaImportStrategy.execute(mediaDTO);

        verify(userService).getAdminUser();
        verify(sessionService).executeInLocalView(any(SessionExecutionBody.class), eq(employee));
        verify(catalogVersionService).getCatalogVersion(CATALOG, STAGED);
        verify(mediaFactoryStrategy, never()).create(any(MediaFactoryParameter.class));
    }

    @Test
    public void textExecuteWithDocument() {
        mediaImportStrategy.setResourceType(ResourceType.DOCUMENT);

        File file = mock(File.class);
        when(file.getName()).thenReturn(QUALIFIER + ".pdf");

        MediaData mediaDTO = new MediaData();
        mediaDTO.setFiles(singletonList(file));
        mediaDTO.setLocalizedNames(Maps.newHashMap(Locale.US, LOCALIZED_NAME));

        EmployeeModel employee = new EmployeeModel();

        when(userService.getAdminUser()).thenReturn(employee);

        when(sessionService.executeInLocalView(any(SessionExecutionBody.class), eq(employee)))
                .thenAnswer(invocation -> ((SessionExecutionBody) invocation.getArguments()[0]).execute());

        CatalogVersionModel catalogVersion = new CatalogVersionModel();

        when(catalogVersionService.getCatalogVersion(CATALOG, STAGED)).thenReturn(catalogVersion);

        doNothing().when(mediaImportStrategy).importMedia(catalogVersion, mediaDTO, ResourceType.DOCUMENT);

        mediaImportStrategy.execute(mediaDTO);

        verify(mediaImportStrategy).importMedia(catalogVersion, mediaDTO, ResourceType.DOCUMENT);
        verify(mediaImportStrategy, times(0)).importMedia(catalogVersion, mediaDTO, ResourceType.PHOTO);
        verify(mediaImportStrategy, times(0)).importMedia(catalogVersion, mediaDTO, ResourceType.VIDEO);
    }

    @Test
    public void textExecuteWithPhoto() {
        mediaImportStrategy.setResourceType(ResourceType.PHOTO);

        File file = mock(File.class);
        when(file.getName()).thenReturn(QUALIFIER + ".pdf");

        MediaData mediaDTO = new MediaData();
        mediaDTO.setFiles(singletonList(file));
        mediaDTO.setLocalizedNames(Maps.newHashMap(Locale.US, LOCALIZED_NAME));

        EmployeeModel employee = new EmployeeModel();

        when(userService.getAdminUser()).thenReturn(employee);

        when(sessionService.executeInLocalView(any(SessionExecutionBody.class), eq(employee)))
                .thenAnswer(invocation -> ((SessionExecutionBody) invocation.getArguments()[0]).execute());

        CatalogVersionModel catalogVersion = new CatalogVersionModel();

        when(catalogVersionService.getCatalogVersion(CATALOG, STAGED)).thenReturn(catalogVersion);

        doNothing().when(mediaImportStrategy).importMedia(catalogVersion, mediaDTO, ResourceType.PHOTO);

        mediaImportStrategy.execute(mediaDTO);

        verify(mediaImportStrategy).importMediaContainer(catalogVersion, mediaDTO, ResourceType.PHOTO);
        verify(mediaImportStrategy, times(0)).importMedia(catalogVersion, mediaDTO, ResourceType.DOCUMENT);
        verify(mediaImportStrategy, times(0)).importMedia(catalogVersion, mediaDTO, ResourceType.VIDEO);
    }

    @Test
    public void textExecuteWithVideo() {
        mediaImportStrategy.setResourceType(ResourceType.VIDEO);

        File file = mock(File.class);
        when(file.getName()).thenReturn(QUALIFIER + ".pdf");

        MediaData mediaDTO = new MediaData();
        mediaDTO.setFiles(singletonList(file));
        mediaDTO.setLocalizedNames(Maps.newHashMap(Locale.US, LOCALIZED_NAME));

        EmployeeModel employee = new EmployeeModel();

        when(userService.getAdminUser()).thenReturn(employee);

        when(sessionService.executeInLocalView(any(SessionExecutionBody.class), eq(employee)))
                .thenAnswer(invocation -> ((SessionExecutionBody) invocation.getArguments()[0]).execute());

        CatalogVersionModel catalogVersion = new CatalogVersionModel();

        when(catalogVersionService.getCatalogVersion(CATALOG, STAGED)).thenReturn(catalogVersion);

        doNothing().when(mediaImportStrategy).importMedia(catalogVersion, mediaDTO, ResourceType.VIDEO);

        mediaImportStrategy.execute(mediaDTO);

        verify(mediaImportStrategy).importMedia(catalogVersion, mediaDTO, ResourceType.VIDEO);
        verify(mediaImportStrategy, times(0)).importMedia(catalogVersion, mediaDTO, ResourceType.DOCUMENT);
        verify(mediaImportStrategy, times(0)).importMedia(catalogVersion, mediaDTO, ResourceType.PHOTO);
    }

    @Test
    public void testImportMedia_noItems() {
        MediaData mediaDTO = new MediaData();
        CatalogVersionModel catalogVersion = mock(CatalogVersionModel.class);
        doReturn(new HashSet<>()).when(mediaImportStrategy).getItems(catalogVersion, mediaDTO);

        mediaImportStrategy.importMedia(catalogVersion, mediaDTO, ResourceType.VIDEO);

        verifyZeroInteractions(mediaFactoryStrategy);
        verify(mediaImportStrategy).getItems(catalogVersion, mediaDTO);
    }

    @Test
    public void testImportMedia() {
        File file = mock(File.class);
        when(file.getName()).thenReturn(QUALIFIER + ".pdf");

        MediaData mediaDTO = new MediaData();
        mediaDTO.setFiles(singletonList(file));
        mediaDTO.setLocalizedNames(Maps.newHashMap(Locale.US, LOCALIZED_NAME));

        CatalogVersionModel catalogVersion = mock(CatalogVersionModel.class);

        ProductModel product = ProductModelMockBuilder.aBaseProduct(CODE, Locale.CHINA).build();

        doReturn(Sets.newLinkedHashSet(product)).when(mediaImportStrategy).getItems(catalogVersion, mediaDTO);

        MediaModel media = new MediaModel();
        media.setExternalID(EXTERNAL_ID);

        when(mediaFactoryStrategy.create(any(MediaFactoryParameter.class))).thenReturn(media);

        doNothing().when(mediaImportStrategy).updateMetaData(EXTERNAL_ID, Sets.newLinkedHashSet(Locale.US), Sets.newLinkedHashSet(product));
        doNothing().when(mediaImportStrategy).updateMediaModel(media, Sets.newLinkedHashSet(Locale.US), Sets.newLinkedHashSet(product));
        doNothing().when(mediaImportStrategy).removeMediaModel(media, Sets.newLinkedHashSet(Locale.US), Sets.newLinkedHashSet(product));

        mediaImportStrategy.importMedia(catalogVersion, mediaDTO, ResourceType.PHOTO);

        verify(mediaImportStrategy).importMedia(catalogVersion, mediaDTO, ResourceType.PHOTO);
        verify(mediaImportStrategy).getItems(catalogVersion, mediaDTO);
        verify(mediaFactoryStrategy).create(any(MediaFactoryParameter.class));
        verify(mediaImportStrategy).updateMetaData(EXTERNAL_ID, Sets.newLinkedHashSet(Locale.US), Sets.newLinkedHashSet(product));
        verify(mediaImportStrategy).updateMediaModel(media, Sets.newLinkedHashSet(Locale.US), Sets.newLinkedHashSet(product));
        verify(mediaImportStrategy).removeMediaModel(media, Sets.newLinkedHashSet(Locale.US), Sets.newLinkedHashSet(product));
    }

    @Test
    public void testImportMediaContainers_noItems() {
        MediaData mediaDTO = new MediaData();
        CatalogVersionModel catalogVersion = mock(CatalogVersionModel.class);
        doReturn(new HashSet<>()).when(mediaImportStrategy).getItems(catalogVersion, mediaDTO);

        mediaImportStrategy.importMediaContainer(catalogVersion, mediaDTO, ResourceType.PHOTO);

        verifyZeroInteractions(mediaContainerFactoryStrategy);
        verify(mediaImportStrategy).getItems(catalogVersion, mediaDTO);
    }

    @Test
    public void testImportMediaContainer() {
        File file = mock(File.class);
        when(file.getName()).thenReturn(QUALIFIER + ".pdf");

        MediaData mediaDTO = new MediaData();
        mediaDTO.setFiles(singletonList(file));
        mediaDTO.setLocalizedNames(Maps.newHashMap(Locale.US, LOCALIZED_NAME));

        CatalogVersionModel catalogVersion = mock(CatalogVersionModel.class);

        ProductModel product = ProductModelMockBuilder.aBaseProduct(CODE, Locale.CHINA).build();

        doReturn(Sets.newLinkedHashSet(product)).when(mediaImportStrategy).getItems(catalogVersion, mediaDTO);

        MediaContainerModel media = new MediaContainerModel();
        media.setExternalID(EXTERNAL_ID);

        when(mediaContainerFactoryStrategy.create(any(MediaFactoryParameter.class))).thenReturn(media);

        doNothing().when(mediaImportStrategy).updateMetaData(EXTERNAL_ID, Sets.newLinkedHashSet(Locale.US), Sets.newLinkedHashSet(product));
        doNothing().when(mediaImportStrategy).updateMediaContainers(singletonList(media), Sets.newLinkedHashSet(Locale.US), Sets.newLinkedHashSet(product));
        doNothing().when(mediaImportStrategy).removeMediaContainers(singletonList(media), Sets.newLinkedHashSet(Locale.US), Sets.newLinkedHashSet(product));

        mediaImportStrategy.importMediaContainer(catalogVersion, mediaDTO, ResourceType.VIDEO);

        verify(mediaImportStrategy).importMediaContainer(catalogVersion, mediaDTO, ResourceType.VIDEO);
        verify(mediaImportStrategy).getItems(catalogVersion, mediaDTO);
        verify(mediaContainerFactoryStrategy).create(any(MediaFactoryParameter.class));
        verify(mediaImportStrategy).updateMetaData(EXTERNAL_ID, Sets.newLinkedHashSet(Locale.US), Sets.newLinkedHashSet(product));
        verify(mediaImportStrategy).updateMediaContainers(singletonList(media), Sets.newLinkedHashSet(Locale.US), Sets.newLinkedHashSet(product));
        verify(mediaImportStrategy).removeMediaContainers(singletonList(media), Sets.newLinkedHashSet(Locale.US), Sets.newLinkedHashSet(product));
    }

    @Test
    public void testUpdateMediaModel() {
        MediaModel media = mock(MediaModel.class);

        ProductModel item = ProductModelMockBuilder.aBaseProduct(CODE, Locale.CHINA).build();

        doNothing().when(mediaImportStrategy).update(media, Locale.CHINA, item, ATTRIBUTE, MEDIA_ATTRIBUTE_TYPE);
        doNothing().when(mediaImportStrategy).update(media, Locale.CANADA, item, ATTRIBUTE, MEDIA_ATTRIBUTE_TYPE);

        mediaImportStrategy.updateMediaModel(media, Sets.newLinkedHashSet(Locale.CHINA, Locale.CANADA), Sets.newLinkedHashSet(item));

        verify(mediaImportStrategy).update(media, Locale.CHINA, item, ATTRIBUTE, MEDIA_ATTRIBUTE_TYPE);
        verify(mediaImportStrategy).update(media, Locale.CANADA, item, ATTRIBUTE, MEDIA_ATTRIBUTE_TYPE);
        verify(modelService).save(item);
    }

    @Test
    public void testRemoveMediaModel() {
        MediaModel media = mock(MediaModel.class);
        ProductModel item = ProductModelMockBuilder.aBaseProduct(CODE, Locale.CHINA).build();

        Set<Locale> requiredLocales = Sets.newLinkedHashSet(Locale.CHINA, Locale.CANADA);
        Set<Locale> deprecatedLocales = Sets.newLinkedHashSet(Locale.ITALIAN);

        doReturn(deprecatedLocales).when(mediaImportStrategy).getDeprecatedLocales(requiredLocales);

        doNothing().when(mediaImportStrategy).removeDeprecatedMediaModel(media, deprecatedLocales, Sets.newLinkedHashSet(item));
        mediaImportStrategy.removeMediaModel(media, requiredLocales, Sets.newLinkedHashSet(item));

        verify(mediaImportStrategy).removeDeprecatedMediaModel(media, deprecatedLocales, Sets.newLinkedHashSet(item));
        verify(mediaImportStrategy, times(0)).removeDeprecatedMediaModel(media, requiredLocales, Sets.newLinkedHashSet(item));
    }

    @Test
    public void testRemoveDeprecatedMediaModel() {
        MediaModel media = mock(MediaModel.class);
        ProductModel item = ProductModelMockBuilder.aBaseProduct(CODE, Locale.CHINA).build();

        Set<Locale> locales = Sets.newLinkedHashSet(Locale.CHINA, Locale.CANADA);

        doNothing().when(mediaImportStrategy).remove(media, Locale.CHINA, item, ATTRIBUTE, MEDIA_ATTRIBUTE_TYPE);
        doNothing().when(mediaImportStrategy).remove(media, Locale.CANADA, item, ATTRIBUTE, MEDIA_ATTRIBUTE_TYPE);

        mediaImportStrategy.removeDeprecatedMediaModel(media, locales, Sets.newLinkedHashSet(item));

        verify(mediaImportStrategy).remove(media, Locale.CHINA, item, ATTRIBUTE, MEDIA_ATTRIBUTE_TYPE);
        verify(mediaImportStrategy).remove(media, Locale.CANADA, item, ATTRIBUTE, MEDIA_ATTRIBUTE_TYPE);
        verify(modelService).save(item);
    }

    @Test
    public void testUpdateMediaContainers_localized() {
        mediaImportStrategy.setMediaAttributeType(MediaAttributeType.LOCALIZED_MEDIA_CONTAINER);

        MediaContainerModel media = mock(MediaContainerModel.class);
        ProductModel item = ProductModelMockBuilder.aBaseProduct(CODE, Locale.CHINA).build();

        Set<Locale> requiredLocales = Sets.newLinkedHashSet(Locale.CHINA, Locale.CANADA);
        Set<Locale> deprecatedLocales = Sets.newLinkedHashSet(Locale.CHINA, Locale.CANADA);

        doReturn(deprecatedLocales).when(mediaImportStrategy).getDeprecatedLocales(requiredLocales);
        doNothing().when(mediaImportStrategy).update(media, Locale.CHINA, item, ATTRIBUTE, MediaAttributeType.LOCALIZED_MEDIA_CONTAINER);
        doNothing().when(mediaImportStrategy).update(media, Locale.CANADA, item, ATTRIBUTE, MediaAttributeType.LOCALIZED_MEDIA_CONTAINER);

        mediaImportStrategy.updateMediaContainers(singletonList(media), requiredLocales, Sets.newLinkedHashSet(item));

        verify(mediaImportStrategy).update(media, Locale.CHINA, item, ATTRIBUTE, MediaAttributeType.LOCALIZED_MEDIA_CONTAINER);
        verify(mediaImportStrategy).update(media, Locale.CANADA, item, ATTRIBUTE, MediaAttributeType.LOCALIZED_MEDIA_CONTAINER);
        verify(mediaImportStrategy, times(0)).update(media, item, ATTRIBUTE, MediaAttributeType.LIST);
        verify(modelService).save(item);
    }

    @Test
    public void testUpdateMediaContainers_notLocalized() {
        mediaImportStrategy.setMediaAttributeType(MediaAttributeType.LIST);

        MediaContainerModel media = mock(MediaContainerModel.class);
        ProductModel item = ProductModelMockBuilder.aBaseProduct(CODE, Locale.CHINA).build();

        Set<Locale> requiredLocales = Sets.newLinkedHashSet(Locale.CHINA, Locale.CANADA);

        doNothing().when(mediaImportStrategy).update(media, item, ATTRIBUTE, MediaAttributeType.LIST);

        mediaImportStrategy.updateMediaContainers(singletonList(media), requiredLocales, Sets.newLinkedHashSet(item));

        verify(mediaImportStrategy).update(media, item, ATTRIBUTE, MediaAttributeType.LIST);
        verify(mediaImportStrategy, times(0)).update(media, item, ATTRIBUTE, MediaAttributeType.LOCALIZED_MEDIA_CONTAINER);
        verify(modelService).save(item);
    }

    @Test
    public void testRemoveMediaContainer_notLocalized() {
        mediaImportStrategy.setMediaAttributeType(MediaAttributeType.LIST);

        MediaContainerModel media = mock(MediaContainerModel.class);
        ProductModel item = ProductModelMockBuilder.aBaseProduct(CODE, Locale.CHINA).build();

        Set<Locale> requiredLocales = Sets.newLinkedHashSet(Locale.CHINA, Locale.CANADA);

        doNothing().when(mediaImportStrategy).remove(media, item, ATTRIBUTE, MediaAttributeType.LIST);

        mediaImportStrategy.removeMediaContainers(singletonList(media), requiredLocales, Sets.newLinkedHashSet(item));

        verify(mediaImportStrategy).remove(media, item, ATTRIBUTE, MediaAttributeType.LIST);
        verify(mediaImportStrategy, times(0)).remove(eq(media), any(Locale.class), eq(item), eq(ATTRIBUTE), eq(MediaAttributeType.LIST));
        verify(modelService).save(item);
    }

    @Test
    public void testRemoveMediaContainer_localized() {
        mediaImportStrategy.setMediaAttributeType(MediaAttributeType.LOCALIZED_MEDIA_CONTAINER);

        MediaContainerModel media = mock(MediaContainerModel.class);
        ProductModel item = ProductModelMockBuilder.aBaseProduct(CODE, Locale.CHINA).build();

        Set<Locale> requiredLocales = Sets.newLinkedHashSet(Locale.CHINA, Locale.CANADA);
        Set<Locale> deprecatedLocales = Sets.newLinkedHashSet(Locale.CHINA, Locale.CANADA);

        doReturn(deprecatedLocales).when(mediaImportStrategy).getDeprecatedLocales(requiredLocales);
        doNothing().when(mediaImportStrategy).remove(media, Locale.CHINA, item, ATTRIBUTE, MediaAttributeType.LOCALIZED_MEDIA_CONTAINER);
        doNothing().when(mediaImportStrategy).remove(media, Locale.CANADA, item, ATTRIBUTE, MediaAttributeType.LOCALIZED_MEDIA_CONTAINER);

        mediaImportStrategy.removeMediaContainers(singletonList(media), requiredLocales, Sets.newLinkedHashSet(item));

        verify(mediaImportStrategy).remove(media, Locale.CHINA, item, ATTRIBUTE, MediaAttributeType.LOCALIZED_MEDIA_CONTAINER);
        verify(mediaImportStrategy).remove(media, Locale.CANADA, item, ATTRIBUTE, MediaAttributeType.LOCALIZED_MEDIA_CONTAINER);
        verify(mediaImportStrategy, times(0)).remove(media, item, ATTRIBUTE, MediaAttributeType.LIST);
        verify(modelService).save(item);
    }

    @Test
    public void testUpdate_WithMediaModel_singleValue() {
        mediaImportStrategy.setMediaAttributeType(MediaAttributeType.SINGLE_VALUE);

        MediaModel media = mock(MediaModel.class);
        ProductModel item = ProductModelMockBuilder.aBaseProduct(CODE, Locale.CHINA).build();

        doNothing().when(mediaImportStrategy).updateSingleMedia(media, Locale.CHINA, item, ATTRIBUTE);

        mediaImportStrategy.update(media, Locale.CHINA, item, ATTRIBUTE, MediaAttributeType.SINGLE_VALUE);

        verify(mediaImportStrategy).updateSingleMedia(media, Locale.CHINA, item, ATTRIBUTE);
        verify(mediaImportStrategy, times(0)).updateMediaCollection(media, Locale.CHINA, item, ATTRIBUTE);
    }

    @Test
    public void testUpdate_WithMediaModel_list() {
        mediaImportStrategy.setMediaAttributeType(MediaAttributeType.LIST);

        MediaModel media = mock(MediaModel.class);
        ProductModel item = ProductModelMockBuilder.aBaseProduct(CODE, Locale.CHINA).build();

        doNothing().when(mediaImportStrategy).updateMediaCollection(media, Locale.CHINA, item, ATTRIBUTE);

        mediaImportStrategy.update(media, Locale.CHINA, item, ATTRIBUTE, MEDIA_ATTRIBUTE_TYPE);

        verify(mediaImportStrategy, times(0)).updateSingleMedia(media, Locale.CHINA, item, ATTRIBUTE);
        verify(mediaImportStrategy).updateMediaCollection(media, Locale.CHINA, item, ATTRIBUTE);
    }

    @Test
    public void testUpdate_withMediaContainer_localized() {
        ArgumentCaptor<Map<Locale, List<MediaContainerModel>>> captor = new ArgumentCaptor<>();

        MediaContainerModel newMedia = mock(MediaContainerModel.class);
        MediaContainerModel oldMedia = mock(MediaContainerModel.class);
        List<MediaContainerModel> updatedContainers = singletonList(newMedia);
        ProductModel item = ProductModelMockBuilder.aBaseProduct(CODE, Locale.CHINA).build();

        List<MediaContainerModel> containers = new ArrayList<>();
        containers.add(oldMedia);

        when(modelService.getAttributeValue(item, ATTRIBUTE, Locale.CHINA)).thenReturn(containers);
        doReturn(updatedContainers).when(mediaImportStrategy).updateMediaContainer(newMedia, singletonList(oldMedia));

        mediaImportStrategy.update(newMedia, Locale.CHINA, item, ATTRIBUTE, MEDIA_ATTRIBUTE_TYPE);

        verify(modelService).setAttributeValue(eq(item), eq(ATTRIBUTE), captor.capture());

        Assertions
                .assertThat(captor.getValue().get(Locale.CHINA))
                .isEqualTo(updatedContainers);
    }

    @Test
    public void testUpdate_withMediaContainer_notLocalized() {
        ArgumentCaptor<List<MediaContainerModel>> captor = new ArgumentCaptor<>();

        MediaContainerModel newMedia = mock(MediaContainerModel.class);
        MediaContainerModel oldMedia = mock(MediaContainerModel.class);
        List<MediaContainerModel> updatedContainers = singletonList(newMedia);
        ProductModel item = ProductModelMockBuilder.aBaseProduct(CODE, Locale.CHINA).build();

        List<MediaContainerModel> containers = new ArrayList<>();
        containers.add(oldMedia);

        when(modelService.getAttributeValue(item, ATTRIBUTE)).thenReturn(containers);
        doReturn(updatedContainers).when(mediaImportStrategy).updateMediaContainer(newMedia, singletonList(oldMedia));

        mediaImportStrategy.update(newMedia, item, ATTRIBUTE, MEDIA_ATTRIBUTE_TYPE);

        verify(modelService).setAttributeValue(eq(item), eq(ATTRIBUTE), captor.capture());

        Assertions
                .assertThat(captor.getValue())
                .isEqualTo(updatedContainers);
    }

    @Test
    public void testRemove_WithMediaModel_singleValue() {
        mediaImportStrategy.setMediaAttributeType(MediaAttributeType.SINGLE_VALUE);

        MediaModel media = mock(MediaModel.class);
        ProductModel item = ProductModelMockBuilder.aBaseProduct(CODE, Locale.CHINA).build();

        doNothing().when(mediaImportStrategy).removeSingleMedia(media, Locale.CHINA, item, ATTRIBUTE);

        mediaImportStrategy.remove(media, Locale.CHINA, item, ATTRIBUTE, MediaAttributeType.SINGLE_VALUE);

        verify(mediaImportStrategy).removeSingleMedia(media, Locale.CHINA, item, ATTRIBUTE);
        verify(mediaImportStrategy, times(0)).removeMediaFromList(media, Locale.CHINA, item, ATTRIBUTE);
    }

    @Test
    public void testRemove_WithMediaModel_list() {
        mediaImportStrategy.setMediaAttributeType(MediaAttributeType.LIST);

        MediaModel media = mock(MediaModel.class);
        ProductModel item = ProductModelMockBuilder.aBaseProduct(CODE, Locale.CHINA).build();

        doNothing().when(mediaImportStrategy).removeMediaFromList(media, Locale.CHINA, item, ATTRIBUTE);

        mediaImportStrategy.remove(media, Locale.CHINA, item, ATTRIBUTE, MediaAttributeType.LIST);

        verify(mediaImportStrategy, times(0)).removeSingleMedia(media, Locale.CHINA, item, ATTRIBUTE);
        verify(mediaImportStrategy).removeMediaFromList(media, Locale.CHINA, item, ATTRIBUTE);
    }

    @Test
    public void testRemove_withMediaContainer_notLocalized() {
        ArgumentCaptor<List<MediaContainerModel>> captor = new ArgumentCaptor<>();

        mediaImportStrategy.setMediaAttributeType(MediaAttributeType.LIST);

        MediaContainerModel newMedia = mock(MediaContainerModel.class);
        MediaContainerModel oldMedia1 = mock(MediaContainerModel.class);
        MediaContainerModel oldMedia2 = mock(MediaContainerModel.class);
        List<MediaContainerModel> existingContainers = new ArrayList<>();
        existingContainers.add(oldMedia1);
        existingContainers.add(oldMedia2);

        ProductModel item = ProductModelMockBuilder.aBaseProduct(CODE, Locale.CHINA).build();

        when(modelService.getAttributeValue(item, ATTRIBUTE)).thenReturn(existingContainers);
        doReturn(true).when(mediaImportStrategy).isSameContainer(newMedia, oldMedia1);
        doReturn(false).when(mediaImportStrategy).isSameContainer(newMedia, oldMedia2);

        mediaImportStrategy.remove(newMedia, item, ATTRIBUTE, MediaAttributeType.LIST);

        verify(modelService).setAttributeValue(eq(item), eq(ATTRIBUTE), captor.capture());

        Assertions
                .assertThat(captor.getValue())
                .containsExactly(oldMedia2);
    }

    @Test
    public void testRemove_withMediaContainer_localized() {
        ArgumentCaptor<Map<Locale, List<MediaContainerModel>>> captor = new ArgumentCaptor<>();

        mediaImportStrategy.setMediaAttributeType(MediaAttributeType.LIST);

        MediaContainerModel newMedia = mock(MediaContainerModel.class);
        MediaContainerModel oldMedia1 = mock(MediaContainerModel.class);
        MediaContainerModel oldMedia2 = mock(MediaContainerModel.class);
        List<MediaContainerModel> existingContainers = new ArrayList<>();
        existingContainers.add(oldMedia1);
        existingContainers.add(oldMedia2);

        ProductModel item = ProductModelMockBuilder.aBaseProduct(CODE, Locale.CHINA).build();

        when(modelService.getAttributeValue(item, ATTRIBUTE, Locale.CHINA)).thenReturn(existingContainers);
        doReturn(true).when(mediaImportStrategy).isSameContainer(newMedia, oldMedia1);
        doReturn(false).when(mediaImportStrategy).isSameContainer(newMedia, oldMedia2);

        mediaImportStrategy.remove(newMedia, Locale.CHINA, item, ATTRIBUTE, MediaAttributeType.LIST);

        verify(modelService).setAttributeValue(eq(item), eq(ATTRIBUTE), captor.capture());

        Assertions
                .assertThat(captor.getValue().get(Locale.CHINA))
                .containsExactly(oldMedia2);
    }

    @Test
    public void testUpdateSingleMedia() {
        ArgumentCaptor<Map<Locale, MediaModel>> captor = new ArgumentCaptor<>();

        MediaModel newMedia = mock(MediaModel.class);

        ProductModel item = ProductModelMockBuilder.aBaseProduct(CODE, Locale.CHINA).build();

        mediaImportStrategy.updateSingleMedia(newMedia, Locale.CHINA, item, ATTRIBUTE);

        verify(modelService).setAttributeValue(eq(item), eq(ATTRIBUTE), captor.capture());

        Assertions
                .assertThat(captor.getValue().get(Locale.CHINA))
                .isEqualTo(newMedia);
    }

    @Test
    public void testUpdateMediaCollection() {
        ArgumentCaptor<Map<Locale, Collection<MediaModel>>> captor = new ArgumentCaptor<>();

        MediaModel oldMedia = mock(MediaModel.class);

        Collection<MediaModel> existingCollection = new ArrayList<>();
        existingCollection.add(oldMedia);

        MediaModel newMedia = mock(MediaModel.class);

        Collection<MediaModel> updatedList = new ArrayList<>();
        updatedList.add(newMedia);

        ProductModel item = ProductModelMockBuilder.aBaseProduct(CODE, Locale.CHINA).build();

        when(modelService.getAttributeValue(item, ATTRIBUTE, Locale.CHINA)).thenReturn(existingCollection);

        doReturn(updatedList).when(mediaImportStrategy).updateMediaModels(newMedia, existingCollection);

        mediaImportStrategy.updateMediaCollection(newMedia, Locale.CHINA, item, ATTRIBUTE);

        verify(modelService).setAttributeValue(eq(item), eq(ATTRIBUTE), captor.capture());

        Assertions
                .assertThat(captor.getValue().get(Locale.CHINA))
                .containsOnly(newMedia);
    }

    @Test
    public void testRemoveSingleMedia() {
        ArgumentCaptor<Map<Locale, MediaModel>> captor = new ArgumentCaptor<>();

        MediaModel oldMedia = mock(MediaModel.class);
        MediaModel newMedia = mock(MediaModel.class);
        ProductModel item = ProductModelMockBuilder.aBaseProduct(CODE, Locale.CHINA).build();

        when(modelService.getAttributeValue(item, ATTRIBUTE, Locale.CHINA)).thenReturn(oldMedia);

        doReturn(true).when(mediaImportStrategy).isSameMediaModel(newMedia, oldMedia);

        mediaImportStrategy.removeSingleMedia(newMedia, Locale.CHINA, item, ATTRIBUTE);

        verify(modelService).getAttributeValue(item, ATTRIBUTE, Locale.CHINA);
        verify(modelService).setAttributeValue(eq(item), eq(ATTRIBUTE), captor.capture());

        Assertions
                .assertThat(captor.getValue().keySet())
                .containsOnly(Locale.CHINA);

        Assertions
                .assertThat(captor.getValue().get(Locale.CHINA))
                .isNull();
    }

    @Test
    public void testRemoveSingleMedia_existingMediaNull() {
        ArgumentCaptor<Map<Locale, MediaModel>> captor = new ArgumentCaptor<>();

        MediaModel oldMedia = mock(MediaModel.class);
        MediaModel newMedia = mock(MediaModel.class);
        ProductModel item = ProductModelMockBuilder.aBaseProduct(CODE, Locale.CHINA).build();

        when(modelService.getAttributeValue(item, ATTRIBUTE, Locale.CHINA)).thenReturn(null);

        doReturn(true).when(mediaImportStrategy).isSameMediaModel(newMedia, oldMedia);

        mediaImportStrategy.removeSingleMedia(newMedia, Locale.CHINA, item, ATTRIBUTE);

        verify(modelService).getAttributeValue(item, ATTRIBUTE, Locale.CHINA);

        verify(modelService, never()).setAttributeValue(eq(item), eq(ATTRIBUTE), any());
    }

    @Test
    public void testRemoveMediaFromList() {
        ArgumentCaptor<Map<Locale, Collection<MediaModel>>> captor = new ArgumentCaptor<>();

        CatalogVersionModel catalogVersion1 = mock(CatalogVersionModel.class);
        CatalogVersionModel catalogVersion2 = mock(CatalogVersionModel.class);
        MediaModel media1 = new MediaModel();
        media1.setCode(CODE);
        media1.setCatalogVersion(catalogVersion1);

        MediaModel media2 = new MediaModel();
        media2.setCode(CODE + "2");
        media2.setCatalogVersion(catalogVersion2);

        MediaModel media3 = new MediaModel();
        media3.setCode(CODE + "2");
        media3.setCatalogVersion(catalogVersion2);

        List<MediaModel> existingList = new ArrayList<>();
        existingList.add(media1);
        existingList.add(media2);

        Map<Locale, Collection<MediaModel>> map = new HashMap<>();
        map.put(Locale.CHINA, existingList);

        List<MediaModel> newList = new ArrayList<>();
        newList.add(media1);

        ProductModel item = ProductModelMockBuilder.aBaseProduct(CODE, Locale.CHINA).build();

        when(modelService.getAttributeValue(item, ATTRIBUTE, Locale.CHINA)).thenReturn(newList);

        mediaImportStrategy.removeMediaFromList(media3, Locale.CHINA, item, ATTRIBUTE);

        verify(modelService).setAttributeValue(eq(item), eq(ATTRIBUTE), captor.capture());

        Assertions
                .assertThat(captor.getValue().get(Locale.CHINA))
                .containsOnly(media1);
    }

    @Test
    public void testExtractQualifier() {
        String filename = "qualifier.therest";

        String result = mediaImportStrategy.extractQualifier(filename);

        Assertions
                .assertThat(result)
                .isEqualTo("qualifier");
    }

    @Test
    public void testUpdateMediaModels() {
        MediaModel newMedia = mock(MediaModel.class);
        MediaModel oldMedia = mock(MediaModel.class);
        List<MediaModel> existingMedias = singletonList(oldMedia);

        doNothing().when(mediaImportStrategy).handleMediaModels(existingMedias, newMedia);

        mediaImportStrategy.updateMediaModels(newMedia, existingMedias);

        verify(mediaImportStrategy).handleMediaModels(existingMedias, newMedia);
    }

    @Test
    public void testUpdateMediaContainers2() {
        MediaContainerModel newMedia = mock(MediaContainerModel.class);
        MediaContainerModel oldMedia = mock(MediaContainerModel.class);
        Collection<MediaContainerModel> existingMedias = singletonList(oldMedia);

        ArgumentCaptor<List> captor = ArgumentCaptor.forClass(List.class);
        doNothing().when(mediaImportStrategy).handleMediaContainers(captor.capture(), eq(newMedia));

        List<MediaContainerModel> result = mediaImportStrategy.updateMediaContainer(newMedia, existingMedias);

        verify(mediaImportStrategy).handleMediaContainers(captor.getValue(), newMedia);

        Assertions
                .assertThat(captor.getValue())
                .containsExactly(oldMedia);

        Assertions
                .assertThat(result)
                .containsExactly(oldMedia);
    }

    @Test
    public void testHandleMediaModels_sameCode() {
        MediaModel newMedia = mock(MediaModel.class);
        MediaModel oldMedia = mock(MediaModel.class);
        List<MediaModel> existingMedias = new ArrayList<>();
        existingMedias.add(oldMedia);

        when(newMedia.getCode()).thenReturn(CODE);
        when(oldMedia.getCode()).thenReturn(CODE);

        mediaImportStrategy.handleMediaModels(existingMedias, newMedia);

        Assertions
                .assertThat(existingMedias)
                .containsExactly(newMedia);
    }

    @Test
    public void testHandleMediaModels_differentCode() {
        MediaModel newMedia = mock(MediaModel.class);
        MediaModel oldMedia = mock(MediaModel.class);
        List<MediaModel> existingMedias = new ArrayList<>();
        existingMedias.add(oldMedia);

        when(newMedia.getCode()).thenReturn(CODE);
        when(oldMedia.getCode()).thenReturn(CODE + "2");
        doReturn(1).when(mediaImportStrategy).getSequenceId(newMedia);
        doReturn(2).when(mediaImportStrategy).getSequenceId(oldMedia);

        mediaImportStrategy.handleMediaModels(existingMedias, newMedia);

        Assertions
                .assertThat(existingMedias.get(0))
                .isEqualTo(newMedia);

        Assertions
                .assertThat(existingMedias.get(1))
                .isEqualTo(oldMedia);
    }

    @Test
    public void testHandleMediaModels_sameQualifier() {
        MediaContainerModel newMedia = mock(MediaContainerModel.class);
        MediaContainerModel oldMedia = mock(MediaContainerModel.class);
        List<MediaContainerModel> existingMedias = Arrays.asList(oldMedia);

        when(newMedia.getQualifier()).thenReturn(CODE);
        when(oldMedia.getQualifier()).thenReturn(CODE);

        mediaImportStrategy.handleMediaContainers(existingMedias, newMedia);

        Assertions
                .assertThat(existingMedias)
                .containsExactly(newMedia);
    }

    @Test
    public void testHandleMediaModels_differentQualifier() {
        MediaContainerModel newMedia = mock(MediaContainerModel.class);
        MediaContainerModel oldMedia = mock(MediaContainerModel.class);
        List<MediaContainerModel> existingMedias = new ArrayList<>();
        existingMedias.add(oldMedia);

        when(newMedia.getQualifier()).thenReturn(CODE);
        when(oldMedia.getQualifier()).thenReturn(CODE + "2");

        doReturn(1).when(mediaImportStrategy).getSequenceId(newMedia);
        doReturn(2).when(mediaImportStrategy).getSequenceId(oldMedia);

        mediaImportStrategy.handleMediaContainers(existingMedias, newMedia);

        Assertions
                .assertThat(existingMedias.get(0))
                .isEqualTo(newMedia);

        Assertions
                .assertThat(existingMedias.get(1))
                .isEqualTo(oldMedia);
    }

    @Test
    public void testGetSequenceId_mediamodel_success() {
        MediaModel media = mock(MediaModel.class);
        when(media.getCode()).thenReturn(CODE + "$4");

        int result = mediaImportStrategy.getSequenceId(media);

        Assertions
                .assertThat(result)
                .isEqualTo(4);
    }

    @Test
    public void testGetSequenceId_mediamodel_noMatch() {
        MediaModel media = mock(MediaModel.class);
        when(media.getCode()).thenReturn(CODE);

        int result = mediaImportStrategy.getSequenceId(media);

        Assertions
                .assertThat(result)
                .isEqualTo(0);
    }

    @Test
    public void testGetSequenceId_container_success() {
        MediaContainerModel media = mock(MediaContainerModel.class);
        when(media.getQualifier()).thenReturn(CODE + "$4");

        int result = mediaImportStrategy.getSequenceId(media);

        Assertions
                .assertThat(result)
                .isEqualTo(4);
    }

    @Test
    public void testGetSequenceId_container_noMatch() {
        MediaContainerModel media = mock(MediaContainerModel.class);
        when(media.getQualifier()).thenReturn(CODE);

        int result = mediaImportStrategy.getSequenceId(media);

        Assertions
                .assertThat(result)
                .isEqualTo(0);
    }

    @Test
    public void testGetDeprecatedLocales() {
        Set<Locale> requiredLocales = Sets.newLinkedHashSet(Locale.ENGLISH, Locale.CHINESE);
        Set<Locale> supportedLocales = Sets.newLinkedHashSet(Locale.ENGLISH, Locale.KOREAN);

        when(i18NService.getSupportedLocales()).thenReturn(supportedLocales);

        Set<Locale> result = mediaImportStrategy.getDeprecatedLocales(requiredLocales);

        Assertions
                .assertThat(result)
                .containsOnly(Locale.KOREAN);
    }

    @Test
    public void testIsSameMediaModel_same() {
        MediaModel newMedia = new MediaModel();
        MediaModel oldMedia = new MediaModel();
        CatalogVersionModel catalogVersion = mock(CatalogVersionModel.class);

        newMedia.setCode(CODE);
        newMedia.setCatalogVersion(catalogVersion);
        oldMedia.setCode(CODE);
        oldMedia.setCatalogVersion(catalogVersion);

        Assertions
                .assertThat(mediaImportStrategy.isSameMediaModel(newMedia, oldMedia))
                .isTrue();
    }

    @Test
    public void testIsSameMediaModel_differentCode() {
        MediaModel newMedia = new MediaModel();
        MediaModel oldMedia = new MediaModel();
        CatalogVersionModel catalogVersion = mock(CatalogVersionModel.class);

        newMedia.setCode(CODE);
        newMedia.setCatalogVersion(catalogVersion);
        oldMedia.setCode(CODE + "1");
        oldMedia.setCatalogVersion(catalogVersion);

        Assertions
                .assertThat(mediaImportStrategy.isSameMediaModel(newMedia, oldMedia))
                .isFalse();
    }

    @Test
    public void testIsSameMediaModel_differentCatalogVersion() {
        MediaModel newMedia = new MediaModel();
        MediaModel oldMedia = new MediaModel();
        CatalogVersionModel catalogVersion1 = mock(CatalogVersionModel.class);
        CatalogVersionModel catalogVersion2 = mock(CatalogVersionModel.class);

        newMedia.setCode(CODE);
        newMedia.setCatalogVersion(catalogVersion1);
        oldMedia.setCode(CODE);
        oldMedia.setCatalogVersion(catalogVersion2);

        Assertions
                .assertThat(mediaImportStrategy.isSameMediaModel(newMedia, oldMedia))
                .isFalse();
    }

    @Test
    public void testIsSameMediaContainerModel_same() {
        MediaContainerModel newMedia = new MediaContainerModel();
        MediaContainerModel oldMedia = new MediaContainerModel();
        CatalogVersionModel catalogVersion = mock(CatalogVersionModel.class);

        newMedia.setQualifier(CODE);
        newMedia.setCatalogVersion(catalogVersion);
        oldMedia.setQualifier(CODE);
        oldMedia.setCatalogVersion(catalogVersion);

        Assertions
                .assertThat(mediaImportStrategy.isSameContainer(newMedia, oldMedia))
                .isTrue();
    }

    @Test
    public void testIsSameMediaContainerModel_differentCode() {
        MediaContainerModel newMedia = new MediaContainerModel();
        MediaContainerModel oldMedia = new MediaContainerModel();
        CatalogVersionModel catalogVersion = mock(CatalogVersionModel.class);

        newMedia.setQualifier(CODE);
        newMedia.setCatalogVersion(catalogVersion);
        oldMedia.setQualifier(CODE + "1");
        oldMedia.setCatalogVersion(catalogVersion);

        Assertions
                .assertThat(mediaImportStrategy.isSameContainer(newMedia, oldMedia))
                .isFalse();
    }

    @Test
    public void testIsSameMediaContainerModel_differentCatalogVersion() {
        MediaContainerModel newMedia = new MediaContainerModel();
        MediaContainerModel oldMedia = new MediaContainerModel();
        CatalogVersionModel catalogVersion1 = mock(CatalogVersionModel.class);
        CatalogVersionModel catalogVersion2 = mock(CatalogVersionModel.class);

        newMedia.setQualifier(CODE);
        newMedia.setCatalogVersion(catalogVersion1);
        oldMedia.setQualifier(CODE);
        oldMedia.setCatalogVersion(catalogVersion2);

        Assertions
                .assertThat(mediaImportStrategy.isSameContainer(newMedia, oldMedia))
                .isFalse();
    }

    @Test
    public void testUpdateMetaDataWithPhoto() {
        mediaImportStrategy.setResourceType(ResourceType.DOCUMENT);
        mediaImportStrategy.setMediaType(MEDIA_TYPE);
        mediaImportStrategy.updateMetaData(EXTERNAL_ID, null, null);

        verify(integrationService).updateField(EXTERNAL_ID, MetaDataField.DOCUMENT_TYPE, MEDIA_TYPE);
        verify(integrationService, times(0)).updateField(EXTERNAL_ID, MetaDataField.PHOTO_TYPE, MEDIA_TYPE);
    }

    @Test
    public void testUpdateMetaDataWithDocument() {
        mediaImportStrategy.setResourceType(ResourceType.PHOTO);
        mediaImportStrategy.setMediaType(MEDIA_TYPE);
        mediaImportStrategy.updateMetaData(EXTERNAL_ID, null, null);

        verify(integrationService).updateField(EXTERNAL_ID, MetaDataField.PHOTO_TYPE, MEDIA_TYPE);
        verify(integrationService, times(0)).updateField(EXTERNAL_ID, MetaDataField.DOCUMENT_TYPE, MEDIA_TYPE);
    }

    /**
     * The {@code MockVMKMediaImportStrategy} class is used to test the business logic defined in the
     * {@link AbstractVMKMediaImportStrategy} class.
     */
    protected static class MockVMKMediaImportStrategy extends AbstractVMKMediaImportStrategy<ProductModel> {

        /**
         * {@inheritDoc}
         */
        public MockVMKMediaImportStrategy() {
        }

        /**
         * Extracts the items to update the media for from the given {@code mediaDTO}.
         *
         * @param catalogVersion the catalog version to import the media for
         * @param mediaDTO       the media to import
         * @return the items to update the media for
         */
        @Override
        public Set<ProductModel> getItems(CatalogVersionModel catalogVersion, MediaData mediaDTO) {
            return null;
        }
    }
}