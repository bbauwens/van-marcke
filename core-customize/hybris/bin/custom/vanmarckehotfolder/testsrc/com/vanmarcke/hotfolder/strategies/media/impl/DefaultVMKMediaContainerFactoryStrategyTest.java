package com.vanmarcke.hotfolder.strategies.media.impl;

import com.vanmarcke.hotfolder.data.MediaFactoryParameter;
import com.vanmarcke.hotfolder.resourcespace.enums.ResourceType;
import com.vanmarcke.hotfolder.resourcespace.enums.Size;
import com.vanmarcke.hotfolder.resourcespace.service.VMKResourceSpaceService;
import com.vanmarcke.hotfolder.services.VMKMediaService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaFolderModel;
import de.hybris.platform.core.model.media.MediaFormatModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import org.apache.commons.lang3.RandomStringUtils;
import org.assertj.core.util.Lists;
import org.fest.assertions.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.File;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import static org.mockito.Mockito.*;

/**
 * The {@code DefaultVMKMediaContainerFactoryStrategyTest} class contains the unit tests for the
 * {@link DefaultVMKMediaContainerFactoryStrategy} class.
 *
 * @author Taki Korovessis, Christiaan Janssen
 * @since 29-10-2019
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultVMKMediaContainerFactoryStrategyTest {

    private static final String QUALIFIER = RandomStringUtils.random(10);
    private static final String OLD_EXTERNAL_ID = RandomStringUtils.random(10);
    private static final String EXTERNAL_ID = RandomStringUtils.random(10);
    private static final String EXTENSION = "jpg";
    private static final String BASE_NAME = RandomStringUtils.randomAlphabetic(10);
    private static final String FILE_NAME = BASE_NAME + "." + EXTENSION;
    private static final String FOLDER_QUALIFIER = RandomStringUtils.random(10);
    private static final String NAME_US = RandomStringUtils.random(10);
    private static final String NAME_UK = RandomStringUtils.random(10);

    private static final String ORIGINAL_URL = RandomStringUtils.random(10);
    private static final String EXTRA_SMALL_URL = RandomStringUtils.random(10);
    private static final String SMALL_URL = RandomStringUtils.random(10);
    private static final String MEDIUM_URL = RandomStringUtils.random(10);
    private static final String THUMBNAIL_URL = RandomStringUtils.random(10);
    private static final String THUMBNAIL_VM_URL = RandomStringUtils.random(10);
    private static final String LARGE_URL = RandomStringUtils.random(10);
    private static final String EXTRA_LARGE_URL = RandomStringUtils.random(10);
    private static final String PREVIEW_URL = RandomStringUtils.random(10);
    private static final String SCREEN_URL = RandomStringUtils.random(10);

    @Mock
    private ModelService modelService;

    @Mock
    private VMKResourceSpaceService integrationService;

    @Mock
    private VMKMediaService mediaService;

    @InjectMocks
    private DefaultVMKMediaContainerFactoryStrategy mediaContainerFactoryStrategy;

    @Test
    public void testCreateWithRecentlyUpdatedItem() {
        CatalogVersionModel catalogVersion = new CatalogVersionModel();

        MediaFactoryParameter parameter = new MediaFactoryParameter();
        parameter.setCatalogVersion(catalogVersion);
        parameter.setQualifier(QUALIFIER);

        MediaContainerModel container = mock(MediaContainerModel.class);
        when(container.getExternalID()).thenReturn(EXTERNAL_ID);
        when(container.getModifiedtime()).thenReturn(new Date());

        when(mediaService.getMediaContainer(catalogVersion, QUALIFIER)).thenReturn(container);

        MediaContainerModel actualMedia = mediaContainerFactoryStrategy.create(parameter);

        Assertions
                .assertThat(actualMedia)
                .isEqualTo(container);

        verify(mediaService).getMediaContainer(catalogVersion, QUALIFIER);
        verifyZeroInteractions(integrationService);
        verifyNoMoreInteractions(mediaService);
        verifyZeroInteractions(modelService);
    }

    @Test
    public void testCreateWithNewItem() {
        CatalogVersionModel catalogVersion = new CatalogVersionModel();

        File file = mock(File.class);
        when(file.getName()).thenReturn(FILE_NAME);

        MediaFactoryParameter parameter = new MediaFactoryParameter();
        parameter.setCatalogVersion(catalogVersion);
        parameter.setQualifier(QUALIFIER);
        parameter.setFile(file);
        parameter.setType("PHOTO");
        parameter.setFolderQualifier(FOLDER_QUALIFIER);

        UnknownIdentifierException e = mock(UnknownIdentifierException.class);

        when(mediaService.getMediaContainer(catalogVersion, QUALIFIER)).thenThrow(e);

        MediaContainerModel container = mock(MediaContainerModel.class);
        when(container.getModifiedtime()).thenReturn(new Date());

        when(modelService.create(MediaContainerModel.class)).thenReturn(container);

        when(integrationService.create(file, BASE_NAME, ResourceType.PHOTO)).thenReturn(EXTERNAL_ID);

        MediaFolderModel folder = new MediaFolderModel();

        when(mediaService.getOrCreateMediaFolder(FOLDER_QUALIFIER)).thenReturn(folder);

        when(mediaService.getMedia(catalogVersion, QUALIFIER + "_ORIGINAL")).thenThrow(e);
        when(mediaService.getMedia(catalogVersion, QUALIFIER + "_EXTRA_SMALL")).thenThrow(e);
        when(mediaService.getMedia(catalogVersion, QUALIFIER + "_SMALL")).thenThrow(e);
        when(mediaService.getMedia(catalogVersion, QUALIFIER + "_MEDIUM")).thenThrow(e);
        when(mediaService.getMedia(catalogVersion, QUALIFIER + "_THUMBNAIL")).thenThrow(e);
        when(mediaService.getMedia(catalogVersion, QUALIFIER + "_THUMBNAIL_VANMARCKE")).thenThrow(e);
        when(mediaService.getMedia(catalogVersion, QUALIFIER + "_LARGE")).thenThrow(e);
        when(mediaService.getMedia(catalogVersion, QUALIFIER + "_EXTRA_LARGE")).thenThrow(e);
        when(mediaService.getMedia(catalogVersion, QUALIFIER + "_PREVIEW")).thenThrow(e);
        when(mediaService.getMedia(catalogVersion, QUALIFIER + "_SCREEN")).thenThrow(e);

        MediaModel original = mock(MediaModel.class);
        MediaModel extraSmall = mock(MediaModel.class);
        MediaModel small = mock(MediaModel.class);
        MediaModel medium = mock(MediaModel.class);
        MediaModel thumbnail = mock(MediaModel.class);
        MediaModel thumbnailVanMarcke = mock(MediaModel.class);
        MediaModel large = mock(MediaModel.class);
        MediaModel extraLarge = mock(MediaModel.class);
        MediaModel preview = mock(MediaModel.class);
        MediaModel screen = mock(MediaModel.class);

        when(modelService.create(MediaModel.class)).thenReturn(
                original, extraSmall, small, medium, thumbnail, thumbnailVanMarcke, large, extraLarge, preview, screen
        );

        MediaFormatModel originalFormat = mock(MediaFormatModel.class);
        MediaFormatModel extraSmallFormat = mock(MediaFormatModel.class);
        MediaFormatModel smallFormat = mock(MediaFormatModel.class);
        MediaFormatModel mediumFormat = mock(MediaFormatModel.class);
        MediaFormatModel thumbnailFormat = mock(MediaFormatModel.class);
        MediaFormatModel thumbnailVanMarckeFormat = mock(MediaFormatModel.class);
        MediaFormatModel largeFormat = mock(MediaFormatModel.class);
        MediaFormatModel extraLargeFormat = mock(MediaFormatModel.class);
        MediaFormatModel previewFormat = mock(MediaFormatModel.class);
        MediaFormatModel screenFormat = mock(MediaFormatModel.class);

        when(mediaService.getOrCreateMediaFormat("ORIGINAL")).thenReturn(originalFormat);
        when(mediaService.getOrCreateMediaFormat("EXTRA_SMALL")).thenReturn(extraSmallFormat);
        when(mediaService.getOrCreateMediaFormat("SMALL")).thenReturn(smallFormat);
        when(mediaService.getOrCreateMediaFormat("MEDIUM")).thenReturn(mediumFormat);
        when(mediaService.getOrCreateMediaFormat("THUMBNAIL")).thenReturn(thumbnailFormat);
        when(mediaService.getOrCreateMediaFormat("THUMBNAIL_VANMARCKE")).thenReturn(thumbnailVanMarckeFormat);
        when(mediaService.getOrCreateMediaFormat("LARGE")).thenReturn(largeFormat);
        when(mediaService.getOrCreateMediaFormat("EXTRA_LARGE")).thenReturn(extraLargeFormat);
        when(mediaService.getOrCreateMediaFormat("PREVIEW")).thenReturn(previewFormat);
        when(mediaService.getOrCreateMediaFormat("SCREEN")).thenReturn(screenFormat);

        when(integrationService.getPath(EXTERNAL_ID, Size.ORIGINAL, EXTENSION)).thenReturn(ORIGINAL_URL);
        when(integrationService.getPath(EXTERNAL_ID, Size.EXTRA_SMALL, EXTENSION)).thenReturn(EXTRA_SMALL_URL);
        when(integrationService.getPath(EXTERNAL_ID, Size.SMALL, EXTENSION)).thenReturn(SMALL_URL);
        when(integrationService.getPath(EXTERNAL_ID, Size.MEDIUM, EXTENSION)).thenReturn(MEDIUM_URL);
        when(integrationService.getPath(EXTERNAL_ID, Size.THUMBNAIL, EXTENSION)).thenReturn(THUMBNAIL_URL);
        when(integrationService.getPath(EXTERNAL_ID, Size.THUMBNAIL_VANMARCKE, EXTENSION)).thenReturn(THUMBNAIL_VM_URL);
        when(integrationService.getPath(EXTERNAL_ID, Size.LARGE, EXTENSION)).thenReturn(LARGE_URL);
        when(integrationService.getPath(EXTERNAL_ID, Size.EXTRA_LARGE, EXTENSION)).thenReturn(EXTRA_LARGE_URL);
        when(integrationService.getPath(EXTERNAL_ID, Size.PREVIEW, EXTENSION)).thenReturn(PREVIEW_URL);
        when(integrationService.getPath(EXTERNAL_ID, Size.SCREEN, EXTENSION)).thenReturn(SCREEN_URL);

        MediaContainerModel actualMedia = mediaContainerFactoryStrategy.create(parameter);

        Assertions
                .assertThat(actualMedia)
                .isEqualTo(container);

        verify(mediaService).getMediaContainer(catalogVersion, QUALIFIER);
        verify(modelService).create(MediaContainerModel.class);
        verify(integrationService, never()).delete(anyString());
        verify(integrationService).create(file, BASE_NAME, ResourceType.PHOTO);
        verify(mediaService).getOrCreateMediaFolder(FOLDER_QUALIFIER);
        verify(mediaService).getMedia(catalogVersion, QUALIFIER + "_ORIGINAL");
        verify(mediaService).getMedia(catalogVersion, QUALIFIER + "_EXTRA_SMALL");
        verify(mediaService).getMedia(catalogVersion, QUALIFIER + "_SMALL");
        verify(mediaService).getMedia(catalogVersion, QUALIFIER + "_MEDIUM");
        verify(mediaService).getMedia(catalogVersion, QUALIFIER + "_THUMBNAIL");
        verify(mediaService).getMedia(catalogVersion, QUALIFIER + "_THUMBNAIL_VANMARCKE");
        verify(mediaService).getMedia(catalogVersion, QUALIFIER + "_LARGE");
        verify(mediaService).getMedia(catalogVersion, QUALIFIER + "_EXTRA_LARGE");
        verify(mediaService).getMedia(catalogVersion, QUALIFIER + "_PREVIEW");
        verify(mediaService).getMedia(catalogVersion, QUALIFIER + "_SCREEN");
        verify(modelService, times(10)).create(MediaModel.class);
        verify(mediaService).getOrCreateMediaFormat("ORIGINAL");
        verify(mediaService).getOrCreateMediaFormat("EXTRA_SMALL");
        verify(mediaService).getOrCreateMediaFormat("SMALL");
        verify(mediaService).getOrCreateMediaFormat("MEDIUM");
        verify(mediaService).getOrCreateMediaFormat("THUMBNAIL");
        verify(mediaService).getOrCreateMediaFormat("THUMBNAIL_VANMARCKE");
        verify(mediaService).getOrCreateMediaFormat("LARGE");
        verify(mediaService).getOrCreateMediaFormat("EXTRA_LARGE");
        verify(mediaService).getOrCreateMediaFormat("PREVIEW");
        verify(mediaService).getOrCreateMediaFormat("SCREEN");
        verify(integrationService).getPath(EXTERNAL_ID, Size.ORIGINAL, EXTENSION);
        verify(integrationService).getPath(EXTERNAL_ID, Size.EXTRA_SMALL, EXTENSION);
        verify(integrationService).getPath(EXTERNAL_ID, Size.SMALL, EXTENSION);
        verify(integrationService).getPath(EXTERNAL_ID, Size.MEDIUM, EXTENSION);
        verify(integrationService).getPath(EXTERNAL_ID, Size.THUMBNAIL, EXTENSION);
        verify(integrationService).getPath(EXTERNAL_ID, Size.THUMBNAIL_VANMARCKE, EXTENSION);
        verify(integrationService).getPath(EXTERNAL_ID, Size.LARGE, EXTENSION);
        verify(integrationService).getPath(EXTERNAL_ID, Size.EXTRA_LARGE, EXTENSION);
        verify(integrationService).getPath(EXTERNAL_ID, Size.PREVIEW, EXTENSION);
        verify(integrationService).getPath(EXTERNAL_ID, Size.SCREEN, EXTENSION);
        verify(modelService).save(original);
        verify(modelService).save(extraSmall);
        verify(modelService).save(small);
        verify(modelService).save(medium);
        verify(modelService).save(thumbnail);
        verify(modelService).save(thumbnailVanMarcke);
        verify(modelService).save(large);
        verify(modelService).save(extraLarge);
        verify(modelService).save(preview);
        verify(modelService).save(screen);
        verify(modelService).save(container);

        verify(original).setCode(QUALIFIER + "_ORIGINAL");
        verify(original).setCatalogVersion(catalogVersion);
        verify(original).setFolder(folder);
        verify(original).setRealFileName(FILE_NAME);
        verify(original).setMediaFormat(originalFormat);
        verify(original).setURL(ORIGINAL_URL);

        verify(extraSmall).setCode(QUALIFIER + "_EXTRA_SMALL");
        verify(extraSmall).setCatalogVersion(catalogVersion);
        verify(extraSmall).setFolder(folder);
        verify(extraSmall).setRealFileName(FILE_NAME);
        verify(extraSmall).setMediaFormat(extraSmallFormat);
        verify(extraSmall).setURL(EXTRA_SMALL_URL);

        verify(small).setCode(QUALIFIER + "_SMALL");
        verify(small).setCatalogVersion(catalogVersion);
        verify(small).setFolder(folder);
        verify(small).setRealFileName(FILE_NAME);
        verify(small).setMediaFormat(smallFormat);
        verify(small).setURL(SMALL_URL);

        verify(medium).setCode(QUALIFIER + "_MEDIUM");
        verify(medium).setCatalogVersion(catalogVersion);
        verify(medium).setFolder(folder);
        verify(medium).setRealFileName(FILE_NAME);
        verify(medium).setMediaFormat(mediumFormat);
        verify(medium).setURL(MEDIUM_URL);

        verify(thumbnail).setCode(QUALIFIER + "_THUMBNAIL");
        verify(thumbnail).setCatalogVersion(catalogVersion);
        verify(thumbnail).setFolder(folder);
        verify(thumbnail).setRealFileName(FILE_NAME);
        verify(thumbnail).setMediaFormat(thumbnailFormat);
        verify(thumbnail).setURL(THUMBNAIL_URL);

        verify(thumbnailVanMarcke).setCode(QUALIFIER + "_THUMBNAIL_VANMARCKE");
        verify(thumbnailVanMarcke).setCatalogVersion(catalogVersion);
        verify(thumbnailVanMarcke).setFolder(folder);
        verify(thumbnailVanMarcke).setRealFileName(FILE_NAME);
        verify(thumbnailVanMarcke).setMediaFormat(thumbnailVanMarckeFormat);
        verify(thumbnailVanMarcke).setURL(THUMBNAIL_VM_URL);

        verify(large).setCode(QUALIFIER + "_LARGE");
        verify(large).setCatalogVersion(catalogVersion);
        verify(large).setFolder(folder);
        verify(large).setRealFileName(FILE_NAME);
        verify(large).setMediaFormat(largeFormat);
        verify(large).setURL(LARGE_URL);

        verify(extraLarge).setCode(QUALIFIER + "_EXTRA_LARGE");
        verify(extraLarge).setCatalogVersion(catalogVersion);
        verify(extraLarge).setFolder(folder);
        verify(extraLarge).setRealFileName(FILE_NAME);
        verify(extraLarge).setMediaFormat(extraLargeFormat);
        verify(extraLarge).setURL(EXTRA_LARGE_URL);

        verify(preview).setCode(QUALIFIER + "_PREVIEW");
        verify(preview).setCatalogVersion(catalogVersion);
        verify(preview).setFolder(folder);
        verify(preview).setRealFileName(FILE_NAME);
        verify(preview).setMediaFormat(previewFormat);
        verify(preview).setURL(PREVIEW_URL);

        verify(screen).setCode(QUALIFIER + "_SCREEN");
        verify(screen).setCatalogVersion(catalogVersion);
        verify(screen).setFolder(folder);
        verify(screen).setRealFileName(FILE_NAME);
        verify(screen).setMediaFormat(screenFormat);
        verify(screen).setURL(SCREEN_URL);

        verify(container).setQualifier(QUALIFIER);
        verify(container).setCatalogVersion(catalogVersion);
        verify(container).setExternalID(EXTERNAL_ID);
        verify(container).setMedias(Lists.newArrayList(original, extraSmall, small, medium, thumbnail, thumbnailVanMarcke, large, extraLarge, preview, screen));
    }

    @Test
    public void testCreateWithExistingItem() {
        CatalogVersionModel catalogVersion = new CatalogVersionModel();

        File file = mock(File.class);
        when(file.getName()).thenReturn(FILE_NAME);

        MediaFactoryParameter parameter = new MediaFactoryParameter();
        parameter.setCatalogVersion(catalogVersion);
        parameter.setQualifier(QUALIFIER);
        parameter.setFile(file);
        parameter.setType("PHOTO");
        parameter.setFolderQualifier(FOLDER_QUALIFIER);

        GregorianCalendar calendar = new GregorianCalendar(2020, Calendar.JANUARY, 1);

        MediaContainerModel container = mock(MediaContainerModel.class);
        when(container.getExternalID()).thenReturn(OLD_EXTERNAL_ID);
        when(container.getModifiedtime()).thenReturn(calendar.getTime());

        when(mediaService.getMediaContainer(catalogVersion, QUALIFIER)).thenReturn(container);

        when(integrationService.create(file, BASE_NAME, ResourceType.PHOTO)).thenReturn(EXTERNAL_ID);

        MediaFolderModel folder = new MediaFolderModel();

        when(mediaService.getOrCreateMediaFolder(FOLDER_QUALIFIER)).thenReturn(folder);

        MediaModel original = mock(MediaModel.class);
        MediaModel extraSmall = mock(MediaModel.class);
        MediaModel small = mock(MediaModel.class);
        MediaModel medium = mock(MediaModel.class);
        MediaModel thumbnail = mock(MediaModel.class);
        MediaModel thumbnailVanMarcke = mock(MediaModel.class);
        MediaModel large = mock(MediaModel.class);
        MediaModel extraLarge = mock(MediaModel.class);
        MediaModel preview = mock(MediaModel.class);
        MediaModel screen = mock(MediaModel.class);

        when(mediaService.getMedia(catalogVersion, QUALIFIER + "_ORIGINAL")).thenReturn(original);
        when(mediaService.getMedia(catalogVersion, QUALIFIER + "_EXTRA_SMALL")).thenReturn(extraSmall);
        when(mediaService.getMedia(catalogVersion, QUALIFIER + "_SMALL")).thenReturn(small);
        when(mediaService.getMedia(catalogVersion, QUALIFIER + "_MEDIUM")).thenReturn(medium);
        when(mediaService.getMedia(catalogVersion, QUALIFIER + "_THUMBNAIL")).thenReturn(thumbnail);
        when(mediaService.getMedia(catalogVersion, QUALIFIER + "_THUMBNAIL_VANMARCKE")).thenReturn(thumbnailVanMarcke);
        when(mediaService.getMedia(catalogVersion, QUALIFIER + "_LARGE")).thenReturn(large);
        when(mediaService.getMedia(catalogVersion, QUALIFIER + "_EXTRA_LARGE")).thenReturn(extraLarge);
        when(mediaService.getMedia(catalogVersion, QUALIFIER + "_PREVIEW")).thenReturn(preview);
        when(mediaService.getMedia(catalogVersion, QUALIFIER + "_SCREEN")).thenReturn(screen);

        MediaFormatModel originalFormat = mock(MediaFormatModel.class);
        MediaFormatModel extraSmallFormat = mock(MediaFormatModel.class);
        MediaFormatModel smallFormat = mock(MediaFormatModel.class);
        MediaFormatModel mediumFormat = mock(MediaFormatModel.class);
        MediaFormatModel thumbnailFormat = mock(MediaFormatModel.class);
        MediaFormatModel thumbnailVanMarckeFormat = mock(MediaFormatModel.class);
        MediaFormatModel largeFormat = mock(MediaFormatModel.class);
        MediaFormatModel extraLargeFormat = mock(MediaFormatModel.class);
        MediaFormatModel previewFormat = mock(MediaFormatModel.class);
        MediaFormatModel screenFormat = mock(MediaFormatModel.class);

        when(mediaService.getOrCreateMediaFormat("ORIGINAL")).thenReturn(originalFormat);
        when(mediaService.getOrCreateMediaFormat("EXTRA_SMALL")).thenReturn(extraSmallFormat);
        when(mediaService.getOrCreateMediaFormat("SMALL")).thenReturn(smallFormat);
        when(mediaService.getOrCreateMediaFormat("MEDIUM")).thenReturn(mediumFormat);
        when(mediaService.getOrCreateMediaFormat("THUMBNAIL")).thenReturn(thumbnailFormat);
        when(mediaService.getOrCreateMediaFormat("THUMBNAIL_VANMARCKE")).thenReturn(thumbnailVanMarckeFormat);
        when(mediaService.getOrCreateMediaFormat("LARGE")).thenReturn(largeFormat);
        when(mediaService.getOrCreateMediaFormat("EXTRA_LARGE")).thenReturn(extraLargeFormat);
        when(mediaService.getOrCreateMediaFormat("PREVIEW")).thenReturn(previewFormat);
        when(mediaService.getOrCreateMediaFormat("SCREEN")).thenReturn(screenFormat);

        when(integrationService.getPath(EXTERNAL_ID, Size.ORIGINAL, EXTENSION)).thenReturn(ORIGINAL_URL);
        when(integrationService.getPath(EXTERNAL_ID, Size.EXTRA_SMALL, EXTENSION)).thenReturn(EXTRA_SMALL_URL);
        when(integrationService.getPath(EXTERNAL_ID, Size.SMALL, EXTENSION)).thenReturn(SMALL_URL);
        when(integrationService.getPath(EXTERNAL_ID, Size.MEDIUM, EXTENSION)).thenReturn(MEDIUM_URL);
        when(integrationService.getPath(EXTERNAL_ID, Size.THUMBNAIL, EXTENSION)).thenReturn(THUMBNAIL_URL);
        when(integrationService.getPath(EXTERNAL_ID, Size.THUMBNAIL_VANMARCKE, EXTENSION)).thenReturn(THUMBNAIL_VM_URL);
        when(integrationService.getPath(EXTERNAL_ID, Size.LARGE, EXTENSION)).thenReturn(LARGE_URL);
        when(integrationService.getPath(EXTERNAL_ID, Size.EXTRA_LARGE, EXTENSION)).thenReturn(EXTRA_LARGE_URL);
        when(integrationService.getPath(EXTERNAL_ID, Size.PREVIEW, EXTENSION)).thenReturn(PREVIEW_URL);
        when(integrationService.getPath(EXTERNAL_ID, Size.SCREEN, EXTENSION)).thenReturn(SCREEN_URL);

        MediaContainerModel actualMedia = mediaContainerFactoryStrategy.create(parameter);

        Assertions
                .assertThat(actualMedia)
                .isEqualTo(container);

        verify(mediaService).getMediaContainer(catalogVersion, QUALIFIER);
        verify(modelService, never()).create(MediaContainerModel.class);
        verify(integrationService).delete(OLD_EXTERNAL_ID);
        verify(integrationService).create(file, BASE_NAME, ResourceType.PHOTO);
        verify(mediaService).getOrCreateMediaFolder(FOLDER_QUALIFIER);
        verify(mediaService).getMedia(catalogVersion, QUALIFIER + "_ORIGINAL");
        verify(mediaService).getMedia(catalogVersion, QUALIFIER + "_EXTRA_SMALL");
        verify(mediaService).getMedia(catalogVersion, QUALIFIER + "_SMALL");
        verify(mediaService).getMedia(catalogVersion, QUALIFIER + "_MEDIUM");
        verify(mediaService).getMedia(catalogVersion, QUALIFIER + "_THUMBNAIL");
        verify(mediaService).getMedia(catalogVersion, QUALIFIER + "_THUMBNAIL_VANMARCKE");
        verify(mediaService).getMedia(catalogVersion, QUALIFIER + "_LARGE");
        verify(mediaService).getMedia(catalogVersion, QUALIFIER + "_EXTRA_LARGE");
        verify(mediaService).getMedia(catalogVersion, QUALIFIER + "_PREVIEW");
        verify(mediaService).getMedia(catalogVersion, QUALIFIER + "_SCREEN");
        verify(modelService, never()).create(MediaModel.class);
        verify(mediaService).getOrCreateMediaFormat("ORIGINAL");
        verify(mediaService).getOrCreateMediaFormat("EXTRA_SMALL");
        verify(mediaService).getOrCreateMediaFormat("SMALL");
        verify(mediaService).getOrCreateMediaFormat("MEDIUM");
        verify(mediaService).getOrCreateMediaFormat("THUMBNAIL");
        verify(mediaService).getOrCreateMediaFormat("THUMBNAIL_VANMARCKE");
        verify(mediaService).getOrCreateMediaFormat("LARGE");
        verify(mediaService).getOrCreateMediaFormat("EXTRA_LARGE");
        verify(mediaService).getOrCreateMediaFormat("PREVIEW");
        verify(mediaService).getOrCreateMediaFormat("SCREEN");
        verify(integrationService).getPath(EXTERNAL_ID, Size.ORIGINAL, EXTENSION);
        verify(integrationService).getPath(EXTERNAL_ID, Size.EXTRA_SMALL, EXTENSION);
        verify(integrationService).getPath(EXTERNAL_ID, Size.SMALL, EXTENSION);
        verify(integrationService).getPath(EXTERNAL_ID, Size.MEDIUM, EXTENSION);
        verify(integrationService).getPath(EXTERNAL_ID, Size.THUMBNAIL, EXTENSION);
        verify(integrationService).getPath(EXTERNAL_ID, Size.THUMBNAIL_VANMARCKE, EXTENSION);
        verify(integrationService).getPath(EXTERNAL_ID, Size.LARGE, EXTENSION);
        verify(integrationService).getPath(EXTERNAL_ID, Size.EXTRA_LARGE, EXTENSION);
        verify(integrationService).getPath(EXTERNAL_ID, Size.PREVIEW, EXTENSION);
        verify(integrationService).getPath(EXTERNAL_ID, Size.SCREEN, EXTENSION);
        verify(modelService).save(original);
        verify(modelService).save(extraSmall);
        verify(modelService).save(small);
        verify(modelService).save(medium);
        verify(modelService).save(thumbnail);
        verify(modelService).save(thumbnailVanMarcke);
        verify(modelService).save(large);
        verify(modelService).save(extraLarge);
        verify(modelService).save(preview);
        verify(modelService).save(screen);
        verify(modelService).save(container);

        verify(original).setFolder(folder);
        verify(original).setRealFileName(FILE_NAME);
        verify(original).setMediaFormat(originalFormat);
        verify(original).setURL(ORIGINAL_URL);

        verify(extraSmall).setFolder(folder);
        verify(extraSmall).setRealFileName(FILE_NAME);
        verify(extraSmall).setMediaFormat(extraSmallFormat);
        verify(extraSmall).setURL(EXTRA_SMALL_URL);

        verify(small).setFolder(folder);
        verify(small).setRealFileName(FILE_NAME);
        verify(small).setMediaFormat(smallFormat);
        verify(small).setURL(SMALL_URL);

        verify(medium).setFolder(folder);
        verify(medium).setRealFileName(FILE_NAME);
        verify(medium).setMediaFormat(mediumFormat);
        verify(medium).setURL(MEDIUM_URL);

        verify(thumbnail).setFolder(folder);
        verify(thumbnail).setRealFileName(FILE_NAME);
        verify(thumbnail).setMediaFormat(thumbnailFormat);
        verify(thumbnail).setURL(THUMBNAIL_URL);

        verify(thumbnailVanMarcke).setFolder(folder);
        verify(thumbnailVanMarcke).setRealFileName(FILE_NAME);
        verify(thumbnailVanMarcke).setMediaFormat(thumbnailVanMarckeFormat);
        verify(thumbnailVanMarcke).setURL(THUMBNAIL_VM_URL);

        verify(large).setFolder(folder);
        verify(large).setRealFileName(FILE_NAME);
        verify(large).setMediaFormat(largeFormat);
        verify(large).setURL(LARGE_URL);

        verify(extraLarge).setFolder(folder);
        verify(extraLarge).setRealFileName(FILE_NAME);
        verify(extraLarge).setMediaFormat(extraLargeFormat);
        verify(extraLarge).setURL(EXTRA_LARGE_URL);

        verify(preview).setFolder(folder);
        verify(preview).setRealFileName(FILE_NAME);
        verify(preview).setMediaFormat(previewFormat);
        verify(preview).setURL(PREVIEW_URL);

        verify(screen).setFolder(folder);
        verify(screen).setRealFileName(FILE_NAME);
        verify(screen).setMediaFormat(screenFormat);
        verify(screen).setURL(SCREEN_URL);

        verify(container).setExternalID(EXTERNAL_ID);
        verify(container).setMedias(Lists.newArrayList(original, extraSmall, small, medium, thumbnail, thumbnailVanMarcke, large, extraLarge, preview, screen));
    }
}