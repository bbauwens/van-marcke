package com.vanmarcke.hotfolder.services.dataimport.batch.account.impl;

import de.hybris.platform.acceleratorservices.dataimport.batch.converter.ImpexRowFilter;

import java.util.Map;

/**
 * Filter to filter out addresses which are <b>not</b> marked as billing addresses.
 */
public class DefaultVMKAccountImpexRowFilter implements ImpexRowFilter {

    private static final String BILLING_FLAG = "Y";
    private static final int BILLING_FLAG_INDEX = 3;


    @Override
    public boolean filter(final Map<Integer, String> row) {
        return BILLING_FLAG.equalsIgnoreCase(row.get(BILLING_FLAG_INDEX));
    }
}