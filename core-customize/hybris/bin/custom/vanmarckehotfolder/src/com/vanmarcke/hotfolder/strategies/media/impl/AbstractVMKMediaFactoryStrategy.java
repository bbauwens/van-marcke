package com.vanmarcke.hotfolder.strategies.media.impl;

import com.vanmarcke.hotfolder.resourcespace.service.VMKResourceSpaceService;
import com.vanmarcke.hotfolder.services.VMKMediaService;
import com.vanmarcke.hotfolder.strategies.media.VMKMediaFactoryStrategy;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import org.apache.commons.lang3.StringUtils;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

/**
 * The {@code AbstractVMKMediaFactoryStrategy} class contains the common business logic for its children.
 *
 * @param <T> the item type
 * @author Christiaan Janssen
 * @since 16-04-2020
 */
public abstract class AbstractVMKMediaFactoryStrategy<T> implements VMKMediaFactoryStrategy<T> {

    private static final int UPDATE_THRESHOLD = 3;

    protected final ModelService modelService;
    protected final VMKResourceSpaceService integrationService;
    protected final VMKMediaService mediaService;

    /**
     * Serves as a starting point for the children of the {@link AbstractVMKMediaFactoryStrategy} class.
     *
     * @param modelService       the model service
     * @param integrationService the integration service
     * @param mediaService       the media service
     */
    public AbstractVMKMediaFactoryStrategy(ModelService modelService,
                                           VMKResourceSpaceService integrationService,
                                           VMKMediaService mediaService) {
        this.modelService = modelService;
        this.integrationService = integrationService;
        this.mediaService = mediaService;
    }

    /**
     * Gets or creates a {@link MediaModel} instance for the given {@code catalogVersion} and {@code code}.
     *
     * @param catalogVersion the catalog version
     * @param code           the code
     * @return the {@link MediaModel} instance
     */
    protected MediaModel getOrCreateMedia(CatalogVersionModel catalogVersion, String code) {
        MediaModel media;
        try {
            media = mediaService.getMedia(catalogVersion, code);
        } catch (UnknownIdentifierException e) {
            media = modelService.create(MediaModel.class);
            media.setCode(code);
            media.setCatalogVersion(catalogVersion);
        }
        return media;
    }

    /**
     * Checks whether the given {@code media} has been created/updated recently.
     *
     * @param id               the unique identifier of the resource
     * @param lastModifiedDate the last modified date
     * @return {@code true} in case the given {@code media} has been created/updated recently, {@code false} otherwise
     */
    protected boolean hasBeenUpdatedRecently(String id, Date lastModifiedDate) {
        if (StringUtils.isEmpty(id) || lastModifiedDate == null) {
            return false;
        }

        LocalDateTime threshold = LocalDateTime.now();
        threshold = threshold.minusMinutes(UPDATE_THRESHOLD);

        LocalDateTime modifiedDate = LocalDateTime.ofInstant(lastModifiedDate.toInstant(), ZoneId.systemDefault());

        return threshold.isBefore(modifiedDate);
    }

    /**
     * Removes the resource with the given {@code id}.
     *
     * @param id the ID
     */
    protected void deleteResource(String id) {
        if (StringUtils.isNotEmpty(id)) {
            integrationService.delete(id);
        }
    }
}
