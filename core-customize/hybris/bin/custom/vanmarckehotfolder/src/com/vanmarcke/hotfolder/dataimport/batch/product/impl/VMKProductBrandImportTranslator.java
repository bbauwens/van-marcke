package com.vanmarcke.hotfolder.dataimport.batch.product.impl;

import com.vanmarcke.hotfolder.dataimport.batch.product.VMKProductBrandImportAdapter;
import de.hybris.platform.core.Registry;
import de.hybris.platform.impex.jalo.header.SpecialColumnDescriptor;
import de.hybris.platform.impex.jalo.translators.AbstractSpecialValueTranslator;
import de.hybris.platform.jalo.Item;

/**
 * Translator for updating the brand for a product.
 */
public class VMKProductBrandImportTranslator extends AbstractSpecialValueTranslator {

    private static final String DEFAULT_IMPORT_ADAPTER_NAME = "vmkProductBrandImportAdapter";

    private VMKProductBrandImportAdapter adapter;

    @Override
    public void init(final SpecialColumnDescriptor columnDescriptor) {
        this.adapter = (VMKProductBrandImportAdapter) Registry.getApplicationContext().getBean(DEFAULT_IMPORT_ADAPTER_NAME);
    }

    @Override
    public void performImport(final String cellValue, final Item processedItem) {
        this.adapter.performImport(cellValue, processedItem);
    }
}