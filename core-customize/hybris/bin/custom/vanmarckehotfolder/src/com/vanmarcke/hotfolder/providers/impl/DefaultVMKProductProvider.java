package com.vanmarcke.hotfolder.providers.impl;

import com.vanmarcke.hotfolder.providers.VMKItemProvider;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.variants.model.VariantProductModel;
import org.apache.log4j.Logger;

import java.util.List;
import java.util.Objects;
import java.util.Set;

import static com.google.common.base.Preconditions.checkState;
import static java.util.stream.Collectors.toSet;
import static org.apache.commons.collections4.IterableUtils.countMatches;

/**
 * This class implements the {@link VMKItemProvider} interface. It implements methods to provide products.
 *
 * @author Taki Korovessis, Tom van den Berg
 * @since 27/10/2019
 */
public class DefaultVMKProductProvider implements VMKItemProvider<ProductModel> {

    private static final Logger LOGGER = Logger.getLogger(DefaultVMKProductProvider.class);

    private final ProductService productService;

    /**
     * Provides an instance of the {@code DefaultVMKProductProvider}.
     *
     * @param productService the product service
     */
    public DefaultVMKProductProvider(ProductService productService) {
        this.productService = productService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Set<ProductModel> get(CatalogVersionModel catalogVersion, List<String> codes) {
        Set<ProductModel> products = codes.stream()
                .map(code -> retrieveProductForCode(catalogVersion, code))
                .filter(Objects::nonNull)
                .collect(toSet());

        long variantCount = countMatches(products, VariantProductModel.class::isInstance);
        checkState(variantCount == products.size() || variantCount == 0, "Base products and Variant products cannot be used interchangeably!");

        return products;
    }

    /**
     * Retrieves a product for the given code. If the product is not found, null is returned.
     *
     * @param catalogVersion the catalog version
     * @param code           the product code
     * @return the product
     */
    private ProductModel retrieveProductForCode(CatalogVersionModel catalogVersion, String code) {
        try {
            return productService.getProductForCode(catalogVersion, code);
        } catch (UnknownIdentifierException e) {
            LOGGER.error(e.getMessage());
        }
        return null;
    }
}