package com.vanmarcke.hotfolder.services.dataimport.batch.account;

import de.hybris.platform.jalo.Item;

/**
 * Adapter to translate an account ASM employee import row into a service call.
 */
public interface VMKAccountImportASMEmployeeAdapter {

    /**
     * Import an account ASM employee.
     *
     * @param cellValue the cell value
     * @param b2bUnit   the b2bUnit
     * @throws IllegalArgumentException if the cellValue is empty, null or invalid or the address is null
     */
    void performImport(String cellValue, Item b2bUnit);
}
