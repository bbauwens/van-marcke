package com.vanmarcke.hotfolder.strategies;

import com.vanmarcke.hotfolder.data.MediaAttributeType;
import com.vanmarcke.hotfolder.data.MediaData;
import com.vanmarcke.hotfolder.data.builder.MediaFactoryParameterBuilder;
import com.vanmarcke.hotfolder.providers.VMKItemProvider;
import com.vanmarcke.hotfolder.resourcespace.enums.MetaDataField;
import com.vanmarcke.hotfolder.resourcespace.enums.ResourceType;
import com.vanmarcke.hotfolder.resourcespace.service.VMKResourceSpaceService;
import com.vanmarcke.hotfolder.strategies.media.VMKMediaFactoryStrategy;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.exceptions.SystemException;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import java.io.File;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.vanmarcke.hotfolder.utils.VMKMediaImportUtils.getMediaPrefix;
import static java.lang.String.format;

/**
 * The {@code AbstractVMKMediaImportStrategy} class provides the common business logic to import media.
 *
 * @param <T> the item type for which media is being imported
 * @author Taki Korovessis, Christiaan Janssen, Tom van den Berg
 * @since 01-10-2019
 */
public abstract class AbstractVMKMediaImportStrategy<T extends ItemModel> implements VMKMediaImportStrategy<T> {

    private static final Pattern PATTERN = Pattern.compile("^([a-zA-Z0-9-_]+)(\\$\\d)$");
    private static final String FILE_FORMAT = "%s-%s";
    private static final String DEFAULT_CATALOG_VERSION = "Staged";

    protected ModelService modelService;
    protected VMKResourceSpaceService integrationService;
    protected VMKItemProvider<T> itemProvider;
    protected I18NService i18NService;
    protected SessionService sessionService;
    protected UserService userService;
    protected CatalogVersionService catalogVersionService;
    protected VMKMediaFactoryStrategy<MediaModel> mediaFactoryStrategy;
    protected VMKMediaFactoryStrategy<MediaContainerModel> mediaContainerFactoryStrategy;
    protected String catalog;

    protected ResourceType resourceType;
    protected String folder;
    protected String mediaType;
    protected String attribute;
    protected MediaAttributeType mediaAttributeType;


    /**
     * {@inheritDoc}
     */
    @Override
    public final void execute(MediaData mediaDTO) {
        if (mediaDTO == null || CollectionUtils.isEmpty(mediaDTO.getFiles())) {
            return;
        }

        sessionService.executeInLocalView(new SessionExecutionBody() {
            @Override
            public void executeWithoutResult() {
                CatalogVersionModel catalogVersion = catalogVersionService.getCatalogVersion(catalog, DEFAULT_CATALOG_VERSION);
                switch (resourceType) {
                    case DOCUMENT:
                        importMedia(catalogVersion, mediaDTO, ResourceType.DOCUMENT);
                        break;
                    case PHOTO:
                        importMediaContainer(catalogVersion, mediaDTO, ResourceType.PHOTO);
                        break;
                    case VIDEO:
                        importMedia(catalogVersion, mediaDTO, ResourceType.VIDEO);
                        break;
                    default:
                        throw new SystemException(format("Resource type '%s' is not supported.", resourceType));
                }
            }
        }, userService.getAdminUser());
    }

    /**
     * Imports the given {@code mediaDTO} for the given {@code catalogVersion}.
     *
     * @param catalogVersion the catalog version
     * @param mediaDTO       the media to import
     */
    protected void importMedia(CatalogVersionModel catalogVersion, MediaData mediaDTO, ResourceType resourceType) {
        Set<T> items = getItems(catalogVersion, mediaDTO);

        if (CollectionUtils.isEmpty(items)) {
            return;
        }

        File file = mediaDTO.getFiles().get(0);

        MediaModel mediaModel = mediaFactoryStrategy.create(MediaFactoryParameterBuilder
                .aMediaFactoryParameter(
                        format(FILE_FORMAT, getMediaPrefix(items), extractQualifier(file.getName())),
                        folder,
                        resourceType,
                        file,
                        catalogVersion,
                        mediaDTO.getLocalizedNames()
                )
                .build());

        Set<Locale> locales = mediaDTO.getLocalizedNames().keySet();

        updateMetaData(mediaModel.getExternalID(), locales, items);
        removeMediaModel(mediaModel, locales, items);
        updateMediaModel(mediaModel, locales, items);
    }


    /**
     * Imports the given {@code mediaDTO} for the given {@code catalogVersion}.
     *
     * @param catalogVersion the catalog version
     * @param mediaDTO       the media to import
     */
    protected void importMediaContainer(CatalogVersionModel catalogVersion, MediaData mediaDTO, ResourceType resourceType) {
        Set<T> items = getItems(catalogVersion, mediaDTO);

        if (CollectionUtils.isEmpty(items)) {
            return;
        }

        List<MediaContainerModel> mediaContainers = new ArrayList<>();

        List<File> files = mediaDTO.getFiles();
        Set<Locale> locales = mediaDTO.getLocalizedNames().keySet();

        for (File file : files) {
            MediaContainerModel mediaContainer = mediaContainerFactoryStrategy.create(MediaFactoryParameterBuilder
                    .aMediaFactoryParameter(
                            format(FILE_FORMAT, getMediaPrefix(items), extractQualifier(file.getName())),
                            folder,
                            resourceType,
                            file,
                            catalogVersion,
                            mediaDTO.getLocalizedNames()
                    )
                    .build());

            mediaContainers.add(mediaContainer);

            updateMetaData(mediaContainer.getExternalID(), locales, items);
        }
        removeMediaContainers(mediaContainers, locales, items);
        updateMediaContainers(mediaContainers, locales, items);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateMetaData(String externalID, Set<Locale> locales, Set<T> items) {
        if (ResourceType.DOCUMENT == resourceType) {
            integrationService.updateField(externalID, MetaDataField.DOCUMENT_TYPE, mediaType);
        } else if (ResourceType.PHOTO == resourceType) {
            integrationService.updateField(externalID, MetaDataField.PHOTO_TYPE, mediaType);
        }
    }

    /**
     * Updates the given items with the media, for the given locales.
     *
     * @param mediaModel the media
     * @param locales    the locales
     * @param items      the items
     */
    protected void updateMediaModel(MediaModel mediaModel, Set<Locale> locales, Set<T> items) {
        items.forEach(item -> {
            locales.forEach(locale -> {
                update(mediaModel, locale, item, attribute, mediaAttributeType);
            });
            modelService.save(item);
        });
    }

    /**
     * Retrieves the deprecated locales and deletes the related media.
     *
     * @param mediaModel      the media
     * @param requiredLocales the required locales
     * @param items           the items
     */
    protected void removeMediaModel(MediaModel mediaModel, Set<Locale> requiredLocales, Set<T> items) {
        if (requiredLocales.size() > 1) {
            Set<Locale> deprecatedLocales = getDeprecatedLocales(requiredLocales);
            removeDeprecatedMediaModel(mediaModel, deprecatedLocales, items);
        }
    }

    /**
     * Updates media containers for the given items.
     *
     * @param mediaContainers the media containers
     * @param locales         the locales
     * @param items           the items
     */
    protected void updateMediaContainers(List<MediaContainerModel> mediaContainers, Set<Locale> locales, Set<T> items) {
        items.forEach(item -> {
            mediaContainers.forEach(newMediaContainer -> {
                if (MediaAttributeType.LOCALIZED_MEDIA_CONTAINER.equals(mediaAttributeType)) {
                    locales.forEach(locale -> {
                        update(newMediaContainer, locale, item, attribute, mediaAttributeType);
                    });
                } else {
                    update(newMediaContainer, item, attribute, mediaAttributeType);
                }
            });
            modelService.save(item);
        });
    }

    /**
     * Removes deprecated media containers from the given items.
     * <p>
     * If the media containers are localized, it takes into account the given locales.
     *
     * @param mediaContainers the media containers
     * @param locales         the locales for which the item does not have to be removed
     * @param items           the items
     */
    protected void removeMediaContainers(List<MediaContainerModel> mediaContainers, Set<Locale> locales, Set<T> items) {
        if (locales.size() > 1) {
            items.forEach(item -> {
                mediaContainers.forEach(mediaContainer -> {
                    if (MediaAttributeType.LOCALIZED_MEDIA_CONTAINER.equals(mediaAttributeType)) {
                        Set<Locale> deprecatedLocales = getDeprecatedLocales(locales);
                        deprecatedLocales.forEach(locale -> {
                            remove(mediaContainer, locale, item, attribute, mediaAttributeType);
                        });
                    } else {
                        remove(mediaContainer, item, attribute, mediaAttributeType);
                    }
                });
                modelService.save(item);
            });
        }
    }

    /**
     * Removes the media for the given locales
     *
     * @param mediaModel        the media
     * @param deprecatedLocales the deprecated locales
     * @param items             the items
     */
    protected void removeDeprecatedMediaModel(MediaModel mediaModel, Set<Locale> deprecatedLocales, Set<T> items) {
        items.forEach(item -> {
            deprecatedLocales.forEach(locale -> {
                remove(mediaModel, locale, item, attribute, mediaAttributeType);
            });
            modelService.save(item);
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void update(MediaModel media, Locale locale, T item, String attribute, MediaAttributeType mediaAttributeType) {
        if (MediaAttributeType.SINGLE_VALUE.equals(mediaAttributeType)) {
            updateSingleMedia(media, locale, item, attribute);
        }

        if (MediaAttributeType.LIST.equals(mediaAttributeType)) {
            updateMediaCollection(media, locale, item, attribute);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void update(MediaContainerModel newContainer, T item, String attribute, MediaAttributeType mediaAttributeType) {
        List<MediaContainerModel> containers = modelService.getAttributeValue(item, attribute);
        modelService.setAttributeValue(item, attribute, updateMediaContainer(newContainer, containers));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void update(MediaContainerModel newContainer, Locale locale, T item, String attribute, MediaAttributeType mediaAttributeType) {
        List<MediaContainerModel> containers = modelService.getAttributeValue(item, attribute, locale);
        Map<Locale, List<MediaContainerModel>> map = Collections.singletonMap(locale, updateMediaContainer(newContainer, containers));
        modelService.setAttributeValue(item, attribute, map);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void remove(MediaModel media, Locale locale, T item, String attribute, MediaAttributeType mediaAttributeType) {
        if (MediaAttributeType.SINGLE_VALUE.equals(mediaAttributeType)) {
            removeSingleMedia(media, locale, item, attribute);
        }

        if (MediaAttributeType.LIST.equals(mediaAttributeType)) {
            removeMediaFromList(media, locale, item, attribute);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void remove(MediaContainerModel mediaContainer, T item, String attribute, MediaAttributeType mediaAttributeType) {
        List<MediaContainerModel> containers = modelService.getAttributeValue(item, attribute);
        List<MediaContainerModel> existingContainers = new ArrayList<>(containers);

        for (int i = 0; i < existingContainers.size(); i++) {
            MediaContainerModel existingContainer = existingContainers.get(i);
            if (isSameContainer(mediaContainer, existingContainer)) {
                existingContainers.remove(existingContainer);
            }
        }

        modelService.setAttributeValue(item, attribute, existingContainers);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void remove(MediaContainerModel mediaContainer, Locale locale, T item, String attribute, MediaAttributeType mediaAttributeType) {
        List<MediaContainerModel> containers = modelService.getAttributeValue(item, attribute, locale);
        List<MediaContainerModel> existingContainers = new ArrayList<>(containers);

        for (int i = 0; i < existingContainers.size(); i++) {
            MediaContainerModel existingContainer = existingContainers.get(i);
            if (isSameContainer(mediaContainer, existingContainer)) {
                existingContainers.remove(existingContainer);
            }
        }

        Map<Locale, List<MediaContainerModel>> map = Collections.singletonMap(locale, existingContainers);
        modelService.setAttributeValue(item, attribute, map);
    }

    /**
     * Adds the given media model to the relevant product, based on the locale and item attribute.
     *
     * @param newMedia  the new media
     * @param locale    the locale
     * @param item      the item
     * @param attribute the product attribute
     */
    protected void updateSingleMedia(MediaModel newMedia, Locale locale, T item, String attribute) {
        Map<Locale, MediaModel> map = Collections.singletonMap(locale, newMedia);
        modelService.setAttributeValue(item, attribute, map);
    }

    /**
     * Adds the given media model to the relevant media collection, based on the locale and item attribute.
     *
     * @param newMedia  the new media
     * @param locale    the locale
     * @param item      the item
     * @param attribute the product attribute
     */
    protected void updateMediaCollection(MediaModel newMedia, Locale locale, T item, String attribute) {
        Collection<MediaModel> medias = modelService.getAttributeValue(item, attribute, locale);
        Map<Locale, Collection<MediaModel>> map = Collections.singletonMap(locale, updateMediaModels(newMedia, medias));
        modelService.setAttributeValue(item, attribute, map);
    }

    /**
     * Removes a media model from a item based on the given locale and product attribute.
     *
     * @param locale    the locale
     * @param item      the item
     * @param attribute the product attribute
     */
    protected void removeSingleMedia(MediaModel newMedia, Locale locale, T item, String attribute) {
        MediaModel existingMedia = modelService.getAttributeValue(item, attribute, locale);
        if (existingMedia != null && isSameMediaModel(newMedia, existingMedia)) {
            modelService.setAttributeValue(item, attribute, Collections.singletonMap(locale, null));
        }
    }

    /**
     * Retrieves a media collection from item, based on the attribute and locale
     * and removes the given media from the collection if it is present.
     *
     * @param media     the new media
     * @param locale    the locale
     * @param item      the item
     * @param attribute the product attribute
     */
    protected void removeMediaFromList(MediaModel media, Locale locale, T item, String attribute) {
        Collection<MediaModel> medias = modelService.getAttributeValue(item, attribute, locale);

        List<MediaModel> list = new ArrayList<>(medias);

        for (int i = 0; i < list.size(); i++) {
            if (isSameMediaModel(media, list.get(i))) {
                list.remove(list.get(i));
            }
        }
        Map<Locale, Collection<MediaModel>> map = Collections.singletonMap(locale, list);
        modelService.setAttributeValue(item, attribute, map);
    }

    /**
     * Extracts the qualifier from the given {@code fileName}.
     *
     * @param fileName the file name
     * @return the qualifier
     */
    protected String extractQualifier(String fileName) {
        return fileName.substring(0, fileName.lastIndexOf('.'));
    }


    /**
     * Updates an existing collection of {@link MediaModel} instances with the given {@code newMedia}.
     * <p>
     * This method will either add or replace an existing {@link MediaModel} instance with the given
     * {@code newMedia}. This is based on the sequence ID of the {@code newMedia}.
     *
     * @param newMedia       the new media
     * @param existingMedias the existing medias
     * @return the updated documents
     */
    public List<MediaModel> updateMediaModels(MediaModel newMedia, Collection<MediaModel> existingMedias) {
        List<MediaModel> documents = new ArrayList<>(existingMedias);
        handleMediaModels(documents, newMedia);
        return documents;
    }

    /**
     * Updates an existing collection of {@link MediaContainerModel} instances with the given {@code newImage}.
     * <p>
     * This method will either add or replace an existing {@link MediaModel} instance with the given
     * {@code newImage}. This is based on the sequence ID of the {@code newImage}.
     *
     * @param newImage       the new image
     * @param existingImages the existing images
     * @return the updated documents
     */
    public List<MediaContainerModel> updateMediaContainer(MediaContainerModel newImage, Collection<MediaContainerModel> existingImages) {
        List<MediaContainerModel> images = new ArrayList<>(existingImages);
        handleMediaContainers(images, newImage);
        return images;
    }

    /**
     * Adds the given {@code newDocument} to or replaces it in the given {@code documents}.
     *
     * @param documents   the existing documents
     * @param newDocument the new document
     */
    protected void handleMediaModels(List<MediaModel> documents, MediaModel newDocument) {
        for (int i = 0; i < documents.size(); i++) {
            MediaModel document = documents.get(i);
            if (document.getCode().equals(newDocument.getCode())) {
                documents.set(i, newDocument);
                return;
            }
        }
        documents.add(newDocument);
        documents.sort(Comparator.comparingInt(this::getSequenceId));
    }

    /**
     * Adds the given {@code newMedia} to or replaces it in the given {@code medias}.
     *
     * @param medias   the existing medias
     * @param newMedia the new media
     */
    protected void handleMediaContainers(List<MediaContainerModel> medias, MediaContainerModel newMedia) {
        for (int i = 0; i < medias.size(); i++) {
            MediaContainerModel media = medias.get(i);
            if (media.getQualifier().equals(newMedia.getQualifier())) {
                medias.set(i, newMedia);
                return;
            }
        }
        medias.add(newMedia);
        medias.sort(Comparator.comparing(this::getSequenceId));
    }

    /**
     * Returns the sequence ID for the given {@code document}.
     * <p>
     * The sequence ID is the suffix of the document after the $ sign.
     *
     * @param document the document
     * @return the sequence ID
     */
    protected int getSequenceId(MediaModel document) {
        Matcher matcher = PATTERN.matcher(document.getCode());
        if (matcher.find()) {
            return Integer.parseInt(matcher.group(2).substring(1));
        }
        return 0;
    }

    /**
     * Returns the sequence ID for the given {@code media}.
     * <p>
     * The sequence ID is the suffix of the media file after the $ sign.
     *
     * @param media the media file
     * @return the sequence ID
     */
    protected int getSequenceId(MediaContainerModel media) {
        Matcher matcher = PATTERN.matcher(media.getQualifier());
        if (matcher.find()) {
            return Integer.parseInt(matcher.group(2).substring(1));
        }
        return 0;
    }

    /**
     * Returns all supported locales which are not in the given set of required locales.
     *
     * @param requiredLocales the required locales
     * @return set of locales
     */
    protected Set<Locale> getDeprecatedLocales(Set<Locale> requiredLocales) {
        Set<Locale> supportedLocales = new HashSet<>(i18NService.getSupportedLocales());
        supportedLocales.removeIf(requiredLocales::contains);
        return supportedLocales;
    }

    /**
     * Determines whether the given media models are equal.
     *
     * @param newMedia      the new media
     * @param existingMedia the existing media
     * @return true if equal
     */
    protected boolean isSameMediaModel(MediaModel newMedia, MediaModel existingMedia) {
        return existingMedia.getCode().equals(newMedia.getCode())
                && existingMedia.getCatalogVersion().equals(newMedia.getCatalogVersion());
    }

    /**
     * Determines whether the given media containers  are equal.
     *
     * @param newMediaContainer      the new container
     * @param existingMediaContainer the existing container
     * @return true if equal
     */
    protected boolean isSameContainer(MediaContainerModel newMediaContainer, MediaContainerModel existingMediaContainer) {
        return existingMediaContainer.getQualifier().equals(newMediaContainer.getQualifier())
                && existingMediaContainer.getCatalogVersion().equals(newMediaContainer.getCatalogVersion());
    }


    public static Pattern getPATTERN() {
        return PATTERN;
    }

    public static String getFileFormat() {
        return FILE_FORMAT;
    }

    public static String getDefaultCatalogVersion() {
        return DEFAULT_CATALOG_VERSION;
    }

    public ModelService getModelService() {
        return modelService;
    }

    public VMKResourceSpaceService getIntegrationService() {
        return integrationService;
    }

    public VMKItemProvider<T> getItemProvider() {
        return itemProvider;
    }

    public ResourceType getResourceType() {
        return resourceType;
    }

    public I18NService getI18NService() {
        return i18NService;
    }

    public SessionService getSessionService() {
        return sessionService;
    }

    public UserService getUserService() {
        return userService;
    }

    public CatalogVersionService getCatalogVersionService() {
        return catalogVersionService;
    }

    public VMKMediaFactoryStrategy<MediaModel> getMediaFactoryStrategy() {
        return mediaFactoryStrategy;
    }

    public VMKMediaFactoryStrategy<MediaContainerModel> getMediaContainerFactoryStrategy() {
        return mediaContainerFactoryStrategy;
    }

    public String getCatalog() {
        return catalog;
    }

    public String getFolder() {
        return folder;
    }

    public String getMediaType() {
        return mediaType;
    }

    public String getAttribute() {
        return attribute;
    }

    public MediaAttributeType getMediaAttributeType() {
        return mediaAttributeType;
    }

    @Required
    public void setModelService(ModelService modelService) {
        this.modelService = modelService;
    }

    @Required
    public void setIntegrationService(VMKResourceSpaceService integrationService) {
        this.integrationService = integrationService;
    }

    @Required
    public void setItemProvider(VMKItemProvider<T> itemProvider) {
        this.itemProvider = itemProvider;
    }

    @Required
    public void setResourceType(ResourceType resourceType) {
        this.resourceType = resourceType;
    }

    @Required
    public void setI18NService(I18NService i18NService) {
        this.i18NService = i18NService;
    }

    @Required
    public void setSessionService(SessionService sessionService) {
        this.sessionService = sessionService;
    }

    @Required
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Required
    public void setCatalogVersionService(CatalogVersionService catalogVersionService) {
        this.catalogVersionService = catalogVersionService;
    }

    @Required
    public void setMediaFactoryStrategy(VMKMediaFactoryStrategy<MediaModel> mediaFactoryStrategy) {
        this.mediaFactoryStrategy = mediaFactoryStrategy;
    }

    @Required
    public void setMediaContainerFactoryStrategy(VMKMediaFactoryStrategy<MediaContainerModel> mediaContainerFactoryStrategy) {
        this.mediaContainerFactoryStrategy = mediaContainerFactoryStrategy;
    }

    @Required
    public void setCatalog(String catalog) {
        this.catalog = catalog;
    }

    @Required
    public void setFolder(String folder) {
        this.folder = folder;
    }

    @Required
    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }

    @Required
    public void setAttribute(String attribute) {
        this.attribute = attribute;
    }

    @Required
    public void setMediaAttributeType(MediaAttributeType mediaAttributeType) {
        this.mediaAttributeType = mediaAttributeType;
    }
}