package com.vanmarcke.hotfolder.utils;

import com.vanmarcke.core.model.BrandCategoryModel;
import com.vanmarcke.hotfolder.resourcespace.enums.Size;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.exceptions.SystemException;
import de.hybris.platform.variants.model.VariantProductModel;
import org.apache.commons.collections4.IterableUtils;

import java.util.Set;

import static java.lang.String.format;

/**
 * The {@code VMKMediaImportUtils} class provides utility methods to make the import of medias easier.
 *
 * @author Taki Korovessis, Christiaan Janssen
 * @since 28-10-2019
 */
public final class VMKMediaImportUtils {

    private static final String PREFIX_BRAND = "b";
    private static final String PREFIX_CATEGORY = "c";
    private static final String PREFIX_PRODUCT = "p";
    private static final String PREFIX_VARIANT = "v";

    /**
     * Returns a media prefix using the type of the given {@code elements}.
     *
     * @param elements the elements
     * @param <T>      the type parameter
     * @return a matching media prefix
     */
    public static <T> String getMediaPrefix(Set<T> elements) {
        T item = IterableUtils.get(elements, 0);
        if (item instanceof VariantProductModel) {
            return PREFIX_VARIANT;
        } else if (item instanceof ProductModel) {
            return PREFIX_PRODUCT;
        } else if (item instanceof BrandCategoryModel) {
            return PREFIX_BRAND;
        } else if (item instanceof CategoryModel) {
            return PREFIX_CATEGORY;
        } else {
            throw new SystemException(format("Class '%s' is not supported", item.getClass()));
        }
    }

    /**
     * Returns an instance of {@link MediaModel} class for the given {@code size} if there's a match in the given
     * {@code mediaContainer}.
     * <p>
     * If there's no match, {@code null} is returned.
     *
     * @param container the media container
     * @param size      the desired size
     * @return the {@link MediaModel} instance
     */
    public static MediaModel getMediaForSize(MediaContainerModel container, Size size) {
        return IterableUtils.find(
                container.getMedias(),
                media -> (media.getMediaFormat() != null)
                        && size.name().equals(media.getMediaFormat().getQualifier()));
    }

    /**
     * Private constructor to hide the default implicit one.
     */
    private VMKMediaImportUtils() {
    }
}