package com.vanmarcke.hotfolder.services.dataimport.batch.account;

import de.hybris.platform.jalo.Item;

/**
 * Adapter to translate an account address import row into a service call.
 */
public interface VMKAccountImportAddressAdapter {

    /**
     * Import an account address value.
     *
     * @param cellValue the cell value
     * @param b2bUnit   the b2bUnit
     * @throws IllegalArgumentException if the cellValue is empty, null or invalid or the address is null
     */
    void performImport(String cellValue, Item b2bUnit);
}
