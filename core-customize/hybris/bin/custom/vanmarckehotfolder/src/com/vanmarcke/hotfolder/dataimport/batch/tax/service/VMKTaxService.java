package com.vanmarcke.hotfolder.dataimport.batch.tax.service;

import com.vanmarcke.hotfolder.data.TaxDTO;
import de.hybris.platform.core.model.order.price.TaxModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.europe1.model.TaxRowModel;

import java.util.Optional;

/**
 * The {@link VMKTaxService} interface defines the business logic to create, retrieve, update and delete tax information.
 *
 * @author Christiaan Janssen
 * @since 02-04-2021
 */
public interface VMKTaxService {

    /**
     * Returns the {@link TaxModel} instance for the given {@code identifier}.
     *
     * @param identifier the identifier
     * @return the {@link TaxModel} instance
     */
    Optional<TaxModel> getTax(String identifier);

    /**
     * Returns the {@link TaxRowModel} instance for the given {@code identifier} and {@code product}.
     *
     * @param identifier the identifier
     * @param product    the product
     * @return the {@link TaxRowModel} instance
     */
    Optional<TaxRowModel> getTaxRow(String identifier, ProductModel product);

    /**
     * Removes the {@link TaxModel} and {@link TaxRowModel} instances for the given {@code identifier} and
     * {@code product}.
     *
     * @param identifier the tax's unique identifier
     * @param product    the product information
     */
    void remove(String identifier, ProductModel product);

    /**
     * Creates the {@link TaxModel} and {@link TaxRowModel} instances for the {@code taxInformation} and
     * {@code product}.
     *
     * @param taxInformation the tax information
     * @param product        the product information
     */
    void createOrUpdate(TaxDTO taxInformation, ProductModel product);
}
