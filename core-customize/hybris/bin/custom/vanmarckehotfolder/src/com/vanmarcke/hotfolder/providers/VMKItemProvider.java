package com.vanmarcke.hotfolder.providers;

import de.hybris.platform.catalog.model.CatalogVersionModel;

import java.util.List;
import java.util.Set;

@FunctionalInterface
public interface VMKItemProvider<T> {

    /**
     * Provides the items to be processed.
     *
     * @param catalogVersion the catalog versiom
     * @param codes          the unique identifiers
     * @return a list of items to be processed
     */
    Set<T> get(CatalogVersionModel catalogVersion, List<String> codes);

}