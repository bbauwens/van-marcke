package com.vanmarcke.hotfolder.dataimport.batch.tax.service.impl;

import com.vanmarcke.hotfolder.data.TaxDTO;
import com.vanmarcke.hotfolder.dataimport.batch.tax.service.VMKTaxService;
import de.hybris.platform.core.model.order.price.TaxModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.europe1.model.TaxRowModel;
import de.hybris.platform.order.TaxService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Optional;

import static org.apache.commons.collections4.CollectionUtils.emptyIfNull;

/**
 * The {@link VMKTaxServiceImpl} class implements the business logic to create, retrieve, update and delete tax
 * information.
 *
 * @author Christiaan Janssen
 * @since 02-04-2021
 */
public class VMKTaxServiceImpl implements VMKTaxService {

    private final TaxService taxService;
    private final ModelService modelService;
    private final Converter<TaxDTO, TaxModel> taxConverter;
    private final Converter<TaxDTO, TaxRowModel> taxRowConverter;

    /**
     * Creates a new instance of the {@link VMKTaxServiceImpl} class.
     *
     * @param taxService      the tax service
     * @param modelService    the model service
     * @param taxConverter    the tax converter
     * @param taxRowConverter the tax row converter
     */
    public VMKTaxServiceImpl(TaxService taxService,
                             ModelService modelService,
                             Converter<TaxDTO, TaxModel> taxConverter,
                             Converter<TaxDTO, TaxRowModel> taxRowConverter) {
        this.taxService = taxService;
        this.modelService = modelService;
        this.taxConverter = taxConverter;
        this.taxRowConverter = taxRowConverter;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<TaxModel> getTax(String identifier) {
        Optional<TaxModel> tax = Optional.empty();
        try {
            tax = Optional.of(taxService.getTaxForCode(identifier));
        } catch (UnknownIdentifierException e) {
            // Do nothing.
        }
        return tax;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<TaxRowModel> getTaxRow(String identifier, ProductModel product) {
        return emptyIfNull(product.getEurope1Taxes())
                .stream()
                .filter(tr -> (tr.getTax() != null && tr.getTax().getCode().equals(identifier)))
                .findAny();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void remove(String identifier, ProductModel product) {
        getTax(identifier).ifPresent(modelService::remove);
        getTaxRow(identifier, product).ifPresent(modelService::remove);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void createOrUpdate(TaxDTO taxInformation, ProductModel product) {
        TaxModel tax = createOrUpdate(taxInformation);
        createOrUpdate(taxInformation, product, tax);
    }

    /**
     * Creates the {@link TaxModel} instances for the {@code taxInformation}.
     *
     * @param taxInformation the tax information
     * @return the {@link TaxModel} class
     */
    private TaxModel createOrUpdate(TaxDTO taxInformation) {
        TaxModel tax = getTax(taxInformation.getCode())
                .orElse(createTax(taxInformation));
        taxConverter.convert(taxInformation, tax);
        modelService.save(tax);
        return tax;
    }

    /**
     * Creates a new instance of the {@link TaxModel} class.
     *
     * @param taxInformation the tax information
     * @return the {@link TaxModel} instance
     */
    private TaxModel createTax(TaxDTO taxInformation) {
        TaxModel tax = modelService.create(TaxModel.class);
        tax.setCode(taxInformation.getCode());
        return tax;
    }

    /**
     * Creates the {@link TaxRowModel} instances for the {@code taxInformation}, {@code product}, and {@code tax}.
     *
     * @param taxInformation the tax information
     * @param product        the product information
     * @param tax            the tax
     */
    private void createOrUpdate(TaxDTO taxInformation, ProductModel product, TaxModel tax) {
        TaxRowModel taxRow = getTaxRow(taxInformation.getCode(), product)
                .orElse(createTaxRow(product, tax));
        taxRowConverter.convert(taxInformation, taxRow);
        modelService.save(taxRow);
    }

    /**
     * Creates a new instance of the {@link TaxRowModel} class.
     *
     * @param product the product
     * @param tax     the tax
     * @return the {@link TaxRowModel} instance
     */
    private TaxRowModel createTaxRow(ProductModel product, TaxModel tax) {
        TaxRowModel taxRow = modelService.create(TaxRowModel.class);
        taxRow.setProduct(product);
        taxRow.setCatalogVersion(product.getCatalogVersion());
        taxRow.setTax(tax);
        return taxRow;
    }
}
