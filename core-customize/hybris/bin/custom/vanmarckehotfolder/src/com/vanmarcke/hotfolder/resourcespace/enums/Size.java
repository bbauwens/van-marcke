package com.vanmarcke.hotfolder.resourcespace.enums;

public enum Size {
    ORIGINAL(""),
    EXTRA_SMALL("xsm"),
    SMALL("sml"),
    MEDIUM("mdm"),
    THUMBNAIL("thm"),
    THUMBNAIL_VANMARCKE("vth"),
    LARGE("lrg"),
    EXTRA_LARGE("xlr"),
    PREVIEW("pre"),
    SCREEN("scn");

    private final String value;

    Size(String value) {
        this.value = value;
    }

    public String value() {
        return this.value;
    }
}