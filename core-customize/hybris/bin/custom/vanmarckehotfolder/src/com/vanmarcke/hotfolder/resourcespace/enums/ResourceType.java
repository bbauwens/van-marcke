package com.vanmarcke.hotfolder.resourcespace.enums;

public enum ResourceType {
    GLOBAL("0"), PHOTO("1"), DOCUMENT("2"), VIDEO("3"), AUDIO("4"), PRODUCT_PHOTOGRAPHY("5");

    private final String value;

    ResourceType(String value) {
        this.value = value;
    }

    public String value() {
        return this.value;
    }
}