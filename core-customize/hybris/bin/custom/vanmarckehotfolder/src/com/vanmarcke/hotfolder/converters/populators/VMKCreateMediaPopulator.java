package com.vanmarcke.hotfolder.converters.populators;

import com.vanmarcke.hotfolder.data.MediaData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.LocaleUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.*;

import static com.vanmarcke.hotfolder.constants.VanmarckehotfolderConstants.MEDIA;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;
import static org.apache.commons.collections4.MapUtils.getString;
import static org.apache.commons.io.FilenameUtils.getBaseName;
import static org.apache.commons.lang3.StringUtils.*;

public class VMKCreateMediaPopulator implements Populator<File, MediaData> {

    @Override
    public void populate(final File source, final MediaData target) {
        target.setQualifier(getBaseName(source.getName().replaceAll("\\s+", "_")));
        populateProperties(source, target);
    }

    protected void populateProperties(final File source, final MediaData target) {
        final Properties properties = loadProperties(source);

        target.setBrands(getStringAsList(properties, "brand"));
        target.setCategories(getStringAsList(properties, "category"));
        target.setProducts(getStringAsList(properties, "product"));

        populateTitle(properties, target);
    }

    private void populateTitle(Properties properties, final MediaData target) {
        final Map<Locale, String> titles = properties.stringPropertyNames().stream()
                .filter(n -> n.startsWith("title"))
                .collect(toMap(this::getLocale, k -> trim(getString(properties, k))));

        //If title is empty for a certain locale, fallback to qualifier
        if (MapUtils.isNotEmpty(titles)) {
            titles.forEach((k, v) -> {
                if (StringUtils.isEmpty(v)) {
                    titles.replace(k, target.getQualifier());
                }
            });
            target.setLocalizedNames(titles);
        } else {
            target.setLocalizedNames(Collections.singletonMap(Locale.ENGLISH, target.getQualifier()));
        }
    }

    private Properties loadProperties(final File source) {
        final Properties properties = new Properties();
        try (InputStream in = new FileInputStream(source)) {
            properties.load(in);
        } catch (Exception e) {
            throw new ConversionException("Failed to load properties.", e);
        }
        return properties;
    }

    private List<String> getStringAsList(final Properties properties, final String key) {
        return Arrays.stream(split(getString(properties, key, EMPTY), MEDIA.SEPARATOR))
                .map(s -> trim(s))
                .distinct()
                .collect(toList());
    }

    private Locale getLocale(final String str) {
        final String[] split = split(str, '.');
        return split.length > 1 ? LocaleUtils.toLocale(split[1]) : Locale.ENGLISH;
    }
}