package com.vanmarcke.hotfolder.dataimport.batch.product.impl;

import com.vanmarcke.core.model.BrandCategoryModel;
import com.vanmarcke.hotfolder.dataimport.batch.product.VMKProductBrandImportAdapter;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.servicelayer.exceptions.SystemException;
import de.hybris.platform.servicelayer.internal.service.AbstractBusinessService;
import org.springframework.beans.factory.annotation.Required;

import java.util.Set;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Predicates.not;
import static java.util.stream.Collectors.toSet;
import static org.apache.commons.collections4.IterableUtils.countMatches;
import static org.apache.commons.collections4.IterableUtils.matchesAny;
import static org.apache.commons.lang3.StringUtils.isBlank;

public class DefaultVMKProductBrandImportAdapter extends AbstractBusinessService implements VMKProductBrandImportAdapter {

    private transient CategoryService categoryService;

    @Override
    public void performImport(final String cellValue, final Item product) {
        checkArgument(product != null);

        try {
            final ProductModel productModel = getModelService().get(product);
            final long brandCount = countMatches(productModel.getSupercategories(), BrandCategoryModel.class::isInstance);
            if (isBlank(cellValue)) {
                if (brandCount > 0) {
                    // remove brand
                    final Set<CategoryModel> newCategories = productModel.getSupercategories().stream()
                            .filter(not(BrandCategoryModel.class::isInstance))
                            .collect(toSet());
                    productModel.setSupercategories(newCategories);
                    getModelService().save(productModel);
                }
            } else if (brandCount > 1 || !matchesAny(productModel.getSupercategories(), c -> c.getCode().equals(cellValue))) {
                // update / add brand
                final Set<CategoryModel> newCategories = productModel.getSupercategories().stream()
                        .filter(not(BrandCategoryModel.class::isInstance))
                        .collect(toSet());
                newCategories.add(categoryService.getCategoryForCode(productModel.getCatalogVersion(), cellValue));
                productModel.setSupercategories(newCategories);
                getModelService().save(productModel);
            }
        } catch (Exception e) {
            throw new SystemException(e);
        }
    }

    @Required
    public void setCategoryService(final CategoryService categoryService) {
        this.categoryService = categoryService;
    }
}
