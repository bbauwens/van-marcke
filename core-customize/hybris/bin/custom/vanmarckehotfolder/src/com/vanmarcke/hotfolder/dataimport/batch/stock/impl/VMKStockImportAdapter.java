package com.vanmarcke.hotfolder.dataimport.batch.stock.impl;

import de.hybris.platform.acceleratorservices.dataimport.batch.stock.impl.DefaultStockImportAdapter;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.exceptions.SystemException;
import org.apache.log4j.Logger;
import org.springframework.util.Assert;

/**
 * The {@link VMKStockImportAdapter} class is used to overwrite the {@link DefaultStockImportAdapter} class.
 *
 * @author Christiaan Janssen
 * @since 01-09-2021
 */
public class VMKStockImportAdapter extends DefaultStockImportAdapter {

    private static final Logger LOG = Logger.getLogger(VMKStockImportAdapter.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public void performImport(final String cellValue, final Item product) {
        Assert.hasText(cellValue);
        Assert.notNull(product);
        try {
            final String[] values = cellValue.split(":");

            final ProductModel productModel = getModelService().get(product);
            final WarehouseModel warehouseModel = getWarehouseService().getWarehouseForCode(values[0]);
            final int actualAmount = getActualAmount(values);

            getStockService().updateActualStockLevel(productModel, warehouseModel, actualAmount, null);
        } catch (final RuntimeException e) {
            LOG.warn("Could not import stock for product " + product + ": " + e);
            throw e;
        } catch (final Exception e) {
            LOG.warn("Could not import stock for " + product + ": " + e);
            throw new SystemException("Could not import stock for " + product, e);
        }
    }

    /**
     * Returns the actual number of products in stock.
     * <p>
     * Returns 0 if there's no information provided.
     *
     * @param values the values
     * @return the actual number of products in stock
     */
    private int getActualAmount(final String[] values) {
        int actualAmount;
        if (values.length > 1 && values[0] != null && !values[1].isEmpty()) {
            actualAmount = Integer.parseInt(values[1]);
        } else {
            actualAmount = 0;
        }
        return actualAmount;
    }
}
