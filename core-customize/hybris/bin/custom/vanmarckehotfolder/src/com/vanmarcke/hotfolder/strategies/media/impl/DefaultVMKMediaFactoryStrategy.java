package com.vanmarcke.hotfolder.strategies.media.impl;

import com.vanmarcke.hotfolder.data.MediaFactoryParameter;
import com.vanmarcke.hotfolder.resourcespace.enums.ResourceType;
import com.vanmarcke.hotfolder.resourcespace.service.VMKResourceSpaceService;
import com.vanmarcke.hotfolder.services.VMKMediaService;
import de.hybris.platform.core.model.media.MediaFolderModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.model.ModelService;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.util.Locale;
import java.util.Map;

public class DefaultVMKMediaFactoryStrategy extends AbstractVMKMediaFactoryStrategy<MediaModel> {

    private static final String VIDEO = "VIDEO";

    /**
     * Creates a new instance of the {@link DefaultVMKMediaFactoryStrategy} class.
     *
     * @param modelService       the model service
     * @param integrationService the integration service
     * @param mediaService       the media service
     */
    public DefaultVMKMediaFactoryStrategy(ModelService modelService,
                                          VMKResourceSpaceService integrationService,
                                          VMKMediaService mediaService) {
        super(modelService, integrationService, mediaService);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MediaModel create(MediaFactoryParameter parameter) {
        MediaModel media = getOrCreateMedia(parameter.getCatalogVersion(), parameter.getQualifier());

        if (hasBeenUpdatedRecently(media.getExternalID(), media.getModifiedtime())) {
            return media;
        }

        deleteResource(media.getExternalID());

        File file = parameter.getFile();
        String name = StringUtils.deleteWhitespace(file.getName());
        String extension = FilenameUtils.getExtension(name);

        String externalID = integrationService.create(file, FilenameUtils.getBaseName(name), ResourceType.valueOf(parameter.getType()));
        MediaFolderModel folder = mediaService.getOrCreateMediaFolder(parameter.getFolderQualifier());

        media.setExternalID(externalID);
        media.setFolder(folder);
        media.setRealFileName(name);
        media.setURL(integrationService.getPath(externalID, null, extension));

        if (VIDEO.equals(parameter.getType())) {
            media.setMediaFormat(mediaService.getFormat(VIDEO));
        }

        for (Map.Entry<Locale, String> entry : parameter.getLocalizedNames().entrySet()) {
            media.setName(entry.getValue(), entry.getKey());
        }

        modelService.save(media);
        return media;
    }
}