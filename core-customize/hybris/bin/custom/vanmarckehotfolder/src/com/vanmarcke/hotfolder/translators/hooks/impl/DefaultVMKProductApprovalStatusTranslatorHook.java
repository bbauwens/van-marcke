package com.vanmarcke.hotfolder.translators.hooks.impl;

import com.vanmarcke.hotfolder.translators.hooks.VMKImpexTranslatorHook;
import de.hybris.platform.core.model.product.ProductModel;

public class DefaultVMKProductApprovalStatusTranslatorHook implements VMKImpexTranslatorHook<ProductModel> {

    @Override
    public String translate(final String valueExpr, final ProductModel toItem) {
        return toItem == null ? "unapproved" : IGNORE;
    }
}