package com.vanmarcke.hotfolder.dataimport.batch.tax.populator;

import com.vanmarcke.hotfolder.data.TaxDTO;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.price.TaxModel;

/**
 * The {@link VMKTaxReversePopulator} class is used to populate instances of the {@link TaxModel} class with the
 * information from the instances of the {@link TaxDTO} class.
 *
 * @author Christiaan Janssen
 * @since 02-04-2021
 */
public class VMKTaxReversePopulator implements Populator<TaxDTO, TaxModel> {

    /**
     * {@inheritDoc}
     */
    @Override
    public void populate(TaxDTO taxDTO, TaxModel taxModel) {
        taxModel.setType(taxDTO.getType());
    }
}
