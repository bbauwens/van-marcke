package com.vanmarcke.hotfolder.resourcespace.api;

import org.springframework.web.util.UriTemplateHandler;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;

public class DefaultVMKUriTemplateHandler implements UriTemplateHandler {

    @Override
    public URI expand(final String uriTemplate, final Map<String, ?> uriVariables) {
        return createUri(uriTemplate);
    }

    @Override
    public URI expand(final String uriTemplate, final Object... uriVariables) {
        return createUri(uriTemplate);
    }

    private URI createUri(final String uriTemplate) {
        try {
            return new URI(uriTemplate);
        } catch (URISyntaxException e) {
            throw new IllegalStateException("Could not create URI object: " + e.getMessage(), e);
        }
    }
}