package com.vanmarcke.hotfolder.resourcespace.api;

import eu.elision.integration.command.configuration.annotation.CommandConfig;

/**
 * The {@code VMKDeleteResourceCommand} class represents the API call used to remove a resource from ResourceSpace.
 *
 * @author Christiaan Janssen
 * @since 16-04-2020
 */
@CommandConfig(serviceName = "vmkResourceSpaceService", url = "/")
public class VMKDeleteResourceCommand extends AbstractVMKCommand<String> {

    /**
     * Creates a new instance of the {@link VMKDeleteResourceCommand} class.
     *
     * @param id the unique identifier of the resource
     */
    public VMKDeleteResourceCommand(String id) {
        addRequestParam("function", "delete_resource");
        addRequestParam("param1", id);
    }
}
