package com.vanmarcke.hotfolder.strategies.impl;

import com.vanmarcke.core.enums.VMBranchType;
import com.vanmarcke.core.model.BrandCategoryModel;
import com.vanmarcke.hotfolder.data.MediaData;
import com.vanmarcke.hotfolder.resourcespace.enums.MetaDataField;
import com.vanmarcke.hotfolder.resourcespace.enums.ResourceType;
import com.vanmarcke.hotfolder.strategies.AbstractVMKMediaImportStrategy;
import de.hybris.platform.catalog.model.CatalogVersionModel;

import java.util.Locale;
import java.util.Objects;
import java.util.Set;

import static com.vanmarcke.hotfolder.constants.VanmarckehotfolderConstants.MEDIA;
import static com.vanmarcke.hotfolder.constants.VanmarckehotfolderConstants.MEDIA.CHECKBOX_LIST_SEPARATOR;
import static java.util.stream.Collectors.joining;

/**
 * The {@code AbstractVMKBrandMediaImportStrategy} class contains common business logic to import brand media.
 *
 * @author Taki Korovessis, Christiaan Janssen, Tom van den Berg
 * @since 13-10-2019
 */
public class VMKBrandMediaImportStrategy extends AbstractVMKMediaImportStrategy<BrandCategoryModel> {

    /**
     * {@inheritDoc}
     */
    @Override
    public Set<BrandCategoryModel> getItems(CatalogVersionModel catalogVersion, MediaData media) {
        return itemProvider.get(catalogVersion, media.getBrands());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateMetaData(String externalID, Set<Locale> locales, Set<BrandCategoryModel> brands) {
        super.updateMetaData(externalID, locales, brands);

        integrationService.updateField(externalID, MetaDataField.MANUFACTURER, brands.stream()
                .map(BrandCategoryModel::getManufacturer)
                .filter(Objects::nonNull)
                .distinct()
                .collect(joining(MEDIA.CHECKBOX_LIST_SEPARATOR)));

        integrationService.updateField(externalID, MetaDataField.SKU, brands.stream()
                .map(BrandCategoryModel::getCode)
                .collect(joining(MEDIA.CHECKBOX_LIST_SEPARATOR)));

        integrationService.updateField(externalID, MetaDataField.BRANDNAME, brands.stream()
                .map(BrandCategoryModel::getCode)
                .collect(joining(MEDIA.CHECKBOX_LIST_SEPARATOR)));

        if (ResourceType.DOCUMENT == resourceType) {
            integrationService.updateField(externalID, MetaDataField.DOCUMENT_LANGUAGE, locales.stream()
                    .map(Locale::toString)
                    .collect(joining(CHECKBOX_LIST_SEPARATOR)));

            integrationService.updateField(externalID, MetaDataField.DOCUMENT_BRANCHE, brands.stream()
                    .map(BrandCategoryModel::getVmBranch)
                    .filter(Objects::nonNull)
                    .distinct()
                    .map(VMBranchType::getCode)
                    .collect(joining(MEDIA.CHECKBOX_LIST_SEPARATOR)));
        }
    }
}