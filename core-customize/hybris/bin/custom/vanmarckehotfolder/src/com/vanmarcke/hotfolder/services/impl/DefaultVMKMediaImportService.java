package com.vanmarcke.hotfolder.services.impl;

import com.vanmarcke.hotfolder.data.MediaData;
import com.vanmarcke.hotfolder.strategies.VMKMediaImportStrategy;
import de.hybris.platform.cloud.hotfolder.dataimport.batch.zip.ZipBatchHeader;
import de.hybris.platform.cloud.hotfolder.dataimport.batch.zip.service.UnzippedFolderImportService;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.io.File;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Pattern;

import static java.util.stream.Collectors.toList;
import static org.apache.commons.io.FilenameUtils.getBaseName;
import static org.apache.commons.io.FilenameUtils.getExtension;
import static org.apache.commons.lang.StringUtils.defaultIfEmpty;
import static org.apache.commons.lang.StringUtils.substringAfterLast;
import static org.apache.commons.lang3.math.NumberUtils.INTEGER_ZERO;
import static org.apache.commons.lang3.math.NumberUtils.toInt;

public class DefaultVMKMediaImportService implements UnzippedFolderImportService {

    private static final String META_SUFFIX = "meta";

    private static final Comparator<File> FILE_COMPARATOR = new Comparator<File>() {

        @Override
        public int compare(final File f1, final File f2) {
            final Integer o1 = getOrder(f1);
            final Integer o2 = getOrder(f2);
            return o1.compareTo(o2);
        }

        private Integer getOrder(final File f) {
            final String order = defaultIfEmpty(substringAfterLast(getBaseName(f.getName()), "$"), "0");
            return toInt(order, INTEGER_ZERO);
        }
    };

    private final Pattern allowedFileNamePattern;
    private final Converter<File, MediaData> createMediaConverter;
    private final VMKMediaImportStrategy importStrategy;

    public DefaultVMKMediaImportService(final Pattern allowedFileNamePattern, final Converter<File, MediaData> createMediaConverter, final VMKMediaImportStrategy importStrategy) {
        this.allowedFileNamePattern = allowedFileNamePattern;
        this.createMediaConverter = createMediaConverter;
        this.importStrategy = importStrategy;
    }

    @Override
    public void execute(final ZipBatchHeader header) {
        final List<MediaData> mediaDataList = header.getUnzippedFiles()
                .stream()
                .filter(e -> META_SUFFIX.equals(getExtension(e.getName())))
                .map(createMediaConverter::convert)
                .collect(toList());

        mediaDataList.forEach(m -> {
            m.setFiles(header.getUnzippedFiles()
                    .stream()
                    .filter(f -> getBaseName(f.getName()).startsWith(m.getQualifier()))
                    .filter(f -> allowedFileNamePattern.matcher(f.getName()).matches())
                    .sorted(FILE_COMPARATOR)
                    .collect(toList()));

            importStrategy.execute(m);
        });
    }
}
