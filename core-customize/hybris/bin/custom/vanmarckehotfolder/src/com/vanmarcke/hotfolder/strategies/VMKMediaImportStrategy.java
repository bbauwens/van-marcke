package com.vanmarcke.hotfolder.strategies;

import com.vanmarcke.hotfolder.data.MediaAttributeType;
import com.vanmarcke.hotfolder.data.MediaData;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaModel;

import java.util.Locale;
import java.util.Set;

/**
 * The {@code VMKMediaImportStrategy} interface defines the business logic to import media.
 *
 * @author Taki Korovessiss, Christiaan Janssen
 * @since 09-10-2019
 */
public interface VMKMediaImportStrategy<T extends ItemModel> {

    /**
     * Execute the import for the given {@code mediaDTO}.
     *
     * @param mediaDTO the media to import
     */
    void execute(MediaData mediaDTO);

    /**
     * Extracts the items to update the media for from the given {@code mediaDTO}.
     *
     * @param catalogVersion the catalog version to import the media for
     * @param mediaDTO       the media to import
     * @return the items to update the media for
     */
    Set<T> getItems(CatalogVersionModel catalogVersion, MediaData mediaDTO);

    /**
     * Updates the meta data for the media with the given {@code externalID}.
     *
     * @param externalID the media's ResourceSpace ID
     * @param locales    the locales for which the update is applicable
     * @param items      the items for which the update is applicable
     */
    void updateMetaData(String externalID, Set<Locale> locales, Set<T> items);

    /**
     * Updates the given {@code media} for the given {@code locale}, {@code item} and {@code item attribute}.
     *
     * @param media         the media containers to update
     * @param locale        the locale for which the update is applicable
     * @param item          the item for which the update is applicable
     * @param attribute     the item attribute for which the update is applicable
     * @param attributeType the media attribute type
     */
    void update(MediaModel media, Locale locale, T item, String attribute, MediaAttributeType attributeType);

    /**
     * Updates the given {@code media container} for the given {@code item} and {@code item attribute}.
     *
     * @param mediaContainer the media container to update
     * @param item           the item for which the update is applicable
     * @param attribute      the item attribute for which the update is applicable
     * @param attributeType  the media attribute type
     */
    void update(MediaContainerModel mediaContainer, T item, String attribute, MediaAttributeType attributeType);

    /**
     * Updates the given {@code media container} for the given {@code item} and {@code item attribute}.
     *
     * @param mediaContainer the media container to update
     * @param locale         the locale
     * @param item           the item for which the update is applicable
     * @param attribute      the item attribute for which the update is applicable
     * @param attributeType  the media attribute type
     */
    void update(MediaContainerModel mediaContainer, Locale locale, T item, String attribute, MediaAttributeType attributeType);

    /**
     * Removes the given {@code media } for the given {@code locale}, {@code item} and {@code item attribute}.
     *
     * @param media         the media  to remove
     * @param locale        the locale for which the removal is applicable
     * @param item          the item for which the removal is applicable
     * @param attribute     the item attribute for which the removal is applicable
     * @param attributeType the media attribute type
     */
    void remove(MediaModel media, Locale locale, T item, String attribute, MediaAttributeType attributeType);

    /**
     * Removes the given {@code media container} for the given {@code item} and {@code item attribute}.
     *
     * @param mediaContainer the media container to remove
     * @param item           the item for which the removal is applicable
     * @param attribute      the item attribute for which the removal is applicable
     * @param attributeType  the media attribute type
     */
    void remove(MediaContainerModel mediaContainer, T item, String attribute, MediaAttributeType attributeType);

    /**
     * Removes the given {@code media container} for the given {@code item} and {@code item attribute}.
     *
     * @param mediaContainer the media container to remove
     * @param locale         the locale
     * @param item           the item for which the removal is applicable
     * @param attribute      the item attribute for which the removal is applicable
     * @param attributeType  the media attribute type
     */
    void remove(MediaContainerModel mediaContainer, Locale locale, T item, String attribute, MediaAttributeType attributeType);
}