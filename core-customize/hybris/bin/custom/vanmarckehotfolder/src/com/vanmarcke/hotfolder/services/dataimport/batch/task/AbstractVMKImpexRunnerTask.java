package com.vanmarcke.hotfolder.services.dataimport.batch.task;

import com.google.common.base.Preconditions;
import de.hybris.platform.acceleratorservices.dataimport.batch.BatchHeader;
import de.hybris.platform.acceleratorservices.dataimport.batch.HeaderTask;
import de.hybris.platform.acceleratorservices.dataimport.batch.task.AbstractImpexRunnerTask;
import de.hybris.platform.servicelayer.exceptions.SystemException;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;
import de.hybris.platform.servicelayer.user.UserService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import java.io.File;
import java.io.FileNotFoundException;

/**
 * Task that imports an impex file by executing impex.
 */
public abstract class AbstractVMKImpexRunnerTask extends AbstractImpexRunnerTask implements HeaderTask {

    private UserService userService;

    @Override
    public BatchHeader execute(final BatchHeader header) throws FileNotFoundException {
        return getSessionService().executeInLocalView(new SessionExecutionBody() {
            @Override
            public BatchHeader execute() {
                try {
                    return doExecute(header);
                } catch (final FileNotFoundException e) {
                    throw new SystemException(e);
                }
            }
        }, getUserService().getAdminUser());
    }

    /**
     * Original execute method coming from AbstractImpeRunnerTask
     *
     * @param header
     * @return
     * @throws FileNotFoundException
     */
    protected BatchHeader doExecute(final BatchHeader header) throws FileNotFoundException {
        Preconditions.checkNotNull(header);
        Preconditions.checkNotNull(header.getEncoding());
        if (CollectionUtils.isNotEmpty(header.getTransformedFiles())) {
            for (final File file : header.getTransformedFiles()) {
                processFile(file, header.getEncoding());
            }
        }
        return header;
    }

    public UserService getUserService() {
        return this.userService;
    }

    @Required
    public void setUserService(final UserService userService) {
        this.userService = userService;
    }
}
