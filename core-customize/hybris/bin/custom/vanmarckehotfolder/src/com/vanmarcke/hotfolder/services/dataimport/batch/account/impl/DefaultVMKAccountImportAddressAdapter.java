package com.vanmarcke.hotfolder.services.dataimport.batch.account.impl;

import com.vanmarcke.hotfolder.services.dataimport.batch.account.VMKAccountImportAddressAdapter;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.servicelayer.exceptions.SystemException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Collection;
import java.util.HashSet;

import static com.google.common.base.Preconditions.checkArgument;
import static java.lang.String.format;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;
import static org.apache.commons.lang3.BooleanUtils.isNotTrue;
import static org.apache.commons.lang3.BooleanUtils.isTrue;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;

/**
 * Default implementation of {@link VMKAccountImportAddressAdapter}.
 */
public class DefaultVMKAccountImportAddressAdapter implements VMKAccountImportAddressAdapter {

    private final ModelService modelService;
    private final CommonI18NService commonI18NService;

    /**
     * Constructor for DefaultVMKAccountImportAddressAdapter
     *
     * @param modelService      the modelService
     * @param commonI18NService the commonI18NService
     */
    public DefaultVMKAccountImportAddressAdapter(ModelService modelService, CommonI18NService commonI18NService) {
        this.modelService = modelService;
        this.commonI18NService = commonI18NService;
    }

    @Override
    public void performImport(String cellValue, Item unit) {
        checkArgument(unit != null, "B2B Unit cannot be null");

        if (isNotEmpty(cellValue)) {
            B2BUnitModel unitModel = modelService.get(unit);
            try {
                importAddress(unitModel, cellValue);
            } catch (final Exception e) {
                throw new SystemException(format("Could not import address for B2B unit '%s'", unitModel.getUid()), e);
            }
        }
    }

    /**
     * Updates or creates the Address
     *
     * @param unitModel the passed B2B Unit
     * @param cellValue the given cell value
     * @return the updated or created A
     */
    protected void importAddress(B2BUnitModel unitModel, String cellValue) {
        final String[] values = cellValue.split(":", -1);

        AddressModel billingAddress = getBillingAddressForUnit(unitModel);
        if (billingAddress != null) {
            populate(values, billingAddress);
            modelService.save(billingAddress);
        } else {
            billingAddress = modelService.create(AddressModel.class);
            billingAddress.setOwner(unitModel);
            populate(values, billingAddress);
            modelService.save(billingAddress);

            Collection<AddressModel> addresses = new HashSet<>(unitModel.getAddresses());
            addresses.add(billingAddress);
            unitModel.setAddresses(addresses);
        }

        unitModel.setBillingAddress(billingAddress);

        if (unitModel.getShippingAddress() == null) {
            unitModel.setShippingAddress(billingAddress);
        }
        modelService.save(unitModel);
        modelService.refresh(unitModel);
    }

    protected AddressModel getBillingAddressForUnit(B2BUnitModel unitModel) {
        if (isNotEmpty(unitModel.getBillingAddresses())) {
            return unitModel.getBillingAddresses().stream()
                    .filter(e -> isNotTrue(e.getDuplicate()))
                    .filter(e -> isTrue(e.getShippingAddress()))
                    .filter(e -> isTrue(e.getVisibleInAddressBook()))
                    .findFirst()
                    .orElseGet(null);
        }
        return null;
    }

    protected void populate(String[] source, AddressModel target) {
        target.setExternalID(source[0]);
        target.setCompany(source[12]);
        target.setStreetname(source[3]);
        target.setStreetnumber(source[4]);
        target.setAppartment(source[5]);
        target.setTown(source[6]);
        target.setPostalcode(source[7]);
        target.setCountry(this.commonI18NService.getCountry(source[8]));
        target.setPhone1(source[9]);
        target.setPhone2(source[10]);
        target.setEmail(source[11]);
        target.setBillingAddress(true);
        target.setShippingAddress(true);
        target.setVisibleInAddressBook(true);
    }
}