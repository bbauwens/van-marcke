package com.vanmarcke.hotfolder.services.impl;

import com.vanmarcke.hotfolder.services.VMKMediaService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaFolderModel;
import de.hybris.platform.core.model.media.MediaFormatModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.media.MediaIOException;
import de.hybris.platform.servicelayer.media.impl.DefaultMediaService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import static java.lang.String.format;
import static org.apache.commons.io.FileUtils.openInputStream;
import static org.apache.commons.io.FilenameUtils.getBaseName;
import static org.apache.commons.lang3.StringUtils.deleteWhitespace;

public class DefaultVMKMediaService extends DefaultMediaService implements VMKMediaService {

    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultVMKMediaService.class);

    private FlexibleSearchService flexibleSearchService;

    @Override
    public <T extends MediaModel> T createMediaFromFile(final File file, final Class<T> clazz) {
        final String fName = deleteWhitespace(file.getName());
        final String fBaseName = getBaseName(fName);

        final T mediaModel = getModelService().create(clazz);
        mediaModel.setCode(System.currentTimeMillis() + "-" + fBaseName);
        mediaModel.setRealFileName(fName);
        getModelService().save(mediaModel);

        try (final InputStream inputStream = openInputStream(file)) {
            setStreamForMedia(mediaModel, inputStream);
            getModelService().refresh(mediaModel);
            return mediaModel;
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
            throw new MediaIOException(e.getMessage(), e);
        }
    }

    @Override
    public MediaContainerModel getMediaContainer(final CatalogVersionModel catalogVersion, final String qualifier) {
        final MediaContainerModel mediaContainerModel = new MediaContainerModel();
        mediaContainerModel.setQualifier(qualifier);
        mediaContainerModel.setCatalogVersion(catalogVersion);
        try {
            return flexibleSearchService.getModelByExample(mediaContainerModel);
        } catch (final ModelNotFoundException e) {
            throw new UnknownIdentifierException("No media container with qualifier " + qualifier + " in catalog version " + catalogVersion.getVersion() + " can be found.", e);
        }
    }

    @Override
    public MediaFolderModel getOrCreateMediaFolder(final String qualifier) {
        MediaFolderModel mediaFolderModel;
        try {
            mediaFolderModel = getFolder(qualifier);
        } catch (UnknownIdentifierException e) {
            LOGGER.debug(format("Could not find media folder '%s'. Will be created on the fly.", qualifier), e);
            mediaFolderModel = getModelService().create(MediaFolderModel.class);
            mediaFolderModel.setQualifier(qualifier);
            mediaFolderModel.setPath(qualifier);
            getModelService().save(mediaFolderModel);
        }
        return mediaFolderModel;
    }

    @Override
    public MediaFormatModel getOrCreateMediaFormat(final String qualifier) {
        MediaFormatModel mediaFormatModel;
        try {
            mediaFormatModel = getFormat(qualifier);
        } catch (UnknownIdentifierException e) {
            LOGGER.debug(format("Could not find media format '%s'. Will be created on the fly.", qualifier), e);
            mediaFormatModel = getModelService().create(MediaFormatModel.class);
            mediaFormatModel.setQualifier(qualifier);
            getModelService().save(mediaFormatModel);
        }
        return mediaFormatModel;
    }

    @Override
    public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService) {
        super.setFlexibleSearchService(flexibleSearchService);
        this.flexibleSearchService = flexibleSearchService;
    }
}