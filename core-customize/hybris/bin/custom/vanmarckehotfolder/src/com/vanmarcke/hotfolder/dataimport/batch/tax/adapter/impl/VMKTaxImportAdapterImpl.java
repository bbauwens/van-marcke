package com.vanmarcke.hotfolder.dataimport.batch.tax.adapter.impl;

import com.vanmarcke.hotfolder.data.TaxDTO;
import com.vanmarcke.hotfolder.dataimport.batch.tax.adapter.VMKTaxImportAdapter;
import com.vanmarcke.hotfolder.dataimport.batch.tax.service.VMKTaxService;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.servicelayer.model.ModelService;
import org.apache.commons.lang3.BooleanUtils;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

/**
 * The {@link VMKTaxImportAdapterImpl} class implements the methods to import tax information.
 *
 * @author Christiaan Janssen
 * @since 02-04-2021
 */
public class VMKTaxImportAdapterImpl implements VMKTaxImportAdapter {

    private final ModelService modelService;
    private final VMKTaxService taxService;

    /**
     * Creates a new instance of the {@link VMKTaxImportAdapterImpl} class.
     *
     * @param modelService the model service
     * @param taxService   the tax service
     */
    public VMKTaxImportAdapterImpl(ModelService modelService,
                                   VMKTaxService taxService) {
        this.modelService = modelService;
        this.taxService = taxService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void performImport(String taxInformation, Item item) {
        validateParameterNotNullStandardMessage("tax information", taxInformation);
        validateParameterNotNullStandardMessage("product", item);

        ProductModel product = modelService.get(item);
        TaxDTO tax = create(taxInformation.split(":"), product);

        if (!tax.isActive()) {
            taxService.remove(tax.getCode(), product);
        } else {
            taxService.createOrUpdate(tax, product);
        }
    }

    /**
     * Creates a {@link TaxDTO} instance for the given {@code taxInformation} and {@code procuct}.
     *
     * @param taxInformation the tax information
     * @param product        the product
     * @return the {@link TaxDTO} instance
     */
    private TaxDTO create(String[] taxInformation, ProductModel product) {
        TaxDTO tax = new TaxDTO();
        tax.setCode(product.getCode() + "-" + taxInformation[0] + "-" + taxInformation[3]);
        tax.setCountryIsoCode(taxInformation[0]);
        tax.setValue(Double.parseDouble(taxInformation[1]));
        tax.setCurrencyIsoCode(taxInformation[2]);
        tax.setType(taxInformation[3]);
        tax.setActive(BooleanUtils.toBoolean(taxInformation[4]));
        return tax;
    }
}
