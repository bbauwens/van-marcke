package com.vanmarcke.hotfolder.providers.impl;

import com.vanmarcke.core.model.BrandCategoryModel;
import com.vanmarcke.hotfolder.providers.VMKItemProvider;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import org.apache.log4j.Logger;

import java.util.List;
import java.util.Set;

import static java.util.stream.Collectors.toSet;

/**
 * This class implements the {@link VMKItemProvider} interface. It implements methods to provide brands.
 *
 * @author Taki Korovessis, Tom van den Berg
 * @since 27/10/2019
 */
public class DefaultVMKBrandProvider implements VMKItemProvider<BrandCategoryModel> {

    private static final Logger LOGGER = Logger.getLogger(DefaultVMKBrandProvider.class);

    private final CategoryService categoryService;

    /**
     * Provides an instance of the {@code DefaultVMKBrandProvider}.
     *
     * @param categoryService the category service
     */
    public DefaultVMKBrandProvider(final CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Set<BrandCategoryModel> get(final CatalogVersionModel catalogVersion, final List<String> codes) {
        return codes.stream()
                .map(e -> retrieveCategoryForCode(catalogVersion, e))
                .filter(BrandCategoryModel.class::isInstance)
                .map(BrandCategoryModel.class::cast)
                .collect(toSet());
    }

    /**
     * Retrieves a brand for the given code. If the brand is not found, null is returned.
     *
     * @param catalogVersion the catalog version
     * @param code           the category code
     * @return the category
     */
    private CategoryModel retrieveCategoryForCode(CatalogVersionModel catalogVersion, String code) {
        try {
            return categoryService.getCategoryForCode(catalogVersion, code);
        } catch (UnknownIdentifierException | IllegalArgumentException | AmbiguousIdentifierException e) {
            LOGGER.error(e.getMessage());
        }
        return null;
    }
}