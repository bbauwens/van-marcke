package com.vanmarcke.hotfolder.translators.hooks;

import de.hybris.platform.acceleratorservices.dataimport.batch.converter.ImpexConverter;
import de.hybris.platform.core.model.ItemModel;

/**
 * Interface of all value translator instances.
 * These objects are used by {@link ImpexConverter} to translate an incoming value into attribute value(s)
 *
 * @param <T> the type of the target item
 */
@FunctionalInterface
public interface VMKImpexTranslatorHook<T extends ItemModel> {

    String IGNORE = "<ignore>";

    /**
     * Translates a tokenized csv value for import usage.
     *
     * @param valueExpr expression to translate
     * @param toItem    the target item
     * @return translated value
     */
    String translate(String valueExpr, T toItem);

}