package com.vanmarcke.hotfolder.dataimport.batch.tax;

import com.vanmarcke.hotfolder.dataimport.batch.tax.adapter.VMKTaxImportAdapter;
import de.hybris.platform.core.Registry;
import de.hybris.platform.impex.jalo.header.SpecialColumnDescriptor;
import de.hybris.platform.impex.jalo.translators.AbstractSpecialValueTranslator;
import de.hybris.platform.jalo.Item;
import org.apache.commons.lang.StringUtils;

/**
 * Translator for updating the tax via {@link de.hybris.platform.order.TaxService}.
 *
 * @author Niels Raemaekers
 * @since 04-11-2021
 */
public class VMKTaxTranslator extends AbstractSpecialValueTranslator {

    private static final String MODIFIER_NAME_ADAPTER = "adapter";
    private static final String DEFAULT_IMPORT_ADAPTER_NAME = "taxTaxImportAdapter";

    private VMKTaxImportAdapter vmkTaxImportAdapter;

    /**
     * {@inheritDoc}
     */
    @Override
    public void init(SpecialColumnDescriptor columnDescriptor) {
        setVmkTaxImportAdapter((VMKTaxImportAdapter) Registry.getApplicationContext().getBean(getImportAdapterBeanName(columnDescriptor)));
    }

    /**
     * Returns the bean name of the import adapter.
     *
     * @param columnDescriptor the column descriptor
     * @return the import adapter bean name
     */
    protected String getImportAdapterBeanName(final SpecialColumnDescriptor columnDescriptor) {
        String beanName = columnDescriptor.getDescriptorData().getModifier(MODIFIER_NAME_ADAPTER);
        if (StringUtils.isBlank(beanName)) {
            beanName = DEFAULT_IMPORT_ADAPTER_NAME;
        }
        return beanName;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void performImport(String cellValue, Item processedItem) {
        vmkTaxImportAdapter.performImport(cellValue, processedItem);
    }

    /**
     * Sets the given {@code vmkTaxImportAdapter}.
     *
     * @param vmkTaxImportAdapter the tax import adapter
     */
    public void setVmkTaxImportAdapter(VMKTaxImportAdapter vmkTaxImportAdapter) {
        this.vmkTaxImportAdapter = vmkTaxImportAdapter;
    }
}
