package com.vanmarcke.hotfolder.strategies.media;

import com.vanmarcke.hotfolder.data.MediaFactoryParameter;

/**
 * Factory for the media types: {@link de.hybris.platform.core.model.media.MediaModel } and {@link de.hybris.platform.core.model.media.MediaContainerModel}
 *
 * @param <T> the media parameter type
 */
@FunctionalInterface
public interface VMKMediaFactoryStrategy<T> {

    /**
     * Creates a new implementation dependent media object instance of {@link de.hybris.platform.core.model.media.MediaModel }
     * or {@link de.hybris.platform.core.model.media.MediaContainerModel}
     *
     * @return a media object
     */
    T create(MediaFactoryParameter parameter);
}