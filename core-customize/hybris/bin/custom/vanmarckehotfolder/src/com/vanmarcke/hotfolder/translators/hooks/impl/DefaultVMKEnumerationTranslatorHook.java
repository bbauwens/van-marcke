package com.vanmarcke.hotfolder.translators.hooks.impl;

import com.vanmarcke.hotfolder.translators.hooks.VMKImpexTranslatorHook;
import de.hybris.platform.core.HybrisEnumValue;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.enumeration.EnumerationService;

import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.apache.commons.lang3.StringUtils.replaceChars;

public class DefaultVMKEnumerationTranslatorHook implements VMKImpexTranslatorHook<ItemModel> {

    private final EnumerationService enumerationService;

    private final String enumerationCode;

    public DefaultVMKEnumerationTranslatorHook(final EnumerationService enumerationService, final String enumerationCode) {
        this.enumerationService = enumerationService;
        this.enumerationCode = enumerationCode;
    }

    @Override
    public String translate(final String valueExpr, final ItemModel toItem) {
        if (isNotBlank(valueExpr)) {
            final String valueCode = replaceChars(valueExpr, ' ', '_');
            final HybrisEnumValue hybrisEnumValue = enumerationService.getEnumerationValue(enumerationCode, valueCode);
            if (hybrisEnumValue != null) {
                return hybrisEnumValue.getCode();
            }
        }
        return IGNORE;
    }
}