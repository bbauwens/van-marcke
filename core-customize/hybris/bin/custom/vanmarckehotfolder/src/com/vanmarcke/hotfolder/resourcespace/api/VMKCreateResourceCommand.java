package com.vanmarcke.hotfolder.resourcespace.api;

import de.hybris.platform.servicelayer.exceptions.SystemException;
import eu.elision.integration.command.configuration.annotation.CommandConfig;

import java.io.UnsupportedEncodingException;

import static java.lang.String.format;
import static java.net.URLEncoder.encode;
import static org.apache.commons.lang3.StringUtils.EMPTY;

@CommandConfig(serviceName = "vmkResourceSpaceService", url = "/")
public class VMKCreateResourceCommand extends AbstractVMKCommand<String> {

    public VMKCreateResourceCommand(final String type, final String url) {
        addRequestParam("function", "create_resource");
        addRequestParam("param1", type);
        addRequestParam("param2", "0");
        try {
            addRequestParam("param3", encode(url, "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            throw new SystemException(format("URL '%s' could not be encoded", url), e);
        }
        addRequestParam("param4", EMPTY);
        addRequestParam("param5", EMPTY);
        addRequestParam("param6", "false");
        addRequestParam("param7", EMPTY);
    }

}