package com.vanmarcke.hotfolder.services.dataimport.batch.account.translator;

import com.vanmarcke.hotfolder.services.dataimport.batch.account.VMKAccountImportASMEmployeeAdapter;
import de.hybris.platform.core.Registry;
import de.hybris.platform.impex.jalo.header.SpecialColumnDescriptor;
import de.hybris.platform.impex.jalo.translators.AbstractSpecialValueTranslator;
import de.hybris.platform.jalo.Item;
import org.apache.commons.lang.StringUtils;

/**
 * Translator for updating the asm employee.
 */
public class VMKAccountImportASMEmployeeTranslator extends AbstractSpecialValueTranslator {
    private static final String MODIFIER_NAME_ADAPTER = "adapter";
    private static final String DEFAULT_IMPORT_ADAPTER_NAME = "defaultVMKAccountImportASMEmployeeAdapter";
    private VMKAccountImportASMEmployeeAdapter vmkAccountImportASMEmployeeAdapter;

    @Override
    public void init(final SpecialColumnDescriptor columnDescriptor) {
        String beanName = columnDescriptor.getDescriptorData().getModifier(MODIFIER_NAME_ADAPTER);
        if (StringUtils.isBlank(beanName)) {
            beanName = DEFAULT_IMPORT_ADAPTER_NAME;
        }
        this.vmkAccountImportASMEmployeeAdapter = (VMKAccountImportASMEmployeeAdapter) Registry.getApplicationContext().getBean(beanName);
    }

    @Override
    public void performImport(final String cellValue, final Item processedItem) {
        this.vmkAccountImportASMEmployeeAdapter.performImport(cellValue, processedItem);
    }

    /**
     * @param vmkAccountImportASMEmployeeAdapter the vmkAccountImportASMEmployeeAdapter to set
     */
    public void setVMKAccountImportASMEmployeeAdapter(final VMKAccountImportASMEmployeeAdapter vmkAccountImportASMEmployeeAdapter) {
        this.vmkAccountImportASMEmployeeAdapter = vmkAccountImportASMEmployeeAdapter;
    }
}
