package com.vanmarcke.hotfolder.strategies.media.impl;

import com.vanmarcke.hotfolder.data.MediaFactoryParameter;
import com.vanmarcke.hotfolder.resourcespace.enums.ResourceType;
import com.vanmarcke.hotfolder.resourcespace.enums.Size;
import com.vanmarcke.hotfolder.resourcespace.service.VMKResourceSpaceService;
import com.vanmarcke.hotfolder.services.VMKMediaService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaFolderModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static java.lang.String.format;

/**
 * The {@code DefaultVMKMediaContainerFactoryStrategy} class is used to create or update {@link MediaContainerModel} instances.
 *
 * @author Taki Korovessis, Christiaan Janssen
 * @since 29/10/2019
 */
public class DefaultVMKMediaContainerFactoryStrategy extends AbstractVMKMediaFactoryStrategy<MediaContainerModel> {

    private static final String FORMAT_MULTIPLE_SIZED_MEDIAS = "%s_%s";

    /**
     * Creates a new instance of the {@link DefaultVMKMediaContainerFactoryStrategy} class.
     *
     * @param modelService       the model service
     * @param integrationService the integration service
     * @param mediaService       the media service
     */
    public DefaultVMKMediaContainerFactoryStrategy(ModelService modelService,
                                                   VMKResourceSpaceService integrationService,
                                                   VMKMediaService mediaService) {
        super(modelService, integrationService, mediaService);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MediaContainerModel create(MediaFactoryParameter parameter) {
        MediaContainerModel container = getOrCreateMediaContainer(parameter.getCatalogVersion(), parameter.getQualifier());

        if (hasBeenUpdatedRecently(container.getExternalID(), container.getModifiedtime())) {
            return container;
        }

        deleteResource(container.getExternalID());

        File file = parameter.getFile();
        String name = StringUtils.deleteWhitespace(file.getName());
        String extension = FilenameUtils.getExtension(name);

        String externalID = integrationService.create(file, FilenameUtils.getBaseName(name), ResourceType.valueOf(parameter.getType()));
        MediaFolderModel folder = mediaService.getOrCreateMediaFolder(parameter.getFolderQualifier());

        List<MediaModel> medias = getMediaModels(parameter, name, extension, externalID, folder);

        container.setExternalID(externalID);
        container.setMedias(medias);
        modelService.save(container);

        return container;
    }

    /**
     * Gets or creates a {@link MediaContainerModel} instance for the given {@code catalogVersion} and {@code qualifier}.
     *
     * @param catalogVersion the catalog version
     * @param qualifier      the qualifier
     * @return the {@link MediaContainerModel} instance
     */
    private MediaContainerModel getOrCreateMediaContainer(CatalogVersionModel catalogVersion, String qualifier) {
        MediaContainerModel mediaContainer;
        try {
            mediaContainer = mediaService.getMediaContainer(catalogVersion, qualifier);
        } catch (UnknownIdentifierException e) {
            mediaContainer = modelService.create(MediaContainerModel.class);
            mediaContainer.setQualifier(qualifier);
            mediaContainer.setCatalogVersion(catalogVersion);
        }
        return mediaContainer;
    }

    /**
     * Returns the {@link MediaModel} instances for the given {@code parameter}, {@code fileName}, {@code extension},
     * {@code externalID}, and {@code folder} and all applicable sizes.
     *
     * @param parameter  the parameter
     * @param fileName   the file name
     * @param extension  the extension
     * @param externalID the external ID
     * @param folder     the folder
     * @return the {@link MediaModel} instances
     */
    private List<MediaModel> getMediaModels(MediaFactoryParameter parameter, String fileName, String extension, String externalID, MediaFolderModel folder) {
        List<MediaModel> medias = new ArrayList<>(Size.values().length);
        for (Size size : Size.values()) {
            medias.add(getMedia(parameter, fileName, extension, externalID, folder, size));
        }
        return medias;
    }

    /**
     * Returns the {@link MediaModel} instance for the given {@code parameter}, {@code fileName}, {@code extension},
     * {@code externalID}, {@code folder}, and {@code size}.
     *
     * @param parameter  the parameter
     * @param fileName   the file name
     * @param extension  the extension
     * @param externalID the external ID
     * @param folder     the folder
     * @param size       the size
     * @return the {@link MediaModel} instance
     */
    private MediaModel getMedia(MediaFactoryParameter parameter, String fileName, String extension, String externalID, MediaFolderModel folder, Size size) {
        MediaModel media = getOrCreateMedia(parameter.getCatalogVersion(), format(FORMAT_MULTIPLE_SIZED_MEDIAS, parameter.getQualifier(), size.name()));
        media.setExternalID(externalID);
        media.setFolder(folder);
        media.setRealFileName(fileName);
        media.setMediaFormat(mediaService.getOrCreateMediaFormat(size.name()));
        media.setURL(integrationService.getPath(externalID, size, extension));

        modelService.save(media);
        return media;
    }
}