package com.vanmarcke.hotfolder.strategies.impl;

import com.vanmarcke.hotfolder.data.MediaAttributeType;
import com.vanmarcke.hotfolder.data.MediaData;
import com.vanmarcke.hotfolder.resourcespace.enums.Size;
import com.vanmarcke.hotfolder.strategies.AbstractVMKMediaImportStrategy;
import com.vanmarcke.hotfolder.utils.VMKMediaImportUtils;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaModel;

import java.util.Collections;
import java.util.Set;

/**
 * The {@code AbstractVMKCategoryMediaImportStrategy} class contains common business logic to import category media.
 *
 * @author Christiaan Janssen, Tom van den Berg
 * @since 03-04-2020
 */
public class VMKCategoryMediaImportStrategy extends AbstractVMKMediaImportStrategy<CategoryModel> {

    /**
     * {@inheritDoc}
     */
    @Override
    public Set<CategoryModel> getItems(CatalogVersionModel catalogVersion, MediaData media) {
        return itemProvider.get(catalogVersion, media.getCategories());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void update(MediaContainerModel container, CategoryModel category, String attribute, MediaAttributeType attributeType) {
        if (MediaAttributeType.PACKSHOT.equals(attributeType) && 0 == getSequenceId(container)) {
            MediaModel media = VMKMediaImportUtils.getMediaForSize(container, Size.PREVIEW);
            modelService.setAttributeValue(category, CategoryModel.PICTURE, media);
            modelService.setAttributeValue(category, CategoryModel.LOGO, Collections.singletonList(media));
            modelService.setAttributeValue(category, CategoryModel.THUMBNAIL, VMKMediaImportUtils.getMediaForSize(container, Size.THUMBNAIL_VANMARCKE));
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void remove(MediaContainerModel container, CategoryModel category, String attribute, MediaAttributeType attributeType) {
        if (MediaAttributeType.PACKSHOT.equals(attributeType) && 0 == getSequenceId(container)) {
            modelService.setAttributeValue(category, CategoryModel.PICTURE, null);
            modelService.setAttributeValue(category, CategoryModel.LOGO, null);
            modelService.setAttributeValue(category, CategoryModel.THUMBNAIL, null);
        }
    }
}