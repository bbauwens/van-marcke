package com.vanmarcke.hotfolder.event.handler.impl;

import com.vanmarcke.hotfolder.event.handler.VMKAfterSaveEventHandler;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.servicelayer.exceptions.ModelLoadingException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.tx.AfterSaveEvent;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import java.util.Collection;

/**
 * The {@link VMKAbstractAfterSaveHandler} class serves as a starting point to handle after save events.
 *
 * @author Christiaan Janssen
 * @since 31-08-2020
 */
public abstract class VMKAbstractAfterSaveHandler<T extends ItemModel> implements VMKAfterSaveEventHandler<T> {

    private static final Logger LOG = Logger.getLogger(VMKAbstractAfterSaveHandler.class);

    private final Class<T> clazz;

    protected ModelService modelService;

    /**
     * Creates a new instance of the {@link VMKAbstractAfterSaveHandler} class.
     *
     * @param clazz the class of the item type to handle
     */
    public VMKAbstractAfterSaveHandler(Class<T> clazz) {
        this.clazz = clazz;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void afterSave(Collection<AfterSaveEvent> collection) {
        try {
            for (AfterSaveEvent event : collection) {
                ItemModel item;
                if (AfterSaveEvent.CREATE == event.getType() || AfterSaveEvent.UPDATE == event.getType()) {
                    item = modelService.get(event.getPk());
                    if (clazz.isInstance(item)) {
                        handle(event, (T) item);
                    }
                }
            }
        } catch (ModelLoadingException e) {
            // Do nothing
        } catch (RuntimeException e) {
            LOG.error(e.getMessage(), e);
        }
    }

    /**
     * Sets the given {@code modelService}.
     *
     * @param modelService the model service
     */
    @Required
    public void setModelService(ModelService modelService) {
        this.modelService = modelService;
    }
}
