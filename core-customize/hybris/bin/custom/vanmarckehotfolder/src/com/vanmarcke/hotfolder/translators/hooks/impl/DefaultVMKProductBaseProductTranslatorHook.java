package com.vanmarcke.hotfolder.translators.hooks.impl;

import com.vanmarcke.hotfolder.translators.hooks.VMKImpexTranslatorHook;
import de.hybris.platform.core.model.product.ProductModel;

public class DefaultVMKProductBaseProductTranslatorHook implements VMKImpexTranslatorHook<ProductModel> {

    private final String baseProduct;

    public DefaultVMKProductBaseProductTranslatorHook(final String baseProduct) {
        this.baseProduct = baseProduct;
    }

    @Override
    public String translate(final String valueExpr, final ProductModel toItem) {
        return toItem == null ? baseProduct : IGNORE;
    }
}