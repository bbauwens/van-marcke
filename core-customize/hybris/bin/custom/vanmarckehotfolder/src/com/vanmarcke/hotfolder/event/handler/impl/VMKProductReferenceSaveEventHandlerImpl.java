package com.vanmarcke.hotfolder.event.handler.impl;

import de.hybris.platform.catalog.model.ProductReferenceModel;
import de.hybris.platform.tx.AfterSaveEvent;
import org.apache.commons.lang3.BooleanUtils;

/**
 * The {@link VMKProductReferenceSaveEventHandlerImpl} class implements the methods to handle after save events.
 *
 * @author Christiaan Janssen
 * @since 31-08-2020
 */
public class VMKProductReferenceSaveEventHandlerImpl extends VMKAbstractAfterSaveHandler<ProductReferenceModel> {

    /**
     * Creates a new instance of the {@link VMKProductReferenceSaveEventHandlerImpl} class.
     */
    public VMKProductReferenceSaveEventHandlerImpl() {
        super(ProductReferenceModel.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void handle(AfterSaveEvent event, ProductReferenceModel item) {
        if (BooleanUtils.isFalse(item.getActive())) {
            modelService.remove(item);
        }
    }
}
