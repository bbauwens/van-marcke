package com.vanmarcke.storefront.security.cookie;

import de.hybris.platform.site.BaseSiteService;
import org.apache.commons.lang.StringUtils;

/**
 * Cookie generator for the Cart Restoration
 */
public class CartRestoreCookieGenerator extends EnhancedCookieGenerator {

    private BaseSiteService baseSiteService;

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCookieName() {
        return StringUtils.deleteWhitespace(baseSiteService.getCurrentBaseSite().getUid()) + "-cart";
    }

    /**
     * Sets the given {@code baseSiteService}.
     *
     * @param baseSiteService the base site service
     */
    public void setBaseSiteService(BaseSiteService baseSiteService) {
        this.baseSiteService = baseSiteService;
    }
}
