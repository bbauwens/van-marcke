package com.vanmarcke.storefront.tags;

import de.hybris.platform.acceleratorstorefrontcommons.tags.Functions;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.model.contents.components.CMSLinkComponentModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.apache.commons.lang3.StringUtils;

/**
 * Custom Functions class
 */
public class VMKFunctions extends Functions {

    /**
     * Code copied from Functions class to add extra method for Localized Url's (see private method below)
     *
     * @param component            the {@link CMSLinkComponentModel}
     * @param productUrlConverter  {@link Converter<ProductModel, ProductData>}
     * @param categoryUrlConverter {@link Converter<CategoryModel, CategoryData>}
     * @return the correct url
     */
    public static String getUrlForCMSLinkComponent(final CMSLinkComponentModel component,
                                                   final Converter<ProductModel, ProductData> productUrlConverter,
                                                   final Converter<CategoryModel, CategoryData> categoryUrlConverter) {
        // Try to get the label for the content page
        ContentPageModel contentPage = component.getContentPage();
        if (contentPage != null) {
            if (contentPage.isHomepage()) {
                return DEFAULT_HOMEPAGE_URL;
            } else {
                return contentPage.getLabel();
            }
        }

        String categoryUrl = getCategoryUrl(component, categoryUrlConverter);
        if (categoryUrl != null) {
            return categoryUrl;
        }

        // Try to get the product and build a URL to the product
        ProductModel product = component.getProduct();
        if (product != null) {
            return convertWithProperConverter(productUrlConverter, product);
        }

        //Added this custom method
        String localizedUrl = getLocalizedUrlFromComponent(component);
        if (localizedUrl != null) {
            return localizedUrl;
        }

        // Try to get the URL from the component
        String url = component.getUrl();
        if (url != null && !url.isEmpty()) {
            return url;
        }

        return null;
    }

    /**
     * Get the localized url from the current component
     *
     * @param component the given component
     * @return the localized String if present of null
     */
    private static String getLocalizedUrlFromComponent(CMSLinkComponentModel component) {
        return StringUtils.isNotBlank(component.getLocalizedUrl()) ? component.getLocalizedUrl() : null;
    }
}
