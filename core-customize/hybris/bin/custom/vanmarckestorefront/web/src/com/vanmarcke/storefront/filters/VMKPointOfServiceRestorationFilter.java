package com.vanmarcke.storefront.filters;

import com.vanmarcke.facades.pos.VMKPointOfServiceFacade;
import com.vanmarcke.facades.storesession.VMKStoreSessionFacade;
import de.hybris.platform.commercefacades.storelocator.data.PointOfServiceData;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import org.apache.commons.collections.CollectionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

/**
 * The {@link VMKPointOfServiceRestorationFilter} class is used to restore the point of service in the session.
 *
 * @author Christiaan Janssen
 * @since 29-04-2021
 */
public class VMKPointOfServiceRestorationFilter extends OncePerRequestFilter {

    private static final Logger LOG = LogManager.getLogger();

    private final UserService userService;
    private final VMKStoreSessionFacade storeSessionFacade;
    private final VMKPointOfServiceFacade vmkPointOfServiceFacade;
    private final BaseSiteService baseSiteService;

    /**
     * Creates a new instance of the {@link VMKPointOfServiceRestorationFilter} class.
     *
     * @param userService        the user service
     * @param storeSessionFacade the store session facade
     */
    public VMKPointOfServiceRestorationFilter(final UserService userService,
                                              final VMKStoreSessionFacade storeSessionFacade,
                                              VMKPointOfServiceFacade vmkPointOfServiceFacade,
                                              BaseSiteService baseSiteService) {
        this.userService = userService;
        this.storeSessionFacade = storeSessionFacade;
        this.vmkPointOfServiceFacade = vmkPointOfServiceFacade;
        this.baseSiteService = baseSiteService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void doFilterInternal(final HttpServletRequest request,
                                    final HttpServletResponse response,
                                    final FilterChain filterChain) throws ServletException, IOException {
        restorePointOfService();
        filterChain.doFilter(request, response);
    }

    /**
     * Restores the point of service in the session.
     */
    private void restorePointOfService() {
        if (baseSiteService.getCurrentBaseSite().getChannel().equals(SiteChannel.DIY)) {
            return;
        }

        final UserModel currentUser = userService.getCurrentUser();

        if (userService.isAnonymousUser(currentUser)) {
            if (Objects.isNull(storeSessionFacade.getCurrentStore())) {
                final List<PointOfServiceData> pointsOfService = getPointsOfService();
                if (CollectionUtils.isEmpty(pointsOfService)) {
                    return;
                }

                storeSessionFacade.setCurrentStore(getDefaultPos(pointsOfService));
            }
        } else {
            final String posName;
            final PointOfServiceModel sessionPOS = currentUser.getSessionStore();

            if (sessionPOS != null) {
                final List<PointOfServiceData> pointsOfService = getPointsOfService();
                final boolean isValidPosForCurrentStore = pointsOfService.stream()
                        .anyMatch(pointOfServiceData -> pointOfServiceData.getName().equals(sessionPOS.getName()));

                if (!isValidPosForCurrentStore) {
                    posName = getDefaultPos(pointsOfService);

                    LOG.debug("Current user's session POS is not valid, falling back to default POS [current user: {} | invalid session PSO: {} | default POS: {}]", currentUser::getUid, sessionPOS::getName, () -> posName);
                } else {
                    posName = sessionPOS.getName();
                }
            } else {
                posName = vmkPointOfServiceFacade.getFallbackPOS().getName();
            }
            storeSessionFacade.setCurrentStore(posName);
        }
    }

    /**
     * Returns the points of service.
     *
     * @return the points of service
     */
    private List<PointOfServiceData> getPointsOfService() {
        List<PointOfServiceData> pos;
        try {
            pos = storeSessionFacade.getAllStores();
        } catch (final Exception e) {
            LOG.debug("Exception when searching for all available stores", e);
            pos = Collections.emptyList();
        }
        return pos;
    }

    /**
     * Returns the closest point of service as the default point of service.
     *
     * @param pointsOfService the points of service
     * @return the default point of service
     */
    private String getDefaultPos(final List<PointOfServiceData> pointsOfService) {
        pointsOfService.sort(Comparator.comparing(PointOfServiceData::getDistanceKm, Comparator.nullsLast(Comparator.naturalOrder())));
        return pointsOfService.get(0).getName();
    }
}
