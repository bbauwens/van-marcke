<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/template" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<spring:htmlEscape defaultHtmlEscape="true"/>
<spring:url value="/import/csv/example.xlsx" var="exampleCSVWishListLink" htmlEscape="false"/>
<spring:url value="/import/csv/wish-list" var="importCSVWishListLink" htmlEscape="false"/>
<spring:url value="/my-account/saved-baskets" var="wishListsLink" htmlEscape="false"/>
<template:page pageTitle="${pageTitle}">
    <div class="container vmb-wish-list-import">
        <spring:url value="/my-account/saved-baskets" var="wishListsUrl" htmlEscape="false"/>
        <div class="back-link">
            <a href="${fn:escapeXml(wishListsUrl)}">
                <span class="glyphicon glyphicon-chevron-left"></span>
            </a>
            <span class="label">
		        <spring:theme code="import.csv.savedCart.title"/>
	        </span>
        </div>
        <div class="account-section">
            <p>
                <spring:theme code="import.csv.savedCart.filesNote"/>
            </p>
            <div class="account-section-content import-csv__content">
                <form:form modelAttribute="importCSVSavedCartForm" id="importCSVSavedCartForm"
                           enctype="multipart/form-data" method="post"
                           action="${importCSVWishListLink}" class="import-csv__form">
                </form:form>
                <div class="well well-quaternary well-md import-csv__well">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 import-csv__file-spec">
                            <b><spring:theme code="import.csv.savedCart.fileContentNote"/></b>
                            <ul>
                                <li class="import-csv__file-spec-item">
                                    <spring:theme code="import.csv.savedCart.fileContent"/>
                                </li>
                                <li class="import-csv__file-spec-item">
                                    <spring:theme code="import.csv.savedCart.fileHeading"/>
                                </li>
                                <li class="import-csv__file-spec-item">
                                    <spring:theme code="import.csv.savedCart.fileConstraint"/>&nbsp;<format:bytes
                                        bytes="${csvFileMaxSize}"/>
                                </li>
                            </ul>
                            <div class="form-group">
                                <a href="${exampleCSVWishListLink}" download class="btn btn-primary btn-small">
                                    <spring:theme code="import.csv.savedCart.example"/>
                                </a>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 import-csv__file-upload">
                            <b><spring:theme code="import.csv.savedCart.selectFile"/></b>
                            <div class="form-group file-upload js-file-upload">
                                <div class="file-upload__wrapper btn btn-primary btn-small" id="chooseFileButton">
                                    <span><spring:theme code="import.csv.savedCart.chooseFile"/></span>
                                    <input type="file" id="csvFile" name="csvFile"
                                           class="file-upload__input js-file-upload__input"
                                           accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-excel"
                                           data-file-max-size="${csvFileMaxSize}"/>
                                </div>
                                <span class="file-upload__file-name js-file-upload__file-name">
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="import-csv__actions js-import-csv">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-3 pull-right">
                            <button class="btn btn-default" type="submit" id="importButton">
                                <span class="glyphicon glyphicon-import"></span>&nbsp;<spring:theme
                                    code="import.csv.savedCart.import"/>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div style="display: none">
            <span id="import-csv-success-message"><spring:theme code="import.csv.savedCart.success"
                                                                arguments="${wishListsLink}"
                                                                htmlEscape="false"/></span>
            <span id="import-csv-upload-message"><spring:theme code="import.csv.savedCart.uploadStarted"/></span>
            <span id="import-csv-generic-error-message"><spring:theme code="import.csv.savedCart.genericError"/></span>
            <span id="import-csv-file-max-size-exceeded-error-message"><spring:theme
                    code="import.csv.savedCart.fileMaxSizeExceeded"/></span>
            <span id="import-xls-file-xls-required"><spring:theme code="import.xls.savedCart.fileExcelRequired"/></span>
            <span id="import-csv-no-file-chosen-error-message"><spring:theme code="import.csv.savedCart.noFile"/></span>
        </div>
        <common:globalMessagesTemplates/>
    </div>
</template:page>