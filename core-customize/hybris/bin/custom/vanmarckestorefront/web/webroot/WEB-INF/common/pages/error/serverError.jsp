<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page trimDirectiveWhitespaces="true" %>
<html>
<head>
    <meta charset="utf-8">
    <title>Van Marcke - Server Error</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <style>
        h1, h2, p {
            color: #113156;
            padding: 10px;
        }

        .space-top {
            margin-top: 50px
        }

        .vm-col {
            margin-left: 3%;
            width: 41.66667%;
            float: left
        }

        .text-large {
            font-size: 60px;
            font-weight: 700
        }

        .text-center {
            text-align: center
        }


    </style>
</head>

<body>
<header class="page-container page-container-responsive space-top-4">

</header>

<div class="page-container page-container-responsive">
    <div class="row space-top">
        <div class="vm-col text-center">
            <img src=/_ui/responsive/theme-vanmarckeblue/images/VM_BLUE.svg width="313" height="428"
                 alt="Van Marcke logo">
        </div>
        <div class="vm-col content-container">
            <h1 class="text-large">Oops!</h1>
            <h2>Error code: 500</h2>
            <div>
                <p>Er is een fout opgetreden en we werken eraan om het probleem op te lossen! Kom snel terug om jouw
                    bestelling te plaatsen! Heb je een dringende vraag? Contacteer dan onze Customer Care via <a
                            href="mailto:blue@vanmarcke.be">e-mail</a> of telefoon: <a
                            href="tel:0800/30.641">0800/30.641</a> (bereikbaar op werkdagen van 9u tot 17u).</p>
            </div>
            <div>
                <p>Une erreur s'est produite et nous travaillons à résoudre le problème! Revenez bientôt pour passer
                    votre commande! Avez-vous une question urgente? Contactez notre service clientèle par <a
                            href="mailto:blue@vanmarcke.be">e-mail</a> ou par
                    téléphone: <a href="tel:0800/30.641">0800/30.641</a> (joignable les jours ouvrables de 9h à 17h).
                </p>
            </div>
            <div>
                <p>An error has occurred and we're working to fix the problem! Come back soon to place your order! Do
                    you have an urgent question? Contact our Customer Care via <a
                            href="mailto:blue@vanmarcke.be">e-mail</a> or telephone: <a
                            href="tel:0800/30.641">0800/30.641</a> (available on working days from 9 a.m. to 5 p.m.).
                </p>
            </div>

        </div>

    </div>
</div>
</body>

</html>
