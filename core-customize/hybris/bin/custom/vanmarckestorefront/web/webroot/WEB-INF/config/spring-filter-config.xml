<?xml version="1.0" encoding="UTF-8"?>
<!--
 [y] hybris Platform

 Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.

 This software is the confidential and proprietary information of SAP
 ("Confidential Information"). You shall not disclose such Confidential
 Information and shall use it only in accordance with the terms of the
 license agreement you entered into with SAP.
-->
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:util="http://www.springframework.org/schema/util"
       xmlns:context="http://www.springframework.org/schema/context"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
		http://www.springframework.org/schema/beans/spring-beans.xsd
		http://www.springframework.org/schema/util
		http://www.springframework.org/schema/util/spring-util.xsd
		http://www.springframework.org/schema/context
        http://www.springframework.org/schema/context/spring-context.xsd">

    <context:annotation-config/>

    <!-- tenant scoped filter -->

    <bean id="storefrontTenantFilterChain" class="com.vanmarcke.storefront.filters.UrlPathFilter">
        <property name="defaultFilter" ref="storefrontTenantDefaultFilterChain"/>
        <property name="urlPathHelper">
            <bean class="org.springframework.web.util.UrlPathHelper"/>
        </property>
        <property name="urlPathMapping">
            <map>
                <entry key="/integration/" value-ref="integrationTenantFilterChain"/>
            </map>
        </property>
    </bean>

    <alias name="defaultStorefrontTenantDefaultFilterChainList" alias="storefrontTenantDefaultFilterChainList"/>
    <util:list id="defaultStorefrontTenantDefaultFilterChainList">
        <!-- filter for handling session failover -->
        <ref bean="hybrisSpringSessionFilter"/>

        <!-- generic platform filters -->
        <ref bean="log4jFilter"/>
        <ref bean="storefrontSessionFilter"/>
        <ref bean="accMediaFilter"/>
        <ref bean="addOnDevelopmentFilter"/>

        <!-- filter to log the current request -->
        <ref bean="requestLoggerFilter"/>

        <!-- filter to setup the cms integration -->
        <ref bean="cmsSiteFilter"/>

        <!-- filter to set language cookie-->
        <ref bean="eliLanguageFilter"/>

        <!-- filter to initialize the storefront -->
        <ref bean="storefrontFilter"/>

        <!-- filter to handle url encoding attributes -->
        <ref bean="urlEncoderFilter"/>

        <!-- filter to handle multipart file upload -->
        <ref bean="fileUploadFilter"/>

        <!-- Security -->
        <ref bean="springSecurityFilterChain"/>

        <!-- Elision Saml filter to login user -->
        <ref bean="eliSamlSingleSignOnFilter"/>

        <!-- filter to log out guest user if he/she attempts to access a page outside of checkout flow -->
        <ref bean="anonymousCheckoutFilter"/>

        <!-- filter to handle pricing -->
        <ref bean="vmkPriceListFilter"/>

        <!-- filter to restore items in cart -->
        <ref bean="cartRestorationFilter"/>

        <!-- filter to decide the checkout mode for the cart -->
        <ref bean="vmkCartCheckoutModeDecisionFilter"/>

        <!-- filter to restore customer preferred location -->
        <ref bean="customerLocationRestorationFilter"/>

        <!-- filter to restore customer preferred store -->
        <ref bean="vmkPointOfServiceRestorationFilter"/>
    </util:list>

    <bean id="storefrontTenantDefaultFilterChain" class="de.hybris.platform.servicelayer.web.PlatformFilterChain">
        <constructor-arg>
            <ref bean="storefrontTenantDefaultFilterChainList"/>
        </constructor-arg>
    </bean>

    <alias name="defaultIntegrationTenantFilterChainList" alias="integrationTenantFilterChainList"/>
    <util:list id="defaultIntegrationTenantFilterChainList">
        <!-- generic platform filters -->
        <ref bean="storefrontSessionFilter"/>
        <ref bean="addOnDevelopmentFilter"/>
        <!-- filter to log the current request -->
        <ref bean="requestLoggerFilter"/>
    </util:list>

    <bean id="integrationTenantFilterChain" class="de.hybris.platform.servicelayer.web.PlatformFilterChain">
        <constructor-arg>
            <ref bean="integrationTenantFilterChainList"/>
        </constructor-arg>
    </bean>

    <bean id="accMediaFilter" class="de.hybris.platform.servicelayer.web.WebAppMediaFilter">
        <property name="mediaPermissionService" ref="mediaPermissionService"/>
        <property name="modelService" ref="modelService"/>
        <property name="userService" ref="userService"/>
        <property name="mediaService" ref="mediaService"/>
        <property name="addContextPath" value="true"/>
    </bean>

    <bean id="urlEncoderFilter" class="com.vanmarcke.storefront.filters.UrlEncoderFilter">
        <property name="urlEncoderFacade" ref="urlEncoderFacade"/>
        <property name="sessionService" ref="sessionService"/>
    </bean>

    <bean id="storefrontSessionFilter" class="de.hybris.platform.servicelayer.web.SessionFilter">
        <property name="sessionService" ref="sessionService"/>
    </bean>

    <bean id="addOnDevelopmentFilter" class="com.vanmarcke.storefront.filters.AcceleratorAddOnFilter">
        <property name="configurationService" ref="configurationService"/>
    </bean>

    <bean id="requestLoggerFilter" class="com.vanmarcke.storefront.filters.RequestLoggerFilter"/>

    <bean id="cmsSiteFilter" class="com.vanmarcke.storefront.filters.cms.CMSSiteFilter">
        <property name="previewDataModelUrlResolver" ref="previewDataModelUrlResolver"/>
        <property name="cmsSiteService" ref="cmsSiteService"/>
        <property name="cmsPreviewService" ref="cmsPreviewService"/>
        <property name="baseSiteService" ref="baseSiteService"/>
        <property name="commerceCommonI18NService" ref="commerceCommonI18NService"/>
        <property name="sessionService" ref="sessionService"/>
        <property name="contextInformationLoader" ref="contextInformationLoader"/>
        <property name="cmsPageContextService" ref="cmsPageContextService"/>
        <property name="siteChannelValidationStrategy" ref="acceleratorSiteChannelValidationStrategy"/>
        <property name="configurationService" ref="configurationService"/>
    </bean>

    <bean id="acceleratorSiteChannelValidationStrategy"
          class="de.hybris.platform.acceleratorservices.site.strategies.impl.DefaultSiteChannelValidationStrategy">
        <property name="supportedSiteChannels" ref="vmkAcceleratorSiteChannels"/>
    </bean>

    <alias name="b2cAcceleratorSiteChannels" alias="acceleratorSiteChannels"/>
    <util:set id="b2cAcceleratorSiteChannels" value-type="de.hybris.platform.commerceservices.enums.SiteChannel">
        <ref bean="SiteChannel.B2C"/>
    </util:set>

    <alias name="defaultRefererExcludeUrlSet" alias="refererExcludeUrlSet"/>
    <util:set id="defaultRefererExcludeUrlSet" value-type="java.lang.String">
        <value>/**/cart/export</value>
    </util:set>

    <bean id="storefrontFilter" class="com.vanmarcke.storefront.filters.StorefrontFilter">
        <property name="storeSessionFacade" ref="storeSessionFacade"/>
        <property name="browseHistory" ref="browseHistory"/>
        <property name="refererExcludeUrlSet" ref="refererExcludeUrlSet"/>
        <property name="pathMatcher" ref="defaultPathMatcher"/>
    </bean>

    <bean id="cartRestorationFilter" class="com.vanmarcke.storefront.filters.CartRestorationFilter">
        <constructor-arg name="cartRestoreCookieGenerator" ref="cartRestoreCookieGenerator"/>
        <constructor-arg name="baseSiteService" ref="baseSiteService"/>
        <constructor-arg name="cartService" ref="cartService"/>
        <constructor-arg name="sessionService" ref="sessionService"/>
        <constructor-arg name="cartFacade" ref="cartFacade"/>
        <constructor-arg name="userFacade" ref="userFacade"/>
    </bean>

    <bean id="anonymousCheckoutFilter" class="com.vanmarcke.storefront.filters.AnonymousCheckoutFilter">
        <property name="guestCheckoutCartCleanStrategy" ref="guestCheckoutCartCleanStrategy"/>
    </bean>

    <bean id="customerLocationRestorationFilter"
          class="com.vanmarcke.storefront.filters.CustomerLocationRestorationFilter">
        <property name="customerLocationFacade" ref="customerLocationFacade"/>
        <property name="customerLocationCookieGenerator" ref="customerLocationCookieGenerator"/>
    </bean>

    <alias name="defaultFileUploadUrlFilterMappings" alias="fileUploadUrlFilterMappings"/>
    <util:map id="defaultFileUploadUrlFilterMappings" key-type="java.lang.String"
              value-type="org.springframework.web.multipart.support.MultipartFilter">
        <entry key="/import/csv/*" value-ref="importCSVMultipartFilter"/>
    </util:map>

    <bean id="fileUploadFilter" class="com.vanmarcke.storefront.filters.FileUploadFilter">
        <property name="urlFilterMapping">
            <ref bean="fileUploadUrlFilterMappings"/>
        </property>
        <property name="pathMatcher" ref="defaultPathMatcher"/>
    </bean>

    <alias name="defaultSessionCookieGenerator" alias="sessionCookieGenerator"/>
    <bean id="defaultSessionCookieGenerator" class="com.vanmarcke.storefront.security.cookie.EnhancedCookieGenerator">
        <property name="cookieSecure" value="true"/>
        <property name="cookieName" value="JSESSIONID"/>
        <property name="cookieMaxAge" value="-1"/>
        <property name="useDefaultPath" value="false"/>
        <property name="httpOnly" value="true"/>
    </bean>

    <alias name="defaultCartRestoreCookieGenerator" alias="cartRestoreCookieGenerator"/>
    <bean id="defaultCartRestoreCookieGenerator"
          class="com.vanmarcke.storefront.security.cookie.CartRestoreCookieGenerator">
        <property name="cookieSecure" value="true"/>
        <property name="cookieMaxAge" value="360000000"/>
        <property name="useDefaultPath" value="true"/>
        <property name="httpOnly" value="true"/>
        <property name="baseSiteService" ref="baseSiteService"/>
    </bean>

    <alias name="defaultCustomerLocationCookieGenerator" alias="customerLocationCookieGenerator"/>
    <bean id="defaultCustomerLocationCookieGenerator"
          class="com.vanmarcke.storefront.security.cookie.CustomerLocationCookieGenerator">
        <property name="cookieSecure" value="true"/>
        <property name="cookieMaxAge" value="360000000"/>
        <property name="useDefaultPath" value="false"/>
        <property name="httpOnly" value="true"/>
        <property name="baseSiteService" ref="baseSiteService"/>
    </bean>

    <alias name="defaultCookieNotificationCookieGenerator" alias="cookieNotificationCookieGenerator"/>
    <bean id="defaultCookieNotificationCookieGenerator"
          class="com.vanmarcke.storefront.security.cookie.CookieNotificationCookieGenerator">
        <property name="cookieSecure" value="true"/>
        <property name="cookieMaxAge" value="360000000"/>
        <property name="useDefaultPath" value="true"/>
    </bean>

    <alias name="defaultPLPListViewCookieGenerator" alias="plpListViewCookieGenerator"/>
    <bean id="defaultPLPListViewCookieGenerator"
          class="com.vanmarcke.storefront.security.cookie.EnhancedCookieGenerator">
        <property name="cookieName" value="view-type"/>
        <property name="cookieSecure" value="true"/>
        <property name="cookieMaxAge" value="360000000"/>
        <property name="useDefaultPath" value="true"/>
        <property name="httpOnly" value="true"/>
    </bean>


    <bean id="importCSVMultipartFilter" class="org.springframework.web.multipart.support.MultipartFilter">
        <property name="multipartResolverBeanName" value="importCSVMultipartResolver"/>
    </bean>

    <bean id="importCSVMultipartResolver" class="org.springframework.web.multipart.commons.CommonsMultipartResolver">
        <property name="maxUploadSize"
                  value="#{configurationService.configuration.getProperty('import.csv.max.upload.size.bytes')}"/>
    </bean>

    <bean id="vmkPointOfServiceRestorationFilter"
          class="com.vanmarcke.storefront.filters.VMKPointOfServiceRestorationFilter">
        <constructor-arg name="userFacade" ref="userFacade"/>
        <constructor-arg name="storeSessionFacade" ref="storeSessionFacade"/>
        <constructor-arg name="vmkPointOfServiceFacade" ref="vmkPointOfServiceFacade"/>
        <constructor-arg name="baseSiteService" ref="baseSiteService"/>
    </bean>

    <bean id="vmkPriceListFilter" class="com.vanmarcke.storefront.filters.VMKPriceListFilter">
        <constructor-arg name="commerceGroupService" ref="vmkCommerceGroupService"/>
        <constructor-arg name="sessionService" ref="sessionService"/>
    </bean>

    <bean id="vmkCartCheckoutModeDecisionFilter"
          class="com.vanmarcke.storefront.filters.VMKCartCheckoutModeDecisionFilter">
        <constructor-arg name="cartFacade" ref="vmkCartFacade"/>
    </bean>
</beans>
