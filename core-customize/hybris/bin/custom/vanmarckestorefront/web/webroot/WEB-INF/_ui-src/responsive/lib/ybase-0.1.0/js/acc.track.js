var trackingAllowed = JSON.parse('${trackingAllowed}');

ACC.track = {
    trackAddToCart: function (productCode, quantity, cartData) {
        if (trackingAllowed === 'true') {
            window.mediator.publish('trackAddToCart', {
                productCode: productCode,
                quantity: quantity,
                cartData: cartData
            });
        }

    },
    trackRemoveFromCart: function (productCode, initialCartQuantity) {
        if (trackingAllowed === 'true') {
            window.mediator.publish('trackRemoveFromCart', {
                productCode: productCode,
                initialCartQuantity: initialCartQuantity
            });
        }
    },

    trackUpdateCart: function (productCode, initialCartQuantity, newCartQuantity) {
        if (trackingAllowed === 'true') {
            window.mediator.publish('trackUpdateCart', {
                productCode: productCode,
                initialCartQuantity: initialCartQuantity,
                newCartQuantity: newCartQuantity
            });
        }
    },

    trackShowReviewClick: function (productCode) {
        if (trackingAllowed === 'true') {
            window.mediator.publish('trackShowReviewClick', {});
        }
    }

};