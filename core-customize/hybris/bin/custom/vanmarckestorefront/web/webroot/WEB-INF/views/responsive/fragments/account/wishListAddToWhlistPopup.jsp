<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>


<script id="wishListAddToWishlistSelectionTemplate" type="text/x-jsrender">
    {{for products}}
        <input type="hidden" name="productCode" value="{{:productCode}}"/>
        <input type="hidden" name="productQuantity" value="{{:quantity}}"/>
    {{/for}}
</script>


<spring:htmlEscape defaultHtmlEscape="true"/>

<spring:url var="addToWishListPostUrl" value="/my-account/saved-baskets/add" htmlEscape="false"/>
<c:set var="loc_val">
    <spring:message code="basket.save.cart.max.chars"/>
</c:set>
<c:set var="validation_error_pattern_name">
    <spring:message code="wishlist.validation-error.pattern-name"/>
</c:set>
<c:set var="validation_error_pattern_description">
    <spring:message code="wishlist.validation-error.pattern-description"/>
</c:set>
<input type="hidden" id="localized_val" name="localized_val" value="${loc_val}"/>
<input type="hidden" id="localized_error_name" name="localized_error_name" value="${validation_error_pattern_name}"/>
<input type="hidden" id="localized_error_description" name="localized_error_description" value="${validation_error_pattern_description}"/>
<div class="vmb-modal fade" id="vmbAddToWishlistWishListModal" tabindex="-1" role="dialog"
     aria-labelledby="vmbAddToWishlistWishListModalTitle">
    <form:form id="addToWishListForm" method="post" action="${addToWishListPostUrl}" cssClass="js-add-products-to-wish-list">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="vmbRestoreWishListModalTitle">
                        <spring:theme code='text.save.cart.title'/>
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="js-add-to-wishlist-product-selection"/>
                    <div class="js-add-to-wishlist-option-new">
                        <label class="control-label" for="wishListNew">
                            <input type="radio"
                                   id="wishListNew"
                                   name="addToWishlist"
                                   value="true"
                                   class="js-add-to-wishlist-option-new"
                                   checked="checked">
                            <spring:theme code="text.account.wishlist.create.new"/>
                        </label>
                        <div class="js-add-to-wishlist-option-new-body">
                            <div class="form-group">
                                <label class="control-label" for="newWishlistName">
                                    <spring:theme code="basket.save.cart.name"/>
                                    <span class="text-danger">&nbsp;*</span>
                                </label>
                                <input type="text"
                                       class="text form-control js-add-to-wishlist-option-new-name"
                                       id="newWishlistName"
                                       name="newWishlistName"
                                       maxlength="50"
                                       autocomplete="off"
                                       required>
                                <div class="js-restore-error-container help-block"></div>
                                <div class="help-block right-cartName" id="validate-error-name">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="newWishlistDescription">
                                    <spring:theme code="basket.save.cart.description"/>
                                </label>
                                <textarea class="form-control"
                                          id="newWishListDescription"
                                          name="newWishlistDescription"
                                          maxlength="255"/>
                                <div class="help-block" id="remainTextArea">
                                </div>
                                <div class="help-block right-cartName" id="validate-error-description">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="js-add-to-wishlist-option-existing">
                        <label class="control-label" for="wishListExisting">
                            <input type="radio"
                                   id="wishListExisting"
                                   name="addToWishlist"
                                   value="false"
                                   class="js-add-to-wishlist-option-existing">
                            <spring:theme code="text.account.wishlist.create.existing"/>
                        </label>
                        <div class="js-add-to-wishlist-option-existing-body" style="display: none">
                            <div class="form-group">
                                <c:forEach items="${searchPageData.results}" var="result" varStatus="status">
                                    <label for="wlname_${status.index}">
                                        <input type="radio"
                                               class="js-add-to-wishlist-option"
                                               name="wishListAddTo"
                                               value="${result.name}">
                                            ${result.name}
                                    </label>
                                    <br/>
                                </c:forEach>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal"><spring:theme
                            code="text.button.cancel"/></button>
                    <button type="submit" id="saveBasketButton" class="btn btn-primary js-add-to-wishlist-btn">
                        <spring:theme code="basket.save.cart.action.save"/>
                    </button>
                </div>
            </div>
        </div>
    </form:form>
</div>
