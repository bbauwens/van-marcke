package com.vanmarcke.storefront.filters;

import com.vanmarcke.facades.principal.user.VMKUserFacade;
import com.vanmarcke.services.model.builder.CartModelMockBuilder;
import com.vanmarcke.storefront.security.cookie.CartRestoreCookieGenerator;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commercefacades.order.data.CartRestorationData;
import de.hybris.platform.commerceservices.order.CommerceCartRestorationException;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.context.event.annotation.BeforeTestMethod;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.mockito.Mockito.*;

/**
 * The {@link CartRestorationFilterTest} class contains the unit tests for the {@link CartRestorationFilterTest} class.
 *
 * @author Christiaan Janssen
 * @since 29-04-2021
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CartRestorationFilterTest {

    private static final String COOKIE_NAME = RandomStringUtils.random(10);
    private static final String CART_GUID = RandomStringUtils.random(10);
    public static final String SESSION_CART_PARAMETER_NAME = "cart";

    @Mock
    private CartRestoreCookieGenerator cartRestoreCookieGenerator;

    @Mock
    private BaseSiteService baseSiteService;

    @Mock
    private CartService cartService;

    @Mock
    private SessionService sessionService;

    @Mock
    private CartFacade cartFacade;

    @Mock
    private VMKUserFacade userFacade;

    @Spy
    @InjectMocks
    private CartRestorationFilter filter;

    @Mock
    BaseSiteModel site1;

    @Mock
    BaseSiteModel site2;


    @Test
    public void testIsCurrentSiteOnSessionCart() {
        when(site1.getUid()).thenReturn("blue-be");
        final CartModel newCart = CartModelMockBuilder
                .aCart()
                .withSite(site1)
                .build();
        when(cartService.hasSessionCart()).thenReturn(true);
        when(baseSiteService.getCurrentBaseSite()).thenReturn(site1);
        when(cartService.getSessionCart()).thenReturn(newCart);
        when(baseSiteService.getBaseSiteForUID("blue-be")).thenReturn(site1);
        Assert.assertTrue(filter.isValidCartOnSession());
    }

    @Test
    public void testIsCurrentSiteOnSessionCart_notSameSite() {
        when(site1.getUid()).thenReturn("blue-be");
        final CartModel newCart = CartModelMockBuilder
                .aCart()
                .withSite(site1)
                .build();
        when(cartService.hasSessionCart()).thenReturn(true);
        when(baseSiteService.getCurrentBaseSite()).thenReturn(site1);
        when(cartService.getSessionCart()).thenReturn(newCart);
        when(baseSiteService.getBaseSiteForUID("blue-be")).thenReturn(site2);
        Assert.assertFalse(filter.isValidCartOnSession());
    }


    @Test
    public void testIsCurrentSiteOnSessionCart_noSessionCart() {
        when(cartService.hasSessionCart()).thenReturn(false);
        Assert.assertFalse(filter.isValidCartOnSession());
        verifyZeroInteractions(baseSiteService);
    }

    @Test
    public void testDoRestore_validRestoredCart() {
        CartRestorationData cartRestorationData = mock(CartRestorationData.class);
        when(sessionService.getAttribute(WebConstants.CART_RESTORATION)).thenReturn(cartRestorationData);
        doReturn(true).when(filter).isValidCartOnSession();
        Assert.assertEquals(CART_GUID, filter.restoreOrCreateValidCart(CART_GUID));
        verify(filter).doRestoreCart(CART_GUID);
        verify(filter).setCartRestaurationSessionAttributes(cartRestorationData);
    }

    @Test
    public void testDoRestore_invalidRestoredCart() throws CommerceCartRestorationException {
        // given
        CartRestorationData cartRestorationData = mock(CartRestorationData.class);
        when(cartRestorationData.getModifications()).thenReturn(null);
        when(cartFacade.restoreSavedCart(CART_GUID)).thenReturn(cartRestorationData);
        when(sessionService.getAttribute(WebConstants.CART_RESTORATION)).thenReturn(cartRestorationData);
        doReturn(false).when(filter).isValidCartOnSession();
        String newCartGuid = RandomStringUtils.random(10);
        final CartModel newCart = CartModelMockBuilder
                .aCart()
                .withGUID(newCartGuid)
                .build();
        when(cartService.getSessionCart()).thenReturn(newCart);

        // when
        String result = filter.restoreOrCreateValidCart(CART_GUID);

        // then
        Assert.assertEquals(newCartGuid, result);
        verify(filter).doRestoreCart(CART_GUID);
        verify(filter).setCartRestaurationSessionAttributes(cartRestorationData);
        verify(cartService).setSessionCart(newCart);
        verify(sessionService, times(1)).setAttribute(WebConstants.CART_RESTORATION, cartRestorationData);
        verify(cartFacade).restoreSavedCart(CART_GUID);
        verify(sessionService).removeAttribute(SESSION_CART_PARAMETER_NAME);
        verify(sessionService, times(1)).setAttribute(WebConstants.CART_RESTORATION, null);
        verify(sessionService).setAttribute(WebConstants.CART_RESTORATION_SHOW_MESSAGE, Boolean.FALSE);
    }

    @Test
    public void testDoFilterInternal_anonymousUser_validCartOnSession() throws ServletException, IOException {
        // given
        final HttpServletRequest request = mock(HttpServletRequest.class);
        final HttpServletResponse response = mock(HttpServletResponse.class);
        final FilterChain filterChain = mock(FilterChain.class);

        when(userFacade.isAnonymousUser()).thenReturn(true);
        final CartModel newCart = CartModelMockBuilder
                .aCart()
                .withGUID(CART_GUID)
                .build();

        when(cartService.getSessionCart()).thenReturn(newCart);
        doReturn(true).when(filter).isValidCartOnSession();
        doReturn(CART_GUID).when(filter).restoreOrCreateValidCart(CART_GUID);

        // when
        filter.doFilterInternal(request, response, filterChain);

        // then
        verify(userFacade).isAnonymousUser();
        verify(cartRestoreCookieGenerator).addCookie(response, CART_GUID);
    }

    @Test
    public void testDoFilterInternal_anonymousUser_restoreValidCartFromCookie() throws ServletException, IOException {
        // given
        when(cartRestoreCookieGenerator.getCookiePath()).thenReturn("/");
        final Cookie anotherCookie = mock(Cookie.class);
        when(anotherCookie.getName()).thenReturn(RandomStringUtils.random(10));

        final Cookie cookie = mock(Cookie.class);
        when(cookie.getName()).thenReturn(COOKIE_NAME);
        when(cookie.getValue()).thenReturn(CART_GUID);

        final Cookie anotherCartCookie = mock(Cookie.class);
        when(anotherCartCookie.getName()).thenReturn(COOKIE_NAME);
        when(anotherCartCookie.getValue()).thenReturn(CART_GUID);
        when(anotherCartCookie.getPath()).thenReturn(RandomStringUtils.random(10));

        final HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getCookies()).thenReturn(new Cookie[]{anotherCookie, cookie, anotherCartCookie});

        final HttpServletResponse response = mock(HttpServletResponse.class);
        final FilterChain filterChain = mock(FilterChain.class);

        when(cartRestoreCookieGenerator.getCookieName()).thenReturn(COOKIE_NAME);
        when(userFacade.isAnonymousUser()).thenReturn(true);
        final CartModel newCart = CartModelMockBuilder
                .aCart()
                .withGUID(CART_GUID)
                .build();

        when(cartService.getSessionCart()).thenReturn(newCart);
        doReturn(false).when(filter).isValidCartOnSession();
        doReturn(CART_GUID).when(filter).restoreOrCreateValidCart(CART_GUID);

        // when
        filter.doFilterInternal(request, response, filterChain);

        // then
        verify(cartRestoreCookieGenerator).getCookieName();
        verify(cartRestoreCookieGenerator).addCookie(response, CART_GUID);
        verify(userFacade).isAnonymousUser();
        verify(filter).restoreOrCreateValidCart(CART_GUID);
    }

    @Test
    public void testDoFilterInternal_anonymousUser_newCartNoCookie() throws ServletException, IOException {
        // given
        final HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getCookies()).thenReturn(new Cookie[]{});

        final HttpServletResponse response = mock(HttpServletResponse.class);
        final FilterChain filterChain = mock(FilterChain.class);

        when(cartRestoreCookieGenerator.getCookieName()).thenReturn(COOKIE_NAME);
        when(userFacade.isAnonymousUser()).thenReturn(true);
        final CartModel newCart = CartModelMockBuilder
                .aCart()
                .withGUID(CART_GUID)
                .build();

        when(cartService.getSessionCart()).thenReturn(newCart);

        // when
        filter.doFilterInternal(request, response, filterChain);

        // then
        verify(userFacade).isAnonymousUser();
        verify(cartRestoreCookieGenerator).addCookie(response, CART_GUID);
        verify(sessionService).removeAttribute(SESSION_CART_PARAMETER_NAME);
        verify(cartService).setSessionCart(newCart);
    }


    @Test
    public void testDoFilterInternal_loggedInUser_validCartOnSession() throws ServletException, IOException {
        // given
        final HttpServletRequest request = mock(HttpServletRequest.class);
        final HttpServletResponse response = mock(HttpServletResponse.class);
        final FilterChain filterChain = mock(FilterChain.class);
        doReturn(true).when(filter).isValidCartOnSession();

        // when
        filter.doFilterInternal(request, response, filterChain);

        // then
        verify(userFacade).isAnonymousUser();
        verify(filter, times(0)).restoreOrCreateValidCart(any(String.class));
    }

    @Test
    public void testDoFilterInternal_loggedInUser_restoreValidCart() throws ServletException, IOException, CommerceCartRestorationException {
        // given
        final HttpServletRequest request = mock(HttpServletRequest.class);
        final HttpServletResponse response = mock(HttpServletResponse.class);
        final FilterChain filterChain = mock(FilterChain.class);
        CartRestorationData cartRestorationData = mock(CartRestorationData.class);
        when(cartFacade.restoreSavedCart(CART_GUID)).thenReturn(cartRestorationData);
        when(sessionService.getAttribute(WebConstants.CART_RESTORATION)).thenReturn(cartRestorationData);
        when(site1.getUid()).thenReturn("blue-be");
        final CartModel cart1 = CartModelMockBuilder
                .aCart()
                .withSite(site1)
                .build();

        when(cartService.getSessionCart()).thenReturn(cart1);
        when(cartService.hasSessionCart()).thenReturn(false, true);

        when(baseSiteService.getCurrentBaseSite()).thenReturn(site1);
        when(baseSiteService.getBaseSiteForUID("blue-be")).thenReturn(site1);


        // when
        filter.doFilterInternal(request, response, filterChain);

        // then
        verify(filter).restoreOrCreateValidCart(null);
        verify(filter).doRestoreCart(null);
        verify(filter).setCartRestaurationSessionAttributes(cartRestorationData);
        verify(cartService, times(1)).getSessionCart();
    }

    @Test
    public void testDoFilterInternal_loggedInUser_restoreInvalidCart() throws ServletException, IOException, CommerceCartRestorationException {
        // given
        final HttpServletRequest request = mock(HttpServletRequest.class);
        final HttpServletResponse response = mock(HttpServletResponse.class);
        final FilterChain filterChain = mock(FilterChain.class);
        CartRestorationData cartRestorationData = mock(CartRestorationData.class);
        when(cartFacade.restoreSavedCart(CART_GUID)).thenReturn(cartRestorationData);
        when(sessionService.getAttribute(WebConstants.CART_RESTORATION)).thenReturn(cartRestorationData);
        when(site1.getUid()).thenReturn("blue-be");
        final CartModel cart1 = mock(CartModel.class);

        when(cartService.getSessionCart()).thenReturn(cart1);
        when(cartService.hasSessionCart()).thenReturn(false, false);
        when(baseSiteService.getCurrentBaseSite()).thenReturn(site1);
        when(baseSiteService.getBaseSiteForUID("blue-be")).thenReturn(site1);

        // when
        filter.doFilterInternal(request, response, filterChain);

        // then
        verify(filter).restoreOrCreateValidCart(null);
        verify(filter).doRestoreCart(null);
        verify(filter).setCartRestaurationSessionAttributes(cartRestorationData);
        verify(sessionService).removeAttribute(SESSION_CART_PARAMETER_NAME);
        verify(cartService, times(1)).getSessionCart();
        verify(cartService).setSessionCart(cart1);
    }
}