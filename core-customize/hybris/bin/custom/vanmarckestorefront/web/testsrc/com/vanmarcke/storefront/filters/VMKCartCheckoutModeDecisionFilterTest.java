package com.vanmarcke.storefront.filters;

import com.vanmarcke.facades.cart.VMKCartFacade;
import de.hybris.bootstrap.annotations.UnitTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKCartCheckoutModeDecisionFilterTest {

    @Mock
    private VMKCartFacade cartFacade;

    @InjectMocks
    private VMKCartCheckoutModeDecisionFilter vmkUpdateCartModeFilter;

    @Test
    public void testDoFilterInternal_Cart() throws Exception {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        FilterChain filterChain = mock(FilterChain.class);

        when(request.getRequestURL()).thenReturn(new StringBuffer("https://www.blue.com/cart/checkout"));

        this.vmkUpdateCartModeFilter.doFilterInternal(request, response, filterChain);

        verify(cartFacade).updateCartCheckoutMode(false);
    }

    @Test
    public void testDoFilterInternal_Checkout() throws Exception {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        FilterChain filterChain = mock(FilterChain.class);

        when(request.getRequestURL()).thenReturn(new StringBuffer("https://www.blue.com/checkout/multi"));

        this.vmkUpdateCartModeFilter.doFilterInternal(request, response, filterChain);

        verify(cartFacade).updateCartCheckoutMode(true);
    }

    @Test
    public void testDoFilterInternal_saferpay() throws Exception {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        FilterChain filterChain = mock(FilterChain.class);

        when(request.getRequestURL()).thenReturn(new StringBuffer("https://www.blue.com/checkout/payment/saferpay"));

        this.vmkUpdateCartModeFilter.doFilterInternal(request, response, filterChain);

        verify(cartFacade).updateCartCheckoutMode(true);
    }

}