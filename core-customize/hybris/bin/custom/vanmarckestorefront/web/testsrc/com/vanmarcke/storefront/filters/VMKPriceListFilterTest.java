package com.vanmarcke.storefront.filters;

import com.vanmarcke.core.model.CommerceGroupModel;
import com.vanmarcke.services.commercegroup.VMKCommerceGroupService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.europe1.enums.UserPriceGroup;
import de.hybris.platform.europe1.enums.UserTaxGroup;
import de.hybris.platform.servicelayer.session.SessionService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static de.hybris.platform.europe1.constants.Europe1Constants.PARAMS.UPG;
import static de.hybris.platform.europe1.constants.Europe1Constants.PARAMS.UTG;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKPriceListFilterTest {

    @Mock
    private VMKCommerceGroupService commerceGroupService;
    @Mock
    private SessionService sessionService;

    @InjectMocks
    private VMKPriceListFilter vmkPriceListFilter;

    @Test
    public void testDoFilterInternal() throws Exception {
        final HttpServletRequest request = mock(HttpServletRequest.class);
        final HttpServletResponse response = mock(HttpServletResponse.class);
        final FilterChain filterChain = mock(FilterChain.class);

        final CommerceGroupModel commerceGroupModel = mock(CommerceGroupModel.class);
        final UserPriceGroup userPriceGroup = mock(UserPriceGroup.class);
        final UserTaxGroup userTaxGroup = mock(UserTaxGroup.class);

        when(commerceGroupModel.getUserPriceGroup()).thenReturn(userPriceGroup);
        when(commerceGroupModel.getUserTaxGroup()).thenReturn(userTaxGroup);

        when(commerceGroupService.getCurrentCommerceGroup()).thenReturn(commerceGroupModel);

        vmkPriceListFilter.doFilterInternal(request, response, filterChain);

        verify(sessionService).setAttribute(UPG, userPriceGroup);
        verify(sessionService).setAttribute(UTG, userTaxGroup);

        verify(filterChain).doFilter(request, response);
    }

    @Test
    public void testDoFilterInternal_withoutCommerceGroup() throws Exception {
        final HttpServletRequest request = mock(HttpServletRequest.class);
        final HttpServletResponse response = mock(HttpServletResponse.class);
        final FilterChain filterChain = mock(FilterChain.class);

        vmkPriceListFilter.doFilterInternal(request, response, filterChain);

        verify(sessionService, never()).setAttribute(eq(UPG), any());
        verify(sessionService, never()).setAttribute(eq(UTG), any());

        verify(filterChain).doFilter(request, response);
    }

}