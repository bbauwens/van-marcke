package com.vanmarcke.storefront.filters;

import com.google.common.collect.Lists;
import com.vanmarcke.facades.pos.VMKPointOfServiceFacade;
import com.vanmarcke.facades.storesession.VMKStoreSessionFacade;
import com.vanmarcke.services.model.builder.PointOfServiceModelMockBuilder;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commercefacades.storelocator.data.PointOfServiceData;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.mockito.Mockito.*;

/**
 * The {@link VMKPointOfServiceRestorationFilterTest} class contains the unit tests for the
 * {@link VMKPointOfServiceRestorationFilter} class.
 *
 * @author Christiaan Janssen
 * @since 29-04-2021
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKPointOfServiceRestorationFilterTest {

    private static final String POS_NAME = RandomStringUtils.random(10);

    @Mock
    private VMKStoreSessionFacade storeSessionFacade;

    @Mock
    private UserService userService;

    @Mock
    private BaseSiteService baseSiteService;

    @Mock
    private VMKPointOfServiceFacade vmkPointOfServiceFacade;

    @InjectMocks
    private VMKPointOfServiceRestorationFilter filter;

    @Test
    public void testDoFilter_defaultPos() throws ServletException, IOException {
        // given
        final CustomerModel customer = mock(CustomerModel.class);
        final HttpServletRequest request = mock(HttpServletRequest.class);

        final HttpServletResponse response = mock(HttpServletResponse.class);
        final FilterChain filterChain = mock(FilterChain.class);

        final PointOfServiceData anotherPos = new PointOfServiceData();
        anotherPos.setName(RandomStringUtils.random(10));
        anotherPos.setDistanceKm(RandomUtils.nextDouble());

        final PointOfServiceData pos = new PointOfServiceData();
        pos.setName(POS_NAME);
        pos.setDistanceKm(0d);

        BaseSiteModel baseSiteModel = mock(BaseSiteModel.class);
        when(baseSiteService.getCurrentBaseSite()).thenReturn(baseSiteModel);
        when(baseSiteModel.getChannel()).thenReturn(SiteChannel.B2B);

        when(storeSessionFacade.getAllStores()).thenReturn(Lists.newArrayList(anotherPos, pos));

        when(storeSessionFacade.getAllStores()).thenReturn(Lists.newArrayList(anotherPos, pos));

        when(userService.getCurrentUser()).thenReturn(customer);
        when(userService.isAnonymousUser(customer)).thenReturn(true);

        // when
        filter.doFilterInternal(request, response, filterChain);

        // then
        verify(storeSessionFacade).getAllStores();
        verify(userService).getCurrentUser();
        verify(userService).isAnonymousUser(customer);
    }

    @Test
    public void testDoFilter_storedPos_registeredUser() throws ServletException, IOException {
        // given
        final B2BCustomerModel customer = mock(B2BCustomerModel.class);
        final HttpServletRequest request = mock(HttpServletRequest.class);
        final HttpServletResponse response = mock(HttpServletResponse.class);
        final FilterChain filterChain = mock(FilterChain.class);
        final PointOfServiceModel pointOfServiceModel = PointOfServiceModelMockBuilder.aPointOfService().build();
        final PointOfServiceData posData = new PointOfServiceData();
        posData.setName(POS_NAME);

        BaseSiteModel baseSiteModel = mock(BaseSiteModel.class);
        when(baseSiteService.getCurrentBaseSite()).thenReturn(baseSiteModel);
        when(baseSiteModel.getChannel()).thenReturn(SiteChannel.B2B);

        when(userService.getCurrentUser()).thenReturn(customer);
        when(userService.isAnonymousUser(customer)).thenReturn(false);

        pointOfServiceModel.setName(POS_NAME);
        when(customer.getSessionStore()).thenReturn(pointOfServiceModel);
        when(storeSessionFacade.getAllStores()).thenReturn(Lists.newArrayList(posData));

        // when
        filter.doFilterInternal(request, response, filterChain);

        // then
        verify(storeSessionFacade, atLeast(2)).getAllStores();
        verify(userService).getCurrentUser();
        verify(userService).isAnonymousUser(customer);
    }
}
