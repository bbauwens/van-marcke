package com.vanmarcke.patches.release.release09;

import com.vanmarcke.patches.release.AbstractVMKPatch;
import de.hybris.platform.patches.organisation.ImportLanguage;

import java.util.Set;

import static com.vanmarcke.patches.structure.VMKRelease.R9;
import static com.vanmarcke.patches.structure.VMKStructureState.V1;

/**
 * The {@link Patch09x00} class contains lists the patches for release 9.
 *
 * @author Christiaan Janssen
 * @since 28-07-2021
 */
public class Patch09x00 extends AbstractVMKPatch {

    /**
     * Provides an implementation of Patch09x00.
     */
    public Patch09x00() {
        super("09_00", "Release 9 - Phase 1", R9, V1);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void createGlobalData(final Set<ImportLanguage> languages, final boolean updateLanguagesOnly) {
        importGlobalData("r09_00_559_setup_for_stock_indication_facet.impex", languages, updateLanguagesOnly);
        importGlobalData("r09_00_643_indexed_properties.impex", languages, updateLanguagesOnly);
        importGlobalData("r09_00_560_variantsSearchTemplate.impex", languages, updateLanguagesOnly);
        importGlobalData("r09_00_575_meta_tags_title_and_desc.impex", languages, updateLanguagesOnly);
        importGlobalData("r09_00_565_triggerProductExportJob.impex", languages, updateLanguagesOnly);
        importGlobalData("r09_00_585_purchasabilityIndexedProperty.impex", languages, updateLanguagesOnly);
        importGlobalData("r09_00_528_brandTypeSolrIndexedProperty.impex", languages, updateLanguagesOnly);
        importGlobalData("r09_00_279_orderSplit.impex", languages, updateLanguagesOnly);
        importGlobalData("r09_00_645_enable_stock_system.impex", languages, updateLanguagesOnly);
        importGlobalData("r09_00_642_daily_full_etim_product_export.impex", languages, updateLanguagesOnly);
        importGlobalData("r09_00_721_productListLocalizedMessage.impex", languages, updateLanguagesOnly);
        importGlobalData("r09_00_689_payment_types.impex", languages, updateLanguagesOnly);
    }
}
