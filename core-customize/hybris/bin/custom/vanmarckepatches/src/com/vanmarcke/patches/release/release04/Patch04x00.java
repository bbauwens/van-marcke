package com.vanmarcke.patches.release.release04;

import com.vanmarcke.patches.release.AbstractVMKPatch;
import de.hybris.platform.patches.organisation.ImportLanguage;

import java.util.Set;

import static com.vanmarcke.patches.structure.VMKRelease.R3;
import static com.vanmarcke.patches.structure.VMKRelease.R4;
import static com.vanmarcke.patches.structure.VMKStructureState.V1;

/**
 * Defines the impex files of patch 04.
 *
 * @author Niels Raemaekers
 * @since 18-03-2021
 */
public class Patch04x00 extends AbstractVMKPatch {

    /**
     * Provides an implementation of Patch04x00.
     */
    public Patch04x00() {
        super("04_00", "Release 4 - Phase 1", R4, V1);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void createGlobalData(final Set<ImportLanguage> languages, final boolean updateLanguagesOnly) {
        importGlobalData("r04_00_999_translations.impex", languages, updateLanguagesOnly);
    }
}
