package com.vanmarcke.patches.release.release03;

import com.vanmarcke.patches.release.AbstractVMKPatch;
import de.hybris.platform.patches.organisation.ImportLanguage;

import java.util.Set;

import static com.vanmarcke.patches.structure.VMKRelease.R3;
import static com.vanmarcke.patches.structure.VMKStructureState.V1;


/**
 * Defines the impex files of patch 03.
 *
 * @author Tom van den Berg
 * @since 15-10-2020
 */
public class Patch03x00 extends AbstractVMKPatch {


    /**
     * Provides an implementation of Patch03x00.
     */
    public Patch03x00() {
        super("03_00", "Release 3 - Phase 1", R3, V1);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void createGlobalData(final Set<ImportLanguage> languages, final boolean updateLanguagesOnly) {
        importGlobalData("r03_00_999_translations.impex", languages, updateLanguagesOnly);
    }
}