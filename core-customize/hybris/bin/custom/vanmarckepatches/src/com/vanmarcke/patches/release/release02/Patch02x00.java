package com.vanmarcke.patches.release.release02;

import com.vanmarcke.patches.release.AbstractVMKPatch;
import de.hybris.platform.patches.organisation.ImportLanguage;

import java.util.Set;

import static com.vanmarcke.patches.structure.VMKRelease.R2;
import static com.vanmarcke.patches.structure.VMKStructureState.V1;

public class Patch02x00 extends AbstractVMKPatch {

    public Patch02x00() {
        super("02_00", "Release 2 - Phase 1", R2, V1);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void createGlobalData(final Set<ImportLanguage> languages, final boolean updateLanguagesOnly) {
        importGlobalData("r02_00_001_vanmarcke_trusted_client_oauth2.impex", languages, updateLanguagesOnly);
//        importGlobalData("r02_00_002_add_german.impex", languages, updateLanguagesOnly);
        importGlobalData("r02_00_003_rollout_luxembourg.impex", languages, updateLanguagesOnly);
        importGlobalData("r02_00_004_ibm_configuration.impex", languages, updateLanguagesOnly);
        importGlobalData("r02_00_005_vanmarcke_contact_and_location.impex", languages, updateLanguagesOnly);
        importGlobalData("r02_00_006_point_of_services_incl_LU.impex", languages, updateLanguagesOnly);
        importGlobalData("r02_00_999_translations.impex", languages, updateLanguagesOnly);
    }
}