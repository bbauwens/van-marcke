package com.vanmarcke.patches.release.release13;

import com.vanmarcke.patches.release.AbstractVMKPatch;
import de.hybris.platform.patches.organisation.ImportLanguage;

import java.util.Set;

import static com.vanmarcke.patches.structure.VMKRelease.R13;
import static com.vanmarcke.patches.structure.VMKStructureState.V1;

/**
 * The {@link Patch13x00} class contains lists the patches for release 13.
 *
 * @author Cristi Stoica
 * @since 18-10-2021
 */
public class Patch13x00 extends AbstractVMKPatch {
    /**
     * Provides an implementation of Patch13x00.
     */
    public Patch13x00() {
        super("13_00", "Release 13 - Phase 1", R13, V1);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void createGlobalData(final Set<ImportLanguage> languages, final boolean updateLanguagesOnly) {
        importGlobalData("r13_00_743_alternativeDeliveryMethods.impex", languages, updateLanguagesOnly);
        importGlobalData("r13_00_776_plvSearchTemplate.impex", languages, updateLanguagesOnly);
        importGlobalData("r13_00_852_diy_mail_templates.impex", languages, updateLanguagesOnly);
        importGlobalData("r13_00_localizedMessages.impex", languages, updateLanguagesOnly);
    }
}
