package com.vanmarcke.patches.release.release12;

import com.vanmarcke.patches.release.AbstractVMKPatch;
import de.hybris.platform.patches.organisation.ImportLanguage;

import java.util.Set;

import static com.vanmarcke.patches.structure.VMKRelease.R12;
import static com.vanmarcke.patches.structure.VMKStructureState.V1;

/**
 * The {@link Patch12x00} class contains lists the patches for release 10.
 *
 * @author Cristi Stoica
 * @since 18-10-2021
 */
public class Patch12x00 extends AbstractVMKPatch {
    /**
     * Provides an implementation of Patch12x00.
     */
    public Patch12x00() {
        super("12_00", "Release 12 - Phase 1", R12, V1);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void createGlobalData(final Set<ImportLanguage> languages, final boolean updateLanguagesOnly) {
        importGlobalData("r12_00_672_invoiceEmail.impex", languages, updateLanguagesOnly);
        importGlobalData("r12_00_cartCleanupJob.impex", languages, updateLanguagesOnly);
        importGlobalData("r12_00_726_solr_index_barcode.impex", languages, updateLanguagesOnly);
        importGlobalData("r12_00_localizedMessages.impex", languages, updateLanguagesOnly);
        importGlobalData("r12_00_679_add_diy.impex", languages, updateLanguagesOnly);
    }
}
