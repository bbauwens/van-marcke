package com.vanmarcke.patches.release.release01;

import com.vanmarcke.patches.release.AbstractVMKPatch;
import de.hybris.platform.patches.organisation.ImportLanguage;

import java.util.Set;

import static com.vanmarcke.patches.structure.VMKRelease.R1;
import static com.vanmarcke.patches.structure.VMKStructureState.V1;

public class Patch01x10 extends AbstractVMKPatch {

    public Patch01x10() {
        super("01_10", "Release 1 - Phase 1", R1, V1);
    }

    @Override
    protected void createGlobalData(final Set<ImportLanguage> languages, final boolean updateLanguagesOnly) {
        importGlobalData("r01_10_001_video_media_format.impex", languages, updateLanguagesOnly);
        importGlobalData("r01_10_002_update_saml_user_groups.impex", languages, updateLanguagesOnly);
        importGlobalData("r01_10_003_set_base_store_on_points_of_service.impex", languages, updateLanguagesOnly);
        importGlobalData("r01_10_004_add_languages_blue_global_base_store.impex", languages, updateLanguagesOnly);
        importGlobalData("r01_10_005_consent_overview_linkname.impex", languages, updateLanguagesOnly);
        importGlobalData("r01_10_006_add_summary_to_product_index.impex", languages, updateLanguagesOnly);
        importGlobalData("r01_10_007_cronjob_scheduling.impex", languages, updateLanguagesOnly);
        importGlobalData("r01_10_008_affectation_france.impex", languages, updateLanguagesOnly);
        importGlobalData("r01_10_009_user_group_rights_technical_data_sheets.impex", languages, updateLanguagesOnly);
        importGlobalData("r01_10_999_translations.impex", languages, updateLanguagesOnly);
    }
}