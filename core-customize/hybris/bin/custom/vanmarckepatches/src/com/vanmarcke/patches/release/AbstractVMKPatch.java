package com.vanmarcke.patches.release;

import com.vanmarcke.patches.structure.VMKOrganisationUnit;
import de.hybris.platform.patches.AbstractPatch;
import de.hybris.platform.patches.Release;
import de.hybris.platform.patches.organisation.ImportLanguage;
import de.hybris.platform.patches.organisation.StructureState;
import de.hybris.platform.patches.utils.StructureStateUtils;

import java.util.Set;

/**
 * Project specific parent class for all patches.
 * It works as facade giving nice API for Patches class and translate calls to not project specific format.
 */
public abstract class AbstractVMKPatch extends AbstractPatch {

    private final String patchDescription;

    public AbstractVMKPatch(final String patchId, final String patchDescription, final Release release, final StructureState structureState) {
        super(patchId, patchId, release, structureState);
        this.patchDescription = patchDescription;
    }

    @Override
    public String getPatchDescription() {
        return patchDescription;
    }

    @Override
    public void createProjectData(final StructureState structureState) {
        // this should return true if we want to update language files only!
        final boolean update = structureState != this.structureState;
        createGlobalData(structureState, update);
        afterCreateGlobalData(structureState, update);
    }

    protected abstract void createGlobalData(final Set<ImportLanguage> languages, boolean updateLanguagesOnly);

    protected void afterCreateGlobalData(final StructureState structureState, boolean update) {
    }

    /**
     * This method is responsible for update global data (not organisation specific).
     *
     * @param structureState indicates which structureState should be used to import these global data (see update parameter)
     * @param update         if set to true:<br/>
     *                       only language specific files will be imported (assumption is that not language specific files were
     *                       imported before since this is just update). It also means that data will uploaded only for languages
     *                       defined for organisations defined in this structureState that weren't introduced for other organisations
     *                       before
     */
    protected void createGlobalData(final StructureState structureState, final boolean update) {
        Set<ImportLanguage> importLanguages;
        if (update) {
            importLanguages = StructureStateUtils.getNewGlobalLanguages(VMKOrganisationUnit.values(), structureState);
        } else {
            importLanguages = StructureStateUtils.getAllGlobalLanguages(VMKOrganisationUnit.values(), structureState);
        }
        createGlobalData(importLanguages, update);
    }

}
