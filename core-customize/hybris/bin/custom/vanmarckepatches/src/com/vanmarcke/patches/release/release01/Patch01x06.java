package com.vanmarcke.patches.release.release01;

import com.vanmarcke.patches.release.AbstractVMKPatch;
import de.hybris.platform.patches.organisation.ImportLanguage;

import java.util.Set;

import static com.vanmarcke.patches.structure.VMKRelease.R1;
import static com.vanmarcke.patches.structure.VMKStructureState.V1;

public class Patch01x06 extends AbstractVMKPatch {

    public Patch01x06() {
        super("01_06", "Release 1 - Phase 1", R1, V1);
    }

    @Override
    protected void createGlobalData(final Set<ImportLanguage> languages, final boolean updateLanguagesOnly) {
        importGlobalData("r01_06_001_cms_storefinder.impex", languages, updateLanguagesOnly);
        importGlobalData("r01_06_999_translations.impex", languages, updateLanguagesOnly);
        importGlobalData("r01_06_001_smartedit_test_user.impex", languages, updateLanguagesOnly);
    }
}