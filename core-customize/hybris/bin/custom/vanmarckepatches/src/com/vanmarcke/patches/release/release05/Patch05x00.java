package com.vanmarcke.patches.release.release05;

import com.vanmarcke.patches.release.AbstractVMKPatch;
import de.hybris.platform.patches.organisation.ImportLanguage;

import java.util.Set;

import static com.vanmarcke.patches.structure.VMKRelease.R5;
import static com.vanmarcke.patches.structure.VMKStructureState.V1;

/**
 * Defines the impex files of patch 05.
 *
 * @author Cristi Stoica
 * @since 28-04-2021
 */
public class Patch05x00 extends AbstractVMKPatch {

    /**
     * Provides an implementation of Patch05x00.
     */
    public Patch05x00() {
        super("05_00", "Release 5 - Phase 1", R5, V1);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void createGlobalData(final Set<ImportLanguage> languages, final boolean updateLanguagesOnly) {
        importGlobalData("r05_00_486_languageNamesLocalized.impex", languages, updateLanguagesOnly);
        importGlobalData("r05_00_496_productFeatureUserRights.impex", languages, updateLanguagesOnly);
        importGlobalData("r05_00_496_solrProductFeature.impex", languages, updateLanguagesOnly);
        importGlobalData("r05_00_497_wishlists.impex", languages, updateLanguagesOnly);
    }
}
