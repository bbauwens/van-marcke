package com.vanmarcke.patches.release.release01;

import com.vanmarcke.patches.release.AbstractVMKPatch;
import de.hybris.platform.patches.organisation.ImportLanguage;

import java.util.Set;

import static com.vanmarcke.patches.structure.VMKRelease.R1;
import static com.vanmarcke.patches.structure.VMKStructureState.V1;

public class Patch01x09 extends AbstractVMKPatch {

    public Patch01x09() {
        super("01_09", "Release 1 - Phase 1", R1, V1);
    }

    @Override
    protected void createGlobalData(final Set<ImportLanguage> languages, final boolean updateLanguagesOnly) {
        importGlobalData("r01_09_002_header_links_update.impex", languages, updateLanguagesOnly);
        importGlobalData("r01_09_003_create_cronjob_saferpay_async.impex", languages, updateLanguagesOnly);
        importGlobalData("r01_09_004_create_retention_job_saferpay_notifications.impex", languages, updateLanguagesOnly);
        importGlobalData("r01_09_005_remove_default_employees.impex", languages, updateLanguagesOnly);
        importGlobalData("r01_09_006_cronjob_scheduling.impex", languages, updateLanguagesOnly);
        importGlobalData("r01_09_007_footer_navigation.impex", languages, updateLanguagesOnly);
        importGlobalData("r01_09_008_saml_user_group.impex", languages, updateLanguagesOnly);
        importGlobalData("r01_09_009_add_payment_transaction_type_name_captured.impex", languages, updateLanguagesOnly);
        importGlobalData("r01_09_010_fix_product_personalization_rule.impex", languages, updateLanguagesOnly);
        importGlobalData("r01_09_011_make_product_code_read_only.impex", languages, updateLanguagesOnly);
        importGlobalData("r01_09_012_fix_solr_indexed_properties.impex", languages, updateLanguagesOnly);
        importGlobalData("r01_09_013_improve_solr_performance.impex", languages, updateLanguagesOnly);
        importGlobalData("r01_09_017_add_nonArchivedVariantsExportCronJob.impex", languages, updateLanguagesOnly);
        importGlobalData("r01_09_018_change_category_link_name.impex", languages, updateLanguagesOnly);
        importGlobalData("r01_09_019_add_vmkRemoveInactiveProductReferencesJob.impex", languages, updateLanguagesOnly);
        importGlobalData("r01_09_020_siteMapConfiguration.impex", languages, updateLanguagesOnly);
        importGlobalData("r01_09_021_fixProductSyncSchedule.impex", languages, updateLanguagesOnly);
        importGlobalData("r01_09_022_links_email_templates.impex", languages, updateLanguagesOnly);
        importGlobalData("r01_09_023_add_vmkSendFailedOrdersNotificationJob.impex", languages, updateLanguagesOnly);
        importGlobalData("r01_09_024_changeFooterNavigationClientServiceComponent.impex", languages, updateLanguagesOnly);
        importGlobalData("r01_09_025_cms_rights_link_component.impex", languages, updateLanguagesOnly);
        importGlobalData("r01_09_026_remove_followup_upselling_pdptemplate.impex", languages, updateLanguagesOnly);
        importGlobalData("r01_09_027_ allowVariationPagesForCategoryPages.impex", languages, updateLanguagesOnly);
        importGlobalData("r01_09_028_asm_sso.impex", languages, updateLanguagesOnly);
        importGlobalData("r01_09_029_create_vmk_return_contacts_and_location_mappings.impex", languages, updateLanguagesOnly);
        importGlobalData("r01_09_030_consent_components.impex", languages, updateLanguagesOnly);
        importGlobalData("r01_09_999_translations.impex", languages, updateLanguagesOnly);
    }
}