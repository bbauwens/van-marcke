package com.vanmarcke.patches.release.release01;

import com.vanmarcke.patches.release.AbstractVMKPatch;
import de.hybris.platform.patches.organisation.ImportLanguage;

import java.util.Set;

import static com.vanmarcke.patches.structure.VMKRelease.R1;
import static com.vanmarcke.patches.structure.VMKStructureState.V1;

public class Patch01x01 extends AbstractVMKPatch {

    public Patch01x01() {
        super("01_01", "Release 1 - Phase 1", R1, V1);
    }

    @Override
    protected void createGlobalData(final Set<ImportLanguage> languages, final boolean updateLanguagesOnly) {
        importGlobalData("r01_01_001_translations.impex", languages, updateLanguagesOnly);
        importGlobalData("r01_01_002_blue_product_solr.impex", languages, updateLanguagesOnly);
        importGlobalData("r01_01_004_cms_pagetemplates.impex", languages, updateLanguagesOnly);
        importGlobalData("r01_01_006_stores.impex", languages, updateLanguagesOnly);
        importGlobalData("r01_01_007_cronjobs.impex", languages, updateLanguagesOnly);
        importGlobalData("r01_01_008_translations_v2.impex", languages, updateLanguagesOnly);
        importGlobalData("r01_01_999_translations.impex", languages, updateLanguagesOnly);
    }
}