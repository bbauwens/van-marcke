package com.vanmarcke.patches.constants;

@SuppressWarnings({"deprecation", "PMD", "squid:CallToDeprecatedMethod"})
public class VanmarckepatchesConstants extends GeneratedVanmarckepatchesConstants {
    public static final String EXTENSIONNAME = "vanmarckepatches";

    private VanmarckepatchesConstants() {
        //empty
    }


}
