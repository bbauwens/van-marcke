/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.vanmarcke.patches.setup;

import com.vanmarcke.patches.constants.VanmarckepatchesConstants;
import de.hybris.platform.core.initialization.SystemSetup;
import de.hybris.platform.core.initialization.SystemSetupContext;
import de.hybris.platform.core.initialization.SystemSetupParameter;
import de.hybris.platform.core.initialization.SystemSetupParameterMethod;
import de.hybris.platform.patches.AbstractPatchesSystemSetup;

import java.util.List;


@SystemSetup(extension = VanmarckepatchesConstants.EXTENSIONNAME)
public class VMKPatchesSystemSetup extends AbstractPatchesSystemSetup {

    /**
     * Implement this method to create initial objects. This method will be called by system creator during
     * initialization and system update. Be sure that this method can be called repeatedly. An example usage of this
     * method is to create required cronjobs or modifying the type system (setting e.g some default values)
     *
     * @param setupContext
     */
    @Override
    @SystemSetup(type = SystemSetup.Type.ESSENTIAL, process = SystemSetup.Process.ALL)
    public void createEssentialData(final SystemSetupContext setupContext) {
        super.createEssentialData(setupContext);
    }

    /**
     * Implement this method to create data that is used in your project. This method will be called during the system
     * initialization.
     *
     * @param setupContext the context provides the selected parameters and values
     */
    @Override
    @SystemSetup(type = SystemSetup.Type.PROJECT, process = SystemSetup.Process.ALL)
    public void createProjectData(final SystemSetupContext setupContext) {
        super.createProjectData(setupContext);
    }

    /**
     * Get initialization Options
     *
     * @return List of SystemSetupParameter objects
     */
    @Override
    @SystemSetupParameterMethod
    public List<SystemSetupParameter> getInitializationOptions() {
        return super.getInitializationOptions();
    }

}