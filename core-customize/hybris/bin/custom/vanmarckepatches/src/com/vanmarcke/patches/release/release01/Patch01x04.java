package com.vanmarcke.patches.release.release01;

import com.vanmarcke.patches.release.AbstractVMKPatch;
import de.hybris.platform.patches.organisation.ImportLanguage;

import java.util.Set;

import static com.vanmarcke.patches.structure.VMKRelease.R1;
import static com.vanmarcke.patches.structure.VMKStructureState.V1;

public class Patch01x04 extends AbstractVMKPatch {

    public Patch01x04() {
        super("01_04", "Release 1 - Phase 1", R1, V1);
    }

    @Override
    protected void createGlobalData(final Set<ImportLanguage> languages, final boolean updateLanguagesOnly) {
        importGlobalData("r01_04_001_saferpay.impex", languages, updateLanguagesOnly);
        importGlobalData("r01_04_002_solr.impex", languages, updateLanguagesOnly);
        importGlobalData("r01_04_999_translations.impex", languages, updateLanguagesOnly);
    }
}