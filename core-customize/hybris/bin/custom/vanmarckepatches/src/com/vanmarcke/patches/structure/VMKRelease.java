package com.vanmarcke.patches.structure;

import de.hybris.platform.patches.Release;

/**
 * Represent project releases. String used in constructor will be used to create paths to impexes that should be
 * imported in logs etc. (should be unique).
 */
public enum VMKRelease implements Release {

    R0("00"),
    R1("01"),
    R2("02"),
    R3("03"),
    R4("04"),
    R5("05"),
    R6("06"),
    R7("07"),
    R8("08"),
    R9("09"),
    R10("10"),
    R11("11"),
    R12("12"),
    R13("13"),
    ENV("ENV"),
    LOCAL("LOCAL");

    private final String id;

    VMKRelease(final String id) {
        this.id = id;
    }

    @Override
    public String getReleaseId() {
        return this.id;
    }
}
