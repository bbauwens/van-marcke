########################################################################################################################
# VARIABLES
########################################################################################################################
# UID
$uid = diy
$name = DIY
# COUNTRY, LANGUAGE AND CURRENCY
$countries = BE
$languages = nl_BE, fr_BE, de_BE
$defaultLanguage = nl_BE
$currencies = EUR
$defaultCurrency = EUR
# PAYMENT
$paymentProvider = Saferpay
# URL
$urlPatternEndPoints = diy
$urlPatternLanguages = nl_BE|fr_BE|de_BE
########################################################################################################################
# THE ONLY CHANGE REQUIRED BELOW THIS LINE IS THE SITE MAP CONFIGURATION
########################################################################################################################

# CATALOG
$contentCatalogId = $uidBlueContentCatalog
$contentCatalogVersion = catalogversion(catalog(id[default = $contentCatalogId]), version[default = 'Staged'])[unique = true, default = $contentCatalogId:Staged]
$contentCatalogSyncJobCode = "Sync $country-BlueContent:Staged->Online"

INSERT_UPDATE ContentCatalog; id[unique = true] ; name [lang = en]           ; languages(isocode); superCatalog(id)
                            ; $contentCatalogId ; $name Blue Content Catalog ; $languages        ; $globalBlueContentCatalog

INSERT_UPDATE CatalogVersion; catalog(id)[unique = true]; version[unique = true]; active; languages(isoCode)
                            ; $contentCatalogId         ; Staged                ; false ; $languages
                            ; $contentCatalogId         ; Online                ; true  ; $languages

INSERT_UPDATE CatalogVersionSyncJob; code[unique = true]        ; sourceVersion(catalog(id), version); targetVersion(catalog(id), version); syncLanguages(isoCode); sessionUser(uid); sessionLanguage(isocode); sessionCurrency(isocode); removeMissingItems; rootTypes(code)[mode = append]
                                   ; $contentCatalogSyncJobCode ; $contentCatalogId:Staged           ; $contentCatalogId:Online           ; $languages            ; admin           ; en                      ; $defaultCurrency        ; true              ; CMSItem, CMSRelation

INSERT_UPDATE UserGroup; uid [unique = true]  ; writableCatalogVersions(catalog(id), version)[mode = append]; readableCatalogVersions(catalog(id), version)[mode = append]
                       ; blue-cmsmanagergroup ; $contentCatalogId:Staged                                    ; $contentCatalogId:Staged, $contentCatalogId:Online

# DELIVERY MODES

$zone = $uid-zone
$deliveryMode = $uid-standard
$pickupMode = $uid-tec

INSERT_UPDATE Zone; code[unique = true]; countries(isocode)
                  ; $zone              ; $countries

INSERT_UPDATE ZoneDeliveryMode; code[unique = true]; net  ; active
                              ; $deliveryMode      ; true ; true

INSERT_UPDATE ZoneDeliveryModeValue; zone(code)[unique = true]; deliveryMode(code)[unique = true]; minimum[unique = true]; value; currency(isocode)[unique = true]
                                   ; $zone                    ; $deliveryMode                    ; 0                     ; 0    ; $defaultCurrency

INSERT_UPDATE PickUpDeliveryMode; code[unique = true]; active; supportedMode(code)
                                ; $pickupMode        ; true  ; BUY_AND_COLLECT

# STORE
$storeId = $uid

UPDATE GenericItem[processor = de.hybris.platform.commerceservices.impex.impl.ConfigPropertyImportProcessor]; pk[unique = true]
$cpiBaseUrl = $config-cpi.api.baseurl
$cpiUsername = $config-cpi.api.user
$cpiPassword = $config-cpi.api.password

INSERT_UPDATE IBMConfiguration; core_name[unique = true]; baseURL     ; username     ; password
                              ; $storeId                ; $cpiBaseUrl ; $cpiUsername ; $cpiPassword

INSERT_UPDATE BaseStore; uid[unique = true]; name  ; ibmConfiguration(core_name); net  ; currencies(isocode); defaultCurrency(isocode); languages(isocode); defaultLanguage(isocode); taxGroup(code); deliveryCountries(isocode); deliveryModes(code)        ; pickupInStoreMode(code); catalogs(id)                                                            ; checkoutFlowGroup         ; paymentProvider  ; saferpaySpecVersion; saferpayCustomerId; saferpayTerminalId; submitOrderProcessCode
                       ; $storeId          ; $name ; $storeId                   ; true ; $currencies        ; $defaultCurrency        ; $languages        ; $defaultLanguage        ; NL            ; $countries                ; $deliveryMode, $pickupMode ; BUY_AND_COLLECT        ; $vmkProductCatalog,$gs1ClassificationCatalog ; blueCommerceCheckoutGroup ; $paymentProvider ; 1.15               ; 247752            ; 17963272          ; blue-order-process

# SITE
$startNavNode = $uidStartNavNode
$navNodeName = DIY
#$commerceGroup = $uid-commerce
$commerceGroup = be-commerce
$urlPattern = (?i)^https?://blue.vanmarcke.com/($urlPatternEndPoints)/($urlPatternLanguages).*$, (?i)^https?://blue.vmk.com:9002/($urlPatternEndPoints)/($urlPatternLanguages).*$, (?i)^https?://.*accstorefront.copy-vanmarcke1-\b[a-z]{1}[0-9]{1}\b-public.model-t.cc.commerce.ondemand.com/($urlPatternEndPoints)/($urlPatternLanguages).*$,
$siteMapPage = Homepage, Product, CategoryLanding, Category, Store, Content, Custom
$siteMapUrlLimitPerFile = 50000

INSERT_UPDATE SiteMapPage; &siteMapPage    ; code(code)[unique = true]; frequency(code)[unique = true]; priority[unique = true]; active[default = true]
                         ; Homepage        ; Homepage                 ; daily                         ; 1.0                    ;
                         ; Product         ; Product                  ; weekly                        ; 0.6                    ;
                         ; CategoryLanding ; CategoryLanding          ; daily                         ; 0.9                    ; false
                         ; Category        ; Category                 ; daily                         ; 0.8                    ;
                         ; Store           ; Store                    ; weekly                        ; 0.6                    ; false
                         ; Content         ; Content                  ; monthly                       ; 0.4                    ;
                         ; Custom          ; Custom                   ; daily                         ; 1.0                    ;

########################################################################################################################
# START MAKING CHANGES
########################################################################################################################
INSERT_UPDATE SiteMapLanguageCurrency; &siteMapLanguageCurrency; language(isoCode)[unique = true]; currency(isocode)[unique = true]
                                     ; $storeId-nl_BE-eur      ; nl_BE                           ; EUR
                                     ; $storeId-fr_BE-eur      ; fr_BE                           ; EUR
                                     ; $storeId-de_BE-eur      ; de_BE                           ; EUR
                                     ; $storeId-fr_FR-eur      ; fr_FR                           ; EUR
                                     ; $storeId-de_LU-eur      ; de_LU                           ; EUR
                                     ; $storeId-fr_LU-eur      ; fr_LU                           ; EUR
                                     ; $storeId-de_CH-eur      ; de_CH                           ; EUR
                                     ; $storeId-fr_CH-eur      ; fr_CH                           ; EUR
                                     ; $storeId-nl_NL-eur      ; nl_NL                           ; EUR

$siteMapLangCurrMapping = $storeId-nl_BE-eur, $storeId-fr_BE-eur, $storeId-de_BE-eur

INSERT_UPDATE SiteMapConfig; &siteMapConfigId       ; configId[unique = true]; siteMapTemplate(code)[unique = true]; siteMapLanguageCurrencies(&siteMapLanguageCurrency); siteMapPages(&siteMapPage); customUrls
                           ; $storeId-siteMapConfig ; $storeId-siteMapConfig ; default-siteMapTemplate             ; $siteMapLangCurrMapping                            ; $siteMapPage              ;
########################################################################################################################
# STOP MAKING CHANGES
########################################################################################################################
INSERT_UPDATE SiteTheme; code[unique = true]
                       ; vanmarckeblue

INSERT_UPDATE PromotionGroup; identifier[unique = true]
                            ; $storeId

INSERT_UPDATE CMSSite; uid[unique = true]; name [lang = en]; active; channel(code); ecommerceEnabled; commerceGroup(uid); contentCatalogs(id)                          ; defaultCatalog(id) ; stores(uid); defaultLanguage(isocode); solrFacetSearchConfiguration(name); storeFinderSolrFacetSearchConfiguration(name); categorySolrFacetSearchConfiguration(name); theme(code)   ; previewURL      ; urlEncodingAttributes; urlPatterns ; defaultPromotionGroup(identifier); startNavigationNode(uid, $globalBlueCatalogVersion); siteMapConfig(&siteMapConfigId)
                     ; $storeId          ; $name           ; true  ; DIY          ; true            ; $commerceGroup    ; $globalBlueContentCatalog, $contentCatalogId ; $vmkProductCatalog ; $storeId   ; $defaultLanguage        ; $blueFacetSearchConfig            ; $storeFacetSearchConfig                      ; $categoryFacetSearchConfig                ; vanmarckeblue ; /?site=$storeId ; storefront,language  ; $urlPattern ; $storeId                         ; globalStartNavNode                                 ; $storeId-siteMapConfig

INSERT_UPDATE SiteMapMediaCronJob; code[unique = true]      ; job(code)       ; contentSite(uid); sessionLanguage(isoCode); sessionCurrency(isocode); sessionUser(uid); siteMapUrlLimitPerFile
                                 ; $storeId-siteMapMediaJob ; siteMapMediaJob ; $storeId        ; en                      ; EUR                     ; admin           ; $siteMapUrlLimitPerFile

INSERT_UPDATE CompositeEntry; code[unique = true]           ; executableCronJob(code)  ; compositeCronJob(code)
                            ; $storeId-siteMapMediaJobEntry ; $storeId-siteMapMediaJob ; siteMapMediaCompositeJob

INSERT_UPDATE VMKChannelRestriction;$globalBlueCatalogVersion[unique=true];uid[unique=true];name;channel(code);components(uid, $globalBlueCatalogVersion);pages(uid, $globalBlueCatalogVersion);
;;diyStoreFinderRestriction;DIY Store Finder Restriction;DIY;OurShopsLink,StoreFinderHeaderLink;storefinderPage