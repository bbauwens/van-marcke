import de.hybris.platform.commerceservices.search.pagedata.PageableData

pageableData = new PageableData()
pageableData.setPageSize(1000)
pageableData.setSort("ASC")
pageableData.setCurrentPage(0)

while (searchResult = pagedFlexibleSearchService.search("select {pk} from {Tax}", null, pageableData)) {
    // make sure there is any result
    if (searchResult.results.size() <= 0) {
        break
    }

    // process the results
    for (tax in searchResult.results) {
        String taxCode = tax.getCode();

        if (taxCode.contains("BEBAT")) {
            tax.setType("BEBAT");
        } else if (taxCode.contains("RECUPEL")) {
            tax.setType("RECUPEL");
        } else if (taxCode.contains("ECOMOBILIER")) {
            tax.setType("ECOMOBILIER");
        } else {
                tax.setType("VAT")
        }

        modelService.save(tax);
    }

    // and make sure to move to the next page
    pageableData.setCurrentPage(pageableData.getCurrentPage() + 1)
}

return "Finished updating Taxes"