$solrIndexedType = solrIndexedType(identifier)[unique = true, default = 'BackofficeProduct'];
$solrValueRanges = solrValueRanges(name);
$solrValueRangeSet = solrValueRangeSet(name);
$backofficeSFCName = Solr Config for Backoffice;
$solrIndexedTypes = solrIndexedTypes(identifier);
$solrServerConfig = solrServerConfig(name);
$solrIndexConfig = solrIndexConfig(name);
$typeCode = type(code)[default = 'Product'];
$target = target[allownull = true, default = 'de.hybris.platform.core.model.product.ProductModel'];
$active = active[allownull = true, default = true];

INSERT_UPDATE Domain; code[unique = true]
                    ; backofficeDomain

###### VALIDATION AND DATA QUALITY ######
# Following two constraints groups are also created in backoffice/resources/impex/projectdataValidation.impex
# due to initialization limitation of the addons which are not respecting the required extensions
INSERT_UPDATE ConstraintGroup; id[unique = true]                ; dedicatedTypes(code); interfaceName
                             ; defaultBackofficeValidationGroup ; Item                ; de.hybris.platform.validation.groupinterfaces.DefaultBackofficeValidationGroup
                             ; emptyValidationGroup             ; Item                ; de.hybris.platform.validation.groupinterfaces.EmptyValidationGroup

# PRODUCT DATA QUALITY:
INSERT_UPDATE SolrIndexedType; identifier[unique = true]; type(code)
                             ; BackofficeProduct        ; Product

INSERT_UPDATE SolrValueRangeSet; name[unique = true]         ; type   ; $solrValueRanges                       ;
                               ; backofficeDataQualityRanges ; double ; 0-19%,20-39%,40-59%,60-79%,80-99%,100% ;

INSERT_UPDATE SolrValueRange; name[unique = true]; from   ; to     ; $solrValueRangeSet          ;
                            ; 0-19%              ; 0.0    ; 0.1999 ; backofficeDataQualityRanges ;
                            ; 20-39%             ; 0.1999 ; 0.3999 ; backofficeDataQualityRanges ;
                            ; 40-59%             ; 0.3999 ; 0.5999 ; backofficeDataQualityRanges ;
                            ; 60-79%             ; 0.5999 ; 0.7999 ; backofficeDataQualityRanges ;
                            ; 80-99%             ; 0.7999 ; 0.9999 ; backofficeDataQualityRanges ;
                            ; 100%               ; 0.9999 ; 1.0001 ; backofficeDataQualityRanges ;

INSERT_UPDATE CoverageConstraintGroup; id[unique = true]                   ; dedicatedTypes(code); interfaceName                                                                      ; coverageDomainID                    ;
                                     ; pcmCoreAttributesCoverageGroup      ; Product             ; de.hybris.platform.validation.groupinterfaces.PcmCoreAttributesCoverageGroup       ; pcmCoreAttributesCoverageGroup      ;
                                     ; pcmMarketingAttributesCoverageGroup ; Product             ; de.hybris.platform.validation.groupinterfaces.MarketingCoreAttributesCoverageGroup ; pcmMarketingAttributesCoverageGroup ;

INSERT_UPDATE SolrIndexedProperty; name[unique = true]                 ; $solrIndexedType[unique = true]; type(code); facet[default = true]; localized[default = false]; currency[default = false]; multiValue[default = false]; rangeSet(name)              ; fieldValueProvider                ; includeInResponse[default = false]; facetType(code)[unique = true, default = 'MultiSelectOr']; priority;
                                 ; pcmCoreAttributesCoverageGroup      ;                                ; double    ;                      ;                           ;                          ;                            ; backofficeDataQualityRanges ; coreDataQualityValueResolver      ;                                   ;                                                          ; 400     ;
                                 ; pcmMarketingAttributesCoverageGroup ;                                ; double    ;                      ;                           ;                          ;                            ; backofficeDataQualityRanges ; marketingDataQualityValueResolver ;                                   ;                                                          ; 300     ;


INSERT_UPDATE HybrisEnumValueCodeConstraint; active[allownull = true, default = true]; id[unique = true]                            ; severity(code, itemtype(code)); message[lang = en]                                       ; target[allownull = true]                           ; type(code); value    ; constraintGroups(id)           ; qualifier      ;
                                           ;                                         ; ProductApprovedHybrisEnumValueCodeConstraint ; ERROR:Severity                ; The attribute value for "Approval" should be "approved". ; de.hybris.platform.core.model.product.ProductModel ; Product   ; approved ; pcmCoreAttributesCoverageGroup ; approvalStatus ;

INSERT_UPDATE NotNullConstraint; id[unique = true]                         ; constraintGroups(id)[mode = append] ; severity(code, itemtype(code)); $active; $target; $typeCode; qualifier         ; descriptor(enclosingType(code), qualifier); languages(isocode)[allownull = true];
                               ; ProductNameNotNullConstraint              ; pcmCoreAttributesCoverageGroup      ; ERROR:Severity                ;        ;        ;          ; name              ; Product:name                              ;                                     ;
                               ; ProductUnitNotNullConstraint              ; pcmCoreAttributesCoverageGroup      ; WARN:Severity                 ;        ;        ;          ; unit              ; Product:unit                              ;                                     ;
                               ; ProductSupercategoriesNotNullConstraint   ; pcmCoreAttributesCoverageGroup      ; INFO:Severity                 ;        ;        ;          ; supercategories   ; Product:supercategories                   ;                                     ;
                               ; ProductEurope1PricesNotNullConstraint     ; pcmCoreAttributesCoverageGroup      ; WARN:Severity                 ;        ;        ;          ; europe1Prices     ; Product:europe1Prices                     ;                                     ;
                               ; ProductPriceQuantityNotNullConstraint     ; pcmCoreAttributesCoverageGroup      ; WARN:Severity                 ;        ;        ;          ; priceQuantity     ; Product:priceQuantity                     ;                                     ;
                               ; ProductManufacturerNameNotNullConstraint  ; pcmCoreAttributesCoverageGroup      ; WARN:Severity                 ;        ;        ;          ; manufacturerName  ; Product:manufacturerName                  ;                                     ;
                               ; ProductDeliveryTimeNotNullConstraint      ; pcmCoreAttributesCoverageGroup      ; WARN:Severity                 ;        ;        ;          ; deliveryTime      ; Product:deliveryTime                      ;                                     ;
                               ; ProductThumbnailNotNullConstraint         ; pcmMarketingAttributesCoverageGroup ; WARN:Severity                 ;        ;        ;          ; thumbnail         ; Product:thumbnail                         ;                                     ;
                               ; ProductPictureNotNullConstraint           ; pcmMarketingAttributesCoverageGroup ; ERROR:Severity                ;        ;        ;          ; picture           ; Product:picture                           ;                                     ;
                               ; ProductGalleryImagesNotNullConstraint     ; pcmMarketingAttributesCoverageGroup ; WARN:Severity                 ;        ;        ;          ; galleryImages     ; Product:galleryImages                     ;                                     ;
                               ; ProductProductReferencesNotNullConstraint ; pcmMarketingAttributesCoverageGroup ; INFO:Severity                 ;        ;        ;          ; productReferences ; Product:productReferences                 ;                                     ;
                               ; ProductDescriptionNotNullConstraint       ; pcmMarketingAttributesCoverageGroup ; ERROR:Severity                ;        ;        ;          ; description       ; Product:description                       ;                                     ;
                               ; ProductNameNotNullForAllLocalesConstraint ; pcmMarketingAttributesCoverageGroup ; WARN:Severity                 ;        ;        ;          ; name              ; Product:name                              ; en,de,fr                            ;


INSERT_UPDATE SizeConstraint; id[unique = true]                    ; constraintGroups(id)[mode = append] ; severity(code, itemtype(code)); $active; $target; $typeCode; qualifier         ; descriptor(enclosingType(code), qualifier); min; max        ;
                            ; ProductEanSizeConstraint             ; pcmCoreAttributesCoverageGroup      ; ERROR:Severity                ;        ;        ;          ; ean               ; Product:ean                               ; 8  ; 13         ;
                            ; ProductEurope1PricesSizeConstraint   ; pcmCoreAttributesCoverageGroup      ; WARN:Severity                 ;        ;        ;          ; europe1Prices     ; Product:europe1Prices                     ; 1  ; 2147483647 ;
                            ; ProductSupercategoriesSizeConstraint ; pcmCoreAttributesCoverageGroup      ; INFO:Severity                 ;        ;        ;          ; supercategories   ; Product:supercategories                   ; 1  ; 2147483647 ;
                            ; ProductGalleryImagesSizeConstraint   ; pcmMarketingAttributesCoverageGroup ; WARN:Severity                 ;        ;        ;          ; galleryImages     ; Product:galleryImages                     ; 1  ; 2147483647 ;
                            ; ProductReferencesSizeConstraint      ; pcmMarketingAttributesCoverageGroup ; INFO:Severity                 ;        ;        ;          ; productReferences ; Product:productReferences                 ; 1  ; 2147483647 ;

INSERT_UPDATE NotEmptyConstraint; id[unique = true]                    ; constraintGroups(id)[mode = append]; severity(code, itemtype(code)); $active; $target; $typeCode; qualifier   ; descriptor(enclosingType(code), qualifier);
                                ; ProductCodeNotEmptyConstraint        ; pcmCoreAttributesCoverageGroup     ; ERROR:Severity                ;        ;        ;          ; code        ; Product:code                              ;
                                ; ProductDescriptionNotEmptyConstraint ; pcmCoreAttributesCoverageGroup     ; WARN:Severity                 ;        ;        ;          ; description ; Product:description                       ;
                                ; ProductEanNotEmptyConstraint         ; pcmCoreAttributesCoverageGroup     ; WARN:Severity                 ;        ;        ;          ; ean         ; Product:ean                               ;

INSERT_UPDATE EnumCodeConstraint; id[unique = true]         ; constraintGroups(id)[mode = append]; severity(code, itemtype(code)); $active; $target; $typeCode; qualifier      ; descriptor(enclosingType(code), qualifier); code     ;
                                ; ProductApprovedConstraint ; pcmCoreAttributesCoverageGroup     ; ERROR:Severity                ;        ;        ;          ; approvalStatus ; Product:approvalStatus                    ; approved ;


# ^PRODUCT DATA QUALITY

#simple fields
INSERT_UPDATE SolrIndexedProperty; name[unique = true]         ; $solrIndexedType; type(code); localized[default = false]; currency[default = false]; multiValue[default = false]; includeInResponse[default = false]; useForSpellChecking[default = false];
                                 ; minOrderQuantity            ;                 ; int       ;                           ;                          ;                            ;                                   ;                                     ;
                                 ; maxOrderQuantity            ;                 ; int       ;                           ;                          ;                            ;                                   ;                                     ;
                                 ; priceQuantity               ;                 ; int       ;                           ;                          ;                            ;                                   ;                                     ;
                                 ; manufacturerAID             ;                 ; text      ;                           ;                          ;                            ;                                   ;                                     ;
                                 ; numberContentUnits          ;                 ; double    ;                           ;                          ;                            ;                                   ;                                     ;
                                 ; manufacturerTypeDescription ;                 ; text      ; true                      ;                          ;                            ;                                   ;                                     ;
                                 ; supplierAlternativeAID      ;                 ; text      ;                           ;                          ;                            ;                                   ;                                     ;
                                 ; erpGroupBuyer               ;                 ; text      ;                           ;                          ;                            ;                                   ;                                     ;
                                 ; erpGroupSupplier            ;                 ; text      ;                           ;                          ;                            ;                                   ;                                     ;
                                 ; order                       ;                 ; int       ;                           ;                          ;                            ;                                   ;                                     ;
                                 ; orderQuantityInterval       ;                 ; int       ;                           ;                          ;                            ;                                   ;                                     ;
                                 ; xmlcontent                  ;                 ; text      ;                           ;                          ;                            ;                                   ;                                     ;
                                 ; startLineNumber             ;                 ; int       ;                           ;                          ;                            ;                                   ;                                     ;
                                 ; endLineNumber               ;                 ; int       ;                           ;                          ;                            ;                                   ;                                     ;

#references
INSERT_UPDATE SolrIndexedProperty; name[unique = true]; $solrIndexedType; type(code); localized[default = false]; fieldValueProvider          ; includeInResponse[default = false]; valueProviderParameter;
                                 ; contentUnitPk      ;                 ; long      ;                           ; itemModelPKValueResolver    ;                                   ; contentUnit           ;
                                 ; contentUnit        ;                 ; string    ; true                      ; itemModelLabelValueResolver ;                                   ; contentUnit           ;

INSERT_UPDATE SolrIndexedProperty; name[unique = true]                  ; $solrIndexedType; type(code); localized[default = false]; currency[default = false]; multiValue[default = true]; includeInResponse[default = false]; useForSpellChecking[default = false]; facet[default = false]; fieldValueProvider           ; facetDisplayNameProvider                                  ; sortableType(code); facetSort(code); customFacetSortProvider        ; backofficeDisplayName[lang = en]          ; backofficeDisplayName[lang = es]             ; backofficeDisplayName[lang = de]            ; facetType(code)
                                 ; completeness                         ;                 ; string    ;                           ;                          ;                           ; false                             ;                                     ; true                  ; pcmCompletenessValueResolver ;                                                           ; string            ; Custom         ; facetNameSortProviderAscending ;                                           ;                                              ;                                             ; MultiSelectOr
                                 ; completeness_locale                  ;                 ; string    ;                           ;                          ;                           ;                                   ;                                     ; true                  ; pcmCompletenessValueResolver ; pcmLocaleCompletenessFacetDisplayNameProvider             ; string            ; Custom         ; facetNameSortProviderAscending ; Locale Completeness                       ; Estado Locales                               ; Gebietsvollständigkeit                      ; MultiSelectOr
                                 ; completeness_attribute               ;                 ; string    ;                           ;                          ;                           ;                                   ;                                     ; true                  ; pcmCompletenessValueResolver ; pcmLocalizedAttributeCompletenessFacetDisplayNameProvider ; string            ; Custom         ; facetNameSortProviderAscending ; Attribute Completeness                    ; Completitud Atributos                        ; Attribut Vollständigkeit                    ; MultiSelectOr
                                 ; completeness_locale_classification   ;                 ; string    ;                           ;                          ;                           ;                                   ;                                     ; true                  ; pcmCompletenessValueResolver ; pcmLocaleCompletenessFacetDisplayNameProvider             ; string            ; Custom         ; facetNameSortProviderAscending ; Locale Classification System Completeness ; Completitud Locales Sistema de Clasificación ; Gebietsschema-System Vollständigkeit        ; MultiSelectOr
                                 ; completeness_classification          ;                 ; string    ;                           ;                          ;                           ;                                   ;                                     ; true                  ; pcmCompletenessValueResolver ; pcmClassificationSystemFacetDisplayNameProvider           ; string            ; Custom         ; facetNameSortProviderAscending ; Classification System Completeness        ; Completitud Sistema de Clasificación         ; Vollständigkeit des Klassifizierungssystems ; MultiSelectOr
                                 ; completeness_classification_category ;                 ; string    ;                           ;                          ;                           ;                                   ;                                     ; true                  ; pcmCompletenessValueResolver ; pcmClassificationCategoryFacetDisplayNameProvider         ; string            ; Custom         ; facetNameSortProviderAscending ; Classification Category Completeness      ; Completitud Categoría de Clasificación       ; Klassifizierung Kategorie Vollständigkeit   ; MultiSelectOr
                                 ; completeness_feature                 ;                 ; string    ;                           ;                          ;                           ;                                   ;                                     ; true                  ; pcmCompletenessValueResolver ; pcmLocalizedFeatureCompletenessFacetDisplayNameProvider   ; string            ; Custom         ; facetNameSortProviderAscending ; Feature Completeness                      ; Completitud Features                         ; Feature Vollständigkeit                     ; MultiSelectOr
                                 ; completeness_product                 ;                 ; float     ;                           ;                          ; false                     ;                                   ;                                     ; true                  ; pcmCompletenessValueResolver ;                                                           ; string            ; Custom         ; facetNameSortProviderAscending ; Product Completeness                      ; Completitud de Producto                      ; Produkt Vollständigkeit                     ; MultiSelectOr