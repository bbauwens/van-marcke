package com.vanmarcke.occaddon.filter;

import com.vanmarcke.occaddon.exception.InvalidResourceException;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.webservicescommons.util.YSanitizer;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * The {@link VMKBaseSiteMatchingFilter} class is used to overwrite the
 * {@code de.hybris.platform.ycommercewebservices.v2.filter.BaseSiteMatchingFilter} class.
 *
 * @author Christiaan Janssen
 * @since 13-01-2021
 */
public class VMKBaseSiteMatchingFilter extends VMKAbstractUrlMatchingFilter {

    private final String regexp;
    private final BaseSiteService baseSiteService;

    /**
     * Creates a new instance of the {@link VMKBaseSiteMatchingFilter} class.
     *
     * @param regexp          the regular expression which activates the filter
     * @param baseSiteService the base site service
     */
    public VMKBaseSiteMatchingFilter(String regexp, BaseSiteService baseSiteService) {
        this.regexp = regexp;
        this.baseSiteService = baseSiteService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response,
                                    FilterChain filterChain) throws ServletException, IOException {
        String baseSiteID = getBaseSiteValue(request, regexp);
        if (baseSiteID != null) {
            BaseSiteModel requestedBaseSite = baseSiteService.getBaseSiteForUID(baseSiteID);
            if (requestedBaseSite != null) {
                BaseSiteModel currentBaseSite = baseSiteService.getCurrentBaseSite();
                if (!requestedBaseSite.equals(currentBaseSite)) {
                    baseSiteService.setCurrentBaseSite(requestedBaseSite, true);
                }
            } else {
                throw new InvalidResourceException(YSanitizer.sanitize(baseSiteID));
            }
        }

        filterChain.doFilter(request, response);
    }
}
