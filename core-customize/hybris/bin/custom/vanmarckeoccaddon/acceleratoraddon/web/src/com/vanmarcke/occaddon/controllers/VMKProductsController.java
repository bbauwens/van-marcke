package com.vanmarcke.occaddon.controllers;

import com.vanmarcke.facades.product.VMKProductFacade;
import com.vanmarcke.facades.product.data.PriceListData;
import com.vanmarcke.facades.product.data.StockListData;
import com.vanmarcke.facades.stock.VMKStockFacade;
import com.vanmarcke.occaddon.constants.VanmarckeoccaddonWebConstants;
import com.vanmarcke.occaddon.dto.product.data.PriceListWsDTO;
import com.vanmarcke.occaddon.dto.product.data.StockListWsDTO;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.request.mapping.annotation.ApiVersion;
import de.hybris.platform.commerceservices.request.mapping.annotation.RequestMappingOverride;
import de.hybris.platform.commercewebservicescommons.dto.product.ProductWsDTO;
import de.hybris.platform.webservicescommons.cache.CacheControl;
import de.hybris.platform.webservicescommons.cache.CacheControlDirective;
import de.hybris.platform.webservicescommons.swagger.ApiBaseSiteIdAndUserIdParam;
import de.hybris.platform.webservicescommons.swagger.ApiBaseSiteIdParam;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.EnumSet;

/**
 * Extension of the products endpoint originally described in de.hybris.platform.ycommercewebservices.v2.controller.ProductsController
 *
 * @author marteto
 */
@RestController
@RequestMapping(value = "/{baseSiteId}")
@Api(tags = {"Products", "VMKApi", "Prices"})
@ApiVersion("v2")
public class VMKProductsController extends VMKBaseController {

    private static final EnumSet<ProductOption> PRODUCT_OPTIONS_SET = EnumSet.of(ProductOption.BASIC,
            ProductOption.URL, ProductOption.PRICE, ProductOption.SUMMARY, ProductOption.DESCRIPTION, ProductOption.GALLERY,
            ProductOption.CATEGORIES, ProductOption.CLASSIFICATION, ProductOption.VARIANT_FULL,
            ProductOption.DOCUMENTS, ProductOption.BRAND, ProductOption.PROMOTIONS,
            ProductOption.REFERENCE_CONSISTS_OF, ProductOption.SUPPLIER_REFERENCE, ProductOption.BARCODES);

    @Resource(name = "blueProductFacade")
    private VMKProductFacade vmkProductFacade;

    @Resource(name = "stockFacade")
    private VMKStockFacade vmkStockFacade;

    @Secured({VanmarckeoccaddonWebConstants.ROLE_CUSTOMERGROUP, VanmarckeoccaddonWebConstants.ROLE_CUSTOMERMANAGERGROUP, VanmarckeoccaddonWebConstants.ROLE_TRUSTED_CLIENT})
    @RequestMapping(value = "/users/{userId}/products/{productCode}/prices", method = RequestMethod.GET)
    @ApiOperation(nickname = "getPrice", value = "Get product LIST and NET price.", notes = "Returns the LIST and NET price of a single product according to a product code.")
    @ApiBaseSiteIdAndUserIdParam
    public PriceListWsDTO getPrice(@ApiParam(value = "Product identifier", required = true) @PathVariable final String productCode) {
        PriceListData priceDataList = vmkProductFacade.getPricesForProduct(productCode);
        return dataMapper.map(priceDataList, PriceListWsDTO.class, DEFAULT_FIELD_SET);
    }

    @Secured({VanmarckeoccaddonWebConstants.ROLE_CUSTOMERGROUP, VanmarckeoccaddonWebConstants.ROLE_CUSTOMERMANAGERGROUP, VanmarckeoccaddonWebConstants.ROLE_TRUSTED_CLIENT})
    @RequestMappingOverride(priorityProperty = "99")
    @RequestMapping(value = "/products/{productCode}/stock/{storeName}", method = RequestMethod.GET)
    @ApiOperation(nickname = "getProductStock", value = "Get a product's stock level.", notes = "Returns a product's stock levels based on the product code.")
    @ApiBaseSiteIdParam
    public StockListWsDTO getStock(@ApiParam(value = "Product code", required = true) @PathVariable final String productCode,
                                   @ApiParam(value = "Store identifier", required = true) @PathVariable final String storeName,
                                   @RequestParam(defaultValue = DEFAULT_FIELD_SET) final String fields) {
        StockListData stockData = vmkStockFacade.getStockForProductCodeAndWarehouse(productCode, storeName);
        return dataMapper.map(stockData, StockListWsDTO.class, fields);
    }

    @Secured({VanmarckeoccaddonWebConstants.ROLE_CUSTOMERGROUP, VanmarckeoccaddonWebConstants.ROLE_CUSTOMERMANAGERGROUP, VanmarckeoccaddonWebConstants.ROLE_TRUSTED_CLIENT})
    @RequestMappingOverride(priorityProperty = "99")
    @RequestMapping(value = "/products/{productCode}/stock", method = RequestMethod.GET)
    @ApiOperation(nickname = "getProductStock", value = "Get a product's stock level.", notes = "Returns a product's stock levels based on the product code.")
    @ApiBaseSiteIdParam
    public StockListWsDTO getStock(@ApiParam(value = "Product code", required = true) @PathVariable final String productCode,
                                   @RequestParam(defaultValue = DEFAULT_FIELD_SET) final String fields) {
        StockListData stockData = vmkStockFacade.getStockForProductCodeAndCurrentBaseStore(productCode);
        return dataMapper.map(stockData, StockListWsDTO.class, fields);
    }
}
