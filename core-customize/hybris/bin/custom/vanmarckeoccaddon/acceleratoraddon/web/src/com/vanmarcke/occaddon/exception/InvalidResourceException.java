package com.vanmarcke.occaddon.exception;

import javax.servlet.ServletException;

/**
 * The {@link InvalidResourceException} class is used to overwrite the
 * {@code de.hybris.platform.ycommercewebservices.exceptions.InvalidResourceException} class.
 *
 * @author Christiaan Janssen
 * @since 13-01-2021
 */
public class InvalidResourceException extends ServletException {

    /**
     * Creates a new instance of the {@link InvalidResourceException} class.
     *
     * @param baseSiteUid the base site UID
     */
    public InvalidResourceException(String baseSiteUid) {
        super("Base site " + baseSiteUid + " doesn't exist");
    }
}
