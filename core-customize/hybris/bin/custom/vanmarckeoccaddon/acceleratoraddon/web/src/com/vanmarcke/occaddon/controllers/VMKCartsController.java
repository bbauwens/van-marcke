package com.vanmarcke.occaddon.controllers;

import com.vanmarcke.facades.cart.VMKCartFacade;
import com.vanmarcke.occaddon.constants.VanmarckeoccaddonWebConstants;
import com.vanmarcke.services.exception.UnsupportedDeliveryDateException;
import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.request.mapping.annotation.RequestMappingOverride;
import de.hybris.platform.commercewebservicescommons.annotation.SiteChannelRestriction;
import de.hybris.platform.commercewebservicescommons.dto.order.CartModificationWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.order.CartWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.order.OrderEntryWsDTO;
import de.hybris.platform.webservicescommons.cache.CacheControl;
import de.hybris.platform.webservicescommons.cache.CacheControlDirective;
import de.hybris.platform.webservicescommons.errors.exceptions.WebserviceValidationException;
import de.hybris.platform.webservicescommons.mapping.FieldSetLevelHelper;
import de.hybris.platform.webservicescommons.swagger.ApiBaseSiteIdUserIdAndCartIdParam;
import de.hybris.platform.webservicescommons.swagger.ApiFieldsParam;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang.StringUtils;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

/**
 * The {@link VMKCartsController} class contains the RESTful endpoints to create, update and delete shopping carts.
 *
 * @author Christiaan Janssen
 * @since 26-08-2021
 */
@Controller
@RequestMapping(value = "/{baseSiteId}/users/{userId}/carts")
@CacheControl(directive = CacheControlDirective.NO_CACHE)
@Api(tags = "Carts")
public class VMKCartsController extends VMKBaseController {

    private static final long DEFAULT_PRODUCT_QUANTITY = 1;

    private final Validator orderEntryCreateValidator;
    private final VMKCartFacade vmkCartFacade;
    private final CartFacade commerceWebServicesCartFacade2;

    /**
     * Creates a new instance of the {@link VMKCartsController} class.
     *
     * @param orderEntryCreateValidator      the order entry create validator
     * @param vmkCartFacade                  the cart facade (main)
     * @param commerceWebServicesCartFacade2 the cart facade (webservices)
     */
    public VMKCartsController(final Validator orderEntryCreateValidator,
                              final VMKCartFacade vmkCartFacade,
                              final CartFacade commerceWebServicesCartFacade2) {
        this.orderEntryCreateValidator = orderEntryCreateValidator;
        this.vmkCartFacade = vmkCartFacade;
        this.commerceWebServicesCartFacade2 = commerceWebServicesCartFacade2;
    }

    @Deprecated(since = "2005", forRemoval = true)
    @PostMapping(value = "/{cartId}/entries")
    @RequestMappingOverride(priorityProperty = "99")
    @ResponseBody
    @SiteChannelRestriction(allowedSiteChannelsProperty = API_COMPATIBILITY_B2C_CHANNELS)
    @ApiOperation(hidden = true, value = "Adds a product to the cart.", notes = "Adds a product to the cart.")
    @ApiBaseSiteIdUserIdAndCartIdParam
    public CartModificationWsDTO createCartEntry(
            @ApiParam(value = "Base site identifier.", required = true) @PathVariable final String baseSiteId,
            @ApiParam(value = "Code of the product to be added to cart. Product look-up is performed for the current product catalog version.", required = true) @RequestParam final String code,
            @ApiParam(value = "Quantity of product.") @RequestParam(defaultValue = "1") final long qty,
            @ApiParam(value = "Name of the store where product will be picked. Set only if want to pick up from a store.") @RequestParam(required = false) final String pickupStore,
            @ApiParam(value = "Response configuration. This is the list of fields that should be returned in the response body.", allowableValues = "BASIC, DEFAULT, FULL") @RequestParam(defaultValue = DEFAULT_FIELD_SET) final String fields)
            throws CommerceCartModificationException {
        throw new CommerceCartModificationException("Deprecated");
    }

    @Secured({VanmarckeoccaddonWebConstants.ROLE_CUSTOMERGROUP, VanmarckeoccaddonWebConstants.ROLE_CUSTOMERMANAGERGROUP, VanmarckeoccaddonWebConstants.ROLE_TRUSTED_CLIENT})
    @PostMapping(value = "/{cartId}/entries", consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    @RequestMappingOverride(priorityProperty = "99")
    @ResponseBody
    @SiteChannelRestriction(allowedSiteChannelsProperty = API_COMPATIBILITY_B2C_CHANNELS)
    @ApiOperation(nickname = "createCartEntry", value = "Adds a product to the cart.", notes = "Adds a product to the cart.")
    @ApiBaseSiteIdUserIdAndCartIdParam
    public CartModificationWsDTO createCartEntry(
            @ApiParam(value = "Base site identifier", required = true) @PathVariable final String baseSiteId,
            @ApiParam(value = "Request body parameter that contains details such as the product code (product.code), the quantity of product (quantity), and the pickup store name (deliveryPointOfService.name).\n\nThe DTO is in XML or .json format.", required = true) @RequestBody final OrderEntryWsDTO entry,
            @ApiParam(value = "Response configuration. This is the list of fields that should be returned in the response body.", allowableValues = "BASIC, DEFAULT, FULL") @RequestParam(defaultValue = DEFAULT_FIELD_SET) final String fields)
            throws CommerceCartModificationException {
        if (entry.getQuantity() == null) {
            entry.setQuantity(DEFAULT_PRODUCT_QUANTITY);
        }

        validate(entry, "entry", orderEntryCreateValidator);

        final String pickupStore = entry.getDeliveryPointOfService() == null ? null : entry.getDeliveryPointOfService().getName();
        return addCartEntryInternal(entry.getProduct().getCode(), entry.getQuantity(), pickupStore, fields);
    }

    /**
     * Validates the given {@code object}.
     *
     * @param object     the object to validate
     * @param objectName the name of the object to validate
     * @param validator  the validator to use
     */
    protected void validate(final Object object,
                            final String objectName,
                            final Validator validator) {
        final Errors errors = new BeanPropertyBindingResult(object, objectName);
        validator.validate(object, errors);
        if (errors.hasErrors()) {
            throw new WebserviceValidationException(errors);
        }
    }

    /**
     * Adds a new entry to the shopping cart.
     *
     * @param code        the product's unique identifier
     * @param qty         the desired quantity
     * @param pickupStore the point of service's unique identifier
     * @param fields      the fields to expose in the HTTP response
     * @return the cart modification response
     * @throws CommerceCartModificationException in case something goes wrong while adding the new entry
     */
    protected CartModificationWsDTO addCartEntryInternal(final String code,
                                                         final long qty,
                                                         final String pickupStore,
                                                         final String fields) throws CommerceCartModificationException {
        final CartModificationData cartModificationData;
        if (StringUtils.isNotEmpty(pickupStore)) {
            cartModificationData = commerceWebServicesCartFacade2.addToCart(code, qty, pickupStore);
        } else {
            cartModificationData = commerceWebServicesCartFacade2.addToCart(code, qty);
        }
        return dataMapper.map(cartModificationData, CartModificationWsDTO.class, fields);
    }

    @Secured({VanmarckeoccaddonWebConstants.ROLE_CUSTOMERGROUP, VanmarckeoccaddonWebConstants.ROLE_CUSTOMERMANAGERGROUP, VanmarckeoccaddonWebConstants.ROLE_TRUSTED_CLIENT})
    @RequestMapping(value = "/{cartId}/requestedDeliveryDate", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(nickname = "setRequestedDeliveryDate", value = "Sets the delivery date for a cart.", notes = "Sets the delivery date for the cart.")
    @ApiBaseSiteIdUserIdAndCartIdParam
    public @ResponseBody
    CartWsDTO setRequestedDeliveryDate(
            @ApiParam(value = "Requested delivery date", required = true) @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) final Date requestedDeliveryDate,
            @ApiFieldsParam @RequestParam(required = false, defaultValue = FieldSetLevelHelper.DEFAULT_LEVEL) final String fields)
            throws UnsupportedDeliveryDateException {
        final CartData cartData = vmkCartFacade.setRequestedDeliveryDate(requestedDeliveryDate);
        return dataMapper.map(cartData, CartWsDTO.class, fields);
    }
}
