package com.vanmarcke.occaddon.filter;

import de.hybris.platform.servicelayer.internal.i18n.I18NConstants;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.ycommercewebservices.context.ContextInformationLoader;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * The {@link VMKSessionAttributesFilter} class is used to overwrite the default {@code SessionAttributesFilter} class.
 *
 * @author Christiaan Janssen
 * @since 12-01-2020
 */
public class VMKSessionAttributesFilter extends OncePerRequestFilter {

    private final ContextInformationLoader contextInformationLoader;
    private final SessionService sessionService;

    /**
     * Creates a new instance of the {@link VMKSessionAttributesFilter} class.
     *
     * @param contextInformationLoader the context information loader
     * @param sessionService           the session service
     */
    public VMKSessionAttributesFilter(final ContextInformationLoader contextInformationLoader,
                                      final SessionService sessionService) {
        this.contextInformationLoader = contextInformationLoader;
        this.sessionService = sessionService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void doFilterInternal(final HttpServletRequest request,
                                    final HttpServletResponse response,
                                    final FilterChain filterChain) throws ServletException, IOException {
        contextInformationLoader.setLanguageFromRequest(request);
        contextInformationLoader.setCurrencyFromRequest(request);
        sessionService.setAttribute("enable.language.fallback.serviceLayer", true);
        sessionService.setAttribute(I18NConstants.LANGUAGE_FALLBACK_ENABLED, true);
        filterChain.doFilter(request, response);
    }
}
