package com.vanmarcke.occaddon.filter;

import com.vanmarcke.occaddon.exception.InvalidResourceException;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.site.BaseSiteService;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * The {@link VMKBaseSiteMatchingFilterTest} class contains the unit tests for the
 * {@link VMKBaseSiteMatchingFilter} class.
 *
 * @author Christiaan Janssen
 * @since 13-01-2021
 */
@UnitTest
public class VMKBaseSiteMatchingFilterTest {

    private static final String SITE_UID = RandomStringUtils.randomAlphabetic(10);
    private static final String PATH = "/" + SITE_UID;
    private static final String REGEX = "^" + PATH + "$";

    @Mock
    private BaseSiteService baseSiteService;

    private VMKBaseSiteMatchingFilter baseSiteMatchingFilter;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setUp() {
        initMocks(this);
        baseSiteMatchingFilter = new VMKBaseSiteMatchingFilter(REGEX, baseSiteService);
    }

    @Test
    public void testDoFilterInternal_withoutSiteUID() throws ServletException, IOException {
        // given
        HttpServletRequest request = mock(HttpServletRequest.class);
        given(request.getPathInfo()).willReturn("/basesites");

        HttpServletResponse response = mock(HttpServletResponse.class);

        FilterChain filterChain = mock(FilterChain.class);

        // when
        baseSiteMatchingFilter.doFilterInternal(request, response, filterChain);

        // then
        verify(baseSiteService, never()).getBaseSiteForUID(anyString());
        verify(baseSiteService, never()).getCurrentBaseSite();
        verify(baseSiteService, never()).setCurrentBaseSite(any(BaseSiteModel.class), eq(true));
        verify(filterChain).doFilter(request, response);
    }

    @Test
    public void testDoFilterInternal_withoutInvalidSiteUID() throws ServletException, IOException {
        // given
        HttpServletRequest request = mock(HttpServletRequest.class);
        given(request.getPathInfo()).willReturn(PATH);

        HttpServletResponse response = mock(HttpServletResponse.class);

        FilterChain filterChain = mock(FilterChain.class);

        // then
        thrown.expect(InvalidResourceException.class);
        thrown.expectMessage("Base site " + SITE_UID + " doesn't exist");

        // when
        baseSiteMatchingFilter.doFilterInternal(request, response, filterChain);
    }

    @Test
    public void testDoFilterInternal_withValidSiteUID_isEqualToCurrentSite() throws ServletException, IOException {
        // given
        HttpServletRequest request = mock(HttpServletRequest.class);
        given(request.getPathInfo()).willReturn(PATH);

        HttpServletResponse response = mock(HttpServletResponse.class);

        FilterChain filterChain = mock(FilterChain.class);

        BaseSiteModel baseSiteModel = mock(BaseSiteModel.class);
        given(baseSiteService.getBaseSiteForUID(SITE_UID)).willReturn(baseSiteModel);
        given(baseSiteService.getCurrentBaseSite()).willReturn(baseSiteModel);

        // when
        baseSiteMatchingFilter.doFilterInternal(request, response, filterChain);

        // then
        verify(baseSiteService).getBaseSiteForUID(SITE_UID);
        verify(baseSiteService).getCurrentBaseSite();
        verify(baseSiteService, never()).setCurrentBaseSite(any(BaseSiteModel.class), eq(true));
        verify(filterChain).doFilter(request, response);
    }

    @Test
    public void testDoFilterInternal_withValidSiteUID_isDifferentToCurrentSite() throws ServletException, IOException {
        // given
        HttpServletRequest request = mock(HttpServletRequest.class);
        given(request.getPathInfo()).willReturn(PATH);

        HttpServletResponse response = mock(HttpServletResponse.class);

        FilterChain filterChain = mock(FilterChain.class);

        BaseSiteModel baseSiteModel = mock(BaseSiteModel.class);
        given(baseSiteService.getBaseSiteForUID(SITE_UID)).willReturn(baseSiteModel);

        BaseSiteModel anotherBaseSiteModel = mock(BaseSiteModel.class);
        given(baseSiteService.getCurrentBaseSite()).willReturn(anotherBaseSiteModel);

        // when
        baseSiteMatchingFilter.doFilterInternal(request, response, filterChain);

        // then
        verify(baseSiteService).getBaseSiteForUID(SITE_UID);
        verify(baseSiteService).getCurrentBaseSite();
        verify(baseSiteService).setCurrentBaseSite(baseSiteModel, true);
        verify(filterChain).doFilter(request, response);
    }
}