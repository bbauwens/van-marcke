package com.vanmarcke.occaddon.constants;

/**
 * Global class for all vanmarckeoccaddon constants. You can add global constants for your extension into this class.
 */
@SuppressWarnings({"deprecation", "squid:CallToDeprecatedMethod"})
public final class VanmarckeoccaddonConstants extends GeneratedVanmarckeoccaddonConstants {

    public static final String EXTENSIONNAME = "vanmarckeoccaddon"; //NOSONAR

    private VanmarckeoccaddonConstants() {
        //empty to avoid instantiating this constant class
    }

    // implement here constants used by this extension
}
