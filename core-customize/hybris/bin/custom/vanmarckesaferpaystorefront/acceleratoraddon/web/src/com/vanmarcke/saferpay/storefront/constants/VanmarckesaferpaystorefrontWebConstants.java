package com.vanmarcke.saferpay.storefront.constants;

/**
 * Global class for all Vanmarckesaferpaystorefront web constants. You can add global constants for your extension into this class.
 */
public final class VanmarckesaferpaystorefrontWebConstants // NOSONAR
{
    private VanmarckesaferpaystorefrontWebConstants() {
        //empty to avoid instantiating this constant class
    }
}