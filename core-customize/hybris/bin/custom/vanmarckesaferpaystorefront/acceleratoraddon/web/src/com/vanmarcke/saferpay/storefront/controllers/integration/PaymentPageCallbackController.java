package com.vanmarcke.saferpay.storefront.controllers.integration;

import com.vanmarcke.saferpay.facades.VMKPaymentFacade;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractCheckoutController;
import de.hybris.platform.commercefacades.order.data.OrderData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.Resource;

import static de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages.ERROR_MESSAGES_HOLDER;
import static de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages.addFlashMessage;

/**
 * Controller to handle merchant callbacks from the Payment Page Interface
 */
@Controller("saferpayPaymentPageController")
@RequestMapping(value = "/checkout/payment/saferpay")
public class PaymentPageCallbackController extends AbstractCheckoutController {

    private static final Logger LOGGER = LoggerFactory.getLogger(PaymentPageCallbackController.class);

    private static final String REDIRECT_URL_CHECKOUT_SUMMARY = REDIRECT_PREFIX + "/checkout/multi/summary/view";

    @Resource(name = "vmkPaymentFacade")
    private VMKPaymentFacade paymentFacade;

    @GetMapping(value = "/success/{token}")
    @RequireHardLogIn
    public String onSuccess(@PathVariable String token, RedirectAttributes redirectAttributes) {
        OrderData orderData;
        try {
            orderData = paymentFacade.processPaymentConfirmation(token);
        } catch (Exception e) {
            //todo: inform client of error -> done: error message exception
            //todo: provide error message in correct impex file -> key: checkout.severe.error.process.payment
            //todo: inform order manager
            addFlashMessage(redirectAttributes, ERROR_MESSAGES_HOLDER, e.getMessage());
            return REDIRECT_URL_CHECKOUT_SUMMARY;
        }
        return redirectToOrderConfirmationPage(orderData);
    }

    @RequestMapping(value = "/fail/{token}", method = RequestMethod.GET)
    @RequireHardLogIn
    public String onFail(@PathVariable String token, RedirectAttributes redirectAttributes) {
        LOGGER.debug(String.format("Saferpay: Fail called for order with token: %s", token));
        return REDIRECT_URL_CHECKOUT_SUMMARY;
    }

    @RequestMapping(value = "/abort/{token}", method = RequestMethod.GET)
    @RequireHardLogIn
    public String onAbort(@PathVariable String token) {
        LOGGER.debug(String.format("Saferpay: Abort called for order with token: %s", token));
        return REDIRECT_URL_CHECKOUT_SUMMARY;
    }

}