package com.vanmarcke.saferpay.storefront.controllers.integration;

import com.vanmarcke.saferpay.facades.PaymentNotificationFacade;
import com.vanmarcke.saferpay.model.SaferpayNotificationModel;
import de.hybris.bootstrap.annotations.UnitTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class PaymentNotificationCallbackControllerTest {

    @Mock
    private PaymentNotificationFacade paymentNotificationFacade;
    @InjectMocks
    private PaymentNotificationCallbackController paymentNotificationCallbackController;

    @Test
    public void testGetNotification() {
        String orderID = "1234";
        SaferpayNotificationModel saferpayNotification = mock(SaferpayNotificationModel.class);

        when(paymentNotificationFacade.createNotificationForCartToken(orderID)).thenReturn(saferpayNotification);
        paymentNotificationCallbackController.getNotification(orderID);

        verify(paymentNotificationFacade).createNotificationForCartToken(orderID);
    }
}