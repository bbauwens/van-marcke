package com.vanmarcke.saferpay.storefront.constants;

/**
 * Global class for all Vanmarckesaferpaystorefront constants. You can add global constants for your extension into this class.
 */
@SuppressWarnings("deprecation")
public final class VanmarckesaferpaystorefrontConstants extends GeneratedVanmarckesaferpaystorefrontConstants {
    public static final String EXTENSIONNAME = "vanmarckesaferpaystorefront";

    private VanmarckesaferpaystorefrontConstants() {
        //empty to avoid instantiating this constant class
    }
}