/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.vanmarcke.cpi.exchange.constants;

/**
 * Global class for all Vanmarckecpiexchange constants. You can add global constants for your extension into this class.
 */
public final class VanmarckecpiexchangeConstants extends GeneratedVanmarckecpiexchangeConstants {
	public static final String EXTENSIONNAME = "vanmarckecpiexchange";

	private VanmarckecpiexchangeConstants() {
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension

}
