package com.vanmarcke.cpi.exchange.productavailability.impl;

import com.vanmarcke.facades.product.data.StockListData;
import com.vanmarcke.services.product.VMKStockService;
import de.hybris.platform.core.model.product.ProductModel;
import org.apache.commons.lang.NotImplementedException;

/**
 * Provides implementations for the {@link VMKStockService}.
 *
 * @author Tom van den Berg
 * @since 18-12-2020
 */
public class VMKSapStockServiceImpl implements VMKStockService {

    /**
     * {@inheritDoc}
     */
    @Override
    public StockListData getStockForProductAndWarehouses(final ProductModel product,
                                                         final String[] warehouseIdentifiers) {
        throw new NotImplementedException();
    }
}
