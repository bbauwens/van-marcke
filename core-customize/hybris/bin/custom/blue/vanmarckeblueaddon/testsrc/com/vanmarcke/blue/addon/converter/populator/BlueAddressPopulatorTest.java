package com.vanmarcke.blue.addon.converter.populator;

import com.vanmarcke.blue.addon.form.BlueAddressForm;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.RegionData;
import org.apache.commons.lang3.RandomStringUtils;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

/**
 * The {@code BlueAddressPopulatorTest} class contains the unit tests for the {@link BlueAddressPopulator} class.
 *
 * @author Christiaan Janssen
 * @since 23/09/2020
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class BlueAddressPopulatorTest {

    private static final String ADDRESS_ID = RandomStringUtils.random(10);
    private static final String TITLE_CODE = RandomStringUtils.random(10);
    private static final String FIRST_NAME = RandomStringUtils.random(10);
    private static final String LAST_NAME = RandomStringUtils.random(10);
    private static final String LINE1 = RandomStringUtils.random(10);
    private static final String LINE2 = RandomStringUtils.random(10);
    private static final String APARTMENT = RandomStringUtils.random(10);
    private static final String TOWN = RandomStringUtils.random(10);
    private static final String POSTAL_CODE = RandomStringUtils.random(10);
    private static final String PHONE = RandomStringUtils.random(10);
    private static final String MOBILE = RandomStringUtils.random(10);
    private static final String COUNTRY_ISO = RandomStringUtils.random(10);
    private static final String REGION_ISO = RandomStringUtils.random(10);

    @InjectMocks
    private BlueAddressPopulator addressPopulator;

    @Test
    public void testPopulate() {
        CountryData country = new CountryData();
        country.setIsocode(COUNTRY_ISO);

        RegionData region = new RegionData();
        region.setIsocode(REGION_ISO);

        AddressData address = new AddressData();
        address.setId(ADDRESS_ID);
        address.setTitleCode(TITLE_CODE);
        address.setFirstName(FIRST_NAME);
        address.setLastName(LAST_NAME);
        address.setLine1(LINE1);
        address.setLine2(LINE2);
        address.setApartment(APARTMENT);
        address.setTown(TOWN);
        address.setPostalCode(POSTAL_CODE);
        address.setPhone(PHONE);
        address.setMobile(MOBILE);
        address.setCountry(country);
        address.setRegion(region);
        address.setShippingAddress(true);
        address.setDefaultAddress(Boolean.TRUE);

        BlueAddressForm form = new BlueAddressForm();

        addressPopulator.populate(address, form);

        Assertions
                .assertThat(form.getAddressId())
                .isEqualTo(ADDRESS_ID);

        Assertions
                .assertThat(form.getTitleCode())
                .isEqualTo(TITLE_CODE);

        Assertions
                .assertThat(form.getFirstName())
                .isEqualTo(FIRST_NAME);

        Assertions
                .assertThat(form.getLastName())
                .isEqualTo(LAST_NAME);

        Assertions
                .assertThat(form.getLine1())
                .isEqualTo(LINE1);

        Assertions
                .assertThat(form.getLine2())
                .isEqualTo(LINE2);

        Assertions
                .assertThat(form.getApartment())
                .isEqualTo(APARTMENT);

        Assertions
                .assertThat(form.getTownCity())
                .isEqualTo(TOWN);

        Assertions
                .assertThat(form.getPostcode())
                .isEqualTo(POSTAL_CODE);

        Assertions
                .assertThat(form.getBillingAddress())
                .isFalse();

        Assertions
                .assertThat(form.getShippingAddress())
                .isTrue();

        Assertions
                .assertThat(form.getPhone())
                .isEqualTo(PHONE);

        Assertions
                .assertThat(form.getMobile())
                .isEqualTo(MOBILE);

        Assertions
                .assertThat(form.getCountryIso())
                .isEqualTo(COUNTRY_ISO);

        Assertions
                .assertThat(form.getRegionIso())
                .isEqualTo(REGION_ISO);

        Assertions
                .assertThat(form.getDefaultAddress())
                .isTrue();
    }
}