package com.vanmarcke.blue.addon.integration.pickup;

import com.vanmarcke.blue.addon.integration.pickup.builders.PickupDateResponseDataBuilder;
import com.vanmarcke.cpi.data.order.PickupDateResponseData;

import static com.vanmarcke.blue.addon.integration.DeliveryIntegrationTestingConstants.ResponseCode;
import static com.vanmarcke.blue.addon.integration.DeliveryIntegrationTestingConstants.ResponseDate;
import static com.vanmarcke.blue.addon.integration.pickup.PickupDummyEntries.TEC_X;

public class PickupdateResponseCases {

    public static PickupDateResponseData CASE1() {
        return PickupDateResponseDataBuilder
                .aMockPickupResponse()
                .withResponseCode(ResponseCode.OK)
                .withPickupDate(ResponseDate.FUTURE_DATE)
                .withTecCollectDate()
                .withProducts(TEC_X(), TEC_X())
                .build();
    }
}
