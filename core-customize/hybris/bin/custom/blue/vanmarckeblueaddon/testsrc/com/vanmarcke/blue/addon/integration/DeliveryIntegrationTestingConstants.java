package com.vanmarcke.blue.addon.integration;

import org.joda.time.LocalDate;

import java.util.Date;

/**
 * Class for all DeliveryIntegrationTestingConstants constants.
 */
public final class DeliveryIntegrationTestingConstants {

    public static final String EXTENSIONNAME = "vanmarckeblueaddon";

    /**
     * Defines static variables for commonly used date values.
     */
    public interface ResponseDate {
        Date FUTURE_DATE = LocalDate.now().plusDays(3).toDate();
        Date PAST_DATE = LocalDate.now().minusDays(3).toDate();
        Date TODAY = LocalDate.now().toDate();
    }

    /**
     * Defines the possible EDC stock indication types.
     */
    public interface EdcStockIndication {
        //Ok, possible on requested date
        String X = "X";
        //Item not in warehouse and future delivery date is known(shipping) (cf. data.pickupDate)
        String Y = "Y";
        //Item not in warehouse and future delivery date is known (pickup)  (cf. data.pickupDate)
        String C = "C";
        //Cross-dock/direct-supply item. Only possible on first delivery in the VAM
        String F = "F";
        //Item not in warehouse and future delivery date unknown
        String L = "L";
    }

    /**
     * Defines the possible response codes.
     */
    public interface ResponseCode {
        //OK : pickup is possible on the requested date
        String OK = "OK";
        //NOK : pickup is possible later than the requested date
        String NOK = "NOK";
        //UNKNOWN : pickup date is unknown
        String UNKNOWN = "UNKNOWN";
    }

    public interface DummyProductCodes {
        String product1 = "123456";
        String product2 = "234567";
        String product3 = "345678";
    }

    private DeliveryIntegrationTestingConstants() {
        //empty to avoid instantiating this constant class
    }
}


