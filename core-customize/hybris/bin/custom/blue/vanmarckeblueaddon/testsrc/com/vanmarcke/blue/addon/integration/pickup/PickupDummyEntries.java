package com.vanmarcke.blue.addon.integration.pickup;

import com.vanmarcke.blue.addon.integration.DeliveryIntegrationTestingConstants;
import com.vanmarcke.blue.addon.integration.pickup.builders.EntryInfoResponseDataBuilder;
import com.vanmarcke.cpi.data.order.EntryInfoResponseData;

import static com.vanmarcke.blue.addon.integration.DeliveryIntegrationTestingConstants.DummyProductCodes;
import static com.vanmarcke.blue.addon.integration.DeliveryIntegrationTestingConstants.EdcStockIndication;

/**
 * This constants class provides ready-made {@link EntryInfoResponseData} instances for the use in integration tests.
 * <p>
 * {@code TEC} or {@code NOTEC} signifies whether a certain product is present in the relevant TEC store.
 * <p>
 * {@code X, Y, F, L, C} are stock indications for the EDC (central warehouse of VM).
 * See {@link DeliveryIntegrationTestingConstants} for further clarification of these codes.
 */
public class PickupDummyEntries {

    static EntryInfoResponseData TEC_X() {
        return EntryInfoResponseDataBuilder
                .aMockEntryPickupDateResponse()
                .withAmount("5")
                .withEdcStockIndication(EdcStockIndication.X)
                .withProductCode(DummyProductCodes.product1)
                .hasTecStock(true)
                .build();
    }

    static EntryInfoResponseData TEC_Y() {
        return EntryInfoResponseDataBuilder
                .aMockEntryPickupDateResponse()
                .withAmount("5")
                .withEdcStockIndication(EdcStockIndication.Y)
                .withProductCode(DummyProductCodes.product1)
                .hasTecStock(true)
                .build();
    }

    public static EntryInfoResponseData TEC_F() {
        return EntryInfoResponseDataBuilder
                .aMockEntryPickupDateResponse()
                .withAmount("5")
                .withEdcStockIndication(EdcStockIndication.F)
                .withProductCode(DummyProductCodes.product1)
                .hasTecStock(true)
                .build();
    }

    public static EntryInfoResponseData TEC_L() {
        return EntryInfoResponseDataBuilder
                .aMockEntryPickupDateResponse()
                .withAmount("5")
                .withEdcStockIndication(EdcStockIndication.L)
                .withProductCode(DummyProductCodes.product1)
                .hasTecStock(true)
                .build();
    }

    public static EntryInfoResponseData TEC_C() {
        return EntryInfoResponseDataBuilder
                .aMockEntryPickupDateResponse()
                .withAmount("5")
                .withEdcStockIndication(EdcStockIndication.C)
                .withProductCode(DummyProductCodes.product1)
                .hasTecStock(true)
                .build();
    }

    static EntryInfoResponseData NOTEC_X() {
        return EntryInfoResponseDataBuilder
                .aMockEntryPickupDateResponse()
                .withAmount("5")
                .withEdcStockIndication(EdcStockIndication.X)
                .withProductCode(DummyProductCodes.product1)
                .hasTecStock(false)
                .build();
    }

    static EntryInfoResponseData NOTEC_Y() {
        return EntryInfoResponseDataBuilder
                .aMockEntryPickupDateResponse()
                .withAmount("5")
                .withEdcStockIndication(EdcStockIndication.Y)
                .withProductCode(DummyProductCodes.product1)
                .hasTecStock(false)
                .build();
    }

    public static EntryInfoResponseData NOTEC_F() {
        return EntryInfoResponseDataBuilder
                .aMockEntryPickupDateResponse()
                .withAmount("5")
                .withEdcStockIndication(EdcStockIndication.F)
                .withProductCode(DummyProductCodes.product1)
                .hasTecStock(false)
                .build();
    }

    public static EntryInfoResponseData NOTEC_L() {
        return EntryInfoResponseDataBuilder
                .aMockEntryPickupDateResponse()
                .withAmount("5")
                .withEdcStockIndication(EdcStockIndication.L)
                .withProductCode(DummyProductCodes.product1)
                .hasTecStock(false)
                .build();
    }

    public static EntryInfoResponseData NOTEC_C() {
        return EntryInfoResponseDataBuilder
                .aMockEntryPickupDateResponse()
                .withAmount("5")
                .withEdcStockIndication(EdcStockIndication.C)
                .withProductCode(DummyProductCodes.product1)
                .hasTecStock(false)
                .build();
    }


}
