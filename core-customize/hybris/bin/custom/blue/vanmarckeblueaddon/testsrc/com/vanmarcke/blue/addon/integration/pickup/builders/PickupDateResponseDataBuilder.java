package com.vanmarcke.blue.addon.integration.pickup.builders;

import com.vanmarcke.cpi.data.order.EntryInfoResponseData;
import com.vanmarcke.cpi.data.order.PickupDateInfoResponseData;
import com.vanmarcke.cpi.data.order.PickupDateResponseData;
import com.vanmarcke.services.model.builder.AddressModelMockBuilder;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Builder for creating mock IBM AS400 responses.
 */
public class PickupDateResponseDataBuilder {

    private String responseCode;
    private Date pickupDate;
    private Date tecCollectDate;
    private List<EntryInfoResponseData> products;

    /**
     * The entry point to the {@link PickupDateResponseDataBuilder}.
     *
     * @return the {@link PickupDateResponseDataBuilder} for method chaining.
     */
    public static PickupDateResponseDataBuilder aMockPickupResponse() {
        return new PickupDateResponseDataBuilder();
    }

    /**
     * Sets the {@code code} on the current {@link AddressModelMockBuilder} instance
     *
     * @param code the code
     * @return the {@link AddressModelMockBuilder} instance for method chaining
     */
    public PickupDateResponseDataBuilder withResponseCode(String code) {
        this.responseCode = code;
        return this;
    }

    /**
     * Sets the {@code pickupDate} on the current {@link AddressModelMockBuilder} instance
     *
     * @param pickupDate the pickupDate
     * @return the {@link AddressModelMockBuilder} instance for method chaining
     */
    public PickupDateResponseDataBuilder withPickupDate(Date pickupDate) {
        this.pickupDate = pickupDate;
        return this;
    }

    /**
     * Sets the {@code hasTecCollectDate} to true on the current {@link AddressModelMockBuilder} instance
     *
     * @return the {@link AddressModelMockBuilder} instance for method chaining
     */
    public PickupDateResponseDataBuilder withTecCollectDate() {
        this.tecCollectDate = new Date();
        return this;
    }

    /**
     * Sets the {@code hasTecCollectDate} to false on the current {@link AddressModelMockBuilder} instance
     *
     * @return the {@link AddressModelMockBuilder} instance for method chaining
     */
    public PickupDateResponseDataBuilder withoutTecCollectDate() {
        this.tecCollectDate = null;
        return this;
    }

    /**
     * Sets the {@code products} on the current {@link AddressModelMockBuilder} instance
     *
     * @param products the products
     * @return the {@link AddressModelMockBuilder} instance for method chaining
     */
    public PickupDateResponseDataBuilder withProducts(EntryInfoResponseData... products) {
        this.products = Arrays.asList(products);
        return this;
    }

    /**
     * Creates an implementation of the {@link PickupDateResponseData}.
     *
     * @return the PickupDateResponseData.
     */
    public PickupDateResponseData build() {
        PickupDateResponseData response = new PickupDateResponseData();
        PickupDateInfoResponseData responseData = new PickupDateInfoResponseData();
        EntryInfoResponseData entryData = new EntryInfoResponseData();

        entryData.setPickupDate(this.pickupDate);
        responseData.setProducts(Collections.singletonList(entryData));
        responseData.setExpressPickupDate(this.tecCollectDate);
        responseData.setProducts(this.products);

        response.setData(responseData);
        return response;
    }

    /**
     * Provides an instance of the PickupDateResponseDataBuilder.
     */
    private PickupDateResponseDataBuilder() {
    }
}
