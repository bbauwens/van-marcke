package com.vanmarcke.blue.addon.facades.impl;

import com.vanmarcke.core.model.CommerceGroupModel;
import com.vanmarcke.services.commercegroup.VMKCommerceGroupService;
import com.vanmarcke.services.model.builder.AddressModelMockBuilder;
import com.vanmarcke.services.model.builder.CartModelMockBuilder;
import com.vanmarcke.services.model.builder.CommerceGroupModelMockBuilder;
import com.vanmarcke.services.model.builder.CountryModelMockBuilder;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.checkout.pci.CheckoutPciStrategy;
import de.hybris.platform.acceleratorservices.enums.CheckoutPciOptionEnum;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.storelocator.data.PointOfServiceData;
import de.hybris.platform.commerceservices.model.PickUpDeliveryModeModel;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeModel;
import org.fest.assertions.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultBlueCheckoutFlowFacadeTest {

    @Mock
    private VMKCommerceGroupService commerceGroupService;

    @Mock
    private CheckoutPciStrategy checkoutPciStrategy;

    @Spy
    @InjectMocks
    private DefaultBlueCheckoutFlowFacade defaultBlueCheckoutFlowFacade;

    @Test
    public void testHasNoDeliveryPointOfServiceWithoutPickupItems() {
        CartData cart = new CartData();

        doReturn(cart).when(defaultBlueCheckoutFlowFacade).getCheckoutCart();
        doReturn(false).when(defaultBlueCheckoutFlowFacade).hasPickUpItems();

        boolean actualResult = defaultBlueCheckoutFlowFacade.hasNoDeliveryPointOfService();

        Assertions
                .assertThat(actualResult)
                .isFalse();
    }

    @Test
    public void testHasNoDeliveryPointOfServiceWithCartNull() {
        doReturn(null).when(defaultBlueCheckoutFlowFacade).getCheckoutCart();
        doReturn(true).when(defaultBlueCheckoutFlowFacade).hasPickUpItems();

        boolean actualResult = defaultBlueCheckoutFlowFacade.hasNoDeliveryPointOfService();

        Assertions
                .assertThat(actualResult)
                .isTrue();
    }

    @Test
    public void testHasNoDeliveryPointOfServiceWithoutDeliveryPointOfService() {
        CartData cart = new CartData();

        doReturn(cart).when(defaultBlueCheckoutFlowFacade).getCheckoutCart();
        doReturn(true).when(defaultBlueCheckoutFlowFacade).hasPickUpItems();

        boolean actualResult = defaultBlueCheckoutFlowFacade.hasNoDeliveryPointOfService();

        Assertions
                .assertThat(actualResult)
                .isTrue();
    }

    @Test
    public void testHasNoDeliveryPointOfServiceWitDeliveryPointOfService() {
        PointOfServiceData pos = new PointOfServiceData();

        CartData cart = new CartData();
        cart.setDeliveryPointOfService(pos);

        doReturn(cart).when(defaultBlueCheckoutFlowFacade).getCheckoutCart();
        doReturn(true).when(defaultBlueCheckoutFlowFacade).hasPickUpItems();

        boolean actualResult = defaultBlueCheckoutFlowFacade.hasNoDeliveryPointOfService();

        Assertions
                .assertThat(actualResult)
                .isFalse();
    }

    @Test
    public void testHasPickUpItemsWithoutCart() {
        doReturn(null).when(defaultBlueCheckoutFlowFacade).getCart();

        boolean actualResult = defaultBlueCheckoutFlowFacade.hasPickUpItems();

        Assertions
                .assertThat(actualResult)
                .isFalse();
    }

    @Test
    public void testHasPickUpItemsWithoutPickupDeliveryMode() {
        DeliveryModeModel deliveryMode = mock(ZoneDeliveryModeModel.class);

        CartModel cart = CartModelMockBuilder
                .aCart()
                .withDeliveryMode(deliveryMode)
                .build();

        doReturn(cart).when(defaultBlueCheckoutFlowFacade).getCart();

        boolean actualResult = defaultBlueCheckoutFlowFacade.hasPickUpItems();

        Assertions
                .assertThat(actualResult)
                .isFalse();
    }

    @Test
    public void testHasPickUpItemsWithPickupDeliveryMode() {
        DeliveryModeModel deliveryMode = mock(PickUpDeliveryModeModel.class);

        CartModel cart = CartModelMockBuilder
                .aCart()
                .withDeliveryMode(deliveryMode)
                .build();

        doReturn(cart).when(defaultBlueCheckoutFlowFacade).getCart();

        boolean actualResult = defaultBlueCheckoutFlowFacade.hasPickUpItems();

        Assertions
                .assertThat(actualResult)
                .isTrue();
    }

    @Test
    public void testHasShippingItemsWithoutCart() {
        doReturn(null).when(defaultBlueCheckoutFlowFacade).getCart();

        boolean actualResult = defaultBlueCheckoutFlowFacade.hasShippingItems();

        Assertions
                .assertThat(actualResult)
                .isFalse();
    }

    @Test
    public void testHasShippingItemsWithoutZonedDeliveryMode() {
        DeliveryModeModel deliveryMode = mock(PickUpDeliveryModeModel.class);

        CartModel cart = CartModelMockBuilder
                .aCart()
                .withDeliveryMode(deliveryMode)
                .build();

        doReturn(cart).when(defaultBlueCheckoutFlowFacade).getCart();

        boolean actualResult = defaultBlueCheckoutFlowFacade.hasShippingItems();

        Assertions
                .assertThat(actualResult)
                .isFalse();
    }

    @Test
    public void testHasShippingItemsWithZonedDeliveryMode() {
        DeliveryModeModel deliveryMode = mock(ZoneDeliveryModeModel.class);

        CartModel cart = CartModelMockBuilder
                .aCart()
                .withDeliveryMode(deliveryMode)
                .build();

        doReturn(cart).when(defaultBlueCheckoutFlowFacade).getCart();

        boolean actualResult = defaultBlueCheckoutFlowFacade.hasShippingItems();

        Assertions
                .assertThat(actualResult)
                .isTrue();
    }

    @Test
    public void testHasValidCountryWithoutCart() {
        doReturn(null).when(defaultBlueCheckoutFlowFacade).getCart();

        boolean actualResult = defaultBlueCheckoutFlowFacade.hasValidCountry();

        Assertions
                .assertThat(actualResult)
                .isFalse();

        verify(commerceGroupService).getCurrentCommerceGroup();
    }

    @Test
    public void testHasValidCountryWithoutPaymentAddress() {
        CartModel cart = CartModelMockBuilder
                .aCart()
                .build();

        doReturn(cart).when(defaultBlueCheckoutFlowFacade).getCart();

        boolean actualResult = defaultBlueCheckoutFlowFacade.hasValidCountry();

        Assertions
                .assertThat(actualResult)
                .isFalse();

        verify(commerceGroupService).getCurrentCommerceGroup();
    }

    @Test
    public void testHasValidCountryWithoutCommerceGroup() {
        AddressModel paymentAddress = AddressModelMockBuilder
                .anAddress()
                .build();

        CartModel cart = CartModelMockBuilder
                .aCart()
                .withPaymentAddress(paymentAddress)
                .build();

        doReturn(cart).when(defaultBlueCheckoutFlowFacade).getCart();

        boolean actualResult = defaultBlueCheckoutFlowFacade.hasValidCountry();

        Assertions
                .assertThat(actualResult)
                .isFalse();

        verify(commerceGroupService).getCurrentCommerceGroup();
    }

    @Test
    public void testHasValidCountryWithDifferentCountries() {
        CountryModel country = CountryModelMockBuilder
                .aCountry()
                .build();

        AddressModel paymentAddress = AddressModelMockBuilder
                .anAddress()
                .withCountry(country)
                .build();

        CartModel cart = CartModelMockBuilder
                .aCart()
                .withPaymentAddress(paymentAddress)
                .build();

        doReturn(cart).when(defaultBlueCheckoutFlowFacade).getCart();

        CountryModel anotherCountry = CountryModelMockBuilder
                .aCountry()
                .build();

        CommerceGroupModel group = CommerceGroupModelMockBuilder
                .aCommerceGroup()
                .withCountry(anotherCountry)
                .build();

        when(commerceGroupService.getCurrentCommerceGroup()).thenReturn(group);

        boolean actualResult = defaultBlueCheckoutFlowFacade.hasValidCountry();

        Assertions
                .assertThat(actualResult)
                .isFalse();

        verify(commerceGroupService).getCurrentCommerceGroup();
    }

    @Test
    public void testHasValidCountryWithSameCountry() {
        CountryModel country = CountryModelMockBuilder
                .aCountry()
                .build();

        AddressModel paymentAddress = AddressModelMockBuilder
                .anAddress()
                .withCountry(country)
                .build();

        CartModel cart = CartModelMockBuilder
                .aCart()
                .withPaymentAddress(paymentAddress)
                .build();

        doReturn(cart).when(defaultBlueCheckoutFlowFacade).getCart();

        CommerceGroupModel group = CommerceGroupModelMockBuilder
                .aCommerceGroup()
                .withCountry(country)
                .build();

        when(commerceGroupService.getCurrentCommerceGroup()).thenReturn(group);

        boolean actualResult = defaultBlueCheckoutFlowFacade.hasValidCountry();

        Assertions
                .assertThat(actualResult)
                .isTrue();

        verify(commerceGroupService).getCurrentCommerceGroup();
    }

    @Test
    public void testGetSubscriptionPciOption() {
        when(checkoutPciStrategy.getSubscriptionPciOption()).thenReturn(CheckoutPciOptionEnum.DEFAULT);

        CheckoutPciOptionEnum actualOption = defaultBlueCheckoutFlowFacade.getSubscriptionPciOption();

        Assertions
                .assertThat(actualOption)
                .isEqualTo(CheckoutPciOptionEnum.DEFAULT);

        verify(checkoutPciStrategy).getSubscriptionPciOption();
    }
}