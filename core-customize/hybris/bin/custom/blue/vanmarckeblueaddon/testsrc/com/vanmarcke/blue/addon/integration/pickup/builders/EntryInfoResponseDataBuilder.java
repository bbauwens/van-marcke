package com.vanmarcke.blue.addon.integration.pickup.builders;

import com.vanmarcke.cpi.data.order.EntryInfoResponseData;

/**
 * Builder for creating mock product pickup date responses.
 * These are used in the {@link PickupDateResponseDataBuilder}.
 */
public class EntryInfoResponseDataBuilder {

    private String productCode;
    private String amount;
    private String edcStockIndication;
    private boolean hasTecStock;

    /**
     * Entry point for the {@link EntryInfoResponseDataBuilder}.
     *
     * @return the mock data builder
     */
    public static EntryInfoResponseDataBuilder aMockEntryPickupDateResponse() {
        return new EntryInfoResponseDataBuilder();
    }

    /**
     * Sets the {@code productCode} on the current {@link EntryInfoResponseDataBuilder} instance
     *
     * @param productCode the productCode
     * @return the {@link EntryInfoResponseDataBuilder} instance for method chaining
     */
    public EntryInfoResponseDataBuilder withProductCode(String productCode) {
        this.productCode = productCode;
        return this;
    }

    /**
     * Sets the {@code amount} on the current {@link EntryInfoResponseDataBuilder} instance
     *
     * @param amount the amount
     * @return the {@link EntryInfoResponseDataBuilder} instance for method chaining
     */
    public EntryInfoResponseDataBuilder withAmount(String amount) {
        this.amount = amount;
        return this;
    }

    /**
     * Sets the {@code edcStockIndication} on the current {@link EntryInfoResponseDataBuilder} instance
     *
     * @param edcStockIndication the edcStockIndication
     * @return the {@link EntryInfoResponseDataBuilder} instance for method chaining
     */
    public EntryInfoResponseDataBuilder withEdcStockIndication(String edcStockIndication) {
        this.edcStockIndication = edcStockIndication;
        return this;
    }

    /**
     * Sets the {@code hasTecStock} on the current {@link EntryInfoResponseDataBuilder} instance
     *
     * @param hasTecStock the hasTecStock
     * @return the {@link EntryInfoResponseDataBuilder} instance for method chaining
     */
    public EntryInfoResponseDataBuilder hasTecStock(boolean hasTecStock) {
        this.hasTecStock = hasTecStock;
        return this;
    }

    /**
     * Creates an {@link EntryInfoResponseData} instance.
     *
     * @return the entry info response data
     */
    public EntryInfoResponseData build() {
        EntryInfoResponseData entry = new EntryInfoResponseData();
        entry.setAmount(this.amount);
        entry.setProductNumber(this.productCode);
        entry.setCodeSku(this.edcStockIndication);
        entry.setEnoughStock(this.hasTecStock);
        return entry;
    }

    /**
     * Provides an instance of the EntryInfoResponseDataBuilder.
     */
    private EntryInfoResponseDataBuilder() {
    }
}
