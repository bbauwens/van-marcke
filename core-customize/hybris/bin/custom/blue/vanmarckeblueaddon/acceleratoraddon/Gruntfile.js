module.exports = function(grunt) {

    // Project configuration
    grunt.initConfig({

        hybris: {
            project: "vanmarcke",
            dirs: {
                jsdest :'web/webroot/_ui/responsive/common/js',
                jssrc: 'web/webroot/WEB-INF/_ui-src/responsive/js',
                jshybrissrc: 'web/webroot/WEB-INF/_ui-src/responsive/lib',
                storefront: '../../../vanmarckestorefront'
            }
        },

        pkg : grunt.file.readJSON('package.json'),

        watch : {
            less : {
                files : [
                    'web/webroot/WEB-INF/_ui-src/shared/less/variableMapping.less',
                    'web/webroot/WEB-INF/_ui-src/shared/less/generatedVariables.less',
                    'web/webroot/WEB-INF/_ui-src/responsive/**/*.less'
                ],
                tasks : ['buildStyles']
            },
            tags : {
                files: ['web/webroot/WEB-INF/tags/**/*.tag'],
                tasks: ['sync:synctags']
            },
            views : {
                files: ['web/webroot/WEB-INF/views/responsive/**/*.jsp'],
                tasks: ['sync:syncviews']
            },
            messages : {
                files: ['web/webroot/WEB-INF/messages/**/*.properties'],
                tasks: ['sync:syncmessages']
            },
            // Auto reload browser on changes
            options: {
                livereload: true
            }
        },

        less : {
            default : {
                options : {
                    strictImports : true,
                    sourceMap: true,
                    sourceMapFileInline: true
                },
                files : [
                    {
                        expand: true,
                        cwd   : 'web/webroot/WEB-INF/_ui-src/responsive/themes/<%= hybris.project %>blue/less/',
                        src   : 'style.less',
                        dest  : 'web/webroot/WEB-INF/_ui-src/responsive/themes/<%= hybris.project %>blue/css/',
                        ext   : '.css'
                    }
                ]
            }
        },

        sync: {
            syncaddonjs : {
                files:  [
                    {
                        cwd: 'web/webroot/_ui/responsive/common/js/',
                        src: [
                           'hybris.js',
                           'hybris_libs.js',
                           'vanmarckeblue.js'
                        ],
                        dest: '<%= hybris.dirs.storefront %>/web/webroot/_ui/addons/<%= hybris.project %>blueaddon/responsive/common/js'
                    }
                ]
            },
            synccss : {
                files:  [
                    {
                        cwd: 'web/webroot/WEB-INF/_ui-src/responsive/themes/vanmarckeblue/css/',
                        src: ['style.css'],
                        dest: '<%= hybris.dirs.storefront %>/web/webroot/_ui/responsive/theme-<%= hybris.project %>blue/css'
                    }
                ]
            },
            synctags : {
                files: [
                    {
                        cwd : 'web/webroot/WEB-INF/tags',
                        src : '**/*.tag',
                        dest: '<%= hybris.dirs.storefront %>/web/webroot/WEB-INF/tags/addons/<%= hybris.project %>blueaddon'
                    }
                ]
            },
            syncviews : {
                files: [
                    {
                        cwd : 'web/webroot/WEB-INF/views/responsive',
                        src : '**/*.jsp',
                        dest: '<%= hybris.dirs.storefront %>/web/webroot/WEB-INF/views/addons/<%= hybris.project %>blueaddon/responsive'
                    }
                ]
            },
            syncmessages : {
                files: [
                    {
                        cwd : 'web/webroot/WEB-INF/messages',
                        src : '**/*.properties',
                        dest: '<%= hybris.dirs.storefront %>/web/webroot/WEB-INF/messages/addons/<%= hybris.project %>blueaddon/'
                    }
                ]
            },
            syncimages : {
                files: [
                    {
                        cwd : 'web/webroot/WEB-INF/_ui-src/responsive/themes/vanmarckeblue/images/',
                        src : '**/*.*',
                        dest: '<%= hybris.dirs.storefront %>/web/webroot/_ui/responsive/theme-<%= hybris.project %>blue/images/'
                    }
                ]
            }
        },
        concat: {
            hybris_libs:{
                src: [
                    '<%= hybris.dirs.jshybrissrc %>/common/jquery-3.2.1.min.js',
                    '<%= hybris.dirs.jshybrissrc %>/common/*.js'
                ],
                dest: '<%= hybris.dirs.jsdest %>/hybris_libs.js'
            },
            hybris:{
                src: [
                    '<%= hybris.dirs.jshybrissrc %>/ybase*/js/acc.*.js',
                    '<%= hybris.dirs.jshybrissrc %>/ybase*/js/_autoload.js'
                ],
                dest: '<%= hybris.dirs.jsdest %>/hybris.js'
            },
            compiled:{
                src: ['<%= hybris.dirs.jssrc %>/*.js', '<%= hybris.dirs.jssrc %>/custom/*.js'],
                dest: '<%= hybris.dirs.jsdest %>/vanmarckeblue.js'
            }
        },
        uglify: {
            options: {
                mangle: false
            },
            hybris_libs: {
                files: {
                    '<%= hybris.dirs.jsdest %>/hybris_libs.js': ['<%= hybris.dirs.jsdest %>/hybris_libs.js']
                }
            },
            hybris: {
                files: {
                    '<%= hybris.dirs.jsdest %>/hybris.js': ['<%= hybris.dirs.jsdest %>/hybris.js']
                }
            },
            vanmarckeblue: {
                files: {
                    '<%= hybris.dirs.jsdest %>/vanmarckeblue.js': ['<%= hybris.dirs.jsdest %>/vanmarckeblue.js']
                }
            }
        },
        clean: {
            jsbuild: ['<%= hybris.dirs.jsdest %>/app.js', '<%= hybris.dirs.jsdest %>/vendor.js']
        }
    });

    // Plugins
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-sync');
    grunt.loadNpmTasks('grunt-postcss');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('compileStyles', [
        'less:default'
    ]);

    grunt.registerTask('buildStyles', [
        'compileStyles',
        'sync:synccss',
        'sync:synctags'
    ]);

    grunt.registerTask('compileJS', [
        'concat',
        'uglify',
        'clean'
    ]);

    grunt.registerTask('buildJS', [
        'compileJS',
        'sync:syncaddonjs',
        'sync:synctags'
    ]);

    // Default tasks
    grunt.registerTask('all', [
        'compileStyles',
        'compileJS',
        'sync'
    ]);

};