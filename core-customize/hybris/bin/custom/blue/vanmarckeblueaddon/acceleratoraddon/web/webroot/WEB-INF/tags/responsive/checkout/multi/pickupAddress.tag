<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/address" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<spring:htmlEscape defaultHtmlEscape="true"/>

<%@ attribute name="cartData" required="true" type="de.hybris.platform.commercefacades.order.data.CartData" %>
<%@ attribute name="showDeliveryAddress" required="true" type="java.lang.Boolean" %>


<c:set var="hasPickupItems" value="${cartData.pickupItemsQuantity > 0}"/>
<c:set var="pickupAddress" value="${cartData.deliveryPointOfService.address}"/>
<c:set var="name" value="TEC ${cartData.deliveryPointOfService.displayName}"/>

<c:if test="${hasPickupItems and showDeliveryAddress}">
    <div class="checkout-order-summary-list">
        <div class="checkout-order-summary-list-heading">
            <h4><spring:theme code="text.account.order.pickup.location"/></h4>
            <div class="address">
                <address:formattedAddress address="${pickupAddress}" name="${name}"/>
            </div>
        </div>
    </div>
</c:if>