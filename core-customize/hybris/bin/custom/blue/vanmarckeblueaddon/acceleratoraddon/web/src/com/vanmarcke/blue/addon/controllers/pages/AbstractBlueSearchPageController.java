package com.vanmarcke.blue.addon.controllers.pages;


import com.vanmarcke.blue.addon.constants.VanmarckeblueaddonWebConstants.ProductListViewType;
import com.vanmarcke.facades.product.VMKProductFacade;
import com.vanmarcke.facades.search.VMKSolrProductSearchFacade;
import com.vanmarcke.facades.search.builder.VariantSearchStateDataBuilder;
import com.vanmarcke.facades.search.data.VariantSearchStateData;
import com.vanmarcke.storefront.security.cookie.EnhancedCookieGenerator;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractSearchPageController;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commerceservices.category.CommerceCategoryService;
import de.hybris.platform.commerceservices.enums.SearchQueryContext;
import de.hybris.platform.commerceservices.search.facetdata.ProductSearchPageData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.FilterQueryOperator;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import eu.elision.samlsinglesignon.util.VMKCookieUtil;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * custom class to provide common custom functionality for blue category and search controllers
 *
 * @author Cristi Stoica
 * @since 02-08-2021
 */
public class AbstractBlueSearchPageController extends AbstractSearchPageController {

    private static final String COOKIE_VIEW_TYPE = "view-type";
    private static final String PLV_VARIANTS_COUNT = "plv.variants.count";

    @Resource(name = "plpListViewCookieGenerator")
    private EnhancedCookieGenerator plpListViewCookieGenerator;

    @Resource(name = "commerceCategoryService")
    private CommerceCategoryService commerceCategoryService;

    @Resource(name = "productSearchFacade")
    private VMKSolrProductSearchFacade<ProductData> productSearchFacade;

    @Resource(name = "blueProductFacade")
    private VMKProductFacade vmkProductFacade;


    @Resource(name = "configurationService")
    private ConfigurationService configurationService;

    @ModelAttribute("plvVariantsCount")
    public String getPlvVariantsCount() {
        return this.configurationService.getConfiguration().getString(PLV_VARIANTS_COUNT);
    }

    /**
     * return and set if needed the stored cookie value for the PLP listing type - list or grid - depending if the listing was provided in the
     * parameter queries
     *
     * @param viewType the (optionally) provided viewType
     * @param request  the http request
     * @param response the http response
     * @return the current list type that is meant to be used
     */
    protected ProductListViewType getAndStorePLPViewType(ProductListViewType viewType, HttpServletRequest request, HttpServletResponse response) {
        if (viewType != null) {
            getPlpListViewCookieGenerator().addCookie(response, viewType.name());
            return viewType;
        } else if (VMKCookieUtil.getCookie(request, COOKIE_VIEW_TYPE) != null) {
            String cookieValue = VMKCookieUtil.extractValue(VMKCookieUtil.getCookie(request, COOKIE_VIEW_TYPE));
            return ProductListViewType.valueOf(cookieValue);
        } else {
            getPlpListViewCookieGenerator().addCookie(response, ProductListViewType.Grid.name());
            return ProductListViewType.Grid;
        }
    }

    /**
     * Further populate product data needed for the detaild product list view
     *
     * @param searchPageData the initial solr results from the category search
     * @param searchQuery    any search query passed from the controller
     */
    protected void populateProductVariants(ProductSearchPageData<SearchStateData, ProductData> searchPageData, String searchQuery, String categoryCode) {
        List<ProductData> searchResultsProducts = searchPageData.getResults();
        for (ProductData product : searchResultsProducts) {
            ProductSearchPageData<SearchStateData, ProductData> variantsSearchPageData;
            try {
                VariantSearchStateData variantSearchStateData = new VariantSearchStateDataBuilder()
                        .withCategoryCode(categoryCode)
                        .withSearchQuery(searchQuery)
                        .withContext(SearchQueryContext.PDP)
                        .withFilterQuery("baseProductCode", product.getBaseProduct(), FilterQueryOperator.AND)
                        .build();

                variantsSearchPageData = encodeSearchPageData(getProductSearchFacade().searchSolrProductVariants(variantSearchStateData));

                if (variantsSearchPageData != null) {
                    List<ProductData> solrVariants = variantsSearchPageData.getResults();
                    getVmkProductFacade().enrichProductDataWithSolrVariants(product, solrVariants, true);
                }

            } catch (final ConversionException e) // NOSONAR
            {
                // nothing to do - the exception is logged in SearchSolrQueryPopulator
            }

            // enrich product data with brand logo also, using brand code retrieved from solr
            vmkProductFacade.enrichProductDataWithBrandLogo(product);
        }
    }



    /**
     * @return the plpListViewCookieGenerator
     */
    public EnhancedCookieGenerator getPlpListViewCookieGenerator() {
        return plpListViewCookieGenerator;
    }

    /**
     * @return the productSearchFacade
     */
    public VMKSolrProductSearchFacade<ProductData> getProductSearchFacade() {
        return this.productSearchFacade;
    }

    /**
     *
     * @return the vmk blueProductFacade
     */
    public VMKProductFacade getVmkProductFacade() {
        return vmkProductFacade;
    }

    /**
     *
     * @return the commerce category service
     */
    public CommerceCategoryService getCommerceCategoryService() {
        return commerceCategoryService;
    }
}
