<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/cart" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<spring:htmlEscape defaultHtmlEscape="true"/>

<div class="cart-header border">
    <div class="row">
        <div class="col-xs-12 col-sm-5">
            <h1 class="cart-headline">
                <spring:theme code="text.cart"/>
            </h1>
        </div>
        <div class="col-xs-12 col-sm-7">
            <spring:url value="/cart/remove" var="removeCartUrl" htmlEscape="false"/>
            <a href="#emptyCartModal" data-toggle="modal" class="save__cart--link cart__head--link trigger-btn">
                <spring:theme code="cart.remove.session.cart"/>
            </a>
            <sec:authorize access="isAuthenticated()">
                <a href="#" class="save__cart--link cart__head--link js-export-basket"
                   data-basket-name="${fn:escapeXml(cartData.guid)}"
                   data-basket-type="CART">
                    <spring:theme code="basket.export.csv.file"/>
                </a>
                <spring:url value="/my-account/saved-baskets" var="listSavedCartUrl" htmlEscape="false"/>
                <a href="${fn:escapeXml(listSavedCartUrl)}" class="save__cart--link cart__head--link">
                    <spring:theme code="saved.cart.total.number" arguments="${wishListCount}"/>
                </a>
                <c:if test="${not empty cartData.rootGroups}">
                    <a href="#" class="save__cart--link cart__head--link js-add-to-wishlist-from-cart">
                        <spring:theme code="basket.save.cart"/>
                    </a>
                </c:if>
            </sec:authorize>
        </div>
    </div>
</div>


<c:if test="${not empty cartData.rootGroups}">
    <c:url value="/cart/checkout" var="checkoutUrl" scope="session"/>
    <c:url value="${continueUrl}" var="continueShoppingUrl" scope="session"/>
    <c:set var="showTax" value="false"/>

    <cart:cartItems cartData="${cartData}"/>

</c:if>
<cart:ajaxCartTopTotalSection/>
<cart:emptyCartModal cartData="${cartData}"/>