VMB.addressValidation = {

    resolve: false,
    reject: false,
    data: false,
    CLOSED: "Modal closed",

    validate: function (addressData) {

        if ($("#vmb-addressvalidation-modal").length > 0) {
            $("#vmb-addressvalidation-modal").remove();
        }

        return new Promise(
            function (resolve, reject) {

                VMB.addressValidation.resolve = resolve;
                VMB.addressValidation.reject = reject;

                if ($("#vmbAdressValidationBodyTemplate").length !== 1) {
                    reject(new Error('Address validation not fully loaded'));
                } else {
                    $.ajax({
                        method: "POST",
                        contentType: "application/json",
                        url: VMB.urls.addressValidation,
                        data: JSON.stringify(addressData)
                    }).done(function (response) {
                        if (_.isString(response)) {
                            response = JSON.parse(response);
                        }

                        VMB.addressValidation.data = response;

                        if (response.hasOwnProperty("suggestions")) {
                            var modalBody = $.templates("#vmbAdressValidationBodyTemplate").render({
                                suggestions: response.suggestions,
                                originalAddress: response.original,
                                suggestionsHeight: 35 + (response.suggestions.length * 35)
                            });

                            if (!response.exactMatch) {
                                $(document).on('click', ".vmb-select-address", VMB.addressValidation.validateAddressChoose);

                                $(document).on('click', ".vmb-cancel-select-address", VMB.addressValidation.cancelAddressChoose);

                                var modalFooter = "<button type='button' class='btn btn-link vmb-cancel-select-address'>" + VMB.i18n.addressValidation.modalCancel + "</button><button type='button' class='btn btn-default vmb-select-address'>" + VMB.i18n.addressValidation.modalButton + "</button>";

                                VMB.modal.show("vmb-addressvalidation-modal", VMB.i18n.addressValidation.modalTitle, modalBody, modalFooter, {canClose: false});
                            } else {
                                VMB.addressValidation.resolve(response.suggestions[0]);
                            }
                        }

                    }).fail(function (jqXHR, textStatus, errorThrown) {
                        VMB.addressValidation.reject(textStatus);
                    });
                }
            }
        );
    },

    validateAddressChoose: function (e) {
        $("#vmb-addressvalidation-modal").modal('hide');

        var chooseResult = $("input[name='chosenAddress']:checked").val();
        if (chooseResult === "-1") {
            VMB.addressValidation.cancelAddressChoose();
        } else if (chooseResult === "origin") {
            VMB.addressValidation.resolve(VMB.addressValidation.data.original);
        } else {
            VMB.addressValidation.resolve(VMB.addressValidation.data.suggestions[parseInt(chooseResult)]);
        }

        $("#vmb-addressvalidation-modal").remove();
    },

    cancelAddressChoose: function (e) {
        $("#vmb-addressvalidation-modal").modal('hide');

        VMB.addressValidation.reject(VMB.addressValidation.CLOSED);

        $("#vmb-addressvalidation-modal").remove();
    }
};