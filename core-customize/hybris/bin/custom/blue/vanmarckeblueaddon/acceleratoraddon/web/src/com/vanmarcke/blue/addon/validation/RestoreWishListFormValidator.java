/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.vanmarcke.blue.addon.validation;

import com.vanmarcke.blue.addon.form.RestoreWishListForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.RestoreSaveCartForm;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.regex.Pattern;


/**
 * Validator for restore save cart.
 */
public class RestoreWishListFormValidator implements Validator
{
	private static final String PATTERN_NAME = "^[ A-Za-z0-9\\x{00C0}-\\x{00ff}_-]*$";
	private static final String PATTERN_DESCRIPTION = "^[ A-Za-z0-9\\x{00C0}-\\x{00ff}?!_\\n.,@-]*$";

	@Override
	public boolean supports(final Class<?> aClass)
	{
		return RestoreWishListForm.class.equals(aClass);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final RestoreWishListForm restoreWishListForm = (RestoreWishListForm) object;
		if (restoreWishListForm.getCartName() != null && !restoreWishListForm.isPreventSaveActiveCart() && !restoreWishListForm.isPreventMergeActiveCart())
		{
			final String cartName = restoreWishListForm.getCartName();
			final String description = restoreWishListForm.getNewWishListDescription();
			if (StringUtils.isBlank(cartName))
			{
				errors.rejectValue("cartName", "basket.save.cart.validation.name.notBlank");
				return;
			}

			if (!isStringValid(cartName, PATTERN_NAME))
			{
				errors.rejectValue("cartName", "wishlist.validation-error.pattern-name");
				return;
			}

			if (StringUtils.length(cartName) > 255)
			{
				errors.rejectValue("cartName", "basket.save.cart.validation.name.size");
				return;
			}

			if (!StringUtils.isBlank(description) && !isStringValid(description, PATTERN_DESCRIPTION))
			{
				errors.rejectValue("newWishListDescription", "wishlist.validation-error.pattern-description");
				return;
			}
		}
	}

	protected boolean isStringValid(String fieldValue, String regexPattern) {
		final Pattern pattern = Pattern.compile(regexPattern);

		return pattern.matcher(fieldValue).matches();
	}

}
