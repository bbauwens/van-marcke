<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/address" %>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/checkout/multi" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="gtm" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/shared/analytics/gtm" %>
<spring:htmlEscape defaultHtmlEscape="true"/>
<template:pageCheckout pageTitle="${pageTitle}" hideHeaderLinks="true">
    <div class="row">
        <div class="col-sm-6">
            <div class="checkout-headline">
                <span class="glyphicon glyphicon-lock"></span>
                <spring:theme code="checkout.multi.secure.checkout"/>
            </div>
            <multi-checkout:checkoutSteps checkoutSteps="${checkoutSteps}" progressBarId="${progressBarId}">
                <jsp:body>
                    <ycommerce:testId code="checkoutStepOne">
                        <div class="checkout-shipping">
                            <multi-checkout:shipmentItems cartData="${cartData}" showDeliveryAddress="false"/>
                            <div class="checkout-indent">
                                <div class="headline"><spring:theme code="checkout.summary.shippingAddress"/></div>
                                <address:addressFormSelector supportedCountries="${countries}"
                                                             regions="${regions}" cancelUrl="${currentStepUrl}"
                                                             country="${country}"/>
                                <address:suggestedAddresses
                                        selectedAddressUrl="/checkout/multi/delivery-address/select"/>
                            </div>
                            <multi-checkout:pickupGroups cartData="${cartData}"/>
                        </div>
                    </ycommerce:testId>
                </jsp:body>
            </multi-checkout:checkoutSteps>
        </div>
        <div class="col-sm-6 hidden-xs">
            <multi-checkout:checkoutOrderDetails cartData="${cartData}" showDeliveryAddress="false"
                                                 showPaymentInfo="false" showTaxEstimate="false" showTax="true"/>
        </div>
    </div>
    <gtm:checkoutSteps step="1" cartData="${cartData}"/>
</template:pageCheckout>