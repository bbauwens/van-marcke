<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="cms" tagdir="/WEB-INF/tags/responsive/template/cms" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/template" %>
<c:set value="/_ui/addons/vanmarckeblueaddon/responsive/common" var="vanMarckeBlueResourcePath"/>
<c:url value="/" var="siteRootUrl"/>

<template:javaScriptVariables/>

<c:choose>
	<c:when test="${wro4jEnabled}">
	  	<script type="text/javascript" src="${contextPath}/wro/all_responsive.js"></script>
	  	<script type="text/javascript" src="${contextPath}/wro/addons_responsive.js"></script>
	</c:when>
	<c:otherwise>

		<script type="text/javascript" src="${vanMarckeBlueResourcePath}/js/hybris_libs.js"></script>
		<script type="text/javascript" src="${vanMarckeBlueResourcePath}/js/hybris.js"></script>

		<c:forEach items="${cmsActionsJsFiles}" var="actionJsFile">
			<script type="text/javascript" src="${commonResourcePath}/js/cms/${actionJsFile}"></script>
		</c:forEach>
		<%-- AddOn JavaScript files --%>
		<c:forEach items="${addOnJavaScriptPaths}" var="addOnJavaScript">
			<c:choose>
				<c:when test="${fn:contains(addOnJavaScript, 'smarteditaddon')}">
                    <c:if test="${!disableSmartEdit}">
                        <script type="text/javascript" src="${addOnJavaScript}"></script>
                    </c:if>
				</c:when>
				<c:otherwise>
					<script type="text/javascript" src="${addOnJavaScript}"></script>
				</c:otherwise>
			</c:choose>
		</c:forEach>
		<script src="//cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.15/lodash.min.js" integrity="sha256-VeNaFBVDhoX3H+gJ37DpT/nTuZTdjYro9yBruHjVmoQ=" crossorigin="anonymous"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.full.min.js" integrity="sha256-8IaVuYKdCIsHm7qenC922qiQB8rYYMs1shW9SwPU4vU=" crossorigin="anonymous"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/js/swiper.min.js" integrity="sha256-uckMYBvIGtce2L5Vf/mwld5arpR5JuhAEeJyjPZSUKY=" crossorigin="anonymous"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/jsrender/1.0.4/jsrender.min.js" integrity="sha256-s+wXPcRhQXvh1VlUAEpAIwi8DaDCxbNpB4QsJ6Kwdro=" crossorigin="anonymous"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/js-cookie/2.2.1/js.cookie.min.js" integrity="sha256-oE03O+I6Pzff4fiMqwEGHbdfcW7a3GRRxlL+U49L5sA=" crossorigin="anonymous"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/bluebird/3.7.0/bluebird.min.js" integrity="sha256-AYu+9V/MXrk+whPP4kdpJPHGYqKZOMtcwI+lWZbpMk8=" crossorigin="anonymous"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js" integrity="sha256-sPB0F50YUDK0otDnsfNHawYmA5M0pjjUf4TvRJkGFrI=" crossorigin="anonymous"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js" integrity="sha256-4iQZ6BVL4qNKlQ27TExEhBN1HFPvAvAMbFavKKosSWQ=" crossorigin="anonymous"></script>
		<script type="text/javascript" src="${vanMarckeBlueResourcePath}/js/vanmarckeblue.js"></script>
		
	</c:otherwise>
</c:choose>
<cms:previewJS cmsPageRequestContextData="${cmsPageRequestContextData}" />
