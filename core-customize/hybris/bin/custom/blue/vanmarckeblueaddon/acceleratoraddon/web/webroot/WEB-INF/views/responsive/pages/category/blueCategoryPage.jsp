<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/nav" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/product" %>
<%@ taglib prefix="categoryComponentSlots" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/category" %>

<template:page pageTitle="${pageTitle}">
    <div class="category-title">
        <h1>${categoryName}</h1>
    </div>
    <div class="category-description">
        <p>${categoryDescription}</p>
    </div>
    <categoryComponentSlots:subCategorySection/>
    <categoryComponentSlots:bottomComponentsSection/>
</template:page>