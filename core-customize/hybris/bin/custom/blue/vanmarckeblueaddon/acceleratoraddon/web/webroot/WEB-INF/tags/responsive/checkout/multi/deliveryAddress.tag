<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/address" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<spring:htmlEscape defaultHtmlEscape="true"/>

<%@ attribute name="cartData" required="true" type="de.hybris.platform.commercefacades.order.data.CartData" %>
<%@ attribute name="showDeliveryAddress" required="true" type="java.lang.Boolean" %>

<c:set var="hasShippedItems" value="${cartData.deliveryItemsQuantity > 0}"/>
<c:set var="deliveryAddress" value="${cartData.deliveryAddress}"/>
<c:set var="name" value="${cartData.deliveryAddress.firstName} ${cartData.deliveryAddress.lastName}"/>

<c:if test="${hasShippedItems and showDeliveryAddress}">
    <div class="checkout-order-summary-list">
        <div class="checkout-order-summary-list-heading">
            <h4><spring:theme code="checkout.summary.deliveryAddress"/></h4>
            <div class="address">
                <address:formattedAddress address="${deliveryAddress}" name="${name}"/>
            </div>
        </div>
    </div>
    <c:if test="${not empty cartData.deliveryComment}">
        <div>
            <spring:theme code="checkout.summary.delivery.note"/>: ${cartData.deliveryComment}
        </div>
    </c:if>
</c:if>