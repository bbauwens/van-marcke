package com.vanmarcke.blue.addon.constants;

public enum ExportSavedCartType {
    CART,
    ORDER,
    WISHLIST;
}
