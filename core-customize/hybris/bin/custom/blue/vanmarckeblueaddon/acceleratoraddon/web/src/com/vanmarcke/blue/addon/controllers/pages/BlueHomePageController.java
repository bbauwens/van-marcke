package com.vanmarcke.blue.addon.controllers.pages;

import com.vanmarcke.services.helper.HreflangHelper;
import de.hybris.platform.acceleratorservices.urlresolver.SiteBaseUrlResolutionService;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commerceservices.url.impl.AbstractUrlResolver;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.Resource;

/**
 * Controller for home page
 */
@Controller
@RequestMapping("/")
public class BlueHomePageController extends AbstractPageController {

    private static final String LOGOUT = "logout";
    private static final String ACCOUNT_CONFIRMATION_SIGNOUT_TITLE = "account.confirmation.signout.title";
    private static final String ACCOUNT_CONFIRMATION_CLOSE_TITLE = "account.confirmation.close.title";

    @Resource(name = "hreflangHelper")
    private HreflangHelper hreflangHelper;

    @Resource(name = "homepageContentPageUrlResolver")
    private AbstractUrlResolver<ContentPageModel> homePageUrlResolver;

    @Resource(name = "siteBaseUrlResolutionService")
    private SiteBaseUrlResolutionService siteBaseUrlResolutionService;

    @RequestMapping(method = RequestMethod.GET)
    public String home(@RequestParam(value = WebConstants.CLOSE_ACCOUNT, defaultValue = "false") final boolean closeAcc,
                       @RequestParam(value = LOGOUT, defaultValue = "false") final boolean logout, final Model model,
                       final RedirectAttributes redirectModel) throws CMSItemNotFoundException {

        if (logout) {
            String message = ACCOUNT_CONFIRMATION_SIGNOUT_TITLE;
            if (closeAcc) {
                message = ACCOUNT_CONFIRMATION_CLOSE_TITLE;
            }
            GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.INFO_MESSAGES_HOLDER, message);
            return REDIRECT_PREFIX + ROOT;
        }

        storeCmsPageInModel(model, getContentPageForLabelOrId(null));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(null));
        updatePageTitle(model, getContentPageForLabelOrId(null));

        model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.INDEX_FOLLOW);
        model.addAttribute("hreflanglinks", getHreflangHelper().generateHreflangMap(getContentPageForLabelOrId(null)));

        String canonicalHome = getSiteBaseUrlResolutionService()
                .getWebsiteUrlForSite(getBaseSiteService().getCurrentBaseSite(), true, homePageUrlResolver.resolve(getContentPageForLabelOrId(null)));
        model.addAttribute("canonicalLink", canonicalHome);

        return getViewForPage(model);
    }

    protected void updatePageTitle(final Model model, final AbstractPageModel cmsPage) {
        storeContentPageTitleInModel(model, cmsPage.getTitle());
    }

    /**
     * @return the hreflangHelper
     */
    public HreflangHelper getHreflangHelper() {
        return hreflangHelper;
    }

    /**
     * @return the siteBaseUrlResolutionService
     */
    public SiteBaseUrlResolutionService getSiteBaseUrlResolutionService() {
        return siteBaseUrlResolutionService;
    }
}
