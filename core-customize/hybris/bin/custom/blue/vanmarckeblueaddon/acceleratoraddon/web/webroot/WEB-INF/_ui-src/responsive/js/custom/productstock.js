(function($) {
    'use strict';

    var isDIY = $('#isDIY').val();

    /**
     * Event handler which triggers after the 'updateStockIndication event has been emitted.
     */
    window.mediator.subscribe('updateStockIndication', function(data) {
        //get all html elements with class update-icon-edc which are not hidden, this equals to the 10 items visible on one PDP page
        var stockProducts = $.map($(".update-icon-edc:not(.hidden)"), function(elm) {
            return $(elm).data("code");
        });

        for(var i = 0; i < stockProducts.length; i++) {
            getStock(stockProducts[i]);
        }
    });

    /**
     * Retrieve stock data for the given product code.
     *
     * @param product the product code
     */
    function getStock(product) {
        $.ajax({
            url        : VMB.urls.stockUpdate + "/" + product,
            type       : 'GET',
            contentType: 'application/json',
            dataType   : 'json',
            async      : true,
            beforeSend : function() {
                showSpinner(product);
                resetStockStatus($(".vmb-js-stock-update"));
            },
            success    : function(response) {
                updateStock(response, product);
            },
            error      : function(xhr) {
                //reset stock status
                resetStockStatus($(".vmb-js-stock-update"));
            },
            complete   : function() {
                hideSpinner(product);
            }
        });
    }

    /**
     * Update the stock for the given product code and response.
     *
     * @param response the response
     * @param product the product code
     */
    function updateStock(response, product) {
        var unknownAvailabilitySelector = $("#vmb-unknown-availability-message-" + product);

        if(!_.isEmpty(response) && !_.isEmpty(response.stockListData)) {
            unknownAvailabilitySelector.addClass("hidden");
            if($(".vmb-stock-info").length > 0) {
                $(".vmb-stock-info").remove();
            }

            var stockListData = response.stockListData;
            if(isValidResponse(stockListData)) {
                stockListData.stock.forEach(function(stockData) {
                    if(stockData.wareHouse == "EDC") {
                        updateEdcStock(stockData, product);
                    } else {
                        updateTecStock(stockData, product);
                    }
                });
            } else {
                unknownAvailabilitySelector.removeClass("hidden");
            }
        } else {
            var edcSelector = $("#vmb-stock-edc-" + product);
            var tecSelector = $("#vmb-stock-tec-" + product);

            if (edcSelector.hasClass("update-icon-edc")) {
                edcSelector.addClass("vmb-stock-status--icon-outOfStock");
            }

            if (tecSelector.hasClass("update-icon-tec")) {
                tecSelector.addClass("vmb-stock-status--icon-outOfStock");
            } else if (tecSelector.hasClass("update-count-tec")) {
                tecSelector.addClass("vmb-stock-status--count-out");
                tecSelector.html("0");
            }
        }
    }

    /**
     * Validate whether the stock response has the required length.
     * Since we need data for TEC and EDC, we need 2 StockData objects and assume
     *
     * @param stockListData the stockListData response object
     * @returns {boolean} true if valid
     */
    function isValidResponse(stockListData) {
        var hasCorrectSize = stockListData.hasOwnProperty("stock")
            && (stockListData.stock.length === 2
            || (isDIY === 'true' && stockListData.stock.length === 1 && stockListData.stock[0].wareHouse === 'EDC'));
        var stockLevelStatusIsValid = true;
        stockListData.stock.forEach(function(stockData) {
            if(!stockData.hasOwnProperty("stockLevelStatus") || !stockData.stockLevelStatus.hasOwnProperty("code")) {
                stockLevelStatusIsValid = false;
            }
        });
        return hasCorrectSize && stockLevelStatusIsValid
    }

    /**
     * Update the EDC stock indication for the given product code and stock data
     *
     * @param stock the stock data
     * @param product the product code
     */
    function updateEdcStock(stock, product) {
        var selector = $("#vmb-stock-edc-" + product);
        resetStockStatus(selector);

        //set EDC stock icon on PDP and cart
        if(selector.hasClass("update-icon-edc")) {
            selector.addClass("vmb-stock-status--icon-" + stock.stockLevelStatus.code);
        }
    }

    /**
     * Update the TEC stock indication for the given product code and stock data
     *
     * @param stock the stock data
     * @param product the product code
     */
    function updateTecStock(stock, product) {
        var selector = $("#vmb-stock-tec-" + product);
        resetStockStatus(selector);

        //Set TEC stock Icon on PDP
        if(selector.hasClass("update-icon-tec")) {
            selector.addClass("vmb-stock-status--icon-" + stock.stockLevelStatus.code);

            //Set TEC stock Level on cart
        } else if(selector.hasClass("update-count-tec")) {
            if(stock.stockLevel > 0) {
                selector.addClass("vmb-stock-status--count-in");
                selector.html(stock.stockLevel);
            } else {
                selector.addClass("vmb-stock-status--count-out");
                selector.html("0");
            }
        }
    }

    /**
     * Resets the current stock status indication from the given selector when a new update is about to happen.
     *
     * @param selector the selector
     */
    function resetStockStatus(selector) {
        selector.removeClass("vmb-stock-status--icon-inStock vmb-stock-status--icon-outOfStock vmb-stock-status--count-in vmb-stock-status--count-out");
    }

    /**
     * Hides the spinner for the given product code.
     *
     * @param productCode the product code
     */
    function hideSpinner(productCode) {
        $("#vmb-stock-edc-" + productCode + "-spinner").addClass("hidden");
        $("#vmb-stock-tec-" + productCode + "-spinner").addClass("hidden");
    }

    /**
     * Shows the spinner for the given product code.
     *
     * @param productCode the product code
     */
    function showSpinner(productCode) {
        $("#vmb-stock-edc-" + productCode + "-spinner").removeClass("hidden");
        $("#vmb-stock-tec-" + productCode + "-spinner").removeClass("hidden");
    }

})(jQuery);