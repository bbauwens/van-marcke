<%@ attribute name="deliveryAddress" required="true" type="de.hybris.platform.commercefacades.user.data.AddressData" %>
<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/checkout/multi" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/formElement" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/template" %>

<spring:htmlEscape defaultHtmlEscape="true"/>
<spring:theme code="checkout.summary.deliveryAddress.deliveryComment.helpText" var="helpText" htmlEscape="false"/>
<spring:theme code="checkout.multi.deliveryMethod.tec" var="pickup"/>
<spring:theme code="checkout.multi.deliveryMethod.delivery" var="delivery"/>
<spring:theme code="order.split.unavailable_dates" var="orderSplitWarnMessage"/>
<spring:theme code="text.quote.description.not.applicable" var="notAvailableText"/>

<input type="hidden" value="${orderSplitWarnMessage}" id="orderSplitWarnMessage"/>
<input type="hidden" value="${notAvailableText}" id="notAvailableText"/>
<input type="hidden" value="${cmsSite.channel == 'DIY'}" id="isDIY"/>

<spring:url var="deliveryMethodFormPostUrl" value="{contextPath}/checkout/multi/delivery-method/next"
            htmlEscape="false">
    <spring:param name="contextPath" value="${request.contextPath}"/>
</spring:url>
<form:form id="deliveryMethodForm" modelAttribute="deliveryMethodForm" action="${fn:escapeXml(deliveryMethodFormPostUrl)}"
           method="post" cssClass="vmb-checkout-delivery">
    <%--      Text: choose delivery method--%>
    <p class="vmb-checkout-delivery__explanation">
        <spring:theme code="delivery.form.explanation"/>
    </p>
    <div class="alert alert-info hidden" id="pickupDateNullInfo">
        <spring:theme code="checkout.multi.favoritestore.pickup.notprovided"/>
    </div>

    <%--------    PICKUP   --------%>
    <c:if test="${availableDeliveryModes['PICKUP'] and cmsSite.channel != 'DIY'}">
        <div class="alert alert-warning hidden" id="reservationMessage"></div>
        <div class="vmb-checkout-delivery__choice">
            <form:radiobutton path="deliveryMethod" id="tecRadioButton" label="${pickup}" value="PICKUP"/>
            <span class="icon icon-eco-energy"></span>
        </div>
        <div class="vmb-checkout-delivery__choiceoptions vmb-checkout-delivery__choiceoptions--pickup">
            <div class="vmbPickUpDatePicker"></div>
            <row>
                <div class="form-group alternativeStores">
                    <select class="alternativeStores__dropdown" id="alternativeStoreDropdown"></select>
                </div>
            </row>
        </div>
    </c:if>

    <%--------    SHIPPING   --------%>
    <c:if test="${availableDeliveryModes['DELIVERY']}">
        <div class="vmb-checkout-delivery__choice">
            <form:radiobutton path="deliveryMethod" id="shippingRadioButton" label="${delivery}" value="DELIVERY"/>
        </div>

        <div class="vmb-checkout-delivery__choiceoptions vmb-checkout-delivery__choiceoptions--shipping hidden">

            <div class="vmbDeliveryDatePicker"></div>

            <button type="button" class="btn btn-primary btn-small vmb-show-addressbook" data-toggle="modal"
                    data-target="#vmbAddressBookModal">
                <spring:theme code="checkout.checkout.multi.deliveryAddress.viewAddressBook"/>
            </button>

            <c:if test="${not empty deliveryAddress}">
                <table class="vmb-checkout-delivery__table-address clearfix">
                    <tr>
                        <th class="vmb-checkout-delivery__address-title">
                            <strong><spring:theme code="checkout.summary.deliveryAddress.name"/></strong>&nbsp;
                        </th>
                        <td class="vmb-checkout-delivery__address-content" id="vmb-checkout-delivery__address-name">
                            <c:choose>
                                <c:when test="${not empty deliveryAddress.firstName or not empty deliveryAddress.lastName}">
                                    ${fn:escapeXml(deliveryAddress.firstName)}&nbsp;${fn:escapeXml(deliveryAddress.lastName)}
                                </c:when>
                                <c:otherwise>
                                    ${fn:escapeXml(deliveryAddress.companyName)}
                                </c:otherwise>
                            </c:choose>
                        </td>
                    </tr>
                    <tr>
                        <th class="vmb-checkout-delivery__address-title">
                            <strong><spring:theme code="checkout.summary.deliveryAddress.address"/></strong>
                        </th>
                        <td class="vmb-checkout-delivery__address-content" id="vmb-checkout-delivery__address-street">
                                ${fn:escapeXml(deliveryAddress.line1)}&nbsp;${fn:escapeXml(deliveryAddress.line2)}&nbsp;${fn:escapeXml(deliveryAddress.apartment)}
                        </td>
                    </tr>
                    <tr>
                        <th class="vmb-checkout-delivery__address-title">
                            <strong><spring:theme code="checkout.summary.deliveryAddress.postalCode"/></strong>
                        </th>
                        <td class="vmb-checkout-delivery__address-content"
                            id="vmb-checkout-delivery__address-postalcode">
                                ${fn:escapeXml(deliveryAddress.postalCode)}
                        </td>
                    </tr>
                    <tr>
                        <th class="vmb-checkout-delivery__address-title">
                            <strong><spring:theme code="checkout.summary.deliveryAddress.town"/></strong>
                        </th>
                        <td class="vmb-checkout-delivery__address-content" id="vmb-checkout-delivery__address-town">
                                ${fn:escapeXml(deliveryAddress.town)}
                        </td>
                    </tr>
                    <tr>
                        <c:choose>
                            <c:when test="${not empty deliveryAddress.phone}">
                                <th class="vmb-checkout-delivery__address-title"
                                    id="vmb-checkout-delivery__address-telephone-label">
                                    <strong>
                                        <spring:theme code="checkout.summary.deliveryAddress.telephone"/>
                                    </strong>
                                </th>
                            </c:when>
                            <c:otherwise>
                                <th class="vmb-checkout-delivery__address-title hidden"
                                    id="vmb-checkout-delivery__address-telephone-label">
                                    <strong>
                                        <spring:theme code="checkout.summary.deliveryAddress.telephone"/>
                                    </strong>
                                </th>
                            </c:otherwise>
                        </c:choose>
                        <td class="vmb-checkout-delivery__address-content"
                            id="vmb-checkout-delivery__address-telephone">
                            <c:if test="${not empty deliveryAddress.phone}">
                                ${fn:escapeXml(deliveryAddress.phone)}
                            </c:if>
                        </td>
                    </tr>
                    <tr>
                        <c:choose>
                            <c:when test="${not empty deliveryAddress.mobile}">
                                <th class="vmb-checkout-delivery__address-title"
                                    id="vmb-checkout-delivery__address-mobile-label">
                                    <strong>
                                        <spring:theme code="address.mobile"/>
                                    </strong>
                                </th>
                            </c:when>
                            <c:otherwise>
                                <th class="vmb-checkout-delivery__address-title hidden"
                                    id="vmb-checkout-delivery__address-mobile-label">
                                    <strong>
                                        <spring:theme code="address.mobile"/>
                                    </strong>
                                </th>
                            </c:otherwise>
                        </c:choose>
                        <td class="vmb-checkout-delivery__address-content"
                            id="vmb-checkout-delivery__address-mobile">
                            <c:if test="${not empty deliveryAddress.mobile}">
                                ${fn:escapeXml(deliveryAddress.mobile)}
                            </c:if>
                        </td>
                    </tr>
                </table>
            </c:if>
        </div>
    </c:if>
    <%--    General reference and comment fields   --%>
    <div>
        <c:if test="${cmsSite.channel != 'DIY'}">
            <formElement:formInputBox idKey="yardReference"
                                      labelKey="checkout.summary.deliveryAddress.yardReference"
                                      path="yardReference" maxlength="30" mandatory="${requiredYardReference}"/>
        </c:if>
        <formElement:formCheckbox idKey="requestSeparateInvoice"
                                  labelKey="checkout.summary.deliveryAddress.requestSeparateInvoice"
                                  path="requestSeparateInvoice" disabled="${changeSeparateInvoiceReference}"/>
        <c:if test="${changeSeparateInvoiceReference}">
            <formElement:formInputBox idKey="purchaseOrderNumber"
                                      labelKey="checkout.multi.paymentMethod.purchase.order.number"
                                      path="purchaseOrderNumber" maxlength="6" mandatory="true"/>
        </c:if>
        <div class="vmk-checkout-delivery__delivery-comment hidden">
            <formElement:formTextArea idKey="deliveryComment"
                                      labelKey="checkout.summary.deliveryAddress.deliveryComment"
                                      path="deliveryComment"
                                      maxlength="254" placeholder="${helpText}"/>
        </div>
    </div>
    <%--    next button   --%>
    <div class="text-right">
        <button type="submit" class="btn btn-default">
            <spring:theme code="checkout.multi.deliveryMethod.continue"/>
        </button>
    </div>
    <div class="hidden" id="pickupModeCode">${deliveryModeMapping['PICKUP']}</div>
    <div class="hidden" id="shippingModeCode">${deliveryModeMapping['DELIVERY']}</div>
    <form:hidden path="tecUid" id="vmbTecUid"/>
    <form:hidden path="deliveryOption" id="vmbDeliveryOption"/>
    <form:hidden path="selectedShippingDate" id="vmbSelectedShippingDate"/>
    <form:hidden path="selectedPickupDate" id="vmbSelectedPickupDate"/>
    <input id="vmbProductDates" type="hidden" name="productEntriesDates">
</form:form>

<div class="vmb-modal fade" id="vmbAddressBookModal" tabindex="-1" role="dialog"
     aria-labelledby="vmbAddressBookModalTitle">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="vmbAddressBookModalTitle">
                    <spring:theme code="delivery.form.change.address"/>
                </h4>
            </div>
            <div class="modal-body">
                <multi-checkout:shipmentAddressBook/>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">
                    <spring:theme code="text.button.close"/>
                </button>
            </div>
        </div>
    </div>
</div>

<template:jsAddressValidation/>
