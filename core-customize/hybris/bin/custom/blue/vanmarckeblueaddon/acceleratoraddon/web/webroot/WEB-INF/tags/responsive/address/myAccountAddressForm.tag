<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/formElement" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/address" %>
<%@ attribute name="addressBook" required="false" type="java.lang.String" %>
<%@ attribute name="cancelUrl" required="false" type="java.lang.String" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/template" %>

<div class="vmb-account-new-address">
    <form:form method="post" modelAttribute="addressForm" cssClass="vmb-form-validation">
        <form:hidden path="addressId"/>
        <form:hidden path="countryIso"/>
        <div id="i18nAddressForm" class="i18nAddressForm">
            <address:blueAddressFormElements/>
        </div>
        <div class="checkbox">
            <c:if test="${not addressBookEmpty && not isDefaultAddress}">
                <ycommerce:testId code="editAddress_defaultAddress_box">
                    <formElement:formCheckbox idKey="defaultAddress"
                                              labelKey="address.default" path="defaultAddress"
                                              inputCSS="add-address-left-input"
                                              labelCSS="add-address-left-label" mandatory="true"/>
                </ycommerce:testId>
            </c:if>
        </div>
        <div id="addressform_button_panel" class="form-actions">
            <c:choose>
                <c:when test="${edit eq true && not addressBook}">
                    <ycommerce:testId code="multicheckout_saveAddress_button">
                        <button
                                class="positive right change_address_button show_processing_message"
                                type="submit">
                            <spring:theme code="checkout.multi.saveAddress"/>
                        </button>
                    </ycommerce:testId>
                </c:when>
                <c:when test="${edit eq true && addressBook eq true}">
                    <div class="accountActions">
                        <div class="text-right">
                            <ycommerce:testId code="editAddress_cancelAddress_button">
                                <c:url value="${cancelUrl}" var="cancel"/>
                                <a class="btn btn-link" href="${fn:escapeXml(cancel)}">
                                    <spring:theme code="text.button.cancel"/>
                                </a>
                            </ycommerce:testId>
                            <ycommerce:testId code="editAddress_saveAddress_button">
                                <button class="btn btn-default btn-submit vmb-account-new-address__submit" type="button">
                                    <spring:theme code="text.button.save"/>
                                </button>
                            </ycommerce:testId>
                        </div>
                    </div>
                </c:when>
                <c:otherwise>
                    <div class="text-right">
                        <button type="button" class="btn btn-default btn-submit vmb-account-new-address__submit"><spring:theme code="address.save.button.label"/></button>
                    </div>
                </c:otherwise>
            </c:choose>
        </div>
    </form:form>
</div>

<template:jsAddressValidation/>