package com.vanmarcke.blue.addon.validation;

import com.vanmarcke.blue.addon.form.EditWishListForm;
import org.springframework.validation.Errors;

/**
 * The {@link VMKWishlistEditFormValidator} class is used to validate {@link EditWishListForm} instances.
 *
 * @author Niels Raemaekers
 * @since 07-05-2021
 */
public class VMKWishlistEditFormValidator extends VMKAbstractWishListFormValidator {

    private static final String PATTERN_NAME = "^[ A-Za-z0-9\\x{00C0}-\\x{00ff}_-]*$";
    private static final String FIELD_NAME = "name";
    private static final String PATTERN_DESCRIPTION = "^[ A-Za-z0-9\\x{00C0}-\\x{00ff}?!_\\n@:;()*%€\\/\\<\\\\>,.\"”'@&+=-]*$";
    private static final String FIELD_DESCRIPTION = "description";

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean supports(Class<?> aClass) {
        return EditWishListForm.class.equals(aClass);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void validate(Object object, Errors errors) {
        EditWishListForm wishListForm = (EditWishListForm) object;

        validateString(errors, wishListForm.getName(), FIELD_NAME, PATTERN_NAME);
        validateString(errors, wishListForm.getDescription(), FIELD_DESCRIPTION, PATTERN_DESCRIPTION);
    }
}
