<%@ tag body-content="scriptless" trimDirectiveWhitespaces="true" %>
<%@ attribute name="pageTitle" required="false" rtexprvalue="true" %>
<%@ attribute name="metaDescription" required="false" %>
<%@ attribute name="metaKeywords" required="false" %>
<%@ attribute name="pageCss" required="false" fragment="true" %>
<%@ attribute name="pageScripts" required="false" fragment="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="addonTemplate" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/template" %>
<%@ taglib prefix="analytics" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/shared/analytics" %>
<%@ taglib prefix="addonScripts" tagdir="/WEB-INF/tags/responsive/common/header" %>
<%@ taglib prefix="generatedVariables" tagdir="/WEB-INF/tags/shared/variables" %>
<%@ taglib prefix="debug" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/shared/debug" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="htmlmeta" uri="http://hybris.com/tld/htmlmeta" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/product" %>
<spring:htmlEscape defaultHtmlEscape="true"/>
<!DOCTYPE html>
<html lang="${fn:escapeXml(currentLanguage.isocode)}">
<head>
    <title>
        ${not empty pageTitle ? pageTitle : not empty cmsPage.title ? fn:escapeXml(cmsPage.title) : 'Van Marcke'}
    </title>

    <%-- Hreflang --%>
    <c:forEach items="${hreflanglinks}" var="hreflanglink">
        <link rel="alternate" hreflang="${hreflanglink.key}" href="${hreflanglink.value}" />
    </c:forEach>


    <%-- Canonical --%>
    <c:if test="${not empty canonicalLink}">
        <link rel="canonical" href="${canonicalLink}" />
    </c:if>

    <%-- Google Structured Data --%>
    <product:productStructuredData productStructuredData="${productStructuredData}"/>

    <%-- Meta Content --%>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <%-- Additional meta tags --%>
    <htmlmeta:meta items="${metatags}"/>
    <%-- Favourite Icon --%>
    <c:set value="/_ui/responsive/theme-vanmarckeblue/images/favicons" var="favIconPath"/>

    <link rel="shortcut icon" type="image/x-icon" media="all" href="${favIconPath}"/>
    <link rel="apple-touch-icon" sizes="180x180" href="${favIconPath}/apple-touch-icon.png?v=vMQObMXjK2">
    <link rel="icon" type="image/png" sizes="32x32" href="${favIconPath}/favicon-32x32.png?v=vMQObMXjK2">
    <link rel="icon" type="image/png" sizes="16x16" href="${favIconPath}/favicon-16x16.png?v=vMQObMXjK2">
    <link rel="manifest" href="${favIconPath}/site.webmanifest?v=vMQObMXjK3">
    <link rel="mask-icon" href="${favIconPath}/safari-pinned-tab.svg?v=vMQObMXjK2" color="#00365c">
    <link rel="shortcut icon" href="${favIconPath}/favicon.ico?v=vMQObMXjK2">

    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#00365c">
    <!-- Windows Phone -->
    <meta name="msapplication-navbutton-color" content="#00365c">
    <!-- iOS Safari -->
    <meta name="apple-mobile-web-app-status-bar-style" content="#00365c">

    <%-- Inject any additional CSS required by the page --%>
    <jsp:invoke fragment="pageCss"/>

    <%-- CSS Files Are Loaded First as they can be downloaded in parallel --%>
    <template:styleSheets/>

    <%--  This script tracks functional, non-identifying data. See 'analytics.js' for analytical tracking.  --%>
    <c:choose>
        <c:when test="${not empty googleAnalyticsTrackingId}">
            <!-- Google Tag Manager -->
            <script>
                (function (w, d, s, l, i) {
                    w[l] = w[l] || [];
                    w[l].push({
                        'gtm.start':
                            new Date().getTime(), event: 'gtm.js'
                    });
                    var f = d.getElementsByTagName(s)[0],
                        j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
                    j.async = true;
                    j.src =
                        'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
                    f.parentNode.insertBefore(j, f);
                })(window, document, 'script', 'dataLayer', '${ycommerce:encodeJavaScript(googleAnalyticsTrackingId)}');
            </script>
            <!-- End Google Tag Manager -->
        </c:when>
        <c:otherwise>
            <script>
                var dataLayer = [];
            </script>
        </c:otherwise>
    </c:choose>

    <analytics:analytics/>
    <generatedVariables:generatedVariables/>
</head>
<body class="${pageBodyCssClasses} ${cmsPageRequestContextData.liveEdit ? ' yCmsLiveEdit' : ''} language-${fn:escapeXml(currentLanguage.isocode)}">
<c:if test="${not empty googleAnalyticsTrackingId}">
    <!-- Google Tag Manager (noscript) -->
    <noscript>
        <iframe src="https://www.googletagmanager.com/ns.html?id=${ycommerce:encodeJavaScript(googleAnalyticsTrackingId)}"
                height="0" width="0" style="display:none;visibility:hidden"></iframe>
    </noscript>
    <!-- End Google Tag Manager (noscript) -->
</c:if>
<%-- Inject the page body here --%>
<jsp:doBody/>
<form name="accessiblityForm">
    <input type="hidden" id="accesibility_refreshScreenReaderBufferField"
           name="accesibility_refreshScreenReaderBufferField" value=""/>
</form>
<div id="ariaStatusMsg" class="skip" role="status" aria-relevant="text" aria-live="polite"></div>
<%-- Load JavaScript required by the site --%>
<addonTemplate:javaScript/>
<%-- Inject any additional JavaScript required by the page --%>
<jsp:invoke fragment="pageScripts"/>
<%-- Inject CMS Components from addons using the placeholder slot--%>
<addonScripts:addonScripts/>
</body>
<debug:debugFooter/>
</html>
