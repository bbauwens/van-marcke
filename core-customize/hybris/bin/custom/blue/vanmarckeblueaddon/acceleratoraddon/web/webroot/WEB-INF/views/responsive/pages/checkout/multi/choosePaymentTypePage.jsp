<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/checkout/multi" %>
<%@ taglib prefix="gtm" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/shared/analytics/gtm" %>

<spring:htmlEscape defaultHtmlEscape="true"/>

<template:pageCheckout pageTitle="${pageTitle}" hideHeaderLinks="true">
    <div class="row">
        <div class="col-sm-6">
            <div class="checkout-headline">
                <span class="glyphicon glyphicon-lock"></span>
                <spring:theme code="checkout.multi.secure.checkout"></spring:theme>
            </div>
            <multi-checkout:checkoutSteps checkoutSteps="${checkoutSteps}" progressBarId="${progressBarId}">
                <jsp:body>
                    <multi-checkout:paymentTypeForm/>
                </jsp:body>
            </multi-checkout:checkoutSteps>
        </div>
        <div class="col-sm-6 hidden-xs">
            <multi-checkout:checkoutOrderDetails cartData="${cartData}" showDeliveryAddress="true" showPaymentInfo="false" showTaxEstimate="false" showTax="true"/>
        </div>
    </div>
    <gtm:checkoutSteps step="2" cartData="${cartData}"/>
</template:pageCheckout>
