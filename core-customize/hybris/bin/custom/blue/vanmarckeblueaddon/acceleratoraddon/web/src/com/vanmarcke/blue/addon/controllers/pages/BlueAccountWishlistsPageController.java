/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.vanmarcke.blue.addon.controllers.pages;

import com.google.common.collect.ImmutableList;
import com.vanmarcke.blue.addon.form.AddToWishListForm;
import com.vanmarcke.blue.addon.form.EditWishListForm;
import com.vanmarcke.blue.addon.form.RestoreWishListForm;
import com.vanmarcke.blue.addon.validation.RestoreWishListFormValidator;
import com.vanmarcke.blue.addon.validation.VMKAddToWishListFormValidator;
import com.vanmarcke.blue.addon.validation.VMKWishlistEditFormValidator;
import com.vanmarcke.facades.data.wishlist2.WishListData;
import com.vanmarcke.facades.wishlist2.VMKWishListFacade;
import com.vanmarcke.storefront.controllers.ControllerConstants;
import de.hybris.platform.acceleratorservices.controllers.page.PageType;
import de.hybris.platform.acceleratorservices.enums.ImportStatus;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractSearchPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commerceservices.order.CommerceSaveCartException;
import de.hybris.platform.core.servicelayer.data.PaginationData;
import de.hybris.platform.core.servicelayer.data.SearchPageData;
import de.hybris.platform.core.servicelayer.data.SortData;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.wishlist2.model.Wishlist2EntryModel;
import de.hybris.platform.wishlist2.model.Wishlist2Model;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.log4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.util.CookieGenerator;
import org.springframework.web.util.UriUtils;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.stream.Collectors;

import static com.vanmarcke.facades.wishlist2.impl.VMKWishListFacadeImpl.WISHLIST_SORT_BY_MODIFIED_TIME;
import static com.vanmarcke.facades.wishlist2.impl.VMKWishListFacadeImpl.WISHLIST_SORT_BY_NAME;

/**
 * Controller for saved carts page
 */
@Controller
@RequestMapping("/my-account/saved-baskets")
public class BlueAccountWishlistsPageController extends AbstractSearchPageController {

    private static final Logger LOGGER = Logger.getLogger(BlueAccountWishlistsPageController.class);

    private static final String MY_ACCOUNT_WISH_LISTS_URL = "/my-account/saved-baskets";
    private static final String REDIRECT_TO_WISH_LISTS_PAGE = REDIRECT_PREFIX + MY_ACCOUNT_WISH_LISTS_URL;

    private static final String WISH_LISTS_CMS_PAGE = "wishlists";
    private static final String WISH_LISTS_DETAILS_CMS_PAGE = "wishlists-detail";

    private static final String WISH_LIST_NAME_PATH_VARIABLE_PATTERN = "{wishlistName:.*}";

    private static final String PAGINATION_NUMBER_OF_RESULTS_COUNT = "pagination.number.results.count";

    public static final String TEXT_ACCOUNT_WISHLIST_CREATE_SUCCESS = "basket.save.cart.on.success";
    public static final String TEXT_ACCOUNT_WISHLIST_CREATE_FAILURE = "basket.save.cart.on.failure";
    public static final String NEW_WISHLIST_NAME_PREFFIX = "created";
    public static final String TEXT_WISHLIST_ERROR_NAME_REUSE = "wishlist.error.name.reuse";

    @Resource(name = "accountBreadcrumbBuilder")
    private ResourceBreadcrumbBuilder accountBreadcrumbBuilder;

    @Resource(name = "vmkWishListFacade")
    private VMKWishListFacade vmkWishListFacade;

    @Resource(name = "cartFacade")
    private CartFacade cartFacade;

    @Resource(name = "restoreWishListFormValidator")
    private RestoreWishListFormValidator restoreWishListFormValidator;

    @Resource(name = "vmkWishlistEditFormValidator")
    private VMKWishlistEditFormValidator vmkWishlistEditFormValidator;

    @Resource(name = "vmkAddToWishListFormValidator")
    private VMKAddToWishListFormValidator vmkAddToWishListFormValidator;

    @Resource(name = "productService")
    private ProductService productService;

    @ModelAttribute("allWishListSorts")

    public List<String> allWishListSorts() {
        return ImmutableList.<String>builder()
                .add(WISHLIST_SORT_BY_NAME)
                .add(WISHLIST_SORT_BY_MODIFIED_TIME)
                .build();
    }

    @RequestMapping(method = RequestMethod.GET)
    @RequireHardLogIn
    public String wishLists(@RequestParam(value = "page", defaultValue = "0") final int page,
                            @RequestParam(value = "sort", required = false) final String sortCode,
                            final Model model)
            throws CMSItemNotFoundException {
        // fetch all required wishlists
        final PaginationData paginationData = createPaginationData(10, page);
        final SortData sortData = createSortData(sortCode);
        final SearchPageData<WishListData> wishlistData = vmkWishListFacade.getWishListsOverview(paginationData, sortData);
        populateModel(model, wishlistData, sortData);
        storeCmsPageInModel(model, getContentPageForLabelOrId(WISH_LISTS_CMS_PAGE));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(WISH_LISTS_CMS_PAGE));
        model.addAttribute(WebConstants.BREADCRUMBS_KEY, accountBreadcrumbBuilder.getBreadcrumbs("text.account.savedCarts"));
        model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
        return getViewForPage(model);
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    @RequireHardLogIn
    public String add(final Model model) {

        final PaginationData paginationData = createPaginationData(vmkWishListFacade.getWishListCount(), 0);
        final SortData sortData = createSortData(WISHLIST_SORT_BY_NAME);
        final SearchPageData<WishListData> wishlistData = vmkWishListFacade.getWishListsOverview(paginationData, sortData);
        populateModel(model, wishlistData, sortData);

        return ControllerConstants.Views.Fragments.Account.WishListAddToWishListPopup;
    }

    @RequestMapping(value = "/" + WISH_LIST_NAME_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
    @RequireHardLogIn
    public String wishList(@PathVariable("wishlistName") final String wishlistName, final Model model,
                           final RedirectAttributes redirectModel) throws CMSItemNotFoundException {
        try {
            final WishListData wishListData = vmkWishListFacade.getWishList(wishlistName);

            if (ImportStatus.PROCESSING.equals(wishListData.getImportStatus())) {
                return REDIRECT_TO_WISH_LISTS_PAGE;
            }
            model.addAttribute("wishListData", wishListData);

            final EditWishListForm editWishListForm = new EditWishListForm();
            editWishListForm.setDescription(wishListData.getDescription());
            editWishListForm.setName(wishListData.getName());
            editWishListForm.setEntries(wishListData.getEntries());
            model.addAttribute("wishListEditForm", editWishListForm);
            model.addAttribute("pageType", PageType.WISHLIST.name());

            final List<Breadcrumb> breadcrumbs = accountBreadcrumbBuilder.getBreadcrumbs(null);
            breadcrumbs.add(new Breadcrumb(MY_ACCOUNT_WISH_LISTS_URL, getMessageSource().getMessage("text.account.savedCarts",
                    null, getI18nService().getCurrentLocale()), null));
            breadcrumbs.add(new Breadcrumb("#", getMessageSource().getMessage("text.account.savedCart.savedCartBreadcrumb",
                    new Object[]
                            {wishListData.getName()}, "Saved Cart {0}", getI18nService().getCurrentLocale()), null));
            model.addAttribute(WebConstants.BREADCRUMBS_KEY, breadcrumbs);

            storeCmsPageInModel(model, getContentPageForLabelOrId(WISH_LISTS_DETAILS_CMS_PAGE));
            model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
            setUpMetaDataForContentPage(model, getContentPageForLabelOrId(WISH_LISTS_DETAILS_CMS_PAGE));
            return getViewForPage(model);

        } catch (UnknownIdentifierException e) {
            LOGGER.error("Products unavailable in the wishlist: " + wishlistName, e);
            redirectModel.addFlashAttribute("brokenWishlistName", wishlistName);
            return REDIRECT_TO_WISH_LISTS_PAGE;
        }
    }

    @RequestMapping(value = "/" + WISH_LIST_NAME_PATH_VARIABLE_PATTERN + "/edit", method = RequestMethod.POST)
    @RequireHardLogIn
    public String savedCartEdit(@PathVariable("wishlistName") final String wishlistName, final EditWishListForm form,
                                final BindingResult bindingResult, final RedirectAttributes redirectModel) throws CommerceSaveCartException {
        vmkWishlistEditFormValidator.validate(form, bindingResult);

        if (bindingResult.hasErrors()) {
            for (final ObjectError error : bindingResult.getAllErrors()) {
                GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, error.getCode());
            }
            return REDIRECT_TO_WISH_LISTS_PAGE + "/" + wishlistName;
        }

        // fetch the proper wishlist
        final WishListData wishListData = vmkWishListFacade.getWishList(wishlistName);

        final String newName = StringUtils.isBlank(form.getName()) ? getNewWishListName("updated") : form.getName();

        try {
            final WishListData updated = vmkWishListFacade.update(wishListData, newName, form.getDescription(), form.getEntries());
            if (form.getEntries().size() > updated.getEntries().size()) {
                GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, "basket.page.message.remove");
            }
            return REDIRECT_TO_WISH_LISTS_PAGE + "/" + updated.getName();
        } catch (ModelSavingException e) {
            LOGGER.warn("Error saving wishlist with name " + newName, e);
            bindingResult.rejectValue("name", TEXT_WISHLIST_ERROR_NAME_REUSE);
            GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, TEXT_WISHLIST_ERROR_NAME_REUSE);

            final String errorMessage = getMessageSource().getMessage(bindingResult.getFieldError().getCode(), null,
                    getI18nService().getCurrentLocale());

            return REDIRECT_TO_WISH_LISTS_PAGE + "/" + wishlistName;
        }
    }

    @RequestMapping(value = "/" + WISH_LIST_NAME_PATH_VARIABLE_PATTERN + "/restore", method = RequestMethod.GET )
    @RequireHardLogIn
    public String restoreSaveCartForId(@PathVariable(value = "wishlistName") final String wishlistName,
                                       @RequestParam(value = "productCodes", required = false) final String[] productCodes,
                                       final Model model) {
        // fetch the proper wishlist
        final WishListData wishListData = vmkWishListFacade.getWishList(wishlistName);

        // filter out the lines which have been selected
        if (ArrayUtils.isNotEmpty(productCodes)) {
            final List<OrderEntryData> filteredEntries = wishListData.getEntries().stream()
                    // filter the ones specified by the customer
                    .filter(entry -> ArrayUtils.contains(productCodes, entry.getProduct().getCode()))
                    // and collect them
                    .collect(Collectors.toList());

            // add to the wishlist
            if (CollectionUtils.isNotEmpty(filteredEntries)) {
                wishListData.setEntries(filteredEntries);
            }
        }

        final boolean hasSessionCart = cartFacade.hasEntries();
        model.addAttribute("hasSessionCart", hasSessionCart);
        if (hasSessionCart) {
            model.addAttribute("autoGeneratedName", getNewWishListName(NEW_WISHLIST_NAME_PREFFIX));
        }
        model.addAttribute(wishListData);
        return ControllerConstants.Views.Fragments.Account.WishListRestorePopup;
    }

    @RequireHardLogIn
    @RequestMapping(value = "/" + WISH_LIST_NAME_PATH_VARIABLE_PATTERN + "/restore", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> postRestoreSaveCartForId(@PathVariable(value = "wishlistName") final String wishlistName,
                                    final @RequestBody RestoreWishListForm restoreWishListForm,
                                    final BindingResult bindingResult,
                                    final HttpServletResponse response)
            throws CommerceSaveCartException {

        this.restoreWishListFormValidator.validate(restoreWishListForm, bindingResult);
        if (bindingResult.hasErrors()) {

            final String errorMessage = getMessageSource().getMessage(bindingResult.getFieldError().getCode(), null,
                    getI18nService().getCurrentLocale());

            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorMessage);
        }

        final boolean hasSessionCart = cartFacade.hasEntries();
        // does the cart need to be saved?
        if (hasSessionCart && StringUtils.isNotBlank(restoreWishListForm.getCartName()) && !restoreWishListForm.isPreventSaveActiveCart() && !restoreWishListForm.isPreventMergeActiveCart()) {
            try {
                // save the current cart to a wishlist
                vmkWishListFacade.saveCartToWishList(restoreWishListForm.getCartName(), restoreWishListForm.getNewWishListDescription());
            } catch (ModelSavingException e) {

                LOGGER.warn("Error saving wishlist with name " + restoreWishListForm.getCartName(), e);
                bindingResult.rejectValue("cartName", TEXT_WISHLIST_ERROR_NAME_REUSE);
                final String errorMessage = getMessageSource().getMessage(bindingResult.getFieldError().getCode(), null,
                        getI18nService().getCurrentLocale());

                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorMessage);
            }
        }

        // do we need to clean the cart?
        if (hasSessionCart && !restoreWishListForm.isPreventMergeActiveCart()) {
            // clean the active cart
            cartFacade.removeSessionCart();
        }

        // add the entries to the cart
        vmkWishListFacade.restoreWishlist(wishlistName, restoreWishListForm.getWishlistProducts());

        // done!
        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/{wishListName}/delete", method = RequestMethod.DELETE)
    @ResponseStatus(value = HttpStatus.OK)
    @RequireHardLogIn
    public @ResponseBody
    String deleteSWishListForName(@PathVariable(value = "wishListName") final String wishListName) {
        vmkWishListFacade.removeWishList(wishListName);
        return String.valueOf(HttpStatus.OK);
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST, consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    @RequireHardLogIn
    @ResponseBody
    public ResponseEntity<?> addExecute(@ModelAttribute("addToWishListForm") AddToWishListForm addToWishListForm, final BindingResult bindingResult) {
        //validate the form
        vmkAddToWishListFormValidator.validate(addToWishListForm, bindingResult);
        if (bindingResult.hasErrors()) {
            String failureMessage = getMessageSource().getMessage(TEXT_ACCOUNT_WISHLIST_CREATE_FAILURE, null, getI18nService().getCurrentLocale());
            return new ResponseEntity<>(failureMessage, new HttpHeaders(), HttpStatus.BAD_REQUEST);
        }

        // retrieve the selection from the customer
        String wishListName = addToWishListForm.isCreateNew() ? addToWishListForm.getNewWishlistName() : addToWishListForm.getWishListAddTo();

        // always use a fallback if none specified
        if (StringUtils.isBlank(wishListName)) {
            wishListName = getNewWishListName(NEW_WISHLIST_NAME_PREFFIX);
        }

        // convert the list of codes
        final List<Pair<String, Integer>> data = getAsPair(addToWishListForm.getProductCode(), addToWishListForm.getProductQuantity());

        // add to the specified wishlist
        try {
            WishListData responseData = vmkWishListFacade.addToWishList(wishListName, addToWishListForm.getNewWishlistDescription(), data, addToWishListForm.isCreateNew());
            HttpHeaders headers = new HttpHeaders();
            String wishListUrl = MY_ACCOUNT_WISH_LISTS_URL + "/" + UriUtils.encode(responseData.getName(), StandardCharsets.UTF_8);
            String successMessage = getMessageSource().getMessage(TEXT_ACCOUNT_WISHLIST_CREATE_SUCCESS, new Object[]{responseData.getName()}, getI18nService().getCurrentLocale());
            headers.add("wishListUrl", wishListUrl);
            headers.add("successMessage", successMessage);

            return ResponseEntity.ok()
                    .headers(headers)
                    .build();

        } catch (ModelSavingException e) {
            LOGGER.warn("Error saving wishlist with name " + wishListName, e);
            bindingResult.rejectValue("newWishlistName", TEXT_WISHLIST_ERROR_NAME_REUSE);
            final String errorMessage = getMessageSource().getMessage(bindingResult.getFieldError().getCode(), null,
                    getI18nService().getCurrentLocale());

            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorMessage);
        }
    }

    @RequestMapping(value = "/" + WISH_LIST_NAME_PATH_VARIABLE_PATTERN + "/technicalsheets", method = RequestMethod.GET)
    @RequireHardLogIn
    public void downloadTechnicalDatasheets(@PathVariable(value = "wishlistName") final String wishlistName, final HttpServletResponse response) throws IOException {
        try (final InputStream is = vmkWishListFacade.exportTechnicalDataSheetsForWishList(wishlistName)) {
            response.setContentType(MediaType.APPLICATION_PDF_VALUE);
            response.setHeader("Content-Disposition", "attachment; filename=" + wishlistName + ".pdf");
            IOUtils.copy(is, response.getOutputStream());
            response.flushBuffer();
        }
    }


    @PostMapping(value = "/remove-unavailable")
    @ResponseStatus(value = HttpStatus.OK)
    @RequireHardLogIn
    public void removeUnavailableEntries(String wishlistName) {
        Wishlist2Model wishlist2Model = vmkWishListFacade.getWishListModel(wishlistName);

        // filter products that are not available on current site and channel
        List<Wishlist2EntryModel> unavailableEntries = wishlist2Model.getEntries().stream()
                .filter(entry -> {
                    try {
                        productService.getProductForCode(entry.getProduct().getCode());
                    } catch (UnknownIdentifierException e) {
                        return true;
                    }
                    return false;
                })
                .collect(Collectors.toList());

        unavailableEntries.forEach(entry -> vmkWishListFacade.removeWishListEntry(String.valueOf(entry.getPk())));
    }

    protected void populateModel(final Model model, final SearchPageData<?> searchPageData, final SortData sortData) {
        final int numberPagesShown = getSiteConfigService().getInt(PAGINATION_NUMBER_OF_RESULTS_COUNT, 5);

        model.addAttribute("numberPagesShown", Integer.valueOf(numberPagesShown));
        model.addAttribute("searchPageData", searchPageData);
        model.addAttribute("isShowAllAllowed", false);
        model.addAttribute("isShowPageAllowed", false);
        model.addAttribute("sortData", sortData);
    }

    private PaginationData createPaginationData(int pageSize, int currentPage) {
        final PaginationData data = new PaginationData();
        data.setCurrentPage(currentPage);
        data.setPageSize(pageSize);
        data.setNeedsTotal(true);
        return data;
    }

    private SortData createSortData(final String sortCode) {
        final SortData sd = new SortData();

        final String sortCodeInternal = StringUtils.firstNonBlank(sortCode, WISHLIST_SORT_BY_MODIFIED_TIME);

        switch (sortCodeInternal) {
            case WISHLIST_SORT_BY_NAME:
                sd.setCode(WISHLIST_SORT_BY_NAME);
                sd.setAsc(true);
                break;
            case WISHLIST_SORT_BY_MODIFIED_TIME:
            default:
                sd.setCode(WISHLIST_SORT_BY_MODIFIED_TIME);
                sd.setAsc(false);
        }
        return sd;
    }

    protected List<Pair<String, Integer>> getAsPair(final String[] lefts, final int[] rights) {
        final ImmutableList.Builder<Pair<String, Integer>> listBuilder = ImmutableList.builder();

        if (lefts != null && lefts.length > 0) {
            for (int idx = 0; idx < lefts.length; idx++) {
                final String left = lefts[idx];
                final int right = rights.length >= idx + 1 ? rights[idx] : 1;

                listBuilder.add(Pair.of(left, right));
            }
        }

        return listBuilder.build();
    }

    protected String getNewWishListName(final String prefix) {
        return String.format("%s-%s", prefix, System.currentTimeMillis());
    }
}
