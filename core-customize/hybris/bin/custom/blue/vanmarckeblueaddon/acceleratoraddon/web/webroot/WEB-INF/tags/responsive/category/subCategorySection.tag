<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<spring:theme code="img.missingProductImage.responsive.product" text="/" var="missingImagePath"/>


<div class="vmb-categories">
    <c:forEach items="${searchPageData.subCategories}" var="subCategory">
        <spring:url htmlEscape="false" value="${subCategory.url}" var="subCategoryUrl"/>
        <a href="${fn:escapeXml(subCategoryUrl)}" title="${subCategory.name}" class="vmb-categories__${subCategory.categoryType}">
            <c:set value="${subCategory.image}" var="logoImg"/>
            <c:choose>
                <c:when test="${not empty logoImg}">
                    <c:choose>
                        <c:when test="${not empty logoImg.altText}">
                            <c:set value="${fn:escapeXml(logoImg.altText)}" var="altText"/>
                            <div class="vmb-categories__${subCategory.categoryType}-center">
                                <img src="${logoImg.url}" alt="${altText}" title="${altText}"
                                     onerror="if (this.src != '${missingImagePath}') this.src = '${missingImagePath}';"/>
                            </div>
                        </c:when>
                        <c:otherwise>
                            <div class="vmb-categories__${subCategory.categoryType}-center">
                                <img src="${logoImg.url}" alt="${subCategory.name}"
                                     title="${subCategory.name}"
                                     onerror="if (this.src != '${missingImagePath}') this.src = '${missingImagePath}';"/>
                            </div>
                        </c:otherwise>
                    </c:choose>
                </c:when>
                <c:otherwise>
                    <div class="vmb-categories__${subCategory.categoryType}-center">
                        <img src="${missingImagePath}" alt="${subCategory.name}" title="${subCategory.name}"/>
                    </div>
                </c:otherwise>
            </c:choose>

            <h3>${subCategory.name}</h3>
            <div class="overlay"></div>
        </a>
    </c:forEach>
</div>
