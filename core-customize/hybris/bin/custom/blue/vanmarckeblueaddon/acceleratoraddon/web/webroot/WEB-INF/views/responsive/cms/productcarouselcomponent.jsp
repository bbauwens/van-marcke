<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/product" %>
<%@ taglib prefix="component" tagdir="/WEB-INF/tags/shared/component" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="gtm" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/shared/analytics/gtm" %>

<spring:htmlEscape defaultHtmlEscape="true"/>

<c:choose>
    <c:when test="${not empty productData}">
        <div class="vmb-product-carousel-component ${component.scroll.code}">

            <div class="vmb-product-carousel-component__title">

                <c:if test="${not empty title}">
                    <h2 class="headline">${fn:escapeXml(title)}</h2>
                </c:if>
                <div class="nav">
                    <div class="swiper-button-prev hidden"></div>
                    <div class="swiper-button-next hidden"></div>
                </div>
            </div>

            <div class="swiper-container" data-carousel="${component.scroll.code}">
                <div class="swiper-wrapper">
                    <c:forEach items="${productData}" var="product" varStatus="loopIndex">
                        <c:url value="${product.url}" var="productUrl"/>
                        <div class="swiper-slide">
                            <div class="vmb-product-item vmb-gtm-product-data"
                                 data-code="${fn:escapeXml(product.code)}"
                                 data-name="${fn:escapeXml(product.name)}"
                                 data-category="${fn:escapeXml(product.analyticsData.category)}"
                                 data-brand="${fn:escapeXml(product.analyticsData.brand)}"
                                 data-url="${product.url}"
                                 data-list="${component.name}"
                                 data-position="${loopIndex.index + 1}"
                                 data-price="${product.price.value}">
                                <a class="vmb-product-item__thumb vmb-gtm-product-item swiper-lazy" href="${productUrl}"
                                   title="${fn:escapeXml(product.name)}">
                                    <product:productPrimaryImage product="${product}" format="product"/>
                                </a>
                                <div class="vmb-product-item__content">
                                    <div class="vmb-product-item__details">
                                        <a class="vmb-product-item__name" href="${productUrl}">
                                            <c:out escapeXml="false" value="${fn:escapeXml(product.name)}"/>
                                        </a>
                                    </div>
                                    <a class="btn btn-default vmb-product-item__variants vmb-gtm-product-item"
                                       href="${productUrl}"><spring:theme code="product.references.button"/></a>
                                </div>
                            </div>
                            <div class="swiper-lazy-preloader"></div>
                        </div>
                    </c:forEach>
                </div>
                <div class="swiper-pagination"></div>

            </div>
        </div>
        <gtm:productCarousel/>
    </c:when>

    <c:otherwise>
        <component:emptyComponent/>
    </c:otherwise>
</c:choose>

