package com.vanmarcke.blue.addon.controllers.pages.checkout;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vanmarcke.blue.addon.controllers.VanmarckeblueaddonControllerConstants;
import com.vanmarcke.blue.addon.form.ContactOrderManagerForm;
import com.vanmarcke.core.enums.CustomerInteractionType;
import com.vanmarcke.facades.customerinteraction.data.CustomerInteractionData;
import com.vanmarcke.facades.principal.customer.VMKCustomerInteractionFacade;
import de.hybris.platform.acceleratorfacades.flow.impl.SessionOverrideCheckoutFlowFacade;
import de.hybris.platform.acceleratorservices.controllers.page.PageType;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.consent.data.ConsentCookieData;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractCheckoutController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.forms.ConsentForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.GuestRegisterForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.validation.GuestRegisterValidator;
import de.hybris.platform.acceleratorstorefrontcommons.security.AutoLoginStrategy;
import de.hybris.platform.acceleratorstorefrontcommons.strategy.CustomerConsentDataStrategy;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.commercefacades.consent.ConsentFacade;
import de.hybris.platform.commercefacades.coupon.data.CouponData;
import de.hybris.platform.commercefacades.order.CheckoutFacade;
import de.hybris.platform.commercefacades.order.OrderFacade;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.util.ResponsiveUtils;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.util.WebUtils;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.vanmarcke.core.constants.VanmarckeCoreConstants.REQUEST_SEPARATE_INVOICE;

/**
 * CheckoutController
 */
@Controller
@RequestMapping(value = "/checkout")
public class BlueCheckoutController extends AbstractCheckoutController {

    private static final Logger LOG = Logger.getLogger(BlueCheckoutController.class);
    /**
     * We use this suffix pattern because of an issue with Spring 3.1 where a Uri value is incorrectly extracted if it contains on or more '.' characters. Please see
     * https://jira.springsource.org/browse/SPR-6164 for a discussion on the issue and future resolution.
     */
    private static final String ORDER_CODE_PATH_VARIABLE_PATTERN = "{orderCode:.*}";

    private static final String CHECKOUT_ORDER_CONFIRMATION_CMS_PAGE_LABEL = "orderConfirmation";
    private static final String CONTINUE_URL_KEY = "continueUrl";
    private static final String CONSENT_FORM_GLOBAL_ERROR = "consent.form.global.error";
    private static final String CONTACT_ORDER_MANAGER_FORM_TYPE_GENERAL = "ORDER_MANAGER";

    @Resource(name = "productFacade")
    private ProductFacade productFacade;

    @Resource(name = "orderFacade")
    private OrderFacade orderFacade;

    @Resource(name = "checkoutFacade")
    private CheckoutFacade checkoutFacade;

    @Resource(name = "guestRegisterValidator")
    private GuestRegisterValidator guestRegisterValidator;

    @Resource(name = "autoLoginStrategy")
    private AutoLoginStrategy autoLoginStrategy;

    @Resource(name = "consentFacade")
    protected ConsentFacade consentFacade;

    @Resource(name = "customerConsentDataStrategy")
    protected CustomerConsentDataStrategy customerConsentDataStrategy;

    @Resource(name = "blueCustomerInteractionFacade")
    protected VMKCustomerInteractionFacade blueCustomerInteractionFacade;

    @ExceptionHandler(ModelNotFoundException.class)
    public String handleModelNotFoundException(final ModelNotFoundException exception, final HttpServletRequest request) {
        request.setAttribute("message", exception.getMessage());
        return FORWARD_PREFIX + "/404";
    }

    @RequestMapping(method = RequestMethod.GET)
    public String checkout(final RedirectAttributes redirectModel) {
        if (getCheckoutFlowFacade().hasValidCart()) {
            if (validateCart(redirectModel)) {
                return REDIRECT_PREFIX + "/cart";
            } else {
                this.checkoutFacade.prepareCartForCheckout();
                return getCheckoutRedirectUrl();
            }
        }
        LOG.info("Missing, empty or unsupported cart");

        // No session cart or empty session cart. Bounce back to the cart page.
        return REDIRECT_PREFIX + "/cart";
    }

    @RequestMapping(value = "/orderConfirmation/" + ORDER_CODE_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
    @RequireHardLogIn
    public String orderConfirmation(@PathVariable("orderCode") final String orderCode, final HttpServletRequest request,
                                    final Model model, final RedirectAttributes redirectModel) throws CMSItemNotFoundException {
        SessionOverrideCheckoutFlowFacade.resetSessionOverrides();
        getSessionService().removeAttribute(REQUEST_SEPARATE_INVOICE);
        return processOrderCode(orderCode, model, request, redirectModel);
    }

    @RequestMapping(value = "/orderConfirmation/" + ORDER_CODE_PATH_VARIABLE_PATTERN, method = RequestMethod.POST)
    public String orderConfirmation(final GuestRegisterForm form, final BindingResult bindingResult, final Model model,
                                    final HttpServletRequest request, final HttpServletResponse response, final RedirectAttributes redirectModel)
            throws CMSItemNotFoundException {
        getGuestRegisterValidator().validate(form, bindingResult);
        return processRegisterGuestUserRequest(form, bindingResult, model, request, response, redirectModel);
    }

    protected String processRegisterGuestUserRequest(final GuestRegisterForm form, final BindingResult bindingResult,
                                                     final Model model, final HttpServletRequest request, final HttpServletResponse response,
                                                     final RedirectAttributes redirectModel) throws CMSItemNotFoundException {
        if (bindingResult.hasErrors()) {
            form.setTermsCheck(false);
            GlobalMessages.addErrorMessage(model, "form.global.error");
            return processOrderCode(form.getOrderCode(), model, request, redirectModel);
        }
        try {
            getCustomerFacade().changeGuestToCustomer(form.getPwd(), form.getOrderCode());
            getAutoLoginStrategy().login(getCustomerFacade().getCurrentCustomer().getUid(), form.getPwd(), request, response);
            getSessionService().removeAttribute(WebConstants.ANONYMOUS_CHECKOUT);
        } catch (final DuplicateUidException e) {
            // User already exists
            LOG.warn("guest registration failed: " + e);
            form.setTermsCheck(false);
            model.addAttribute(new GuestRegisterForm());
            GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
                    "guest.checkout.existingaccount.register.error", new Object[]
                            {form.getUid()});
            return REDIRECT_PREFIX + request.getHeader("Referer");
        }

        // Consent form data
        try {
            final ConsentForm consentForm = form.getConsentForm();
            if (consentForm != null && consentForm.getConsentGiven()) {
                getConsentFacade().giveConsent(consentForm.getConsentTemplateId(), consentForm.getConsentTemplateVersion());
            }
        } catch (final Exception e) {
            LOG.error("Error occurred while creating consents during registration", e);
            GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, CONSENT_FORM_GLOBAL_ERROR);
        }

        // save anonymous-consent cookies as ConsentData
        final Cookie cookie = WebUtils.getCookie(request, WebConstants.ANONYMOUS_CONSENT_COOKIE);
        if (cookie != null) {
            try {
                final ObjectMapper mapper = new ObjectMapper();
                final List<ConsentCookieData> consentCookieDataList = Arrays.asList(mapper.readValue(
                        URLDecoder.decode(cookie.getValue(), StandardCharsets.UTF_8.displayName()), ConsentCookieData[].class));
                consentCookieDataList.stream().filter(consentData -> WebConstants.CONSENT_GIVEN.equals(consentData.getConsentState()))
                        .forEach(consentData -> this.consentFacade.giveConsent(consentData.getTemplateCode(),
                                Integer.valueOf(consentData.getTemplateVersion())));
            } catch (final UnsupportedEncodingException e) {
                LOG.error(String.format("Cookie Data could not be decoded : %s", cookie.getValue()), e);
            } catch (final IOException e) {
                LOG.error("Cookie Data could not be mapped into the Object", e);
            } catch (final Exception e) {
                LOG.error("Error occurred while creating Anonymous cookie consents", e);
            }
        }

        this.customerConsentDataStrategy.populateCustomerConsentDataInSession();

        return REDIRECT_PREFIX + "/";
    }

    protected String processOrderCode(final String orderCode, final Model model, final HttpServletRequest request,
                                      final RedirectAttributes redirectModel) throws CMSItemNotFoundException {
        final OrderData orderDetails;

        try {
            orderDetails = this.orderFacade.getOrderDetailsForCode(orderCode);
        } catch (final UnknownIdentifierException e) {
            LOG.warn("Attempted to load an order confirmation that does not exist or is not visible. Redirect to home page.");
            return REDIRECT_PREFIX + ROOT;
        }

        addRegistrationConsentDataToModel(model);

        if (orderDetails.isGuestCustomer() && !StringUtils.substringBefore(orderDetails.getUser().getUid(), "|")
                .equals(getSessionService().getAttribute(WebConstants.ANONYMOUS_CHECKOUT_GUID))) {
            return getCheckoutRedirectUrl();
        }

        if (orderDetails.getEntries() != null && !orderDetails.getEntries().isEmpty()) {
            for (final OrderEntryData entry : orderDetails.getEntries()) {
                final String productCode = entry.getProduct().getCode();
                final ProductData product = this.productFacade.getProductForCodeAndOptions(productCode,
                        Arrays.asList(ProductOption.BASIC, ProductOption.PRICE, ProductOption.CATEGORIES));
                entry.setProduct(product);
            }
        }

        model.addAttribute("orderCode", orderCode);
        model.addAttribute("orderData", orderDetails);
        model.addAttribute("allItems", orderDetails.getEntries());
        model.addAttribute("deliveryAddress", orderDetails.getDeliveryAddress());
        model.addAttribute("deliveryMode", orderDetails.getDeliveryMode());
        model.addAttribute("paymentInfo", orderDetails.getPaymentInfo());
        model.addAttribute("pageType", PageType.ORDERCONFIRMATION.name());

        final List<CouponData> giftCoupons = orderDetails.getAppliedOrderPromotions().stream()
                .filter(x -> CollectionUtils.isNotEmpty(x.getGiveAwayCouponCodes())).flatMap(p -> p.getGiveAwayCouponCodes().stream())
                .collect(Collectors.toList());
        model.addAttribute("giftCoupons", giftCoupons);

        processEmailAddress(model, orderDetails);

        final String continueUrl = getSessionService().getAttribute(WebConstants.CONTINUE_URL);
        model.addAttribute(CONTINUE_URL_KEY, (continueUrl != null && !continueUrl.isEmpty()) ? continueUrl : ROOT);

        final AbstractPageModel cmsPage = getContentPageForLabelOrId(CHECKOUT_ORDER_CONFIRMATION_CMS_PAGE_LABEL);
        storeCmsPageInModel(model, cmsPage);
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(CHECKOUT_ORDER_CONFIRMATION_CMS_PAGE_LABEL));
        model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);

        final ContactOrderManagerForm contactOrderManagerForm = new ContactOrderManagerForm();
        addContactForm(model, orderDetails, contactOrderManagerForm);

        if (ResponsiveUtils.isResponsive()) {
            return getViewForPage(model);
        }

        return VanmarckeblueaddonControllerConstants.Views.Pages.Checkout.CHECKOUT_CONFIRMATION_PAGE;
    }

    /**
     * Adds the "contact order manager" form to the model after pre-filling certain data
     *
     * @param model                   the model
     * @param contactOrderManagerForm the contactOrderManagerForm
     */
    protected void addContactForm(final Model model, final OrderData orderData, final ContactOrderManagerForm contactOrderManagerForm) {
        contactOrderManagerForm.setOrderNumber(orderData.getCode());
        contactOrderManagerForm.setEmail(orderData.getUser().getUid());
        contactOrderManagerForm.setType(CONTACT_ORDER_MANAGER_FORM_TYPE_GENERAL);
        model.addAttribute("contactOrderManagerForm", contactOrderManagerForm);
    }

    @RequestMapping(value = "/contactOrderManager/{orderCode}", method = RequestMethod.POST)
    @RequireHardLogIn
    @ResponseStatus(value = HttpStatus.OK)
    public void sendContactForm(@PathVariable final String orderCode, final ContactOrderManagerForm contactOrderManagerForm) {
        if (StringUtils.isNotBlank(contactOrderManagerForm.getContactComment())) {
            final CustomerInteractionData customerInteractionData = new CustomerInteractionData();
            customerInteractionData.setType(CustomerInteractionType.valueOf(contactOrderManagerForm.getType()));
            customerInteractionData.setMessage(contactOrderManagerForm.getContactComment());
            customerInteractionData.setOrderNumber(orderCode);
            blueCustomerInteractionFacade.submitOrderCustomerInteraction(customerInteractionData);
        }
    }

    protected void processEmailAddress(final Model model, final OrderData orderDetails) {
        final String uid;

        if (orderDetails.isGuestCustomer() && !model.containsAttribute("guestRegisterForm")) {
            final GuestRegisterForm guestRegisterForm = new GuestRegisterForm();
            guestRegisterForm.setOrderCode(orderDetails.getGuid());
            uid = orderDetails.getPaymentInfo().getBillingAddress().getEmail();
            guestRegisterForm.setUid(uid);
            model.addAttribute(guestRegisterForm);
        } else {
            uid = orderDetails.getUser().getUid();
        }
        model.addAttribute("email", uid);
    }

    protected GuestRegisterValidator getGuestRegisterValidator() {
        return this.guestRegisterValidator;
    }

    protected AutoLoginStrategy getAutoLoginStrategy() {
        return this.autoLoginStrategy;
    }

}
