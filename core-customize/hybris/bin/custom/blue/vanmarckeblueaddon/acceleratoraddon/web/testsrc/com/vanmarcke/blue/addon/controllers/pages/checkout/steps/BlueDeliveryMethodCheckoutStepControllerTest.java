package com.vanmarcke.blue.addon.controllers.pages.checkout.steps;

import com.elision.hybris.l10n.facades.LocalizedMessageFacade;
import com.vanmarcke.blue.addon.form.BlueAddressForm;
import com.vanmarcke.facades.forms.validation.DeliveryMethodFormValidator;
import com.vanmarcke.facades.order.VMKCheckoutFacade;
import com.vanmarcke.facades.site.data.BaseSiteData;
import com.vanmarcke.facades.storesession.VMKStoreSessionFacade;
import com.vanmarcke.services.forms.delivery.DeliveryMethodForm;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.CheckoutStep;
import de.hybris.platform.commercefacades.storesession.data.LanguageData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Collections;
import java.util.Locale;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class BlueDeliveryMethodCheckoutStepControllerTest {

    private static final String STEP = RandomStringUtils.randomAlphabetic(10);
    private static final String PROGRESS_BAR_ID = RandomStringUtils.randomAlphabetic(10);
    private static final String ISO_CODE = RandomStringUtils.randomAlphabetic(10);
    private static final String MESSAGE_KEY = "delivery.form.option.error";
    private static final String ERROR_CODE = RandomStringUtils.randomAlphabetic(10);
    private static final String ERROR_MESSAGE = RandomStringUtils.randomAlphabetic(10);

    @Mock
    private LocalizedMessageFacade messageFacade;

    @Mock
    private DeliveryMethodFormValidator deliveryMethodFormValidator;

    @Mock
    private VMKStoreSessionFacade blueStoreSessionFacade;

    @Mock
    private VMKCheckoutFacade blueCheckoutFacade;

    @Spy
    @InjectMocks
    private BlueDeliveryMethodCheckoutStepController controller;

    @Test
    public void testBack() {
        RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
        CheckoutStep checkoutStep = mock(CheckoutStep.class);
        when(checkoutStep.previousStep()).thenReturn(STEP);
        doReturn(checkoutStep).when(controller).getCheckoutStep();

        String result = controller.back(redirectAttributes);

        assertThat(result)
                .isEqualTo(STEP);
    }

    @Test
    public void testGetBreadCrumbKey() {
        String multi = "checkout.multi.";
        String breadcrumb = ".breadcrumb";

        CheckoutStep checkoutStep = mock(CheckoutStep.class);
        when(checkoutStep.getProgressBarId()).thenReturn(PROGRESS_BAR_ID);
        doReturn(checkoutStep).when(controller).getCheckoutStep();

        String result = controller.getBreadcrumbKey();

        assertThat(result)
                .isEqualTo(multi + PROGRESS_BAR_ID + breadcrumb);
    }

    @Test
    public void testCreateAddressForm() {
        CountryData countrydata = new CountryData();
        BaseSiteData currentSite = new BaseSiteData();
        currentSite.setCountry(countrydata);
        countrydata.setIsocode(ISO_CODE);

        when(blueStoreSessionFacade.getCurrentSite()).thenReturn(currentSite);

        BlueAddressForm addressForm = controller.createAddressForm();

        assertThat(addressForm.getCountryIso())
                .isEqualTo(ISO_CODE);
    }

    @Test
    public void testNext_withErrors() {
        RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
        DeliveryMethodForm form = mock(DeliveryMethodForm.class);

        ObjectError error = mock(ObjectError.class);
        when(error.getCode()).thenReturn(ERROR_CODE);

        BindingResult bindingResult = mock(BindingResult.class);
        when(bindingResult.hasErrors()).thenReturn(true);
        when(bindingResult.getAllErrors()).thenReturn(Collections.singletonList(error));

        CheckoutStep checkoutStep = mock(CheckoutStep.class);
        when(checkoutStep.nextStep()).thenReturn(STEP);
        doReturn(checkoutStep).when(controller).getCheckoutStep();

        LanguageData language = new LanguageData();
        language.setIsocode(Locale.US.toString());

        when(blueCheckoutFacade.getDeliveryMethodForm()).thenReturn(form);

        when(messageFacade.getMessageForCodeAndCurrentLocale(ERROR_CODE)).thenReturn(ERROR_MESSAGE);

        String result = controller.next(redirectAttributes, form, bindingResult);

        verify(deliveryMethodFormValidator).validate(form, bindingResult);
        verify(blueStoreSessionFacade, never()).getCurrentLanguage();
        verify(messageFacade).getMessageForCodeAndCurrentLocale(ERROR_CODE);
        verify(controller).redirectToCurrentStepWithError(redirectAttributes, ERROR_MESSAGE);
        verify(blueCheckoutFacade, never()).saveDeliveryInformation(form);

        assertThat(result)
                .isNull();
    }

    @Test
    public void testNext() {
        RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
        DeliveryMethodForm form = mock(DeliveryMethodForm.class);
        BindingResult bindingResult = mock(BindingResult.class);

        when(bindingResult.hasErrors()).thenReturn(false);

        CheckoutStep checkoutStep = mock(CheckoutStep.class);
        when(checkoutStep.nextStep()).thenReturn(STEP);
        doReturn(checkoutStep).when(controller).getCheckoutStep();

        String result = controller.next(redirectAttributes, form, bindingResult);

        verify(deliveryMethodFormValidator).validate(form, bindingResult);
        verify(controller, times(0)).redirectToCurrentStepWithError(redirectAttributes, MESSAGE_KEY);
        verify(blueCheckoutFacade).saveDeliveryInformation(form);

        assertThat(result)
                .isEqualTo(STEP);
    }

}