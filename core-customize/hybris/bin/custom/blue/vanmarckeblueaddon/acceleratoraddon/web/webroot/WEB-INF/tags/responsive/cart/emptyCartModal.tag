<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="cartData" required="true" type="de.hybris.platform.commercefacades.order.data.CartData" %>

<spring:url var="removeCartUrl" value="/cart/entry/execute/REMOVE"/>
<c:set var="entryNumbers" value=""/>
<c:if test="${not empty cartData.rootGroups}">
    <c:forEach items="${cartData.rootGroups}" var="group">
        <c:if test="${not empty group.orderEntries}">
            <c:forEach items="${group.orderEntries}" var="entry">
                <c:set var="entryNumbers" value="${entryNumbers}${entry.entryNumber};"/>
            </c:forEach>
        </c:if>
    </c:forEach>
</c:if>

<div id="emptyCartModal" class="modal fade">
    <div class="modal-dialog modal-confirm">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title w-100 cart-removal__header"><spring:theme code="cart.remove.modal.header"/></h4>
            </div>
            <div class="modal-body">
                <p class="cart-removal__body"><spring:theme code="cart.remove.modal.body"/></p>
            </div>
            <div class="modal-footer justify-content-center">
                <button type="button" class="btn btn-secondary cart-removal__btn-cancel" data-dismiss="modal">
                    <spring:theme code="cart.remove.modal.cancel"/></button>
                <button type="button"
                        id="cart-removal__submit"
                        class="btn btn-danger"
                        data-url="${fn:escapeXml(removeCartUrl)}"
                        data-entry-numbers="${entryNumbers}">
                    <spring:theme code="cart.remove.modal.delete"/>
                </button>
            </div>
        </div>
    </div>
</div>