<%@ tag import="com.vanmarcke.services.util.URIUtils" %>
<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ tag import="com.vanmarcke.core.constants.VanmarckeCoreConstants" %>

<spring:htmlEscape defaultHtmlEscape="true"/>

<%-- JS configuration --%>
<script type="text/javascript">
    /*<![CDATA[*/
    <%-- Define a javascript variable to hold the content path --%>
    var ACC = {config: {}};
    ACC.config.contextPath = '${ycommerce:encodeJavaScript(contextPath)}';
    ACC.config.encodedContextPath = '${ycommerce:encodeJavaScript(encodedContextPath)}';
    ACC.config.commonResourcePath = '${ycommerce:encodeJavaScript(commonResourcePath)}';
    ACC.config.themeResourcePath = '${ycommerce:encodeJavaScript(themeResourcePath)}';
    ACC.config.siteResourcePath = '${ycommerce:encodeJavaScript(siteResourcePath)}';
    ACC.config.rootPath = '${ycommerce:encodeJavaScript(siteRootUrl)}';
    ACC.config.CSRFToken = '${ycommerce:encodeJavaScript(CSRFToken.token)}';
    ACC.pwdStrengthVeryWeak = '<spring:theme code="password.strength.veryweak" htmlEscape="false" javaScriptEscape="true" />';
    ACC.pwdStrengthWeak = '<spring:theme code="password.strength.weak" htmlEscape="false" javaScriptEscape="true" />';
    ACC.pwdStrengthMedium = '<spring:theme code="password.strength.medium" htmlEscape="false" javaScriptEscape="true" />';
    ACC.pwdStrengthStrong = '<spring:theme code="password.strength.strong" htmlEscape="false" javaScriptEscape="true" />';
    ACC.pwdStrengthVeryStrong = '<spring:theme code="password.strength.verystrong" htmlEscape="false" javaScriptEscape="true" />';
    ACC.pwdStrengthUnsafePwd = '<spring:theme code="password.strength.unsafepwd" htmlEscape="false" javaScriptEscape="true" />';
    ACC.pwdStrengthTooShortPwd = '<spring:theme code="password.strength.tooshortpwd" htmlEscape="false" javaScriptEscape="true" />';
    ACC.pwdStrengthMinCharText = '<spring:theme code="password.strength.minchartext" htmlEscape="false" javaScriptEscape="true" />';
    ACC.accessibilityLoading = '<spring:theme code="aria.pickupinstore.loading" htmlEscape="false" javaScriptEscape="true" />';
    ACC.accessibilityStoresLoaded = '<spring:theme code="aria.pickupinstore.storesloaded" htmlEscape="false" javaScriptEscape="true" />';
    ACC.config.googleApiKey = '${ycommerce:encodeJavaScript(googleApiKey)}';
    ACC.config.googleApiVersion = '${ycommerce:encodeJavaScript(googleApiVersion)}';
    ACC.config.consentTypes = JSON.parse('${consentTypes}');
    ACC.pageType = '${pageType}';

    <c:if test="${request.secure}">
    <c:url var="autocompleteUrl" value="/search/autocompleteSecure" />
    </c:if>
    <c:if test="${not request.secure}">
    <c:url var="autocompleteUrl" value="/search/autocomplete" />
    </c:if>
    ACC.autocompleteUrl = '${ycommerce:encodeJavaScript(autocompleteUrl)}';

    <c:set var="currentUrl" value='<%=URIUtils.normalize(request.getAttribute("javax.servlet.forward.request_uri").toString())%>'/>
    <c:set value="/elisionsamlsinglesignon/saml${currentUrl}" var="loginUrl"/>
    ACC.config.loginUrl = '${ycommerce:encodeJavaScript(loginUrl)}';

    <c:url var="authenticationStatusUrl" value="/authentication/status" />
    ACC.config.authenticationStatusUrl = '${ycommerce:encodeJavaScript(authenticationStatusUrl)}';

    <c:forEach var="jsVar" items="${jsVariables}">
    <c:if test="${not empty jsVar.qualifier}" >
    ACC['${ycommerce:encodeJavaScript(jsVar.qualifier)}'] = '${ycommerce:encodeJavaScript(jsVar.value)}';
    </c:if>
    </c:forEach>

    var VMB = {};

    <%-- Translations --%>

    VMB.i18n = {
        search: {
            autoSuggestion: '<spring:message code="search.auto.title.suggestion" javaScriptEscape="true"/>',
            productResult: '<spring:message code="search.auto.title.products" javaScriptEscape="true"/>',
            categoryResult: '<spring:message code="search.auto.title.categories" javaScriptEscape="true" />',
            more: '<spring:theme code="search.auto.show.more" htmlEscape="false" javaScriptEscape="true" />'
        },
        addressValidation: {
            modalTitle: "<spring:theme code='address.validation.modal.title' javaScriptEscape="true" />",
            modalButton: "<spring:theme code='address.validation.modal.button' javaScriptEscape="true" />",
            modalCancel: "<spring:theme code='text.button.cancel' javaScriptEscape="true" />",
        },
        checkout: {
            placeholderAltTec: "<spring:theme code='checkout.multi.deliveryMethod.teccollect' javaScriptEscape="true" />",
            selectedStore: "<spring:theme code='checkout.multi.selectedStore' javaScriptEscape='true' />",
            selectedStoreDistance: "<spring:theme code='checkout.multi.selectedStore.Distance' javaScriptEscape='true' />",
            pointOfService: {
                availability: "<spring:theme code="text.checkout.pointOfService.availability" javaScriptEscape="true" />",
                today: "<spring:theme code="text.checkout.availability.label.today" javaScriptEscape='true' />",
                tomorrow: "<spring:theme code="text.checkout.availability.label.tomorrow" javaScriptEscape='true' />",
                future: "<spring:theme code="text.checkout.availability.label.future" javaScriptEscape='true' />"
            }
        },
        weekdays: {
            SUNDAY: "<spring:theme code='text.widget.datepicker.dayNames.su' javaScriptEscape="true" />",
            MONDAY: "<spring:theme code='text.widget.datepicker.dayNames.mo' javaScriptEscape="true" />",
            TUESDAY: "<spring:theme code='text.widget.datepicker.dayNames.tu' javaScriptEscape="true" />",
            WEDNESDAY: "<spring:theme code='text.widget.datepicker.dayNames.we' javaScriptEscape="true" />",
            THURSDAY: "<spring:theme code='text.widget.datepicker.dayNames.th' javaScriptEscape="true" />",
            FRIDAY: "<spring:theme code='text.widget.datepicker.dayNames.fr' javaScriptEscape="true" />",
            SATURDAY: "<spring:theme code='text.widget.datepicker.dayNames.sa' javaScriptEscape="true" />"
        },
        store: {
            currentPointOfServiceChange: "<spring:theme code='text.pointOfService.change.message' javaScriptEscape="true" />"
        }
    };

    VMB.noImageUrl = '<spring:theme code="img.missingProductImage.responsive.product" htmlEscape="false" javaScriptEscape="true"/>';

    <c:url var="addToCartMultiUrl" value="/cart/addMulti" />
    <c:url value="/my-account/saved-carts" var="savedCartsOverviewUrl"/>
    <c:url value="/my-account/saved-baskets" var="wishListsOverviewUrl"/>
    <c:url var="favoriteStoresUrl" value="/_s/stores" />
    <c:url var="checkoutTECStores" value="/checkout/multi/delivery-method/alternativetecstores" />
    <c:url var="stockUpdateUrl" value="/multiStock" />
    <c:url var="netpriceUrl" value="/_s/netprice" />
    <c:url var="addressValidationUrl" value="/av" />

    VMB.urls = {
        addToCartMulti: '${ycommerce:encodeJavaScript(addToCartMultiUrl)}',
        savedCartsOverview: '${ycommerce:encodeJavaScript(savedCartsOverviewUrl)}',
        wishListsOverview: '${ycommerce:encodeJavaScript(wishListsOverviewUrl)}',
        favoriteStores: '${ycommerce:encodeJavaScript(favoriteStoresUrl)}',
        checkoutTECStores: '${ycommerce:encodeJavaScript(checkoutTECStores)}',
        stockUpdate: '${ycommerce:encodeJavaScript(stockUpdateUrl)}',
        netpriceUrl: '${ycommerce:encodeJavaScript(netpriceUrl)}',
        addressValidation: '${ycommerce:encodeJavaScript(addressValidationUrl)}'
    };

    VMB.isSmartEditActive = ${!disableSmartEdit};

    <%-- Define a javascript variable for translating Select 2 wigdet --%>

    VMB.select2 = {
        placeholder: '<spring:theme code="text.dropdown.advanced.placeholder" htmlEscape="false" javaScriptEscape="true" />',
        language: {
            errorLoading: function () {
                return '<spring:theme code="text.dropdown.advanced.errorLoading" htmlEscape="false" javaScriptEscape="true" />'
            },
            inputTooLong: function (e) {
                var t = e.input.length - e.maximum;
                return '<spring:theme code="text.dropdown.advanced.inputTooLong" htmlEscape="false" javaScriptEscape="true" arguments="[0]" />'.replace("[0]", t);
            },
            inputTooShort: function (e) {
                var t = e.minimum - e.input.length;
                return '<spring:theme code="text.dropdown.advanced.inputTooShort" htmlEscape="false" javaScriptEscape="true" arguments="[0]" />'.replace("[0]", t);
            },
            loadingMore: function () {
                return '<spring:theme code="text.dropdown.advanced.loadingMore" htmlEscape="false" javaScriptEscape="true" />';
            },
            maximumSelected: function (e) {
                var t = e.maximum;
                return '<spring:theme code="text.dropdown.advanced.maximumSelected" htmlEscape="false" javaScriptEscape="true" arguments="[0]" />'.replace("[0]", t);
            },
            noResults: function () {
                return '<spring:theme code="text.dropdown.advanced.noResults" htmlEscape="false" javaScriptEscape="true" />'
            },
            searching: function () {
                return '<spring:theme code="text.dropdown.advanced.searching" htmlEscape="false" javaScriptEscape="true" />'
            },
            removeAllItems: function () {
                return '<spring:theme code="text.dropdown.advanced.removeAllItems" htmlEscape="false" javaScriptEscape="true" />'
            }
        }
    };

    VMB.datepicker = {
        regional: {
            closeText: '<spring:theme code="text.widget.datepicker.closeText" htmlEscape="false" javaScriptEscape="true" />',
            prevText: "←",
            nextText: "→",
            currentText: '<spring:theme code="text.widget.datepicker.currentText" htmlEscape="false" javaScriptEscape="true" />',
            monthNames: [
                '<spring:theme code="text.widget.datepicker.monthNames.jan" htmlEscape="false" javaScriptEscape="true" />',
                '<spring:theme code="text.widget.datepicker.monthNames.feb" htmlEscape="false" javaScriptEscape="true" />',
                '<spring:theme code="text.widget.datepicker.monthNames.mar" htmlEscape="false" javaScriptEscape="true" />',
                '<spring:theme code="text.widget.datepicker.monthNames.apr" htmlEscape="false" javaScriptEscape="true" />',
                '<spring:theme code="text.widget.datepicker.monthNames.may" htmlEscape="false" javaScriptEscape="true" />',
                '<spring:theme code="text.widget.datepicker.monthNames.jun" htmlEscape="false" javaScriptEscape="true" />',
                '<spring:theme code="text.widget.datepicker.monthNames.jul" htmlEscape="false" javaScriptEscape="true" />',
                '<spring:theme code="text.widget.datepicker.monthNames.aug" htmlEscape="false" javaScriptEscape="true" />',
                '<spring:theme code="text.widget.datepicker.monthNames.sep" htmlEscape="false" javaScriptEscape="true" />',
                '<spring:theme code="text.widget.datepicker.monthNames.oct" htmlEscape="false" javaScriptEscape="true" />',
                '<spring:theme code="text.widget.datepicker.monthNames.nov" htmlEscape="false" javaScriptEscape="true" />',
                '<spring:theme code="text.widget.datepicker.monthNames.dec" htmlEscape="false" javaScriptEscape="true" />'
            ],
            monthNamesShort: [
                '<spring:theme code="text.widget.datepicker.monthNamesShort.jan" htmlEscape="false" javaScriptEscape="true" />',
                '<spring:theme code="text.widget.datepicker.monthNamesShort.feb" htmlEscape="false" javaScriptEscape="true" />',
                '<spring:theme code="text.widget.datepicker.monthNamesShort.mar" htmlEscape="false" javaScriptEscape="true" />',
                '<spring:theme code="text.widget.datepicker.monthNamesShort.apr" htmlEscape="false" javaScriptEscape="true" />',
                '<spring:theme code="text.widget.datepicker.monthNamesShort.may" htmlEscape="false" javaScriptEscape="true" />',
                '<spring:theme code="text.widget.datepicker.monthNamesShort.jun" htmlEscape="false" javaScriptEscape="true" />',
                '<spring:theme code="text.widget.datepicker.monthNamesShort.jul" htmlEscape="false" javaScriptEscape="true" />',
                '<spring:theme code="text.widget.datepicker.monthNamesShort.aug" htmlEscape="false" javaScriptEscape="true" />',
                '<spring:theme code="text.widget.datepicker.monthNamesShort.sep" htmlEscape="false" javaScriptEscape="true" />',
                '<spring:theme code="text.widget.datepicker.monthNamesShort.oct" htmlEscape="false" javaScriptEscape="true" />',
                '<spring:theme code="text.widget.datepicker.monthNamesShort.nov" htmlEscape="false" javaScriptEscape="true" />',
                '<spring:theme code="text.widget.datepicker.monthNamesShort.dec" htmlEscape="false" javaScriptEscape="true" />'
            ],
            dayNames: [
                '<spring:theme code="text.widget.datepicker.dayNames.su" htmlEscape="false" javaScriptEscape="true" />',
                '<spring:theme code="text.widget.datepicker.dayNames.mo" htmlEscape="false" javaScriptEscape="true" />',
                '<spring:theme code="text.widget.datepicker.dayNames.tu" htmlEscape="false" javaScriptEscape="true" />',
                '<spring:theme code="text.widget.datepicker.dayNames.we" htmlEscape="false" javaScriptEscape="true" />',
                '<spring:theme code="text.widget.datepicker.dayNames.th" htmlEscape="false" javaScriptEscape="true" />',
                '<spring:theme code="text.widget.datepicker.dayNames.fr" htmlEscape="false" javaScriptEscape="true" />',
                '<spring:theme code="text.widget.datepicker.dayNames.sa" htmlEscape="false" javaScriptEscape="true" />'
            ],
            dayNamesShort: [
                '<spring:theme code="text.widget.datepicker.dayNamesShort.su" htmlEscape="false" javaScriptEscape="true" />',
                '<spring:theme code="text.widget.datepicker.dayNamesShort.mo" htmlEscape="false" javaScriptEscape="true" />',
                '<spring:theme code="text.widget.datepicker.dayNamesShort.tu" htmlEscape="false" javaScriptEscape="true" />',
                '<spring:theme code="text.widget.datepicker.dayNamesShort.we" htmlEscape="false" javaScriptEscape="true" />',
                '<spring:theme code="text.widget.datepicker.dayNamesShort.th" htmlEscape="false" javaScriptEscape="true" />',
                '<spring:theme code="text.widget.datepicker.dayNamesShort.fr" htmlEscape="false" javaScriptEscape="true" />',
                '<spring:theme code="text.widget.datepicker.dayNamesShort.sa" htmlEscape="false" javaScriptEscape="true" />'
            ],
            dayNamesMin: [
                '<spring:theme code="text.widget.datepicker.dayNamesShort.su" htmlEscape="false" javaScriptEscape="true" />',
                '<spring:theme code="text.widget.datepicker.dayNamesShort.mo" htmlEscape="false" javaScriptEscape="true" />',
                '<spring:theme code="text.widget.datepicker.dayNamesShort.tu" htmlEscape="false" javaScriptEscape="true" />',
                '<spring:theme code="text.widget.datepicker.dayNamesShort.we" htmlEscape="false" javaScriptEscape="true" />',
                '<spring:theme code="text.widget.datepicker.dayNamesShort.th" htmlEscape="false" javaScriptEscape="true" />',
                '<spring:theme code="text.widget.datepicker.dayNamesShort.fr" htmlEscape="false" javaScriptEscape="true" />',
                '<spring:theme code="text.widget.datepicker.dayNamesShort.sa" htmlEscape="false" javaScriptEscape="true" />'
            ],
            weekHeader: '<spring:theme code="text.widget.datepicker.weekHeader" htmlEscape="false" javaScriptEscape="true" />',
            dateFormat: "yy-mm-dd",
            firstDay: 1,
            showMonthAfterYear: false,
            yearSuffix: ""
        }
    };

    VMB.constraints = {
        savedCartDescriptionFieldLimit: '${VanmarckeCoreConstants.SAVED_CART_DESCRIPTION_FIELD_LIMIT}'
    };
    /*]]>*/
</script>
<template:javaScriptAddOnsVariables/>

<%-- generated variables from commonVariables.properties --%>
<script type="text/javascript" src="${fn:escapeXml(sharedResourcePath)}/js/generatedVariables.js"></script>
	