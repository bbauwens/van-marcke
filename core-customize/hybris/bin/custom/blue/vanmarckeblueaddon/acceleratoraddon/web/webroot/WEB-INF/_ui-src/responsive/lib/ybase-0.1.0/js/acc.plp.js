ACC.plp = {
    _autoload: ["init"],

    pagination: {
        toLoad: false,
    },

    init: function() {
        $(document).ready(function () {
            $(window).scroll(function () {
                var buttonsBar = $('#sticky-buttons');
                if (buttonsBar.length > 0) {
                    if (ACC.plp.isScrolledToBottom()) {
                        ACC.plp.loadMoreProducts();
                    } else {
                        ACC.plp.pagination.toLoad = true;
                    }

                    var firstHeadlineBox = $('.vmb-plp-list-product-headline').get(0).getBoundingClientRect();
                    var initialBottom = firstHeadlineBox.bottom;

                    if ($(this).scrollTop() > initialBottom) {
                        $('.scroll-to-top-wrap').css('display', 'flex');
                    } else if ($('#sticky-buttons').length > 0) {
                        $('.scroll-to-top-wrap').css('display', 'none');
                    }
                }

            });
        });
    },

    loadMoreProducts: function () {
        let currentPage = $('.currentPage').last().val();
        if (typeof currentPage === 'undefined') {
            currentPage = 0;
        } else {
            currentPage = parseInt(currentPage);
        }

        let areProductsLeftToLoad = $('#search-product-left-to-load-' + currentPage).val();

        if (typeof areProductsLeftToLoad === 'undefined') {
            let countOfLoadedProducts = $('.vmb-product-main-info').length;
            areProductsLeftToLoad = $('.totalNumberOfResults').val() > countOfLoadedProducts;
        } else {
            areProductsLeftToLoad = areProductsLeftToLoad === 'true';
        }

        if (!$("main").hasClass("loading") && ACC.plp.pagination.toLoad && areProductsLeftToLoad) {
            let productUrl = ACC.config.encodedContextPath + $('#search-query-input').val();
            let nextPage = currentPage + 1;

            if (productUrl.indexOf('?') > -1) {
                let params = productUrl.slice(productUrl.indexOf('?') + 1);
                productUrl = productUrl.slice(0, productUrl.indexOf('?')) + "/products?page=" + nextPage + "&" + params;
            } else {
                productUrl = productUrl + "/products?page=" + nextPage;
            }

            $.ajax({
                url: productUrl,
                type: 'get',
                beforeSend: function () {
                    // prevent another product page load while the current loading is in progress
                    ACC.plp.pagination.toLoad = false;
                    showLoader();
                },
                success: function (response) {
                    if (response !== "") {
                        $('.vmb-plp-products').append(response);
                        $('#sticky-buttons').detach().appendTo('.vmb-plp-products');

                        // fetch stock for new products
                        window.mediator.publish('updateStockIndication', '');
                    }
                },
                error: function (xhr) {
                    //Do Something to handle error
                },
                complete: function () {
                    hideLoader();
                }
            });
        }

    },

    onScrollToTopClick: function () {
        $("html, body").animate({ scrollTop: 0 }, "slow");
    },

    isScrolledIntoView: function (elem, completely) {
        let rect = elem.getBoundingClientRect();
        let elemTop = rect.top;
        let elemBottom = rect.bottom;
        return completely ? elemBottom < window.innerHeight : elemTop < window.innerHeight && elemBottom >= 0;
    },

    isScrolledToBottom: function() {
        let elem = $('.vmb-product-main-info').last().get(0);
        return ACC.plp.isScrolledIntoView(elem, true);
    }
};