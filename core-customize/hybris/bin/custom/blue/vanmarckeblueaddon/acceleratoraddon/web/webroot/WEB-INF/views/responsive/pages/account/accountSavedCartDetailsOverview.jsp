<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/cart" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common" %>
<%@ taglib prefix="order" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/order" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/shared/format" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<sec:authorize var="isAnonymousUser" access="hasAnyRole('ROLE_ANONYMOUS')"/>

<spring:htmlEscape defaultHtmlEscape="true"/>

<spring:url value="/my-account/saved-carts" var="savedCartsUrl" htmlEscape="false"/>

<div class="container vmb-saved-cart-details">
    <div class="back-link">
        <a href="#" data-base-href="${savedCartsUrl}" class="savedCartBackBtn">
            <span class="glyphicon glyphicon-chevron-left"></span>
        </a>
        <span class="label"><spring:theme code="text.account.savedCart.title.details"/></span>
    </div>
    <div class="vmb-saved-cart-details__info">
        <div class="row">
            <div class="col-xs-12 col-sm-push-10 col-sm-2 item-action col-no-padding hidden-print vmb-saved-cart-details__info__actions">

                <c:if test="${savedCartData.technicalSheetsAvailable}">
                    <a class="btn btn-link" href="<c:url value="/my-account/saved-carts/${fn:escapeXml(savedCartData.code)}/technicalsheets"/>" title="<spring:theme code="text.account.savedCart.download.techsheets"/>">
                        <span class="icon icon-download_spec"></span>
                    </a>
                </c:if>

                <button type="button" class="btn btn-link" onclick="print()">
                    <span class="glyphicon glyphicon-print"></span>
                </button>

                <button type="button" class="btn btn-link js-update-saved-cart edit edit-item-link">
                    <span class="glyphicon glyphicon-pencil"></span>
                </button>

                <button class="btn btn-link js-delete-saved-cart remove-item-link" data-savedcart-id="${fn:escapeXml(savedCartData.code)}" data-savedcart-name="${fn:escapeXml(savedCartData.name)}">
                    <span class="glyphicon glyphicon-trash"></span>
                </button>

                <c:if test="${fn:length(savedCartData.entries) > 0}">
                    <button type="button" class="btn btn-link js-restore-saved-cart restore-item-link" data-savedcart-id="${fn:escapeXml(savedCartData.code)}">
                        <span class="glyphicon glyphicon-share-alt"></span>&nbsp;<span><spring:theme code='text.account.savedCart.restore'/></span>
                    </button>
                </c:if>
            </div>
            <div class="col-xs-12 col-sm-pull-2 col-sm-10 col-no-padding vmb-saved-cart-details__info__data">
                <div class="row">
                    <div class="col-sm-5 item-wrapper">
                        <div class="item-group">
                            <ycommerce:testId code="savedCartDetails_overviewName_label">
                                <span class="item-label"><spring:theme code="text.account.savedCart.name"/></span>
                                <span class="item-value">${fn:escapeXml(savedCartData.name)}</span>
                            </ycommerce:testId>
                        </div>
                    </div>
                    <div class="col-sm-5 item-wrapper">
                        <div class="item-group">
                            <ycommerce:testId code="savedCartDetails_overviewSaveDate_label">
                                <span class="item-label"><spring:theme code="text.account.savedCart.dateSaved"/></span>
                                <span class="item-value"><fmt:formatDate value="${savedCartData.saveTime}"
                                                                         dateStyle="medium" timeStyle="short"
                                                                         type="both"/></span>
                            </ycommerce:testId>
                        </div>
                    </div>
                    <div class="col-sm-2 item-wrapper">
                        <div class="item-group">
                            <ycommerce:testId code="savedCartDetails_overviewNumItems_label">
                                <span class="item-label"><spring:theme code="text.account.savedCart.qty"/></span>
                                <span class="item-value">${fn:length(savedCartData.entries)}</span>
                            </ycommerce:testId>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <ycommerce:testId code="savedCartDetails_overviewDescription_label">
                            <span class="item-label"><spring:theme code="text.account.savedCart.description"/></span>
                            <span class="item-value">${fn:escapeXml(savedCartData.description)}</span>
                        </ycommerce:testId>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <h3 class="vmb-saved-cart-details__producttitle"><spring:theme code="text.account.savedCarts.products"/></h3>

    <table class="table table-condensed vmb-product-table">
        <thead>
        <tr>
            <th class="vmb-productitem__toggle"></th>
            <th class="vmb-productitem__image"></th>
            <th class="vmb-productitem__info"><spring:theme code="basket.page.item"/></th>
            <th class="vmb-productitem__quantity"><spring:theme code="basket.page.qty"/></th>
            <th class="vmb-productitem__stock-warehouse" data-toggle="tooltip" data-placement="top" title="<spring:theme code="text.stockstatus.icon.warehouse" htmlEscape="false"/>"></th>
            <c:if test="${cmsSite.channel != 'DIY'}">
                <th class="vmb-productitem__stock-tec" data-toggle="tooltip" data-placement="top" title="<spring:theme code="text.stockstatus.icon.tec" htmlEscape="false"/>">&nbsp;<format:nostock/></th>
                <th class="vmb-productitem__price"><spring:theme code="basket.page.price" arguments="${currentCurrency.symbol}"/></th>
            </c:if>
            <c:if test="${!isAnonymousUser and (user.showNetPrice or cmsSite.channel == 'DIY')}">
                <th class="vmb-productitem__nettoprice vmb-netto-price-display"><spring:theme code="basket.page.price.netto" arguments="${currentCurrency.symbol}"/></th>
            </c:if>
            <th class="vmb-productitem__total--column"><spring:theme code="basket.page.total"/></th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${savedCartData.entries}" var="entry" varStatus="loop">
            <order:orderEntryDetails orderEntry="${entry}" order="${savedCartData}" itemIndex="${loop.index}" showStock="true"/>
        </c:forEach>
        </tbody>
    </table>

    <div class="vmb-saved-cart-details__bottom hidden-print">
        <a href="#" data-base-href="${savedCartsUrl}" class="btn btn-link savedCartBackBtn">
            <spring:theme code="text.account.savedCart.backToSavedCarts"/>
        </a>

        <c:if test="${fn:length(savedCartData.entries) > 0}">
            <button type="button" id="restoreButton" class="btn btn-default js-restore-saved-cart restore-item-link" data-savedcart-id="${fn:escapeXml(savedCartData.code)}">
                <span class="glyphicon glyphicon-share-alt"></span>&nbsp;<spring:theme code="text.account.savedCart.restore"/>
            </button>
        </c:if>
    </div>

</div>

<spring:url value="/my-account/saved-carts/{/cartId}/edit" var="actionUrl" htmlEscape="false">
    <spring:param name="cartId" value="${savedCartData.code}"/>
</spring:url>
<cart:saveCartModal actionUrl="${actionUrl}" titleKey="text.account.savedCart.edit.title"/>

<cart:savedCartDeleteModal/>