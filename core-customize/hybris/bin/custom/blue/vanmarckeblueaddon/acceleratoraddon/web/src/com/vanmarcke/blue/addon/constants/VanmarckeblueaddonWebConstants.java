package com.vanmarcke.blue.addon.constants;

/**
 * Global class for all Vanmarckeblueaddon web constants. You can add global constants for your extension into this
 * class.
 */
public final class VanmarckeblueaddonWebConstants // NOSONAR
{

  public static final String META_TAG_PAGE_TITLE = "meta.tag.default.title";
  public static final String META_TAG_PAGE_DESCRIPTION = "meta.tag.default.description";

  private VanmarckeblueaddonWebConstants() {
    //empty to avoid instantiating this constant class
  }

  // implement here constants used by this extension

  public enum ProductListViewType
  {
    Grid,
    List;
  }

}
