<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<spring:htmlEscape defaultHtmlEscape="true" />

<div class="vmb-modal fade" id="vmbCWishListWarningModal" tabindex="-1" role="dialog" aria-labelledby="vmbCWishListWarningModalTitle">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="vmbCWishListWarningModalTitle"><spring:theme code='text.account.savedcart.delete.popuptitle'/></h4>
			</div>
			<div class="modal-body">
				<p>
					<spring:theme code="text.account.savedcart.delete.msg"/>
				</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">
					<spring:theme code="general.delete.button"/>
				</button>
			</div>
		</div>
	</div>
</div>
