<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="title">${componentTitle}</div>
<ul class="footer__nav--links">
    <c:if test="${not empty componentContent}">
        <li class="yCmsComponent footer__link">${componentContent}</li>
    </c:if>
    <c:if test="${not empty componentTelephone}">
        <li class="yCmsComponent footer__link">
        <span class="glyphicon glyphicon-earphone">
            <a href="tel:${componentTelephone}"><span class="glyphicon-text-vmk-styling">${componentTelephoneFrontend}</span></a>
        </span>
        </li>
    </c:if>
    <c:if test="${not empty componentEmail}">
        <li class="yCmsComponent footer__link">
        <span class="glyphicon glyphicon-envelope">
            <a href="mailto:${componentEmail}"><span class="glyphicon-text-vmk-styling">${componentEmail}</span></a>
        </span>
        </li>
    </c:if>
    <c:if test="${not empty screenTakeoverUrl}">
        <li class="yCmsComponent footer__link">
            <a href="${screenTakeoverUrl}" target="_blank">
                <spring:message code="footer.checkout.screen.takeover.text"/>
            </a>
        </li>
    </c:if>
</ul>