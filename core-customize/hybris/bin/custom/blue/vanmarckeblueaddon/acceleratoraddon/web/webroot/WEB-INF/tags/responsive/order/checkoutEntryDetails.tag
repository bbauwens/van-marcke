<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="order" required="true" type="de.hybris.platform.commercefacades.order.data.OrderData" %>
<%@ attribute name="orderEntry" required="true" type="de.hybris.platform.commercefacades.order.data.OrderEntryData" %>
<%@ attribute name="consignmentEntry" required="false" type="de.hybris.platform.commercefacades.order.data.ConsignmentEntryData" %>
<%@ attribute name="itemIndex" required="true" type="java.lang.Integer" %>
<%@ attribute name="showStock" required="false" type="java.lang.Boolean" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/product" %>
<%@ taglib prefix="grid" tagdir="/WEB-INF/tags/responsive/grid" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="order" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/order" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/shared/format" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<spring:htmlEscape defaultHtmlEscape="true"/>

<c:set var="varShowStock" value="${(empty showStock) ? true : showStock}"/>

<c:url value="${orderEntry.product.url}" var="productUrl"/>
<c:set var="entryStock" value="${fn:escapeXml(orderEntry.product.stock.stockLevelStatus.code)}"/>

<sec:authorize var="isAnonymousUser" access="hasAnyRole('ROLE_ANONYMOUS')"/>

<input type="hidden" value="${cmsSite.channel == 'DIY'}" id="isDIY"/>

<tr class="vmb-gtm-product-data"
        data-code="${fn:escapeXml(orderEntry.product.code)}"
        data-name="${fn:escapeXml(orderEntry.product.name)}"
        data-category="${fn:escapeXml(orderEntry.product.analyticsData.category)}"
        data-brand="${fn:escapeXml(orderEntry.product.analyticsData.brand)}"
        data-url="${productUrl}"
        data-list="Savedcart products"
        data-position="${itemIndex + 1}"
        data-price="${orderEntry.basePrice.value}"
>
    <%-- chevron for multi-d products --%>
    <td class="hidden-xs hidden-sm item__toggle"></td>

    <%-- product image --%>
    <td class="vmb-productitem__image">
        <a class="vmb-gtm-product-item" href="${orderEntry.product.purchasable ? productUrl : '#'}">
            <product:productPrimaryImage product="${orderEntry.product}" format="thumbnail"/>
        </a>
    </td>

    <%-- product name, code, promotions --%>
    <td class="vmb-productitem__info">
        <a class="vmb-gtm-product-item" href="${orderEntry.product.purchasable ? productUrl : '#'}"><span class="name">${fn:escapeXml(orderEntry.product.name)}</span></a>

        <div class="code"><spring:theme code="basket.page.entry.sku"/>: ${fn:escapeXml(orderEntry.product.code)}</div>

        <c:if test="${not empty order.appliedProductPromotions}">
            <div class="promo">
                <ul>
                    <c:forEach items="${order.appliedProductPromotions}" var="promotion">
                        <c:set var="displayed" value="false"/>
                        <c:forEach items="${promotion.consumedEntries}" var="consumedEntry">
                            <c:if test="${not displayed and consumedEntry.orderEntryNumber == orderEntry.entryNumber}">
                                <c:set var="displayed" value="true"/>
                                <li>
                                        ${ycommerce:sanitizeHTML(promotion.description)}
                                </li>
                            </c:if>
                        </c:forEach>
                    </c:forEach>
                </ul>
            </div>
        </c:if>
    </td>
        <c:if test="${order.isSplitted eq false}">
            <%-- selected date --%>
            <td class="vmb-productitem__date">
                <fmt:formatDate value="${orderEntry.deliveryPickupDate}" pattern="dd-MM-yyyy"/>
            </td>
        </c:if>
        <%-- quantity --%>
        <td class="vmb-productitem__quantity" data-label="<spring:theme code="text.account.order.qty"/>:">
       <span class="count">
            <c:choose>
                <c:when test="${consignmentEntry ne null }">
                    ${consignmentEntry.quantity}
                </c:when>
                <c:otherwise>
                    ${orderEntry.quantity}
                </c:otherwise>
            </c:choose>
       </span>
    </td>

        <c:if test="${varShowStock}">

            <%-- stock store --%>
            <td class="vmb-productitem__stock-warehouse">
                <format:stock type="warehouse" stock="${orderEntry.product.stock}" code="${orderEntry.product.code}"/>
            </td>

            <c:if test="${cmsSite.channel != 'DIY'}">
                <%-- stock TEC --%>
                <td class="vmb-productitem__stock-tec">
                    <format:stock type="store" stock="${orderEntry.product.stock}" code="${orderEntry.product.code}"/>
                </td>
            </c:if>

        </c:if>

    <c:if test="${!isAnonymousUser and (user.showNetPrice or cmsSite.channel == 'DIY')}">
        <%-- netto --%>
        <td class="vmb-productitem__nettoprice" data-label="<spring:theme code="basket.page.price.netto" arguments="${currentCurrency.symbol}"/>:">
            <order:orderEntryPrice orderEntry="${orderEntry}"/>
        </td>
    </c:if>

    <%-- total --%>
    <td class="vmb-productitem__total js-item-total" data-label="<spring:theme code="basket.page.total"/>:">
        <format:price priceData="${orderEntry.totalPrice}" displayFreeForZero="true"/>
    </td>

    <td></td>
</tr>

