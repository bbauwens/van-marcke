package com.vanmarcke.blue.addon.controllers.pages.checkout.steps;

import com.elision.hybris.l10n.facades.LocalizedMessageFacade;
import com.vanmarcke.facades.order.VMKCheckoutFacade;
import com.vanmarcke.facades.paymentmodes.VMKPaymentModeFacade;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.PreValidateCheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.PreValidateQuoteCheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.CheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.checkout.steps.AbstractCheckoutStepController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.b2bacceleratoraddon.forms.PaymentTypeForm;
import de.hybris.platform.b2bacceleratorfacades.order.data.B2BPaymentTypeData;
import de.hybris.platform.b2bacceleratorservices.enums.CheckoutPaymentType;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.order.data.CartData;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.Resource;
import java.util.Collection;

import static com.vanmarcke.blue.addon.controllers.VanmarckeblueaddonControllerConstants.Views.Pages.MultiStepCheckout.CHOOSE_PAYMENT_TYPE_PAGE;

@Controller
@RequestMapping(value = "/checkout/multi/payment-type")
public class BluePaymentTypeCheckoutStepController extends AbstractCheckoutStepController {

    private final static String PAYMENT_TYPE = "payment-type";

    @Resource(name = "vmkCheckoutFacade")
    private VMKCheckoutFacade blueCheckoutFacade;

    @Resource(name = "vmkPaymentModeFacade")
    private VMKPaymentModeFacade vmkPaymentModeFacade;

    @Resource(name = "localizedMessageFacade")
    private LocalizedMessageFacade messageFacade;

    @Override
    @RequestMapping(value = "/choose", method = RequestMethod.GET)
    @RequireHardLogIn
    @PreValidateQuoteCheckoutStep
    @PreValidateCheckoutStep(checkoutStep = PAYMENT_TYPE)
    public String enterStep(final Model model, final RedirectAttributes redirectAttributes)
            throws CMSItemNotFoundException {
        final CartData cartData = getCheckoutFacade().getCheckoutCart();

        Collection<B2BPaymentTypeData> paymentTypes = blueCheckoutFacade.getPaymentTypes();

        model.addAttribute("cartData", cartData);
        model.addAttribute("paymentTypeForm", preparePaymentTypeForm(cartData, paymentTypes));
        model.addAttribute("paymentModes", vmkPaymentModeFacade.getSupportedPaymentModes());
        model.addAttribute("paymentTypes", paymentTypes);
        model.addAttribute("isCreditWorthy", cartData.isCreditworthy());
        model.addAttribute("isCreditLimitWarning", cartData.isCreditLimitWarning());
        model.addAttribute("isCreditLimitExceeded", cartData.isCreditLimitExceeded());

        if (cartData.isCreditLimitWarning()) {
            GlobalMessages.addInfoMessage(model, messageFacade.getMessageForCodeAndCurrentLocale("text.checkout.payment.credit.warning"));
        }

        prepareDataForPage(model);
        storeCmsPageInModel(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
        model.addAttribute(WebConstants.BREADCRUMBS_KEY, getResourceBreadcrumbBuilder().getBreadcrumbs("checkout.multi.paymentType.breadcrumb"));
        model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
        setCheckoutStepLinksForModel(model, getCheckoutStep());

        return CHOOSE_PAYMENT_TYPE_PAGE;
    }

    @RequestMapping(value = "/choose", method = RequestMethod.POST)
    @RequireHardLogIn
    public String choose(@ModelAttribute final PaymentTypeForm paymentTypeForm,
                         final BindingResult bindingResult,
                         final Model model) throws CMSItemNotFoundException {
        if (bindingResult.hasErrors()) {

            GlobalMessages.addErrorMessage(model, "checkout.error.paymenttype.formentry.invalid");
            model.addAttribute("paymentTypeForm", paymentTypeForm);
            prepareDataForPage(model);
            storeCmsPageInModel(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
            setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
            model.addAttribute(WebConstants.BREADCRUMBS_KEY, getResourceBreadcrumbBuilder().getBreadcrumbs("checkout.multi.paymentType.breadcrumb"));
            model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
            setCheckoutStepLinksForModel(model, getCheckoutStep());
            return CHOOSE_PAYMENT_TYPE_PAGE;
        }

        updateCheckoutCart(paymentTypeForm);

        return getCheckoutStep().nextStep();
    }

    protected void updateCheckoutCart(final PaymentTypeForm paymentTypeForm) {
        final CartData cartData = new CartData();

        // set payment type
        final B2BPaymentTypeData paymentTypeData = new B2BPaymentTypeData();
        paymentTypeData.setCode(paymentTypeForm.getPaymentType());

        cartData.setPaymentType(paymentTypeData);

        // set purchase order number
        cartData.setPurchaseOrderNumber(paymentTypeForm.getPurchaseOrderNumber());

        blueCheckoutFacade.updateCheckoutCart(cartData);
    }

    @RequestMapping(value = "/next", method = RequestMethod.GET)
    @RequireHardLogIn
    @Override
    public String next(final RedirectAttributes redirectAttributes) {
        return getCheckoutStep().nextStep();
    }

    @RequestMapping(value = "/back", method = RequestMethod.GET)
    @RequireHardLogIn
    @Override
    public String back(final RedirectAttributes redirectAttributes) {
        return getCheckoutStep().previousStep();
    }

    protected PaymentTypeForm preparePaymentTypeForm(final CartData cartData, Collection<B2BPaymentTypeData> paymentTypes) {
        PaymentTypeForm paymentTypeForm = new PaymentTypeForm();
        paymentTypeForm.setPurchaseOrderNumber(cartData.getPurchaseOrderNumber());

        B2BPaymentTypeData selectedPaymentType = cartData.getPaymentType();
        if (selectedPaymentType != null && paymentTypes.stream().anyMatch(paymentType -> paymentType.getCode().equals(selectedPaymentType.getCode()))) {
            paymentTypeForm.setPaymentType(selectedPaymentType.getCode());
        } else {
            if (paymentTypes.stream().anyMatch(paymentType -> paymentType.getCode().equals(CheckoutPaymentType.ACCOUNT.getCode()))) {
                paymentTypeForm.setPaymentType(CheckoutPaymentType.ACCOUNT.getCode());
            } else {
                paymentTypes
                        .stream()
                        .findFirst()
                        .ifPresent(paymentType -> paymentTypeForm.setPaymentType(paymentType.getCode()));
            }
        }

        return paymentTypeForm;
    }

    protected CheckoutStep getCheckoutStep() {
        return getCheckoutStep(PAYMENT_TYPE);
    }

}
