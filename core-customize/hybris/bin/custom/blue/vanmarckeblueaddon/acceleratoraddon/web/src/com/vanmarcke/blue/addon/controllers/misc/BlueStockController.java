package com.vanmarcke.blue.addon.controllers.misc;

import com.vanmarcke.facades.stock.VMKStockFacade;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.AbstractController;
import de.hybris.platform.commercefacades.product.data.ProductData;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;

/**
 * Controller for product details page
 */
@Controller
@RequestMapping("/multiStock")
public class BlueStockController extends AbstractController {

    @Resource(name = "stockFacade")
    private VMKStockFacade stockFacade;

    @GetMapping(value = "/{productCode}")
    public ResponseEntity<ProductData> productStock(@PathVariable String productCode) {
        return ResponseEntity.ok(stockFacade.getProductDataWithStockForCurrentWareHouse(decode(productCode)));
    }

    /**
     * Decodes a string with UTF-8 encoding.
     *
     * @param source A string to decode
     * @return A decoded string
     */
    protected String decode(String source) {
        try {
            return URLDecoder.decode(source, StandardCharsets.UTF_8.toString());
        } catch (UnsupportedEncodingException e) {
            return source;
        }
    }
}