    <%@ page trimDirectiveWhitespaces="true" %>
        <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
        <%@ taglib prefix="template" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/template" %>
        <%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>

        <template:page pageTitle="${pageTitle}">


            <cms:pageSlot position="Section2A" var="feature" element="div" class="row">
                <cms:component component="${feature}" element="div" class="col-xs-12 vmb-home-s2a"/>
            </cms:pageSlot>

            <div class="row">
            <cms:pageSlot position="Section2B" var="feature" element="">
                <cms:component component="${feature}" element="div" class="col-xs-12 col-sm-3 vmb-home-s2b"/>
            </cms:pageSlot>
            <cms:pageSlot position="Section2C" var="feature">
                <cms:component component="${feature}" element="div" class="col-xs-12 col-sm-3 vmb-home-s2c"/>
            </cms:pageSlot>
            <cms:pageSlot position="Section2D" var="feature">
                <cms:component component="${feature}" element="div" class="col-xs-12 col-sm-3 vmb-home-s2d"/>
            </cms:pageSlot>
            <cms:pageSlot position="Section2E" var="feature">
                <cms:component component="${feature}" element="div" class="col-xs-12 col-sm-3 vmb-home-s2e"/>
            </cms:pageSlot>
            </div>

            <cms:pageSlot position="Section3" var="feature" element="div" class="row">
                <cms:component component="${feature}" element="div" class="col-xs-12 vmb-home-s3"/>
            </cms:pageSlot>

            <cms:pageSlot position="Section4" var="feature" element="div" class="row">
                <cms:component component="${feature}" element="div" class="col-xs-12 vmb-home-s4"/>
            </cms:pageSlot>

            <div class="row">
            <cms:pageSlot position="Section5" var="feature" element="" class="">
                <cms:component component="${feature}" element="div" class="col-xs-12 col-sm-6 vmb-home-s5"/>
            </cms:pageSlot>

            <cms:pageSlot position="Section5B" var="feature" element="" class="">
                <cms:component component="${feature}" element="div" class="col-xs-12 col-sm-6 vmb-home-s5b"/>
            </cms:pageSlot>
            </div>

            <div class="row">
            <cms:pageSlot position="Section6" var="feature" element="" class="">
                <cms:component component="${feature}" element="div" class="col-xs-12 col-sm-6 vmb-home-s6"/>
            </cms:pageSlot>
            <cms:pageSlot position="Section6B" var="feature" element="" class="">
                <cms:component component="${feature}" element="div" class="col-xs-12 col-sm-6 vmb-home-s6b"/>
            </cms:pageSlot>
            </div>

            <div class="row">
            <cms:pageSlot position="Section7A" var="feature" element="" class="">
                <cms:component component="${feature}" element="div" class="col-xs-12 col-sm-9 vmb-home-s7a"/>
            </cms:pageSlot>
            <cms:pageSlot position="Section7B" var="feature" element="" class="">
                <cms:component component="${feature}" element="div" class="col-xs-12 col-sm-3 vmb-home-s7b"/>
            </cms:pageSlot>
            </div>

            <div class="row">
            <cms:pageSlot position="Section8A" var="feature" element="" class="">
                <cms:component component="${feature}" element="div" class="col-xs-12 col-sm-9 vmb-home-s8a"/>
            </cms:pageSlot>
            <cms:pageSlot position="Section8B" var="feature" element="" class="">
                <cms:component component="${feature}" element="div" class="col-xs-12 col-sm-3 vmb-home-s8b"/>
            </cms:pageSlot>
            </div>

            <div class="row">
            <cms:pageSlot position="Section9A" var="feature" element="" class="">
                <cms:component component="${feature}" element="div" class="col-xs-12 col-sm-4 vmb-home-s9a"/>
            </cms:pageSlot>
            <cms:pageSlot position="Section9B" var="feature" element="" class="">
                <cms:component component="${feature}" element="div" class="col-xs-12 col-sm-4 vmb-home-s9b"/>
            </cms:pageSlot>
            <cms:pageSlot position="Section9C" var="feature" element="" class="">
                <cms:component component="${feature}" element="div" class="col-xs-12 col-sm-4 vmb-home-s9c"/>
            </cms:pageSlot>
            </div>

            <div class="row">
            <cms:pageSlot position="Section10A" var="feature" element="" class="">
                <cms:component component="${feature}" element="div" class="col-xs-12 col-sm-4 vmb-home-s10a"/>
            </cms:pageSlot>
            <cms:pageSlot position="Section10B" var="feature" element="" class="">
                <cms:component component="${feature}" element="div" class="col-xs-12 col-sm-4 vmb-home-s10b"/>
            </cms:pageSlot>
            <cms:pageSlot position="Section10C" var="feature" element="" class="">
                <cms:component component="${feature}" element="div" class="col-xs-12 col-sm-4 vmb-home-s10c"/>
            </cms:pageSlot>
            </div>

            <div class="row">
            <cms:pageSlot position="Section11A" var="feature" element="" class="">
                <cms:component component="${feature}" element="div" class="col-xs-12 col-sm-3 vmb-home-s11a"/>
            </cms:pageSlot>
            <cms:pageSlot position="Section11B" var="feature" element="" class="">
                <cms:component component="${feature}" element="div" class="col-xs-12 col-sm-3 vmb-home-s11b"/>
            </cms:pageSlot>
            <cms:pageSlot position="Section11C" var="feature" element="" class="">
                <cms:component component="${feature}" element="div" class="col-xs-12 col-sm-3 vmb-home-s11c"/>
            </cms:pageSlot>
            <cms:pageSlot position="Section11D" var="feature" element="" class="">
                <cms:component component="${feature}" element="div" class="col-xs-12 col-sm-3 vmb-home-s11d"/>
            </cms:pageSlot>
            </div>

            <div class="row">
            <cms:pageSlot position="Section12A" var="feature" element="" class="">
                <cms:component component="${feature}" element="div" class="col-xs-12 col-sm-3 vmb-home-s12a"/>
            </cms:pageSlot>
            <cms:pageSlot position="Section12B" var="feature" element="" class="">
                <cms:component component="${feature}" element="div" class="col-xs-12 col-sm-3 vmb-home-s12b"/>
            </cms:pageSlot>
            <cms:pageSlot position="Section12C" var="feature" element="" class="">
                <cms:component component="${feature}" element="div" class="col-xs-12 col-sm-3 vmb-home-s12c"/>
            </cms:pageSlot>
            <cms:pageSlot position="Section12D" var="feature" element="" class="">
                <cms:component component="${feature}" element="div" class="col-xs-12 col-sm-3 vmb-home-s12d"/>
            </cms:pageSlot>
            </div>

            <cms:pageSlot position="Section13" var="feature" element="div" class="row">
                <cms:component component="${feature}" element="div" class="col-xs-12 vmb-home-s13"/>
            </cms:pageSlot>

            <cms:pageSlot position="Section14" var="feature" element="div" class="row">
                <cms:component component="${feature}" element="div" class="col-xs-12 vmb-home-s14"/>
            </cms:pageSlot>

        </template:page>
