<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="cartData" required="true" type="de.hybris.platform.commercefacades.order.data.CartData" %>
<%@ attribute name="showDeliveryAddress" required="true" type="java.lang.Boolean" %>
<%@ attribute name="showPaymentInfo" required="false" type="java.lang.Boolean" %>
<%@ attribute name="showTax" required="false" type="java.lang.Boolean" %>
<%@ attribute name="showTaxEstimate" required="false" type="java.lang.Boolean" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/checkout/multi" %>
<%@ taglib prefix="order" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/order" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<spring:htmlEscape defaultHtmlEscape="true"/>
<spring:url value="/checkout/multi/summary/placeOrder" var="placeOrderUrl" htmlEscape="false"/>
<spring:url value="/checkout/multi/termsAndConditions" var="getTermsAndConditionsUrl" htmlEscape="false"/>
<div class="checkout-order-summary checkout-review-xs">
    <ycommerce:testId code="orderSummary">
        <multi-checkout:paymentInfo cartData="${cartData}" paymentInfo="${cartData.paymentInfo}"
                                    showPaymentInfo="${showPaymentInfo}"/>
        <multi-checkout:deliveryAddress cartData="${cartData}" showDeliveryAddress="${showDeliveryAddress}"/>
        <multi-checkout:pickupAddress cartData="${cartData}" showDeliveryAddress="${showDeliveryAddress}"/>
        <order:appliedVouchers order="${cartData}"/>
        <multi-checkout:orderTotals cartData="${cartData}" showTaxEstimate="${showTaxEstimate}" showTax="${showTax}"/>
        <multi-checkout:checkoutProductList cartData="${cartData}"/>
    </ycommerce:testId>

    <div class="place-order-form visible-xs">
        <form:form action="${placeOrderUrl}" id="placeOrderForm1" modelAttribute="placeOrderForm">
            <div class="checkbox">
                <label> <form:checkbox id="Terms1" path="termsCheck"/>
                    <spring:theme var="termsAndConditionsHtml" code="checkout.summary.placeOrder.readTermsAndConditions"
                                  arguments="${fn:escapeXml(getTermsAndConditionsUrl)}" htmlEscape="false"/>
                        ${ycommerce:sanitizeHTML(termsAndConditionsHtml)}
                </label>
            </div>
            <button id="placeOrder" type="submit"
                    class="btn btn-default btn-block btn-place-order btn-block btn-lg checkoutSummaryButton"
                    disabled="disabled">
                <spring:theme code="checkout.summary.placeOrder"/>
            </button>
        </form:form>
    </div>
</div>