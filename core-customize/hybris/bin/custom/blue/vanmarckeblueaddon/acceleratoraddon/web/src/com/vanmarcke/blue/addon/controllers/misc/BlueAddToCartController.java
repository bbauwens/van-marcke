package com.vanmarcke.blue.addon.controllers.misc;

import com.vanmarcke.facades.analytics.data.CartAnalyticsData;
import com.vanmarcke.facades.cart.VMKCartFacade;
import de.hybris.platform.acceleratorfacades.product.data.ProductWrapperData;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.AbstractController;
import de.hybris.platform.acceleratorstorefrontcommons.forms.AddToCartForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.AddToCartOrderForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.AddToEntryGroupForm;
import de.hybris.platform.commercefacades.order.converters.populator.GroupCartModificationListPopulator;
import de.hybris.platform.commercefacades.order.data.AddToCartParams;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.order.CommerceCartMergingException;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.util.Config;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.text.MessageFormat;
import java.util.*;

import static com.vanmarcke.blue.addon.controllers.VanmarckeblueaddonControllerConstants.Views.Fragments.Cart.ADD_TO_CART_POPUP;

/**
 * The {@code BlueAddToCartController} class exposes the endpoints to add products to the cart.
 * <p>
 * This functionality is not bound to a specific page.
 *
 * @author Niels Raemakers, Taki Korovessis, Christiaan Janssen
 * @since 24-05-2019
 */
@Controller
public class BlueAddToCartController extends AbstractController {

    private static final Logger LOG = Logger.getLogger(BlueAddToCartController.class);

    private static final String QUANTITY_ATTR = "quantity";
    private static final String TYPE_MISMATCH_ERROR_CODE = "typeMismatch";
    private static final String ERROR_MSG_TYPE = "errorMsg";
    private static final String QUANTITY_INVALID_BINDING_MESSAGE_KEY = "basket.error.quantity.invalid.binding";
    private static final String SHOWN_PRODUCT_COUNT = "vanmarckestorefront.storefront.minicart.shownProductCount";

    @Resource(name = "vmkCartFacade")
    private VMKCartFacade cartFacade;

    @Resource(name = "productVariantFacade")
    private ProductFacade productFacade;

    @Resource(name = "groupCartModificationListPopulator")
    private GroupCartModificationListPopulator groupCartModificationListPopulator;

    @Resource
    private Converter<CartModificationData, CartAnalyticsData> cartAnalyticsConverter;

    /**
     * Adds a product to the cart based on the given {@code form} and returns the add-to-cart popup afterwards.
     *
     * @param code          the product code
     * @param model         the view model
     * @param form          the add-to-cart form which contains the information about the product to add
     * @param bindingResult the binding result
     * @return the add-to-cart popup
     */
    @PostMapping(value = "/cart/add", produces = MediaType.APPLICATION_JSON_VALUE)
    public String addToCart(@RequestParam("productCodePost") String code, Model model, @Valid AddToCartForm form, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return getViewWithBindingErrorMessages(model, bindingResult);
        }

        long qty = form.getQty();

        if (qty <= 0) {
            model.addAttribute(ERROR_MSG_TYPE, "basket.error.quantity.invalid");
            model.addAttribute(QUANTITY_ATTR, 0L);
        } else {
            try {
                CartModificationData cartModification = cartFacade.addToCart(code, qty);
                model.addAttribute(QUANTITY_ATTR, cartModification.getQuantityAdded());
                model.addAttribute("entry", cartModification.getEntry());
                model.addAttribute("cartCode", cartModification.getCartCode());
                model.addAttribute("isQuote", cartFacade.getSessionCart().getQuoteData() != null ? Boolean.TRUE : Boolean.FALSE);

                if (cartModification.getQuantityAdded() == 0L) {
                    model.addAttribute(ERROR_MSG_TYPE, "basket.information.quantity.noItemsAdded." + cartModification.getStatusCode());
                } else if (cartModification.getQuantityAdded() < qty) {
                    model.addAttribute(ERROR_MSG_TYPE, "basket.information.quantity.reducedNumberOfItemsAdded." + cartModification.getStatusCode());
                }

                model.addAttribute("cartAnalyticsData", Collections.singletonList(cartAnalyticsConverter.convert(cartModification)));
            } catch (CommerceCartModificationException e) {
                LOG.error(MessageFormat.format("Unable to add the product ''{0}'' to the cart: {1}.", code, e.getMessage()));

                model.addAttribute(ERROR_MSG_TYPE, "basket.error.occurred");
                model.addAttribute(QUANTITY_ATTR, 0L);
            } catch (UnknownIdentifierException e) {
                LOG.error(MessageFormat.format("Unable to add the product ''{0}'' to the cart: {1}.", code, e.getMessage()));

                model.addAttribute(ERROR_MSG_TYPE, "basket.error.occurred");
                model.addAttribute(QUANTITY_ATTR, 0L);

                return ADD_TO_CART_POPUP;
            }
        }

        model.addAttribute("product", productFacade.getProductForCodeAndOptions(code, Collections.singletonList(ProductOption.BASIC)));

        return ADD_TO_CART_POPUP;
    }

    /**
     * Binds the error messages to the view model and returns the view.
     *
     * @param model         the view model
     * @param bindingErrors the binding errors
     * @return the view
     */
    protected String getViewWithBindingErrorMessages(Model model, BindingResult bindingErrors) {
        for (ObjectError error : bindingErrors.getAllErrors()) {
            if (TYPE_MISMATCH_ERROR_CODE.equals(error.getCode())) {
                model.addAttribute(ERROR_MSG_TYPE, QUANTITY_INVALID_BINDING_MESSAGE_KEY);
            } else {
                model.addAttribute(ERROR_MSG_TYPE, error.getDefaultMessage());
            }
        }
        return ADD_TO_CART_POPUP;
    }

    /**
     * Adds multiple products to the cart based on the given {@code entries} and returns the cart modification
     * data afterwards.
     * <p>
     * This is an asynchronous call.
     *
     * @param entries the entries to add
     * @return the cart modification data
     * @throws CommerceCartMergingException in case something goes wrong while merging the cart
     */
    @PostMapping(value = "/cart/addMulti", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    List<CartModificationData> addToCartMulti(@RequestBody List<AddToCartParams> entries) throws CommerceCartMergingException {


        return cartFacade.addToCart(entries);
    }

    /**
     * Adds a product to the cart based on the given {@code form} and returns the add-to-cart popup afterwards.
     *
     * @param form  the add-to-cart form which contains the information about the product to add
     * @param model the view model
     * @return the add-to-cart popup
     */
    @PostMapping(value = "/cart/addGrid", produces = MediaType.APPLICATION_JSON_VALUE)
    public String addGridToCart(@RequestBody AddToCartOrderForm form, Model model) {
        Set<String> errorMessages = new HashSet<>();
        List<CartModificationData> modificationDataList = new ArrayList<>();

        for (OrderEntryData cartEntry : form.getCartEntries()) {
            if (isInvalidProductEntry(cartEntry)) {
                LOG.error("Error processing entry");
            } else if (isInvalidQuantity(cartEntry)) {
                errorMessages.add("basket.error.quantity.invalid");
            } else {
                String errorMessage = addEntryToCart(modificationDataList, cartEntry, true);
                if (StringUtils.isNotEmpty(errorMessage)) {
                    errorMessages.add(errorMessage);
                } else {
                    model.addAttribute("cartAnalyticsData", cartAnalyticsConverter.convertAll(modificationDataList));
                }
            }
        }

        if (CollectionUtils.isNotEmpty(modificationDataList)) {
            groupCartModificationListPopulator.populate(null, modificationDataList);

            model.addAttribute("modifications", modificationDataList);
        }

        if (CollectionUtils.isNotEmpty(errorMessages)) {
            model.addAttribute("errorMessages", errorMessages);
        }

        model.addAttribute("numberShowing", Config.getInt(SHOWN_PRODUCT_COUNT, 3));

        return ADD_TO_CART_POPUP;
    }

    /**
     * Adds the given {@code cartEntry} to the current cart.
     *
     * @param modificationDataList the cart modifications
     * @param cartEntry            the cart entry to add
     * @param isReducedQtyError    a flag to indicate whether a reduced quatity should result in an error
     * @return the error message
     */
    protected String addEntryToCart(List<CartModificationData> modificationDataList, OrderEntryData cartEntry, boolean isReducedQtyError) {
        String errorMsg = StringUtils.EMPTY;
        try {
            long qty = cartEntry.getQuantity();
            CartModificationData cartModificationData = cartFacade.addToCart(cartEntry.getProduct().getCode(), qty);
            if (cartModificationData.getQuantityAdded() == 0L) {
                errorMsg = "basket.information.quantity.noItemsAdded." + cartModificationData.getStatusCode();
            } else if (cartModificationData.getQuantityAdded() < qty && isReducedQtyError) {
                errorMsg = "basket.information.quantity.reducedNumberOfItemsAdded." + cartModificationData.getStatusCode();
            }

            modificationDataList.add(cartModificationData);
        } catch (CommerceCartModificationException e) {
            LOG.error(MessageFormat.format("Unable to add the product ''{0}'' to the cart: {1}.", cartEntry.getProduct().getCode(), e.getMessage()));

            errorMsg = "basket.error.occurred";
        }
        return errorMsg;
    }

    /**
     * Adds a product to the cart based on the given {@code form} and returns the add-to-cart popup afterwards.
     * <p>
     * This method is specifically used by the quick-order functionality.
     *
     * @param form  the add-to-cart form which contains the information about the product to add
     * @param model the view model
     * @return the add-to-cart popup
     */
    @PostMapping(value = "/cart/addQuickOrder", produces = MediaType.APPLICATION_JSON_VALUE)
    public String addQuickOrderToCart(@RequestBody AddToCartOrderForm form, Model model) {
        List<CartModificationData> modificationDataList = new ArrayList<>();
        List<ProductWrapperData> productWrapperDataList = new ArrayList<>();
        int maxQuickOrderEntries = Config.getInt("vanmarckestorefront.quick.order.rows.max", 25);
        int sizeOfCartEntries = CollectionUtils.size(form.getCartEntries());
        form.getCartEntries().stream().limit(Math.min(sizeOfCartEntries, maxQuickOrderEntries)).forEach(cartEntry -> {
            String errorMsg;
            String sku = isInvalidProductEntry(cartEntry) ? StringUtils.EMPTY : cartEntry.getProduct().getCode();
            if (StringUtils.isEmpty(sku)) {
                errorMsg = "text.quickOrder.product.code.invalid";
            } else if (isInvalidQuantity(cartEntry)) {
                errorMsg = "text.quickOrder.product.quantity.invalid";
            } else {
                errorMsg = addEntryToCart(modificationDataList, cartEntry, false);
            }

            if (StringUtils.isNotEmpty(errorMsg)) {
                productWrapperDataList.add(createProductWrapperData(sku, errorMsg));
            } else {
                model.addAttribute("cartAnalyticsData", cartAnalyticsConverter.convertAll(modificationDataList));
            }
        });

        if (CollectionUtils.isNotEmpty(productWrapperDataList)) {
            model.addAttribute("quickOrderErrorData", productWrapperDataList);
            model.addAttribute("quickOrderErrorMsg", "basket.quick.order.error");
        }

        if (CollectionUtils.isNotEmpty(modificationDataList)) {
            model.addAttribute("modifications", modificationDataList);
        }

        return ADD_TO_CART_POPUP;
    }

    /**
     * Checks whether the product of the given {@code cartEntry} is invalid.
     *
     * @param cartEntry the cart entry
     * @return {@code true} in case the given {@code cartEntry} is invalid, {@code false} otherwise
     */
    protected boolean isInvalidProductEntry(OrderEntryData cartEntry) {
        return cartEntry.getProduct() == null || StringUtils.isBlank(cartEntry.getProduct().getCode());
    }

    /**
     * Checks whether quantity of the given {@code cartEntry} is invalid.
     *
     * @param cartEntry the cart entry
     * @return {@code true} in case the given {@code cartEntry} is invalid, {@code false} otherwise
     */
    protected boolean isInvalidQuantity(OrderEntryData cartEntry) {
        return cartEntry.getQuantity() == null || cartEntry.getQuantity() < 1L;
    }

    /**
     * Creates a product data wrapper for the given {@code code} and {@code errorMessage}.
     *
     * @param sku          the product code
     * @param errorMessage the error message
     * @return the {@link ProductWrapperData} instance
     */
    protected ProductWrapperData createProductWrapperData(String sku, String errorMessage) {
        ProductData productData = new ProductData();
        productData.setCode(sku);

        ProductWrapperData productWrapperData = new ProductWrapperData();
        productWrapperData.setProductData(productData);
        productWrapperData.setErrorMsg(errorMessage);
        return productWrapperData;
    }

    /**
     * Adds a product to the cart based on the given {@code form} and redirects to the cart page afterwards
     * <p>
     * This method is specifically used by the quick-order functionality.
     *
     * @param model         the view model
     * @param form          the add-to-cart form which contains the information about the product to add
     * @param bindingResult the binding result
     * @return the cart page
     */
    @RequestMapping(value = "/entrygroups/cart/addToEntryGroup", method = {RequestMethod.POST, RequestMethod.GET})
    public String addEntryGroupToCart(Model model, @Valid AddToEntryGroupForm form, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return getViewWithBindingErrorMessages(model, bindingResult);
        }
        long qty = 1;
        try {
            AddToCartParams addToCartParams = new AddToCartParams();
            addToCartParams.setEntryGroupNumbers(new HashSet<>(Collections.singletonList(form.getEntryGroupNumber())));
            addToCartParams.setProductCode(form.getProductCode());
            addToCartParams.setQuantity(qty);
            addToCartParams.setStoreId(null);
            CartModificationData cartModification = cartFacade.addToCart(addToCartParams);
            model.addAttribute(QUANTITY_ATTR, cartModification.getQuantityAdded());
            model.addAttribute("entry", cartModification.getEntry());
            model.addAttribute("cartCode", cartModification.getCartCode());

            if (cartModification.getQuantityAdded() == 0L) {
                model.addAttribute(ERROR_MSG_TYPE, "basket.information.quantity.noItemsAdded." + cartModification.getStatusCode());
            } else if (cartModification.getQuantityAdded() < qty) {
                model.addAttribute(ERROR_MSG_TYPE,
                        "basket.information.quantity.reducedNumberOfItemsAdded." + cartModification.getStatusCode());
            }

            model.addAttribute("cartAnalyticsData", Collections.singletonList(cartAnalyticsConverter.convert(cartModification)));
        } catch (CommerceCartModificationException e) {
            LOG.error(MessageFormat.format("Unable to add the product ''{0}'' to the cart: {1}.", form.getProductCode(), e.getMessage()));

            model.addAttribute(ERROR_MSG_TYPE, "basket.error.occurred");
            model.addAttribute(QUANTITY_ATTR, 0L);
        }

        model.addAttribute("product", productFacade.getProductForCodeAndOptions(form.getProductCode(), Collections.singletonList(ProductOption.BASIC)));

        return REDIRECT_PREFIX + "/cart";
    }
}