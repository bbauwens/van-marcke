<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/checkout/multi" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="gtm" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/shared/analytics/gtm" %>
<spring:htmlEscape defaultHtmlEscape="true"/>
<spring:url value="/checkout/multi/summary/placeOrder" var="placeOrderUrl"/>
<spring:url value="/checkout/multi/termsAndConditions" var="getTermsAndConditionsUrl" htmlEscape="false"/>
<template:pageCheckout pageTitle="${pageTitle}" hideHeaderLinks="true">
    <div class="row">
        <div class="col-sm-6">
            <div class="checkout-headline">
                <span class="glyphicon glyphicon-lock"></span>
                <spring:theme code="checkout.multi.secure.checkout"/>
            </div>
            <multi-checkout:checkoutSteps checkoutSteps="${checkoutSteps}" progressBarId="${progressBarId}">
                <ycommerce:testId code="checkoutStepFour">
                    <div class="checkout-review hidden-xs">
                        <form:form action="${placeOrderUrl}" id="placeOrderForm1" modelAttribute="placeOrderForm"
                                   cssClass="place-order-form">
                            <div class="checkbox">
                                <label> <form:checkbox id="Terms1" path="termsCheck"/>
                                    <spring:theme code="checkout.summary.placeOrder.readTermsAndConditions"
                                                  arguments="${getTermsAndConditionsUrl}" text="Terms and Conditions"
                                                  htmlEscape="false"/>
                                </label>
                            </div>
                            <div class="text-right">
                                <button id="placeOrder" type="submit"
                                        class="btn btn-default btn-place-order checkoutSummaryButton"
                                        disabled="disabled">
                                    <spring:theme code="checkout.summary.placeOrder" text="Place Order"/>
                                </button>
                            </div>
                        </form:form>
                    </div>
                </ycommerce:testId>
            </multi-checkout:checkoutSteps>
        </div>

        <div class="col-sm-6">
            <multi-checkout:checkoutOrderSummary cartData="${cartData}" showDeliveryAddress="true"
                                                 showPaymentInfo="true" showTaxEstimate="true" showTax="true"/>
        </div>
    </div>
    <gtm:checkoutSteps step="4" cartData="${cartData}"/>
</template:pageCheckout>