<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="cartData" required="true" type="de.hybris.platform.commercefacades.order.data.CartData" %>
<%@ attribute name="showDeliveryAddress" required="true" type="java.lang.Boolean" %>
<%@ attribute name="showPaymentInfo" required="false" type="java.lang.Boolean" %>
<%@ attribute name="showTax" required="false" type="java.lang.Boolean" %>
<%@ attribute name="showTaxEstimate" required="false" type="java.lang.Boolean" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/checkout/multi" %>
<%@ taglib prefix="order" tagdir="/WEB-INF/tags/responsive/order" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common" %>

<spring:htmlEscape defaultHtmlEscape="true"/>
<div class="checkout-order-summary">
    <multi-checkout:paymentInfo cartData="${cartData}" paymentInfo="${cartData.paymentInfo}"
                                showPaymentInfo="${showPaymentInfo}"/>
    <multi-checkout:deliveryAddress cartData="${cartData}" showDeliveryAddress="${showDeliveryAddress}"/>
    <multi-checkout:pickupAddress cartData="${cartData}" showDeliveryAddress="${showDeliveryAddress}"/>
    <order:appliedVouchers order="${cartData}"/>
    <multi-checkout:orderTotals cartData="${cartData}" showTaxEstimate="${showTaxEstimate}" showTax="${showTax}"/>
    <multi-checkout:checkoutProductList cartData="${cartData}"/>
    <common:globalMessagesTemplates/>
</div>