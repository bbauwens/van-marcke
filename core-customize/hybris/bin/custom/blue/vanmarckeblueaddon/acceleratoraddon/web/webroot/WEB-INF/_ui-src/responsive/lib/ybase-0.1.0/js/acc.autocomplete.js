ACC.autocomplete = {

	_autoload: [
		"bindSearchAutocomplete",
        "bindDisableSearch",
		"bindFormSubmit"
	],

	searchProductResults : [],

	bindSearchAutocomplete: function ()
	{
		// extend the default autocomplete widget, to solve issue on multiple instances of the searchbox component
		$.widget( "custom.yautocomplete", $.ui.autocomplete, {
			_create:function(){
				
				// get instance specific options form the html data attr
				var option = this.element.data("options");
				// set the options to the widget
				this._setOptions({
					minLength: option.minCharactersBeforeRequest,
					displayProductImages: option.displayProductImages,
					delay: option.waitTimeBeforeRequest,
					autocompleteUrl: option.autocompleteUrl,
					source: this.source
				});
				
				// call the _super()
				$.ui.autocomplete.prototype._create.call(this);
				this.widget().menu("option", "items", "> :not(.ignore)").menu("option", "menus", "ul:not(.ignore)");
			},
			options:{
				cache:{}, // init cache per instance
				focus: function (){return false;}, // prevent textfield value replacement on item focus
				select: function (event, ui){
					ui.item.value = ACC.sanitizer.sanitize(ui.item.value, false);
                    window.location.href = ui.item.url;
                }
			},
			_renderItem : function (ul, item){

				if(item.type === "autoSuggestion") {
					var renderHtml = "<a href='" + item.url + "' class='vmb-gtm-search' data-term='" + item.value +"' href='" + item.url + "' ><div class='name'>" + item.value + "</div></a>";
					return $("<li class='auto-suggestion'>")
						.data("item.autocomplete", item)
						.append(renderHtml)
						.appendTo(ul.find(".search-data-autoSuggestion"));
				} else if(item.type === "productResult") {

					var renderHtml = "<a class='vmb-gtm-search' data-term='" + item.value +"' href='" + item.url + "' >";

					if(item.image != null) {
						renderHtml += "<div class='thumb'><img src='" + item.image + "' onerror='if (this.src != VMB.noImageUrl){ this.src = VMB.noImageUrl; this.parentNode.href=VMB.noImageUrl;}'  /></div>";
					}

					if(item.price === undefined || item.price == null) {
						item.price = "";
					}

					renderHtml += "<div class='name'>" + item.value  + "</div>";
					renderHtml += "<div class='price'>" + item.price + "</div>";
					renderHtml += "</a>";

					return $("<li class='product-result vmb-gtm-product-data' data-list='Search result' data-position='"+ item.position + "'  data-category='"+ item.category + "' data-brand='"+ item.brand + "' data-code='"+ item.code + "' data-name='"+ item.name + "' data-price='"+ item.priceValue + "'>").data("item.autocomplete", item).append(renderHtml).appendTo(ul.find(".search-data-productResult"));
				} else if(item.type === "categoryResult") {

					var renderHtml = "<a class='vmb-gtm-search' data-term='" + item.value +"' href='" + item.url + "' >";

					renderHtml += "<div class='name'>" + item.value + "</div>";
					renderHtml += "</a>";

					return $("<li class='category-result'>").data("item.autocomplete", item).append(renderHtml).appendTo(ul.find(".search-data-categoryResult"));
				}
			},
			_renderMenu: function(ul, items) {
				var that        = this,
					currentType = "";
				ul.append("<li class='left ignore'></li>");

				if(_.find(items, {type: "productResult"}) !== undefined){
					ul.append("<li class='right ignore'></li>");
				}

				var hasSearchProducts = false;
				$.each(items, function(index, item) {
					var li;
					if(item.type !== currentType) {
						var container = ".left";

						if(item.type === 'productResult') {
							container = ".right";
							hasSearchProducts = true;
						}

						if(['productResult','autoSuggestion','categoryResult'].indexOf(item.type) !== -1){
							var itemElms = [
								"<div class='ignore " + item.type + "'>",
								"<span class='search-item-title vmb-autocomplete-type' aria-label='" + VMB.i18n.search[item.type] + "'>" + VMB.i18n.search[item.type] + "</span>",
								"<ol class='ignore search-data-" + item.type + "'></ol>",
								"</div>"
							];
						}

						ul.children(container).append(itemElms.join("\n"));
						currentType = item.type;
					}

					li = that._renderItemData(ul, item);

				});

				if(hasSearchProducts) {
					ul.children('.right').find(".search-data-productResult").append("<li class='vmb-search-showmore'>" + VMB.i18n.search.more + "</li>");

					$(document).on("click", ".vmb-search-showmore", function() {
						$("form[name='search_form_SearchBox']").submit();
					});
				}
			},
			source: function (request, response)
			{
				var self=this;
				var term = request.term.toLowerCase();
				if (term in self.options.cache)
				{
					var cachedProducts = _.filter(self.options.cache[term], {"type": "productResult"});

					for(var i = 0; i <cachedProducts.length; i++){
						ACC.autocomplete.searchProductResults.push({
							'id' : cachedProducts[i].code,
							'name': cachedProducts[i].name,
							'price': cachedProducts[i].price,
							'brand': cachedProducts[i].brand,
							'category': cachedProducts[i].category,
							'position': cachedProducts[i].position
						});
					}

					return response(self.options.cache[term]);
				}

				$.getJSON(self.options.autocompleteUrl, {term: request.term}, function (data)
				{
					var autoSearchData = [];
					var gtmProducts = [];
					ACC.autocomplete.searchProductResults = [];

					if(data.suggestions != null){
						$.each(data.suggestions, function (i, obj)
						{
							autoSearchData.push({
								value: obj.term,
								url: ACC.config.encodedContextPath + "/search?text=" + encodeURIComponent(obj.term),
								type: "autoSuggestion"
							});
						});
					}

					if(data.categories != null){
						$.each(data.categories, function (i, obj)
						{
							autoSearchData.push({
								value: obj.name,
								url: ACC.config.encodedContextPath +  obj.url,
								type: "categoryResult"
							});
						});
					}

					if(data.products != null){
						$.each(data.products, function (i, obj)
						{
							autoSearchData.push({
								value: ACC.sanitizer.sanitize(obj.name),
								code: obj.code,
								desc: ACC.sanitizer.sanitize(obj.description),
								url:  ACC.config.encodedContextPath + obj.url,
								price: obj.price ? obj.price.formattedValue : '',
								priceValue: obj.price ? obj.price.value : '',
								brand: obj.analyticsData.hasOwnProperty('brand') ? obj.analyticsData.brand : '',
								category: obj.analyticsData.hasOwnProperty('category') ? obj.analyticsData.category : '',
								position: i,
								type: "productResult",
								image: (obj.images!=null && self.options.displayProductImages) ? obj.images[0].url : VMB.noImageUrl // prevent errors if obj.images = null
							});

							ACC.autocomplete.searchProductResults.push({
								'id' : obj.code,
								'name': ACC.sanitizer.sanitize(obj.name),
								'price': obj.price ? obj.price.value : '',
								'brand': obj.analyticsData.hasOwnProperty('brand') ? obj.analyticsData.brand : '',
								'category': obj.analyticsData.hasOwnProperty('category') ? obj.analyticsData.category : '',
								'position': i
							});
						});
					}
					self.options.cache[term] = autoSearchData;

					return response(autoSearchData);
				});
			}

		});

	
		$search = $(".js-site-search-input");
		if($search.length>0){
			$search.yautocomplete();

			enquire.register("screen and (max-width:" + screenSmMax + ")", {

				match: function() {
					$search.yautocomplete("disable");
				},

				unmatch: function() {
					$search.yautocomplete("enable");
				}

			});
		}

	},

	bindDisableSearch: function ()
    {
        $('#js-site-search-input').keyup(function(e){

			// get keycode of current keypress event
			var code = (e.keyCode || e.which);

			// do nothing if it's an arrow key
			if(code == 37 || code == 38 || code == 39 || code == 40) {
				return;
			}

			$('#js-site-search-input').val($('#js-site-search-input').val().replace(/^\s+/gm,''));
            $('.js_search_button').prop('disabled', this.value == "" ? true : false);
        })
    },

	bindFormSubmit: function () {
		$("form[name='search_form_SearchBox']").submit(function () {
			ACC.track.searchResult(ACC.autocomplete.searchProductResults, $("#js-site-search-input").val());
		});
	}
};