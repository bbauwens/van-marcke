package com.vanmarcke.blue.addon.interceptors;

import com.google.gson.Gson;
import com.vanmarcke.facades.consent.VMKConsentFacade;
import com.vanmarcke.facades.storesession.VMKStoreSessionFacade;
import de.hybris.platform.acceleratorstorefrontcommons.interceptors.BeforeViewHandler;
import de.hybris.platform.cms2.misc.CMSFilter;
import de.hybris.platform.servicelayer.session.SessionService;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * BeforeView Handler that will handle general page actions
 *
 * @author Niels Raemaekers
 * @since 27-8-2019
 */
public class BlueBeforeViewHandler implements BeforeViewHandler {

    private VMKStoreSessionFacade blueStoreSessionFacade;
    private SessionService sessionService;
    private VMKConsentFacade consentFacade;

    /**
     * Provides an instance of the {@code BlueBeforeViewHandler}.
     *
     * @param blueStoreSessionFacade the store session facade
     * @param sessionService         the session service
     * @param consentFacade          the consent facade
     */
    public BlueBeforeViewHandler(VMKStoreSessionFacade blueStoreSessionFacade,
                                 SessionService sessionService,
                                 VMKConsentFacade consentFacade) {
        this.blueStoreSessionFacade = blueStoreSessionFacade;
        this.sessionService = sessionService;
        this.consentFacade = consentFacade;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void beforeView(HttpServletRequest request, HttpServletResponse response, ModelAndView modelAndView) {
        modelAndView.addObject("allSites", blueStoreSessionFacade.getAllSites());
        modelAndView.addObject("currentSite", blueStoreSessionFacade.getCurrentSite());
        modelAndView.addObject("disableSmartEdit", sessionService.getAttribute(CMSFilter.PREVIEW_TICKET_ID_PARAM) == null);
        modelAndView.addObject("favoriteStore", blueStoreSessionFacade.getCurrentStore());
        modelAndView.addObject("consentTypes", new Gson().toJson(consentFacade.getConsentTypes()));
    }
}
