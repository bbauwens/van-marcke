package com.vanmarcke.blue.addon.form;

/**
 * Form to add products to a wish list.
 */
public class AddToWishListForm {

    private String[] productCode;
    private int[] productQuantity;

    private String wishListAddTo;
    private String newWishlistName;
    private String newWishlistDescription;
    private boolean addToWishlist;

    public void setProductCode(final String[] productCode) {
        this.productCode = productCode;
    }

    public String[] getProductCode() {
        return productCode;
    }

    public int[] getProductQuantity() {
        return productQuantity;
    }

    public void setProductQuantity(final int[] productQuantity) {
        this.productQuantity = productQuantity;
    }

    public String getWishListAddTo() {
        return wishListAddTo;
    }

    public void setWishListAddTo(final String wishListAddTo) {
        this.wishListAddTo = wishListAddTo;
    }

    public String getNewWishlistName() {
        return newWishlistName;
    }

    public void setNewWishlistName(final String newWishlistName) {
        this.newWishlistName = newWishlistName;
    }

    public String getNewWishlistDescription() {
        return newWishlistDescription;
    }

    public void setNewWishlistDescription(final String newWishlistDescription) {
        this.newWishlistDescription = newWishlistDescription;
    }

    public boolean isCreateNew() {
        return addToWishlist;
    }

    public boolean isAddToWishlist() {
        return addToWishlist;
    }

    public void setAddToWishlist(final boolean addToWishlist) {
        this.addToWishlist = addToWishlist;
    }
}
