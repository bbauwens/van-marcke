<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="cartData" required="true" type="de.hybris.platform.commercefacades.order.data.CartData" %>
<%@ attribute name="showPotentialPromotions" required="false" type="java.lang.Boolean" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/shared/format" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/product" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<h4 class="checkout-summary-products"><spring:theme code="checkout.summary.products"/></h4>

<ul class="checkout-order-summary-list">
    <c:forEach items="${cartData.entries}" var="entry" varStatus="loop">
            <c:url value="${entry.product.url}" var="productUrl"/>
            <li class="checkout-order-summary-list-items vmb-gtm-product-data"
                data-code="${fn:escapeXml(entry.product.code)}"
                data-name="${fn:escapeXml(entry.product.name)}"
                data-category="${fn:escapeXml(entry.product.analyticsData.category)}"
                data-brand="${fn:escapeXml(entry.product.analyticsData.brand)}"
                data-url="${entry.product.url}"
                data-list="Checkout products"
                data-position="${loop.index + 1}"
                data-price="${entry.basePrice.value}"
            >
                <div class="thumb">
                    <a href="${productUrl}" class="vmb-gtm-product-item">
                        <product:productPrimaryImage product="${entry.product}" format="thumbnail"/>
                    </a>
                </div>
                <div class="price"><format:price priceData="${entry.totalPrice}" displayFreeForZero="true"/></div>
                <div class="details">
                    <div class="name">
                        <a href="${productUrl}" class="vmb-gtm-product-item">${fn:escapeXml(entry.product.name)}</a>
                        <div class="vmb-warning-sign">
                            <span class="glyphicon glyphicon-warning-sign"></span>
                        </div>
                    </div>
                    <div class="sku"><spring:theme
                            code="order.entry.article.number"/>: ${entry.product.code}</div>
                    <div>
                        <span class="label-spacing"><spring:theme code="order.itemPrice"/>:</span>
                        <c:if test="${entry.product.multidimensional}">
                            <%-- if product is multidimensional with different prices, show range, else, show unique price --%>
                            <c:choose>
                                <c:when test="${entry.product.priceRange.minPrice.value ne entry.product.priceRange.maxPrice.value}">
                                    <format:price priceData="${entry.product.priceRange.minPrice}"/> - <format:price
                                        priceData="${entry.product.priceRange.maxPrice}"/>
                                </c:when>
                                <c:when test="${entry.product.priceRange.minPrice.value eq entry.product.priceRange.maxPrice.value}">
                                    <format:price priceData="${entry.product.priceRange.minPrice}"/>
                                </c:when>
                                <c:otherwise>
                                    <format:price priceData="${entry.product.price}"/>
                                </c:otherwise>
                            </c:choose>
                        </c:if>
                        <c:if test="${! entry.product.multidimensional}">
                            <format:price priceData="${entry.basePrice}" displayFreeForZero="true"/>
                        </c:if>
                    </div>
                    <div class="qty"><span><spring:theme code="basket.page.qty"/>:</span>${entry.quantity}</div>
                    <div>
                        <c:forEach items="${entry.product.baseOptions}" var="option">
                            <c:if test="${not empty option.selected and option.selected.url eq entry.product.url}">
                                <c:forEach items="${option.selected.variantOptionQualifiers}" var="selectedOption">
                                    <div>${fn:escapeXml(selectedOption.name)}: ${fn:escapeXml(selectedOption.value)}</div>
                                </c:forEach>
                            </c:if>
                        </c:forEach>
                        <c:if test="${ycommerce:doesPotentialPromotionExistForOrderEntryOrOrderEntryGroup(cartData, entry) && showPotentialPromotions}">
                            <c:forEach items="${cartData.potentialProductPromotions}" var="promotion">
                                <c:set var="displayed" value="false"/>
                                <c:forEach items="${promotion.consumedEntries}" var="consumedEntry">
                                    <c:if test="${not displayed && ycommerce:isConsumedByEntry(consumedEntry,entry)}">
                                        <c:set var="displayed" value="true"/>
                                        <span class="promotion">${ycommerce:sanitizeHTML(promotion.description)}</span>
                                    </c:if>
                                </c:forEach>
                            </c:forEach>
                        </c:if>
                        <c:if test="${ycommerce:doesAppliedPromotionExistForOrderEntryOrOrderEntryGroup(cartData, entry)}">
                            <c:forEach items="${cartData.appliedProductPromotions}" var="promotion">
                                <c:set var="displayed" value="false"/>
                                <c:forEach items="${promotion.consumedEntries}" var="consumedEntry">
                                    <c:if test="${not displayed && ycommerce:isConsumedByEntry(consumedEntry,entry)}">
                                        <c:set var="displayed" value="true"/>
                                        <span class="promotion">${ycommerce:sanitizeHTML(promotion.description)}</span>
                                    </c:if>
                                </c:forEach>
                            </c:forEach>
                        </c:if>
                    </div>
                    <c:if test="${entry.product.multidimensional}">
                        <a href="#" id="QuantityProductToggle" data-index="${loop.index}"
                           class="showQuantityProductOverlay updateQuantityProduct-toggle">
                            <span><spring:theme code="order.product.seeDetails"/></span>
                        </a>
                    </c:if>
                    <div class="productEntryDate">
                        <c:forEach items="${checkoutSteps}" var="checkoutStep" varStatus="status">
                            <c:choose>
                                <c:when test="${progressBarId eq checkoutStep.progressBarId}">
                                    <c:set scope="page" var="activeCheckoutStepNumber" value="${checkoutStep.stepNumber}"/>
                                </c:when>
                            </c:choose>
                        </c:forEach>
                        <div class="vmb-expected-delivery-date">
                            <c:choose>
                                <c:when test="${activeCheckoutStepNumber eq 1}">
                                    <span class="expectedPickupDateLabel" <c:if test="${empty cartData.deliveryMode.code || fn:endsWith(cartData.deliveryMode.code, '-standard')}">hidden</c:if>><spring:theme code="order.split.expected.pickup.date"/>:&nbsp;</span>
                                    <span class="expectedDeliveryDateLabel" <c:if test="${empty cartData.deliveryMode.code || fn:endsWith(cartData.deliveryMode.code, '-tec')}">hidden</c:if>><spring:theme code="order.split.expected.delivery.date"/>:&nbsp;</span>
                                    <select name="productDateSelecter" class="form-control vmb-product-dates"
                                            data-product-code="${entry.product.code}"
                                            data-delivery-date="<fmt:formatDate value="${entry.deliveryPickupDate}" pattern="dd-MM-yyyy"/>"
                                            data-first-date="<fmt:formatDate value="${entry.firstPossibleDate}" pattern="yyyy-MM-dd"/>"
                                            <c:if test="${not empty lastPossibleDate}">
                                                data-last-date="<fmt:formatDate value="${lastPossibleDate}" pattern="yyyy-MM-dd"/>"
                                            </c:if>
                                            <c:if test="${empty lastPossibleDate || entry.firstPossibleDate == lastPossibleDate}">
                                                disabled
                                            </c:if>>
                                        <option value="<fmt:formatDate value="${entry.firstPossibleDate}" pattern="yyyy-MM-dd"/>" selected="selected">
                                            <fmt:formatDate value="${entry.firstPossibleDate}" pattern="dd-MM-yyyy"/>
                                        </option>
                                        <c:if test="${not empty lastPossibleDate && entry.firstPossibleDate != lastPossibleDate}">
                                            <option value="<fmt:formatDate value="${lastPossibleDate}" pattern="yyyy-MM-dd"/>">
                                                <fmt:formatDate value="${lastPossibleDate}" pattern="dd-MM-yyyy"/>
                                            </option>
                                        </c:if>
                                    </select>
                                </c:when>
                                <c:otherwise>
                                    <div class="selectedDate">
                                        <span class="expectedPickupDateLabel" <c:if test="${empty cartData.deliveryMode.code || fn:endsWith(cartData.deliveryMode.code, '-standard')}">hidden</c:if>><spring:theme code="order.split.expected.pickup.date"/>:&nbsp;</span>
                                        <span class="expectedDeliveryDateLabel" <c:if test="${empty cartData.deliveryMode.code || fn:endsWith(cartData.deliveryMode.code, '-tec')}">hidden</c:if>><spring:theme code="order.split.expected.delivery.date"/>:&nbsp;</span>
                                        <fmt:formatDate value="${entry.deliveryPickupDate}" pattern="dd-MM-yyyy"/>
                                    </div>
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                </div>
            </li>
    </c:forEach>
</ul>
