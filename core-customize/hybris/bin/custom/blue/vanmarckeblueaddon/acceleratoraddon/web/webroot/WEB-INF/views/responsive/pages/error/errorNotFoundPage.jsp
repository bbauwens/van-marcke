<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/template" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<spring:htmlEscape defaultHtmlEscape="false"/>
<template:page pageTitle="${pageTitle}">
    <cms:pageSlot position="MiddleContent" var="comp">
        <cms:component component="${comp}" element="div" class="errorNotFoundPageMiddle"/>
    </cms:pageSlot>
    <div class="error-page">
        <img src="/_ui/responsive/theme-vanmarckeblue/images/404.png" alt="404">
        <h3><spring:theme code="error.404.title"/></h3>
        <p><spring:theme code="error.404.contactus"/></p>
    </div>
    <cms:pageSlot position="BottomContent" var="comp">
        <cms:component component="${comp}" element="div" class="errorNotFoundPageBottom"/>
    </cms:pageSlot>
</template:page>