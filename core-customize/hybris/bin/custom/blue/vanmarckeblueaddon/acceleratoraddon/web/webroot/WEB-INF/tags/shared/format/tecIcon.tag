<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<span class="icon icon-tec" data-toggle="tooltip" data-placement="top"
      title="<spring:theme code="text.stockstatus.icon.tec" htmlEscape="false"/>"></span>