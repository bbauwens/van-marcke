<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="order" required="true" type="de.hybris.platform.commercefacades.order.data.OrderData" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="action" tagdir="/WEB-INF/tags/responsive/action" %>

<spring:htmlEscape defaultHtmlEscape="true"/>
<div class="row">
    <div class="col-sm-12 col-md-9 col-no-padding">
        <div class="row">
            <div class="col-sm-4 item-wrapper">
                <c:if test="${orderData.externalCode ne null}">
                    <div class="item-group">
                        <ycommerce:testId code="orderDetail_overviewOrderID_label">
                            <c:choose>
                                <c:when test="${orderData.isSplitted}">
                                    <span class="item-label"><spring:theme code="text.account.orderHistory.orderNumbers"/></span>
                                    <c:forEach var="ibmOrderData" items="${orderData.ibmSubOrders}">
                                        <span class="item-value">${fn:escapeXml(ibmOrderData.code)}</span>
                                    </c:forEach>
                                </c:when>
                                <c:otherwise>
                                    <span class="item-label"><spring:theme code="text.account.orderHistory.orderNumber"/></span>
                                    <span class="item-value">${fn:escapeXml(orderData.externalCode)}</span>
                                </c:otherwise>
                            </c:choose>
                        </ycommerce:testId>
                    </div>
                </c:if>
                <div class="item-group">
                    <ycommerce:testId code="orderDetail_overviewOrderStatus_label">
                        <span class="item-label"><spring:theme code="text.account.orderHistory.orderStatus"/></span>
                        <c:if test="${not empty orderData.statusDisplay}">
                            <span class="item-value"><spring:theme code="text.account.order.status.display.${orderData.statusDisplay}"/></span>
                        </c:if>
                    </ycommerce:testId>
                </div>
            </div>
            <div class="col-sm-4 item-wrapper">
                <div class="item-group">
                    <ycommerce:testId code="orderDetail_overviewStatusDate_label">
                        <span class="item-label"><spring:theme code="text.account.orderHistory.datePlaced"/></span>
                        <span class="item-value"><fmt:formatDate value="${order.created}" dateStyle="medium" timeStyle="short" type="both"/></span>
                    </ycommerce:testId>
                </div>
                <div class="item-group">
                    <ycommerce:testId code="orderDetail_overviewPlacedBy_label">
                        <span class="item-label"><spring:theme code="checkout.multi.summary.orderPlacedBy"/></span>
                        <span class="item-value">${fn:escapeXml(order.b2bCustomerData.firstName)}&nbsp;${fn:escapeXml(order.b2bCustomerData.lastName)}</span>
                    </ycommerce:testId>
                </div>
            </div>
            <div class="col-sm-4 item-wrapper">
                <div class="item-group">
                    <ycommerce:testId code="orderDetail_overviewOrderTotal_label">
                        <span class="item-label"><spring:theme code="text.account.order.total"/></span>
                        <span class="item-value"><format:price priceData="${order.totalPriceWithTax}"/></span>
                    </ycommerce:testId>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-12 col-md-3 item-action text-right">
        <c:set var="orderCode" value="${orderData.code}" scope="request"/>
        <action:actions element="div" parentComponent="${component}"/>
        <button class="btn btn-default vmb-checkout-confirmForm-open">
            <spring:theme code="checkout.multi.contactForm.button"/>
        </button>
        <br>
        <div class="btn-lg">
            <c:set var="exportToolTip"><spring:theme code="basket.export.csv.file"/></c:set>
            <a href="#"
               class="js-export-basket restore-item-link"
               data-basket-name="${fn:escapeXml(order.guid)}"
               data-basket-type="ORDER">
            <span class="glyphicon glyphicon-export" data-toggle="tooltip"
                  data-original-title="${exportToolTip}"></span>
            </a>
        </div>
    </div>
</div>

