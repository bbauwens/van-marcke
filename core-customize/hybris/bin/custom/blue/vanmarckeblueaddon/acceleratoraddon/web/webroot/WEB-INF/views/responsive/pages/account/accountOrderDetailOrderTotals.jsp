<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="order" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/order" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/formElement" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<order:checkoutOrderDetailOrderTotals order="${orderData}"/>

<div class="vmb-modal fade" id="vmbCheckoutConfirmFormModal" tabindex="-1" role="dialog" aria-labelledby="vmbCheckoutConfirmFormTitle">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="vmbCheckoutConfirmFormTitle"><spring:theme code="checkout.multi.contactForm.title"/></h4>
            </div>
            <div class="modal-body">
                <spring:url value="/checkout/contactOrderManager/{/orderCode}" var="actionUrl" htmlEscape="false">
                    <spring:param name="orderCode" value="${orderData.code}"/>
                </spring:url>

                <ycommerce:testId code="orderDetail_contact_form_section">
                    <form:form id="contactOrderManagerForm" modelAttribute="contactOrderManagerForm" action="${actionUrl}" method="post">
                        <formElement:formRadioBoxLeft idKey="type_general"
                                                      labelKey="checkout.multi.contactForm.type.general"
                                                      value="ORDER_MANAGER" path="type"/>
                        <formElement:formRadioBoxLeft idKey="type_invoice"
                                                      labelKey="checkout.multi.contactForm.type.invoice"
                                                      value="CREDIT_CONTROLLER"
                                                      path="type"/>
                        <formElement:formTextArea idKey="contactComment"
                                                  labelKey="text.account.order.delivery.comment" path="contactComment"
                                                  mandatory="true"/>
                    </form:form>
                </ycommerce:testId>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal"><spring:theme code="text.button.cancel"/></button>
                <button class="btn btn-default vmb-checkout-confirmForm-button">
                    <spring:theme code="checkout.multi.contactForm.send"/>
                </button>
            </div>
        </div>
    </div>
</div>
<div class="hidden" hidden>
    <span id="contactOrderManagerFormError"><spring:theme code="checkout.multi.contactForm.error"/></span>
    <span id="contactOrderManagerFormSuccess"><spring:theme code="checkout.multi.contactForm.success"/></span>
</div>