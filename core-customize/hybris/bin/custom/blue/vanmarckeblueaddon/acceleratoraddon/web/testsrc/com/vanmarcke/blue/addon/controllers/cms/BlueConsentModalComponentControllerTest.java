package com.vanmarcke.blue.addon.controllers.cms;

import com.vanmarcke.core.model.ConsentModalComponentModel;
import de.hybris.bootstrap.annotations.UnitTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.ui.Model;

import javax.servlet.http.HttpServletRequest;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class BlueConsentModalComponentControllerTest {

    @InjectMocks
    private BlueConsentModalComponentController controller;

    @Test
    public void testFillModel() {
        HttpServletRequest request = new MockHttpServletRequest();
        Model model = mock(Model.class);
        ConsentModalComponentModel component = mock(ConsentModalComponentModel.class);

        controller.fillModel(request, model, component);

        verify(component).getConsents();
        verify(model).addAttribute("consentParagraphs", component.getConsents());
    }
}