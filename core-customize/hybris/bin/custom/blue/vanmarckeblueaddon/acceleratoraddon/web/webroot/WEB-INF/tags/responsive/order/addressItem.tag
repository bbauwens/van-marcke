<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="address" required="true" type="de.hybris.platform.commercefacades.user.data.AddressData" %>
<%@ attribute name="storeAddress" required="false" type="java.lang.Boolean" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:if test="${not empty address.firstName }">
    ${fn:escapeXml(address.firstName)}&nbsp;
</c:if>
<c:if test="${ not empty address.lastName }">
    ${fn:escapeXml(address.lastName)}
</c:if>
<c:if test="${not empty address.firstName or not empty address.lastName}">
    <br/>
</c:if>
<c:if test="${ not empty address.line1 }">
    ${fn:escapeXml(address.line1)}&nbsp;
</c:if>
<c:if test="${ not empty address.line2 }">
    ${fn:escapeXml(address.line2)}&nbsp;
</c:if>
<c:if test="${ not empty address.apartment }">
    ${fn:escapeXml(address.apartment)}
</c:if>
<br/>
<c:if test="${ not empty address.postalCode }">
    ${fn:escapeXml(address.postalCode)}&nbsp;
</c:if>
<c:if test="${not empty address.town }">
    ${fn:escapeXml(address.town)}&nbsp;
</c:if>
<c:if test="${ not empty address.country.isocode }">
    (${fn:escapeXml(address.country.isocode)})
</c:if>
<c:if test="${not empty address.phone}">
    <br/>
    ${fn:escapeXml(address.phone)}
</c:if>
<c:if test="${not empty address.mobile}">
    <br/>
    ${fn:escapeXml(address.mobile)}
</c:if>