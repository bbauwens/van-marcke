package com.vanmarcke.blue.addon.controllers.pages;

import com.vanmarcke.facades.cart.VMKSaveCartFacade;
import com.vanmarcke.services.order.cart.conversion.impl.VMKCsvExcelConversionServiceImpl;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorfacades.csv.CsvFacade;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.CommerceSaveCartParameterData;
import de.hybris.platform.commercefacades.order.data.CommerceSaveCartResultData;
import de.hybris.platform.commerceservices.order.CommerceSaveCartException;
import de.hybris.platform.servicelayer.i18n.I18NService;
import org.apache.commons.lang3.RandomStringUtils;
import org.fest.assertions.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import javax.ws.rs.core.HttpHeaders;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.List;
import java.util.Locale;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class BlueAccountSavedCartsPageControllerTest {

    private static final String CART_ID = RandomStringUtils.randomAlphabetic(10);
    private static final Locale LOCALE = Locale.ENGLISH;
    private static final String SKU = "basket.export.cart.item.sku";
    private static final String QUANTITY = "basket.export.cart.item.quantity";
    private static final String NAME = "basket.export.cart.item.name";
    private static final String PRICE = "basket.export.cart.item.price";
    private static final String TOTAL = "basket.export.cart.item.total-price";

    private static final String MESSAGE1 = RandomStringUtils.randomAlphabetic(10);
    private static final String MESSAGE2 = RandomStringUtils.randomAlphabetic(10);
    private static final String MESSAGE3 = RandomStringUtils.randomAlphabetic(10);
    private static final String MESSAGE4 = RandomStringUtils.randomAlphabetic(10);
    private static final String MESSAGE5 = RandomStringUtils.randomAlphabetic(10);

    @Captor
    private ArgumentCaptor<List<String>> headersCaptor;

    @Captor
    private ArgumentCaptor<CommerceSaveCartParameterData> cartParameterCaptor;

    @Mock
    private VMKSaveCartFacade saveCartFacade;

    @Mock
    private CsvFacade csvFacade;

    @Mock
    private VMKCsvExcelConversionServiceImpl excelConversionService;

    @Mock
    private I18NService i18NService;

    @Mock
    private MessageSource messageSource;

    @InjectMocks
    private BlueAccountSavedCartsPageController controller;

    @Test
    public void exportExcelFileForCartGuid() throws CommerceSaveCartException, IOException {
        CartData cartData = new CartData();

        CommerceSaveCartResultData commerceSaveCartResultData = new CommerceSaveCartResultData();
        commerceSaveCartResultData.setSavedCartData(cartData);

        byte[] byteArray = new byte[]{};

        when(i18NService.getCurrentLocale()).thenReturn(LOCALE);

        when(messageSource.getMessage(SKU, null, LOCALE)).thenReturn(MESSAGE1);
        when(messageSource.getMessage(QUANTITY, null, LOCALE)).thenReturn(MESSAGE2);
        when(messageSource.getMessage(NAME, null, LOCALE)).thenReturn(MESSAGE3);
        when(messageSource.getMessage(PRICE, null, LOCALE)).thenReturn(MESSAGE4);
        when(messageSource.getMessage(TOTAL, null, LOCALE)).thenReturn(MESSAGE5);

        when(saveCartFacade.getCartForCodeAndCurrentUser(any(CommerceSaveCartParameterData.class))).thenReturn(commerceSaveCartResultData);

        when(excelConversionService.convertCsvToXls(any(StringReader.class))).thenReturn(byteArray);

        ResponseEntity responseEntity = controller.downloadExcel(CART_ID);

        verify(saveCartFacade).getCartForCodeAndCurrentUser(cartParameterCaptor.capture());
        verify(csvFacade).generateCsvFromCart(headersCaptor.capture(), eq(true), eq(cartData), any(StringWriter.class));
        verify(excelConversionService).convertCsvToXls(any(StringReader.class));

        List<String> headers = headersCaptor.getValue();

        Assertions
                .assertThat(headers)
                .containsExactly(MESSAGE1, MESSAGE2, MESSAGE3, MESSAGE4, MESSAGE5);

        CommerceSaveCartParameterData parameter = cartParameterCaptor.getValue();

        Assertions
                .assertThat(parameter.getCartId())
                .isEqualTo(CART_ID);

        Assertions
                .assertThat(responseEntity.getStatusCode())
                .isEqualTo(HttpStatus.CREATED);

        Assertions
                .assertThat(responseEntity.getHeaders().getContentType())
                .isEqualTo(new MediaType("application", "force-download"));

        Assertions
                .assertThat(responseEntity.getHeaders().get(HttpHeaders.CONTENT_DISPOSITION))
                .contains("attachment; filename=cart.xls");
    }


    @Test
    public void exportExcelFileForCartGuidThrowsCommerceCartException() throws CommerceSaveCartException, IOException {
        CartData cartData = new CartData();

        CommerceSaveCartException exception = mock(CommerceSaveCartException.class);

        when(saveCartFacade.getCartForCodeAndCurrentUser(any(CommerceSaveCartParameterData.class))).thenThrow(exception);

        ResponseEntity responseEntity = controller.downloadExcel(CART_ID);

        verify(saveCartFacade).getCartForCodeAndCurrentUser(cartParameterCaptor.capture());
        verify(csvFacade, times(0)).generateCsvFromCart(anyListOf(String.class), eq(true), eq(cartData), any(StringWriter.class));
        verify(excelConversionService, times(0)).convertCsvToXls(any(StringReader.class));

        verify(exception).getMessage();

        Assertions
                .assertThat(responseEntity.getStatusCode())
                .isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
    }
}