/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.vanmarcke.blue.addon.form;

import com.vanmarcke.facades.data.wishlist2.RestoreProductData;

import java.util.List;

/**
 * Form to restore a wish list.
 */
public class RestoreWishListForm
{
    private boolean preventSaveActiveCart;
    private boolean preventMergeActiveCart;
    private boolean keepRestoredCart;
    private String cartName;
    private List<RestoreProductData> wishlistProducts;
    private String newWishListDescription;

    public boolean isKeepRestoredCart() {
        return keepRestoredCart;
    }

    public void setKeepRestoredCart(boolean keepRestoredCart) {
        this.keepRestoredCart = keepRestoredCart;
    }

    public boolean isPreventSaveActiveCart() {
        return preventSaveActiveCart;
    }

    public void setPreventSaveActiveCart(boolean preventSaveActiveCart) {
        this.preventSaveActiveCart = preventSaveActiveCart;
    }

    public String getCartName() {
        return cartName;
    }

    public void setCartName(String cartName) {
        this.cartName = cartName;
    }

    public boolean isPreventMergeActiveCart() {
        return preventMergeActiveCart;
    }

    public void setPreventMergeActiveCart(boolean preventMergeActiveCart) {
        this.preventMergeActiveCart = preventMergeActiveCart;
    }

    public String getNewWishListDescription() {
        return newWishListDescription;
    }

    public void setNewWishListDescription(String newWishListDescription) {
        this.newWishListDescription = newWishListDescription;
    }

    public List<RestoreProductData> getWishlistProducts() {
        return wishlistProducts;
    }

    public void setWishlistProducts(List<RestoreProductData> wishlistProducts) {
        this.wishlistProducts = wishlistProducts;
    }
}
