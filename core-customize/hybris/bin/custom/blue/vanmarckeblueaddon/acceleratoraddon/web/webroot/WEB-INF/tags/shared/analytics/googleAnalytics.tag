<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="gtm" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/shared/analytics/gtm" %>


<script>
    /* Google Analytics */

    var dataLayer = dataLayer || [];
    var pageType = "${pageType}";

    <c:choose>
    <c:when test="${pageType == 'PRODUCT'}">
    <gtm:productDetails/>
    </c:when>

    <c:when test="${pageType == 'CATEGORY'}">
    <gtm:productLister/>
    </c:when>

    <c:when test="${pageType == 'PRODUCTSEARCH'}">
    <c:choose>
    <c:when test="${searchPageData.pagination.totalNumberOfResults > 0}">
    <gtm:productSearchResult/>
    </c:when>
    <c:otherwise>
    <gtm:productNoSearchResult/>
    </c:otherwise>
    </c:choose>
    </c:when>

    <c:when test="${pageType == 'ORDERCONFIRMATION'}">
    <gtm:checkoutConfirm/>
    </c:when>

    <c:when test="${pageType == 'CART'}">
    <gtm:cartPage/>
    </c:when>
    </c:choose>
</script>

<gtm:addToCart/>
<gtm:cartRemove/>
<gtm:productClick/>
<gtm:productSearch/>
<gtm:checkout/>