ACC.storefinder = {

	_autoload: [
		["init", $(".js-store-finder").length != 0],
		["bindStoreChange", $(".js-store-finder").length != 0],
		["bindSearch", $(".js-store-finder").length != 0],
		"bindPagination"
	],
	
	storeData:"",
	storeId:"",
	coords:{},
	storeSearchData:{},

	createListItemHtml: function (data,id){

		var item="";
		item+='<li class="list__entry">';
		item+='<input type="radio" name="storeNamePost" value="'+data.displayName+'" id="store-filder-entry-'+id+'" class="js-store-finder-input" data-id="'+id+'">';
		item+='<label for="store-filder-entry-'+id+'" class="js-select-store-label">';
		item+='<span class="entry__info">';
		item+='<span class="entry__name">'+data.displayName+'</span>';
		item+='<span class="entry__address">'+data.line1+' '+data.line2+'</span>';
		item+='<span class="entry__city">'+data.town+'</span>';
		item+='</span>';
		if(data.hasOwnProperty('formattedDistance') && data.formattedDistance) {
			item += '<span class="entry__distance">';
			item += '<span>' + data.formattedDistance + '</span>';
			item += '</span>';
		}
		item+='</label>';
		item+='</li>';
		return item;
	},

	refreshNavigation: function (){
		var listitems = "";
		data = ACC.storefinder.storeData
		
		if(data){
			for(i = 0;i < data["data"].length;i++){
				listitems += ACC.storefinder.createListItemHtml(data["data"][i],i)
			}
	
			$(".js-store-finder-navigation-list").html(listitems);
	
			// select the first store
			var firstInput= $(".js-store-finder-input")[0];
			$(firstInput).click();

			if(data.total < 11){
				$(".js-store-finder-pager-prev").addClass("hidden");
				$(".js-store-finder-pager-next").addClass("hidden");
			}else{
				$(".js-store-finder-pager-prev").removeClass("hidden");
				$(".js-store-finder-pager-next").removeClass("hidden");
			}

		}else{
			$(".js-store-finder-pager-prev").addClass("hidden");
			$(".js-store-finder-pager-next").addClass("hidden");
		}

		var page = ACC.storefinder.storeSearchData.page;
		$(".js-store-finder-pager-item-from").html(page*10+1);

		var to = ((page*10+10)>ACC.storefinder.storeData.total)? ACC.storefinder.storeData.total : page*10+10 ;
		$(".js-store-finder-pager-item-to").html(to);
		$(".js-store-finder-pager-item-all").html(ACC.storefinder.storeData.total);
		$(".js-store-finder").removeClass("show-store");

	},


	bindPagination:function ()
	{


		
		$(document).on("click",".js-store-finder-details-back",function(e){
			e.preventDefault();
			
			$(".js-store-finder").removeClass("show-store");
			
		})
		



		$(document).on("click",".js-store-finder-pager-prev",function(e){
			e.preventDefault();
			var page = ACC.storefinder.storeSearchData.page;
			ACC.storefinder.getStoreData(page-1)
			checkStatus(page-1);
		})

		$(document).on("click",".js-store-finder-pager-next",function(e){
			e.preventDefault();
			var page = ACC.storefinder.storeSearchData.page;
			ACC.storefinder.getStoreData(page+1)
			checkStatus(page+1);
		})

		function checkStatus(page){
			if(page==0){
				$(".js-store-finder-pager-prev").attr("disabled","disabled")
			}else{
				$(".js-store-finder-pager-prev").removeAttr("disabled")
			}
			
			if(page == Math.floor(ACC.storefinder.storeData.total/10)){
				$(".js-store-finder-pager-next").attr("disabled","disabled")
			}else{
				$(".js-store-finder-pager-next").removeAttr("disabled")
			}
		}

	},


	bindStoreChange:function()
	{
		$(document).on("change",".js-store-finder-input",function(e){
			e.preventDefault();



			storeData=ACC.storefinder.storeData["data"];

			var storeId=$(this).data("id");

			var $ele = $(".js-store-finder-details");
			


			$.each(storeData[storeId],function(key,value){
				if(key=="image"){
					if(value!=""){
						$ele.find(".js-store-image").html('<img src="'+value+'" alt="" />').removeClass('hidden');
						$ele.find(".store__finder--details-info").removeClass('no-image');
					}else{
						$ele.find(".js-store-image").html('').addClass('hidden');
						$ele.find(".store__finder--details-info").addClass('no-image');
					}
				}else if(key=="productcode"){
					$ele.find(".js-store-productcode").val(value);
				}
				else if(key=="openings"){
					if(value!=""){
						var $oele = $ele.find(".js-store-"+key);
						var openings = "";
						$.each(value,function(key2,value2){
							openings += "<dt>"+VMB.i18n.weekdays[key2]+"</dt>";
							openings += "<dd>"+value2+"</dd>";
						});

						$oele.html(openings);

					}else{
						$ele.find(".js-store-"+key).html('');
					}

				}
				else if(key=="specialOpenings"){}
				else if(key=="features"){
					var features="";
					$.each(value,function(key2,value2){
						features += "<li>"+value2+"</li>";
					});

					$ele.find(".js-store-"+key).html(features);

					if(_.isEmpty(features)){
						$ele.find(".openings__title").addClass('hidden');
					}else{
						$ele.find(".openings__title").removeClass('hidden');
					}

				}
				else if(key=="phone"){
					if(value !== ""){
						var phoneHref = value.replace(" ", "").replace("/", "").replace(".", "");
						$ele.find(".js-store-"+key).html("<a href='tel:"+phoneHref+"'>"+value+"</a>");
					}
				}
				else{
					if(value!=""){
						$ele.find(".js-store-"+key).html(value);
					}else{
						$ele.find(".js-store-"+key).html('');
					}
				}

			})


			ACC.storefinder.storeId = storeData[storeId];
			ACC.storefinder.initGoogleMap();

		})

		$(document).on("click",".js-select-store-label",function(e){
			$(".js-store-finder").addClass("show-store")
		})

		$(document).on("click",".js-back-to-storelist",function(e){
			$(".js-store-finder").removeClass("show-store")
		})

	},



	initGoogleMap:function(){

		if($(".js-store-finder-map").length > 0){
			ACC.global.addGoogleMapsApi("ACC.storefinder.loadGoogleMap");
		}
	},
 
	loadGoogleMap: function(){

		storeInformation = ACC.storefinder.storeId;

		if($(".js-store-finder-map").length > 0)
		{			
			$(".js-store-finder-map").attr("id","store-finder-map")
			var centerPoint = new google.maps.LatLng(storeInformation["latitude"], storeInformation["longitude"]);
			
			var mapOptions = {
				zoom: 13,
				zoomControl: true,
				panControl: true,
				streetViewControl: false,
				mapTypeId: google.maps.MapTypeId.ROADMAP,
				center: centerPoint
			}
			
			var map = new google.maps.Map(document.getElementById("store-finder-map"), mapOptions);
			
			var marker = new google.maps.Marker({
				position: new google.maps.LatLng(storeInformation["latitude"], storeInformation["longitude"]),
				map: map,
				title: storeInformation["name"],
				icon: "/_ui/responsive/theme-vanmarckeblue/images/vmb_stock_tec_print.png"
			});

			var infowindow = new google.maps.InfoWindow({
				content: storeInformation["displayName"],
				disableAutoPan: true
			});
			google.maps.event.addListener(marker, 'click', function (){
				infowindow.open(map, marker);
			});
		}
		
	},


	bindSearch:function(){

		$(document).on("submit",'#storeFinderForm', function(e){
			e.preventDefault()
			var q = $(".js-store-finder-search-input").val();
			ACC.storefinder.getInitStoreData(q, ACC.storefinder.coords.latitude, ACC.storefinder.coords.longitude);
		});


		$(".js-store-finder").hide();
		$(document).on("click",'#findStoresNearMe', function(e){
			e.preventDefault()
			ACC.storefinder.getInitStoreData(null,ACC.storefinder.coords.latitude,ACC.storefinder.coords.longitude);
		})


	},


	getStoreData: function(page){
		ACC.storefinder.storeSearchData.page = page;
		url= $(".js-store-finder").data("url");
		$.ajax({
			url: url,
			data: ACC.storefinder.storeSearchData,
			type: "get",
			success: function (response){
				ACC.storefinder.storeData = $.parseJSON(response);
				ACC.storefinder.refreshNavigation();
				if(ACC.storefinder.storeData.total < 11){
					$(".js-store-finder-pager-next").attr("disabled","disabled");
				}
			}
		});
	},

	getInitStoreData: function(q,latitude,longitude){
		$(".alert").remove();
		var data ={
			"q":"" ,
			"page":0
		};
		if(q != null){
			data.q = q;
		}

		if(latitude != null){
			data.latitude = latitude;
		}

		if(longitude != null){
			data.longitude = longitude;
		}

		ACC.storefinder.storeSearchData = data;
		ACC.storefinder.getStoreData(data.page);
		$(".js-store-finder").show();
		$(".js-store-finder-pager-prev").attr("disabled","disabled");
		$(".js-store-finder-pager-next").removeAttr("disabled")
	},

	init:function(){
		$("#findStoresNearMe").attr("disabled","disabled");
		if(navigator.geolocation){
			navigator.geolocation.getCurrentPosition(
				function (position){
					ACC.storefinder.coords = position.coords;
					$('#findStoresNearMe').removeAttr("disabled");
				},
				function (error)
				{
					console.log("An error occurred... The error code and message are: " + error.code + "/" + error.message);
				}
			);
		}
	}
};