<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/nav" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="wishlist" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/wishlist" %>

<spring:htmlEscape defaultHtmlEscape="true"/>

<spring:url value="/my-account/saved-baskets/" var="wishListsLink" htmlEscape="false"/>
<c:set var="searchUrl" value="/my-account/saved-baskets?sort=${ycommerce:encodeUrl(sortData.code)}"/>

<div class="vmb-wish-list-overview">
    <h1><spring:theme code="text.account.savedCarts"/></h1>

    <c:if test="${empty searchPageData.results}">
        <div class="account-section-content content-empty">
            <ycommerce:testId code="wishLists_noOrders_label">
                <spring:theme code="text.account.savedCarts.noSavedCarts"/>
            </ycommerce:testId>
        </div>
    </c:if>

    <c:if test="${not empty searchPageData.results}">
        <div class="account-section-content">

            <nav:paginationNewSPD top="true" msgKey="text.account.savedCarts.page" showCurrentPageInfo="true"
                                  hideRefineButton="true"
                                  supportShowPaged="${isShowPageAllowed}" supportShowAll="${isShowAllAllowed}"
                                  searchPageData="${searchPageData}" searchUrl="${searchUrl}"
                                  numberPagesShown="${numberPagesShown}"
                                  sorts="${allWishListSorts}"/>

            <div class="account-overview-table wish__lists__overview--table">
                <c:set var="cartIdRowMapping" value=''/>
                <table class="table vmb-wish-list-overview__table responsive-table">
                    <thead>
                    <tr>
                        <th><spring:theme code="text.account.savedCart.name"/></th>
                        <th><spring:theme code="text.account.savedCart.dateSaved"/></th>
                        <th><spring:theme code="text.account.savedCart.description"/></th>
                        <th><spring:theme code="text.account.savedCart.qty"/></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${searchPageData.results}" var="wishList" varStatus="loop">
                        <c:choose>
                            <c:when test="${wishList.importStatus eq 'PROCESSING' }">
                                <c:set var="importCartIsProcessing" value="true"/>
                                <c:set var="cartIdRowMapping"
                                       value="${cartIdRowMapping}${fn:escapeXml(wishList.name)}:${loop.index},"/>
                            </c:when>
                            <c:otherwise>
                                <c:set var="importCartIsProcessing" value="false"/>
                            </c:otherwise>
                        </c:choose>
                        <tr id="row-${loop.index}">
                            <td class="wish-list-name responsive-table-cell"
                                data-label="<spring:theme code='text.account.savedCart.name' javaScriptEscape="true" htmlEscape="true"/>:">
                                <ycommerce:testId code="wishLists_name_link">
                                    <a href="${wishListsLink}${ycommerce:encodeUrl(wishList.name)}"
                                       class="responsive-table-link js-wish-list-name ${importCartIsProcessing ? 'not-active' : '' }">
                                            ${fn:escapeXml(wishList.name)}
                                    </a>
                                </ycommerce:testId>
                            </td>
                            <td class="responsive-table-cell vmb-wish-list-overview__table"
                                data-label="<spring:theme code='text.account.savedCart.dateSaved' javaScriptEscape="true" htmlEscape="true" />:">
                                    <span class="js-wish-list-date ${importCartIsProcessing ? 'hidden' : '' }">
                                        <ycommerce:testId code="wishLists_created_label">
                                            <fmt:formatDate value="${wishList.saveTime}" dateStyle="medium"
                                                            timeStyle="short"
                                                            type="both"/>
                                        </ycommerce:testId>
                                    </span>
                            </td>
                            <td class="responsive-table-cell vmb-wish-list-overview__table-description"
                                data-label="<spring:theme code='text.account.savedCart.description' javaScriptEscape="true" htmlEscape="true"/>:">
                                <ycommerce:testId code="wishLists_description_label">
                                        <span class="js-wish-list-description">
                                            <c:choose>
                                                <c:when test="${importCartIsProcessing}">
                                                    <span class="file-importing js-file-importing">
                                                        <img src="${commonResourcePath}/images/3dots.gif" width="25"
                                                             height="25"/>
                                                    </span>
                                                </c:when>
                                                <c:otherwise>
                                                    ${fn:escapeXml(wishList.description)}
                                                </c:otherwise>
                                            </c:choose>
                                        </span>
                                </ycommerce:testId>
                            </td>
                            <td data-label="<spring:theme code='text.account.savedCart.qty' javaScriptEscape="true" htmlEscape="true"/>:"
                                class="vmb-wish-list-overview__table-quantity">
                                <ycommerce:testId code="wishLists_noOfItems_label">
                                        <span class="js-wish-list-number-of-items">
                                            <c:if test="${importCartIsProcessing eq false}">
                                                ${wishList.quantity}
                                            </c:if>
                                        </span>
                                </ycommerce:testId>
                            </td>
                            <td class="responsive-table-cell vmb-wish-list-overview__table-icons">
                                <ul class="list-unstyled list-inline">
                                    <li>
                                        <c:set var="editToolTip"><spring:theme
                                                code="text.account.savedCart.edit"/></c:set>
                                        <a href="${wishListsLink}${ycommerce:encodeUrl(wishList.name)}"
                                           class="responsive-table-link js-wish-list-name ${importCartIsProcessing ? 'not-active' : '' }">
                                            <span class="glyphicon glyphicon-pencil" data-toggle="tooltip"
                                                  data-original-title="${editToolTip}"></span>
                                        </a>
                                    </li>
                                    <li>
                                        <c:set var="restoreToolTip"><spring:theme
                                                code="text.account.savedCart.restore"/></c:set>
                                        <a href="#"
                                           class="js-restore-wish-list js-restore-wish-list-all restore-item-link ${importCartIsProcessing || fn:length(searchPageData.results) < 1 ? 'hidden' : '' }"
                                           data-wishlist-id="${fn:escapeXml(wishList.name)}">
                                            <span class="glyphicon glyphicon-shopping-cart" data-toggle="tooltip"
                                                  data-original-title="${restoreToolTip}"></span>
                                        </a>
                                    </li>
                                    <li>
                                        <c:set var="exportToolTip"><spring:theme code="basket.export.csv.file"/></c:set>
                                        <a href="#"
                                           class="js-export-basket restore-item-link ${importCartIsProcessing || fn:length(searchPageData.results) < 1 ? 'hidden' : '' }"
                                           data-basket-name="${fn:escapeXml(wishList.name)}"
                                           data-basket-type="WISHLIST">
                                            <span class="glyphicon glyphicon-export" data-toggle="tooltip"
                                                  data-original-title="${exportToolTip}"></span>
                                        </a>
                                    </li>
                                    <li>
                                        <c:set var="deleteToolTip"><spring:theme code="general.delete.button"/></c:set>
                                        <a href="#"
                                           class="js-delete-wish-list remove-item-link ${importCartIsProcessing ? 'hidden' : '' }"
                                           data-wishlist-id="${fn:escapeXml(wishList.name)}"
                                           data-wishlist-name="${fn:escapeXml(wishList.name)}">
                                            <span class="glyphicon glyphicon-trash" data-toggle="tooltip"
                                                  data-original-title="${deleteToolTip}"></span>
                                        </a>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
                <div class="js-uploading-wish-lists-update" data-id-row-mapping="${cartIdRowMapping}"
                     data-refresh-cart="${refreshSavedCart}"
                     data-refresh-interval="${refreshSavedCartInterval}"></div>
            </div>

            <nav:paginationNewSPD top="false" msgKey="text.account.savedCarts.page" showCurrentPageInfo="true"
                                  hideRefineButton="true"
                                  supportShowPaged="${isShowPageAllowed}" supportShowAll="${isShowAllAllowed}"
                                  searchPageData="${searchPageData}" searchUrl="${searchUrl}"
                                  numberPagesShown="${numberPagesShown}"
                                  sorts="${allWishListSorts}"/>
        </div>
        <wishlist:wishListDeleteModal/>
        <wishlist:wishlistUnavailableProductsModal/>

    </c:if>


    <div class="text-right">
        <a class="btn btn-default" href="<c:url value="/import/csv/wish-list"/>">
            <span class="glyphicon glyphicon-import"></span>&nbsp;<spring:theme code="import.csv.savedCart.title"/>
        </a>
    </div>
</div>