package com.vanmarcke.blue.addon.controllers;

import com.vanmarcke.core.model.ClientServiceParagraphComponentModel;
import com.vanmarcke.core.model.ConsentModalComponentModel;
import com.vanmarcke.core.model.FooterPaymentModesComponentModel;
import de.hybris.platform.acceleratorcms.model.components.MiniCartComponentModel;
import de.hybris.platform.acceleratorcms.model.components.ProductReferencesComponentModel;
import de.hybris.platform.cms2lib.model.components.ProductCarouselComponentModel;

/**
 *
 */
public interface VanmarckeblueaddonControllerConstants {

    // Constant names cannot be changed due to their usage in dependant extensions, thus nosonar
    String BLUE_ADDON_PREFIX = "addon:/vanmarckeblueaddon/";

    /**
     * Class with action name constants
     */
    interface Actions {

        interface Cms {

            String PREFIX = "/view/";
            String SUFFIX = "Controller";

            String MINI_CART_COMPONENT = PREFIX + MiniCartComponentModel._TYPECODE + SUFFIX;
            String FOOTER_PAYMENT_MODES_COMPONENT = PREFIX + FooterPaymentModesComponentModel._TYPECODE + SUFFIX;
            String CLIENT_SERVICE_PARAGRAPH_COMPONENT = PREFIX + ClientServiceParagraphComponentModel._TYPECODE + SUFFIX;
            String PRODUCT_REFERENCE_COMPONENT = PREFIX + ProductReferencesComponentModel._TYPECODE + SUFFIX;
            String PRODUCT_CAROUSEL_COMPONENT = PREFIX + ProductCarouselComponentModel._TYPECODE + SUFFIX;
            String CONSENT_MODAL_COMPONENT = PREFIX + ConsentModalComponentModel._TYPECODE + SUFFIX;
        }
    }

    /**
     * Class with view name constants
     */
    interface Views {

        interface Cms {

            String COMPONENT_PREFIX = "cms/";
        }

        interface Pages {

            interface Account {

                String ACCOUNT_LOGIN_PAGE = BLUE_ADDON_PREFIX + "pages/account/accountLoginPage";
                String ACCOUNT_REGISTER_PAGE = "pages/account/accountRegisterPage";
            }

            interface Checkout {

                String CHECKOUT_CONFIRMATION_PAGE = "pages/checkout/checkoutConfirmationPage";
                String CHECKOUT_LOGIN_PAGE = BLUE_ADDON_PREFIX + "pages/checkout/checkoutLoginPage";
            }

            interface MultiStepCheckout {

                String ADD_EDIT_DELIVERY_ADDRESS_PAGE = BLUE_ADDON_PREFIX + "pages/checkout/multi/addEditDeliveryAddressPage";
                String CHOOSE_DELIVERY_METHOD_PAGE = BLUE_ADDON_PREFIX + "pages/checkout/multi/chooseDeliveryMethodPage";
                String PAYMENT_ADDRESS_PAGE = BLUE_ADDON_PREFIX + "pages/checkout/multi/paymentAddressPage";
                String CHOOSE_PICKUP_LOCATION_PAGE = "pages/checkout/multi/choosePickupLocationPage";
                String ADD_PAYMENT_METHOD_PAGE = "pages/checkout/multi/addPaymentMethodPage";
                String CHECKOUT_SUMMARY_PAGE = BLUE_ADDON_PREFIX + "pages/checkout/multi/checkoutSummaryPage";
                String HOSTED_ORDER_PAGE_ERROR_PAGE = "pages/checkout/multi/hostedOrderPageErrorPage";
                String HOSTED_ORDER_POST_PAGE = "pages/checkout/multi/hostedOrderPostPage";
                String SILENT_ORDER_POST_PAGE = "pages/checkout/multi/silentOrderPostPage";
                String CHOOSE_PAYMENT_TYPE_PAGE = BLUE_ADDON_PREFIX + "pages/checkout/multi/choosePaymentTypePage";
            }

            interface Error {

                String ERROR_NOT_FOUND_PAGE = BLUE_ADDON_PREFIX + "pages/error/errorNotFoundPage";
            }

            interface Cart {

                String CART_PAGE = BLUE_ADDON_PREFIX + "pages/cart/cartPage";
            }

            interface StoreFinder {

                String STORE_FINDER_DETAILS_PAGE = "pages/storeFinder/storeFinderDetailsPage";
                String STORE_FINDER_VIEW_MAP_PAGE = "pages/storeFinder/storeFinderViewMapPage";
                String STORE_FINDER_SEARCH_PAGE = BLUE_ADDON_PREFIX + "pages/storeFinder/storeFinderSearchPage";
            }

            interface Guest {

                String GUEST_ORDER_PAGE = BLUE_ADDON_PREFIX + "pages/guest/guestOrderPage";
            }

            interface Product {

                String ORDER_FORM = "pages/product/productOrderFormPage";
            }

            interface CSV {

                String IMPORT_CSV_SAVED_CART_PAGE = BLUE_ADDON_PREFIX + "pages/csv/importCSVSavedCartPage";
            }
        }

        interface Fragments {

            interface Cart {

                String ADD_TO_CART_POPUP = "fragments/cart/addToCartPopup";
                String MINI_CART_PANEL = BLUE_ADDON_PREFIX + "fragments/cart/miniCartPanel";
                String CART_POPUP = BLUE_ADDON_PREFIX + "fragments/cart/cartPopup";
                String EXPAND_GRID_IN_CART = "fragments/cart/expandGridInCart";
            }

            interface AddressValidation {

                String ADDRESS_VALIDATION_POPUP = BLUE_ADDON_PREFIX + "fragments/address/validationPopup";
            }

            interface Account {

                String COUNTRY_ADDRESS_FORM = "fragments/address/countryAddressForm";
                String SAVED_CART_RESTORE_POPUP = BLUE_ADDON_PREFIX + "fragments/account/savedCartRestorePopup";
            }

            interface Checkout {

                String TERMS_AND_CONDITIONS_POPUP = "fragments/checkout/termsAndConditionsPopup";
                String BILLING_ADDRESS_FORM = "fragments/checkout/billingAddressForm";
                String READ_ONLY_EXPANDED_ORDER_FORM = "fragments/checkout/readOnlyExpandedOrderForm";
                String CHECKOUT_ORDER_TOTALS = "fragments/checkout/orderTotals";
            }

            interface Product {

                String FUTURE_STOCK_POPUP = "fragments/product/futureStockPopup";
                String QUICK_VIEW_POPUP = "fragments/product/quickViewPopup";
                String ZOOM_IMAGES_POPUP = "fragments/product/zoomImagesPopup";
                String PRODUCT_LIST = "fragments/product/productList";
            }

            interface CustomerListComponent {

                String ASM_CUSTOMER_LIST_TABLE = BLUE_ADDON_PREFIX + "asm/fragments/customer360/asmCustomerListTable";
                String ASM_CUSTOMER_LIST_POPUP = BLUE_ADDON_PREFIX + "asm/fragments/customer360/asmCustomerListComponent";

            }
        }
    }
}
