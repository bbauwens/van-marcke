package com.vanmarcke.blue.addon.controllers.misc;

import com.google.common.collect.ImmutableList;
import com.vanmarcke.blue.addon.constants.ExportSavedCartType;
import com.vanmarcke.blue.addon.form.ExportSavedBasketForm;
import com.vanmarcke.facades.cart.VMKCartFacade;
import com.vanmarcke.facades.data.order.SavedCartData;
import com.vanmarcke.facades.exportcart.VMKExportSavedCartFacade;
import com.vanmarcke.facades.order.VMKB2BOrderFacade;
import com.vanmarcke.facades.wishlist2.VMKWishListFacade;
import com.vanmarcke.services.constants.VanmarckeservicesConstants;
import com.vanmarcke.storefront.controllers.ControllerConstants;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.Range;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.IOException;

@Controller
@RequestMapping("/export-basket")
public class BlueExportSavedCartController extends AbstractPageController {

    private static final Logger LOG = Logger.getLogger(BlueExportSavedCartController.class);

    private static final Range EXPORT_PDF_EMPTY_LINES_RANGE = Range.between(0, 100);
    public static final String BASKET_NAME = "basketName";
    public static final String BASKET_TYPE = "basketType";

    private final VMKWishListFacade vmkWishListFacade;

    private final VMKExportSavedCartFacade vmkExportSavedCartFacade;

    private final VMKCartFacade cartFacade;

    private final VMKB2BOrderFacade vmkB2BOrderFacade;

    public BlueExportSavedCartController(VMKWishListFacade vmkWishListFacade,
                                         VMKExportSavedCartFacade vmkExportSavedCartFacade,
                                         VMKCartFacade cartFacade,
                                         VMKB2BOrderFacade vmkB2BOrderFacade) {
        this.vmkWishListFacade = vmkWishListFacade;
        this.vmkExportSavedCartFacade = vmkExportSavedCartFacade;
        this.cartFacade = cartFacade;
        this.vmkB2BOrderFacade = vmkB2BOrderFacade;
    }

    @RequestMapping(method = RequestMethod.GET)
    @RequireHardLogIn
    public Object export(@RequestParam(BASKET_NAME) final String basketName,
                         @RequestParam(BASKET_TYPE) final String basketType,
                         final Model model) {
        model.addAttribute(BASKET_NAME, basketName);
        model.addAttribute(BASKET_TYPE, basketType);

        if (ExportSavedCartType.WISHLIST == ExportSavedCartType.valueOf(basketType)) {
            try {
                vmkWishListFacade.getWishList(basketName);
            } catch (UnknownIdentifierException e) {
                return ResponseEntity.badRequest();
            }
        }
        return ControllerConstants.Views.Fragments.Account.WishListExportPopup;
    }

    @RequestMapping(value = "/generate", method = RequestMethod.GET)
    @RequireHardLogIn
    public ResponseEntity<ByteArrayResource> performExport(final ExportSavedBasketForm exportSavedBasketForm, final Model model) {
        try {
            if (StringUtils.equalsIgnoreCase("xls", exportSavedBasketForm.getExportType())) {

                // fetch the proper wishlist
                final SavedCartData savedCartData = getSavedCartDataByCartType(exportSavedBasketForm, exportSavedBasketForm.isExportExcelNettoPrice());

                // Prepare the header
                final ImmutableList.Builder<String> wishListHeaders = ImmutableList.<String>builder()
                        .add(getMessageSource().getMessage("basket.export.cart.item.sku", null, getI18nService().getCurrentLocale()))
                        .add(getMessageSource().getMessage("basket.export.cart.item.quantity", null, getI18nService().getCurrentLocale()))
                        .add(getMessageSource().getMessage("basket.export.cart.item.name", null, getI18nService().getCurrentLocale()))
                        .add(getMessageSource().getMessage("basket.export.cart.item.price", null, getI18nService().getCurrentLocale()));
                if(exportSavedBasketForm.isExportExcelNettoPrice()) {
                    wishListHeaders.add(getMessageSource().getMessage("basket.export.cart.item.net-price", null, getI18nService().getCurrentLocale()));
                }
                wishListHeaders.add(getMessageSource().getMessage("basket.export.cart.item.total-price", null, getI18nService().getCurrentLocale()))
                        .build();

                // first convert the requested wish list to Excel
                final byte[] xlsOutput = vmkExportSavedCartFacade.generateExcelFromSavedCart(wishListHeaders.build(), savedCartData, exportSavedBasketForm.isExportExcelNettoPrice(), 0);

                // pare the response data headers
                HttpHeaders httpHeader = new HttpHeaders();
                httpHeader.setContentType(new MediaType("application", "force-download"));
                httpHeader.set(HttpHeaders.CONTENT_DISPOSITION, String.format("attachment; filename=saved-basket-%s.xls", urlEncode(exportSavedBasketForm.getBasketName())));
                unsetExportSessionAttributes();

                // and set the response with the headers
                return ResponseEntity
                        .accepted()
                        .headers(httpHeader)
                        .body(new ByteArrayResource(xlsOutput));
            } else {

                // fetch the proper wishlist
                final SavedCartData savedCartData = getSavedCartDataByCartType(exportSavedBasketForm, exportSavedBasketForm.isExportPdfNettoPrice());

                // prepare the header
                final ImmutableList.Builder<String> wishListHeaders = ImmutableList.<String>builder();
                if(exportSavedBasketForm.isExportPdfIncludeImage()) {
                    wishListHeaders.add("");
                }
                wishListHeaders
                        .add(getMessageSource().getMessage("basket.export.cart.item.sku", null, getI18nService().getCurrentLocale()))
                        .add(getMessageSource().getMessage("basket.page.item", null, getI18nService().getCurrentLocale()))
                        .add(getMessageSource().getMessage("basket.page.qty", null, getI18nService().getCurrentLocale()))
                        .add(getMessageSource().getMessage("basket.page.price", null, getI18nService().getCurrentLocale()));
                if(exportSavedBasketForm.isExportPdfNettoPrice()) {
                    wishListHeaders.add(getMessageSource().getMessage("basket.page.price.netto", null, getI18nService().getCurrentLocale()));
                }
                wishListHeaders.add(getMessageSource().getMessage("basket.page.total", null, getI18nService().getCurrentLocale()))
                        .build();

                // export as PDF.
                final boolean includeNet = BooleanUtils.isTrue(exportSavedBasketForm.isExportPdfNettoPrice());
                final boolean includeImages = BooleanUtils.isTrue(exportSavedBasketForm.isExportPdfIncludeImage());
                final boolean includeTechnical = BooleanUtils.isTrue(exportSavedBasketForm.isExportPdfTechnical());
                final int emptyLines = EXPORT_PDF_EMPTY_LINES_RANGE.contains(exportSavedBasketForm.getExportPdfLines())? exportSavedBasketForm.getExportPdfLines() : 0;
                final byte[] pdfOutput = vmkExportSavedCartFacade.generatePdfFromSavedCart(wishListHeaders.build(), savedCartData, includeNet, includeImages, includeTechnical, emptyLines);

                // pare the response data headers
                HttpHeaders httpHeader = new HttpHeaders();
                httpHeader.setContentType(new MediaType("application", "force-download"));
                httpHeader.set(HttpHeaders.CONTENT_DISPOSITION, String.format("attachment; filename=saved-basket-%s.pdf", urlEncode(exportSavedBasketForm.getBasketName())));
                unsetExportSessionAttributes();

                // and set the response with the headers
                return ResponseEntity
                        .accepted()
                        .headers(httpHeader)
                        .body(new ByteArrayResource(pdfOutput));
            }

        } catch (final IOException e) {
            LOG.error("There was an error trying to export the basket", e);
            // done!
            unsetExportSessionAttributes();
            return ResponseEntity.badRequest().build();
        } catch (final ModelNotFoundException e) {
            LOG.error("There was an error in finding the basket", e);
            unsetExportSessionAttributes();
            return ResponseEntity.badRequest().build();
        } catch (final UnknownIdentifierException e) {
            LOG.error("Products unavailable in the wishlist: " + exportSavedBasketForm.getBasketName(), e);
            return ResponseEntity.badRequest().build();
        }
    }

    private SavedCartData getSavedCartDataByCartType(ExportSavedBasketForm exportSavedBasketForm, boolean exportNet) throws ModelNotFoundException {
        String basketName = exportSavedBasketForm.getBasketName();
        try {
            ExportSavedCartType basketType = ExportSavedCartType.valueOf(exportSavedBasketForm.getBasketType());
            getSessionService().setAttribute(VanmarckeservicesConstants.IS_EXPORT_SESSION_ATTRIBUTE, true);
            getSessionService().setAttribute(VanmarckeservicesConstants.IS_EXPORT_NET_PRICE_ATTRIBUTE, exportNet);

            switch (basketType) {
                case WISHLIST:
                    return vmkWishListFacade.getWishListWithNetValues(basketName, exportNet);
                case CART:
                    return cartFacade.getSavedCartDetails(basketName);
                case ORDER:
                    return vmkB2BOrderFacade.getOrderDetailsForExport(basketName);
                default:
                    throw new IllegalArgumentException();
            }

        } catch (IllegalArgumentException e) {
            LOG.error(String.format("Wrong basket type provided {}", exportSavedBasketForm.getBasketType()), e);
            throw new ModelNotFoundException(String.format("No cart or order found for given name {} or type ", basketName));
        }
    }

    private void unsetExportSessionAttributes() {
        getSessionService().setAttribute(VanmarckeservicesConstants.IS_EXPORT_SESSION_ATTRIBUTE, false);
        getSessionService().setAttribute(VanmarckeservicesConstants.IS_EXPORT_NET_PRICE_ATTRIBUTE, false);
    }
}