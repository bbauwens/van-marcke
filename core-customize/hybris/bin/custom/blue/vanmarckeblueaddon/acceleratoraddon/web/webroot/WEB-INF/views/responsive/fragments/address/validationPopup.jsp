<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json" %>

<json:object escapeXml="false">
    <json:object name="original">
        <json:property name="line1" value="${originalAddressData.line1}"/>
        <json:property name="line2" value="${originalAddressData.line2}"/>
        <json:property name="apartment" value="${originalAddressData.apartment}"/>
        <json:property name="town" value="${originalAddressData.town}"/>
        <json:property name="postalCode" value="${originalAddressData.postalCode}"/>
        <json:object name="country">
            <json:property name="isocode" value="${originalAddressData.country.isocode}"/>
        </json:object>
    </json:object>
    <json:property name="exactMatch" value="${exactMatch}"/>
    <c:choose>
        <c:when test="${not empty validationErrorCode}">
            <json:object name="error">
                <json:property name="code" value="${validationErrorCode}"/>
                <json:property name="message" value="${validationErrorDescription}"/>
            </json:object>
        </c:when>
        <c:otherwise>
            <json:array name="suggestions" items="${validationResult}" var="address" >
                <json:object>
                    <json:property name="line1" value="${address.line1}"/>
                    <json:property name="line2" value="${address.line2}"/>
                    <json:property name="apartment" value="${originalAddressData.apartment}"/>
                    <json:property name="town" value="${address.town}"/>
                    <json:property name="postalCode" value="${address.postalCode}"/>
                    <json:object name="country">
                        <json:property name="isocode" value="${address.country.isocode}"/>
                    </json:object>
                </json:object>
            </json:array>
        </c:otherwise>
    </c:choose>
</json:object>