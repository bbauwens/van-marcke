<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/formElement" %>

<div class="row">
    <div class="col-xs-12 col-sm-6">
        <formElement:formInputBox idKey="address-firstName" labelKey="address.firstName" path="firstName"
                                  inputCSS="form-control" mandatory="true" maxlength="24"/>
    </div>
    <div class="col-xs-12 col-sm-6">
        <formElement:formInputBox idKey="address-surname" labelKey="address.surname" path="lastName"
                                  inputCSS="form-control" mandatory="true" maxlength="25"/>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-6">
        <formElement:formInputBox idKey="address-line1" labelKey="address.line1" path="line1" inputCSS="form-control"
                                  mandatory="true" maxlength="50"/>
    </div>
    <div class="col-xs-12 col-sm-3">
        <formElement:formInputBox idKey="address-line2" labelKey="address.line2" path="line2" inputCSS="form-control"
                                  mandatory="true" maxlength="5"/>
    </div>
    <div class="col-xs-12 col-sm-3">
        <formElement:formInputBox idKey="address-apartment" labelKey="address.apartment" path="apartment"
                                  inputCSS="form-control" maxlength="5"/>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-4">
        <formElement:formInputBox idKey="address-postcode" labelKey="address.postcode" path="postcode"
                                  inputCSS="form-control" mandatory="true" maxlength="10"/>
    </div>
    <div class="col-xs-12 col-sm-8">
        <formElement:formInputBox idKey="address-townCity" labelKey="address.townCity" path="townCity"
                                  inputCSS="form-control" mandatory="true" maxlength="30"/>
    </div>
</div>
<formElement:formInputBox idKey="address-country" labelKey="address.country" path="countryIso" inputCSS="form-control"
                          mandatory="true" disabled="true"/>
<formElement:formInputBox idKey="address-phone" labelKey="address.phone" path="phone" inputCSS="form-control"
                          maxlength="10"/>
<formElement:formInputBox idKey="address-mobile" labelKey="address.mobile" path="mobile" inputCSS="form-control"
                          mandatory="true" maxlength="15"/>