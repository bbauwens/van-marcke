(function ($) {
    'use strict';

    $.datepicker.setDefaults(VMB.datepicker.regional);

    // Function that delays curtain event
    $.fn.onDelayed = function (eventName, delayInMs, callback) {
        var _timeout;
        this.on(eventName, function (e) {
            if (!!_timeout) {
                clearTimeout(_timeout);
            }
            _timeout = setTimeout(function () {
                callback(e);
            }, delayInMs);
        });
    };

    // Restricts input for each element in the set of matched elements to the given inputFilter.
    $.fn.inputFilter = function (inputFilter) {
        return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function (evt) {

            if (!this.hasOwnProperty("oldValue")) {
                this.oldValue = 1;
            }

            if (inputFilter(this.value)) {
                this.oldValue = this.value;
            } else if (this.hasOwnProperty("oldValue")) {
                this.value = this.oldValue;
                evt.preventDefault();
                return false;
            }
        });
    };

    //Auto hide Global Messages
    if ($('.global-alerts .alert').length > 0) {
        setTimeout(function () {
            $('.global-alerts .alert').fadeOut();
        }, 15000)
    }

    // Set default settingd for Select 2
    $.fn.select2.defaults.set('language', VMB.select2.language);
    $.fn.select2.defaults.set('placeholder', VMB.select2.placeholder);

    $('[data-toggle="tooltip"]').tooltip({html: true, container: 'body'});

    $("#_asmPersonifyForm input[name='customerName'], input[name='customerId']").off("hover");

    $("#_asmPersonifyForm > div").hover(function () {
        var thisElem = $("#_asmPersonifyForm input[name='customerName'], input[name='customerId']");
        var item = (thisElem.attr('data-hover')) ? jQuery.parseJSON(thisElem.attr('data-hover')) : thisElem.data("hover");
        var disabled = (thisElem.attr('data-hover')) ? "disabled" : "";

        if (!(item === null || item === undefined)) {
            thisElem
                .after(
                    $('<div>')
                        .attr('id', 'asmHover')
                        .addClass(disabled)
                        .append(
                            $('<span>').addClass('name').text(item.name),
                            $('<span>').addClass('email').text(item.email),
                            $('<span>').addClass('date').text(item.date)
                        )
                    );
            }
        }, function () {
            removeAsmHover();
        }
    );

    $(".vmb-form-validation").validate({
        showErrors: function (errorMap, errorList) {
            $.each(errorList, function (i, val) {
                $(val.element).addClass('invalid');
                $(val.element).parent().addClass("has-error");
            });

            if (errorList.length > 0) {
                $(this.currentForm).find('.btn-submit').prop("disabled", true);
            } else {
                cleanUpFormErrors("." + $(this.currentForm).attr('class'));
                $(this.currentForm).find('.btn-submit').prop("disabled", false);
            }
        }
    });

    $("img, a").hover(function () {
        $(this).attr("tooltip-data", $(this).attr("title"));
        $(this).removeAttr("title");
    }, function () {
        $(this).attr("title", $(this).attr("tooltip-data"));
        $(this).removeAttr("tooltip-data");
    });
})(jQuery);


//Function to show loader
function showLoader() {
    $("main").addClass("loading");
    $(".vmb-loader").fadeIn();
}

//Function to hide loader
function hideLoader() {
    $("main").removeClass("loading");
    $(".vmb-loader").fadeOut();
}

// Function to show a global error message
var msgsAutoHide = {};

var vmbMessages = {
    msgsAutoHide: {},

    types: {
        ERROR: "error",
        SUCCESS: "success",
        INFO: "info",
        WARING: "warning"
    },

    typeClasses: {
        error: "danger",
        success: "success",
        info: "info",
        warning: "warning"
    },
    createError: function (id, text, autoHide) {
        this.create(id, this.types.ERROR, text, autoHide);
    },
    createInfo: function (id, text, autoHide) {
        this.create(id, this.types.INFO, text, autoHide);
    },
    createSuccess: function (id, text, autoHide) {
        this.create(id, this.types.SUCCESS, text, autoHide);
    },
    createWarning: function (id, text, autoHide) {
        this.create(id, this.types.WARING, text, autoHide);
    },

    create: function (id, type, text, autoHide) {

        $('.global-alerts .vmb-alert-' + id).remove();

        var AlertMsgBox = "<div class='alert alert-" + this.typeClasses[type] + " vmb-alert-" + id + "'>" + text + "<button class='close closeAccAlert' aria-hidden='true' data-dismiss='alert' type='button'>&times;</button></div>";

        if ($(".global-alerts").length > 0) {
            $(".global-alerts").append(AlertMsgBox);
        } else {
            $("body").prepend("<div class='global-alerts'>" + AlertMsgBox + "</div>");
        }

        $(".closeAccAlert").on("click", function () {
            $(this).parent().remove();
        });

        if (autoHide) {
            if (this.msgsAutoHide[id]) {
                clearTimeout(this.msgsAutoHide[id]);
            }

            this.msgsAutoHide[id] = setTimeout(function () {
                $('.global-alerts .vmb-alert-' + id).fadeOut();
            }, 15000)
        }
    }
};

function cleanUpFormErrors(form) {

    $(form + " input.invalid").each(function () {
        $(this).removeClass("invalid");
        $(this).parent().removeClass("has-error");
    });

}

//Function to test if val is empty
function empty(val) {

    // test results
    //---------------
    // []        true, empty array
    // {}        true, empty object
    // null      true
    // undefined true
    // ""        true, empty string
    // ''        true, empty string
    // 0         false, number
    // true      false, boolean
    // false     false, boolean
    // Date      false
    // function  false

    if (val === undefined)
        return true;

    if (typeof (val) == 'function' || typeof (val) == 'number' || typeof (val) == 'boolean' || Object.prototype.toString.call(val) === '[object Date]')
        return false;

    if (val == null || val.length === 0)        // null or 0 length array
        return true;

    if (typeof (val) == "object") {
        // empty object

        var r = true;

        for (var f in val)
            r = false;

        return r;
    }

    return false;
}

/**
 * Replaces markers in given text with arguments from given array resolved in order
 *
 * @param fmt the text to format
 * @param arguments the arguments array
 */
function formatMessage(fmt, arguments) {
    var result = fmt;
    for (var k in arguments) {
        result = result.replace(new RegExp('\\{' + k + '\\}', 'g'), arguments[k]);
    }
    return result;
}
