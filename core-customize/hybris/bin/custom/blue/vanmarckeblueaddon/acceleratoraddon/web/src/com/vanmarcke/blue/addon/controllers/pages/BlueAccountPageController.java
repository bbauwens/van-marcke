package com.vanmarcke.blue.addon.controllers.pages;

import com.vanmarcke.blue.addon.form.BlueAddressForm;
import com.vanmarcke.facades.cms.VMKConsentParagraphComponentFacade;
import com.vanmarcke.facades.consent.VMKConsentFacade;
import com.vanmarcke.facades.principal.b2bunit.VMKB2BUnitFacade;
import com.vanmarcke.facades.storesession.VMKStoreSessionFacade;
import de.hybris.platform.acceleratorfacades.ordergridform.OrderGridFormFacade;
import de.hybris.platform.acceleratorfacades.product.data.ReadOnlyOrderGridData;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractSearchPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.forms.AddressForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.UpdateEmailForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.UpdatePasswordForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.UpdateProfileForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.validation.EmailValidator;
import de.hybris.platform.acceleratorstorefrontcommons.forms.validation.PasswordValidator;
import de.hybris.platform.acceleratorstorefrontcommons.forms.validation.ProfileValidator;
import de.hybris.platform.acceleratorstorefrontcommons.forms.verification.AddressVerificationResultHandler;
import de.hybris.platform.acceleratorstorefrontcommons.strategy.CustomerConsentDataStrategy;
import de.hybris.platform.acceleratorstorefrontcommons.util.AddressDataUtil;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.address.AddressVerificationFacade;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.i18n.I18NFacade;
import de.hybris.platform.commercefacades.order.OrderFacade;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.order.data.OrderHistoryData;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commercefacades.user.exceptions.PasswordMismatchException;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.commerceservices.util.ResponsiveUtils;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.util.Config;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.vanmarcke.blue.addon.controllers.VanmarckeblueaddonControllerConstants.Views.Fragments.Account.COUNTRY_ADDRESS_FORM;
import static com.vanmarcke.blue.addon.controllers.VanmarckeblueaddonControllerConstants.Views.Fragments.Checkout.READ_ONLY_EXPANDED_ORDER_FORM;

/**
 * Controller for home page
 */
@Controller
@RequestMapping("/my-account")
public class BlueAccountPageController extends AbstractSearchPageController {

    private static final String TEXT_ACCOUNT_ADDRESS_BOOK = "text.account.addressBook";
    private static final String BREADCRUMBS_ATTR = "breadcrumbs";
    private static final String IS_DEFAULT_ADDRESS_ATTR = "isDefaultAddress";
    private static final String ADDRESS_BOOK_EMPTY_ATTR = "addressBookEmpty";
    private static final String FORM_GLOBAL_ERROR = "form.global.error";
    private static final String PROFILE_CURRENT_PASSWORD_INVALID = "profile.currentPassword.invalid";
    private static final String TEXT_ACCOUNT_PROFILE = "text.account.profile";
    private static final String ADDRESS_DATA_ATTR = "addressData";
    private static final String ADDRESS_FORM_ATTR = "addressForm";
    private static final String MY_ACCOUNT_ADDRESS_BOOK_URL = "/my-account/address-book";
    private static final String TEXT_ACCOUNT_CONSENT_MANAGEMENT = "text.account.consent.consentManagement";
    private static final String TEXT_ACCOUNT_CLOSE = "text.account.close";

    // Internal Redirects
    private static final String REDIRECT_TO_ADDRESS_BOOK_PAGE = REDIRECT_PREFIX + MY_ACCOUNT_ADDRESS_BOOK_URL;
    private static final String REDIRECT_TO_PAYMENT_INFO_PAGE = REDIRECT_PREFIX + "/my-account/payment-details";
    private static final String REDIRECT_TO_EDIT_ADDRESS_PAGE = REDIRECT_PREFIX + "/my-account/edit-address/";
    private static final String REDIRECT_TO_UPDATE_EMAIL_PAGE = REDIRECT_PREFIX + "/my-account/update-email";
    private static final String REDIRECT_TO_UPDATE_PROFILE = REDIRECT_PREFIX + "/my-account/update-profile";
    private static final String REDIRECT_TO_PASSWORD_UPDATE_PAGE = REDIRECT_PREFIX + "/my-account/update-password";
    private static final String REDIRECT_TO_ORDER_HISTORY_PAGE = REDIRECT_PREFIX + "/my-account/orders";

    /**
     * We use this suffix pattern because of an issue with Spring 3.1 where a Uri value is incorrectly extracted if it
     * contains on or more '.' characters. Please see https://jira.springsource.org/browse/SPR-6164 for a discussion on
     * the issue and future resolution.
     */
    private static final String ORDER_CODE_PATH_VARIABLE_PATTERN = "{orderCode:.*}";
    private static final String ADDRESS_CODE_PATH_VARIABLE_PATTERN = "{addressCode:.*}";

    // CMS Pages
    private static final String ACCOUNT_CMS_PAGE = "account";
    private static final String PROFILE_CMS_PAGE = "profile";
    private static final String UPDATE_PASSWORD_CMS_PAGE = "updatePassword";
    private static final String UPDATE_PROFILE_CMS_PAGE = "update-profile";
    private static final String UPDATE_EMAIL_CMS_PAGE = "update-email";
    private static final String CONSENT_MGMT_CMS_PAGE = "consent-management";
    private static final String ADDRESS_BOOK_CMS_PAGE = "address-book";
    private static final String ADD_EDIT_ADDRESS_CMS_PAGE = "add-edit-address";
    private static final String PAYMENT_DETAILS_CMS_PAGE = "payment-details";
    private static final String ORDER_HISTORY_CMS_PAGE = "orders";
    private static final String ORDER_DETAIL_CMS_PAGE = "order";
    private static final String CLOSE_ACCOUNT_CMS_PAGE = "close-account";

    private static final Logger LOG = Logger.getLogger(BlueAccountPageController.class);

    @Resource(name = "orderFacade")
    private OrderFacade orderFacade;

    @Resource(name = "userFacade")
    private UserFacade userFacade;

    @Resource(name = "customerFacade")
    private CustomerFacade customerFacade;

    @Resource(name = "accountBreadcrumbBuilder")
    private ResourceBreadcrumbBuilder accountBreadcrumbBuilder;

    @Resource(name = "passwordValidator")
    private PasswordValidator passwordValidator;

    @Resource(name = "profileValidator")
    private ProfileValidator profileValidator;

    @Resource(name = "emailValidator")
    private EmailValidator emailValidator;

    @Resource(name = "i18NFacade")
    private I18NFacade i18NFacade;

    @Resource(name = "addressVerificationFacade")
    private AddressVerificationFacade addressVerificationFacade;

    @Resource(name = "addressVerificationResultHandler")
    private AddressVerificationResultHandler addressVerificationResultHandler;

    @Resource(name = "orderGridFormFacade")
    private OrderGridFormFacade orderGridFormFacade;

    @Resource(name = "customerConsentDataStrategy")
    protected CustomerConsentDataStrategy customerConsentDataStrategy;

    @Resource(name = "addressDataUtil")
    private AddressDataUtil addressDataUtil;

    @Resource(name = "vmkStoreSessionFacade")
    private VMKStoreSessionFacade blueStoreSessionFacade;

    @Resource(name = "b2bUnitFacade")
    private VMKB2BUnitFacade b2bUnitFacade;

    @Resource(name = "vmkConsentFacade")
    private VMKConsentFacade consentFacade;

    @Resource(name = "vmkConsentParagraphComponentFacade")
    private VMKConsentParagraphComponentFacade consentParagraphComponentFacade;

    @Resource(name = "blueAddressConverter")
    private Converter<AddressData, BlueAddressForm> blueAddressConverter;

    @Resource(name = "blueReverseAddressConverter")
    private Converter<BlueAddressForm, AddressData> blueReverseAddressConverter;

    protected PasswordValidator getPasswordValidator() {
        return this.passwordValidator;
    }

    protected ProfileValidator getProfileValidator() {
        return this.profileValidator;
    }

    protected EmailValidator getEmailValidator() {
        return this.emailValidator;
    }

    protected I18NFacade getI18NFacade() {
        return this.i18NFacade;
    }

    protected AddressVerificationFacade getAddressVerificationFacade() {
        return this.addressVerificationFacade;
    }

    protected AddressVerificationResultHandler getAddressVerificationResultHandler() {
        return this.addressVerificationResultHandler;
    }

    @RequestMapping(value = "/addressform", method = RequestMethod.GET)
    @RequireHardLogIn
    public String getCountryAddressForm(@RequestParam("addressCode") final String addressCode, @RequestParam("countryIsoCode") final String countryIsoCode, final Model model) {
        final AddressForm addressForm = new AddressForm();
        model.addAttribute(ADDRESS_FORM_ATTR, addressForm);
        for (final AddressData addressData : this.userFacade.getAddressBook()) {
            if (addressData.getId() != null && addressData.getId().equals(addressCode)
                    && countryIsoCode.equals(addressData.getCountry().getIsocode())) {
                model.addAttribute(ADDRESS_DATA_ATTR, addressData);
                this.addressDataUtil.convert(addressData, addressForm);
                break;
            }
        }
        return COUNTRY_ADDRESS_FORM;
    }

    @RequestMapping(method = RequestMethod.GET)
    @RequireHardLogIn
    public String account(final Model model, final RedirectAttributes redirectModel) throws CMSItemNotFoundException {
        if (ResponsiveUtils.isResponsive()) {
            GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "system.error.page.not.found", null);
            return REDIRECT_PREFIX + "/";
        }
        storeCmsPageInModel(model, getContentPageForLabelOrId(ACCOUNT_CMS_PAGE));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ACCOUNT_CMS_PAGE));
        model.addAttribute(BREADCRUMBS_ATTR, this.accountBreadcrumbBuilder.getBreadcrumbs(null));
        model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
        return getViewForPage(model);
    }

    @RequestMapping(value = "/orders", method = RequestMethod.GET)
    @RequireHardLogIn
    public String orders(@RequestParam(value = "page", defaultValue = "0") final int page,
                         @RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode,
                         @RequestParam(value = "sort", required = false) final String sortCode, final Model model)
            throws CMSItemNotFoundException {
        // Handle paged search results
        final PageableData pageableData = createPageableData(page, 5, sortCode, showMode);
        final SearchPageData<OrderHistoryData> searchPageData = this.orderFacade.getPagedOrderHistoryForStatuses(pageableData);
        populateModel(model, searchPageData, showMode);

        storeCmsPageInModel(model, getContentPageForLabelOrId(ORDER_HISTORY_CMS_PAGE));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ORDER_HISTORY_CMS_PAGE));
        model.addAttribute(BREADCRUMBS_ATTR, this.accountBreadcrumbBuilder.getBreadcrumbs("text.account.orderHistory"));
        model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
        return getViewForPage(model);
    }

    @RequestMapping(value = "/order/" + ORDER_CODE_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
    @RequireHardLogIn
    public String order(@PathVariable("orderCode") final String orderCode, final Model model,
                        final RedirectAttributes redirectModel) throws CMSItemNotFoundException {
        try {
            final OrderData orderDetails = this.orderFacade.getOrderDetailsForCode(orderCode);
            model.addAttribute("orderData", orderDetails);

            final List<Breadcrumb> breadcrumbs = this.accountBreadcrumbBuilder.getBreadcrumbs(null);
            breadcrumbs.add(new Breadcrumb("/my-account/orders", getMessageSource().getMessage("text.account.orderHistory", null,
                    getI18nService().getCurrentLocale()), null));
            breadcrumbs.add(new Breadcrumb("#", getMessageSource().getMessage("text.account.order.orderBreadcrumb", new Object[]
                    {orderDetails.getCode()}, "Order {0}", getI18nService().getCurrentLocale()), null));
            model.addAttribute(BREADCRUMBS_ATTR, breadcrumbs);

        } catch (final UnknownIdentifierException e) {
            LOG.warn("Attempted to load a order that does not exist or is not visible", e);
            GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "system.error.page.not.found", null);
            return REDIRECT_TO_ORDER_HISTORY_PAGE;
        }
        storeCmsPageInModel(model, getContentPageForLabelOrId(ORDER_DETAIL_CMS_PAGE));
        model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ORDER_DETAIL_CMS_PAGE));
        return getViewForPage(model);
    }

    @RequestMapping(value = "/order/" + ORDER_CODE_PATH_VARIABLE_PATTERN + "/getReadOnlyProductVariantMatrix", method = RequestMethod.GET)
    @RequireHardLogIn
    public String getProductVariantMatrixForResponsive(@PathVariable("orderCode") final String orderCode,
                                                       @RequestParam("productCode") final String productCode, final Model model) {
        final OrderData orderData = this.orderFacade.getOrderDetailsForCodeWithoutUser(orderCode);

        final Map<String, ReadOnlyOrderGridData> readOnlyMultiDMap = this.orderGridFormFacade.getReadOnlyOrderGridForProductInOrder(
                productCode, Arrays.asList(ProductOption.BASIC, ProductOption.CATEGORIES), orderData);
        model.addAttribute("readOnlyMultiDMap", readOnlyMultiDMap);

        return READ_ONLY_EXPANDED_ORDER_FORM;
    }

    @RequestMapping(value = "/profile", method = RequestMethod.GET)
    @RequireHardLogIn
    public String profile(final Model model) throws CMSItemNotFoundException {
        final CustomerData customerData = this.customerFacade.getCurrentCustomer();

        model.addAttribute("customerData", customerData);

        storeCmsPageInModel(model, getContentPageForLabelOrId(PROFILE_CMS_PAGE));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(PROFILE_CMS_PAGE));
        model.addAttribute(BREADCRUMBS_ATTR, this.accountBreadcrumbBuilder.getBreadcrumbs(TEXT_ACCOUNT_PROFILE));
        model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
        return getViewForPage(model);
    }

    @GetMapping(value = "/consent-management")
    @RequireHardLogIn
    public String consents(Model model) throws CMSItemNotFoundException {
        storeCmsPageInModel(model, getContentPageForLabelOrId(CONSENT_MGMT_CMS_PAGE));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(CONSENT_MGMT_CMS_PAGE));
        model.addAttribute(BREADCRUMBS_ATTR, this.accountBreadcrumbBuilder.getBreadcrumbs(TEXT_ACCOUNT_CONSENT_MANAGEMENT));
        model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
        model.addAttribute("consents", consentFacade.getConsents());
        model.addAttribute("consentParagraphs", consentParagraphComponentFacade.getConsentParagraphs());
        return getViewForPage(model);
    }

    @RequestMapping(value = "/update-email", method = RequestMethod.GET)
    @RequireHardLogIn
    public String editEmail(final Model model) throws CMSItemNotFoundException {
        final CustomerData customerData = this.customerFacade.getCurrentCustomer();
        final UpdateEmailForm updateEmailForm = new UpdateEmailForm();

        updateEmailForm.setEmail(customerData.getDisplayUid());

        model.addAttribute("updateEmailForm", updateEmailForm);

        storeCmsPageInModel(model, getContentPageForLabelOrId(UPDATE_EMAIL_CMS_PAGE));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(UPDATE_EMAIL_CMS_PAGE));
        model.addAttribute(BREADCRUMBS_ATTR, this.accountBreadcrumbBuilder.getBreadcrumbs(TEXT_ACCOUNT_PROFILE));
        model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
        return getViewForPage(model);
    }

    @RequestMapping(value = "/update-email", method = RequestMethod.POST)
    @RequireHardLogIn
    public String updateEmail(final UpdateEmailForm updateEmailForm, final BindingResult bindingResult, final Model model,
                              final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException {
        getEmailValidator().validate(updateEmailForm, bindingResult);
        String returnAction = REDIRECT_TO_UPDATE_EMAIL_PAGE;

        if (!bindingResult.hasErrors() && !updateEmailForm.getEmail().equals(updateEmailForm.getChkEmail())) {
            bindingResult.rejectValue("chkEmail", "validation.checkEmail.equals", new Object[]{}, "validation.checkEmail.equals");
        }

        if (bindingResult.hasErrors()) {
            returnAction = setErrorMessagesAndCMSPage(model, UPDATE_EMAIL_CMS_PAGE);
        } else {
            try {
                this.customerFacade.changeUid(updateEmailForm.getEmail(), updateEmailForm.getPassword());
                GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.CONF_MESSAGES_HOLDER,
                        "text.account.profile.confirmationUpdated", null);

                // Replace the spring security authentication with the new UID
                final String newUid = this.customerFacade.getCurrentCustomer().getUid().toLowerCase();
                final Authentication oldAuthentication = SecurityContextHolder.getContext().getAuthentication();
                final UsernamePasswordAuthenticationToken newAuthentication = new UsernamePasswordAuthenticationToken(newUid, null,
                        oldAuthentication.getAuthorities());
                newAuthentication.setDetails(oldAuthentication.getDetails());
                SecurityContextHolder.getContext().setAuthentication(newAuthentication);
            } catch (final DuplicateUidException e) {
                bindingResult.rejectValue("email", "profile.email.unique");
                returnAction = setErrorMessagesAndCMSPage(model, UPDATE_EMAIL_CMS_PAGE);
            } catch (final PasswordMismatchException passwordMismatchException) {
                bindingResult.rejectValue("password", PROFILE_CURRENT_PASSWORD_INVALID);
                returnAction = setErrorMessagesAndCMSPage(model, UPDATE_EMAIL_CMS_PAGE);
            }
        }

        return returnAction;
    }

    protected String setErrorMessagesAndCMSPage(final Model model, final String cmsPageLabelOrId) throws CMSItemNotFoundException {
        GlobalMessages.addErrorMessage(model, FORM_GLOBAL_ERROR);
        storeCmsPageInModel(model, getContentPageForLabelOrId(cmsPageLabelOrId));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(cmsPageLabelOrId));
        model.addAttribute(BREADCRUMBS_ATTR, this.accountBreadcrumbBuilder.getBreadcrumbs(TEXT_ACCOUNT_PROFILE));
        return getViewForPage(model);
    }


    @RequestMapping(value = "/update-profile", method = RequestMethod.GET)
    @RequireHardLogIn
    public String editProfile(final Model model) throws CMSItemNotFoundException {
        final CustomerData customerData = this.customerFacade.getCurrentCustomer();
        final UpdateProfileForm updateProfileForm = new UpdateProfileForm();

        updateProfileForm.setTitleCode(customerData.getTitleCode());
        updateProfileForm.setFirstName(customerData.getFirstName());
        updateProfileForm.setLastName(customerData.getLastName());

        model.addAttribute("updateProfileForm", updateProfileForm);

        storeCmsPageInModel(model, getContentPageForLabelOrId(UPDATE_PROFILE_CMS_PAGE));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(UPDATE_PROFILE_CMS_PAGE));

        model.addAttribute(BREADCRUMBS_ATTR, this.accountBreadcrumbBuilder.getBreadcrumbs(TEXT_ACCOUNT_PROFILE));
        model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
        return getViewForPage(model);
    }

    @RequestMapping(value = "/update-profile", method = RequestMethod.POST)
    @RequireHardLogIn
    public String updateProfile(final UpdateProfileForm updateProfileForm, final BindingResult bindingResult, final Model model,
                                final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException {
        getProfileValidator().validate(updateProfileForm, bindingResult);

        String returnAction = REDIRECT_TO_UPDATE_PROFILE;
        final CustomerData currentCustomerData = this.customerFacade.getCurrentCustomer();
        final CustomerData customerData = new CustomerData();
        customerData.setTitleCode(updateProfileForm.getTitleCode());
        customerData.setFirstName(updateProfileForm.getFirstName());
        customerData.setLastName(updateProfileForm.getLastName());
        customerData.setUid(currentCustomerData.getUid());
        customerData.setDisplayUid(currentCustomerData.getDisplayUid());

        storeCmsPageInModel(model, getContentPageForLabelOrId(UPDATE_PROFILE_CMS_PAGE));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(UPDATE_PROFILE_CMS_PAGE));

        if (bindingResult.hasErrors()) {
            returnAction = setErrorMessagesAndCMSPage(model, UPDATE_PROFILE_CMS_PAGE);
        } else {
            try {
                this.customerFacade.updateProfile(customerData);
                GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.CONF_MESSAGES_HOLDER,
                        "text.account.profile.confirmationUpdated", null);

            } catch (final DuplicateUidException e) {
                bindingResult.rejectValue("email", "registration.error.account.exists.title");
                returnAction = setErrorMessagesAndCMSPage(model, UPDATE_PROFILE_CMS_PAGE);
            }
        }


        model.addAttribute(BREADCRUMBS_ATTR, this.accountBreadcrumbBuilder.getBreadcrumbs(TEXT_ACCOUNT_PROFILE));
        return returnAction;
    }

    @RequestMapping(value = "/update-password", method = RequestMethod.GET)
    @RequireHardLogIn
    public String updatePassword(final Model model) throws CMSItemNotFoundException {
        final UpdatePasswordForm updatePasswordForm = new UpdatePasswordForm();

        model.addAttribute("updatePasswordForm", updatePasswordForm);

        storeCmsPageInModel(model, getContentPageForLabelOrId(UPDATE_PASSWORD_CMS_PAGE));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(UPDATE_PASSWORD_CMS_PAGE));

        model.addAttribute(BREADCRUMBS_ATTR, this.accountBreadcrumbBuilder.getBreadcrumbs("text.account.profile.updatePasswordForm"));
        model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
        return getViewForPage(model);
    }

    @RequestMapping(value = "/update-password", method = RequestMethod.POST)
    @RequireHardLogIn
    public String updatePassword(final UpdatePasswordForm updatePasswordForm, final BindingResult bindingResult,
                                 final Model model, final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException {
        getPasswordValidator().validate(updatePasswordForm, bindingResult);
        if (!bindingResult.hasErrors()) {
            if (updatePasswordForm.getNewPassword().equals(updatePasswordForm.getCheckNewPassword())) {
                try {
                    this.customerFacade.changePassword(updatePasswordForm.getCurrentPassword(), updatePasswordForm.getNewPassword());
                } catch (final PasswordMismatchException localException) {
                    bindingResult.rejectValue("currentPassword", PROFILE_CURRENT_PASSWORD_INVALID, new Object[]{}, PROFILE_CURRENT_PASSWORD_INVALID);
                }
            } else {
                bindingResult.rejectValue("checkNewPassword", "validation.checkPwd.equals", new Object[]{}, "validation.checkPwd.equals");
            }
        }

        if (bindingResult.hasErrors()) {
            GlobalMessages.addErrorMessage(model, FORM_GLOBAL_ERROR);
            storeCmsPageInModel(model, getContentPageForLabelOrId(UPDATE_PASSWORD_CMS_PAGE));
            setUpMetaDataForContentPage(model, getContentPageForLabelOrId(UPDATE_PASSWORD_CMS_PAGE));

            model.addAttribute(BREADCRUMBS_ATTR, this.accountBreadcrumbBuilder.getBreadcrumbs("text.account.profile.updatePasswordForm"));
            return getViewForPage(model);
        } else {
            GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.CONF_MESSAGES_HOLDER, "text.account.confirmation.password.updated", null);
            return REDIRECT_TO_PASSWORD_UPDATE_PAGE;
        }
    }

    @RequestMapping(value = "/address-book", method = RequestMethod.GET)
    @RequireHardLogIn
    public String getAddressBook(final Model model) throws CMSItemNotFoundException {
        model.addAttribute(ADDRESS_DATA_ATTR, this.b2bUnitFacade.getB2BUnitAddressBook());

        storeCmsPageInModel(model, getContentPageForLabelOrId(ADDRESS_BOOK_CMS_PAGE));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ADDRESS_BOOK_CMS_PAGE));
        model.addAttribute(BREADCRUMBS_ATTR, this.accountBreadcrumbBuilder.getBreadcrumbs(TEXT_ACCOUNT_ADDRESS_BOOK));
        model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
        return getViewForPage(model);
    }

    @RequestMapping(value = "/add-address", method = RequestMethod.GET)
    @RequireHardLogIn
    public String addAddress(Model model) throws CMSItemNotFoundException {
        model.addAttribute(ADDRESS_FORM_ATTR, getPreparedAddressForm());
        model.addAttribute(ADDRESS_BOOK_EMPTY_ATTR, CollectionUtils.isEmpty(this.b2bUnitFacade.getB2BUnitAddressBook()));
        model.addAttribute(IS_DEFAULT_ADDRESS_ATTR, Boolean.FALSE);
        storeCmsPageInModel(model, getContentPageForLabelOrId(ADD_EDIT_ADDRESS_CMS_PAGE));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ADD_EDIT_ADDRESS_CMS_PAGE));

        List<Breadcrumb> breadcrumbs = this.accountBreadcrumbBuilder.getBreadcrumbs(null);
        breadcrumbs.add(new Breadcrumb(MY_ACCOUNT_ADDRESS_BOOK_URL, getMessageSource().getMessage(TEXT_ACCOUNT_ADDRESS_BOOK, null, getI18nService().getCurrentLocale()), null));
        breadcrumbs.add(new Breadcrumb("#", getMessageSource().getMessage("text.account.addressBook.addEditAddress", null, getI18nService().getCurrentLocale()), null));
        model.addAttribute(BREADCRUMBS_ATTR, breadcrumbs);
        model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
        return getViewForPage(model);
    }

    protected BlueAddressForm getPreparedAddressForm() {
        CustomerData currentCustomerData = this.customerFacade.getCurrentCustomer();
        BlueAddressForm addressForm = new BlueAddressForm();
        CountryData currentCountry = this.blueStoreSessionFacade.getCurrentSite().getCountry();
        addressForm.setCountryIso(currentCountry.getIsocode());
        return addressForm;
    }

    @RequestMapping(value = "/add-address", method = RequestMethod.POST)
    @RequireHardLogIn
    public String addAddress(BlueAddressForm addressForm, Model model, RedirectAttributes redirectModel) {
        AddressData newAddress = blueReverseAddressConverter.convert(addressForm);
        newAddress.setVisibleInAddressBook(true);

        if (CollectionUtils.isEmpty(this.b2bUnitFacade.getB2BUnitAddressBook())) {
            newAddress.setDefaultAddress(true);
        } else {
            newAddress.setDefaultAddress(addressForm.getDefaultAddress() != null && addressForm.getDefaultAddress());
        }

        model.addAttribute("edit", Boolean.TRUE);
        model.addAttribute(IS_DEFAULT_ADDRESS_ATTR, this.b2bUnitFacade.isDefaultDeliveryAddress(addressForm.getAddressId()));

        this.b2bUnitFacade.addDeliveryAddressToUnit(newAddress);

        GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, "account.confirmation.address.added", null);
        return REDIRECT_PREFIX + MY_ACCOUNT_ADDRESS_BOOK_URL;
    }

    protected void setUpAddressFormAfterError(final AddressForm addressForm, final Model model) {
        model.addAttribute(ADDRESS_BOOK_EMPTY_ATTR, CollectionUtils.isEmpty(this.userFacade.getAddressBook()));
        model.addAttribute(IS_DEFAULT_ADDRESS_ATTR, this.userFacade.isDefaultAddress(addressForm.getAddressId()));

    }

    @RequestMapping(value = "/edit-address/" + ADDRESS_CODE_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
    @RequireHardLogIn
    public String editAddress(@PathVariable("addressCode") String addressCode, Model model) throws CMSItemNotFoundException {
        BlueAddressForm addressForm = new BlueAddressForm();
        model.addAttribute(ADDRESS_FORM_ATTR, addressForm);

        List<AddressData> addressBook = this.b2bUnitFacade.getB2BUnitAddressBook();
        model.addAttribute(ADDRESS_BOOK_EMPTY_ATTR, CollectionUtils.isEmpty(addressBook));

        for (final AddressData addressData : addressBook) {
            if (addressData.getId() != null && addressData.getId().equals(addressCode)) {
                model.addAttribute(ADDRESS_DATA_ATTR, addressData);

                blueAddressConverter.convert(addressData, addressForm);

                if (this.b2bUnitFacade.isDefaultDeliveryAddress(addressData.getId())) {
                    addressForm.setDefaultAddress(Boolean.TRUE);
                    model.addAttribute(IS_DEFAULT_ADDRESS_ATTR, Boolean.TRUE);
                } else {
                    addressForm.setDefaultAddress(Boolean.FALSE);
                    model.addAttribute(IS_DEFAULT_ADDRESS_ATTR, Boolean.FALSE);
                }
                break;
            }
        }

        storeCmsPageInModel(model, getContentPageForLabelOrId(ADD_EDIT_ADDRESS_CMS_PAGE));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ADD_EDIT_ADDRESS_CMS_PAGE));

        final List<Breadcrumb> breadcrumbs = this.accountBreadcrumbBuilder.getBreadcrumbs(null);
        breadcrumbs.add(new Breadcrumb(MY_ACCOUNT_ADDRESS_BOOK_URL, getMessageSource().getMessage(TEXT_ACCOUNT_ADDRESS_BOOK, null, getI18nService().getCurrentLocale()), null));
        breadcrumbs.add(new Breadcrumb("#", getMessageSource().getMessage("text.account.addressBook.addEditAddress", null, getI18nService().getCurrentLocale()), null));
        model.addAttribute(BREADCRUMBS_ATTR, breadcrumbs);
        model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
        model.addAttribute("edit", Boolean.TRUE);
        return getViewForPage(model);
    }

    @RequestMapping(value = "/edit-address/" + ADDRESS_CODE_PATH_VARIABLE_PATTERN, method = RequestMethod.POST)
    @RequireHardLogIn
    public String editAddress(BlueAddressForm addressForm, Model model, RedirectAttributes redirectModel) {
        model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);

        AddressData newAddress = blueReverseAddressConverter.convert(addressForm);
        newAddress.setVisibleInAddressBook(true);

        if (Boolean.TRUE.equals(addressForm.getDefaultAddress()) || this.b2bUnitFacade.getB2BUnitAddressBook().size() <= 1) {
            newAddress.setDefaultAddress(true);
        }

        model.addAttribute("edit", Boolean.TRUE);
        model.addAttribute(IS_DEFAULT_ADDRESS_ATTR, this.b2bUnitFacade.isDefaultDeliveryAddress(addressForm.getAddressId()));

        this.b2bUnitFacade.editDeliveryAddress(newAddress);

        GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, "account.confirmation.address.updated", null);
        return REDIRECT_TO_EDIT_ADDRESS_PAGE + newAddress.getId();
    }

    @RequestMapping(value = "/select-suggested-address", method = RequestMethod.POST)
    public String doSelectSuggestedAddress(final AddressForm addressForm, final RedirectAttributes redirectModel) {
        final Set<String> resolveCountryRegions = org.springframework.util.StringUtils
                .commaDelimitedListToSet(Config.getParameter("resolve.country.regions"));

        final AddressData selectedAddress = this.addressDataUtil.convertToVisibleAddressData(addressForm);

        final CountryData countryData = selectedAddress.getCountry();

        if (!resolveCountryRegions.contains(countryData.getIsocode())) {
            selectedAddress.setRegion(null);
        }

        if (Boolean.TRUE.equals(addressForm.getEditAddress())) {
            this.userFacade.editAddress(selectedAddress);
        } else {
            this.userFacade.addAddress(selectedAddress);
        }

        GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, "account.confirmation.address.added");

        return REDIRECT_TO_ADDRESS_BOOK_PAGE;
    }

    @RequestMapping(value = "/remove-address/" + ADDRESS_CODE_PATH_VARIABLE_PATTERN, method = {RequestMethod.GET, RequestMethod.POST})
    @RequireHardLogIn
    public String removeAddress(@PathVariable("addressCode") final String addressCode, final RedirectAttributes redirectModel) {
        final AddressData addressData = new AddressData();
        addressData.setId(addressCode);
        this.b2bUnitFacade.removeDeliveryAddress(addressData);

        GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, "account.confirmation.address.removed");
        return REDIRECT_TO_ADDRESS_BOOK_PAGE;
    }

    @RequestMapping(value = "/set-default-address/" + ADDRESS_CODE_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
    @RequireHardLogIn
    public String setDefaultAddress(@PathVariable("addressCode") final String addressCode, final RedirectAttributes redirectModel) {
        final AddressData addressData = new AddressData();
        addressData.setDefaultAddress(true);
        addressData.setVisibleInAddressBook(true);
        addressData.setId(addressCode);
        this.b2bUnitFacade.setDefaultDeliveryAddress(addressData);
        GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER,
                "account.confirmation.default.address.changed");
        return REDIRECT_TO_ADDRESS_BOOK_PAGE;
    }

    @RequestMapping(value = "/payment-details", method = RequestMethod.GET)
    @RequireHardLogIn
    public String paymentDetails(final Model model) throws CMSItemNotFoundException {
        model.addAttribute("customerData", this.customerFacade.getCurrentCustomer());
        model.addAttribute("paymentInfoData", this.userFacade.getCCPaymentInfos(true));
        storeCmsPageInModel(model, getContentPageForLabelOrId(PAYMENT_DETAILS_CMS_PAGE));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ADD_EDIT_ADDRESS_CMS_PAGE));
        model.addAttribute(BREADCRUMBS_ATTR, this.accountBreadcrumbBuilder.getBreadcrumbs("text.account.paymentDetails"));
        model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
        return getViewForPage(model);
    }

    @RequestMapping(value = "/set-default-payment-details", method = RequestMethod.POST)
    @RequireHardLogIn
    public String setDefaultPaymentDetails(@RequestParam final String paymentInfoId) {
        CCPaymentInfoData paymentInfoData = null;
        if (StringUtils.isNotBlank(paymentInfoId)) {
            paymentInfoData = this.userFacade.getCCPaymentInfoForCode(paymentInfoId);
        }
        this.userFacade.setDefaultPaymentInfo(paymentInfoData);
        return REDIRECT_TO_PAYMENT_INFO_PAGE;
    }

    @RequestMapping(value = "/remove-payment-method", method = RequestMethod.POST)
    @RequireHardLogIn
    public String removePaymentMethod(@RequestParam(value = "paymentInfoId") final String paymentMethodId,
                                      final RedirectAttributes redirectAttributes) {
        this.userFacade.unlinkCCPaymentInfo(paymentMethodId);
        GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.CONF_MESSAGES_HOLDER,
                "text.account.profile.paymentCart.removed");
        return REDIRECT_TO_PAYMENT_INFO_PAGE;
    }

    @RequestMapping(value = "/close-account", method = RequestMethod.GET)
    @RequireHardLogIn
    public String showCloseAccountPage(final Model model) throws CMSItemNotFoundException {
        storeCmsPageInModel(model, getContentPageForLabelOrId(CLOSE_ACCOUNT_CMS_PAGE));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(CLOSE_ACCOUNT_CMS_PAGE));
        model.addAttribute(BREADCRUMBS_ATTR, this.accountBreadcrumbBuilder.getBreadcrumbs(TEXT_ACCOUNT_CLOSE));
        model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
        return getViewForPage(model);
    }

    @RequestMapping(value = "/close-account", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    @RequireHardLogIn
    public void closeAccount(final HttpServletRequest request) throws ServletException {
        this.customerFacade.closeAccount();
        request.logout();
    }
}
