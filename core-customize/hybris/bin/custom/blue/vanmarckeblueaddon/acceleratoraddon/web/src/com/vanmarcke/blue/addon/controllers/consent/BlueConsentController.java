package com.vanmarcke.blue.addon.controllers.consent;

import com.vanmarcke.facades.consent.VMKConsentFacade;
import com.vanmarcke.facades.data.VMKConsentData;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Controller for handling consents.
 *
 * @author Tom van den Berg
 * @since 17-6-2020
 */
@Controller
@RequestMapping("/consents")
public class BlueConsentController {

    private final VMKConsentFacade consentFacade;

    /**
     * Provides an instance of the {@code BlueConsentController}.
     *
     * @param consentFacade the consent facade
     */
    public BlueConsentController(VMKConsentFacade consentFacade) {
        this.consentFacade = consentFacade;
    }

    /**
     * Handles the incoming POST request to save the given {code consents}.
     *
     * @param consents the consents
     * @param request  the request
     */
    @PostMapping
    public ResponseEntity<?> saveConsents(@RequestBody List<VMKConsentData> consents, HttpServletRequest request) {
        consentFacade.process(consents, request);
        return ResponseEntity.ok().build();
    }

    /**
     * Returns the consents for the current user.
     *
     * @return the consents
     */
    @GetMapping
    public ResponseEntity<String> getConsents() {
        return ResponseEntity.ok(consentFacade.getConsentsAsString());
    }
}
