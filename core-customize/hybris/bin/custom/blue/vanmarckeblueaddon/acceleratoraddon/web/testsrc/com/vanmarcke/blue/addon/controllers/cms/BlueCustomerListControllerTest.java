package com.vanmarcke.blue.addon.controllers.cms;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractSearchPageController;
import de.hybris.platform.assistedservicefacades.AssistedServiceFacade;
import de.hybris.platform.assistedserviceservices.utils.AssistedServiceSession;
import de.hybris.platform.commercefacades.customer.CustomerListFacade;
import de.hybris.platform.commercefacades.user.data.CustomerListData;
import de.hybris.platform.commercefacades.user.data.UserGroupData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.PaginationData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.core.model.user.UserModel;
import org.apache.commons.lang3.RandomStringUtils;
import org.fest.assertions.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.ui.Model;

import javax.servlet.http.HttpServletResponse;
import java.util.*;

import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class BlueCustomerListControllerTest {

    private static final String UID_AGENT = RandomStringUtils.randomAlphabetic(10);
    private static final String UID_USER = RandomStringUtils.randomAlphabetic(10);
    private static final String UID_CUSTOMER_LIST = RandomStringUtils.randomAlphabetic(10);

    private static final String COMPONENT_PATH = "addon:/vanmarckeblueaddon/asm/fragments/customer360/asmCustomerListComponent";
    private static final String TABLE_PATH = "addon:/vanmarckeblueaddon/asm/fragments/customer360/asmCustomerListTable";
    private static final String FLASH_ERROR_MSG = "asm.emulate.error.agent_missed";

    private static final String DEFAULT_CUSTOMER_LIST = "defaultList";
    private static final String AVAILABLE_CUSTOMER_LIST = "availableLists";
    private static final String CUSTOMER_LIST_DATA = "customerListData";
    private static final String QUERY = "query";
    private static final String A_QUERY = RandomStringUtils.randomAlphabetic(10);


    private static final int PAGE_NUMBER = new Random().nextInt(10);
    private static final int PAGE_SIZE = new Random().nextInt(10);
    private static final String SORT_CODE = RandomStringUtils.randomAlphabetic(10);

    private static final long LOWER_NUMBER = new Random().nextInt(10);
    private static final long HIGHER_NUMBER = LOWER_NUMBER + 5;

    private static final AbstractSearchPageController.ShowMode SHOWMODE_PAGE = AbstractSearchPageController.ShowMode.Page;
    private static final AbstractSearchPageController.ShowMode SHOWMODE_ALL = AbstractSearchPageController.ShowMode.All;

    @Captor
    private ArgumentCaptor<Map<String, Object>> parametersMapCaptor;

    @Mock(name = "customerListFacade")
    private CustomerListFacade customerListFacade;

    @Mock(name = "assistedServiceFacade")
    private AssistedServiceFacade assistedServiceFacade;

    @Spy
    @InjectMocks
    private BlueCustomerListController controller;

    @Test
    public void getCustomersListPopupLoggedInAgent() {
        Model model = mock(Model.class);
        HttpServletResponse response = mock(HttpServletResponse.class);

        AssistedServiceSession session = mock(AssistedServiceSession.class);
        UserModel agent = mock(UserModel.class);

        when(assistedServiceFacade.isAssistedServiceAgentLoggedIn()).thenReturn(true);
        when(assistedServiceFacade.getAsmSession()).thenReturn(session);
        when(session.getAgent()).thenReturn(agent);
        when(agent.getUid()).thenReturn(UID_AGENT);

        UserGroupData userGroupData = new UserGroupData();
        userGroupData.setUid(UID_USER);
        List<UserGroupData> customerLists = new ArrayList<>();
        customerLists.add(userGroupData);

        when(customerListFacade.getCustomerListsForEmployee(UID_AGENT)).thenReturn(customerLists);

        String actual = controller.getCustomersListPopup(model, response);

        Assertions
                .assertThat(actual)
                .isEqualTo(COMPONENT_PATH);

        verify(model).addAttribute(AVAILABLE_CUSTOMER_LIST, customerLists);
        verify(model).addAttribute(DEFAULT_CUSTOMER_LIST, UID_USER);
        verify(assistedServiceFacade).isAssistedServiceAgentLoggedIn();
        verify(assistedServiceFacade).getAsmSession();
        verify(customerListFacade).getCustomerListsForEmployee(UID_AGENT);
    }

    @Test
    public void getCustomersListPopupNotLoggedInAgent() {
        Model model = mock(Model.class);
        HttpServletResponse response = mock(HttpServletResponse.class);

        AssistedServiceSession session = mock(AssistedServiceSession.class);

        when(assistedServiceFacade.isAssistedServiceAgentLoggedIn()).thenReturn(false);
        when(assistedServiceFacade.getAsmSession()).thenReturn(session);

        String actual = controller.getCustomersListPopup(model, response);

        Assertions
                .assertThat(actual)
                .isNull();

        verify(session).setFlashErrorMessage(FLASH_ERROR_MSG);
        verify(response).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        verify(assistedServiceFacade).isAssistedServiceAgentLoggedIn();
        verify(assistedServiceFacade).getAsmSession();
        verifyZeroInteractions(model, customerListFacade);
    }

    @Test
    public void getCustomersListPopupLoggedInAgentNoCustomers() {
        Model model = mock(Model.class);
        HttpServletResponse response = mock(HttpServletResponse.class);

        AssistedServiceSession session = mock(AssistedServiceSession.class);
        UserModel agent = mock(UserModel.class);

        when(assistedServiceFacade.isAssistedServiceAgentLoggedIn()).thenReturn(true);
        when(assistedServiceFacade.getAsmSession()).thenReturn(session);
        when(session.getAgent()).thenReturn(agent);
        when(agent.getUid()).thenReturn(UID_AGENT);


        when(customerListFacade.getCustomerListsForEmployee(UID_AGENT)).thenReturn(Collections.emptyList());

        String actual = controller.getCustomersListPopup(model, response);

        Assertions
                .assertThat(actual)
                .isEqualTo(COMPONENT_PATH);

        verify(assistedServiceFacade).isAssistedServiceAgentLoggedIn();
        verify(assistedServiceFacade).getAsmSession();
        verify(customerListFacade).getCustomerListsForEmployee(UID_AGENT);
        verifyZeroInteractions(model);
    }

    @Test
    public void testPopulateModel() {
        Model model = mock(Model.class);
        SearchPageData<?> searchPageData = mock(SearchPageData.class);
        AbstractSearchPageController.ShowMode showMode = SHOWMODE_ALL;
        PaginationData paginationData = mock(PaginationData.class);

        when(searchPageData.getPagination()).thenReturn(paginationData);
        when(paginationData.getNumberOfPages()).thenReturn(0);
        when(paginationData.getPageSize()).thenReturn((int) HIGHER_NUMBER);
        when(controller.getMaxSearchPageSize()).thenReturn((int) LOWER_NUMBER);

        controller.populateModel(model, searchPageData, showMode);

        verify(model).addAttribute("numberPagesShown", 3);
        verify(model).addAttribute("searchPageData", searchPageData);
        verify(model).addAttribute("isShowAllAllowed", controller.calculateShowAll(searchPageData, showMode));
        verify(model).addAttribute("isShowPageAllowed", controller.calculateShowPaged(searchPageData, showMode));
    }

    @Test
    public void testCreatePageableDataShowAll() {
        AbstractSearchPageController.ShowMode showMode = SHOWMODE_ALL;

        PageableData result = controller.createPageableData(PAGE_NUMBER, PAGE_SIZE, SORT_CODE, showMode);

        Assertions
                .assertThat(result.getPageSize())
                .isEqualTo(100);

        Assertions
                .assertThat(result.getCurrentPage())
                .isEqualTo(PAGE_NUMBER);

        Assertions
                .assertThat(result.getSort())
                .isEqualTo(SORT_CODE);
    }

    @Test
    public void testCreatePageableData() {
        AbstractSearchPageController.ShowMode showMode = SHOWMODE_PAGE;

        PageableData result = controller.createPageableData(PAGE_NUMBER, PAGE_SIZE, SORT_CODE, showMode);

        Assertions
                .assertThat(result.getPageSize())
                .isEqualTo(PAGE_SIZE);

        Assertions
                .assertThat(result.getCurrentPage())
                .isEqualTo(PAGE_NUMBER);

        Assertions
                .assertThat(result.getSort())
                .isEqualTo(SORT_CODE);
    }

    @Test
    public void testCalculateShowAll_FirstCase() {
        SearchPageData<?> searchPageData = mock(SearchPageData.class);
        AbstractSearchPageController.ShowMode showMode = SHOWMODE_ALL;

        boolean result = controller.calculateShowAll(searchPageData, showMode);

        Assertions
                .assertThat(result)
                .isFalse();
    }

    @Test
    public void testCalculateShowAll__SecondCase() {
        SearchPageData<?> searchPageData = mock(SearchPageData.class);
        AbstractSearchPageController.ShowMode showMode = SHOWMODE_PAGE;
        PaginationData paginationData = mock(PaginationData.class);

        when(searchPageData.getPagination()).thenReturn(paginationData);
        when(paginationData.getTotalNumberOfResults()).thenReturn(LOWER_NUMBER);
        when(paginationData.getPageSize()).thenReturn((int) HIGHER_NUMBER);

        boolean result = controller.calculateShowAll(searchPageData, showMode);

        Assertions
                .assertThat(result)
                .isFalse();
    }

    @Test
    public void testCalculateShowAll_ThirdCase() {
        SearchPageData<?> searchPageData = mock(SearchPageData.class);
        AbstractSearchPageController.ShowMode showMode = SHOWMODE_PAGE;
        PaginationData paginationData = mock(PaginationData.class);

        when(searchPageData.getPagination()).thenReturn(paginationData);
        when(paginationData.getTotalNumberOfResults()).thenReturn(HIGHER_NUMBER);
        when(paginationData.getPageSize()).thenReturn((int) LOWER_NUMBER);
        doReturn(false).when(controller).isShowAllAllowed(searchPageData);

        boolean result = controller.calculateShowAll(searchPageData, showMode);

        Assertions
                .assertThat(result)
                .isFalse();
    }

    @Test
    public void testCalculateShowAll_SuccessCase() {
        SearchPageData<?> searchPageData = mock(SearchPageData.class);
        AbstractSearchPageController.ShowMode showMode = SHOWMODE_PAGE;
        PaginationData paginationData = mock(PaginationData.class);

        when(searchPageData.getPagination()).thenReturn(paginationData);
        when(paginationData.getTotalNumberOfResults()).thenReturn(HIGHER_NUMBER);
        when(paginationData.getPageSize()).thenReturn((int) LOWER_NUMBER);
        doReturn(true).when(controller).isShowAllAllowed(searchPageData);

        boolean result = controller.calculateShowAll(searchPageData, showMode);

        Assertions
                .assertThat(result)
                .isTrue();
    }

    @Test
    public void testCalculateShowPaged_FirstCase() {
        SearchPageData<?> searchPageData = mock(SearchPageData.class);
        AbstractSearchPageController.ShowMode showMode = SHOWMODE_PAGE;

        boolean result = controller.calculateShowPaged(searchPageData, showMode);

        Assertions
                .assertThat(result)
                .isFalse();
    }

    @Test
    public void testCalculateShowPaged__SecondCase() {
        SearchPageData<?> searchPageData = mock(SearchPageData.class);
        AbstractSearchPageController.ShowMode showMode = SHOWMODE_ALL;
        PaginationData paginationData = mock(PaginationData.class);

        when(searchPageData.getPagination()).thenReturn(paginationData);
        when(paginationData.getNumberOfPages()).thenReturn(0);
        when(paginationData.getPageSize()).thenReturn((int) HIGHER_NUMBER);
        when(controller.getMaxSearchPageSize()).thenReturn((int) LOWER_NUMBER);

        boolean result = controller.calculateShowPaged(searchPageData, showMode);

        Assertions
                .assertThat(result)
                .isFalse();
    }

    @Test
    public void testCalculateShowPaged_ThirdCase() {
        SearchPageData<?> searchPageData = mock(SearchPageData.class);
        AbstractSearchPageController.ShowMode showMode = SHOWMODE_ALL;
        PaginationData paginationData = mock(PaginationData.class);

        when(searchPageData.getPagination()).thenReturn(paginationData);
        when(paginationData.getNumberOfPages()).thenReturn(2);

        boolean result = controller.calculateShowPaged(searchPageData, showMode);

        Assertions
                .assertThat(result)
                .isTrue();
    }

    @Test
    public void testCalculateShowPaged_FourthCase() {
        SearchPageData<?> searchPageData = mock(SearchPageData.class);
        AbstractSearchPageController.ShowMode showMode = SHOWMODE_ALL;
        PaginationData paginationData = mock(PaginationData.class);

        when(searchPageData.getPagination()).thenReturn(paginationData);
        when(paginationData.getNumberOfPages()).thenReturn(0);
        when(paginationData.getPageSize()).thenReturn((int) HIGHER_NUMBER);
        when(controller.getMaxSearchPageSize()).thenReturn((int) HIGHER_NUMBER);

        boolean result = controller.calculateShowPaged(searchPageData, showMode);

        Assertions
                .assertThat(result)
                .isTrue();

    }

    @Test
    public void testListPaginatedCustomerCustomerListUidIsBlank() {
        Model model = mock(Model.class);

        String actual = controller.listPaginatedCustomers(model, PAGE_NUMBER,
                SHOWMODE_ALL,
                SORT_CODE,
                "", "");

        Assertions
                .assertThat(actual)
                .isEqualTo(TABLE_PATH);
    }

    @Test
    public void testListPaginatedCustomerSearchBoxDisabled() {
        Model model = mock(Model.class);

        AssistedServiceSession session = mock(AssistedServiceSession.class);
        UserModel agent = mock(UserModel.class);

        when(assistedServiceFacade.getAsmSession()).thenReturn(session);
        when(session.getAgent()).thenReturn(agent);
        when(agent.getUid()).thenReturn(UID_AGENT);

        CustomerListData customerListData = mock(CustomerListData.class);
        when(customerListFacade.getCustomerListForUid(UID_CUSTOMER_LIST, UID_AGENT)).thenReturn(customerListData);

        when(customerListData.isSearchBoxEnabled()).thenReturn(false);

        PageableData pageableData = mock(PageableData.class);
        when(controller.createPageableData(PAGE_NUMBER, 5, SORT_CODE, SHOWMODE_PAGE))
                .thenReturn(pageableData);

        Map<String, Object> parametersMap = new HashMap<>();
        SearchPageData searchPageData = mock(SearchPageData.class);
        when(customerListFacade.getPagedCustomersForCustomerListUID(
                eq(UID_CUSTOMER_LIST), eq(UID_AGENT), eq(pageableData), anyMapOf(String.class, Object.class)))
                .thenReturn(searchPageData);

        doNothing().when(controller).populateModel(model, searchPageData, SHOWMODE_PAGE);

        String actual = controller.listPaginatedCustomers(
                model,
                PAGE_NUMBER,
                SHOWMODE_PAGE,
                SORT_CODE,
                UID_CUSTOMER_LIST,
                A_QUERY);

        Assertions
                .assertThat(actual)
                .isEqualTo(TABLE_PATH);

        verify(model).addAttribute(CUSTOMER_LIST_DATA, customerListData);
        verify(model, times(0)).addAttribute(QUERY, A_QUERY);
        verify(controller).populateModel(model, searchPageData, SHOWMODE_PAGE);
        verify(model).addAttribute(DEFAULT_CUSTOMER_LIST, UID_CUSTOMER_LIST);
        verify(assistedServiceFacade).getAsmSession();
        verify(customerListFacade).getCustomerListForUid(UID_CUSTOMER_LIST, UID_AGENT);
        verify(customerListFacade).getPagedCustomersForCustomerListUID(
                eq(UID_CUSTOMER_LIST), eq(UID_AGENT), eq(pageableData), anyMapOf(String.class, Object.class));
    }

    @Test
    public void testListPaginatedCustomerSearchBoxEnabled() {
        Model model = mock(Model.class);

        AssistedServiceSession session = mock(AssistedServiceSession.class);
        UserModel agent = mock(UserModel.class);

        when(assistedServiceFacade.getAsmSession()).thenReturn(session);
        when(session.getAgent()).thenReturn(agent);
        when(agent.getUid()).thenReturn(UID_AGENT);

        CustomerListData customerListData = mock(CustomerListData.class);
        when(customerListFacade.getCustomerListForUid(UID_CUSTOMER_LIST, UID_AGENT)).thenReturn(customerListData);

        when(customerListData.isSearchBoxEnabled()).thenReturn(true);

        PageableData pageableData = mock(PageableData.class);
        when(controller.createPageableData(PAGE_NUMBER, 5, SORT_CODE, SHOWMODE_PAGE))
                .thenReturn(pageableData);

        Map<String, Object> parametersMap = new HashMap<>();
        SearchPageData searchPageData = mock(SearchPageData.class);
        when(customerListFacade.getPagedCustomersForCustomerListUID(
                eq(UID_CUSTOMER_LIST), eq(UID_AGENT), eq(pageableData), anyMapOf(String.class, Object.class)))
                .thenReturn(searchPageData);

        doNothing().when(controller).populateModel(model, searchPageData, SHOWMODE_PAGE);

        String actual = controller.listPaginatedCustomers(
                model,
                PAGE_NUMBER,
                SHOWMODE_PAGE,
                SORT_CODE,
                UID_CUSTOMER_LIST,
                A_QUERY);

        verify(model).addAttribute(CUSTOMER_LIST_DATA, customerListData);
        verify(model).addAttribute(QUERY, A_QUERY);
        verify(controller).populateModel(model, searchPageData, SHOWMODE_PAGE);
        verify(model).addAttribute(DEFAULT_CUSTOMER_LIST, UID_CUSTOMER_LIST);
        verify(assistedServiceFacade).getAsmSession();
        verify(customerListFacade).getCustomerListForUid(UID_CUSTOMER_LIST, UID_AGENT);
        verify(customerListFacade).getPagedCustomersForCustomerListUID(
                eq(UID_CUSTOMER_LIST), eq(UID_AGENT), eq(pageableData), parametersMapCaptor.capture());

        Assertions
                .assertThat(actual)
                .isEqualTo(TABLE_PATH);

        Assertions
                .assertThat(parametersMapCaptor.getValue().get(QUERY))
                .isEqualTo(A_QUERY);
    }
}