(function () {
    var COOKIE_NAME = 'CONSENTS';
    var COOKIE_EXPIRY = 365;

    /**
     * Handles the submit button.
     */
    $('#save-consents-button').on('click', function () {
        var data = getConsentsFromForm();
        setCookie(COOKIE_NAME, data, COOKIE_EXPIRY);
        sendConsents(data);
    });

    /**
     * Returns the consents from the form.
     *
     * @returns {[]} the consents
     */
    function getConsentsFromForm() {
        var data = [];
        for (var i = 0; i < ACC.config.consentTypes.length; i++) {
            var consentType = ACC.config.consentTypes[i];
            data.push({
                consentType: consentType,
                optType: $('#consent-checkbox-' + consentType).prop("checked") ? 'OPT_IN' : 'OPT_OUT'
            })
        }
        return data;
    }

    /**
     * Sets the cookie in the browser.
     *
     * @param name the name of the cookie
     * @param value the value to set
     * @param days the days to expiration
     */
    function setCookie(name, value, days) {
        var expires = "";
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            expires = "; expires=" + date.toUTCString();
        }
        document.cookie = name + "=" + encodeURI(JSON.stringify(value) || "") + expires + "; path=/";
    }

    /**
     * Sends the consent data to the backend.
     *
     * @param data the data to send
     */
    function sendConsents(data) {
        $.ajax({
            url: ACC.config.encodedContextPath + "/consents",
            type: 'POST',
            data: JSON.stringify(data),
            contentType: 'application/json',
            dataType: 'json',
            async: true,
            beforeSend: function () {
                showLoader();
            },
            complete: function (response) {
                hideLoader()
            }
        });
    }
})(jQuery);