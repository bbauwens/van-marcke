package com.vanmarcke.blue.addon.checkout.steps.validation.impl;

import com.vanmarcke.blue.addon.facades.BlueCheckoutFlowFacade;
import com.vanmarcke.facades.storesession.VMKStoreSessionFacade;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.validation.ValidationResults;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commercefacades.order.CheckoutFacade;
import de.hybris.platform.commercefacades.storelocator.data.PointOfServiceData;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.site.BaseSiteService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class BlueResponsiveDeliveryMethodCheckoutStepValidatorTest {

    @Mock
    private CheckoutFacade checkoutFacade;

    @Mock
    private BlueCheckoutFlowFacade checkoutFlowFacade;

    @Mock
    private VMKStoreSessionFacade storeSessionFacade;

    @Mock
    BaseSiteService baseSiteService;

    @InjectMocks
    private BlueResponsiveDeliveryMethodCheckoutStepValidator responsiveDeliveryMethodCheckoutStepValidator;

    @Test
    public void testValidateOnEnter_withoutValidCountry() {
        RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);

        when(checkoutFlowFacade.hasValidCountry()).thenReturn(false);

        ValidationResults result = responsiveDeliveryMethodCheckoutStepValidator.validateOnEnter(redirectAttributes);
        assertThat(result).isEqualTo(ValidationResults.REDIRECT_TO_CART);

        verify(checkoutFlowFacade, never()).hasValidCart();
        verify(storeSessionFacade, never()).getCurrentStore();
        verify(checkoutFacade, never()).hasShippingItems();
        verify(checkoutFlowFacade, never()).hasNoDeliveryAddress();
    }

    @Test
    public void testValidateOnEnter_withInvalidCart() {
        RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);

        when(checkoutFlowFacade.hasValidCountry()).thenReturn(true);

        when(checkoutFlowFacade.hasValidCart()).thenReturn(false);

        ValidationResults result = responsiveDeliveryMethodCheckoutStepValidator.validateOnEnter(redirectAttributes);
        assertThat(result).isEqualTo(ValidationResults.REDIRECT_TO_CART);

        verify(storeSessionFacade, never()).getCurrentStore();
        verify(checkoutFacade, never()).hasShippingItems();
        verify(checkoutFlowFacade, never()).hasNoDeliveryAddress();
    }

    @Test
    public void testValidateOnEnter_withoutSessionStore() {
        RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
        BaseSiteModel baseSite = mock(BaseSiteModel.class);

        when(checkoutFlowFacade.hasValidCountry()).thenReturn(true);

        when(checkoutFlowFacade.hasValidCart()).thenReturn(true);

        when(storeSessionFacade.getCurrentStore()).thenReturn(null);

        when(baseSite.getChannel()).thenReturn(SiteChannel.B2B);
        when(baseSiteService.getCurrentBaseSite()).thenReturn(baseSite);

        ValidationResults result = responsiveDeliveryMethodCheckoutStepValidator.validateOnEnter(redirectAttributes);
        assertThat(result).isEqualTo(ValidationResults.REDIRECT_TO_CART);

        verify(checkoutFacade, never()).hasShippingItems();
        verify(checkoutFlowFacade, never()).hasNoDeliveryAddress();
    }

    @Test
    public void testValidateOnEnter_withoutSessionStore_withDIY() {
        RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
        BaseSiteModel baseSite = mock(BaseSiteModel.class);

        when(checkoutFlowFacade.hasValidCountry()).thenReturn(true);

        when(checkoutFlowFacade.hasValidCart()).thenReturn(true);

        when(storeSessionFacade.getCurrentStore()).thenReturn(null);

        when(baseSite.getChannel()).thenReturn(SiteChannel.DIY);
        when(baseSiteService.getCurrentBaseSite()).thenReturn(baseSite);

        ValidationResults result = responsiveDeliveryMethodCheckoutStepValidator.validateOnEnter(redirectAttributes);
        assertThat(result).isEqualTo(ValidationResults.SUCCESS);
    }

    @Test
    public void testValidateOnEnter_withoutDeliveryAddress() {
        RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
        PointOfServiceData store = mock(PointOfServiceData.class);

        when(checkoutFlowFacade.hasValidCountry()).thenReturn(true);

        when(checkoutFlowFacade.hasValidCart()).thenReturn(true);

        when(storeSessionFacade.getCurrentStore()).thenReturn(store);


        when(checkoutFacade.hasShippingItems()).thenReturn(true);

        when(checkoutFlowFacade.hasNoDeliveryAddress()).thenReturn(true);

        ValidationResults result = responsiveDeliveryMethodCheckoutStepValidator.validateOnEnter(redirectAttributes);
        assertThat(result).isEqualTo(ValidationResults.REDIRECT_TO_CART);
    }

    @Test
    public void testValidateOnEnter() {
        RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
        PointOfServiceData store = mock(PointOfServiceData.class);

        when(checkoutFlowFacade.hasValidCountry()).thenReturn(true);

        when(storeSessionFacade.getCurrentStore()).thenReturn(store);

        when(checkoutFlowFacade.hasValidCart()).thenReturn(true);

        when(checkoutFacade.hasShippingItems()).thenReturn(true);

        when(checkoutFlowFacade.hasNoDeliveryAddress()).thenReturn(false);

        ValidationResults result = responsiveDeliveryMethodCheckoutStepValidator.validateOnEnter(redirectAttributes);
        assertThat(result).isEqualTo(ValidationResults.SUCCESS);
    }
}