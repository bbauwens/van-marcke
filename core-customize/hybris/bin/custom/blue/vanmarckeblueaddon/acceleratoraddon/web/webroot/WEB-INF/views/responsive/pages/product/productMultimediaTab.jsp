<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/product" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:if test="${product.documentsVisible}">
    <div class="tabhead">
        <a href="">${fn:escapeXml(title)}</a> <span class="glyphicon"></span>
    </div>
    <div class="tabbody vmb-documents-tabbody">
        <div class="tab-container">
            <product:productMultimediaTab product="${product}"/>
        </div>
    </div>
</c:if>
