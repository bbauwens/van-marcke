<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/address" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/formElement" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<spring:htmlEscape defaultHtmlEscape="true"/>

<spring:url var="selectDeliveryAddressUrl"
            value="{contextPath}/checkout/multi/delivery-address/select"
            htmlEscape="false">
    <spring:param name="contextPath" value="${request.contextPath}"/>
</spring:url>
<c:if test="${(empty addressFormEnabled or addressFormEnabled) and cmsSite.channel != 'DIY'}">
    <spring:url var="selectDeliveryAddressFormPostUrl" value="{contextPath}/checkout/multi/delivery-address/select"
                htmlEscape="false">
        <spring:param name="contextPath" value="${request.contextPath}"/>
    </spring:url>
    <a href="#" type="button" class="vmb-btn-new-address vmb-address-selector"><spring:theme
            code="checkout.checkout.multi.deliveryAddress.newAddress"/> <span class="glyphicon"></span></a>
    <div class="vmb-checkout-new-address">
        <form:form method="post" modelAttribute="addressForm" action="${fn:escapeXml(selectDeliveryAddressFormPostUrl)}"
                   cssClass="vmb-form-validation">
            <form:hidden path="addressId" class="add_edit_delivery_address_id"
                         status="${not empty suggestedAddresses ? 'hasSuggestedAddresses' : ''}"/>
            <input type="hidden" name="bill_state" id="address.billstate"/>
            <form:hidden path="countryIso"/>
            <div id="i18nAddressForm" class="i18nAddressForm">
                <address:blueAddressFormElements/>
            </div>
            <div class="checkbox">
                <formElement:formCheckbox idKey="saveAddressInMyAddressBook"
                                          labelKey="checkout.summary.deliveryAddress.saveAddressInMyAddressBook"
                                          path="saveInAddressBook"
                                          inputCSS="add-address-left-input vmb-checkout-new-address-save"
                                          labelCSS="add-address-left-label" mandatory="false"/>
                <formElement:formCheckbox idKey="defaultAddress"
                                          labelKey="address.default" path="defaultAddress"
                                          inputCSS="add-address-left-input vmb-checkout-new-address-default"
                                          labelCSS="add-address-left-label hidden" mandatory="false"/>
            </div>
            <div id="addressform_button_panel" class="form-actions">
                <ycommerce:testId code="multicheckout_useAddress_button">
                    <button class="btn btn-default btn-submit positive right vmb-checkout-new-address__submit"
                            data-url="${fn:escapeXml(selectDeliveryAddressUrl)}"
                            type="button">
                        <spring:theme code="checkout.multi.deliveryAddress.useThisAddress"/>
                    </button>
                </ycommerce:testId>
            </div>
        </form:form>
    </div>
</c:if>
<c:if test="${not empty deliveryAddresses}">
    <a href="#" type="button" class="vmb-btn-address-book vmb-address-selector open"><spring:theme
            code="checkout.checkout.multi.deliveryAddress.viewAddressBook"/> <span class="glyphicon"></span></a>
    <div id="addressbook" style="display: block">
        <c:forEach items="${deliveryAddresses}" var="deliveryAddress" varStatus="status">
            <div class="addressEntry">
                    <ul>
                        <li>
                            <strong>
                                <c:if test="${not empty deliveryAddress.title}">${fn:escapeXml(deliveryAddress.title)}&nbsp;</c:if>
                                <c:choose>
                                    <c:when test="${not empty deliveryAddress.firstName or not empty deliveryAddress.lastName}">
                                        ${fn:escapeXml(deliveryAddress.firstName)}&nbsp;${fn:escapeXml(deliveryAddress.lastName)}
                                    </c:when>
                                    <c:otherwise>
                                        ${fn:escapeXml(deliveryAddress.companyName)}
                                    </c:otherwise>
                                </c:choose>
                            </strong>
                            <br>
                                ${fn:escapeXml(deliveryAddress.line1)}&nbsp;
                                ${fn:escapeXml(deliveryAddress.line2)}&nbsp;
                                ${fn:escapeXml(deliveryAddress.apartment)}
                            <br>
                                ${fn:escapeXml(deliveryAddress.town)}
                            <c:if test="${not empty deliveryAddress.region.name}">
                                &nbsp;${fn:escapeXml(deliveryAddress.region.name)}
                            </c:if>
                            <br>
                                ${fn:escapeXml(deliveryAddress.country.name)}&nbsp;
                                ${fn:escapeXml(deliveryAddress.postalCode)}
                        </li>
                    </ul>
                <button type="submit" class="btn btn-default btn-small vmb-select-address-btn__submit"
                        data-url="${fn:escapeXml(selectDeliveryAddressUrl)}"
                        onclick="selectAddress(${fn:escapeXml(deliveryAddress.id)})">
                    <spring:theme code="checkout.multi.deliveryAddress.useThisAddress"/>
                </button>
            </div>
        </c:forEach>
    </div>
</c:if>