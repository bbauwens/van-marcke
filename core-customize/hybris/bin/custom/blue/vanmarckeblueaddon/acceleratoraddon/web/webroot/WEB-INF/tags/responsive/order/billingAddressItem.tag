<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/address" %>
<%@ taglib prefix="order" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/order" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ attribute name="order" required="true" type="de.hybris.platform.commercefacades.order.data.OrderData" %>

<spring:htmlEscape defaultHtmlEscape="true"/>

<c:set var="address" value="${order.paymentInfo.billingAddress}"/>
<c:set var="name" value="${order.b2bCustomerData.unit.name}"/>

<div class="label-order">
    <spring:theme code="text.account.paymentDetails.billingAddress"/>
</div>
<div class="value-order">
    <address:formattedAddress address="${address}" name="${name}"/>
</div>    
