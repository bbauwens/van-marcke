<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<div class="vmb-product-variant-addtocart">
    <sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
        <button type="button" class="btn btn-primary js-add-to-wishlist">
            <spring:theme code="text.saveForLater"/>
        </button>
    </sec:authorize>
    <button type="button" class="btn btn-default vmb-add-to-cart" onclick="addToCartMulti()">
        <span class="glyphicon glyphicon-shopping-cart"></span>&nbsp;<spring:theme code="text.addToCart"/>
    </button>
</div>
