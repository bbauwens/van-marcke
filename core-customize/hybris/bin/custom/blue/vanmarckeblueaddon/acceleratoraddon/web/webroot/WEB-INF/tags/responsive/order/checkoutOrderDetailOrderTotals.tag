<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="order" required="true" type="de.hybris.platform.commercefacades.order.data.OrderData" %>
<%@ taglib prefix="order" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/order" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<spring:htmlEscape defaultHtmlEscape="true" />
<spring:url value="${continueUrl}" var="continueShoppingUrl" scope="session" htmlEscape="false"/>

<c:if test="${not empty order}">
    <div class="account-orderdetail">
        <div class="account-orderdetail__footer">
            <div class="row no-margin">
                <div class="col-sm-6 col-md-7 col-lg-8">
                    <order:appliedVouchers order="${order}" />
                    <order:receivedPromotions order="${order}" />
                </div>
                <div class="col-sm-6 col-md-5 col-lg-4">
                    <div class="vmb-checkout-confirmation__totals">
                        <order:orderTotalsItem order="${order}" />
                        <div class="text-right">
                            <button class="btn btn-default btn-block vmb-checkout-confirmForm-open">
                                <spring:theme code="checkout.multi.contactForm.button"/>
                            </button>
                            <button class="btn btn-primary btn-block btn--continue-shopping js-continue-shopping-button" data-continue-shopping-url="${fn:escapeXml(continueShoppingUrl)}">
                                <spring:theme code="checkout.orderConfirmation.continueShopping"/>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</c:if>

