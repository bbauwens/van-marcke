<%@ page trimDirectiveWhitespaces="true" %>
<%@ page import="com.vanmarcke.core.constants.VanmarckeCoreConstants" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/cart" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common" %>
<%@ taglib prefix="order" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/order" %>
<%@ taglib prefix="wishlist" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/wishlist" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/shared/format" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/formElement" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<sec:authorize var="isAnonymousUser" access="hasAnyRole('ROLE_ANONYMOUS')"/>
<spring:htmlEscape defaultHtmlEscape="true"/>
<spring:url value="/my-account/saved-baskets" var="wishListsUrl" htmlEscape="false"/>
<spring:url value="/my-account/saved-baskets/{/cartId}/edit" var="actionUrl" htmlEscape="false">
    <spring:param name="cartId" value="${wishListData.name}"/>
</spring:url>
<c:set var="loc_val">
    <spring:message code="basket.save.cart.max.chars"/>
</c:set>
<c:set var="validation_error_pattern_name">
    <spring:message code="wishlist.validation-error.pattern-name"/>
</c:set>
<c:set var="validation_error_pattern_description">
    <spring:message code="wishlist.validation-error.pattern-description"/>
</c:set>

<input type="hidden" id="localized_val" name="localized_val" value="${loc_val}"/>
<input type="hidden" id="localized_error_name" name="localized_error_name" value="${validation_error_pattern_name}"/>
<input type="hidden" id="localized_error_description" name="localized_error_description" value="${validation_error_pattern_description}"/>

<div style="display: none">
    <span id="localized-error-no-products-selected">
        <spring:theme code="basket.page.savedCart.add.error"/>
    </span>
</div>

<div class="container vmb-saved-cart-details">
    <div class="back-link">
        <a href="#" data-base-href="${wishListsUrl}" class="savedCartBackBtn">
            <span class="glyphicon glyphicon-chevron-left"></span>
        </a>
        <span class="label"><spring:theme code="text.account.savedCart.title.details"/></span>
    </div>
    <form:form action="${actionUrl}" method="POST" id="wishListEditForm" cssClass="js-update-wish-list" modelAttribute="wishListEditForm" autocomplete="off">
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <div class="form-group">
                    <c:if test="${not empty messageKey}">
                        <div class="legend"><spring:theme code="${messageKey}"/></div>
                    </c:if>
                    <formElement:formInputBox idKey="wishListName"
                                              labelKey="basket.save.cart.name"
                                              path="name" maxlength="50" mandatory="true"/>
                    <div class="help-block right-cartName" id="validate-error-name">
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 text-right">
                <ul class="list-unstyled list-inline">
                    <li>
                        <c:set var="exportToolTip"><spring:theme code="basket.export.csv.file"/></c:set>
                        <a href="#"
                           class="js-export-basket restore-item-link"
                           data-basket-name="${fn:escapeXml(wishListData.name)}"
                           data-basket-type="WISHLIST">
                            <span class="glyphicon glyphicon-export" data-toggle="tooltip"
                                  data-original-title="${exportToolTip}"></span>
                        </a>
                    </li>
                    <li>
                        <c:set var="deleteToolTip"><spring:theme code="general.delete.button"/></c:set>
                        <button class="btn btn-link js-delete-wish-list remove-item-link"
                                data-wishlist-id="${fn:escapeXml(wishListData.name)}"
                                data-wishlist-name="${fn:escapeXml(wishListData.name)}">
                            <span class="glyphicon glyphicon-trash" data-toggle="tooltip"
                                  data-original-title="${deleteToolTip}"></span>
                        </button>
                    </li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <div class="form-group">
                    <label class="control-label" for="description">
                        <spring:theme code="basket.save.cart.description"/>
                    </label>
                    <form:textarea cssClass="form-control" id="wishListDescription" path="description" maxlength="${VanmarckeCoreConstants.SAVED_CART_DESCRIPTION_FIELD_LIMIT}"
                                   rows="2"/>
                    <div class="help-block" id="remainTextArea">
                    </div>
                    <div class="help-block right-cartName" id="validate-error-description">
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6">
                <div class="form-group">
                    <label class="control-label" for="modifiedDate">
                        <spring:theme code="text.account.savedCart.dateSaved"/>
                    </label>
                    <fmt:formatDate value="${wishListData.saveTime}" dateStyle="medium" timeStyle="short" type="both"
                                    var="modifiedDate"/>
                    <input class="form-control" id="modifiedDate" type="text" value="${modifiedDate}" disabled/>
                </div>
            </div>
        </div>
        <h3 class="vmb-saved-cart-details__producttitle">
            <spring:theme code="text.account.savedCarts.products"/>
        </h3>
        <table class="table table-condensed vmb-product-table xxxxxx">
            <thead>
            <tr>
                <th class="vmb-productitem__toggle"></th>
                <th class="vmb-productitem__image"></th>
                <th class="vmb-productitem__info">
                    <spring:theme code="basket.page.item"/>
                </th>
                <th class="vmb-productitem__wishlist wishlist-checkboxes" style="min-width: 130px">
                    <input id="wishlist-check" type="checkbox" class="js-select-all-products-wish-list"><spring:theme code="basket.page.wishlist"/>
                </th>
                <th class="vmb-productitem__quantity"><spring:theme code="basket.page.qty"/></th>
                <th class="vmb-productitem__stock-warehouse" data-toggle="tooltip" data-placement="top"
                    title="<spring:theme code="text.stockstatus.icon.warehouse" htmlEscape="false"/>"></th>
                <c:if test="${cmsSite.channel != 'DIY'}">
                    <th class="vmb-productitem__stock-tec" data-toggle="tooltip" data-placement="top"
                      title="<spring:theme code="text.stockstatus.icon.tec" htmlEscape="false"/>">
                     &nbsp;<format:nostock/></th>
                    <th class="vmb-productitem__price"><spring:theme code="basket.page.price"
                                                                     arguments="${currentCurrency.symbol}"/></th>
                </c:if>
                <c:if test="${!isAnonymousUser and (user.showNetPrice or cmsSite.channel == 'DIY')}">
                    <th class="vmb-productitem__nettoprice vmb-netto-price-display">
                        <spring:theme code="basket.page.price.netto" arguments="${currentCurrency.symbol}"/></th>
                </c:if>
                <th class="vmb-productitem__total--column"><spring:theme code="basket.page.total"/></th>
                <th></th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${wishListData.entries}" var="entry" varStatus="loop">
                <wishlist:wishListEntryDetails wishListEntry="${entry}" itemIndex="${loop.index}" showStock="true"/>
            </c:forEach>
            </tbody>
        </table>

        <div class="vmb-saved-cart-details__bottom hidden-print">
            <c:if test="${fn:length(wishListData.entries) > 0}">
                <button type="button" class="btn btn-link js-restore-wish-list restore-item-link"
                        data-wishlist-id="${fn:escapeXml(wishListData.name)}">
                    <span><spring:theme code='text.account.savedCart.restore'/></span>
                </button>
            </c:if>

            <c:set var="saveText"><spring:theme code="basket.save.cart.action.save"/></c:set>
            <input type="submit" class="btn btn-default" id="saveCartButton" value="${saveText}"/>
        </div>
    </form:form>
</div>

<wishlist:wishListDeleteModal/>

<div style="display: none">
    <span id="wishlist-no-products-selected-message">
        <spring:theme code="basket.page.savedCart.add.error"/>
    </span>
</div>

<common:globalMessagesTemplates/>
