ACC.cart = {

    _autoload: [
        "cartRestoration",
        ["bindApplyVoucher", $("#js-voucher-apply-btn").length != 0],
        ["bindToReleaseVoucher", $("#js-applied-vouchers").length != 0],
        'bindClearCart'
    ],

    cartRestoration: function () {
        $('.cartRestoration').click(function () {
            var sCartUrl = $(this).data("cartUrl");
            window.location = sCartUrl;
        });
    },

    refreshCartData: function (cartData, entryNum, quantity, itemIndex) {
        // if cart is empty, we need to reload the whole page
        if (cartData.entries.length == 0) {
            location.reload();
        } else {
            var form;

            if (entryNum == -1) // grouped item
            {
                form = $('.js-qty-form' + itemIndex);
                var productCode = form.find('input[name=productCode]').val();

                var quantity = 0;
                var entryPrice = 0;
                for (var i = 0; i < cartData.entries.length; i++) {
                    var entry = cartData.entries[i];
                    if (entry.product.code == productCode) {
                        quantity = entry.quantity;
                        entryPrice = entry.totalPrice;
                        ACC.cart.updateEntryNumbersForCartMenuData(entry);
                        break;
                    }
                }

                if (quantity == 0) {
                    location.reload();
                } else {
                    form.find(".qtyValue").html(quantity);
                    form.parent().parent().find(".js-item-total").html(entryPrice.formattedValue);
                }
            }

            ACC.cart.refreshCartPageWithJSONResponse(cartData);
        }
    },

    refreshCartPageWithJSONResponse: function (cartData) {
        // refresh mini cart
        ACC.minicart.updateMiniCartDisplay();
        $('.js-cart-top-totals').html($("#cartTopTotalSectionTemplate").tmpl(cartData));
        $('div .cartpotproline').remove();
        $('div .cartproline').remove();
        $('.js-cart-totals').remove();
        $('#ajaxCartPotentialPromotionSection').html($("#cartPotentialPromotionSectionTemplate").tmpl(cartData));
        $('#ajaxCartPromotionSection').html($("#cartPromotionSectionTemplate").tmpl(cartData));
        $('#ajaxCart').html($("#cartTotalsTemplate").tmpl(cartData));
        ACC.quote.bindQuoteDiscount();
    },

    updateEntryNumbersForCartMenuData: function (entry) {
        var entryNumbers = "";
        $.each(entry.entries, function (index, subEntry) {
            if (index != 0) {
                entryNumbers = entryNumbers + ";";
            }
            entryNumbers = entryNumbers + subEntry.entryNumber;
        });
        $('.js-execute-entry-action-button').data('actionEntryNumbers', entryNumbers);
    },

    getProductQuantity: function (gridContainer, mapData, i) {
        var tables = gridContainer.find("table");

        $.each(tables, function (index, currentTable) {
            var skus = jQuery.map($(currentTable).find("input[type='hidden'].sku"), function (o) {
                return o.value
            });
            var quantities = jQuery.map($(currentTable).find("input[type='textbox'].sku-quantity"), function (o) {
                return o
            });
            var selectedVariants = [];

            $.each(skus, function (index, skuId) {
                var quantity = mapData[skuId];
                if (quantity != undefined) {
                    quantities[index].value = quantity;

                    var indexPattern = "[0-9]+";
                    var currentIndex = parseInt(quantities[index].id.match(indexPattern));
                    var gridTotalValue = gridContainer.find("[data-grid-total-id=" + 'total_value_' + currentIndex + "]");
                    var gridLevelTotalPrice = "";
                    var currentPrice = $("input[id='productPrice[" + currentIndex + "]']").val();
                    if (quantity > 0) {
                        gridLevelTotalPrice = ACC.productorderform.formatTotalsCurrency(parseFloat(currentPrice) * parseInt(quantity));
                    }
                    gridTotalValue.html(gridLevelTotalPrice);

                    selectedVariants.push({
                        id: skuId,
                        size: $(quantities[index]).siblings('.variant-prop').data('variant-prop'),
                        quantity: quantity,
                        total: gridLevelTotalPrice
                    });
                }
            });

            if (selectedVariants.length != 0) {
                $.tmpl(ACC.productorderform.$variantSummaryTemplate, {
                    variants: selectedVariants
                }).appendTo($(currentTable).addClass('selected'));
                $(currentTable).find('.variant-summary .variant-property').html($(currentTable).find('.variant-detail').data('variant-property'));
                $(currentTable).data(ACC.productorderform.selectedVariantData, selectedVariants);
            }
        });

    },

    bindApplyVoucher: function () {

        $("#js-voucher-apply-btn").on("click", function (e) {
            ACC.cart.handleApplyVoucher(e);
        });

        $("#js-voucher-code-text").on("keypress", function (e) {
            var code = (e.keyCode ? e.keyCode : e.which);
            if (code == 13) {
                ACC.cart.handleApplyVoucher(e);
            }
        });
    },

    handleApplyVoucher: function (e) {
        var voucherCode = $.trim($("#js-voucher-code-text").val());
        if (voucherCode != '' && voucherCode.length > 0) {
            $("#applyVoucherForm").submit();
        }
    },

    bindToReleaseVoucher: function () {
        $('.js-release-voucher-remove-btn').on("click", function (event) {
            $(this).closest('form').submit();
        });
    },

    bindClearCart: function () {
        $('#cart-removal__submit').on('click', function () {
            showLoader();

            var clearCartForm = $("#cartEntryActionForm");
            var entryNumbers = $(this).data("entryNumbers").toString().split(';');

            entryNumbers.forEach(function (entryNumber) {
                if (entryNumber != null && entryNumber !== "") {
                    var entryNumbersInput = $("<input>").attr("type", "hidden").attr("name", "entryNumbers").val(entryNumber);
                    clearCartForm.append($(entryNumbersInput));
                }
            });
            clearCartForm.attr('action', $(this).data("url")).submit();
        });
    }
};