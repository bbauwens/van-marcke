package com.vanmarcke.blue.addon.validation;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.config.SiteConfigService;
import de.hybris.platform.acceleratorstorefrontcommons.forms.ImportCSVSavedCartForm;
import org.apache.commons.lang.RandomStringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.validation.Errors;
import org.springframework.web.multipart.MultipartFile;

import java.util.Random;

import static org.mockito.Mockito.*;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class BlueImportExcelSavedCartFormValidatorTest {

    private static final String IMPORT_CSV_FILE_MAX_SIZE_BYTES_KEY = "import.csv.file.max.size.bytes";
    private static final String XLS_FILE_FIELD = "xlsFile";
    private static final String XLS_FILE_EXTENSION = ".xls";
    private static final String APP_XLS_CONTENT_TYPE = "application/vnd.ms-excel";
    private static final String CONTENT_TYPE_WRONG = RandomStringUtils.randomAlphabetic(10);
    private static final String FILE_NAME_WRONG = RandomStringUtils.randomAlphabetic(10);
    private static final String FILE_NAME = RandomStringUtils.randomAlphabetic(10) + XLS_FILE_EXTENSION;
    private static final long FILE_SIZE_SMALLER = new Random().nextInt(100);
    private static final long FILE_SIZE_LARGER = FILE_SIZE_SMALLER + 5;

    private Errors errors;
    private ImportCSVSavedCartForm target;
    private MultipartFile xlsFile;

    @Mock
    private SiteConfigService siteConfigService;

    @Spy
    @InjectMocks
    private BlueImportExcelSavedCartFormValidator validator;

    @Before
    public void setUp() {
        target = new ImportCSVSavedCartForm();
        xlsFile = mock(MultipartFile.class);
        target.setCsvFile(xlsFile);
        errors = mock(Errors.class);
        when(siteConfigService.getLong(IMPORT_CSV_FILE_MAX_SIZE_BYTES_KEY, 0)).thenReturn(0L);
    }

    @Test
    public void testValidateDifferentObject() {
        Object target = new String();
        validator.validate(target, errors);

        verifyZeroInteractions(errors);
    }

    @Test
    public void testValidateXlsFileNull() {
        target.setCsvFile(null);

        validator.validate(target, errors);

        verify(errors).rejectValue(XLS_FILE_FIELD, "import.csv.savedCart.fileRequired");
        verifyNoMoreInteractions(errors);
    }

    @Test
    public void testValidateXlsFileEmpty() {
        when(xlsFile.isEmpty()).thenReturn(true);

        validator.validate(target, errors);

        verify(errors).rejectValue(XLS_FILE_FIELD, "import.csv.savedCart.fileRequired");
        verifyNoMoreInteractions(errors);
    }

    @Test
    public void testValidateXlsFileWrongContentType() {
        when(xlsFile.getContentType()).thenReturn(CONTENT_TYPE_WRONG);

        validator.validate(target, errors);

        verify(errors).rejectValue(XLS_FILE_FIELD, "import.xls.savedCart.fileExcelRequired");
        verifyNoMoreInteractions(errors);
    }

    @Test
    public void testValidateXlsFileNameNull() {
        when(xlsFile.getOriginalFilename()).thenReturn(null);

        validator.validate(target, errors);

        verify(errors).rejectValue(XLS_FILE_FIELD, "import.xls.savedCart.fileExcelRequired");
        verifyNoMoreInteractions(errors);
    }

    @Test
    public void testValidateXlsFileWrongExtension() {
        when(xlsFile.getOriginalFilename()).thenReturn(FILE_NAME_WRONG);

        validator.validate(target, errors);

        verify(errors).rejectValue(XLS_FILE_FIELD, "import.xls.savedCart.fileExcelRequired");
        verifyNoMoreInteractions(errors);
    }

    @Test
    public void testValidateXlsFileExceedsMaxFileSize() {
        when(xlsFile.isEmpty()).thenReturn(false);
        when(xlsFile.getContentType()).thenReturn(APP_XLS_CONTENT_TYPE);
        when(xlsFile.getOriginalFilename()).thenReturn(FILE_NAME);

        when(xlsFile.getSize()).thenReturn(FILE_SIZE_LARGER);
        doReturn(FILE_SIZE_SMALLER).when(validator).getFileMaxSize();

        validator.validate(target, errors);

        verify(errors).rejectValue(XLS_FILE_FIELD, "import.csv.savedCart.fileMaxSizeExceeded");
        verifyNoMoreInteractions(errors);
    }

    @Test
    public void testValidateXlsFileSuccessCase() {
        when(xlsFile.getSize()).thenReturn(FILE_SIZE_SMALLER);
        when(xlsFile.isEmpty()).thenReturn(false);
        when(xlsFile.getContentType()).thenReturn(APP_XLS_CONTENT_TYPE);
        when(xlsFile.getOriginalFilename()).thenReturn(FILE_NAME);
        doReturn(FILE_SIZE_LARGER).when(validator).getFileMaxSize();

        validator.validate(target, errors);

        verifyZeroInteractions(errors);
    }

    @Test
    public void testGetFileMaxSize() {
        validator.getFileMaxSize();

        verify(siteConfigService).getLong(IMPORT_CSV_FILE_MAX_SIZE_BYTES_KEY, 0);
    }
}