VMB.modal = {

    defaultConfig : {
        canClose: true,
        className: ""
    },

    show: function (id, title, body, footer, config) {

        config = $.extend({}, VMB.modal.defaultConfig, config);

        var modalOptions = {};

        $("body").append($.templates("#vmbDynamicModalTemplate").render({
            modalId : id,
            modalTitle: title,
            modalBody: body,
            modalFooter: _.isEmpty(footer) ? false : footer,
            canClose: config.canClose,
            className: config.className
        }));

        if(!config.canClose){
            modalOptions.keyboard = false;
            modalOptions.backdrop = 'static';
        }

        $("#" + id).modal(modalOptions);
    }

};