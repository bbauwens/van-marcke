(function () {
    if ($("#deliveryMethodForm ").length > 0) {

        //constants
        var SHIPPING = 'DELIVERY';
        var PICKUP = 'PICKUP';
        var tecRadioButton = $('#tecRadioButton');
        var shippingRadioButton = $('#shippingRadioButton');
        var pickupDatePicker = $('.vmbPickUpDatePicker');
        var shippingDatePicker = $('.vmbDeliveryDatePicker');
        var $alternativeStoreDropDown = $('#alternativeStoreDropdown');
        var tecUid = $('#vmbTecUid');
        var reservationMessage = $('#reservationMessage');
        var warnMessageInput = $('#orderSplitWarnMessage');

        //variables
        var selectedPickupDate = $('#vmbSelectedPickupDate');
        var selectedShippingDate = $('#vmbSelectedShippingDate');

        var deliveryMethod = $('#vmbDeliveryOption');
        var tecResponse = null;
        var shippingModeCode = $('#shippingModeCode');

        var isDIY = $('#isDIY').val();

        /**
         *
         *
         * 1. Load page -> show previously chosen datepicker based on delivery method on form
         * 2. Perform ajax call -> include month and year
         * 3. JSON response contains array with available dates
         * 4. Create datepicker -> before show day checks if day is present in array
         *
         *
         */

        $(function () {
            showRequiredDatepicker();
        });

        //handle click event radio button TEC
        tecRadioButton.click(function (event) {
            getPickupDates(false, selectedPickupDate.val());
            setDeliveryOption(PICKUP);
            ACC.track.checkoutStep(1, PICKUP);
        });

        //handle click event radio button shipping
        shippingRadioButton.click(function (event) {
            getShippingDates();
            setDeliveryOption(SHIPPING);
            ACC.track.checkoutStep(1, SHIPPING);
        });

        //handle click event alternative store dropdown
        $alternativeStoreDropDown.change(function () {
            setTecUid(this.value);
            getPickupDates(true, selectedPickupDate.val());
        });

        /**
         * Retrieves the shipping dates.
         */
        function getShippingDates() {
            $.ajax({
                url: ACC.config.encodedContextPath + "/delivery/" + SHIPPING,
                type: 'get',
                dataType: 'json',
                beforeSend: function () {
                    showLoader();
                },
                success: function (response) {
                    handleAjaxResult(response, SHIPPING);
                },
                error: function (xhr) {
                    location.reload();
                },
                complete: function () {
                    hideLoader();
                }
            });
        }

        /**
         * Retrieves the pickup dates for the current Tec UID.
         *
         * @param selectTecDate indicates whether the tec date should be selected
         * @param selectedDate the selected pickup date
         */
        function getPickupDates(selectTecDate, selectedDate) {
            $.ajax({
                url: ACC.config.encodedContextPath + '/delivery/' + PICKUP + '?pos=' + tecUid.val() + (!selectTecDate && selectedDate ? '&date=' + selectedDate : ''),
                type: 'get',
                dataType: 'json',
                beforeSend: function () {
                    showLoader();
                },
                success: function (response) {
                    // Store the response to be able to calculate the valid date.
                    tecResponse = $.extend({}, response);
                    handleAjaxResult(tecResponse, PICKUP, selectTecDate);
                },
                error: function (xhr) {
                    //Do Something to handle error
                },
                complete: function () {
                    hideLoader();
                }
            });
        }

        /**
         * Handles the given Ajax response based on the given delivery method.
         *
         * @param response the response object
         * @param method the delivery method
         * @param selectTecDate indicates whether the tec date should be selected
         */
        function handleAjaxResult(response, method, selectTecDate) {
            var selectedDate = null;

            if (PICKUP === method) {
                updateTecDatePicker(response, selectTecDate);
                updateAlternativeTecStoreDropDown(response);
                showReservationMessage(response);
                showPickupDatePicker();
                pickupDatePicker.datepicker("refresh");
                selectedDate = selectedPickupDate.val();
            } else if (SHIPPING === method) {
                updateShippingDatePicker(response);
                showShippingDatePicker();
                hideMessages();
                selectedDate = selectedShippingDate.val();
            }

            if (selectedDate) {
                resetProductEntryDates(response, selectedDate);
            }
        }

        /**
         * Decide whether the given date should be selectable on the pickup datepicker.
         * Every day of the selected month on the datepicker will be processed one by one.
         *
         * @param date the date
         * @param response the ajax response
         * @returns {boolean} true if selectable
         */
        function isValidPickupDate(date, response) {
            var dateToValidate = getDate(date);

            // check holiday dates
            var invalidShippingDates = getInvalidShippingDates();
            for (i = 0; i < invalidShippingDates.length; i++) {
                if (invalidShippingDates[i].getTime() === dateToValidate.getTime()) {
                    return false;
                }
            }

            var minimumPickupDate = response && response.hasOwnProperty('firstPossiblePickupDate') ? getDate(response.firstPossiblePickupDate) : undefined;

            //closed on sunday
            if (dateToValidate.getDay() === 0) {
                return false;
            }

            // open today's date if tec collect is possible
            if (response && response.tecCollectPossible && isToday(dateToValidate)) {
                if (new Date().getDay() === 6) {
                    return response.openOnSaturday === 'true';
                }
                return true;
            }

            // close all dates before the minimum pickup date
            if (!minimumPickupDate || dateToValidate < minimumPickupDate) {
                return false;
            }

            // only open saturdays if the store is open on saturday
            if (dateToValidate.getDay() === 6) {
                return response.openOnSaturday;
            }

            return true;
        }

        /**
         * Return the given date as a date object.
         *
         * @param date the date as a date object or as a string
         * @returns {Date|*} a date object
         */
        function getDate(date) {
            var result = _.isString(date) ? new Date(date) : date;
            result.setHours(0, 0, 0, 0)
            return result;
        }

        /**
         * Decide whether the given date should be selectable on the shipping datepicker.
         *
         * @param date the date
         * @returns {boolean} true if selectable
         */
        function isValidShippingDate(date) {
            var invalidShippingDates = getInvalidShippingDates();
            for (i = 0; i < invalidShippingDates.length; i++) {
                if (invalidShippingDates[i].getTime() === date.getTime()) {
                    return false;
                }
            }

            //check if date is saturday or sunday
            return date.getDay() !== 0 && date.getDay() !== 6;
        }

        function getInvalidShippingDates() {
            return [
                new Date(2021, 4, 13, 0, 0, 0, 0),
                new Date(2021, 4, 14, 0, 0, 0, 0),
                new Date(2021, 4, 24, 0, 0, 0, 0),
                new Date(2021, 6, 21, 0, 0, 0, 0),
                new Date(2021, 7, 15, 0, 0, 0, 0),
                new Date(2021, 10, 1, 0, 0, 0, 0),
                new Date(2021, 10, 11, 0, 0, 0, 0),
                new Date(2021, 10, 12, 0, 0, 0, 0),
                new Date(2021, 11, 24, 0, 0, 0, 0),
                new Date(2021, 11, 25, 0, 0, 0, 0),
                new Date(2021, 11, 28, 0, 0, 0, 0),
                new Date(2021, 11, 29, 0, 0, 0, 0),
                new Date(2021, 11, 30, 0, 0, 0, 0),
                new Date(2021, 11, 31, 0, 0, 0, 0)
            ];
        }

        /**
         * Decide whether to show the info message when the Pickup date is at least the day after
         * tomorrow in comparison to the TEC collect date (which is always today). This info message
         * states that the ordered items will be available in the chosen TEC store for X amount of days.
         *
         * @param response the ajax response
         */
        function showReservationMessage(response) {
            if (response.reservationMessage) {
                reservationMessage.text(response.reservationMessage);
                reservationMessage.removeClass('hidden');
            }
        }

        /**
         * Hide all messages.
         */
        function hideMessages() {
            hideReservationMessage();
        }

        /**
         * Hide TEC collect message.
         */
        function hideReservationMessage() {
            reservationMessage.addClass('hidden');

        }

        /**
         * Update the variables of the pickup datepicker.
         *
         * @param response the ajax response
         * @param selectTecDate indicates whether the tec date should be selected
         */
        function updateTecDatePicker(response, selectTecDate) {
            if (response.firstPossiblePickupDate
                || response.tecCollectPossible
                || (response.alternativePointOfService && response.alternativePointOfService.length > 0)) {

                initPickupDatepicker(response.tecCollectPossible);
                $('#pickupDateNullInfo').addClass('hidden');

                var minDate = response.tecCollectPossible ? formatDate(new Date()) : response.firstPossiblePickupDate;
                pickupDatePicker.datepicker("option", "minDate", minDate);

                if (selectTecDate) {
                    var tecDate = formatDate(getFirstPossiblePickupDate(response));
                    pickupDatePicker.datepicker("setDate", tecDate);
                    setPickupDate(tecDate);
                } else {
                    var selectedDate = selectedPickupDate.val();
                    if (selectedDate && isValidPickupDate(new Date(selectedDate), response)) { // if pickupdate has been selected, set this date
                        pickupDatePicker.datepicker("setDate", selectedDate);
                        setPickupDate(selectedDate);
                    } else { // if pickupdate is not yet selected, set date from response + 1 day
                        pickupDatePicker.datepicker("setDate", minDate);
                        setPickupDate(minDate);
                    }
                }
            } else {
                $('#pickupDateNullInfo').removeClass('hidden');
            }
            pickupDatePicker.datepicker("refresh");
        }

        /**
         * get first possible pickup date when tec collect is possible, taking into consideration the scenario
         * when the firstPossiblePickupDate returns null from the server
         *
         * @param response the ajax response
         * @returns {Date|*} the minimum possible pickup date
         */
        function getFirstPossiblePickupDate(response) {
            if (response.tecCollectPossible) {
                return  getNextValidDate(new Date(), response, PICKUP);
            }

            return response && response.hasOwnProperty('firstPossiblePickupDate') ? getDate(response.firstPossiblePickupDate) : undefined;
        }

        /**
         * get the first possible date equal or after the provided date
         * @param date the minimum date
         * @param response the backend response with first possible date
         * @param deliveryMode SHIPPING or PICKUP
         * @returns the first valid pickup date
         */
        function getNextValidDate(date, response, deliveryMode) {
            let currentDate = date;
            while (true) {
                let newDate = currentDate.getDate() + 1;
                let isValid = deliveryMode === PICKUP ? isValidPickupDate(date, response) : isValidShippingDate(date);
                if (!isValid) {
                    currentDate = newDate;
                    continue;
                }

                return currentDate;
            }
        }

        /**
         * Update the variables of the shipping datepicker.
         *
         * @param response the ajax response
         */
        function updateShippingDatePicker(response) {
            initShippingDatepicker();
            shippingDatePicker.datepicker("option", "minDate", response.firstPossibleShippingDate);

            var selectedDate = selectedShippingDate.val();

            if (selectedDate && selectedDate >= response.firstPossibleShippingDate) {
                shippingDatePicker.datepicker("setDate", selectedDate);
                setShippingDate(selectedDate);
            } else {
                shippingDatePicker.datepicker("setDate", response.firstPossibleShippingDate);
                setShippingDate(response.firstPossibleShippingDate);
            }
        }

        /**
         * Determine which datepicker to show on loading the page.
         */
        function showRequiredDatepicker() {

            //if shipping has been chosen beforehand or pickup is not possible, show shipping datepicker
            if (isDIY === 'true' || (shippingModeCode.text() !== "" && (deliveryMethod.val() === shippingModeCode.text()))) {
                setDeliveryOption(SHIPPING)
                getShippingDates()
            } else {
                setDeliveryOption(PICKUP);
                getPickupDates(false);
            }
        }

        /**
         * Show the shipping datepicker and hide the pickup datepicker
         */
        function showShippingDatePicker() {
            $(".vmb-checkout-delivery__choiceoptions--pickup").addClass('hidden');
            $(".vmb-checkout-delivery__choiceoptions--shipping").removeClass('hidden');
            $(".vmk-checkout-delivery__delivery-comment").removeClass('hidden');
        }

        /**
         * Show the pickup datepicker and hide the shipping datepicker
         * Reset the delivery comment
         */
        function showPickupDatePicker() {
            $(".vmb-checkout-delivery__choiceoptions--pickup").removeClass('hidden');
            $(".vmb-checkout-delivery__choiceoptions--shipping").addClass('hidden');
            $(".vmk-checkout-delivery__delivery-comment").addClass('hidden');
            $('.vmk-checkout-delivery__delivery-comment').find('textarea').val('');
        }

        /**
         * Set the selected shipping date and the general delivery date to the given date.
         *
         * @param date the given date
         */
        function setShippingDate(date) {
            selectedShippingDate.val(date);
        }

        /**
         * Set the selected pickupdate and the general delivery date to the given date.
         *
         * @param date the given date
         */
        function setPickupDate(date) {
            selectedPickupDate.val(date);
        }

        /**
         * Sets the TEC uid to the form.
         *
         * @param tecId the TEC uid
         */
        function setTecUid(tecId) {
            tecUid.val(tecId);
        }

        /**
         * Set the delivery option on the form according to the given delivery method.
         *
         * @param method the delivery method
         */
        function setDeliveryOption(method) {
            if (PICKUP === method) {
                deliveryMethod.val($('#pickupModeCode').text())
                tecRadioButton.prop('checked', true);
                shippingRadioButton.prop('checked', false);
                $('.expectedPickupDateLabel').removeAttr('hidden');
                $('.expectedDeliveryDateLabel').attr('hidden', true);
            }
            if (SHIPPING === method) {
                deliveryMethod.val($('#shippingModeCode').text())
                shippingRadioButton.prop('checked', true);
                tecRadioButton.prop('checked', false);
                $('.expectedDeliveryDateLabel').removeAttr('hidden');
                $('.expectedPickupDateLabel').attr('hidden', true);
            }

        }

        /**
         * Updates the alternative TEC store dropdown list.
         *
         * @param response the response object
         */
        function updateAlternativeTecStoreDropDown(response) {
            var storesData = [];
            var altStores = response.alternativePointOfService;
            storesData.push({
                id: -1
            })

            if (altStores && altStores.length > 0) {
                $(".alternativeStores").removeClass('hidden');

                $.each(altStores, function () {
                    var message;

                    if (this.daysUntilPickup === 0) {
                        message = formatMessage(VMB.i18n.checkout.pointOfService.availability, [VMB.i18n.checkout.pointOfService.today])
                    } else if (this.daysUntilPickup === 1) {
                        message = formatMessage(VMB.i18n.checkout.pointOfService.availability, [VMB.i18n.checkout.pointOfService.tomorrow])
                    } else if (this.daysUntilPickup >= 1) {
                        message = formatMessage(VMB.i18n.checkout.pointOfService.future, [this.daysUntilPickup])
                    }
                    storesData.push({
                        id: this.name,
                        text: this.displayName + '|' + message,
                        disabled: !this.enabledOnSelectedDate,
                        selected: this.name === tecUid.val()
                    });
                });
            } else if (altStores && altStores.length === 0) {
                $(".alternativeStores").addClass('hidden');
            }

            var checkoutStoreSelectorItemRender = function (item) {
                var i = item.text.indexOf('|');
                if (i === -1) {
                    return item.text;
                } else {
                    var splits = [item.text.slice(0, i).trim(), item.text.slice(i + 1).trim()];

                    if (splits[1].length > 0) {
                        return $('<strong>' + splits[0] + '</strong><br><span>' + splits[1] + '</span>');
                    } else {
                        return $('<strong>' + splits[0] + '</strong>');
                    }
                }
            };

            $alternativeStoreDropDown.empty().select2({
                templateSelection: checkoutStoreSelectorItemRender,
                templateResult: checkoutStoreSelectorItemRender,
                data: storesData,
                minimumResultsForSearch: Infinity,
                dropdownCssClass: "checkout-store-selector-select2",
                placeholder: {
                    id: '-1',
                    text: VMB.i18n.checkout.placeholderAltTec
                },
                escapeMarkup: function (text) {
                    return text;
                }
            });

        }

        /**
         * Initializes the shipping datepicker.
         */
        function initShippingDatepicker() {
            shippingDatePicker.datepicker({
                dateFormat: "yy-mm-dd",
                showWeek: true,
                onSelect: function (selectedDate, inst) {
                    setShippingDate(selectedDate);
                    setProductDates(selectedDate, false);
                },
                beforeShowDay: function (date) {
                    return [isValidShippingDate(date)]
                },
            })
        }

        /**
         * Initializes the pickup datepicker based on the given response
         */
        function initPickupDatepicker(tecCollectPossible) {
            pickupDatePicker.datepicker({
                dateFormat: "yy-mm-dd",
                showWeek: true,
                onSelect: function (selectedDate, inst) {
                    setPickupDate(selectedDate);
                    setProductDates(selectedDate, tecCollectPossible);
                    getPickupDates(false, selectedDate);
                },
                beforeShowDay: function (date) {
                    return [isValidPickupDate(date, tecResponse)]
                },
            })
        }

        /**
         * Add 1 day to the given date if the date is today.
         *
         * @param date the date
         * @returns {String} the new date string
         */
        function getDatePlusOneDay(date) {
            var result = new Date(date);
            if (sameDay(result, new Date())) {
                result.setDate(result.getDate() + 1);
            }
            return formatDate(result);
        }

        /**
         * Determine whether the given day is today.
         *
         * @param d1 the date
         * @returns {boolean} true if today
         */
        function isToday(d1) {
            return sameDay(d1, new Date())
        }


        /**
         * Determines whether the given dates are on the same day.
         *
         * @param d1 the first day
         * @param d2 the second day
         * @returns {boolean} true if same day
         */
        function sameDay(d1, d2) {
            return d1.getFullYear() === d2.getFullYear() &&
                d1.getMonth() === d2.getMonth() &&
                d1.getDate() === d2.getDate();
        }

        /**
         * Set correct dates for all products in the checkout
         *
         * @param selectedDate
         */
        function setProductDates(selectedDate, tecCollectPossible) {

            $(".vmb-product-dates").each(function () {

                var firstDate = $(this).data("firstDate");
                var lastDate = $(this).data("lastDate");

                setOptionValues($(this), getNewOptionsProductDate(selectedDate, firstDate, lastDate, tecCollectPossible));

                $(this).val($(this).find("option:first").val());
                orderEntrySortByDate($(this));
            })
            // check if we need warning signs for order split scenario
            warnOnLaterAvailableDates();

            // show order-split popup if needed
            checkSplitOrderWarningNeeded(selectedDate);

            // change datepicker if we have equal dates on all entries
            disableDropdownsIfAllEqual(new Date(selectedDate));
        }

        /**
         * Creates options list for product dates dropdown
         * @param selectedDate
         * @param firstDate
         * @param lastDate
         * @returns {*|{}}
         */
        function getNewOptionsProductDate(selectedDate, firstDate, lastDate, tecCollectPossible) {

            newoptions = {};
            var selectedDateObj = moment(selectedDate);
            var firstDateObj = moment(firstDate);
            var lastDateObj = moment(lastDate);
            if (empty(firstDate) && empty(lastDate)) {
                newoptions[selectedDateObj.format("DD-MM-YYYY")] = selectedDate;
                return newoptions;
            }

            // case 1: selected date is express pickup in store today for all products
            if (tecCollectPossible && isToday(new Date(selectedDate))) {
                newoptions[selectedDateObj.format("DD-MM-YYYY")] = selectedDateObj.format("YYYY-MM-DD");
                return newoptions;
            }

            // case 2: selected date is only available for a split order (not this product)
            if (!empty(firstDate) && (selectedDateObj.isBefore(firstDateObj))) {
                newoptions[lastDateObj.format("DD-MM-YYYY")] = lastDateObj.format("YYYY-MM-DD");
                return newoptions;
            }

            if (firstDateObj.isSameOrBefore(selectedDateObj)) {
                newoptions[selectedDateObj.format("DD-MM-YYYY")] = selectedDate;
            }

            if (selectedDateObj.isSameOrBefore(lastDateObj)) {
                newoptions[lastDateObj.format("DD-MM-YYYY")] = lastDate;
            }
            return newoptions;
        }

        /**
         * method called on switching delivery modes - based on response from delivery backend controller
         * @param response delivery backend controller response on chosing the delivery mode
         * @param selectedValue default selected date on switching delivery modes
         */
        function resetProductEntryDates(response, selectedValue) {
            let isValidDate = shippingRadioButton.is(':checked') || (tecRadioButton.is(':checked') && isValidPickupDate(selectedValue, response));
            if (response.hasOwnProperty('products') && response.products.length > 0) {
                $.each(response.products, function (key, productEntryDate) {
                    if (isValidDate) {
                        resetProductDate(productEntryDate, selectedValue, response.tecCollectPossible);
                    } else {
                        displayNotAvailableTextOnDropdown($(".vmb-product-dates[data-product-code='" + productEntryDate.productCode + "']"));
                    }
                });
                // check if we need warning signs for order split scenario
                warnOnLaterAvailableDates();

                // show order-split popup if needed
                checkSplitOrderWarningNeeded(selectedValue);

                // change datepicker if we have equal dates on all entries
                disableDropdownsIfAllEqual(new Date(selectedValue));
            }
        }

        /**
         * check if one of the products has a first possible delivery date later than the selected one
         * @param selectedDate
         */
        function checkSplitOrderWarningNeeded(selectedDate) {
            let selectedDateObj = new Date(selectedDate);
            let maxUniqueDateObj = null;
            let showWarning = false;
            if (!checkAllDatesEqual()) {
                $(".vmb-product-dates").each(function () {
                    let dropDownDateObj = new Date($(this).val());
                    if (dropDownDateObj.getTime() > selectedDateObj.getTime()) {
                        if (maxUniqueDateObj === null) {
                            maxUniqueDateObj = dropDownDateObj;
                            showWarning = true;
                            return false;
                        }
                    }
                });
            }
            if (showWarning) {
                displayWarningMessageLaterDeliveryDate(maxUniqueDateObj);
            }
        }

        function resetProductDate(productEntryDate, selectedDate, tecCollectPossible) {
            var elem = $(".vmb-product-dates[data-product-code='" + productEntryDate.productCode + "']");

            if (elem && elem.length > 0) {
                // reset product dates with fresh data from the ajax response
                elem.data("firstDate", productEntryDate.firstPossibleDate);
                elem.data("lastDate", productEntryDate.lastPossibleDate);
                var alreadySetDate = elem.data("deliveryDate");
                var options = getNewOptionsProductDate(selectedDate, productEntryDate.firstPossibleDate, productEntryDate.lastPossibleDate, tecCollectPossible);
                setOptionValues(elem, options);

                if (typeof alreadySetDate !== "undefined") {
                    if (alreadySetDate in options) {
                        elem.val(options[alreadySetDate]);
                        orderEntrySortByDate(elem);
                        return;
                    }
                }

                elem.val(elem.find("option:first").val());
                orderEntrySortByDate(elem);
            }
        }

        /**
         * if not all products have the same selected delivery date, show warning on the products with
         * different date than the selected one
         * @param options available dates
         */
        function warnOnLaterAvailableDates() {
            let selectedDate = tecRadioButton.is(':checked') ? selectedPickupDate.val() : selectedShippingDate.val();
            let allDatesEqual = checkAllDatesEqual();
            $(".vmb-product-dates").each(function () {
                let currentValue = $(this).val();
                let productEntryParent = $(this).closest('.checkout-order-summary-list-items');
                if (!allDatesEqual) {
                    if (currentValue !== selectedDate) {
                        productEntryParent.find('.vmb-warning-sign').css('display', 'inline-block');
                    } else {
                        productEntryParent.find('.vmb-warning-sign').css('display', 'none');
                    }
                } else {
                    $('.getAccAlert').remove();
                    productEntryParent.find('.vmb-warning-sign').css('display', 'none');
                }
            });
        }

        /**
         * disable all dropdowns and set datepicker to selected dates, when all entries are deliverable to the selected date
         * @param selectedDate
         */
        function disableDropdownsIfAllEqual(selectedDate) {
            if (checkAllDatesEqual()) {
                setCurrentDatePicker(selectedDate);

                // disable all dropdowns
                $(".vmb-product-dates").each(function () {
                    $(this).prop("disabled", true);
                })

            }
        }

        /**
         * display N/A message on the product entry dropdown when date is not valid
         * @param elem the product entry date dropdown
         */
        function displayNotAvailableTextOnDropdown(elem) {
            elem.empty();
            elem.append($("<option></option>").attr("value", "dateNotAvailable").text($("#notAvailableText").val()));
            elem.prop("disabled", true);
        }

        /**
         * Helper Function to set option element to dropdown
         * @param elem
         * @param newOptions
         */
        function setOptionValues(elem, newOptions) {
            elem.empty(); // remove old options
            $.each(newOptions, function (key, value) {
                elem.append($("<option></option>").attr("value", value).text(key));
            });

            var disabled = true;
            var tmp = null;
            $.each(newOptions, function (key, value) {
                if (tmp == null) {
                    tmp = value;
                } else if (tmp !== value) {
                    disabled = false;
                }
            });

            if (disabled) {
                elem.prop("disabled", true);
            } else {
                elem.prop("disabled", false);
            }
        }

        $("#deliveryMethodForm").submit(function () {
            // Render products dates in the correct format
            var vmbProductDates = [];

            $(".vmb-product-dates").each(function () {
                vmbProductDates.push($(this).data("productCode") + "¤" + $(this).val());
            });

            $('#vmbProductDates').val(vmbProductDates.join('¦'));

            return true;
        });


        // ####  ADDRESS BOOK ####
        $(".vmb-btn-new-address").click(function (event) {
            event.preventDefault();
            $("#addressbook").slideUp();
            $(".vmb-btn-address-book").removeClass("open");

            $(".vmb-checkout-new-address").slideToggle();
            $(this).toggleClass("open");
        });

        $(".vmb-btn-address-book").click(function (event) {
            event.preventDefault();
            $(".vmb-checkout-new-address").slideUp();
            $(".vmb-btn-new-address").removeClass("open");

            $("#addressbook").slideToggle();
            $(this).toggleClass("open");
        });

        $(".vmb-checkout-new-address-save").change(function () {
            if ($(".vmb-checkout-new-address-save").is(':checked')) {
                $(".vmb-checkout-new-address-default").closest("label").removeClass('hidden');
            } else {
                $(".vmb-checkout-new-address-default").closest("label").addClass('hidden');
                $(".vmb-checkout-new-address-default").prop('checked', false);
            }
        });

        /**
         * Display warning message for setting the delivery date to latest possible date to accomodate all cart entries
         *
         * @param maxDate - the max date that can be set
         */
        function displayWarningMessageLaterDeliveryDate(maxDate) {
            let globalAlerts = $(".global-alerts");
            globalAlerts.empty();

            if (globalAlerts.length === 0) {
                $("body").prepend("<div class='global-alerts'></div>");
                globalAlerts = $(".global-alerts");
            }

            let placeholderRegex = /\{0\}/;
            let maxDateLink = moment(maxDate, "YYYY-MM-DD").format("DD-MM-YYYY");

            let warnMessage = warnMessageInput.val();

            let warnPopupMessage = ($('#global-alert-warning-template')).tmpl({message: warnMessage});
            let changeDateLink = "<a href=\"#\" id=\"vmb-latest-delivery-date-btn\">" + maxDateLink + "</a>";
            let html = $("<div />").append(warnPopupMessage.clone()).html();
            let warnPopupMessageHtml = html.replace(placeholderRegex, changeDateLink);

            globalAlerts.append(warnPopupMessageHtml);
            $("#vmb-latest-delivery-date-btn").on("click", setAllDatesToLaterDate);

            $(".closeAccAlert").on("click", function () {
                $(this).parent('.getAccAlert').remove();
            });
        }

        /**
         * set all dates on the order entries date picker to the maximum date from the warn message
         * @param event click event
         */
        function setAllDatesToLaterDate(event) {
            event.preventDefault();
            let maxDate = $(this).text();
            let maxDateFormatted = moment(maxDate, 'DD-MM-YYYY').format('YYYY-MM-DD');
            $(".vmb-product-dates").each(function () {
                let maxDateOption = $(this).find("option[value=\"" + maxDateFormatted + "\"]").val();
                if (typeof maxDateOption === "undefined") {
                    $(this).append($("<option></option>").attr("value", maxDateFormatted).text(maxDate));
                }
                $(this).val($(this).find("option[value=\"" + maxDateFormatted + "\"]").val()).change();
                let productEntryParent = $(this).closest('.checkout-order-summary-list-items');
                productEntryParent.find('.vmb-warning-sign').css('display', 'none');
            });

            setCurrentDatePicker(new Date(maxDateFormatted));
            $(this).parent('.getAccAlert').remove();
        }

        /**
         * Set current datepicker - either
         * @param date Date object
         */
        function setCurrentDatePicker(date) {
            let formattedDate = formatDate(date);
            if (tecRadioButton.is(':checked')) {
                pickupDatePicker.datepicker("setDate", formattedDate);
                setPickupDate(formattedDate)
            } else {
                shippingDatePicker.datepicker("setDate", formattedDate);
                setShippingDate(formattedDate);
            }

        }

        /**
         * ensure all product entries are ordered in reverse ordered by selected delivery date
         */
        $(".vmb-product-dates").change(function (event) {
            event.preventDefault();
            // check if we need to re-order the entry
            orderEntrySortByDate($(this));

            // check if we need warning signs for order split scenario
            warnOnLaterAvailableDates();

            // change datepicker if we have equal dates on all entries
            disableDropdownsIfAllEqual(new Date(this.value));
        });

        function orderEntrySortByDate(entry) {
            let productElem = entry.closest(".checkout-order-summary-list-items");
            let currentDate = moment(entry.val(), 'YYYY-MM-DD');

            // move current changed date dropdown product up or down until the order entry list is in right order by delivery date
            while (true) {
                let previousProduct = productElem.prev(".checkout-order-summary-list-items");
                let goingUp = false;
                let goingDown = false;

                if (previousProduct.length > 0) {
                    let previousDate = moment(previousProduct.find(".vmb-product-dates").val(), 'YYYY-MM-DD');
                    if (currentDate.isAfter(previousDate)) {
                        productElem.insertBefore(previousProduct);
                        goingUp = true;
                    }
                }

                if (!goingUp) {
                    let nextProduct = productElem.next(".checkout-order-summary-list-items");
                    if (nextProduct.length > 0) {
                        let nextDate = moment(nextProduct.find(".vmb-product-dates").val(), 'YYYY-MM-DD');
                        if (nextDate.isAfter(currentDate)) {
                            productElem.insertAfter(nextProduct);
                            goingDown = true;
                        }
                    }
                }

                if (!goingUp && !goingDown) {
                    break;
                }
            }
        }

        /**
         * check if all selected dates are equal, in case of false the order is split
         * @returns {boolean} true for all dates equal, false for order split
         */
        function checkAllDatesEqual() {
            const dates = new Set();
            $(".vmb-product-dates").each(function () {
                dates.add($(this).val());
            })

            return dates.size === 1;
        }
    }


    if ($(".checkout-paymentmethod").length > 0) {

        $(".js-chckt-pm-radio-button").click(function () {
            ACC.track.checkoutStep(2, $(this).val());
        })

    }

    if ($("#contactOrderManagerForm").length > 0) {

        $(".vmb-checkout-confirmForm-open").click(function () {
            $("#vmbCheckoutConfirmFormModal").modal();
        });

        $(".vmb-checkout-confirmForm-button").click(function () {

            $(this).prop("disabled", true);

            if (!_.isEmpty($("#contactComment").val())) {
                $("#contactOrderManagerForm").ajaxSubmit({
                    success: function (data) {
                        vmbMessages.createSuccess("contactOrderManagerFormSuccess", $("#contactOrderManagerFormSuccess").html(), true);
                        $("#vmbCheckoutConfirmFormModal").modal('hide');
                    },
                    error: function (xht, textStatus, ex) {
                        vmbMessages.createError("contactOrderManagerFormError", $("#contactOrderManagerFormError").html(), true);
                        $("#vmbCheckoutConfirmFormModal").modal('hide');
                    }
                });
            } else {
                $(this).prop("disabled", false);
                $("#contactComment").parent().addClass("has-error");
            }
        });

        $("#vmbCheckoutConfirmFormModal").on('hidden.bs.modal', function () {
            $("#contactOrderManagerForm").resetForm();
            $(".vmb-checkout-confirmForm-button").prop("disabled", false);
            $("#contactComment").parent().removeClass("has-error");
        });
    }

    $("#placeOrder").click(function () {
        showLoader();
    });

    if ($(".vmb-checkout-confirmation").length) {
        $("#show_netto_price_toggle").prop("disabled", true);
    }

    $(".vmb-checkout-new-address__submit").click(function () {
        cleanUpFormErrors("#addressForm");

        var addressForm = $("#addressForm");
        if (addressForm.valid()) {

            $(".vmb-checkout-new-address__submit")
                .prop("disabled", true)
                .addClass("btn--loading");

            var addressData = {
                line1: addressForm.find("#address-line1").val(),
                line2: addressForm.find("#address-line2").val(),
                apartment: addressForm.find("#address-apartment").val(),
                town: addressForm.find("#address-townCity").val(),
                postalCode: addressForm.find("#address-postcode").val(),
                country: {
                    isocode: addressForm.find("#address-country").val()
                }
            };

            var addressForm = {
                firstName: addressForm.find("#address-firstName").val(),
                lastName: addressForm.find("#address-surname").val(),
                line1: addressForm.find("#address-line1").val(),
                line2: addressForm.find("#address-line2").val(),
                apartment: addressForm.find("#address-apartment").val(),
                townCity: addressForm.find("#address-townCity").val(),
                postcode: addressForm.find("#address-postcode").val(),
                phone: addressForm.find("#address-phone").val(),
                mobile: addressForm.find("#address-mobile").val(),
                countryIso: addressForm.find("#address-country").val(),
                saveInAddressBook: addressForm.find("#saveAddressInMyAddressBook").prop('checked'),
                defaultAddress: addressForm.find("#defaultAddress").prop('checked')
            };

            VMB.addressValidation.validate(addressData).then(function (validatedAddress) {
                addressForm = copyValidatedAddress(addressForm, validatedAddress);
                createAddress(addressForm);
            })['catch'](function (error) {
                if (error === VMB.addressValidation.CLOSED) {
                    $(".vmb-checkout-new-address__submit")
                        .prop("disabled", false)
                        .removeClass("btn--loading");
                    return true;
                }
                createAddress(addressForm);
            });
        }
    });
})();

/**
 * Copies the values of the validated address over to the address form which is used for the address creation.
 *
 * @param addressForm the address form
 * @param validatedAddress the validated address
 * @return {*} the updated address form
 */
function copyValidatedAddress(addressForm, validatedAddress) {
    addressForm.line1 = validatedAddress.line1;
    addressForm.line2 = validatedAddress.line2;
    addressForm.townCity = validatedAddress.town;
    addressForm.postcode = validatedAddress.postalCode;
    addressForm.countryIso = validatedAddress.country.isocode;
    return addressForm;
}

/**
 * Created an address for the given {@code addressForm}.
 *
 * @param addressForm the address form
 */
function createAddress(addressForm) {
    $.ajax({
        url: $('.vmb-checkout-new-address__submit').data('url'),
        method: 'POST',
        dataType: 'json',
        data: addressForm,
        beforeSend: function () {
            showLoader();
        },
        success: function (data) {
            window.location.reload();
        },
        complete: function (data) {
            $('#vmbAddressBookModal').modal('toggle');
        }
    });
}

/**
 * Performs an AJAX call to set the address with the given address id on the current session cart.
 * <p>
 * It receives the address data to update the front end with the current selected address.
 *
 * @param addressId the address id
 */
function selectAddress(addressId) {
    var isPageReload = false;
    $.ajax({
        url: $('.vmb-select-address-btn__submit').data('url') + '/' + addressId,
        method: 'GET',
        dataType: 'json',
        beforeSend: function () {
            showLoader();
        },
        success: function (data) {
            if (data.hasOwnProperty("isCartRecalculated") && data.isCartRecalculated === true) {
                window.location.reload();
                isPageReload = true;
            } else {
                editAddressView(data);
            }
        },
        complete: function (data) {
            if (!isPageReload) {
                hideLoader();
            }
            $('#vmbAddressBookModal').modal('toggle');
        }
    });
}

/**
 * Updates the address fields in the first step of the checkout process after a different address is chosen.
 *
 * @param address the address data
 */
function editAddressView(address) {
    if (address) {
        setName(address);
        setStreet(address);
        setPostalCode(address.postalCode)
        setTown(address.town)
        setAddressTelephone(address.phone);
        setAddressCellTelephone(address.mobile);
        setDeliveryMode(address.zoneDeliveryMode);
    }
}


/**
 * Retrieves the first/lastname or the company name from the given address.
 *
 * @param address the address
 * @returns the first/lastname or the company name
 */
function getAddressName(address) {
    var name = "";
    if (address.firstName) {
        name = address.firstName;
    }

    if (address.lastName) {
        name = name + ' ' + address.lastName;
    }

    if (!address.firstName && !address.lastName && address.companyName) {
        name = address.companyName;
    }

    return name.trim();
}

/**
 * Sets the name on the form.
 *
 * @param address the address
 */
function setName(address) {
    $('#vmb-checkout-delivery__address-name').html(getAddressName(address));
}

/**
 * Retrieves the street name and number from the given address.
 *
 * @param address the address
 * @returns the street
 */
function getAddressStreet(address) {
    var addressString = '';

    if (address.line1) {
        addressString += address.line1;
    }

    if (address.line2) {
        addressString += ' ';
        addressString += address.line2;
    }

    if (address.apartment) {
        addressString += ' ';
        addressString += address.apartment;
    }

    return addressString.trim();
}

/**
 * Sets the street on the form.
 *
 * @param address the address
 */
function setStreet(address) {
    $('#vmb-checkout-delivery__address-street').html(getAddressStreet(address));
}

/**
 * Sets the postal code on the form.
 *
 * @param postalCode the postal code
 */
function setPostalCode(postalCode) {
    $('#vmb-checkout-delivery__address-postalcode').html(postalCode);
}

/**
 * Sets the town on the form.
 *
 * @param town the town
 */
function setTown(town) {
    $('#vmb-checkout-delivery__address-town').html(town);
}

/**
 * Sets the telephone number on the form.
 *
 * @param phone the phone number
 */
function setAddressTelephone(phone) {
    if (phone) {
        $('#vmb-checkout-delivery__address-telephone-label').removeClass('hidden');
        $('#vmb-checkout-delivery__address-telephone').html(phone.trim());
    } else {
        $('#vmb-checkout-delivery__address-telephone-label').addClass('hidden');
        $('#vmb-checkout-delivery__address-telephone').html('');
    }
}

/**
 * Sets the cell telephone number on the form.
 *
 * @param mobile the cell phone number
 */
function setAddressCellTelephone(mobile) {
    if (mobile) {
        $('#vmb-checkout-delivery__address-mobile-label').removeClass('hidden');
        $('#vmb-checkout-delivery__address-mobile').html(mobile.trim());
    } else {
        $('#vmb-checkout-delivery__address-mobile-label').addClass('hidden');
        $('#vmb-checkout-delivery__address-mobile').html('');
    }
}

/**
 * Sets the delivery mode on the form.
 *
 * @param deliveryMode the delivery mode
 */
function setDeliveryMode(deliveryMode) {
    if (deliveryMode) {
        $('#vmbDeliveryOption').val(deliveryMode.trim());
    }
}


/**
 * Formats the given date to a string with format yyyy-MM-dd
 * @param date the date object
 * @returns {string} the date as string
 */
function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();
    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;
    return [year, month, day].join('-');
}