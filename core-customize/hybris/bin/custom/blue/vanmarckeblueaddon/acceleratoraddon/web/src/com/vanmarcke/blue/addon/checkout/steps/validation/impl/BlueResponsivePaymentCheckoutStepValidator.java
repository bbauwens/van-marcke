package com.vanmarcke.blue.addon.checkout.steps.validation.impl;

import com.vanmarcke.blue.addon.facades.BlueCheckoutFlowFacade;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.validation.AbstractCheckoutStepValidator;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.validation.ValidationResults;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import static de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages.addFlashMessage;

public class BlueResponsivePaymentCheckoutStepValidator extends AbstractCheckoutStepValidator {

    private static final Logger LOGGER = LoggerFactory.getLogger(BlueResponsivePaymentCheckoutStepValidator.class);

    @Override
    public ValidationResults validateOnEnter(final RedirectAttributes redirectAttributes) {
        if (!getCheckoutFlowFacade().hasValidCart()) {
            LOGGER.info("Missing, empty or unsupported cart");
            return ValidationResults.REDIRECT_TO_CART;
        }

        if (getCheckoutFlowFacade().hasNoDeliveryAddress()) {
            addFlashMessage(redirectAttributes, GlobalMessages.INFO_MESSAGES_HOLDER, "checkout.multi.deliveryAddress.notprovided");
            return ValidationResults.REDIRECT_TO_DELIVERY_METHOD;
        }

        if (getCheckoutFlowFacade().hasNoDeliveryMode()) {
            addFlashMessage(redirectAttributes, GlobalMessages.INFO_MESSAGES_HOLDER, "checkout.multi.deliveryMethod.notprovided");
            return ValidationResults.REDIRECT_TO_DELIVERY_METHOD;
        }

        if (((BlueCheckoutFlowFacade) getCheckoutFlowFacade()).hasNoDeliveryPointOfService()) {
            addFlashMessage(redirectAttributes, GlobalMessages.INFO_MESSAGES_HOLDER, "checkout.multi.deliveryPointOfService.notprovided");
            return ValidationResults.REDIRECT_TO_DELIVERY_METHOD;
        }

        return ValidationResults.SUCCESS;
    }
}