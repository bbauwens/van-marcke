package com.vanmarcke.blue.addon.controllers.misc;

import com.vanmarcke.facades.principal.user.VMKUserFacade;
import com.vanmarcke.facades.storesession.VMKStoreSessionFacade;
import de.hybris.platform.acceleratorservices.uiexperience.UiExperienceService;
import de.hybris.platform.acceleratorservices.urlencoder.UrlEncoderService;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.AbstractController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.storelocator.data.PointOfServiceData;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.commerceservices.enums.UiExperienceLevel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.RequestContextUtils;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

/**
 * Controller for store session. Used to change the session language, site and experience level.
 */
@Controller
@RequestMapping("/_s")
public class BlueStoreSessionController extends AbstractController {

    private static final Logger LOG = Logger.getLogger(BlueStoreSessionController.class);
    public static final String ORIGINAL_REFERER = "originalReferer";

    @Resource(name = "storeSessionFacade")
    private VMKStoreSessionFacade storeSessionFacade;

    @Resource(name = "userFacade")
    private VMKUserFacade userFacade;

    @Resource(name = "uiExperienceService")
    private UiExperienceService uiExperienceService;

    @Resource(name = "enumerationService")
    private EnumerationService enumerationService;

    @Resource(name = "urlEncoderService")
    private UrlEncoderService urlEncoderService;

    @Resource(name = "cmsSiteService")
    private CMSSiteService cmsSiteService;

    @RequestMapping(value = "/language", method = {RequestMethod.GET, RequestMethod.POST})
    public String selectLanguage(@RequestParam("code") final String isoCode, final HttpServletRequest request) {
        final String previousLanguage = getCurrentLanguage(request);
        this.storeSessionFacade.setCurrentLanguage(isoCode);
        if (!this.userFacade.isAnonymousUser()) {
            this.userFacade.syncSessionLanguage();
        }
        return this.urlEncoderService.isLanguageEncodingEnabled() ? getReturnRedirectUrlForBlueWithUrlEncoding(request, previousLanguage,
                this.storeSessionFacade.getCurrentLanguage().getIsocode()) : getReturnRedirectUrlWithoutReferer(request);
    }

    @RequestMapping(value = "/store", method = {RequestMethod.POST})
    public void selectStore(@RequestParam("storeName") final String storeName, final HttpServletResponse response) {
        selectStoreForCurrentSession(storeName);
    }

    private void selectStoreForCurrentSession(String storeName) {
        final String trimmedStoreName = StringUtils.trimToNull(storeName);
        storeSessionFacade.setCurrentStore(trimmedStoreName);
        if (!userFacade.isAnonymousUser()) {
            userFacade.syncSessionStore();
        }
    }

    @RequestMapping(value = "/stores", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<PointOfServiceData> getAllStores() {
        List<PointOfServiceData> pointOfServiceDataList;
        try {
            pointOfServiceDataList = this.storeSessionFacade.getAllStores();
        } catch (final Exception e) {
            LOG.error("Could not get all Stores " + e.getMessage(), e);
            pointOfServiceDataList = Collections.emptyList();
        }
        return pointOfServiceDataList;
    }

    @RequestMapping(value = "/netprice", method = RequestMethod.PUT)
    @ResponseBody
    public boolean saveNetPricePreference(@RequestParam(value = "showNetPrice", defaultValue = "false") final boolean showNetPrice) {
        return this.userFacade.saveNetPricePreference(showNetPrice);
    }

    /**
     * Change the site to the one requested based on the passed uid
     *
     * @param uid     the uid of the requested site
     * @param request the request
     * @return the correct url for the site
     */
    @RequestMapping(value = "/site", method = {RequestMethod.GET, RequestMethod.POST})
    public String selectSite(@RequestParam("code") final String uid, final HttpServletRequest request) {
        final String previousLanguage = getCurrentLanguage(request);
        storeSessionFacade.setCurrentSite(uid);

        restorePointOfService();

        if (!this.userFacade.isAnonymousUser()) {
            this.userFacade.syncSessionSite();
            this.userFacade.syncSessionLanguage();
        }

        return urlEncoderService.isLanguageEncodingEnabled() ? getReturnRedirectUrlForBlueWithUrlEncoding(request, previousLanguage, storeSessionFacade.getCurrentLanguage().getIsocode()) : getReturnRedirectUrlWithoutReferer(request);
    }

    /**
     * Returns the current language based on the request URI.
     *
     * @param request the HTTP request
     * @return the current language
     */
    private String getCurrentLanguage(final HttpServletRequest request) {
        String language = null;
        if (StringUtils.isNotEmpty(request.getRequestURI())) {
            final String[] segments = request.getRequestURI().split("/");
            if (ArrayUtils.isNotEmpty(segments) && segments.length >= 2) {
                language = isDIY() ? segments[2] : segments[1];
            }
        }
        return language;
    }

    /**
     * Restores the point of service in the session.
     */
    private void restorePointOfService() {
        if (userFacade.isAnonymousUser()) {
            final List<PointOfServiceData> pointsOfService = getAllStores();
            if (CollectionUtils.isEmpty(pointsOfService)) {
                return;
            }
            selectStoreForCurrentSession(getDefaultPos(pointsOfService));
        } else {
            PointOfServiceModel pos = userFacade.getNearestStoreForB2BCustomer(cmsSiteService.getCurrentSite().getStores().get(0));
            if (Objects.nonNull(pos)) {
                selectStoreForCurrentSession(pos.getName());
            }
        }
    }

    /**
     * Sets closest point of service as the default point of service.
     *
     * @param pointsOfService the points of service
     * @return the default point of service
     */
    private String getDefaultPos(final List<PointOfServiceData> pointsOfService) {
        pointsOfService.sort(Comparator.comparing(PointOfServiceData::getDistanceKm, Comparator.nullsLast(Comparator.naturalOrder())));
        return pointsOfService.get(0).getName();
    }

    /**
     * Checks whether the given {@code cookiePos} is a valid point of service, by comparing it to the available
     * {@code stores}.
     *
     * @param stores    the points of service
     * @param cookiePos the point of service stored in the cookie
     * @return {@code true} in case the given {@code cookiePos} is a valid point of service, {@code false} otherwise
     */
    private boolean isValidPos(final List<PointOfServiceData> stores, final String cookiePos) {
        return stores
                .stream()
                .anyMatch(store -> cookiePos.equals(store.getName()));
    }

    @RequestMapping(value = "/ui-experience", method = {RequestMethod.GET, RequestMethod.POST})
    public String selectUiExperienceLevel(@RequestParam("level") final String uiExperienceLevelString,
                                          final HttpServletRequest request) {
        if (uiExperienceLevelString == null || uiExperienceLevelString.isEmpty()) {
            // Empty value - clear the override
            this.uiExperienceService.setOverrideUiExperienceLevel(null);
        } else {
            final UiExperienceLevel uiExperienceLevel = toUiExperienceLevel(uiExperienceLevelString);
            if (uiExperienceLevel == null) {
                LOG.warn("Unknown UiExperience level [" + uiExperienceLevelString + "] available values are: "
                        + Arrays.toString(getAvailableUiExperienceLevelsCodes()));
            } else {
                this.uiExperienceService.setOverrideUiExperienceLevel(uiExperienceLevel);
            }
        }

        // Always clear the prompt hide flag
        setHideUiExperienceLevelOverridePrompt(request, false);
        return getReturnRedirectUrl(request);
    }

    protected UiExperienceLevel toUiExperienceLevel(final String code) {
        if (code != null && !code.isEmpty()) {
            try {
                return this.enumerationService.getEnumerationValue(UiExperienceLevel.class, code);
            } catch (final UnknownIdentifierException ignore) {
                // Ignore, return null
            }
        }
        return null;
    }

    protected List<UiExperienceLevel> getAvailableUiExperienceLevels() {
        return this.enumerationService.getEnumerationValues(UiExperienceLevel.class);
    }

    protected String[] getAvailableUiExperienceLevelsCodes() {
        final List<UiExperienceLevel> availableUiExperienceLevels = getAvailableUiExperienceLevels();
        if (availableUiExperienceLevels == null || availableUiExperienceLevels.isEmpty()) {
            return new String[0];
        }

        final String[] codes = new String[availableUiExperienceLevels.size()];
        for (int i = 0; i < codes.length; i++) {
            codes[i] = availableUiExperienceLevels.get(i).getCode();
        }

        return codes;
    }

    @RequestMapping(value = "/ui-experience-level-prompt", method =
            {RequestMethod.GET, RequestMethod.POST})
    public String selectUiExperienceLevelPrompt(@RequestParam("hide") final boolean hideFlag, final HttpServletRequest request) {
        setHideUiExperienceLevelOverridePrompt(request, hideFlag);
        return getReturnRedirectUrl(request);
    }

    protected void setHideUiExperienceLevelOverridePrompt(final HttpServletRequest request, final boolean flag) {
        request.getSession().setAttribute("hideUiExperienceLevelOverridePrompt", Boolean.valueOf(flag));
    }

    protected String getReturnRedirectUrl(final HttpServletRequest request) {
        final String referer = request.getHeader("Referer");
        if (referer != null && !referer.isEmpty()) {
            return REDIRECT_PREFIX + referer;
        }
        return REDIRECT_PREFIX + '/';
    }

    protected String getReturnRedirectUrlWithoutReferer(final HttpServletRequest request) {
        final String originalReferer = (String) request.getSession().getAttribute(ORIGINAL_REFERER);
        if (StringUtils.isNotBlank(originalReferer)) {
            return REDIRECT_PREFIX + originalReferer;
        }

        final String referer = StringUtils.remove(request.getRequestURL().toString(), request.getServletPath());
        if (referer != null && !referer.isEmpty()) {
            return REDIRECT_PREFIX + referer;
        }
        return REDIRECT_PREFIX + '/';
    }

    protected String getReturnRedirectUrlForBlueWithUrlEncoding(final HttpServletRequest request, final String old, final String current) {
        final String originalReferer = (String) request.getSession().getAttribute(ORIGINAL_REFERER);
        if (StringUtils.isNotBlank(originalReferer) && originalReferer.contains(old)) {
            return REDIRECT_PREFIX + StringUtils.replace(originalReferer, "/" + old, "/" + current);
        }

        String referer = StringUtils.remove(request.getRequestURL().toString(), request.getServletPath());
        if (!StringUtils.endsWith(referer, "/")) {
            referer = referer;
        }
        if (StringUtils.isNotBlank(referer) && StringUtils.contains(referer, "/" + old + "/")) {
            return REDIRECT_PREFIX + StringUtils.replace(referer, "/" + old + "/", "/" + current + "/");
        }
        return REDIRECT_PREFIX + referer;
    }

    @ExceptionHandler(UnknownIdentifierException.class)
    public String handleUnknownIdentifierException(final UnknownIdentifierException exception, final HttpServletRequest request) {
        final Map<String, Object> currentFlashScope = RequestContextUtils.getOutputFlashMap(request);
        currentFlashScope.put(GlobalMessages.ERROR_MESSAGES_HOLDER, exception.getMessage());
        return REDIRECT_PREFIX + "/404";
    }

    private boolean isDIY() {
        return cmsSiteService.getCurrentSite().getChannel().equals(SiteChannel.DIY);
    }
}
