<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="galleryImages" required="true" type="java.util.List" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/product" %>
<spring:theme code="img.missingProductImage.responsive.product" var="imagePath" htmlEscape="false"/>

<spring:htmlEscape defaultHtmlEscape="true"/>

<c:choose>
    <c:when test="${originalContextPath ne null}">
        <c:url value="${imagePath}" var="imageUrl" context="${originalContextPath}"/>
    </c:when>
    <c:otherwise>
        <c:url value="${imagePath}" var="imageUrl"/>
    </c:otherwise>
</c:choose>

<div class="vmb-image-gallery">
    <c:choose>
        <c:when test="${galleryImages == null || galleryImages.size() == 0}">
            <div class="vmb-product--no-image">
                <img class="" src="${imageUrl}" alt="no-image"/>
            </div>
        </c:when>
        <c:otherwise>
            <div class="vmb-product-images sp-wrap">
                <c:set var="imgShown" value="false"/>
                <c:forEach items="${galleryImages}" var="container" varStatus="varStatus">
                    <c:if test="${not empty container.superZoom}">
                        <c:set var="imgShown" value="true"/>
                        <a href="${container.superZoom.url}" class="item">
                            <img class="" src="${container.superZoom.url}"
                                 alt="${fn:escapeXml(container.thumbnail.altText)}"
                                 onerror="if (this.src != '${imageUrl}'){this.src = '${imageUrl}';this.parentNode.href='${imageUrl}';}">
                        </a>
                    </c:if>
                    <c:if test="${not empty container.VIDEO}">
                        <c:set var="imgShown" value="true"/>
                        <a class="item-video" href="${container.VIDEO.url}">
                            <img class="" src="/_ui/responsive/theme-vanmarckeblue/images/video.png" alt="video">
                        </a>
                    </c:if>
                </c:forEach>
                <c:if test="${imgShown eq false}">
                    <div class="vmb-product--no-image">
                        <img class="" src="${imageUrl}" alt="no-image"/>
                    </div>
                </c:if>
            </div>
        </c:otherwise>
    </c:choose>
</div>