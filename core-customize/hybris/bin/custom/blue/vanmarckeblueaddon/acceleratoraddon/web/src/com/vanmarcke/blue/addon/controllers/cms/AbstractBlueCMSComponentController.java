package com.vanmarcke.blue.addon.controllers.cms;

import com.vanmarcke.blue.addon.constants.VanmarckeblueaddonConstants;
import de.hybris.platform.addonsupport.controllers.cms.AbstractCMSAddOnComponentController;
import de.hybris.platform.cms2.model.contents.components.AbstractCMSComponentModel;

/**
 * Abstract accelerator CMS component controller providing a common implementation for the getView method.
 */
public abstract class AbstractBlueCMSComponentController<T extends AbstractCMSComponentModel>
        extends AbstractCMSAddOnComponentController<T> {

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getAddonUiExtensionName(T component) {
        return VanmarckeblueaddonConstants.EXTENSIONNAME;
    }
}