package com.vanmarcke.blue.addon.controllers.pages;

import com.vanmarcke.blue.addon.constants.VanmarckeblueaddonWebConstants.ProductListViewType;
import com.vanmarcke.blue.addon.controllers.VanmarckeblueaddonControllerConstants;
import com.vanmarcke.facades.search.VMKSearchFacade;
import de.hybris.platform.acceleratorcms.model.components.SearchBoxComponentModel;
import de.hybris.platform.acceleratorservices.controllers.page.PageType;
import de.hybris.platform.acceleratorservices.customer.CustomerLocationService;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.impl.SearchBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.util.MetaSanitizerUtil;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.servicelayer.services.CMSComponentService;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.search.ProductSearchFacade;
import de.hybris.platform.commercefacades.search.data.AutocompleteResultData;
import de.hybris.platform.commercefacades.search.data.SearchQueryData;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commerceservices.enums.SearchQueryContext;
import de.hybris.platform.commerceservices.search.facetdata.FacetData;
import de.hybris.platform.commerceservices.search.facetdata.FacetRefinement;
import de.hybris.platform.commerceservices.search.facetdata.ProductSearchPageData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collections;
import java.util.List;

@Controller
@RequestMapping("/search")
public class BlueSearchPageController extends AbstractBlueSearchPageController {

    private static final String SEARCH_META_DESCRIPTION_ON = "search.meta.description.on";
    private static final String SEARCH_META_DESCRIPTION_RESULTS = "search.meta.description.results";

    private static final String COMPONENT_UID_PATH_VARIABLE_PATTERN = "{componentUid:.*}";

    private static final String SEARCH_CMS_PAGE_ID = "search";
    private static final String NO_RESULTS_CMS_PAGE_ID = "searchEmpty";

    @Resource(name = "productSearchFacade")
    private ProductSearchFacade<ProductData> productSearchFacade;

    @Resource(name = "searchBreadcrumbBuilder")
    private SearchBreadcrumbBuilder searchBreadcrumbBuilder;

    @Resource(name = "customerLocationService")
    private CustomerLocationService customerLocationService;

    @Resource(name = "cmsComponentService")
    private CMSComponentService cmsComponentService;

    @Resource(name = "vmkCategorySearchFacade")
    private VMKSearchFacade<CategoryData> vmkSearchFacade;

    @RequestMapping(method = RequestMethod.GET, params = "!q")
    public String textSearch(@RequestParam(value = "text", defaultValue = "") final String searchText,
                             @RequestParam(value = "sort", required = false) final String sort,
                             @RequestParam(value = "viewType", required = false) final ProductListViewType viewType,
                             final HttpServletRequest request, final HttpServletResponse response,
                             final Model model) throws CMSItemNotFoundException {
        // determine correct list view depending on the cookie stored value
        ProductListViewType desiredViewType = getAndStorePLPViewType(viewType, request, response);
        model.addAttribute("viewType", desiredViewType);

        if (StringUtils.isNotBlank(searchText)) {
            final PageableData pageableData = createPageableData(0, getSearchPageSize(), sort, ShowMode.Page);

            final SearchStateData searchState = new SearchStateData();
            final SearchQueryData searchQueryData = new SearchQueryData();
            searchQueryData.setValue(searchText);
            searchState.setQuery(searchQueryData);

            ProductSearchPageData<SearchStateData, ProductData> searchPageData = null;
            try {
                searchPageData = encodeSearchPageData(this.productSearchFacade.textSearch(searchState, pageableData));
                if (ProductListViewType.List == desiredViewType) {
                    populateProductVariants(searchPageData, searchText, null);
                }

            } catch (final ConversionException e) // NOSONAR
            {
                // nothing to do - the exception is logged in SearchSolrQueryPopulator
            }

            if (searchPageData == null) {
                storeCmsPageInModel(model, getContentPageForLabelOrId(NO_RESULTS_CMS_PAGE_ID));
            } else if (searchPageData.getKeywordRedirectUrl() != null) {
                // if the search engine returns a redirect, just
                return "redirect:" + searchPageData.getKeywordRedirectUrl();
            } else if (searchPageData.getPagination().getTotalNumberOfResults() == 0) {
                model.addAttribute("searchPageData", searchPageData);
                storeCmsPageInModel(model, getContentPageForLabelOrId(NO_RESULTS_CMS_PAGE_ID));
                updatePageTitle(searchText, model);
            } else if (shouldRedirectToPDP(searchPageData)) {
                return "redirect:" + searchPageData.getResults().get(0).getUrl();
            } else {
                storeContinueUrl(request);
                populateModel(model, searchPageData, ShowMode.Page);
                storeCmsPageInModel(model, getContentPageForLabelOrId(SEARCH_CMS_PAGE_ID));
                updatePageTitle(searchText, model);
            }
            model.addAttribute("userLocation", this.customerLocationService.getUserLocation());
            getRequestContextData(request).setSearch(searchPageData);
            if (searchPageData != null) {
                model.addAttribute(WebConstants.BREADCRUMBS_KEY, this.searchBreadcrumbBuilder.getBreadcrumbs(null, searchText,
                        CollectionUtils.isEmpty(searchPageData.getBreadcrumbs())));
            }
        } else {
            storeCmsPageInModel(model, getContentPageForLabelOrId(NO_RESULTS_CMS_PAGE_ID));
        }

        model.addAttribute("searchText", searchText);
        model.addAttribute("pageType", PageType.PRODUCTSEARCH.name());
        model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_FOLLOW);

        final String metaDescription = MetaSanitizerUtil
                .sanitizeDescription(getMessageSource().getMessage(SEARCH_META_DESCRIPTION_RESULTS, null,
                        SEARCH_META_DESCRIPTION_RESULTS, getI18nService().getCurrentLocale()) + " " + searchText + " "
                        + getMessageSource().getMessage(SEARCH_META_DESCRIPTION_ON, null, SEARCH_META_DESCRIPTION_ON,
                        getI18nService().getCurrentLocale())
                        + " " + getSiteName());
        final String metaKeywords = MetaSanitizerUtil.sanitizeKeywords(searchText);
        setUpMetaData(model, metaKeywords, metaDescription);

        return getViewForPage(model);
    }

    /**
     * check if we need to redirect to the PDP instead of rendering the result page.
     * This will return true if we need to redirect to the PDP, false otherwise
     *
     * @param searchPageData search page data object
     * @return  true when there is only 1 result (BaseProduct) and only 1 variantProduct.
     *          true when there is a match with a product code
     *          true when there is a match with only one product vendorItemNumber
     *          false when there is a match with a product code and a product vendorItemNumber
     *          false when there is a match with more products with the same vendorItemNumber
     */
    private boolean shouldRedirectToPDP(final ProductSearchPageData<SearchStateData, ProductData> searchPageData) {

        if (searchPageData != null && CollectionUtils.isNotEmpty(searchPageData.getResults())) {
            List<ProductData> results = searchPageData.getResults();

            boolean skuMatched = false;
            int vendorItemNumberMatches = 0;

            if (results.size() == 1 && results.get(0).getNumberOfVariants() == 1) {
                return true;
            }

            for (ProductData productData : results) {
                if (StringUtils.equals(productData.getCode(), searchPageData.getFreeTextSearch()))
                {
                    skuMatched = true;
                    continue;
                }

                if (StringUtils.equals(productData.getVendorItemNumber(), searchPageData.getFreeTextSearch()))
                {
                    vendorItemNumberMatches ++;
                }
            }

            return (skuMatched && vendorItemNumberMatches == 0) || (!skuMatched && vendorItemNumberMatches == 1);
        }

        return false;
    }

    @RequestMapping(method = RequestMethod.GET, params = "q")
    public String refineSearch(@RequestParam("q") final String searchQuery,
                               @RequestParam(value = "page", defaultValue = "0") final int page,
                               @RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode,
                               @RequestParam(value = "sort", required = false) final String sortCode,
                               @RequestParam(value = "text", required = false) final String searchText,
                               @RequestParam(value = "viewType", required = false) final ProductListViewType viewType,
                               final HttpServletRequest request, final HttpServletResponse response,
                               final Model model) throws CMSItemNotFoundException {
        // determine correct list view depending on the cookie stored value
        ProductListViewType desiredViewType = getAndStorePLPViewType(viewType, request, response);
        model.addAttribute("viewType", desiredViewType);

        final ProductSearchPageData<SearchStateData, ProductData> searchPageData = performSearch(searchQuery, page, showMode,
                sortCode, getSearchPageSize());

        populateModel(model, searchPageData, showMode);
        model.addAttribute("userLocation", this.customerLocationService.getUserLocation());

        if (searchPageData.getPagination().getTotalNumberOfResults() == 0) {
            updatePageTitle(searchPageData.getFreeTextSearch(), model);
            storeCmsPageInModel(model, getContentPageForLabelOrId(NO_RESULTS_CMS_PAGE_ID));
        } else {
            storeContinueUrl(request);
            updatePageTitle(searchPageData.getFreeTextSearch(), model);
            storeCmsPageInModel(model, getContentPageForLabelOrId(SEARCH_CMS_PAGE_ID));
            if (ProductListViewType.List == desiredViewType) {
                populateProductVariants(searchPageData, searchQuery, null);
            }
        }
        model.addAttribute(WebConstants.BREADCRUMBS_KEY, this.searchBreadcrumbBuilder.getBreadcrumbs(null, searchPageData));
        model.addAttribute("pageType", PageType.PRODUCTSEARCH.name());
        model.addAttribute("searchQuery", searchQuery);
        model.addAttribute("searchText", searchText);

        final String metaDescription = MetaSanitizerUtil
                .sanitizeDescription(getMessageSource().getMessage(SEARCH_META_DESCRIPTION_RESULTS, null,
                        SEARCH_META_DESCRIPTION_RESULTS, getI18nService().getCurrentLocale()) + " " + searchText + " "
                        + getMessageSource().getMessage(SEARCH_META_DESCRIPTION_ON, null, SEARCH_META_DESCRIPTION_ON,
                        getI18nService().getCurrentLocale())
                        + " " + getSiteName());

        final String metaKeywords = MetaSanitizerUtil.sanitizeKeywords(searchText);
        setUpMetaData(model, metaKeywords, metaDescription);

        return getViewForPage(model);
    }

    @GetMapping(value = "/products", produces = MediaType.APPLICATION_JSON_VALUE)
    protected String getProducts(@RequestParam(value = "q", required = false) final String searchQuery,
                                 @RequestParam(value = "page", defaultValue = "0") final int page,
                                 @RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode,
                                 @RequestParam(value = "sort", required = false) final String sortCode,
                                 final Model model) {

        final ProductSearchPageData<SearchStateData, ProductData> searchPageData = performSearch(searchQuery, page, showMode,
                sortCode, getSearchPageSize());

        populateProductVariants(searchPageData, searchQuery, null);

        model.addAttribute("searchPageData", searchPageData);
        return VanmarckeblueaddonControllerConstants.Views.Fragments.Product.PRODUCT_LIST;
    }


    protected ProductSearchPageData<SearchStateData, ProductData> performSearch(final String searchQuery, final int page,
                                                                                final ShowMode showMode, final String sortCode, final int pageSize) {
        final PageableData pageableData = createPageableData(page, pageSize, sortCode, showMode);

        final SearchStateData searchState = new SearchStateData();
        final SearchQueryData searchQueryData = new SearchQueryData();
        searchQueryData.setValue(searchQuery);
        searchState.setQuery(searchQueryData);

        return encodeSearchPageData(this.productSearchFacade.textSearch(searchState, pageableData));
    }

    @ResponseBody
    @RequestMapping(value = "/results", method = RequestMethod.GET)
    public SearchResultsData<ProductData> jsonSearchResults(@RequestParam("q") final String searchQuery,
                                                            @RequestParam(value = "page", defaultValue = "0") final int page,
                                                            @RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode,
                                                            @RequestParam(value = "sort", required = false) final String sortCode) throws CMSItemNotFoundException {
        final ProductSearchPageData<SearchStateData, ProductData> searchPageData = performSearch(searchQuery, page, showMode,
                sortCode, getSearchPageSize());
        final SearchResultsData<ProductData> searchResultsData = new SearchResultsData<>();
        searchResultsData.setResults(searchPageData.getResults());
        searchResultsData.setPagination(searchPageData.getPagination());
        return searchResultsData;
    }

    @ResponseBody
    @RequestMapping(value = "/facets", method = RequestMethod.GET)
    public FacetRefinement<SearchStateData> getFacets(@RequestParam("q") final String searchQuery,
                                                      @RequestParam(value = "page", defaultValue = "0") final int page,
                                                      @RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode,
                                                      @RequestParam(value = "sort", required = false) final String sortCode) throws CMSItemNotFoundException {
        final SearchStateData searchState = new SearchStateData();
        final SearchQueryData searchQueryData = new SearchQueryData();
        searchQueryData.setValue(searchQuery);
        searchState.setQuery(searchQueryData);

        final ProductSearchPageData<SearchStateData, ProductData> searchPageData = this.productSearchFacade.textSearch(searchState,
                createPageableData(page, getSearchPageSize(), sortCode, showMode));
        final List<FacetData<SearchStateData>> facets = refineFacets(searchPageData.getFacets(),
                convertBreadcrumbsToFacets(searchPageData.getBreadcrumbs()));
        final FacetRefinement<SearchStateData> refinement = new FacetRefinement<>();
        refinement.setFacets(facets);
        refinement.setCount(searchPageData.getPagination().getTotalNumberOfResults());
        refinement.setBreadcrumbs(searchPageData.getBreadcrumbs());
        return refinement;
    }

    @ResponseBody
    @RequestMapping(value = "/autocomplete/" + COMPONENT_UID_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
    public AutocompleteResultData getAutocompleteSuggestions(@PathVariable final String componentUid,
                                                             @RequestParam("term") final String term) throws CMSItemNotFoundException {
        final AutocompleteResultData resultData = new AutocompleteResultData();

        final SearchBoxComponentModel component = (SearchBoxComponentModel) this.cmsComponentService.getSimpleCMSComponent(componentUid);

        if (component.isDisplaySuggestions()) {
            resultData.setSuggestions(subList(this.productSearchFacade.getAutocompleteSuggestions(term), component.getMaxSuggestions()));
        }

        if (component.isDisplayProducts()) {
            resultData.setProducts(subList(this.productSearchFacade.textSearch(term, SearchQueryContext.SUGGESTIONS).getResults(),
                    component.getMaxProducts()));
        }

        if (component.isDisplayCategories()) {
            resultData.setCategories(subList(this.vmkSearchFacade.textSearch(term), component.getMaxSuggestions()));
        }

        return resultData;
    }

    protected <E> List<E> subList(final List<E> list, final int maxElements) {
        if (CollectionUtils.isEmpty(list)) {
            return Collections.emptyList();
        }

        if (list.size() > maxElements) {
            return list.subList(0, maxElements);
        }

        return list;
    }

    protected void updatePageTitle(final String searchText, final Model model) {
        storeContentPageTitleInModel(model, getPageTitleResolver().resolveContentPageTitle(
                getMessageSource().getMessage("search.meta.title", null, "search.meta.title", getI18nService().getCurrentLocale())
                        + " " + searchText));
    }
}
