<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>


<sec:authorize var="isAnonymousUser" access="hasAnyRole('ROLE_ANONYMOUS')"/>
<%--- Add to cart modal --%>
<c:url value="/cart" var="cartUrl"/>

<div class="vmb-modal fade" id="vmbAddToCartModal" tabindex="-1" role="dialog" aria-labelledby="vmbAddToCartModalTitle">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="vmbAddToCartModalTile"><spring:theme code="basket.added.to.basket"/></h4>
            </div>
            <div class="modal-body">
                <%--- Created by addtocart.js --%>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal"><spring:theme code="cart.page.continue"/></button>
                <a href="${cartUrl}" class="btn btn-default add-to-cart-button">
                    <spring:theme code="checkout.checkout"/>
                </a>
            </div>
        </div>
    </div>
</div>

<%--- Add to cart modal item --%>

<spring:theme code="img.missingProductImage.responsive.product" var="imagePath" htmlEscape="false"/>

<c:choose>
    <c:when test="${originalContextPath ne null}">
        <c:url value="${imagePath}" var="imageUrl" context="${originalContextPath}"/>
    </c:when>
    <c:otherwise>
        <c:url value="${imagePath}" var="imageUrl"/>
    </c:otherwise>
</c:choose>

<script id="vmbAddToCartItemTemplate" type="text/x-jsrender">
    <div class="vmb-addtocart-item row {{:statusCode}}">
        <div class="vmb-addtocart-item__image col-sm-12 col-md-3">
            <img src="{{:productImage}}" alt='cartItem' onerror="if (this.src != '${imageUrl}'){ this.src = '${imageUrl}'; this.parentNode.href='${imageUrl}';}" >
        </div>
        <div class="vmb-addtocart-item__details col-sm-12 col-md-9">
             {{if statusMessage}}
                <div class="alert alert-danger">{{:statusMessage}}</div>
             {{/if}}
            <a class="name" href="{{:entry.product.url}}">{{:entry.product.name}}</a>
            <div class="code">{{:entry.product.code}}</div>
            {{if statusCode === 'success'}}
                <c:if test="${cmsSite.channel != 'DIY'}">
                    <div class="quantity-price"><span>{{:quantityAdded}}</span> &times; {{:entry.basePrice.formattedValue}}</div>
                </c:if>
            {{else statusCode === 'lowStock'}}
                <p class="text-danger"><spring:theme code="basket.information.quantity.reducedNumberOfItemsAdded.maxOrderQuantityExceeded"/></p>
            {{else statusCode === 'noStock'}}
                <p class="text-danger"><spring:theme code="basket.information.quantity.reducedNumberOfItemsAdded.maxOrderQuantityExceeded"/></p>
            {{/if}}
        </div>
    </div>

</script>

<script id="vmbAddToCartErrorTemplate" type="text/x-jsrender">
    <div class="alert alert-danger" role="alert">
        <p><span class='glyphicon glyphicon-exclamation-sign'></span> <spring:theme code="text.add.to.cart.error" htmlEscape="false"/> </p>
    </div>

</script>

<%--- VMB Loader --%>
<div class="vmb-loader">
    <div class="vmb-loader__content">
        <div class="vmb-loader__logo"></div>
    </div>
</div>

<script id="vmbDynamicModalTemplate" type="text/x-jsrender">
    <div class="vmb-modal fade {{:className}}" id="{{:modalId}}" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    {{if canClose}}
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{/if}}
                    <h4 class="modal-title">{{:modalTitle}}</h4>
                </div>
                <div class="modal-body">
                    {{:modalBody}}
                </div>
                {{if modalFooter}}
                <div class="modal-footer">
                    {{:modalFooter}}
                </div>
                {{/if}}
            </div>
        </div>
    </div>
</script>