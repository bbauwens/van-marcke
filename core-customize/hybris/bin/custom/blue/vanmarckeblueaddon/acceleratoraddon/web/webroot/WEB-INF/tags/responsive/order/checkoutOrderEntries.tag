<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="order" required="true" type="de.hybris.platform.commercefacades.order.data.OrderData" %>
<%@ attribute name="ibmOrder" required="false" type="de.hybris.platform.commercefacades.order.data.IBMOrderData" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="order" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/order" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<spring:htmlEscape defaultHtmlEscape="true"/>

<c:choose>
    <c:when test="${ibmOrder ne null}">
        <c:set var="entries" value="${ibmOrder.orderEntries}"/>
    </c:when>
    <c:otherwise>
        <c:set var="entries" value="${order.entries}"/>
    </c:otherwise>
</c:choose>

<table class="table table-condensed vmb-product-table">
    <thead>
    <tr>
        <th class="vmb-productitem__toggle"></th>
        <th class="vmb-productitem__image"></th>
        <th class="vmb-productitem__info"><spring:theme code="basket.page.item"/></th>
        <c:if test="${ibmOrder eq null}">
            <th class="vmb-productitem__date"><spring:theme code="order.product.selectedDate"/></th>
        </c:if>
        <th class="vmb-productitem__quantity"><spring:theme code="basket.page.qty"/></th>
        <th class="vmb-productitem__nettoprice"><spring:theme code="basket.page.price.netto" arguments="${currentCurrency.symbol}"/></th>
        <th class="vmb-productitem__total--column"><spring:theme code="basket.page.total"/></th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${entries}" var="entry" varStatus="loop">
        <order:checkoutEntryDetails orderEntry="${entry}" order="${order}" itemIndex="${loop.index}" showStock="false"/>
    </c:forEach>
    </tbody>
</table>
