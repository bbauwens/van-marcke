package com.vanmarcke.blue.addon.controllers.delivery;

import com.vanmarcke.facades.data.DeliveryInfoData;
import com.vanmarcke.facades.order.VMKCheckoutFacade;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Provides endpoints for handling delivery and pickup date requests.
 *
 * @author Tom van den Berg
 * @since 29-6-2020
 */
@Controller
@RequestMapping("/delivery")
public class BlueDeliveryController {

    private final VMKCheckoutFacade checkoutFacade;

    /**
     * Provides an instance of the {@link BlueDeliveryController}.
     *
     * @param checkoutFacade the checkout facade
     */
    public BlueDeliveryController(VMKCheckoutFacade checkoutFacade) {
        this.checkoutFacade = checkoutFacade;
    }

    @GetMapping(value = {"/{deliveryMethod}"})
    @ResponseBody
    public DeliveryInfoData getDeliveryInfo(@PathVariable(name = "deliveryMethod") final String deliveryMethod,
                                            @RequestParam(name = "pos", required = false) final String pointOfServiceName,
                                            @RequestParam(name = "date", required = false) final String selectedDate) {
        checkoutFacade.setFavoriteAlternativeStoreOnSessionCart(pointOfServiceName);
        return checkoutFacade.getDeliveryInformation(deliveryMethod, selectedDate);
    }
}
