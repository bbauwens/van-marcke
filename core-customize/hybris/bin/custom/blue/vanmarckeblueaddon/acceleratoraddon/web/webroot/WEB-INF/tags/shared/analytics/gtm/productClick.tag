<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json" %>

<script type="text/javascript">
    function trackProductClick(actionField, product, callback) {

        dataLayer.push({
            'event': 'productClick',
            'ecommerce': {
                'click': {
                    'actionField': {'list': actionField},
                    'products': [product]
                }
            },
            'eventCallback': callback
        });

    }

    window.mediator.subscribe('trackProductClick', function(data) {

        if(data.hasOwnProperty('actionField')  && data.hasOwnProperty('product')  && data.hasOwnProperty('callback')) {
            trackProductClick(data.actionField, data.product, data.callback);
        }
    });
</script>