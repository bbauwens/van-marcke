<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/product" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common" %>

<template:page pageTitle="${pageTitle}">
    <cms:pageSlot position="Section1" var="comp" element="div" class="productDetailsPageSection1">
        <cms:component component="${comp}" element="div" class="productDetailsPageSection1-component"/>
    </cms:pageSlot>
    <product:productDetailsPanel/>

    <div class="row vmb-product-variant-info">
        <div class="col-sm-12 col-md-6">
            <product:productPageTabs/>
        </div>
        <div class="col-sm-12 col-md-6">
            <cms:pageSlot position="CrossSelling" var="comp" element="div"
                          class="productDetailsPageSectionCrossSelling">
                <cms:component component="${comp}" element="" class="productDetailsPageSectionCrossSelling-component"/>
            </cms:pageSlot>
            <cms:pageSlot position="FollowUp" var="comp" element="div" class="productDetailsPageSectionFollowUp">
                <cms:component component="${comp}" element="" class="productDetailsPageSectionFollowUp-component"/>
            </cms:pageSlot>
            <cms:pageSlot position="UpSelling" var="comp" element="div" class="productDetailsPageSectionUpSelling">
                <cms:component component="${comp}" element="" class="productDetailsPageSectionUpSelling-component"/>
            </cms:pageSlot>
            <cms:pageSlot position="Section4" var="comp" element="div" class="productDetailsPageSection4">
                <cms:component component="${comp}" element="" class="productDetailsPageSection4-component"/>
            </cms:pageSlot>
        </div>
    </div>

    <common:globalMessagesTemplates/>
</template:page>