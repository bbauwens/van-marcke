<%@ tag import="com.vanmarcke.services.util.URIUtils" %>
<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="hideHeaderLinks" required="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/nav" %>
<%@ taglib prefix="header" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/common/header" %>
<%@ taglib prefix="footer" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/common/footer" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/formElement" %>

<spring:htmlEscape defaultHtmlEscape="true"/>
<c:url value="/quickOrder" var="quickOrderUrl"/>

<cms:pageSlot position="SiteTopSlot" var="component" element="div">
    <cms:component component="${component}"/>
</cms:pageSlot>

<c:set var="currentUrl" value='<%=URIUtils.normalize(request.getAttribute("javax.servlet.forward.request_uri").toString())%>'/>

<header class="vmb-print-header">
    <img class="logo" src="/_ui/responsive/theme-vanmarckeblue/images/VM_BLUE.svg" alt="logo">
</header>
<header class="js-mainHeader vmb-main-header">
    <div class="vmb-site-logo col-xs-12 col-md-1">
        <a class="logo" href="<c:url value="/"/>"></a>
    </div>
    <div class="vmb-site-nav col-xs-12 col-md-11">
        <nav class="navigation navigation--top hidden-xs hidden-sm">
            <div class="row">
                <div class="col-sm-12">
                    <div class="nav__left">
                        <cms:pageSlot position="TopHeaderSlot" var="component" element="ul" class="vmb-topheader-menu">
                            <cms:component component="${component}"/>
                        </cms:pageSlot>
                    </div>
                    <div class="nav__right">
                        <ul class="nav__links nav__links--account">
                            <c:if test="${empty hideHeaderLinks}">
                                <c:if test="${uiExperienceOverride}">
                                    <li class="backToMobileLink">
                                        <c:url value="/_s/ui-experience?level=" var="backToMobileStoreUrl"/>
                                        <a href="${fn:escapeXml(backToMobileStoreUrl)}">
                                            <spring:theme code="text.backToMobileStore"/>
                                        </a>
                                    </li>
                                </c:if>
                                <sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
                                    <c:set var="maxNumberChars" value="25"/>
                                    <c:if test="${fn:length(user.firstName) gt maxNumberChars}">
                                        <c:set target="${user}" property="firstName"
                                               value="${fn:substring(user.firstName, 0, maxNumberChars)}..."/>
                                    </c:if>
                                    <li class="logged_in js-logged_in js-myAccount-toggle vmb-my-account"
                                        data-toggle="collapse" data-parent=".nav__right">
                                        <ycommerce:testId code="header_LoggedUser">
                                            <spring:theme code="header.welcome"
                                                          arguments="${user.firstName},${user.lastName}"/>
                                        </ycommerce:testId>
                                        <span class="glyphicon glyphicon-chevron-down"></span>
                                        <cms:pageSlot position="HeaderLinks" var="link">
                                            <cms:component component="${link}" element=""/>
                                        </cms:pageSlot>
                                    </li>
                                </sec:authorize>
                                <sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')">
                                    <li class="liOffcanvas">
                                        <ycommerce:testId code="header_Login_link">
                                            <c:choose>
                                                <c:when test="${disableSmartEdit}">
                                                    <c:set value="/elisionsamlsinglesignon/saml${currentUrl}" var="loginUrl"/>
                                                </c:when>
                                                <c:otherwise>
                                                    <c:set value="/login" var="loginUrl"/>
                                                </c:otherwise>
                                            </c:choose>
                                            <a href="${fn:escapeXml(loginUrl)}">
                                                <spring:theme code="header.link.login"/>
                                            </a>
                                        </ycommerce:testId>
                                    </li>
                                </sec:authorize>
                                <sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
                                    <li class="liOffcanvas">
                                        <ycommerce:testId code="header_signOut">
                                            <c:choose>
                                                <c:when test="${disableSmartEdit}">
                                                    <c:set value="/elisionsamlsinglesignon/saml/logout" var="logoutUrl"/>
                                                </c:when>
                                                <c:otherwise>
                                                    <c:set value="/logout" var="logoutUrl"/>
                                                </c:otherwise>
                                            </c:choose>
                                            <a href="${fn:escapeXml(logoutUrl)}">
                                                <spring:theme code="header.link.logout"/>
                                            </a>
                                        </ycommerce:testId>
                                    </li>
                                </sec:authorize>
                            </c:if>
                        </ul>
                        <footer:siteSelector allSites="${allSites}" currentSite="${currentSite}"/>
                        <header:languageSelector languages="${languages}" currentLanguage="${currentLanguage}"/>
                    </div>
                </div>
            </div>
        </nav>
        <%-- a hook for the my account links in desktop/wide desktop--%>
        <div class="hidden-xs hidden-sm js-secondaryNavAccount collapse" id="accNavComponentDesktopOne">
            <ul class="nav__links">
            </ul>
        </div>
        <div class="hidden-xs hidden-sm js-secondaryNavCompany collapse" id="accNavComponentDesktopTwo">
            <ul class="nav__links js-nav__links">
            </ul>
        </div>
        <nav class="navigation navigation--middle js-navigation--middle">
            <div class="container-fluid">
                <div class="row">
                    <div class="mobile__nav__row mobile__nav__row--table">
                        <div class="mobile__nav__row--table-group">
                            <div class="mobile__nav__row--table-row">
                                <div class="mobile__nav__row--table-cell visible-xs hidden-sm">
                                    <button class="mobile__nav__row--btn btn mobile__nav__row--btn-menu vmb-toggle-sm-navigation"
                                            type="button">
                                        <span class="glyphicon glyphicon-align-justify"></span>
                                    </button>
                                </div>
                                <div class="mobile__nav__row--table-cell visible-xs mobile__nav__row--seperator">
                                    <ycommerce:testId code="header_search_activation_button">
                                        <button class="mobile__nav__row--btn btn mobile__nav__row--btn-search js-toggle-xs-search hidden-sm hidden-md hidden-lg"
                                                type="button">
                                            <span class="glyphicon glyphicon-search"></span>
                                        </button>
                                    </ycommerce:testId>
                                </div>
                                <%--
                                <div class="mobile__nav__row--table-cell visible-xs mobile__nav__row--seperator">
                                    <a class="mobile__nav__row--btn btn mobile__nav__row--btn-quickorder hidden-sm hidden-md hidden-lg" href="${quickOrderUrl}">
                                        <span class="glyphicon glyphicon-list-alt"></span>
                                    </a>
                                </div>
                                --%>
                                <cms:pageSlot position="MiniCart" var="cart" element="div"
                                              class="miniCartSlot componentContainer mobile__nav__row--table hidden-sm hidden-md hidden-lg">
                                    <cms:component component="${cart}" element="div"
                                                   class="mobile__nav__row--table-cell"/>
                                </cms:pageSlot>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row desktop__nav">
                    <div class="nav__left col-xs-12 col-sm-9 col-md-6">
                        <div class="row">
                            <div class="col-sm-2 hidden-xs visible-sm mobile-menu">
                                <button class="btn vmb-toggle-sm-navigation" type="button">
                                    <span class="glyphicon glyphicon-align-justify"></span>
                                </button>
                            </div>
                            <div class="col-sm-10 col-md-12 vmb-main-header__quicknav">
                                <nav:topNavigation/>
                                <div class="site-search">
                                    <cms:pageSlot position="SearchBox" var="component">
                                        <cms:component component="${component}" element="div"/>
                                    </cms:pageSlot>
                                </div>
                                <%--
                                <div class="quick-order hidden-xs">
                                    <a class="btn btn-primary" href="${quickOrderUrl}"><spring:theme
                                            code="text.quickOrder.header"/></a>
                                </div>
                                --%>
                            </div>
                        </div>
                    </div>
                    <div class="nav__right col-xs-6 col-sm-3 col-md-6 hidden-xs">
                        <ul class="nav__links nav__links--shop_info">
                            <c:if test="${cmsSite.channel != 'DIY'}">
                                <sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
                                    <li class="vmb-netto-price-toggle__desktop">
                                        <formElement:formSwitch idKey="show_netto_price_toggle"
                                                                labelKey="header.checkbox.showNetPrice"
                                                                labelArg="${currentCurrency.symbol}"
                                                                itemValue="showPrice" checked="${user.showNetPrice}"
                                                                containerCss="vmb-netto-price-toggle"/>
                                    </li>
                                </sec:authorize>
                            </c:if>
                            <li class="vmb-favorite-store__desktop">
                                <header:favoriteStoreSelector/>
                            </li>
                            <li>
                                <cms:pageSlot position="MiniCart" var="cart" element="div" class="componentContainer">
                                    <cms:component component="${cart}" element="div"/>
                                </cms:pageSlot>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
        <a id="skiptonavigation"></a>
    </div>
</header>
<cms:pageSlot position="BreadcrumbSlot" var="component" element="div" class="breadcrumb-container">
    <cms:component component="${component}"/>
</cms:pageSlot>
<cms:pageSlot position="BottomHeaderSlot" var="component" element="div" class="container-fluid">
    <cms:component component="${component}"/>
</cms:pageSlot>
