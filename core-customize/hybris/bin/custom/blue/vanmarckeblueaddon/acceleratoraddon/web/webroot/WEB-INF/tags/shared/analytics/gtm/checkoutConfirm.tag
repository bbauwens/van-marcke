<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json" %>

dataLayer.push(
<json:object>
    <json:property name="event" value="purchase"/>
    <json:object name="ecommerce">
        <json:object name="purchase">
            <json:object name="actionField">
                <json:property name="id" value="${orderData.code}"/>
                <json:property name='revenue' value="${orderData.totalPriceWithTax.value}"/>
                <json:property name="tax" value="${orderData.totalTax.value}"/>
                <json:property name="shipping" value="${orderData.deliveryCost.value}"/>
                <c:if test="${not empty orderData.appliedVouchers}">
                    <json:array name="coupon" items="${orderData.appliedVouchers}"/>
                </c:if>
            </json:object>
            <json:array name="products">
                <c:forEach items="${orderData.unconsignedEntries}" var="orderEntry" varStatus="loopEntry">
                    <json:object>
                        <json:property name="name" value="${orderEntry.product.analyticsData.name}"/>
                        <json:property name='id'>${orderEntry.product.code}</json:property>
                        <json:property name="quantity" value="${orderEntry.quantity}"/>
                        <json:property name="price" value="${orderEntry.basePrice.value}"/>
                        <json:property name="category" value="${orderEntry.product.analyticsData.category}"/>
                        <json:property name="brand" value="${orderEntry.product.analyticsData.brand}"/>
                    </json:object>
                </c:forEach>
            </json:array>
        </json:object>
    </json:object>
</json:object>
);