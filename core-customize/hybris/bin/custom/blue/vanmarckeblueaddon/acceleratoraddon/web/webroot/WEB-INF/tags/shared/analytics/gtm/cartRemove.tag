<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json" %>

<script type="text/javascript">
    function trackRemoveFromCart(product, initialQuantity, cartData) {

        dataLayer.push({
            'event'    : 'removeFromCart',
            'ecommerce': {
                'currencyCode': '${currentCurrency.isocode}',
                'remove'      : {
                    'products': [
                        {
                            'name'    : product.name,
                            'id'      : product.id,
                            'price'   : product.price,
                            'category': product.category,
                            'brand'   : product.brand,
                            'quantity': initialQuantity
                        }
                    ]
                }
            }
        });
    }

    window.mediator.subscribe('trackRemoveFromCart', function(data) {
        if(data.product && data.initialCartQuantity) {
            trackRemoveFromCart(data.product, data.initialCartQuantity);
        }
    });
</script>