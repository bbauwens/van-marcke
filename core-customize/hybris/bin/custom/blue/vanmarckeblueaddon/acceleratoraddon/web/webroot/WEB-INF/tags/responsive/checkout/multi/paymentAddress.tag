<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/address" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<spring:htmlEscape defaultHtmlEscape="true"/>

<%@ attribute name="cartData" required="true" type="de.hybris.platform.commercefacades.order.data.CartData" %>

<c:set var="billingAddress" value="${cartData.paymentInfo.billingAddress}"/>
<c:set var="name" value="${cartData.b2bCustomerData.unit.name}"/>

<div class="address">
    <address:formattedAddress address="${billingAddress}" name="${name}"/>
</div>