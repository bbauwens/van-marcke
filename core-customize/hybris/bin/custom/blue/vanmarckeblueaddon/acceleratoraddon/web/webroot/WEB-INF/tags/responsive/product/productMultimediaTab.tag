<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/product" %>

<div class="tab-details">
    <table class="table vmb-documents-table">
        <tbody>
        <product:productMultimediaSingle doc="${product.technicalDataSheet}"
                                         title="product.download.title.technicalDataSheet"/>
        <product:productMultimediaMultiple list="${product.manuals}" title="product.download.title.manuals"/>
        <product:productMultimediaMultiple list="${product.certificates}" title="product.download.title.certificates"/>
        <product:productMultimediaSingle doc="${product.DOP}" title="product.download.title.DOP"/>
        <product:productMultimediaSingle doc="${product.normalisation}" title="product.download.title.normalisation"/>
        <product:productMultimediaSingle doc="${product.safetyDataSheet}"
                                         title="product.download.title.safetyDataSheet"/>
        <product:productMultimediaSingle doc="${product.productSpecificationSheet}"
                                         title="product.download.title.productSpecificationSheet"/>
        <product:productMultimediaSingle doc="${product.sparePartsList}" title="product.download.title.sparePartsList"/>
        <product:productMultimediaSingle doc="${product.warranty}" title="product.download.title.warranty"/>
        <product:productMultimediaSingle doc="${product.ecoDataSheet}" title="product.download.title.ecoDataSheet"/>
        </tbody>
    </table>
</div>