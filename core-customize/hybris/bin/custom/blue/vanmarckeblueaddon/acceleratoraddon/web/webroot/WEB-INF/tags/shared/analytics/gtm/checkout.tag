<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<script type="text/javascript">
    function trackCheckoutStep(step, checkoutOption) {

        dataLayer.push({
            'event': 'checkoutOption',
            'ecommerce': {
                'checkout_option': {
                    'actionField': {'step': step, 'option': checkoutOption}
                }
            }
        });

    }

    window.mediator.subscribe('trackCheckoutStep', function(data) {
        if(data.hasOwnProperty('step')  && data.hasOwnProperty('checkoutOption')) {
            trackCheckoutStep(data.step, data.checkoutOption)
        }
    });
</script>