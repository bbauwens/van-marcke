var PDP ={
    numPages: 1,
    showAmount: 10,
    numProducts: 0,

    // Init setup to make pagination possible - to be called if pagination is desired
    init : function () {
        this.numProducts = $(".vmb-product-variant-table tbody tr").length;
        this.numPages = Math.ceil(this.numProducts / this.showAmount);

        var activePage = 1;

        //Find Active product

        var productVariantCodes = $('.vmb-product-variant-table tbody tr').map(function() {
            return "" + $(this).data('code');
        }).get();

        var indexSelected = _.indexOf(productVariantCodes, vmbActiveProduct);

        if(indexSelected > 0) {
            // calc on what page the selected product is
            activePage = Math.ceil((indexSelected + 1) / this.showAmount);
        }

        this.paginate(activePage);

        $(".vmb-product-variant-table").removeClass("init");
    },

    //Function to paginate trough table
    paginate : function(page){

        var startIndex = (page - 1) * this.showAmount;
        var endIndex = (page * this.showAmount) - 1;

        //Hide and show the correct table rows
        $.each($(".vmb-product-variant-table__row"), function (index, element) {
            if (index >= startIndex && index <= endIndex) {
                $(element).removeClass('hidden');
                $("#stock-warehouse-" + index + " .vmb-stock-status").removeClass('hidden');
                $("#stock-tec-" + index + " .vmb-stock-status").removeClass('hidden');
            } else {
                $(element).addClass('hidden');
                $("#stock-warehouse-" + index + " .vmb-stock-status").addClass('hidden');
                $("#stock-tec-" + index + " .vmb-stock-status").addClass('hidden');

            }
        });

        this.renderPagination(page);

        window.mediator.publish('updateStockIndication', '');
    },
    // Render paginationbar
    renderPagination: function (current) {
        if(this.numPages > 1) {
            $("#vmbProductVariantPag").closest(".pagination-toolbar").removeClass("empty");
            $("#vmbProductVariantPag").html($.templates("#productVariantTablePagTemplate").render({
                current : current,
                prev    : current - 1,
                next    : current + 1,
                numPages: this.numPages
            }));
        } else {
            $("#vmbProductVariantPag").closest(".pagination-toolbar").addClass("empty");
            $("#vmbProductVariantPag").empty().closest();
        }
    }
};


(function($) {
    'use strict';

    //Init image gallary of product
    $('.sp-wrap').smoothproducts();

    //Click function on the table so that user can browse trough variants
    $(".vmb-product-variant-table__row td:not(:last-child,:nth-last-child(6))").click(function(event) {
        if($(event.target).data("toggle") === undefined){

            var tr = $(this).parent();
            var url = tr.data("productUrl");

            ACC.track.productClick("Varianttable", {
                "id": tr.data("code").toString(),
                "name":  tr.data("name").toString(),
                "price" :  tr.data("price"),
                "position": tr.data("position"),
                "category": $(".vmb-product-variant-table").data("category"),
                "brand": $(".vmb-product-variant-table").data("brand"),

            },  window.location.origin + url);

            window.location.href = url;
        }
    });

})(jQuery);