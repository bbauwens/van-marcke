<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<c:set value="" var="activeStorePhoneNr"/>
<c:set value="" var="activeStorePhoneNrValue"/>
<c:if test="${not empty favoriteStore and not empty favoriteStore.address and not empty favoriteStore.address.phone}">
    <c:set value="${favoriteStore.address.phone}" var="activeStorePhoneNr"/>
    <c:set value="${fn:replace(favoriteStore.address.phone, '.', '')}" var="activeStorePhoneNrValue"/>
    <c:set value="${fn:replace(activeStorePhoneNrValue, ' ', '')}" var="activeStorePhoneNrValue"/>
    <c:set value="${fn:replace(activeStorePhoneNrValue, '/', '')}" var="activeStorePhoneNrValue"/>
</c:if>
<div class="vmb-favorite-store<c:if test="${cmsSite.channel == 'DIY'}"> hidden</c:if>">
    <div class="vmb-favorite-store__phone">
        <c:if test="${not empty activeStorePhoneNrValue}">
            <a href="tel:${activeStorePhoneNrValue}"><span class="glyphicon glyphicon-earphone"></span> </a>
        </c:if>
    </div>
    <spring:url value="/_s/store" var="setStoreActionUrl"/>
    <div class="vmb-favorite-store__switcher">
        <label class="control-label vmb-switch__label" for="vmbFavoriteStoreForm">
            <spring:theme code="text.pointOfService.label"/>
        </label>
        <select name="storeSelect" class="form-control favorite-store-selector" ${(empty disableFavoriteStore || !disableFavoriteStore) ? "" : "disabled"}>
            <c:if test="${not empty favoriteStore}">
                <option value="${favoriteStore.name}">
                        ${favoriteStore.displayName} <c:if test="${not empty favoriteStore.address and not empty favoriteStore.address.phone}"> | ${favoriteStore.address.phone}</c:if>
                </option>
            </c:if>
        </select>
        <form:form action="${setStoreActionUrl}" method="post" id="vmbFavoriteStoreForm">
            <input id="vmbFavoriteStoreName" type="hidden" name="storeName" value="${not empty favoriteStore ? favoriteStore.name : ''}">
        </form:form>
    </div>
</div>