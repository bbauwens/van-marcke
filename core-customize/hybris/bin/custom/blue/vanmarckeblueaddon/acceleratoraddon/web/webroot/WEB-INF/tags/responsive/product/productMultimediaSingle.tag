<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ attribute name="doc" required="true" type="de.hybris.platform.commercefacades.product.data.ImageData" %>
<%@ attribute name="title" required="true" type="java.lang.String" %>


<c:if test="${not empty doc}">
    <tr>
        <td>
            <spring:theme code="${title}"/>
        </td>
        <td>
            <a href="${doc.url}" download="pdf_image"
               target="_blank">${doc.name}</a>
        </td>
    </tr>
</c:if>