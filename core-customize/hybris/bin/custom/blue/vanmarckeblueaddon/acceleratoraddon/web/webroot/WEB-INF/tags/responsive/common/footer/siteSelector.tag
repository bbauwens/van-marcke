<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="allSites" required="true" type="java.util.Collection" %>
<%@ attribute name="currentSite" required="true" type="com.vanmarcke.facades.site.data.BaseSiteData" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<spring:htmlEscape defaultHtmlEscape="true"/>
<c:if test="${fn:length(allSites) > 1 and cmsSite.channel != 'DIY'}">
    <spring:url value="/_s/site" var="setSiteActionUrl"/>
    <form:form action="${setSiteActionUrl}" method="post" id="" cssClass="vmb-form-site-selector">
        <spring:theme code="text.site" var="siteText"/>
        <label class="control-label sr-only" for="currency-selector">${siteText}</label>
        <ycommerce:testId code="header_site_select">
            <select name="code" class="vmb-site-selector hidden">
                <c:forEach items="${allSites}" var="site">
                    <option value="${fn:escapeXml(site.uid)}"
                        ${site.uid == currentSite.uid ? 'selected="selected"' : ''}>
                        <c:out value="${site.name}"/>
                    </option>
                </c:forEach>
            </select>
        </ycommerce:testId>
    </form:form>
</c:if>