<%@ page trimDirectiveWhitespaces="true"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/product"%>
<%@ taglib prefix="component" tagdir="/WEB-INF/tags/shared/component"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="gtm" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/shared/analytics/gtm" %>

<spring:htmlEscape defaultHtmlEscape="true" />

<c:choose>
	<c:when test="${not empty productReferences and component.maximumNumberProducts > 0}">
		<div class="vmb-reference-component">

			<div class="vmb-reference-component__title">

				<c:if test="${not empty component.title}">
					<h2 class="headline">${fn:escapeXml(component.title)}</h2>
				</c:if>
				<div class="nav">
					<div class="swiper-button-prev hidden"></div>
					<div class="swiper-button-next hidden"></div>
				</div>
			</div>

			<div class="swiper-container">
				<div class="swiper-wrapper">
					<c:forEach end="${component.maximumNumberProducts}" items="${productReferences}" var="productReference" varStatus="loopIndex">
						<c:url value="${productReference.target.url}" var="productUrl"/>

						<div class="vmb-product-item swiper-slide vmb-gtm-product-data"
							 data-code="${fn:escapeXml(productReference.target.code)}"
							 data-name="${fn:escapeXml(productReference.target.name)}"
							 data-category="${fn:escapeXml(productReference.target.analyticsData.category)}"
							 data-brand="${fn:escapeXml(productReference.target.analyticsData.brand)}"
							 data-url="${productUrl}"
							 data-list="Productreferences ${fn:escapeXml(productReference.referenceType)}"
							 data-position="${loopIndex.index + 1}"
						>
							<a class="vmb-product-item__thumb vmb-gtm-product-item" href="${productUrl}" title="${fn:escapeXml(productReference.target.name)}">
								<product:productPrimaryImage product="${productReference.target}" format="product"/>
							</a>
							<div class="vmb-product-item__content">
								<div class="vmb-product-item__details">
									<a class="vmb-product-item__name" href="${productUrl}">
										<c:out escapeXml="false" value="${fn:escapeXml(productReference.target.name)}"/>
									</a>
								</div>
								<a class="btn btn-default vmb-product-item__variants vmb-gtm-product-item" href="${productUrl}"><spring:theme code="product.references.button"/></a>
							</div>
						</div>
					</c:forEach>
				</div>
				<div class="swiper-pagination"></div>

			</div>
		</div>

		<gtm:productReference/>
	</c:when>

	<c:otherwise>
		<component:emptyComponent />
	</c:otherwise>
</c:choose>