(function ($) {
    'use strict';


    //Open main menu nav
    $(".vmb-main-menu-btn button").on("click touchstart", function (event) {
        event.preventDefault();
        $(".vmb-main-menu-btn").toggleClass("open");

        if (window.innerWidth <= parseInt(screenSmMax)) {
            $(".vmb-main-menu-btn button .glyphicon").toggleClass("glyphicon-triangle-right glyphicon-triangle-bottom");
        }

        event.stopPropagation();
    });

    $(document).on("click", function (evt) {
        if (!$(".vmb-category-menu").find($(evt.target)).length) {
            $(".vmb-category-menu").parent().removeClass("open");
        }
    });

    // Click function to open mobile navigation
    $(".vmb-toggle-sm-navigation").click(function (event) {
        $(".navigation--bottom").toggleClass("open");

        //when mobile navigation is open add a bg to make it more clear
        if ($(".navigation--bottom").hasClass("open")) {
            $("body").append("<div class='vmb-sm-navigation__bg'></div>");

            // Click function on the mobile bg so that it can be closed when clicked on
            $(".vmb-sm-navigation__bg").click(function () {
                $(".navigation--bottom").removeClass("open");
                $(this).remove();
            })
        } else {
            $(".vmb-sm-navigation__bg").remove();
        }

        event.stopPropagation();
    });

    //click function to open mobile my account nav
    $(".vmb-mobile-account button").on("click touchstart", function (event) {
        event.preventDefault();
        $(".vmb-mobile-account").toggleClass("open");
        $(".vmb-mobile-account button .glyphicon").toggleClass("glyphicon-triangle-right glyphicon-triangle-bottom");
        event.stopPropagation();
    });

    //Copy direct links to its mobile placeholder
    $(".vmb-topheader-menu").clone().appendTo(".vmb-mobile-directlinks");

    //Copy my account links to mobile placehoder
    var myaccountNavs = [];
    $(".NAVcompONENT a").each(function () {
        myaccountNavs.push("<li><a href='" + $(this).prop('href') + "' title='" + $(this).text() + "'>" + $(this).html() + "</a></li>");
    });

    $(".vmb-mobile-account ul").prepend(myaccountNavs);

    // Function that renders select2 dropdown items to the desired markup
    var favoriteStoreItemRender = function (item) {
        var i = item.text.indexOf('|');
        if (i === -1) {
            return item.text;
        } else {
            var splits = [item.text.slice(0, i), item.text.slice(i + 1)];
            return $('<strong>' + splits[0].trim() + '</strong><br><span>' + splits[1].trim() + "</span>");
        }
    };

    //Favorite Store
    $(".favorite-store-selector").select2({
        templateSelection: favoriteStoreItemRender,
        templateResult: favoriteStoreItemRender,
        minimumResultsForSearch: Infinity,
        dropdownCssClass: "favorite-store-selector-select2",
        width: 'element',
        ajax: {
            url: VMB.urls.favoriteStores,
            dataType: 'json',
            processResults: function (data) {
                // Tranforms the response object to expected format for Select2

                var Select2Response = {"results": []};

                $.each(data, function (index, dataItem) {

                    var item = {
                        id: dataItem.name,
                        text: dataItem.displayName
                    };

                    if (dataItem.hasOwnProperty('address') && dataItem.address.hasOwnProperty('phone') && !empty(dataItem.address.phone)) {
                        item.text += "|" + dataItem.address.phone;
                    }

                    Select2Response.results.push(item);
                });

                return Select2Response;
            }
        }
    });

    // Send selected store to the backend
    $(".favorite-store-selector").change(function () {

        var storeId = $(this).val();
        $(this).prop("disabled", true);

        $("#vmbFavoriteStoreName").val(storeId);

        $("#vmbFavoriteStoreForm").ajaxSubmit(function () {
            $(".favorite-store-selector").prop("disabled", false);
            vmbMessages.createInfo("test", VMB.i18n.store.currentPointOfServiceChange, true);
            window.mediator.publish('updateStockIndication', '');
        });

        var selectedPhoneNr = $(this).find("option[value='" + storeId + "']").html().split("|");

        if (selectedPhoneNr.length > 1) {
            selectedPhoneNr = selectedPhoneNr[1];
        } else {
            selectedPhoneNr = "";
        }

        if (!empty(selectedPhoneNr)) {

            if ($(".vmb-favorite-store__phone a").length > 0) {
                $(".vmb-favorite-store__phone a").attr("href", "tel:" + selectedPhoneNr.replace(/\s|\.|\//gi, ""));
            } else {
                $(".vmb-favorite-store__phone").html('<a href="tel:' + selectedPhoneNr.replace(/\s|\.|\//gi, "") + '"><span class="glyphicon glyphicon-earphone"></span> </a>');
            }
        }

    });

    // Netto price toggle

    if ($("#show_netto_price_toggle").length > 0) {
        $("#show_netto_price_toggle").change(function () {
            var switchToggle = $(this);
            switchToggle.prop("disabled", true);
            var netPriceChecked = switchToggle.is(":checked");

            var shouldReload = ACC.pageType === "CART" || ACC.pageType === "SAVEDCART" || ACC.pageType === "WISHLIST";
            
            if (shouldReload) {
                showLoader();
            }

            $.ajax({
                url: VMB.urls.netpriceUrl + "?showNetPrice=" + netPriceChecked,
                type: 'PUT',
                success: function (result) {
                    switchToggle.prop("disabled", false);
                    switchToggle.prop(":checked", result);

                    if (shouldReload) {
                        window.location.reload();
                    } else {
                        showNetPrice(result);
                    }
                },
                error: function (qXHR, textStatus, errorThrown) {
                    var switchToggleChecked = !switchToggle.is(":checked");
                    switchToggle.prop(":checked", switchToggleChecked);
                    switchToggle.prop("disabled", false);

                    if (shouldReload) {
                        window.location.reload();
                    } else {
                        showNetPrice(switchToggleChecked);
                    }
                }
            });
        });
    }

    // Site Selector

    var siteSelectorItemRender = function (item) {

        if (!item.id) {
            return item.text;
        }

        var flag = item.id.replace("blue-", "");

        return $("<span class='flag-icon flag-icon--round flag-icon-" + flag + "'></span><span>" + item.text + "</span>")
    };

    $(".vmb-site-selector").select2({
        templateSelection: siteSelectorItemRender,
        templateResult: siteSelectorItemRender,
        minimumResultsForSearch: Infinity,
        dropdownCssClass: "site-selector-select2",
        width: 'auto'
    });

    $(".vmb-site-selector").change(function () {
        $(this).closest("form").submit();
    });


    enquire.register("screen and (max-width:" + screenSmMax + ")", {

        match: function () {
            //Collapse function for level 1 cat navigation
            $(".childlevel1 > .glyphicon").click(function (event) {
                event.preventDefault();
                $(this).parent().toggleClass("show-sub");
                $(this).toggleClass("glyphicon-triangle-right glyphicon-triangle-bottom");
            });

            //Collapse function for level 2 cat navigation
            $(".childlevel2__link > .glyphicon").click(function (event) {
                event.preventDefault();
                $(this).parent().parent().toggleClass("open");
                $(this).toggleClass("glyphicon-triangle-right glyphicon-triangle-bottom");
            });

            $(".vmb-main-menu-btn").removeClass("open");
            $(".vmb-main-menu-btn button .glyphicon").removeClass("glyphicon-triangle-bottom").addClass("glyphicon-triangle-right ");
            $(".vmb-mobile-account button .glyphicon").removeClass("glyphicon-triangle-bottom").addClass("glyphicon-triangle-right ");

            $(".vmb-netto-price-toggle").detach().appendTo($(".vmb-netto-price-toggle__mobile"));
            $(".vmb-favorite-store").detach().appendTo($(".vmb-favorite-store__mobile"));

        },

        unmatch: function () {
            //Remove the mobile navigation functions when screen is not mobile
            $(".vmb-sm-navigation__bg").remove();
            $(".childlevel1 > .glyphicon").off("click");
            $(".childlevel2__link > .glyphicon").off("click");
            $(".childlevel1 > .glyphicon.glyphicon-triangle-bottom").removeClass("glyphicon-triangle-bottom").addClass("glyphicon-triangle-right ");
            $(".childlevel2").removeClass("open");
            $(".childlevel2__link > .glyphicon.glyphicon-triangle-bottom").removeClass("glyphicon-triangle-bottom").addClass("glyphicon-triangle-right ");
            $(".vmb-main-menu-btn button .glyphicon").removeClass("glyphicon-triangle-right").addClass("glyphicon-triangle-bottom ");
            $(".vmb-mobile-account button .glyphicon").removeClass("glyphicon-triangle-right").addClass("glyphicon-triangle-bottom ");

            $(".vmb-netto-price-toggle").detach().appendTo($(".vmb-netto-price-toggle__desktop"));
            $(".vmb-favorite-store").detach().appendTo($(".vmb-favorite-store__desktop"));
        }

    });

})(jQuery);

function showNetPrice(show) {

    if (show) {
        $(".vmb-netto-price-display").removeClass("hidden");
    } else {
        $(".vmb-netto-price-display").addClass("hidden");
    }

    $(".vmb-product-variant-table__wrapper").toggleClass("net-price-shown");
}