<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="order" tagdir="/WEB-INF/tags/responsive/order" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>

<spring:htmlEscape defaultHtmlEscape="true"/>
<spring:url value="/login/register/termsandconditions" var="getTermsAndConditionsUrl" htmlEscape="false"/>

<div class="checkout-success">
    <div class="checkout-success__body">
        <div class="checkout-success__body__headline">
            <spring:theme code="checkout.orderConfirmation.thankYouForOrder"/>
        </div>
        <c:choose>
            <c:when test="${orderData.externalCode ne null}">
                <c:choose>
                    <c:when test="${orderData.isSplitted}">
                        <c:set var="orderLabel">
                            <spring:theme code="text.account.order.split.orderNumbers"
                                          arguments="${fn:escapeXml(orderData.ibmSubOrders.get(0).code)},${fn:escapeXml(orderData.ibmSubOrders.get(1).code)}"/>
                        </c:set>
                        <p>${fn:replace(fn:replace(orderLabel, '&lt;','<'), '&gt;','>')}</p>
                    </c:when>
                    <c:otherwise>
                        <p><spring:theme code="text.account.order.orderNumberLabel"/><b> ${fn:escapeXml(orderData.externalCode)}</b></p>
                    </c:otherwise>
                </c:choose>
                <p><spring:theme code="checkout.orderConfirmation.copySentToShort"/><b> ${fn:escapeXml(email)}</b></p>
            </c:when>
            <c:otherwise>
                <p><spring:theme code="text.account.order.orderNumber.notAvailable"/></p>
            </c:otherwise>
        </c:choose>
    </div>

    <order:giftCoupons giftCoupons="${giftCoupons}"/>

</div>

<h1 class="vmb-checkout-confirmation__title">
    <spring:theme code="checkout.multi.order.summary"/>
</h1>
