package com.vanmarcke.blue.addon.controllers.cms;

import com.vanmarcke.core.model.ClientServiceParagraphComponentModel;
import de.hybris.bootstrap.annotations.UnitTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.ui.Model;

import javax.servlet.http.HttpServletRequest;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class BlueClientServiceParagraphComponentControllerTest {

    @InjectMocks
    private BlueClientServiceParagraphComponentController blueClientServiceParagraphComponentController;

    @Test
    public void testFillModel() {
        HttpServletRequest request = new MockHttpServletRequest();
        Model model = mock(Model.class);
        ClientServiceParagraphComponentModel component = mock(ClientServiceParagraphComponentModel.class);

        blueClientServiceParagraphComponentController.fillModel(request, model, component);

        verify(model).addAttribute("componentTitle", component.getTitle());
        verify(model).addAttribute("componentContent", component.getContent());
        verify(model).addAttribute("componentTelephone", component.getTelephone());
        verify(model).addAttribute("componentTelephoneFrontend", component.getTelephoneFrontend());
        verify(model).addAttribute("componentEmail", component.getEmail());
        verify(model).addAttribute("screenTakeoverUrl", component.getScreenTakeoverUrl());
    }
}