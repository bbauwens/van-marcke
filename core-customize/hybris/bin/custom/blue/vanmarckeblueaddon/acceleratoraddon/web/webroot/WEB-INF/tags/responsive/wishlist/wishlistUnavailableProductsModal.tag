<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<spring:htmlEscape defaultHtmlEscape="true"/>

<div class="vmb-modal fade" id="vmbUnavailableProductsModal" tabindex="-1" role="dialog" aria-labelledby="vmbUnavailableProductsModalTitle">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <spring:theme code="wishlist.unavailable.products"/>
            </div>
            <div class="modal-footer">

                <button type="button" class="btn btn-link" data-dismiss="modal"><spring:theme code="text.button.cancel"/></button>
                <input type="hidden" value="${fn:escapeXml(brokenWishlistName)}" id="brokenWishlistName">
                <input type="hidden" value="${fn:escapeXml(actionType)}" id="actionType">

                <button type="button"
                        id="js-wishlist-delete-unavailable"
                        class="btn btn-primary">
                    <spring:theme code="general.delete.button"/>
                </button>
            </div>
        </div>
    </div>
</div>
