<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ attribute name="list" required="true" type="java.util.ArrayList" %>
<%@ attribute name="title" required="true" type="java.lang.String" %>

<c:if test="${not empty list}">
    <tr>
        <td>
            <spring:theme code="${title}"/>
        </td>
        <td>
            <c:forEach items="${list}" var="item" varStatus="loop">
                <a href="${item.url}" download="pdf_image"
                   target="_blank">${item.name}</a>${loop.last ? '' : ', '}
            </c:forEach>
        </td>
    </tr>
</c:if>