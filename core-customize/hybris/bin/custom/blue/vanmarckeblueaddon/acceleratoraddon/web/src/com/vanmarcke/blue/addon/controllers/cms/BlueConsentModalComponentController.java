package com.vanmarcke.blue.addon.controllers.cms;

import com.vanmarcke.core.model.ConsentModalComponentModel;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

import static com.vanmarcke.blue.addon.controllers.VanmarckeblueaddonControllerConstants.Actions.Cms.CONSENT_MODAL_COMPONENT;

/**
 * Controller for the {@link ConsentModalComponentModel}.
 *
 * @author Tom van den Berg
 * @since 18-06-2020
 */
@Controller("ConsentModalComponentController")
@RequestMapping(value = CONSENT_MODAL_COMPONENT)
public class BlueConsentModalComponentController extends AbstractBlueCMSComponentController<ConsentModalComponentModel> {

    /**
     * {@inheritDoc}
     */
    @Override
    protected void fillModel(HttpServletRequest request, Model model, ConsentModalComponentModel component) {
        model.addAttribute("consentParagraphs", component.getConsents());
    }
}
