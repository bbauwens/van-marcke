package com.vanmarcke.blue.addon.controllers.pages;

import com.vanmarcke.blue.addon.controllers.VanmarckeblueaddonControllerConstants;
import com.vanmarcke.blue.addon.validation.BlueImportExcelSavedCartFormValidator;
import com.vanmarcke.facades.wishlist2.VMKWishListFacade;
import com.vanmarcke.services.order.cart.conversion.VMKCsvExcelConversionService;
import com.vanmarcke.storefront.controllers.ControllerConstants;
import de.hybris.platform.acceleratorfacades.cartfileupload.SavedCartFileUploadFacade;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.acceleratorstorefrontcommons.forms.ImportCSVSavedCartForm;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.servicelayer.user.UserService;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;

/**
 * Controller for importing CSV file(s)
 */
@Controller
@RequestMapping("/import/csv")
public class BlueImportCSVPageController extends AbstractPageController {

    private static final String SAVED_CART_PATH_SEGMENT = "/saved-cart";
    private static final String WISH_LIST_PATH_SEGMENT = "/wish-list";
    private static final String IMPORT_CSV_FILE_MAX_SIZE_BYTES_KEY = "import.csv.file.max.size.bytes";
    private static final String IMPORT_CSV_SAVED_CART_CMS_PAGE = "importCSVSavedCartPage";
    private static final String IMPORT_CSV_WISH_LIST_CMS_PAGE = "importCSVWishListPage";

    private static final Logger LOG = Logger.getLogger(BlueImportCSVPageController.class);

    @Resource
    private UserService userService;

    @Resource(name = "simpleBreadcrumbBuilder")
    private ResourceBreadcrumbBuilder resourceBreadcrumbBuilder;

    @Resource(name = "blueImportExcelSavedCartFormValidator")
    private BlueImportExcelSavedCartFormValidator formValidator;

    @Resource(name = "savedCartFileUploadFacade")
    private SavedCartFileUploadFacade savedCartFileUploadFacade;

    @Resource(name = "defaultExcelService")
    private VMKCsvExcelConversionService excelService;

    @Resource(name = "vmkWishListFacade")
    private VMKWishListFacade vmkWishListFacade;

    @GetMapping("/example.xlsx")
    @RequireHardLogIn
    public void example(HttpServletResponse response) throws IOException {
        IOUtils.copy(getResource().getInputStream(), response.getOutputStream());
        response.flushBuffer();
    }

    /**
     * Returns the resource.
     * <p>
     * If there's no resource for the user's language, the English version is used as a fallback.
     *
     * @return the resource
     */
    private ClassPathResource getResource() {
        ClassPathResource resource = new ClassPathResource(getFilePath(getLanguageCode()));
        if (!resource.exists()) {
            resource = new ClassPathResource(getFilePath("en"));
        }
        return resource;
    }

    /**
     * Returns the file path.
     *
     * @param languageCode the language code
     * @return the file path
     */
    private String getFilePath(String languageCode) {
        return MessageFormat.format("csv/example-{0}.xlsx", languageCode);
    }

    /**
     * Returns the code of the language for the current user.
     *
     * @return the language code
     */
    private String getLanguageCode() {
        String languageCode = "en";
        LanguageModel language = userService.getCurrentUser().getSessionLanguage();
        if (language != null) {
            languageCode = language.getIsocode().substring(0, 2);
        }
        return languageCode;
    }

    @RequestMapping(value = SAVED_CART_PATH_SEGMENT, method = RequestMethod.GET)
    @RequireHardLogIn
    public String savedCartImport(final Model model) throws CMSItemNotFoundException {
        model.addAttribute("csvFileMaxSize", getSiteConfigService().getLong(IMPORT_CSV_FILE_MAX_SIZE_BYTES_KEY, 0));

        storeCmsPageInModel(model, getContentPageForLabelOrId(IMPORT_CSV_SAVED_CART_CMS_PAGE));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(IMPORT_CSV_SAVED_CART_CMS_PAGE));
        model.addAttribute(WebConstants.BREADCRUMBS_KEY, this.resourceBreadcrumbBuilder.getBreadcrumbs("import.csv.savedCart.title"));
        model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);

        return VanmarckeblueaddonControllerConstants.Views.Pages.CSV.IMPORT_CSV_SAVED_CART_PAGE;
    }

    /**
     * This controller method has been changed to support importing of carts as excel files (xls/xlsx).
     * The incoming excel file is converted to a CSV file, which in turn will be saved as a cart.
     *
     * @param importCSVSavedCartForm the import form
     * @param bindingResult          the binding result
     * @return the server response
     */
    @ResponseBody
    @RequestMapping(value = SAVED_CART_PATH_SEGMENT, method = RequestMethod.POST)
    @RequireHardLogIn
    public ResponseEntity<String> handleSavedCartImport(
            @ModelAttribute("importCSVSavedCartForm") ImportCSVSavedCartForm importCSVSavedCartForm, BindingResult bindingResult) {
        this.formValidator.validate(importCSVSavedCartForm, bindingResult);
        if (bindingResult.hasErrors()) {
            final String errorMessage = getMessageSource().getMessage(bindingResult.getAllErrors().get(0).getCode(), null,
                    getI18nService().getCurrentLocale());
            return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
        } else {
            try {
                InputStream inputStream = excelService.convertXlsToCsv(importCSVSavedCartForm.getCsvFile());
                this.savedCartFileUploadFacade.createCartFromFileUpload(inputStream,
                        importCSVSavedCartForm.getCsvFile().getOriginalFilename(),
                        importCSVSavedCartForm.getCsvFile().getContentType());
                inputStream.close();
                return new ResponseEntity<>(HttpStatus.OK);
            } catch (final IOException e) {
                if (LOG.isDebugEnabled()) {
                    LOG.debug(e.getMessage(), e);
                }
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }

        }
    }

    @RequestMapping(value = WISH_LIST_PATH_SEGMENT, method = RequestMethod.GET)
    @RequireHardLogIn
    public String wishListImport(final Model model) throws CMSItemNotFoundException {
        model.addAttribute("csvFileMaxSize", getSiteConfigService().getLong(IMPORT_CSV_FILE_MAX_SIZE_BYTES_KEY, 0));

        storeCmsPageInModel(model, getContentPageForLabelOrId(IMPORT_CSV_WISH_LIST_CMS_PAGE));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(IMPORT_CSV_WISH_LIST_CMS_PAGE));
        model.addAttribute(WebConstants.BREADCRUMBS_KEY, resourceBreadcrumbBuilder.getBreadcrumbs("import.csv.savedCart.title"));
        model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);

        return ControllerConstants.Views.Pages.CSV.ImportCSVWishListPage;
    }

    @ResponseBody
    @RequestMapping(value = WISH_LIST_PATH_SEGMENT, method = RequestMethod.POST)
    @RequireHardLogIn
    public ResponseEntity<String> handleWishListImport(
            @ModelAttribute("importCSVSavedCartForm") final ImportCSVSavedCartForm importCSVWishListForm,
            final BindingResult bindingResult) throws IOException {
        formValidator.validate(importCSVWishListForm, bindingResult);
        if (bindingResult.hasErrors()) {
            final String errorMessage = getMessageSource().getMessage(bindingResult.getAllErrors().get(0).getCode(), null,
                    getI18nService().getCurrentLocale());
            return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
        } else {
            try (InputStream inputStream = excelService.convertXlsToCsv(importCSVWishListForm.getCsvFile())) {
                // convert the Excel sheet into a CSV file
                final byte[] csvContents = IOUtils.toByteArray(inputStream);

                // import the data
                vmkWishListFacade.importWishlistFromCsv(csvContents);

                // report succes!
                return new ResponseEntity<>(HttpStatus.OK);
            }
            catch (final IOException e)
            {
                if (LOG.isDebugEnabled())
                {
                    LOG.debug(e.getMessage(), e);
                }

                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }

        }
    }
}
