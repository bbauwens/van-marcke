package com.vanmarcke.blue.addon.controllers.consent;

import com.vanmarcke.facades.consent.VMKConsentFacade;
import com.vanmarcke.facades.data.VMKConsentData;
import de.hybris.bootstrap.annotations.UnitTest;
import org.apache.commons.lang3.RandomStringUtils;
import org.fest.assertions.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class BlueConsentControllerTest {

    private static final String CONSENTS = RandomStringUtils.random(10);

    @Mock
    private VMKConsentFacade facade;

    @InjectMocks
    private BlueConsentController controller;

    @Test
    public void testSaveConsents() {
        List<VMKConsentData> consents = Collections.singletonList(mock(VMKConsentData.class));
        HttpServletRequest request = mock(HttpServletRequest.class);

        controller.saveConsents(consents, request);

        verify(facade).process(consents, request);
        verifyNoMoreInteractions(facade);
    }

    @Test
    public void testGetConsents() {
        when(facade.getConsentsAsString()).thenReturn(CONSENTS);

        ResponseEntity<String> actualResponse = controller.getConsents();

        Assertions
                .assertThat(actualResponse.getStatusCode())
                .isEqualTo(HttpStatus.OK);

        Assertions
                .assertThat(actualResponse.getBody())
                .isEqualTo(CONSENTS);
    }
}