<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/product" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<spring:htmlEscape defaultHtmlEscape="true"/>

<spring:theme code="text.addToCart" var="addToCartText"/>
<spring:theme code="text.popupCartTitle" var="popupCartTitleText"/>
<c:url value="/cart" var="cartUrl"/>
<c:url value="/cart/checkout" var="checkoutUrl"/>

<c:choose>
    <c:when test="${not empty cartData.quoteData}">
        <c:set var="miniCartProceed" value="quote.view"/>
    </c:when>
    <c:otherwise>
        <c:set var="miniCartProceed" value="checkout.checkout"/>
    </c:otherwise>
</c:choose>


<div class="mini-cart js-mini-cart">
    <ycommerce:testId code="mini-cart-popup">
        <div class="mini-cart-body">
            <div class="legend">
                <h3><a href="${cartUrl}"><spring:theme code="text.cart"/></a></h3>
                <button class="btn btn-link js-vmb-mini-cart-close"><span class="glyphicon glyphicon-remove"></span></button>
            </div>
            <c:choose>
                <c:when test="${numberShowing > 0 }">

                    <ol class="mini-cart-list">
                        <c:forEach items="${entries}" var="entry">
                            <c:url value="${entry.product.url}" var="entryProductUrl"/>
                            <li class="mini-cart-item">
                                <div class="thumb">
                                    <a href="${entryProductUrl}">
                                        <product:productPrimaryImage product="${entry.product}" format="cartIcon"/>
                                    </a>
                                </div>
                                <div class="details">
                                    <a class="name" href="${entryProductUrl}">${fn:escapeXml(entry.product.name)}</a>
                                    <div class="sku"><spring:theme code="basket.page.entry.sku"/>: ${entry.product.code}</div>
                                    <div class="qty"><spring:theme code="popup.cart.quantity"/>: ${entry.quantity}</div>
                                </div>
                            </li>
                        </c:forEach>
                    </ol>

                    <div class="mini-cart-checkout-button__container">
                        <a href="" class="btn btn-link js-vmb-mini-cart-close">
                            <spring:theme code="cart.page.continue"/>
                        </a>
                        <a href="${cartUrl}" class="btn btn-default">
                            <spring:theme code="text.go.to.cart"/>
                        </a>
                    </div>

                    <c:if test="${not empty lightboxBannerComponent && lightboxBannerComponent.visible}">
                        <cms:component component="${lightboxBannerComponent}" evaluateRestriction="true"/>
                    </c:if>
                </c:when>

                <c:otherwise>
                    <ol class="mini-cart-list">
                        <li><spring:theme code="popup.cart.empty"/></li>
                    </ol>
                    <div class="mini-cart-checkout-button__container">
                        <a href="" class="btn btn-link js-vmb-mini-cart-close">
                            <spring:theme text="Continue Shopping" code="cart.page.continue"/>
                        </a>
                        <a href="${cartUrl}" class="btn btn-default">
                            <spring:theme code="text.go.to.cart"/>
                        </a>
                    </div>
                    <c:if test="${not empty lightboxBannerComponent && lightboxBannerComponent.visible}">
                        <cms:component component="${lightboxBannerComponent}" evaluateRestriction="true"/>
                    </c:if>
                </c:otherwise>
            </c:choose>
        </div>
    </ycommerce:testId>
</div>


