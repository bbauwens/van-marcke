package com.vanmarcke.blue.addon.controllers.cms;

import com.vanmarcke.core.model.FooterPaymentModesComponentModel;
import com.vanmarcke.facades.payment.data.PaymentModeData;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.payment.PaymentModeModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.ui.Model;

import java.util.Collections;

import static java.util.Arrays.asList;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class BlueFooterPaymentModesComponentControllerTest {

    @Mock
    private BaseStoreService baseStoreService;
    @Mock
    private Converter<PaymentModeModel, PaymentModeData> paymentModeConverter;

    @InjectMocks
    private BlueFooterPaymentModesComponentController blueFooterPaymentModesComponentController;

    @Test
    public void testFillModel() {
        final MockHttpServletRequest request = new MockHttpServletRequest();
        final Model model = mock(Model.class);
        final FooterPaymentModesComponentModel component = mock(FooterPaymentModesComponentModel.class);
        final BaseStoreModel baseStore = mock(BaseStoreModel.class);
        final PaymentModeModel activePaymentModeModel = mock(PaymentModeModel.class);
        final PaymentModeModel inactivePaymentModeModel = mock(PaymentModeModel.class);

        when(activePaymentModeModel.getActive()).thenReturn(true);

        PaymentModeData activePaymentModeData = mock(PaymentModeData.class);
        when(paymentModeConverter.convert(activePaymentModeModel)).thenReturn(activePaymentModeData);

        when(baseStoreService.getCurrentBaseStore()).thenReturn(baseStore);
        when(baseStore.getPaymentModes()).thenReturn(asList(activePaymentModeModel, inactivePaymentModeModel));

        this.blueFooterPaymentModesComponentController.fillModel(request, model, component);

        verify(model).addAttribute("paymentModes", Collections.singletonList(activePaymentModeData));
    }
}