<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ attribute name="list" required="false" type="java.lang.String" %>
<%@ attribute name="position" required="false" type="java.lang.Integer" %>
<%@ attribute name="plvVariantsCount" required="false" type="java.lang.Integer" %>
<%@ attribute name="categoryCode" required="false" type="java.lang.String" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/product"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="action" tagdir="/WEB-INF/tags/responsive/action" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/shared/format" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<spring:htmlEscape defaultHtmlEscape="true" />

<c:url value="${product.url}" var="productUrl"/>
<c:set value="${not empty product.potentialPromotions}" var="hasPromotion"/>

<c:set var="productSearchQueryUrl" value="${productUrl}"/>

<c:choose>
    <c:when test="${not empty searchQuery && not empty searchText}">
        <c:set var="productSearchQueryUrl" value="${productUrl}?q=${ycommerce:encodeUrl(searchQuery)}&text=${ycommerce:encodeUrl(searchText)}"/>
    </c:when>
    <c:when test="${not empty searchQuery && empty searchText}">
        <c:set var="productSearchQueryUrl" value="${productUrl}?q=${ycommerce:encodeUrl(searchQuery)}"/>
    </c:when>
    <c:otherwise>
        <c:set var="productSearchQueryUrl" value="${productUrl}?text=${ycommerce:encodeUrl(searchText)}"/>
    </c:otherwise>
</c:choose>

<c:if test="${not empty categoryCode}">
    <c:set var="productSearchQueryUrl" value="${productSearchQueryUrl}&category=${ycommerce:encodeUrl(categoryCode)}"/>
</c:if>

<sec:authorize var="isAnonymousUser" access="hasAnyRole('ROLE_ANONYMOUS')"/>

<c:set var="tableType" value=""/>

<c:if test="${user.showNetPrice}">
    <c:set var="tableType" value="net-price-shown"/>
</c:if>

<c:if test="${cmsSite.channel == 'DIY'}">
    <c:set var="tableType" value="diy-anonymous"/>
    <c:if test="${!isAnonymousUser}">
        <c:set var="tableType" value="diy-logged-in"/>
    </c:if>
</c:if>

<input type="hidden" value="${cmsSite.channel == 'DIY'}" id="isDIY"/>

<div class="col-xs-10 col-xs-push-1 col-sm-push-0 col-sm-offset-1 col-sm-10 col-md-offset-0 col-md-12 no-padding">
    <div class="vmb-product-main-info vmb-gtm-product-data"
         data-list="${list}"
         data-code="${fn:escapeXml(product.code)}"
         data-name="${fn:escapeXml(product.name)}"
         data-category="${fn:escapeXml(product.analyticsData.category)}"
         data-brand="${fn:escapeXml(product.analyticsData.brand)}"
         data-url="${productSearchQueryUrl}"
         data-position="${position}">

        <div class="vmb-plp-list-product-headline">
            <c:if test="${not empty product.brand.logo}">
                <div class="vmb-product-brandLogo">
                    <img src="${product.brand.logo.url}" alt="brand-logo"/>
                </div>
            </c:if>
            <div class="vmb-product-info-headline">
                <c:if test="${not empty product.images}">
                    <product:productPrimaryImage product="${product}" format="thumbnail"/>
                </c:if>
                <h3 class="product-title">${fn:escapeXml(product.baseProductName)}</h3>
            </div>
            <a type="button" class="btn btn-default vmb-gtm-product-item" href="${productSearchQueryUrl}">
                <spring:theme code="productList.moreInformation"/>
            </a>
        </div>

        <div class="vmb-plp-list-product-content">
            <c:if test="${not empty product.variantOptions}">
                <c:set var="variantOptions" value="${product.variantOptions}"/>
            </c:if>
            <c:if test="${not empty product.baseOptions[0].options}">
                <c:set var="variantOptions" value="${product.baseOptions[0].options}"/>
            </c:if>

            <div class="vmb-product-variant-table__container">
                <div class="vmb-product-variant-table__wrapper ${tableType}">
                    <table class="table table-condensed vmb-product-variant-table">
                        <thead>
                        <tr>
                            <th class="sku"><spring:theme code="pdp.varianttable.sku"/></th>
                            <th class="name"><spring:theme code="pdp.varianttable.name"/></th>
                            <th class="stock-warehouse" data-toggle="tooltip" data-placement="top"
                                title="<spring:theme code="text.stockstatus.icon.warehouse" htmlEscape="false"/>">
                                <span class="icon icon-VM_BLUE_logo"></span>
                            </th>
                            <c:if test="${cmsSite.channel != 'DIY'}">
                                <th class="stock-tec">
                                    <format:tecIcon/><format:nostock/>
                                </th>
                                <th class="price">
                                    <spring:theme code="pdp.varianttable.price" arguments="${currentCurrency.symbol}"/>
                                </th>
                            </c:if>
                            <th class="net-price vmb-netto-price-display <c:if test="${!user.showNetPrice or (isAnonymousUser and cmsSite.channel == 'DIY')}">hidden</c:if>">
                                <spring:theme code="pdp.varianttable.netprice" arguments="${currentCurrency.symbol}"/>
                            </th>
                            <th class="qty"><spring:theme code="pdp.varianttable.pieces"/></th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:if test="${not empty variantOptions}">
                            <c:set var="listSize" value="${variantOptions.size() >= plvVariantsCount ? plvVariantsCount : variantOptions.size()}"/>
                            <c:forEach items="${variantOptions}" var="variant" varStatus="loopIndex">
                                <c:if test="${loopIndex.index < listSize}">
                                    <c:url value="${variant.url}" var="variantUrl"/>
                                    <tr class="vmb-product-variant-table__row ${product.code.equals(variant.code) ? ' active' : ''}"
                                        data-product-url="${variantUrl}" data-code="${variant.code}" data-position="${loopIndex.index + 1}"
                                        data-price="${variant.priceData.value}" data-name="${variant.name}">
                                        <td class="sku" data-label="<spring:theme code="pdp.varianttable.sku"/>"><a
                                                href="${variantUrl}">${variant.code}</a></td>
                                        <td class="name" data-label="<spring:theme code="pdp.varianttable.name"/>">${variant.name}
                                            <c:if test="${variant.stockStatus eq 'NO_STOCK'}">
                                                &nbsp;<span class="vmk-no-stock" data-toggle="tooltip" data-placement="top"
                                                title="<spring:theme code="product.product.details.nostock.message"
                                                                     htmlEscape="false"/>">No Stock</span>
                                            </c:if>
                                            <c:if test="${variant.stockStatus eq 'SOON_IN_STOCK'}">
                                                &nbsp;<span class="vmk-soon-in-stock" data-toggle="tooltip" data-placement="top"
                                                title="<spring:theme code="product.details.upcoming.message"
                                                                     htmlEscape="false"/>"><spring:theme code="product.details.upcoming.label"/> </span>
                                            </c:if>
                                            <c:if test="${variant.stockStatus eq 'LIMITED_STOCK'}">
                                                &nbsp;<span class="vmk-soon-in-stock" data-toggle="tooltip" data-placement="top"
                                                title="<spring:theme code="product.details.discontinued.message"
                                                                     htmlEscape="false"/>"><spring:theme code="product.details.discontinued.label"/> </span>
                                            </c:if>
                                        </td>
                                        <td class="stock stock-warehouse" id="stock-warehouse-${loopIndex.index}">
                                            <format:stock type="warehouse" stock="${variant.stock}" code="${variant.code}"/></td>
                                        <c:if test="${cmsSite.channel != 'DIY'}">
                                            <td class="stock stock-tec"
                                                id="stock-tec-${loopIndex.index}">
                                                <format:stock type="store" stock="${variant.stock}" code="${variant.code}"/></td>
                                            <td class="price"
                                                data-label="<spring:theme code="pdp.varianttable.price" arguments="${currentCurrency.symbol}"/>">
                                                <format:price priceData="${variant.priceData}"/></td>
                                        </c:if>
                                        <td class="net-price vmb-netto-price-display <c:if test="${!user.showNetPrice or (isAnonymousUser and cmsSite.channel == 'DIY')}">hidden</c:if>"
                                            data-label="<spring:theme code="pdp.varianttable.netprice" arguments="${currentCurrency.symbol}"/>">
                                            <format:price priceData="${variant.netPriceData}"/></td>
                                        <td class="qty" data-label="<spring:theme code="pdp.varianttable.pieces"/>">
                                            <input type="number"
                                                   class="form-control vmb-product-quantity"
                                                   data-code="${variant.code}" ${empty variant.priceData ? 'disabled' : ''} ${variant.stockStatus ne 'IN_STOCK' ? 'disabled' : ''}/>
                                        </td>
                                    </tr>
                                </c:if>
                            </c:forEach>
                        </c:if>
                        </tbody>
                    </table>
                </div>
                <c:if test="${variantOptions ne null and variantOptions.size() > plvVariantsCount}">
                    <a href="${productSearchQueryUrl}"><spring:theme code="plp.view.more.variants"/></a>
                </c:if>
            </div>
        </div>
    </div>
</div>


<div style="display: none">
    <span id="wishlist-no-products-selected-message">
        <spring:theme code="wishlist.no.products.selected"/>
    </span>
</div>

