package com.vanmarcke.blue.addon.controllers.cms;

import com.vanmarcke.core.model.FooterPaymentModesComponentModel;
import com.vanmarcke.facades.paymentmodes.VMKPaymentModeFacade;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import static com.vanmarcke.blue.addon.controllers.VanmarckeblueaddonControllerConstants.Actions.Cms.FOOTER_PAYMENT_MODES_COMPONENT;

/**
 * FooterPaymentModesComponentController
 */
@Controller("FooterPaymentModesComponentController")
@RequestMapping(value = FOOTER_PAYMENT_MODES_COMPONENT)
public class BlueFooterPaymentModesComponentController extends AbstractBlueCMSComponentController<FooterPaymentModesComponentModel> {

    @Resource(name = "vmkPaymentModeFacade")
    private VMKPaymentModeFacade vmkPaymentModeFacade;

    @Override
    protected void fillModel(final HttpServletRequest request, final Model model, final FooterPaymentModesComponentModel component) {
        model.addAttribute("paymentModes", vmkPaymentModeFacade.getSupportedPaymentModes());
    }
}
