<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json" %>

<script type="text/javascript">
    function trackProductSearch(searchTerm, products) {
        dataLayer.push({
            'event'    : 'search',
            'ecommerce': {
                'searchRefinements'         : {
                    'terms': [
                        {
                            'text'    :  searchTerm
                        }
                    ],
                    'products' : products
                }
            }
        });
    }

    function trackNoSearchResult(searchTerm) {
        dataLayer.push({
            'event'    : 'searchNoResult',
            'ecommerce': {
                'searchRefinements'         : {
                    'terms': [
                        {
                            'text'    :  searchTerm
                        }
                    ]
                }
            }
        });
    }


    function trackSearchClick(searchTerm) {
        dataLayer.push({
            'event'    : 'searchClick',
            'ecommerce': {
                'searchRefinements'         : {
                    'terms': [
                        {
                            'text'    :  searchTerm
                        }
                    ]
                }
            }
        });
    }


    window.mediator.subscribe('trackSearch', function(data) {
        if(data.hasOwnProperty('searchTerm')  && data.hasOwnProperty('products')) {
            trackProductSearch(data.searchTerm, data.products);
        }
    });

    window.mediator.subscribe('trackSearchNoResult', function(data) {
        if(data.hasOwnProperty('searchTerm')) {
            trackNoSearchResult(data.searchTerm);
        }
    });

    window.mediator.subscribe('trackSearchClick', function(data) {
        if(data.hasOwnProperty('searchTerm')) {
            ACC.track.searchResult(ACC.autocomplete.searchProductResults, $("#js-site-search-input").val());
            trackSearchClick(data.searchTerm);
        }
    });

</script>