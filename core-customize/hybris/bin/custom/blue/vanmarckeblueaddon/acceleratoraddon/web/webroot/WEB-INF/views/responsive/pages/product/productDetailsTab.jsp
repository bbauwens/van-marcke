<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/product" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:if test="${not empty product.description || not empty product.supplierReference}">
    <div class="tabhead">
        <a href="">${fn:escapeXml(title)}</a> <span class="glyphicon"></span>
    </div>
    <div class="tabbody">
        <div class="tab-container">
            <product:productDetailsTab product="${product}"/>
        </div>
    </div>
</c:if>