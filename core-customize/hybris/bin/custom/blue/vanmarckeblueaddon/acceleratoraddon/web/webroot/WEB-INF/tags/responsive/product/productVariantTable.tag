<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/shared/format" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<c:if test="${not empty product.variantOptions}">
    <c:set var="variantOptions" value="${product.variantOptions}"/>
</c:if>
<c:if test="${not empty product.baseOptions[0].options}">
    <c:set var="variantOptions" value="${product.baseOptions[0].options}"/>
</c:if>

<sec:authorize var="isAnonymousUser" access="hasAnyRole('ROLE_ANONYMOUS')"/>

<c:set var="tableType" value=""/>

<c:if test="${user.showNetPrice}">
    <c:set var="tableType" value="net-price-shown"/>
</c:if>

<c:if test="${cmsSite.channel == 'DIY'}">
    <c:set var="tableType" value="diy-anonymous"/>
    <c:if test="${!isAnonymousUser}">
        <c:set var="tableType" value="diy-logged-in"/>
    </c:if>
</c:if>

<input type="hidden" value="${cmsSite.channel == 'DIY'}" id="isDIY"/>

<div class="vmb-product-variant-table__container">
    <div class="vmb-product-variant-table__wrapper ${tableType}">
        <table class="table table-condensed vmb-product-variant-table"
               data-category="${product.analyticsData.category}" data-brand="${product.analyticsData.brand}">
            <thead>
            <tr>
                <th class="sku"><spring:theme code="pdp.varianttable.sku"/></th>
                <th class="name"><spring:theme code="pdp.varianttable.name"/></th>
                <c:forEach items="${distinctiveFeaturesHeaders}" var="attr">
                    <th class="definingAttr">${attr.value}</th>
                </c:forEach>
                <th class="stock-warehouse" data-toggle="tooltip" data-placement="top"
                    title="<spring:theme code="text.stockstatus.icon.warehouse" htmlEscape="false"/>">
                    <span class="icon icon-VM_BLUE_logo"></span>
                </th>
                <c:if test="${cmsSite.channel != 'DIY'}">
                    <th class="stock-tec">
                        <format:tecIcon/><format:nostock/>
                    </th>
                    <th class="price">
                        <spring:theme code="pdp.varianttable.price" arguments="${currentCurrency.symbol}"/>
                    </th>
                </c:if>
                <th class="net-price vmb-netto-price-display <c:if test="${!user.showNetPrice or (isAnonymousUser and cmsSite.channel == 'DIY')}">hidden</c:if>">
                    <spring:theme code="pdp.varianttable.netprice" arguments="${currentCurrency.symbol}"/>
                </th>
                <th class="qty"><spring:theme code="pdp.varianttable.pieces"/></th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${variantOptions}" var="variant" varStatus="loopIndex">
                <c:url value="${variant.url}" var="variantUrl"/>
                <tr class="vmb-product-variant-table__row ${product.code.equals(variant.code) ? ' active' : ''}"
                    data-product-url="${variantUrl}" data-code="${variant.code}" data-position="${loopIndex.index + 1}"
                    data-price="${variant.priceData.value}" data-name="${variant.name}">
                    <td class="sku" data-label="<spring:theme code="pdp.varianttable.sku"/>"><a
                            href="${variantUrl}">${variant.code}</a></td>
                    <td class="name" data-label="<spring:theme code="pdp.varianttable.name"/>">${variant.name}
                        <c:if test="${variant.stockStatus eq 'NO_STOCK'}">
                            &nbsp;<span class="vmk-no-stock" data-toggle="tooltip" data-placement="top"
                            title="<spring:theme code="product.product.details.nostock.message"
                                                 htmlEscape="false"/>">No Stock</span>
                        </c:if>
                        <c:if test="${variant.stockStatus eq 'SOON_IN_STOCK'}">
                            &nbsp;<span class="vmk-soon-in-stock" data-toggle="tooltip" data-placement="top"
                            title="<spring:theme code="product.details.upcoming.message"
                                                 htmlEscape="false"/>"><spring:theme code="product.details.upcoming.label"/> </span>
                        </c:if>
                        <c:if test="${variant.stockStatus eq 'LIMITED_STOCK'}">
                            &nbsp;<span class="vmk-soon-in-stock" data-toggle="tooltip" data-placement="top"
                            title="<spring:theme code="product.details.discontinued.message"
                                                 htmlEscape="false"/>"><spring:theme code="product.details.discontinued.label"/> </span>
                        </c:if>
                    </td>
                    <c:forEach items="${distinctiveFeaturesHeaders}" var="attr">
                        <td class="definingAttr" data-label="${attr.value}">
                            <c:set var="defa" value="${variant.definingAttributes.get(attr.key)}"/>

                            <c:if test="${not empty defa.featureValues}">
                                <c:set var="defaValue" value="${defa.featureValues[0].value}"/>

                                <c:choose>
                                    <c:when test="${'true'.equals(defaValue)}">
                                        <span class="glyphicon glyphicon-ok"></span>
                                    </c:when>
                                    <c:when test="${'false'.equals(defaValue)}">
                                        <span class="glyphicon glyphicon-remove"></span>
                                    </c:when>
                                    <c:otherwise>
                                        ${defaValue}
                                    </c:otherwise>
                                </c:choose>
                            </c:if>
                        </td>
                    </c:forEach>
                    <td class="stock stock-warehouse" id="stock-warehouse-${loopIndex.index}">
                        <format:stock type="warehouse" stock="${variant.stock}" code="${variant.code}"/></td>
                    <c:if test="${cmsSite.channel != 'DIY'}">
                        <td class="stock stock-tec"
                            id="stock-tec-${loopIndex.index}">
                            <format:stock type="store" stock="${variant.stock}" code="${variant.code}"/></td>
                        <td class="price"
                            data-label="<spring:theme code="pdp.varianttable.price" arguments="${currentCurrency.symbol}"/>">
                            <format:price priceData="${variant.priceData}"/></td>
                    </c:if>
                    <td class="net-price vmb-netto-price-display <c:if test="${!user.showNetPrice or (isAnonymousUser and cmsSite.channel == 'DIY')}">hidden</c:if>"
                        data-label="<spring:theme code="pdp.varianttable.netprice" arguments="${currentCurrency.symbol}"/>">
                        <format:price priceData="${variant.netPriceData}"/></td>
                    <td class="qty" data-label="<spring:theme code="pdp.varianttable.pieces"/>">
                        <input type="number"
                               class="form-control vmb-product-quantity"
                               data-code="${variant.code}" ${empty variant.priceData ? 'disabled' : ''} ${variant.stockStatus ne 'IN_STOCK' ? 'disabled' : ''}/>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
    <div class="vmb-product-variant-addtocart">
        <sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
            <button type="button" class="btn btn-primary js-add-to-wishlist">
                <spring:theme code="text.saveForLater"/>
            </button>
        </sec:authorize>
        <button type="button" class="btn btn-default vmb-add-to-cart" onclick="addToCartMulti()">
            <span class="glyphicon glyphicon-shopping-cart"></span>&nbsp;<spring:theme code="text.addToCart"/>
        </button>
    </div>
</div>

<div style="display: none">
    <span id="wishlist-no-products-selected-message">
        <spring:theme code="wishlist.no.products.selected"/>
    </span>
</div>

<script>
    var vmbActiveProduct = "${product.code}";
</script>
<script id="productVariantTablePagTemplate" type="text/x-jsrender">
    <ul class="pagination">
        {{if current < 2}}
            <li class="pagination-prev disabled"><span class="glyphicon glyphicon-triangle-left"></span></li>
            <li class="pagination-prev disabled"><span class="glyphicon glyphicon-chevron-left"></span></li>
        {{else}}
            <li class="pagination-prev"><a href="#" rel="prev" onclick="pvmGoto(1, event)" class="glyphicon glyphicon-triangle-left"></a></li>
            <li class="pagination-prev"><a href="#" rel="prev" onclick="pvmGoto({{:prev}}, event)" class="glyphicon glyphicon-chevron-left"></a></li>
        {{/if}}

        {{for start=1 end=numPages+1 noIteration=true itemVar="~page"}}
            {{for ~page  itemVar="~i"}}
                {{if ~root.current == ~i}}
                    <li class="active"><span>{{:~i}}<span class="sr-only">(current)</span></span></li>
                {{else}}
                    <li><a class="" href="#" onclick="pvmGoto({{:~i}}, event)">{{:~i}}</a></li>
                {{/if}}
            {{/for}}
        {{/for}}

        {{if current < numPages}}
            <li class="pagination-next"><a href="#" onclick="pvmGoto({{:next}}, event)" rel="next" class="glyphicon glyphicon-chevron-right"></a></li>
            <li class="pagination-next"><a href="#" onclick="pvmGoto({{:numPages}}, event)" rel="next" class="glyphicon glyphicon-triangle-right"></a></li>
        {{else}}
            <li class="pagination-next disabled"><span class="glyphicon glyphicon-chevron-right"></span></li>
            <li class="pagination-next disabled"><span class="glyphicon glyphicon-triangle-right"></span></li>
        {{/if}}
    </ul>
</script>


