<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<script type="text/javascript">
    function trackAddToCart(products) {

        dataLayer.push({
            'event'    : 'addToCart',
            'ecommerce': {
                'currencyCode': '${currentCurrency.isocode}',
                'add'         : {
                    'products': products
                }
            }
        });
    }

    window.mediator.subscribe('trackAddToCart', function(data) {
        if(data.products && _.isArray(data.products)) {
            trackAddToCart(data.products);
        }else if (data.products && _.isObject(data.products)){
            trackAddToCart([data.products]);
        }
    });
</script>