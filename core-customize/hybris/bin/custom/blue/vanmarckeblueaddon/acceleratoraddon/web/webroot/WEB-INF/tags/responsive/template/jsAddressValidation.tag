<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<script id="vmbAdressValidationBodyTemplate" type="text/x-jsrender">
    <div class="vmb-addressvalidation-modal__suggestions" style="height: {{:suggestionsHeight}}px">
        {{if suggestions.length == 0}}
            <div>
              <label>
                <spring:theme code="address.validation.modal.invalid"/><br>
              </label>
            </div>
        {{/if}}
        {{if suggestions.length != 0}}
            <label>
            <spring:theme code="address.validation.modal.subtitle"/>
            </label>
            {{for suggestions}}
                <div class="radio">
                  <label>
                    <input type="radio" name="chosenAddress" value="{{:#index}}">
                    {{:line1}} {{:line2}} {{:apartment}}, {{:postalCode}} {{:town}}
                  </label>
                </div>
            {{/for}}
        {{/if}}
    </div>
    <div class="vmb-addressvalidation-modal__original">
        <label>
            <spring:theme code="address.validation.modal.keep"/>
        </label>
        <div class="radio">
          <label>
            <input type="radio" name="chosenAddress" value="origin" checked>
            {{:originalAddress.line1}} {{:originalAddress.line2}} {{:originalAddress.apartment}}, {{:originalAddress.postalCode}} {{:originalAddress.town}}
          </label>
        </div>
    </div>
</script>

