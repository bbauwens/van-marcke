<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/checkout/multi" %>

<%@ attribute name="cartData" required="true" type="de.hybris.platform.commercefacades.order.data.CartData" %>

<ycommerce:testId code="checkoutStepThree">
    <div class="step-body-form vmb-checkout-paymentaddress">

        <c:if test="${not empty cartData.paymentInfo && not empty cartData.paymentInfo.billingAddress}">
            <div class="address">
                <multi-checkout:paymentAddress cartData="${cartData}"/>
            </div>
            <div class="alert alert-info">
                <spring:theme code="checkout.multi.paymentAddress.information"/>
            </div>
        </c:if>

        <spring:url var="confirmPaymentAddressUrl" value="{contextPath}/checkout/multi/payment-address/confirm"
                    htmlEscape="false">
            <spring:param name="contextPath" value="${request.contextPath}"/>
        </spring:url>

        <div class="text-right">
            <a href="${confirmPaymentAddressUrl}" id="confirmPaymentAddressSubmit" type="button"
               class="btn btn-default"> <spring:theme code="checkout.multi.deliveryMethod.continue"/></a>
        </div>
    </div>
</ycommerce:testId>