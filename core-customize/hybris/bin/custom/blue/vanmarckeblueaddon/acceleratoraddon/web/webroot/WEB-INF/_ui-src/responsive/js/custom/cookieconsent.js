(function () {
        var COOKIE_NAME = 'CONSENTS';
        var COOKIE_EXPIRY = 365;

        $(document).ready(function () {
            var valid = false;
            var storedConsents = getCookie(COOKIE_NAME);
            if (areValidConsents(storedConsents)) {
                valid = true;
            } else if (isLoggedIn()) {
                var consents = getConsents();
                setCookie(COOKIE_NAME, consents, COOKIE_EXPIRY);
                if (areValidConsents(consents)) {
                    valid = true;
                }
            }

            if (!valid) {
                $('#cookieModal').modal('show');
            }
        })

        /**
         * Checks whether the user is logged in
         *
         * @return boolean {@code true} in case the user is logged in, {@code false} otherwise
         */
        function isLoggedIn() {
            var oUserInfo = $(".nav__right ul li.logged_in");
            return oUserInfo && oUserInfo.length === 1;
        }

        /**
         * Checks whether the given consents are valid.
         *
         * @param consents the cookie
         * @returns boolean {@code true} in case the consents are valid, {@code false} otherwise
         */
        function areValidConsents(consents) {
            if (!consents) {
                return false;
            }
            var existingConsentTypes = getExistingConsentTypes(consents);
            for (var i = 0; i < ACC.config.consentTypes.length; i++) {
                var found = false;
                for (var j = 0; j < existingConsentTypes.length; j++) {
                    if (ACC.config.consentTypes[i] === existingConsentTypes[j]) {
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    return false;
                }
            }
            return true;
        }

        /**
         * Returns the existing consent types for the given {@code consents}.
         *
         * @param consents the consents
         * @returns {[]} the existing consent types
         */
        function getExistingConsentTypes(consents) {
            var consentsTypes = [];
            for (var i = 0; i < consents.length; i++) {
                consentsTypes.push(consents[i].consentType);
            }
            return consentsTypes;
        }

        /**
         * Handles the 'accept settings' submit button.
         */
        $('#cookieModalConsentSettings').on('click', function () {
            var data = getConsentsFromForm();
            setCookie(COOKIE_NAME, data, COOKIE_EXPIRY);
            sendConsents(data);
        });

        /**
         * Returns the consents from the form.
         *
         * @returns {[]} the consents
         */
        function getConsentsFromForm() {
            var data = [];
            for (var i = 0; i < ACC.config.consentTypes.length; i++) {
                var consentType = ACC.config.consentTypes[i];
                data.push({
                    consentType: consentType,
                    optType: $('#consent-toggle-' + consentType).prop("checked") ? 'OPT_IN' : 'OPT_OUT'
                })
            }
            return data;
        }

        /**
         * Handles the 'accept all' submit button.
         */
        $('#cookieModalConsentAll').on('click', function () {
            var data = getConsentsForAgreeAll();
            setCookie(COOKIE_NAME, data, COOKIE_EXPIRY);
            sendConsents(data);
        });

        /**
         * Hides/unhides the relevant menu items
         *
         * @param event the event
         */
        $('div.manage-consent-item').click(function () {
            $('div.consent-content').addClass('hidden');
            $('#consent-content-' + $(this).data('consenttype')).removeClass('hidden')
        });

        /**
         * Returns the consents for people who are too lazy to read and agree with everything.
         *
         * @returns {[]} the consents for lazy people
         */
        function getConsentsForAgreeAll() {
            var data = [];
            for (var i = 0; i < ACC.config.consentTypes.length; i++) {
                data.push({
                    consentType: ACC.config.consentTypes[i],
                    optType: 'OPT_IN'
                })
            }
            return data;
        }

        /**
         * Sets the cookie in the browser.
         *
         * @param name the name of the cookie
         * @param value the value to set
         * @param days the days to expiration
         */
        function setCookie(name, value, days) {
            var expires = "";
            if (days) {
                var date = new Date();
                date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                expires = "; expires=" + date.toUTCString();
            }
            document.cookie = name + "=" + encodeURI(JSON.stringify(value) || "") + expires + "; path=/";
        }

        /**
         * Returns the cookie value for the cookie with the given {@code name}
         *
         * @param name the cookie name
         * @return {any} the cookie value
         */
        function getCookie(name) {
            var decodedUri = decodeURI($.cookie(name));

            if (decodedUri) {
                return JSON.parse(decodedUri);
            }
            return undefined;
        }

        /**
         * Sends the consent data to the backend.
         *
         * @param data the data to send
         */
        function sendConsents(data) {
            $.ajax({
                url: ACC.config.encodedContextPath + "/consents",
                type: 'POST',
                data: JSON.stringify(data),
                contentType: 'application/json',
                dataType: 'json',
                async: true,
                beforeSend: function () {
                    showLoader();
                },
                complete: function (response) {
                    hideLoader()
                }
            });
        }

        /**
         * Returns the consents
         *
         * @return the consents
         */
        function getConsents() {
            $.ajax({
                url: ACC.config.encodedContextPath + "/consents",
                type: 'GET',
                async: true,
                beforeSend: function () {
                    showLoader();
                },
                complete: function (response) {
                    hideLoader();
                    return response.responseJSON;
                }
            });
        }
    }
)(jQuery);