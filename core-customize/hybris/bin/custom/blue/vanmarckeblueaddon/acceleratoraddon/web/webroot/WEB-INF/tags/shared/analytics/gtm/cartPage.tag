<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json" %>


<c:set var="prodPosition" value="${1}" />

dataLayer.push(
<json:object>
    <json:property name="event" value="impressions"/>
    <json:object name="ecommerce">
        <json:property name="currencyCode" value="${currentCurrency.isocode}"/>
        <json:array name="impressions" items="${cartData.entries}" var="entry">
            <json:object>
                <json:property name="name" value="${fn:escapeXml(entry.product.analyticsData.name)}"/>
                <json:property name="id" value="${entry.product.code}"/>
                <json:property name="price" value="${entry.basePrice.value}"/>
                <json:property name="category" value="${entry.product.analyticsData.category}"/>
                <json:property name="brand" value="${entry.product.analyticsData.brand}"/>
                <json:property name="list" value="Cart"/>
                <json:property name="position" value="${prodPosition}"/>
            </json:object>
            <c:set var="prodPosition" value="${prodPosition + 1}" />
        </json:array>
    </json:object>
</json:object>
);