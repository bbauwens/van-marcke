package com.vanmarcke.blue.addon.form;

/**
 * Form used to export a wish list.
 */
public class ExportSavedBasketForm {

    private String basketName;
    private boolean exportPdfNettoPrice;
    private boolean exportPdfIncludeImage;
    private boolean exportPdfTechnical;
    private int exportPdfLines;
    private String exportType;
    private boolean exportExcelNettoPrice;
    private String basketType;

    public boolean isExportPdfNettoPrice() {
        return exportPdfNettoPrice;
    }

    public void setExportPdfNettoPrice(boolean exportPdfNettoPrice) {
        this.exportPdfNettoPrice = exportPdfNettoPrice;
    }

    public boolean isExportPdfIncludeImage() {
        return exportPdfIncludeImage;
    }

    public void setExportPdfIncludeImage(boolean exportPdfIncludeImage) {
        this.exportPdfIncludeImage = exportPdfIncludeImage;
    }

    public boolean isExportPdfTechnical() {
        return exportPdfTechnical;
    }

    public void setExportPdfTechnical(boolean exportPdfTechnical) {
        this.exportPdfTechnical = exportPdfTechnical;
    }

   public String getExportType() {
        return exportType;
    }

    public void setExportType(String exportType) {
        this.exportType = exportType;
    }

    public boolean isExportExcelNettoPrice() {
        return exportExcelNettoPrice;
    }

    public void setExportExcelNettoPrice(boolean exportExcelNettoPrice) {
        this.exportExcelNettoPrice = exportExcelNettoPrice;
    }

    public int getExportPdfLines() {
        return exportPdfLines;
    }

    public void setExportPdfLines(int exportPdfLines) {
        this.exportPdfLines = exportPdfLines;
    }

    public String getBasketType() {
        return basketType;
    }

    public void setBasketType(String basketType) {
        this.basketType = basketType;
    }

    public String getBasketName() {
        return basketName;
    }

    public void setBasketName(String basketName) {
        this.basketName = basketName;
    }


}
