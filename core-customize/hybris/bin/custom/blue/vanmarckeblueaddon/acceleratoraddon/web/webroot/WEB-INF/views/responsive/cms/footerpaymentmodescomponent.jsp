<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="vmb-paymentmodes">
    <c:forEach var="paymentMode" items="${paymentModes}">
        <c:if test="${not empty paymentMode.thumbnail && not empty paymentMode.thumbnail.url}">
            <img class="vmb-payment-mode__img" src="${paymentMode.thumbnail.url}" alt="${paymentMode.code}"/>
        </c:if>
    </c:forEach>
</div>