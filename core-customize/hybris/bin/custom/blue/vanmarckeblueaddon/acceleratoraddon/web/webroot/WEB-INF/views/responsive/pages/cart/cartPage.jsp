<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/template" %>
<%@ taglib prefix="event" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/shared/event" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/cart" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<spring:htmlEscape defaultHtmlEscape="true"/>

<template:page pageTitle="${pageTitle}">

    <cart:cartValidation/>
    <cart:cartPickupValidation/>

    <div>
        <div>
            <cms:pageSlot position="TopContent" var="feature">
                <cms:component component="${feature}" element="div" class=""/>
            </cms:pageSlot>
        </div>

        <c:if test="${not empty cartData.rootGroups}">
            <cms:pageSlot position="CenterLeftContentSlot" var="feature">
                <cms:component component="${feature}" element="div" class=""/>
            </cms:pageSlot>
        </c:if>

        <c:if test="${not empty cartData.rootGroups}">
            <cms:pageSlot position="CenterRightContentSlot" var="feature">
                <cms:component component="${feature}" element="div" class=""/>
            </cms:pageSlot>
            <cms:pageSlot position="BottomContentSlot" var="feature">
                <cms:component component="${feature}" element="div" class=""/>
            </cms:pageSlot>
        </c:if>

        <c:if test="${empty cartData.rootGroups}">
            <cms:pageSlot position="EmptyCartMiddleContent" var="feature">
                <cms:component component="${feature}" element="div" class="content__empty vmb-cart-empty"/>
            </cms:pageSlot>
        </c:if>
    </div>

    <event:updateStock/>
</template:page>
