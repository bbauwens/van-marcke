<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="wishListEntry" required="true" type="com.vanmarcke.facades.data.wishlist2.WishListEntryData" %>
<%@ attribute name="itemIndex" required="true" type="java.lang.Integer" %>
<%@ attribute name="showStock" required="false" type="java.lang.Boolean" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/product" %>
<%@ taglib prefix="grid" tagdir="/WEB-INF/tags/responsive/grid" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/shared/format" %>
<%@ taglib prefix="order" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/order" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<spring:htmlEscape defaultHtmlEscape="true"/>

<c:set var="varShowStock" value="${(empty showStock) ? true : showStock}"/>

<c:url value="${wishListEntry.product.url}" var="productUrl"/>
<c:set var="entryStock" value="${fn:escapeXml(wishListEntry.product.stock.stockLevelStatus.code)}"/>
<sec:authorize var="isAnonymousUser" access="hasAnyRole('ROLE_ANONYMOUS')"/>

<input type="hidden" value="${cmsSite.channel == 'DIY'}" id="isDIY"/>

<tr class="vmb-gtm-product-data"
    data-code="${fn:escapeXml(wishListEntry.product.code)}"
    data-name="${fn:escapeXml(wishListEntry.product.name)}"
    data-category="${fn:escapeXml(wishListEntry.product.analyticsData.category)}"
    data-brand="${fn:escapeXml(wishListEntry.product.analyticsData.brand)}"
    data-url="${productUrl}"
    data-list="Savedcart products"
    data-position="${itemIndex + 1}"
    data-price="${wishListEntry.basePrice.value}"
>
    <%-- chevron for multi-d products --%>
    <td class="hidden-xs hidden-sm item__toggle"></td>

    <%-- product image --%>
    <td class="vmb-productitem__image">
        <a class="vmb-gtm-product-item" href="${wishListEntry.product.purchasable ? productUrl : '#'}">
            <product:productPrimaryImage product="${wishListEntry.product}" format="thumbnail"/>
        </a>
    </td>

    <%-- product name, code, promotions --%>
    <td class="vmb-productitem__info">
        <a class="vmb-gtm-product-item" href="${wishListEntry.product.purchasable ? productUrl : '#'}"><span
                class="name">${fn:escapeXml(wishListEntry.product.name)}</span></a>
        <div class="code"><spring:theme
                code="basket.page.entry.sku"/>: ${fn:escapeXml(wishListEntry.product.code)}</div>
    </td>

    <c:set var="wishlistCheckIdx" value="wishlist_check_${wishListEntry.product.code}"/>
    <td class="count vmb-productitem__wishlist">
        <input type="checkbox"
               class="js-select-all-products-wish-list-item"
               data-code="${wishListEntry.product.code}"
               data-wishlist-target-class="${wishlistCheckIdx}"/>
    </td>

    <%-- quantity --%>
    <td class="vmb-productitem__quantity" data-label="<spring:theme code="text.account.order.qty"/>:">
        <form:input cssClass="form-control js-update-entry-quantity-input vmb-product-quantity ${wishlistCheckIdx}"
                    type="number" size="1"
                    path="entries[${itemIndex}].quantity"/>
    </td>

    <c:if test="${varShowStock}">

        <%-- stock store --%>
        <td class="vmb-productitem__stock-warehouse">
            <format:stock type="warehouse" stock="${wishListEntry.product.stock}" code="${wishListEntry.product.code}"/>
        </td>

        <c:if test="${cmsSite.channel != 'DIY'}">
        <%-- stock TEC --%>
            <td class="vmb-productitem__stock-tec">
                <format:stock type="store" stock="${wishListEntry.product.stock}" code="${wishListEntry.product.code}"/>
            </td>
        </c:if>

    </c:if>
        <c:if test="${cmsSite.channel != 'DIY'}">
            <%-- price --%>
            <td class="vmb-productitem__price"
                data-label="<spring:theme code="basket.page.price" arguments="${currentCurrency.symbol}"/>:">
                <order:orderEntryPrice orderEntry="${wishListEntry}"/>
            </td>
        </c:if>

        <c:if test="${!isAnonymousUser and (user.showNetPrice or cmsSite.channel == 'DIY')}">
            <%-- netto --%>
            <td class="vmb-productitem__nettoprice vmb-netto-price-display"
                data-label="<spring:theme code="basket.page.price.netto" arguments="${currentCurrency.symbol}"/>:">
                <order:orderEntryNettoPrice orderEntry="${wishListEntry}"/>
            </td>
        </c:if>

    <%-- total --%>
    <td class="vmb-productitem__total js-item-total" data-label="<spring:theme code="basket.page.total"/>:">
        <format:price priceData="${wishListEntry.totalPrice}" displayFreeForZero="true"/>
    </td>

    <td></td>

    <td class="vmb-productitem__actions">
        <c:set var="deleteToolTip"><spring:theme code="general.delete.button"/></c:set>
        <button class="btn btn-link js-wishlist-delete-item"
                data-code="${wishListEntry.product.code}"
                data-wishlist-target-class="${wishlistCheckIdx}">
            <span class="glyphicon glyphicon-remove" data-toggle="tooltip"
                  data-original-title="${deleteToolTip}"></span>
        </button>
    </td>
</tr>

