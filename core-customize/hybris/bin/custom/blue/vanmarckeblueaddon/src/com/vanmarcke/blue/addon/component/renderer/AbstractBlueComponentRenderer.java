package com.vanmarcke.blue.addon.component.renderer;

import de.hybris.platform.acceleratorcms.component.renderer.CMSComponentRenderer;
import de.hybris.platform.addonsupport.renderer.impl.DefaultAddOnCMSComponentRenderer;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.cms2.model.contents.components.AbstractCMSComponentModel;
import de.hybris.platform.site.BaseSiteService;
import org.springframework.beans.factory.annotation.Required;

import javax.servlet.ServletException;
import javax.servlet.jsp.PageContext;
import java.io.IOException;
import java.util.Map;

import static com.vanmarcke.blue.addon.constants.VanmarckeblueaddonConstants.EXTENSIONNAME;
import static de.hybris.platform.commerceservices.enums.SiteChannel.B2B;
import static de.hybris.platform.commerceservices.enums.SiteChannel.DIY;

public abstract class AbstractBlueComponentRenderer<C extends AbstractCMSComponentModel> extends DefaultAddOnCMSComponentRenderer<C> {

    private static final String COMPONENT = "component";
    private BaseSiteService baseSiteService;

    private CMSComponentRenderer<C> defaultCmsComponentRenderer;

    @Override
    public void renderComponent(final PageContext pageContext, final C component) throws ServletException, IOException {
        final BaseSiteModel currentSite = getBaseSiteService().getCurrentBaseSite();
        if (currentSite != null && (currentSite.getChannel() == B2B || currentSite.getChannel() == DIY)) {
            super.renderComponent(pageContext, component);
        } else {
            getDefaultCmsComponentRenderer().renderComponent(pageContext, component);
        }
    }

    @Override
    protected Map<String, Object> getVariablesToExpose(final PageContext pageContext, final C component) {
        final Map<String, Object> model = super.getVariablesToExpose(pageContext, component);
        model.put(COMPONENT, component);
        return model;
    }

    @Override
    protected String getAddonUiExtensionName(final C component) {
        return EXTENSIONNAME;
    }

    public BaseSiteService getBaseSiteService() {
        return baseSiteService;
    }

    @Required
    public void setBaseSiteService(BaseSiteService baseSiteService) {
        this.baseSiteService = baseSiteService;
    }

    public CMSComponentRenderer<C> getDefaultCmsComponentRenderer() {
        return defaultCmsComponentRenderer;
    }

    @Required
    public void setDefaultCmsComponentRenderer(CMSComponentRenderer<C> defaultCmsComponentRenderer) {
        this.defaultCmsComponentRenderer = defaultCmsComponentRenderer;
    }
}