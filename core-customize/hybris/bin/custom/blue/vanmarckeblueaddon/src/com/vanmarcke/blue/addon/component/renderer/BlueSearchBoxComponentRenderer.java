package com.vanmarcke.blue.addon.component.renderer;

import de.hybris.platform.acceleratorcms.model.components.SearchBoxComponentModel;

/**
 * Custom Search Box Component Renderer
 */
public class BlueSearchBoxComponentRenderer<C extends SearchBoxComponentModel> extends AbstractBlueComponentRenderer<C> {

}