package com.vanmarcke.blue.addon.component.renderer;

import de.hybris.platform.acceleratorcms.model.components.ProductRefinementComponentModel;

/**
 * Custom Product Refinement Component Renderer
 */
public class BlueProductRefinementComponentRenderer<C extends ProductRefinementComponentModel> extends AbstractBlueComponentRenderer<C> {

}