package com.vanmarcke.blue.addon.converter.populator;

import com.vanmarcke.blue.addon.form.BlueAddressForm;
import de.hybris.platform.commercefacades.i18n.I18NFacade;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.RegionData;
import de.hybris.platform.converters.Populator;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * The {@link BlueReverseAddressPopulator} class is used to populate {@link AddressData} instances with the data of the
 * corresponding {@link BlueAddressForm} instance.
 *
 * @author Christiaan Janssen
 * @since 22-09-2020
 */
public class BlueReverseAddressPopulator implements Populator<BlueAddressForm, AddressData> {

    private final I18NFacade i18NFacade;

    /**
     * Creates a new instance of the {@link BlueReverseAddressPopulator} class.
     *
     * @param i18NFacade the I18N facade
     */
    public BlueReverseAddressPopulator(I18NFacade i18NFacade) {
        this.i18NFacade = i18NFacade;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void populate(BlueAddressForm source, AddressData target) {
        target.setId(source.getAddressId());
        target.setTitleCode(source.getTitleCode());
        target.setFirstName(source.getFirstName());
        target.setLastName(source.getLastName());
        target.setLine1(source.getLine1());
        target.setLine2(source.getLine2());
        target.setApartment(source.getApartment());
        target.setTown(source.getTownCity());
        target.setPostalCode(source.getPostcode());
        target.setBillingAddress(false);
        target.setShippingAddress(true);
        target.setPhone(source.getPhone());
        target.setMobile(source.getMobile());
        target.setDefaultAddress(BooleanUtils.toBoolean(source.getDefaultAddress()));
        target.setVisibleInAddressBook(BooleanUtils.toBoolean(source.getSaveInAddressBook()));

        if (StringUtils.isNotEmpty(source.getCountryIso())) {
            final CountryData countryData = i18NFacade.getCountryForIsocode(source.getCountryIso());
            target.setCountry(countryData);
        }

        if (StringUtils.isNotEmpty(source.getRegionIso())) {
            final RegionData regionData = i18NFacade.getRegion(source.getCountryIso(), source.getRegionIso());
            target.setRegion(regionData);
        }
    }
}
