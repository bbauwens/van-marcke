package com.vanmarcke.blue.addon.component.renderer;

import de.hybris.platform.acceleratorcms.model.components.CategoryNavigationComponentModel;

/**
 * Custom Category Navigation Component Renderer
 */
public class BlueCategoryNavigationComponentRenderer<C extends CategoryNavigationComponentModel> extends AbstractBlueComponentRenderer<C> {

}