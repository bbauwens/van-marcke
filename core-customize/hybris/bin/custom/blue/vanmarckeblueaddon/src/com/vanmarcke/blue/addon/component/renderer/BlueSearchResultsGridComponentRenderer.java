package com.vanmarcke.blue.addon.component.renderer;

import de.hybris.platform.acceleratorcms.model.components.SearchResultsGridComponentModel;

/**
 * Custom Search Results Grid Component Renderer
 */
public class BlueSearchResultsGridComponentRenderer<C extends SearchResultsGridComponentModel> extends AbstractBlueComponentRenderer<C> {
}
