package com.vanmarcke.blue.addon.form;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * The {@link BlueAddressForm} class contains the address information.
 *
 * @author Christiaan Janssen
 * @since 22-09-2020
 */
public class BlueAddressForm {

    private String addressId;
    private String titleCode;
    private String firstName;
    private String lastName;
    private String line1;
    private String line2;
    private String apartment;
    private String townCity;
    private String regionIso;
    private String postcode;
    private String countryIso;
    private Boolean saveInAddressBook;
    private Boolean defaultAddress;
    private Boolean shippingAddress;
    private Boolean billingAddress;
    private Boolean editAddress;
    private String phone;
    private String mobile;

    /**
     * Returns the address ID
     *
     * @return the address ID
     */
    public String getAddressId() {
        return addressId;
    }

    /**
     * Sets the given {@code addressId}.
     *
     * @param addressId the address ID
     */
    public void setAddressId(String addressId) {
        this.addressId = addressId;
    }

    /**
     * Returns the title code
     *
     * @return the title code
     */
    @NotNull(message = "{address.title.invalid}")
    @Size(min = 1, max = 255, message = "{address.title.invalid}")
    public String getTitleCode() {
        return titleCode;
    }

    /**
     * Sets the given {@code titleCode}.
     *
     * @param titleCode the title code
     */
    public void setTitleCode(String titleCode) {
        this.titleCode = titleCode;
    }

    /**
     * Returns the first name
     *
     * @return the first name
     */
    @NotNull(message = "{address.firstName.invalid}")
    @Size(min = 1, max = 255, message = "{address.firstName.invalid}")
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the given {@code firstName}.
     *
     * @param firstName the first name
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Returns the last name
     *
     * @return the last name
     */
    @NotNull(message = "{address.lastName.invalid}")
    @Size(min = 1, max = 255, message = "{address.lastName.invalid}")
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the given {@code lastName}.
     *
     * @param lastName the last name
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Returns the street name.
     *
     * @return the street name
     */
    @NotNull(message = "{address.line1.invalid}")
    @Size(min = 1, max = 255, message = "{address.line1.invalid}")
    public String getLine1() {
        return line1;
    }

    /**
     * Sets the given {@code line1}.
     *
     * @param line1 the street name
     */
    public void setLine1(String line1) {
        this.line1 = line1;
    }

    /**
     * Returns the street number
     *
     * @return the street number
     */
    @NotNull(message = "{address.line2.invalid}")
    @Size(min = 1, max = 255, message = "{address.line2.invalid}")
    public String getLine2() {
        return line2;
    }

    /**
     * Sets the give {@code line2}.
     *
     * @param line2 the street number
     */
    public void setLine2(String line2) {
        this.line2 = line2;
    }

    /**
     * Returns the apartment.
     *
     * @return the apartment
     */
    public String getApartment() {
        return apartment;
    }

    /**
     * Sets the given {@code apartment}.
     *
     * @param apartment the apartment
     */
    public void setApartment(String apartment) {
        this.apartment = apartment;
    }

    /**
     * Returns the town.
     *
     * @return the town
     */
    @NotNull(message = "{address.townCity.invalid}")
    @Size(min = 1, max = 255, message = "{address.townCity.invalid}")
    public String getTownCity() {
        return townCity;
    }

    /**
     * Sets the given {@code townCity}.
     *
     * @param townCity the town or city
     */
    public void setTownCity(String townCity) {
        this.townCity = townCity;
    }

    /**
     * Returns the region ISO code
     *
     * @return the region ISO code
     */
    public String getRegionIso() {
        return regionIso;
    }

    /**
     * Sets the given {@code regionIso}.
     *
     * @param regionIso the region ISO code
     */
    public void setRegionIso(String regionIso) {
        this.regionIso = regionIso;
    }

    /**
     * Returns the postal code.
     *
     * @return the postal code
     */
    @NotNull(message = "{address.postcode.invalid}")
    @Size(min = 1, max = 10, message = "{address.postcode.invalid}")
    public String getPostcode() {
        return postcode;
    }

    /**
     * Sets the given {@code postCode}.
     *
     * @param postcode the postal code
     */
    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    /**
     * Returns the country ISO code
     *
     * @return the country ISO code
     */
    @NotNull(message = "{address.country.invalid}")
    @Size(min = 1, max = 255, message = "{address.country.invalid}")
    public String getCountryIso() {
        return countryIso;
    }

    /**
     * Sets the given {@code countryIso}.
     *
     * @param countryIso the country ISO code
     */
    public void setCountryIso(String countryIso) {
        this.countryIso = countryIso;
    }

    /**
     * Returns the save in address book flag.
     *
     * @return the save in address book flag
     */
    public Boolean getSaveInAddressBook() {
        return saveInAddressBook;
    }

    /**
     * Sets the given {@code saveInAddressBook}.
     *
     * @param saveInAddressBook the save in address book flag
     */
    public void setSaveInAddressBook(Boolean saveInAddressBook) {
        this.saveInAddressBook = saveInAddressBook;
    }

    /**
     * Returns the default address flag.
     *
     * @return the default address flag
     */
    public Boolean getDefaultAddress() {
        return defaultAddress;
    }

    /**
     * Sets the given {@code defaultAddress}
     *
     * @param defaultAddress the default address flag
     */
    public void setDefaultAddress(Boolean defaultAddress) {
        this.defaultAddress = defaultAddress;
    }

    /**
     * Returns the shipping address flag.
     *
     * @return the shipping address flag
     */
    public Boolean getShippingAddress() {
        return shippingAddress;
    }

    /**
     * Sets the given {@code shippingAddress}.
     *
     * @param shippingAddress the shipping address flag
     */
    public void setShippingAddress(Boolean shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    /**
     * Returns the billing address flag.
     *
     * @return the billing address flag
     */
    public Boolean getBillingAddress() {
        return billingAddress;
    }

    /**
     * Sets the given {@code billingAddress}.
     *
     * @param billingAddress the shipping address flag
     */
    public void setBillingAddress(Boolean billingAddress) {
        this.billingAddress = billingAddress;
    }

    /**
     * Returns the edit address flag.
     *
     * @return the edit address flag
     */
    public Boolean getEditAddress() {
        return editAddress;
    }

    /**
     * Sets the given {@code editAddress}.
     *
     * @param editAddress the edit address flag
     */
    public void setEditAddress(Boolean editAddress) {
        this.editAddress = editAddress;
    }

    /**
     * Returns the phone number
     *
     * @return the phone number
     */
    @Size(min = 1, max = 10, message = "{address.phone.invalid-size}")
    public String getPhone() {
        return phone;
    }

    /**
     * Sets the givne {@code phone}.
     *
     * @param phone the phone number
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * Returns the mobile phone number.
     *
     * @return the mobile phone number
     */
    @NotNull(message = "{address.mobile.invalid}")
    @Size(min = 1, max = 15, message = "{address.mobile.invalid-size}")
    public String getMobile() {
        return mobile;
    }

    /**
     * Sets the mobile phone number
     *
     * @param mobile the mobile phone number
     */
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
}
