package com.vanmarcke.blue.addon.constants;

/**
 * Global class for all Vanmarckeblueaddon constants. You can add global constants for your extension into this class.
 */
public final class VanmarckeblueaddonConstants extends GeneratedVanmarckeblueaddonConstants {

  public static final String EXTENSIONNAME = "vanmarckeblueaddon";

  private VanmarckeblueaddonConstants() {
    //empty to avoid instantiating this constant class
  }

  // implement here constants used by this extension
}
