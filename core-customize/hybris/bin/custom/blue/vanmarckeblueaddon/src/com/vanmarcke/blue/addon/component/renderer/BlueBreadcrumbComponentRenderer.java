package com.vanmarcke.blue.addon.component.renderer;

import de.hybris.platform.acceleratorcms.model.components.BreadcrumbComponentModel;

/**
 * Blue CMS component renderer for the {@link BreadcrumbComponentModel}
 */
public class BlueBreadcrumbComponentRenderer<C extends BreadcrumbComponentModel> extends AbstractBlueComponentRenderer<C> {

}