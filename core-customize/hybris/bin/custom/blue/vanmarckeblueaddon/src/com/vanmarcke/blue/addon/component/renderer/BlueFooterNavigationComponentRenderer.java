package com.vanmarcke.blue.addon.component.renderer;

import de.hybris.platform.acceleratorcms.model.components.FooterNavigationComponentModel;

/**
 * Custom Footer Navigation Component Renderer
 */
public class BlueFooterNavigationComponentRenderer<C extends FooterNavigationComponentModel> extends AbstractBlueComponentRenderer<C> {

}