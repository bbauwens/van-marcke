# -----------------------------------------------------------------------
# [y] hybris Platform
#
# Copyright (c) 2000-2016 SAP SE or an SAP affiliate company.
# All rights reserved.
#
# This software is the confidential and proprietary information of SAP
# ("Confidential Information"). You shall not disclose such Confidential
# Information and shall use it only in accordance with the terms of the
# license agreement you entered into with SAP.
# -----------------------------------------------------------------------
$currencies = EUR
$defaultCurrency = EUR
$defaultLanguage = en
$deliveryCountries = US

$catalog-id = testCatalog
$catalog-version = Online

$testCategory0-id = testCategory0
$testCategory1-id = testCategory1

$catalogversion = catalogversion(catalog(id), version)[unique=true, default=$catalog-id:$catalog-version];;;;;;;;;;;;;;;;;;;;;;
$supercategories = supercategories(code, catalogversion(catalog(id[default=$catalog-id]), version[default=$catalog-version]));;;;;;;;;;;;;;;;;;;;;;
$prices = europe1prices[translator=de.hybris.platform.europe1.jalo.impex.Europe1PricesTranslator];;;;;;;;;;;;;;;;;;;;;;

INSERT_UPDATE Language; isocode[unique = true]; active
                      ; de                    ; true
                      ; en                    ; true

INSERT_UPDATE Unit; unitType[unique = true]; code[unique = true]; name[lang = de]; name[lang = en]; conversion
                  ; pieces                 ; pieces             ; pieces         ; pieces         ; 1

INSERT_UPDATE Currency; isocode[unique = true]; name[lang = de]; name[lang = en]; active; base  ; conversion; digits; symbol;;;;;;;;;;;;;;
                      ; EUR                   ; Euro           ; Euro           ; true  ; true  ; 1         ; 2     ; E
                      ; USD                   ; US-Dollar      ; US Dollar      ; true  ; false ; 1,38      ; 2     ; $
INSERT_UPDATE Country; isocode[unique = true]; name[lang = de]                ; name[lang = en]          ; active; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ;
                     ; DE                    ; Deutschland                    ; Germany                  ; true  ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ;
                     ; US                    ; Vereinigte Staaten von Amerika ; United States of America ; true  ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ;
                     ;                       ;                                ;                          ;       ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ;
INSERT_UPDATE UserGroup; uid[unique = true]; groups(uid)  ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ;
                       ; cockpitgroup      ;              ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ;
                       ; customergroup     ; cockpitgroup ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ;
                       ;                   ;              ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ;
INSERT_UPDATE Customer; uid[unique = true]; customerID  ; name       ; description; sessionLanguage(isocode); sessionCurrency(isocode); groups(uid)   ; password; ; ; ; ; ; ; ; ; ; ; ; ; ;
                      ; ahertz            ; K2006-C0005 ; Anja Hertz ;            ; en                      ; EUR                     ; customergroup ; 1234    ; ; ; ; ; ; ; ; ; ; ; ; ; ;
                      ;                   ;             ;            ;            ;                         ;                         ;               ;         ; ; ; ; ; ; ; ; ; ; ; ; ; ;
INSERT_UPDATE Address; owner(Customer.uid)[unique = true]; streetname[unique = true]; streetnumber[unique = true]; postalcode[unique = true]; duplicate[unique = true]; town   ; country(isocode); billingAddress; contactAddress; shippingAddress; unloadingAddress; firstname; lastname; email           ; gender(code); middlename; phone1; title(code); url; company     ; fax; department
                     ; ahertz                            ; Zennerstrasse            ; 45                         ; 28277                    ; false                   ; Bremen ; DE              ; false         ; false         ; false          ; true            ; Anja     ; Hertz   ; ahertz@na-du.de ; FEMALE      ;           ;       ;            ;    ; hybris GmbH ;    ;
                     ;                                   ;                          ;                            ;                          ;                         ;        ;                 ;               ;               ;                ;                 ;          ;         ;                 ;             ;           ;       ;            ;    ;             ;    ;
INSERT_UPDATE Customer; uid[unique = true]; defaultPaymentaddress(owner(Customer.uid), streetname, streetnumber, postalcode, duplicate); defaultShipmentAddress(owner(Customer.uid), streetname, streetnumber, postalcode, duplicate); ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ;
                      ; ahertz            ; ahertz:Schiller Allee:8:28277:false                                                        ; ahertz:Zennerstrasse:45:28277:false                                                         ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ;
                      ;                   ;                                                                                            ;                                                                                             ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ;
INSERT_UPDATE Catalog; id[unique = true]; name[lang = de]; name[lang = en]; defaultCatalog; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ;
                     ; $catalog-id      ; $catalog-id    ; $catalog-id    ; true          ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ;
                     ;                  ;                ;                ;               ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ;
INSERT_UPDATE CatalogVersion; catalog(id)[unique = true]; version[unique = true]; active; defaultCurrency(isocode);       ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ;
                            ; $catalog-id               ; $catalog-version      ; true  ; EUR                     ; de,en ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ;
                            ;                           ;                       ;       ;                         ;       ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ;
INSERT_UPDATE Category; code[unique = true]; $catalogversion; name[lang = de]   ; name[lang = en]   ; $supercategories  ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ;
                      ; $testCategory0-id  ;                ; $testCategory0-id ; $testCategory0-id ;                   ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ;
                      ; $testCategory1-id  ;                ; $testCategory1-id ; $testCategory1-id ; $testCategory0-id ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ;
                      ;                    ;                ;                   ;                   ;                   ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ;
INSERT_UPDATE Product; code[unique = true]; $catalogversion; name[lang = de]; name[lang = en]; unit(code); $prices   ; approvalStatus(code); owner(Principal.uid); startLineNumber; $supercategories  ; ; ; ; ; ; ; ; ; ; ; ;
                     ; product_TECX1      ;                ; testDE         ; testEN         ; pieces    ; 57,95 EUR ; approved            ; ahertz              ; 0              ; $testCategory0-id ; ; ; ; ; ; ; ; ; ; ; ;
                     ; product_TECX2      ;                ; testDE         ; testEN         ; pieces    ; 57,95 EUR ; approved            ; ahertz              ; 0              ; $testCategory0-id ; ; ; ; ; ; ; ; ; ; ; ;
                     ;                    ;                ;                ;                ;           ;           ;                     ;                     ;                ;                   ; ; ; ; ; ; ; ; ; ; ; ;
INSERT_UPDATE Cart; user(uid); code[unique = true]; currency(isocode); date[dateformat = dd.MM.yyyy HH:mm:ss]; net  ; calculated; yardReference           ; deliveryDate[dateformat = dd.MM.yyyy HH:mm:ss]; deliveryPointOfService; ; ; ; ; ; ; ; ; ; ; ; ;
                  ; ahertz   ; cart_TECX          ; EUR              ; 27.04.2010 00:00:00                   ; true ; false     ; yard_ref_cart_TECX      ; 01.01.2021 00:00:00                           ;
                  ; ahertz   ; cart_TECX_TECX     ; EUR              ; 27.04.2010 00:00:00                   ; true ; false     ; yard_ref_cart_TECX_TECX ;                                               ; testPosOpenOnSaturday ; ; ; ; ; ; ; ; ; ; ; ; ;
                  ;          ;                    ;                  ;                                       ;      ;           ;                         ;                                               ;                       ; ; ; ; ; ; ; ; ; ; ; ; ;
INSERT CartEntry; order(Cart.code); creationTime[dateformat = dd.MM.yyyy HH:mm:ss]; product(code, catalogVersion(catalog(id), version)); quantity; unit(code); entryNumber; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ;
                ; cart_TECX       ; 27.04.2010 00:00:00                           ; product_TECX1:$catalog-id:$catalog-version         ; 2       ; pieces    ; 1          ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ;
                ; cart_TECX_TECX  ; 27.04.2010 00:00:00                           ; product_TECX1:$catalog-id:$catalog-version         ; 2       ; pieces    ; 1          ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ;
                ; cart_TECX_TECX  ; 27.04.2010 00:00:00                           ; product_TECX2:$catalog-id:$catalog-version         ; 2       ; pieces    ; 1          ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ;
                ;                 ;                                               ;                                                    ;         ;           ;            ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ;


INSERT_UPDATE OpeningSchedule; code[unique = true];
                             ; testScheduleOpenOnSaturday
                             ; testScheduleClosedOnSaturday

INSERT_UPDATE WeekdayOpeningDay; openingSchedule(code)[unique = true]; openingTime[translator = com.vanmarcke.common.translators.VMKDateTranslator, dateFormat = HH:mm]; closingTime[translator = com.vanmarcke.common.translators.VMKDateTranslator, dateFormat = HH:mm]; dayOfWeek(code)[unique = true];
                               ; testScheduleOpenOnSaturday          ; 07:30                                                                                           ; 17:30                                                                                           ; SATURDAY

INSERT_UPDATE PointOfService; name[unique = true]     ; openingSchedule(name)
                            ; testPosOpenOnSaturday   ; testScheduleOpenOnSaturday
                            ; testPosClosedOnSaturday ; testScheduleClosedOnSaturday

INSERT_UPDATE PromotionGroup; Identifier[unique = true]; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ;
                            ; default                  ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ;
                            ;                          ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ;
                            ;                          ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ;
                            ;                          ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ;
INSERT_UPDATE Vendor; code[unique = true]; name[lang = en]; name[lang = de]; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ;
                    ; electro            ; Electro        ; Electro        ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ;
                    ;                    ;                ;                ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ;
INSERT_UPDATE BaseStore; uid[unique = true]; catalogs(id); currencies(isocode); net   ; taxGroup(code); storelocatorDistanceUnit(code); defaultCurrency(isocode); languages(isocode); defaultLanguage(isocode); deliveryCountries(isocode); solrFacetSearchConfiguration(name); submitOrderProcessCode
                       ; testStore         ; $catalog-id ; $currencies        ; false ;               ; km                            ; $defaultCurrency        ; $defaultLanguage  ; $defaultLanguage        ; $deliveryCountries        ;                                   ;
                       ;                   ;             ;                    ;       ;               ;                               ;                         ;                   ;                         ;                           ;                                   ; ; ; ; ; ; ; ; ; ; ;
INSERT_UPDATE BaseSite; uid[unique = true]; stores(uid); defaultPromotionGroup(Identifier)
                      ; testSite          ; testStore  ; default ;
                      ;                   ;            ;         ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ;

INSERT_UPDATE Zone; code[unique = true]; countries(isocode)
                  ; usa                ; US

# Zone Delivery Modes
INSERT_UPDATE ZoneDeliveryMode; code[unique = true]; net; active[default = true]
                              ; premium-gross      ; false


# USA
INSERT_UPDATE ZoneDeliveryModeValue; deliveryMode(code)[unique = true]; zone(code)[default = 'usa'][unique = true]; currency(isocode)[unique = true]; value ; minimum[unique = true]
                                   ; premium-gross                    ;                                           ; $defaultCurrency                ; 16,99 ; 0,00

INSERT_UPDATE BaseStore2DeliveryModeRel; source(uid)[unique = true]; target(code)[unique = true]
                                       ; testStore                 ; premium-gross