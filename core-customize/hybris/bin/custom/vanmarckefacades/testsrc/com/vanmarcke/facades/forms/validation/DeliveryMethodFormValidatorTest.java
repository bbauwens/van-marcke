package com.vanmarcke.facades.forms.validation;

import com.vanmarcke.core.enums.ReferenceConditionalType;
import com.vanmarcke.core.exception.NoSessionCartException;
import com.vanmarcke.services.b2b.VMKB2BUnitService;
import com.vanmarcke.services.delivery.VMKDeliveryMethodService;
import com.vanmarcke.services.delivery.VMKPickupInfoService;
import com.vanmarcke.services.delivery.VMKShippingInfoService;
import com.vanmarcke.services.forms.delivery.DeliveryMethodForm;
import com.vanmarcke.services.model.builder.CartModelMockBuilder;
import com.vanmarcke.services.model.builder.PointOfServiceModelMockBuilder;
import com.vanmarcke.services.storelocator.pos.VMKPointOfServiceService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.cmsfacades.util.builder.BaseStoreModelBuilder;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import org.apache.commons.lang3.RandomStringUtils;
import org.assertj.core.api.Assertions;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.validation.Errors;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Optional;

import static com.vanmarcke.core.constants.VanmarckeCoreConstants.DeliveryMethods.PICKUP;
import static com.vanmarcke.core.constants.VanmarckeCoreConstants.DeliveryMethods.SHIPPING;
import static org.mockito.Mockito.*;

/**
 * The {@link DeliveryMethodFormValidatorTest} class contains the unit tests for the
 * {@link DeliveryMethodFormValidator} class.
 *
 * @author Niels Raemaekers, Tom van den Berg, Christiaan Janssen
 * @since 18-08-2020
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DeliveryMethodFormValidatorTest {

    private static final String DELIVERY_OPTION = RandomStringUtils.random(10);
    private static final String DELIVERY_METHOD = RandomStringUtils.random(10);
    private static final String TEC_UID = RandomStringUtils.random(10);
    private static final SimpleDateFormat FORMATTER = new SimpleDateFormat("yyyy-MM-dd");
    private static final Date FUTURE_DATE = DateTime.now().plusDays(3).toDate();
    private static final Date TODAY = DateTime.now().toDate();
    private static final LocalDate TODAY_LOCALDATE = LocalDate.now();

    private CartModel cart;

    @Mock
    private VMKPickupInfoService pickupInfoService;
    @Mock
    private VMKShippingInfoService shippingInfoService;
    @Mock
    private VMKDeliveryMethodService deliveryMethodService;
    @Mock
    private VMKPointOfServiceService pointOfServiceService;
    @Mock
    private CartService cartService;
    @Mock
    private VMKB2BUnitService b2BUnitService;
    @Mock
    private B2BUnitModel currentUnit;
    @Mock
    private BaseStoreService baseStoreService;
    @Mock
    private BaseSiteService baseSiteService;
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Spy
    @InjectMocks
    private DeliveryMethodFormValidator validator;

    @Before
    public void setUp() throws Exception {
        cart = CartModelMockBuilder.aCart().build();
        when(cartService.getSessionCart()).thenReturn(cart);
        when(b2BUnitService.getB2BUnitModelForCurrentUser()).thenReturn(currentUnit);
        doReturn(TODAY_LOCALDATE).when(validator).getCurrentDay();
    }

    @Test
    public void testGetCart_noSessionCartException() {
        expectedException.expect(NoSessionCartException.class);
        expectedException.expectMessage("No session cart was found.");

        when(cartService.getSessionCart()).thenReturn(null);

        validator.getCart();
    }

    @Test
    public void testValidateWithoutDeliveryOption() {
        DeliveryMethodForm deliveryMethodForm = new DeliveryMethodForm();
        deliveryMethodForm.setDeliveryMethod(SHIPPING);
        deliveryMethodForm.setSelectedShippingDate(FORMATTER.format(new Date()));

        HashMap<String, Boolean> deliveryMethods = new HashMap<>();
        deliveryMethods.put(SHIPPING, true);

        when(deliveryMethodService.getSupportedDeliveryMethods(cart)).thenReturn(deliveryMethods);
        when(shippingInfoService.getFirstPossibleShippingDate(cart)).thenReturn(Optional.of(FUTURE_DATE));

        BaseSiteModel baseSiteModel = mock(BaseSiteModel.class);
        when(baseSiteService.getCurrentBaseSite()).thenReturn(baseSiteModel);
        when(baseSiteModel.getChannel()).thenReturn(SiteChannel.B2B);

        Errors errors = mock(Errors.class);

        validator.validate(deliveryMethodForm, errors);

        verify(deliveryMethodService).getSupportedDeliveryMethods(cart);
        verify(shippingInfoService).getFirstPossibleShippingDate(cart);
        verify(errors).rejectValue("deliveryOption", "delivery.form.option.none.selected");
    }

    @Test
    public void testValidateWithInvalidDeliveryMethod() {
        DeliveryMethodForm deliveryMethodForm = new DeliveryMethodForm();
        deliveryMethodForm.setDeliveryOption(DELIVERY_OPTION);
        deliveryMethodForm.setDeliveryMethod(DELIVERY_METHOD);
        deliveryMethodForm.setSelectedShippingDate(FORMATTER.format(new Date()));

        HashMap<String, Boolean> deliveryMethods = new HashMap<>();
        deliveryMethods.put(DELIVERY_METHOD, false);

        when(deliveryMethodService.getSupportedDeliveryMethods(cart)).thenReturn(deliveryMethods);
        when(shippingInfoService.getFirstPossibleShippingDate(cart)).thenReturn(Optional.of(new Date()));

        BaseSiteModel baseSiteModel = mock(BaseSiteModel.class);
        when(baseSiteService.getCurrentBaseSite()).thenReturn(baseSiteModel);
        when(baseSiteModel.getChannel()).thenReturn(SiteChannel.B2B);

        Errors errors = mock(Errors.class);

        validator.validate(deliveryMethodForm, errors);

        verify(deliveryMethodService).getSupportedDeliveryMethods(cart);
        verify(errors).rejectValue("deliveryMethod", "delivery.form.method.invalid");
    }

    @Test
    public void testValidateWithValidPickupDate() {
        DeliveryMethodForm deliveryMethodForm = new DeliveryMethodForm();
        deliveryMethodForm.setDeliveryOption(DELIVERY_OPTION);
        deliveryMethodForm.setDeliveryMethod(PICKUP);
        deliveryMethodForm.setSelectedPickupDate(FORMATTER.format(FUTURE_DATE));
        deliveryMethodForm.setTecUid(TEC_UID);

        HashMap<String, Boolean> deliveryMethods = new HashMap<>();
        deliveryMethods.put(PICKUP, true);

        when(deliveryMethodService.getSupportedDeliveryMethods(cart)).thenReturn(deliveryMethods);
        when(pickupInfoService.getFirstPossiblePickupDate(cart)).thenReturn(Optional.of(FUTURE_DATE));

        PointOfServiceModel pos = PointOfServiceModelMockBuilder
                .aPointOfService()
                .build();

        BaseStoreModel baseStoreModel = BaseStoreModelBuilder
                .aModel()
                .build();
        baseStoreModel.setPointsOfService(Collections.singletonList(pos));

        when(pointOfServiceService.getPointOfServiceForName(TEC_UID)).thenReturn(pos);
        when(baseStoreService.getCurrentBaseStore()).thenReturn(baseStoreModel);

        BaseSiteModel baseSiteModel = mock(BaseSiteModel.class);
        when(baseSiteService.getCurrentBaseSite()).thenReturn(baseSiteModel);
        when(baseSiteModel.getChannel()).thenReturn(SiteChannel.B2B);

        Errors errors = mock(Errors.class);

        validator.validate(deliveryMethodForm, errors);

        verify(deliveryMethodService).getSupportedDeliveryMethods(cart);
        verify(pickupInfoService).getFirstPossiblePickupDate(cart);
        verify(pointOfServiceService).getPointOfServiceForName(TEC_UID);
        verify(errors, never()).rejectValue(anyString(), anyString());
    }

    @Test
    public void testValidateWithNullPos() {
        DeliveryMethodForm deliveryMethodForm = new DeliveryMethodForm();
        deliveryMethodForm.setDeliveryOption(DELIVERY_OPTION);
        deliveryMethodForm.setDeliveryMethod(PICKUP);
        deliveryMethodForm.setSelectedPickupDate(FORMATTER.format(FUTURE_DATE));
        deliveryMethodForm.setTecUid(TEC_UID);

        HashMap<String, Boolean> deliveryMethods = new HashMap<>();
        deliveryMethods.put(PICKUP, true);

        when(deliveryMethodService.getSupportedDeliveryMethods(cart)).thenReturn(deliveryMethods);
        when(pickupInfoService.getFirstPossiblePickupDate(cart)).thenReturn(Optional.of(FUTURE_DATE));

        PointOfServiceModel pos = PointOfServiceModelMockBuilder
                .aPointOfService()
                .build();

        when(pointOfServiceService.getPointOfServiceForName(TEC_UID)).thenReturn(null);

        BaseSiteModel baseSiteModel = mock(BaseSiteModel.class);
        when(baseSiteService.getCurrentBaseSite()).thenReturn(baseSiteModel);
        when(baseSiteModel.getChannel()).thenReturn(SiteChannel.B2B);

        Errors errors = mock(Errors.class);

        validator.validate(deliveryMethodForm, errors);

        verify(deliveryMethodService).getSupportedDeliveryMethods(cart);
        verify(pickupInfoService).getFirstPossiblePickupDate(cart);
        verify(pointOfServiceService).getPointOfServiceForName(TEC_UID);
        verify(errors).rejectValue(null, "delivery.form.date.invalid");
        verifyNoMoreInteractions(errors);
    }

    @Test
    public void testValidateWithInvalidPos() {
        DeliveryMethodForm deliveryMethodForm = new DeliveryMethodForm();
        deliveryMethodForm.setDeliveryOption(DELIVERY_OPTION);
        deliveryMethodForm.setDeliveryMethod(PICKUP);
        deliveryMethodForm.setSelectedPickupDate(FORMATTER.format(FUTURE_DATE));
        deliveryMethodForm.setTecUid(TEC_UID);

        HashMap<String, Boolean> deliveryMethods = new HashMap<>();
        deliveryMethods.put(PICKUP, true);

        when(deliveryMethodService.getSupportedDeliveryMethods(cart)).thenReturn(deliveryMethods);
        when(pickupInfoService.getFirstPossiblePickupDate(cart)).thenReturn(Optional.of(FUTURE_DATE));

        PointOfServiceModel pos1 = PointOfServiceModelMockBuilder
                .aPointOfService()
                .build();

        PointOfServiceModel pos2 = PointOfServiceModelMockBuilder
                .aPointOfService()
                .build();

        BaseStoreModel baseStoreModel = BaseStoreModelBuilder
                .aModel()
                .build();
        baseStoreModel.setPointsOfService(Collections.singletonList(pos1));

        when(pointOfServiceService.getPointOfServiceForName(TEC_UID)).thenReturn(pos2);
        when(baseStoreService.getCurrentBaseStore()).thenReturn(baseStoreModel);

        BaseSiteModel baseSiteModel = mock(BaseSiteModel.class);
        when(baseSiteService.getCurrentBaseSite()).thenReturn(baseSiteModel);
        when(baseSiteModel.getChannel()).thenReturn(SiteChannel.B2B);

        Errors errors = mock(Errors.class);

        validator.validate(deliveryMethodForm, errors);

        verify(deliveryMethodService).getSupportedDeliveryMethods(cart);
        verify(pickupInfoService).getFirstPossiblePickupDate(cart);
        verify(pointOfServiceService).getPointOfServiceForName(TEC_UID);
        verify(errors).rejectValue(null, "delivery.form.date.invalid");
        verifyNoMoreInteractions(errors);
    }

    @Test
    public void testValidateWithInvalidPickupDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -1);

        DeliveryMethodForm deliveryMethodForm = new DeliveryMethodForm();
        deliveryMethodForm.setDeliveryOption(DELIVERY_OPTION);
        deliveryMethodForm.setDeliveryMethod(PICKUP);
        deliveryMethodForm.setSelectedPickupDate(FORMATTER.format(FUTURE_DATE));

        HashMap<String, Boolean> deliveryMethods = new HashMap<>();
        deliveryMethods.put(PICKUP, true);

        when(deliveryMethodService.getSupportedDeliveryMethods(cart)).thenReturn(deliveryMethods);
        when(pickupInfoService.getFirstPossiblePickupDate(cart)).thenReturn(Optional.of(new Date()));

        BaseSiteModel baseSiteModel = mock(BaseSiteModel.class);
        when(baseSiteService.getCurrentBaseSite()).thenReturn(baseSiteModel);
        when(baseSiteModel.getChannel()).thenReturn(SiteChannel.B2B);

        Errors errors = mock(Errors.class);

        validator.validate(deliveryMethodForm, errors);

        verify(deliveryMethodService).getSupportedDeliveryMethods(cart);
        verify(pickupInfoService).getFirstPossiblePickupDate(cart);
        verify(errors).rejectValue(null, "delivery.form.date.invalid");
        verifyNoMoreInteractions(errors);
    }

    @Test
    public void testValidateWithValidTECCollectDate() {
        DeliveryMethodForm deliveryMethodForm = new DeliveryMethodForm();
        deliveryMethodForm.setDeliveryOption(DELIVERY_OPTION);
        deliveryMethodForm.setDeliveryMethod(PICKUP);
        deliveryMethodForm.setSelectedPickupDate(FORMATTER.format(FUTURE_DATE));
        deliveryMethodForm.setTecUid(TEC_UID);

        HashMap<String, Boolean> deliveryMethods = new HashMap<>();
        deliveryMethods.put(PICKUP, true);

        when(deliveryMethodService.getSupportedDeliveryMethods(cart)).thenReturn(deliveryMethods);
        when(pickupInfoService.getFirstPossiblePickupDate(cart)).thenReturn(null);
        when(pickupInfoService.isTecCollectPossible(cart)).thenReturn(true);

        PointOfServiceModel pos = PointOfServiceModelMockBuilder
                .aPointOfService()
                .build();

        BaseStoreModel baseStoreModel = BaseStoreModelBuilder
                .aModel()
                .build();
        baseStoreModel.setPointsOfService(Collections.singletonList(pos));

        when(pointOfServiceService.getPointOfServiceForName(TEC_UID)).thenReturn(pos);
        when(baseStoreService.getCurrentBaseStore()).thenReturn(baseStoreModel);

        BaseSiteModel baseSiteModel = mock(BaseSiteModel.class);
        when(baseSiteService.getCurrentBaseSite()).thenReturn(baseSiteModel);
        when(baseSiteModel.getChannel()).thenReturn(SiteChannel.B2B);

        Errors errors = mock(Errors.class);

        validator.validate(deliveryMethodForm, errors);

        verify(deliveryMethodService).getSupportedDeliveryMethods(cart);
        verify(pickupInfoService).getFirstPossiblePickupDate(cart);
        verify(pointOfServiceService).getPointOfServiceForName(TEC_UID);
        verify(pickupInfoService).isTecCollectPossible(cart);
        verify(errors, never()).rejectValue(anyString(), anyString());
    }

    @Test
    public void testValidateWithValidShippingDate() {
        DeliveryMethodForm deliveryMethodForm = new DeliveryMethodForm();
        deliveryMethodForm.setDeliveryOption(DELIVERY_OPTION);
        deliveryMethodForm.setDeliveryMethod(SHIPPING);
        deliveryMethodForm.setSelectedShippingDate(FORMATTER.format(FUTURE_DATE));

        HashMap<String, Boolean> deliveryMethods = new HashMap<>();
        deliveryMethods.put(SHIPPING, true);

        when(deliveryMethodService.getSupportedDeliveryMethods(cart)).thenReturn(deliveryMethods);
        when(shippingInfoService.getFirstPossibleShippingDate(cart)).thenReturn(Optional.of(FUTURE_DATE));

        PointOfServiceModel pos = PointOfServiceModelMockBuilder
                .aPointOfService()
                .build();

        BaseStoreModel baseStoreModel = BaseStoreModelBuilder
                .aModel()
                .build();
        baseStoreModel.setPointsOfService(Collections.singletonList(pos));

        when(pointOfServiceService.getCurrentPointOfService()).thenReturn(pos);
        when(baseStoreService.getCurrentBaseStore()).thenReturn(baseStoreModel);

        BaseSiteModel baseSiteModel = mock(BaseSiteModel.class);
        when(baseSiteService.getCurrentBaseSite()).thenReturn(baseSiteModel);
        when(baseSiteModel.getChannel()).thenReturn(SiteChannel.B2B);

        Errors errors = mock(Errors.class);

        validator.validate(deliveryMethodForm, errors);

        verify(deliveryMethodService).getSupportedDeliveryMethods(cart);
        verify(shippingInfoService).getFirstPossibleShippingDate(cart);
        verify(pointOfServiceService).getCurrentPointOfService();
        verify(errors, never()).rejectValue(anyString(), anyString());
    }

    @Test
    public void testValidateWithInvalidShippingDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -1);

        DeliveryMethodForm deliveryMethodForm = new DeliveryMethodForm();
        deliveryMethodForm.setDeliveryOption(DELIVERY_OPTION);
        deliveryMethodForm.setDeliveryMethod(SHIPPING);
        deliveryMethodForm.setSelectedShippingDate(FORMATTER.format(FUTURE_DATE));

        HashMap<String, Boolean> deliveryMethods = new HashMap<>();
        deliveryMethods.put(SHIPPING, true);

        when(deliveryMethodService.getSupportedDeliveryMethods(cart)).thenReturn(deliveryMethods);
        when(shippingInfoService.getFirstPossibleShippingDate(cart)).thenReturn(Optional.of(new Date()));

        BaseSiteModel baseSiteModel = mock(BaseSiteModel.class);
        when(baseSiteService.getCurrentBaseSite()).thenReturn(baseSiteModel);
        when(baseSiteModel.getChannel()).thenReturn(SiteChannel.B2B);

        Errors errors = mock(Errors.class);

        validator.validate(deliveryMethodForm, errors);

        verify(deliveryMethodService).getSupportedDeliveryMethods(cart);
        verify(shippingInfoService).getFirstPossibleShippingDate(cart);
        verify(errors).rejectValue(null, "delivery.form.date.invalid");
        verifyNoMoreInteractions(errors);
    }

    @Test
    public void testValidateWithInvalidDeliveryComment_tooLong() {
        DeliveryMethodForm deliveryMethodForm = new DeliveryMethodForm();
        deliveryMethodForm.setDeliveryComment(RandomStringUtils.random(256));
        deliveryMethodForm.setDeliveryOption(DELIVERY_OPTION);
        deliveryMethodForm.setDeliveryMethod(SHIPPING);
        deliveryMethodForm.setSelectedShippingDate(FORMATTER.format(FUTURE_DATE));

        HashMap<String, Boolean> deliveryMethods = new HashMap<>();
        deliveryMethods.put(SHIPPING, true);

        when(deliveryMethodService.getSupportedDeliveryMethods(cart)).thenReturn(deliveryMethods);
        when(shippingInfoService.getFirstPossibleShippingDate(cart)).thenReturn(Optional.of(FUTURE_DATE));

        PointOfServiceModel pos = PointOfServiceModelMockBuilder
                .aPointOfService()
                .build();

        BaseStoreModel baseStoreModel = BaseStoreModelBuilder
                .aModel()
                .build();
        baseStoreModel.setPointsOfService(Collections.singletonList(pos));

        when(pointOfServiceService.getCurrentPointOfService()).thenReturn(pos);
        when(baseStoreService.getCurrentBaseStore()).thenReturn(baseStoreModel);

        BaseSiteModel baseSiteModel = mock(BaseSiteModel.class);
        when(baseSiteService.getCurrentBaseSite()).thenReturn(baseSiteModel);
        when(baseSiteModel.getChannel()).thenReturn(SiteChannel.B2B);

        Errors errors = mock(Errors.class);

        validator.validate(deliveryMethodForm, errors);

        verify(errors).rejectValue("deliveryComment", "cart.validation-error.delivery-comment-length");
    }

    @Test
    public void testValidateWithValidDeliveryComment_success() {
        DeliveryMethodForm deliveryMethodForm = new DeliveryMethodForm();
        deliveryMethodForm.setDeliveryComment(RandomStringUtils.random(10));
        deliveryMethodForm.setDeliveryOption(DELIVERY_OPTION);
        deliveryMethodForm.setDeliveryMethod(SHIPPING);
        deliveryMethodForm.setSelectedShippingDate(FORMATTER.format(FUTURE_DATE));

        HashMap<String, Boolean> deliveryMethods = new HashMap<>();
        deliveryMethods.put(SHIPPING, true);

        when(deliveryMethodService.getSupportedDeliveryMethods(cart)).thenReturn(deliveryMethods);
        when(shippingInfoService.getFirstPossibleShippingDate(cart)).thenReturn(Optional.of(FUTURE_DATE));

        PointOfServiceModel pos = PointOfServiceModelMockBuilder
                .aPointOfService()
                .build();

        BaseStoreModel baseStoreModel = BaseStoreModelBuilder
                .aModel()
                .build();
        baseStoreModel.setPointsOfService(Collections.singletonList(pos));

        BaseSiteModel baseSiteModel = mock(BaseSiteModel.class);
        when(baseSiteService.getCurrentBaseSite()).thenReturn(baseSiteModel);
        when(baseSiteModel.getChannel()).thenReturn(SiteChannel.B2B);

        when(pointOfServiceService.getCurrentPointOfService()).thenReturn(pos);
        when(baseStoreService.getCurrentBaseStore()).thenReturn(baseStoreModel);

        Errors errors = mock(Errors.class);

        validator.validate(deliveryMethodForm, errors);

        verify(errors, never()).rejectValue("deliveryComment", "cart.validation-error.delivery-comment-length");
    }

    @Test
    public void testIsInvalidDateIsDateEqualOrLargerThanPossibleDateWithFirstPossibleNull() {
        boolean actualResult = validator.isInvalidDate(
                FORMATTER.format(FUTURE_DATE), null);

        Assertions
                .assertThat(actualResult)
                .isTrue();

        verify(pickupInfoService, never()).isTecCollectPossible(cart);

    }

    @Test
    public void testIsInvalidDateIsDateEqualOrLargerThanPossibleDateWithValidDate() {
        boolean actualResult = validator.isInvalidDate(
                FORMATTER.format(FUTURE_DATE), TODAY);

        Assertions
                .assertThat(actualResult)
                .isFalse();
    }

    @Test
    public void testIsInvalidDateIsDateEqualOrLargerThanPossibleDateWithInvalidDate() {
        boolean result = validator.isInvalidDate(
                FORMATTER.format(TODAY), FUTURE_DATE);

        Assertions
                .assertThat(result)
                .isTrue();
    }

    @Test
    public void testIsInvalidDateIsDateEqualOrLargerThanPossibleDateWithTecCollectPossible() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -1);

        String deliveryDate = FORMATTER.format(new Date());
        when(pickupInfoService.isTecCollectPossible(cart)).thenReturn(true);


        boolean actualResult = validator.isInvalidDate(deliveryDate, new Date());

        Assertions
                .assertThat(actualResult)
                .isFalse();
        verify(pickupInfoService).isTecCollectPossible(cart);

    }

    @Test
    public void testIsInvalidDateIsDateEqualOrLargerThanPossibleDateWithTecCollectNotPossible() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -1);

        String deliveryDate = FORMATTER.format(new Date());
        when(pickupInfoService.isTecCollectPossible(cart)).thenReturn(false);

        boolean actualResult = validator.isInvalidDate(deliveryDate, new Date());

        Assertions
                .assertThat(actualResult)
                .isFalse();

        verify(pickupInfoService).isTecCollectPossible(cart);
    }

    @Test
    public void testSupportsWithValidClass() {
        boolean actualResult = validator.supports(DeliveryMethodForm.class);

        Assertions
                .assertThat(actualResult)
                .isTrue();
    }

    @Test
    public void testSupportsWithInvalidClass() {
        boolean actualResult = validator.supports(String.class);

        Assertions
                .assertThat(actualResult)
                .isFalse();
    }

    @Test
    public void testValidateMandatorySeparateInvoiceReference_ok() {
        DeliveryMethodForm deliveryMethodForm = new DeliveryMethodForm();
        deliveryMethodForm.setDeliveryComment(RandomStringUtils.random(10));
        deliveryMethodForm.setDeliveryOption(DELIVERY_OPTION);
        deliveryMethodForm.setDeliveryMethod(SHIPPING);
        deliveryMethodForm.setSelectedShippingDate(FORMATTER.format(FUTURE_DATE));
        deliveryMethodForm.setRequestSeparateInvoice(true);
        deliveryMethodForm.setPurchaseOrderNumber("AZERTY");

        HashMap<String, Boolean> deliveryMethods = new HashMap<>();
        deliveryMethods.put(SHIPPING, true);

        when(deliveryMethodService.getSupportedDeliveryMethods(cart)).thenReturn(deliveryMethods);
        when(shippingInfoService.getFirstPossibleShippingDate(cart)).thenReturn(Optional.of(FUTURE_DATE));
        when(b2BUnitService.mandatorySeparateInvoiceReference()).thenReturn(true);
        when(currentUnit.getRef1()).thenReturn(ReferenceConditionalType.MANDATORY);

        PointOfServiceModel pos = PointOfServiceModelMockBuilder
                .aPointOfService()
                .build();

        BaseStoreModel baseStoreModel = BaseStoreModelBuilder
                .aModel()
                .build();
        baseStoreModel.setPointsOfService(Collections.singletonList(pos));

        when(pointOfServiceService.getCurrentPointOfService()).thenReturn(pos);
        when(baseStoreService.getCurrentBaseStore()).thenReturn(baseStoreModel);

        BaseSiteModel baseSiteModel = mock(BaseSiteModel.class);
        when(baseSiteService.getCurrentBaseSite()).thenReturn(baseSiteModel);
        when(baseSiteModel.getChannel()).thenReturn(SiteChannel.B2B);

        Errors errors = mock(Errors.class);

        validator.validate(deliveryMethodForm, errors);

        verify(b2BUnitService).mandatorySeparateInvoiceReference();
        verify(errors, never()).rejectValue("mandatorySeparateInvoiceReference", "cart.validation-error.mandatory-separate-invoice-reference");
    }

    @Test
    public void testValidateMandatorySeparateInvoiceReference_error() {
        DeliveryMethodForm deliveryMethodForm = new DeliveryMethodForm();
        deliveryMethodForm.setDeliveryComment(RandomStringUtils.random(10));
        deliveryMethodForm.setDeliveryOption(DELIVERY_OPTION);
        deliveryMethodForm.setDeliveryMethod(SHIPPING);
        deliveryMethodForm.setSelectedShippingDate(FORMATTER.format(FUTURE_DATE));
        deliveryMethodForm.setRequestSeparateInvoice(false);

        HashMap<String, Boolean> deliveryMethods = new HashMap<>();
        deliveryMethods.put(SHIPPING, true);

        when(deliveryMethodService.getSupportedDeliveryMethods(cart)).thenReturn(deliveryMethods);
        when(shippingInfoService.getFirstPossibleShippingDate(cart)).thenReturn(Optional.of(FUTURE_DATE));
        when(b2BUnitService.mandatorySeparateInvoiceReference()).thenReturn(true);
        when(currentUnit.getRef1()).thenReturn(ReferenceConditionalType.MANDATORY);

        PointOfServiceModel pos = PointOfServiceModelMockBuilder
                .aPointOfService()
                .build();

        BaseStoreModel baseStoreModel = BaseStoreModelBuilder
                .aModel()
                .build();
        baseStoreModel.setPointsOfService(Collections.singletonList(pos));

        when(pointOfServiceService.getCurrentPointOfService()).thenReturn(pos);
        when(baseStoreService.getCurrentBaseStore()).thenReturn(baseStoreModel);

        BaseSiteModel baseSiteModel = mock(BaseSiteModel.class);
        when(baseSiteService.getCurrentBaseSite()).thenReturn(baseSiteModel);
        when(baseSiteModel.getChannel()).thenReturn(SiteChannel.B2B);

        Errors errors = mock(Errors.class);

        validator.validate(deliveryMethodForm, errors);

        verify(b2BUnitService).mandatorySeparateInvoiceReference();
        verify(errors).rejectValue("mandatorySeparateInvoiceReference", "cart.validation-error.mandatory-separate-invoice-reference");
    }

    @Test
    public void testValidateMandatorySeparateInvoiceReference_errorLength() {
        DeliveryMethodForm deliveryMethodForm = new DeliveryMethodForm();
        deliveryMethodForm.setDeliveryComment(RandomStringUtils.random(10));
        deliveryMethodForm.setDeliveryOption(DELIVERY_OPTION);
        deliveryMethodForm.setDeliveryMethod(SHIPPING);
        deliveryMethodForm.setSelectedShippingDate(FORMATTER.format(FUTURE_DATE));
        deliveryMethodForm.setRequestSeparateInvoice(false);
        deliveryMethodForm.setPurchaseOrderNumber("WAY TOO LONG");

        HashMap<String, Boolean> deliveryMethods = new HashMap<>();
        deliveryMethods.put(SHIPPING, true);

        when(deliveryMethodService.getSupportedDeliveryMethods(cart)).thenReturn(deliveryMethods);
        when(shippingInfoService.getFirstPossibleShippingDate(cart)).thenReturn(Optional.of(FUTURE_DATE));
        when(b2BUnitService.mandatorySeparateInvoiceReference()).thenReturn(true);
        when(currentUnit.getRef1()).thenReturn(ReferenceConditionalType.MANDATORY);

        PointOfServiceModel pos = PointOfServiceModelMockBuilder
                .aPointOfService()
                .build();

        BaseStoreModel baseStoreModel = BaseStoreModelBuilder
                .aModel()
                .build();
        baseStoreModel.setPointsOfService(Collections.singletonList(pos));

        when(pointOfServiceService.getCurrentPointOfService()).thenReturn(pos);
        when(baseStoreService.getCurrentBaseStore()).thenReturn(baseStoreModel);

        BaseSiteModel baseSiteModel = mock(BaseSiteModel.class);
        when(baseSiteService.getCurrentBaseSite()).thenReturn(baseSiteModel);
        when(baseSiteModel.getChannel()).thenReturn(SiteChannel.B2B);

        Errors errors = mock(Errors.class);

        validator.validate(deliveryMethodForm, errors);

        verify(b2BUnitService).mandatorySeparateInvoiceReference();
        verify(errors).rejectValue("mandatorySeparateInvoiceReference", "cart.validation-error.mandatory-separate-invoice-reference");
    }

    @Test

    public void testValidateReference_ok() {
        DeliveryMethodForm deliveryMethodForm = new DeliveryMethodForm();
        deliveryMethodForm.setDeliveryComment(RandomStringUtils.random(10));
        deliveryMethodForm.setDeliveryOption(DELIVERY_OPTION);
        deliveryMethodForm.setDeliveryMethod(SHIPPING);
        deliveryMethodForm.setSelectedShippingDate(FORMATTER.format(FUTURE_DATE));
        deliveryMethodForm.setYardReference("some reference");

        HashMap<String, Boolean> deliveryMethods = new HashMap<>();
        deliveryMethods.put(SHIPPING, true);

        when(deliveryMethodService.getSupportedDeliveryMethods(cart)).thenReturn(deliveryMethods);
        when(shippingInfoService.getFirstPossibleShippingDate(cart)).thenReturn(Optional.of(FUTURE_DATE));
        when(currentUnit.getRef2()).thenReturn(ReferenceConditionalType.MANDATORY);

        PointOfServiceModel pos = PointOfServiceModelMockBuilder
                .aPointOfService()
                .build();

        BaseStoreModel baseStoreModel = BaseStoreModelBuilder
                .aModel()
                .build();
        baseStoreModel.setPointsOfService(Collections.singletonList(pos));

        when(pointOfServiceService.getCurrentPointOfService()).thenReturn(pos);
        when(baseStoreService.getCurrentBaseStore()).thenReturn(baseStoreModel);

        BaseSiteModel baseSiteModel = mock(BaseSiteModel.class);
        when(baseSiteService.getCurrentBaseSite()).thenReturn(baseSiteModel);
        when(baseSiteModel.getChannel()).thenReturn(SiteChannel.B2B);

        Errors errors = mock(Errors.class);

        validator.validate(deliveryMethodForm, errors);

        verify(b2BUnitService).getB2BUnitModelForCurrentUser();
        verify(errors, never()).rejectValue("yardReference", "cart.validation-error.reference-empty");

    }

    @Test

    public void testValidateReference_error() {
        DeliveryMethodForm deliveryMethodForm = new DeliveryMethodForm();
        deliveryMethodForm.setDeliveryComment(RandomStringUtils.random(10));
        deliveryMethodForm.setDeliveryOption(DELIVERY_OPTION);
        deliveryMethodForm.setDeliveryMethod(SHIPPING);
        deliveryMethodForm.setSelectedShippingDate(FORMATTER.format(FUTURE_DATE));
        HashMap<String, Boolean> deliveryMethods = new HashMap<>();
        deliveryMethods.put(SHIPPING, true);

        when(deliveryMethodService.getSupportedDeliveryMethods(cart)).thenReturn(deliveryMethods);
        when(shippingInfoService.getFirstPossibleShippingDate(cart)).thenReturn(Optional.of(FUTURE_DATE));
        when(currentUnit.getRef2()).thenReturn(ReferenceConditionalType.MANDATORY);

        PointOfServiceModel pos = PointOfServiceModelMockBuilder
                .aPointOfService()
                .build();

        BaseStoreModel baseStoreModel = BaseStoreModelBuilder
                .aModel()
                .build();
        baseStoreModel.setPointsOfService(Collections.singletonList(pos));

        when(pointOfServiceService.getCurrentPointOfService()).thenReturn(pos);
        when(baseStoreService.getCurrentBaseStore()).thenReturn(baseStoreModel);

        BaseSiteModel baseSiteModel = mock(BaseSiteModel.class);
        when(baseSiteService.getCurrentBaseSite()).thenReturn(baseSiteModel);
        when(baseSiteModel.getChannel()).thenReturn(SiteChannel.B2B);

        Errors errors = mock(Errors.class);

        validator.validate(deliveryMethodForm, errors);

        verify(b2BUnitService).getB2BUnitModelForCurrentUser();
        verify(errors).rejectValue("yardReference", "cart.validation-error.reference-empty");
    }
}
