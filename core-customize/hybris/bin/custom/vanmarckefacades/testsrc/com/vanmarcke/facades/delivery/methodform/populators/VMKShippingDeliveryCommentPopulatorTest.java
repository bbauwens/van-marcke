package com.vanmarcke.facades.delivery.methodform.populators;

import com.vanmarcke.services.forms.delivery.DeliveryMethodForm;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.CartModel;
import org.apache.commons.lang.RandomStringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKShippingDeliveryCommentPopulatorTest {

    private static final String COMMENT = RandomStringUtils.randomAlphabetic(10);

    @InjectMocks
    private VMKShippingDeliveryCommentPopulator populator;

    @Test
    public void testPopulate() {
        CartModel cart = mock(CartModel.class);
        DeliveryMethodForm form = new DeliveryMethodForm();
        when(cart.getDeliveryComment()).thenReturn(COMMENT);

        populator.populate(cart, form);

        assertThat(form.getDeliveryComment()).isEqualTo(COMMENT);
    }
}