package com.vanmarcke.facades.paymentmodes.impl;

import com.vanmarcke.facades.payment.data.PaymentModeData;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.payment.PaymentModeModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.store.BaseStoreModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKPaymentModeFacadeImplTest {

    @Mock
    private Converter<PaymentModeModel, PaymentModeData> vmkPaymentModeConverter;
    @Mock
    private CartService cartService;
    @InjectMocks
    private DefaultVMKPaymentModeFacade defaultVMKPaymentModeFacade;

    @Test
    public void testGetSupportedPaymentModes_noSessionCart() {

        when(cartService.hasSessionCart()).thenReturn(Boolean.FALSE);
        List<PaymentModeData> result = defaultVMKPaymentModeFacade.getSupportedPaymentModes();

        assertThat(result).isEmpty();
    }

    @Test
    public void testGetSupportedPaymentModes_() {
        CartModel cart = mock(CartModel.class);
        BaseStoreModel baseStore = mock(BaseStoreModel.class);
        PaymentModeModel paymentMode = mock(PaymentModeModel.class);
        PaymentModeData paymentModeData = mock(PaymentModeData.class);

        when(cartService.hasSessionCart()).thenReturn(Boolean.TRUE);
        when(cartService.getSessionCart()).thenReturn(cart);
        when(cart.getStore()).thenReturn(baseStore);
        when(baseStore.getPaymentModes()).thenReturn(Collections.singletonList(paymentMode));
        when(paymentMode.getActive()).thenReturn(Boolean.TRUE);
        when(vmkPaymentModeConverter.convert(paymentMode)).thenReturn(paymentModeData);
        List<PaymentModeData> result = defaultVMKPaymentModeFacade.getSupportedPaymentModes();

        assertThat(result).isNotEmpty();
        assertThat(result).contains(paymentModeData);
    }
}