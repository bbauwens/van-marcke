package com.vanmarcke.facades.search.converters.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.search.facetdata.FacetData;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKFacetPopulatorTest {

    @InjectMocks
    private VMKFacetPopulator populator;

    @Test
    public void testPopulate() {
        FacetData source = mock(FacetData.class);
        FacetData target = new FacetData();

        when(source.getUnitSymbol()).thenReturn("mm");
        when(source.getTopValues()).thenReturn(null);
        when(source.getValues()).thenReturn(null);
        populator.populate(source, target);

        assertThat(target.getUnitSymbol()).isEqualTo("mm");
    }
}
