package com.vanmarcke.facades.cart.converters.populators;

import com.vanmarcke.services.order.credit.VMKCreditCheckService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKCartCreditCheckPopulatorTest {

    @Mock
    private VMKCreditCheckService vmkCreditCheckService;

    @InjectMocks
    private VMKCartCreditCheckPopulator<AbstractOrderModel, AbstractOrderData> blueCartCreditCheckPopulator;

    @Test
    public void testPopulate_noCheckoutMode() {
        // given
        final AbstractOrderModel orderModel = mock(AbstractOrderModel.class);
        when(orderModel.getCheckoutMode()).thenReturn(false);

        final AbstractOrderData orderData = new AbstractOrderData();

        // when
        blueCartCreditCheckPopulator.populate(orderModel, orderData);

        // then
        assertThat(orderData.isCreditworthy()).isFalse();

        verify(vmkCreditCheckService, never()).validateCreditworthinessForCart(orderModel);
    }

    @Test
    public void testPopulate_checkoutMode_creditWorthy() {
        // given
        final AbstractOrderModel orderModel = mock(AbstractOrderModel.class);
        when(orderModel.getCheckoutMode()).thenReturn(true);

        when(vmkCreditCheckService.validateCreditworthinessForCart(orderModel)).thenReturn(true);

        final AbstractOrderData orderData = new AbstractOrderData();

        // when
        blueCartCreditCheckPopulator.populate(orderModel, orderData);

        // then
        assertThat(orderData.isCreditworthy()).isTrue();

        verify(vmkCreditCheckService).validateCreditworthinessForCart(orderModel);
    }

    @Test
    public void testPopulate_checkoutMode_notCreditWorthy() {
        // given
        final AbstractOrderModel orderModel = mock(AbstractOrderModel.class);
        when(orderModel.getCheckoutMode()).thenReturn(true);

        when(vmkCreditCheckService.validateCreditworthinessForCart(orderModel)).thenReturn(false);

        final AbstractOrderData orderData = new AbstractOrderData();

        // when
        blueCartCreditCheckPopulator.populate(orderModel, orderData);

        // then
        assertThat(orderData.isCreditworthy()).isFalse();

        verify(vmkCreditCheckService).validateCreditworthinessForCart(orderModel);
    }
}