package com.vanmarcke.facades.productcarousel.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2lib.model.components.ProductCarouselComponentModel;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKProductCarouselFacadeTest {

    protected static final List<ProductOption> PRODUCT_OPTIONS = Arrays.asList(ProductOption.BASIC, ProductOption.PRICE);

    @Mock
    ProductFacade productFacade;
    @InjectMocks
    private VMKProductCarouselFacade defaultVMKProductCarouselFacade;

    @Test
    public void testFetchProductsForNonPreviewMode_products() {
        final List<ProductModel> products = new ArrayList<>();
        final ProductCarouselComponentModel component = mock(ProductCarouselComponentModel.class);
        final ProductModel productModel = mock(ProductModel.class);
        final ProductData productData = mock(ProductData.class);
        products.add(productModel);

        when(component.getProducts()).thenReturn(products);
        when(productModel.getCode()).thenReturn("1234");
        when(this.productFacade.getProductForCodeAndOptions("1234", PRODUCT_OPTIONS)).thenReturn(productData);
        final List<ProductData> result = this.defaultVMKProductCarouselFacade.fetchProductsForNonPreviewMode(component);

        assertThat(result).isNotEmpty().contains(productData);
    }

    @Test
    public void testFetchProductsForNonPreviewMode_categories() {
        final List<CategoryModel> categories = new ArrayList<>();
        final List<ProductModel> products = new ArrayList<>();
        final ProductCarouselComponentModel component = mock(ProductCarouselComponentModel.class);
        final CategoryModel categoryModel = mock(CategoryModel.class);
        final ProductModel productModel = mock(ProductModel.class);
        final ProductData productData = mock(ProductData.class);
        categories.add(categoryModel);
        products.add(productModel);

        when(component.getCategories()).thenReturn(categories);
        when(categoryModel.getProducts()).thenReturn(products);
        when(productModel.getCode()).thenReturn("1234");
        when(this.productFacade.getProductForCodeAndOptions("1234", PRODUCT_OPTIONS)).thenReturn(productData);
        final List<ProductData> result = this.defaultVMKProductCarouselFacade.fetchProductsForNonPreviewMode(component);

        assertThat(result).isNotEmpty().contains(productData);
    }

    @Test
    public void testFetchProductsForNonPreviewMode_productsWithUnknownIdentifier() {
        final List<ProductModel> products = new ArrayList<>();
        final ProductCarouselComponentModel component = mock(ProductCarouselComponentModel.class);
        final ProductModel unknownProductModel = mock(ProductModel.class);
        final ProductModel productModel = mock(ProductModel.class);
        final ProductData productData = mock(ProductData.class);
        products.add(unknownProductModel);
        products.add(productModel);

        when(component.getProducts()).thenReturn(products);
        when(unknownProductModel.getCode()).thenReturn("????");
        when(productModel.getCode()).thenReturn("1234");
        when(this.productFacade.getProductForCodeAndOptions("????", PRODUCT_OPTIONS)).thenThrow(UnknownIdentifierException.class);
        when(this.productFacade.getProductForCodeAndOptions("1234", PRODUCT_OPTIONS)).thenReturn(productData);
        final List<ProductData> result = this.defaultVMKProductCarouselFacade.fetchProductsForNonPreviewMode(component);

        assertThat(result).isNotEmpty().contains(productData);
    }
}