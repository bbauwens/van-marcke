package com.vanmarcke.facades.cart.impl;

import com.vanmarcke.services.util.PdfUtils;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.CommerceSaveCartParameterData;
import de.hybris.platform.commercefacades.order.data.CommerceSaveCartResultData;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.commerceservices.order.CommerceSaveCartException;
import de.hybris.platform.commerceservices.order.CommerceSaveCartService;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.MediaType;

import java.io.InputStream;
import java.util.Collections;
import java.util.List;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKSaveCartFacadeImplTest {

    @Mock
    private MediaService mediaService;
    @Mock
    private Converter<CartModel, CartData> cartSummaryConverter;
    @Mock
    private CommerceCartService commerceCartService;
    @Mock
    private UserService userService;
    @Mock
    private Converter<CartModel, CartData> cartConverter;
    @Mock
    private BaseSiteService baseSiteService;
    @Mock
    private CommerceSaveCartService commerceSaveCartService;
    @Mock
    private PdfUtils pdfUtils;

    private VMKSaveCartFacadeImpl vmkSaveCartFacade;

    @Before
    public void setup() {
        vmkSaveCartFacade = new VMKSaveCartFacadeImpl(mediaService, cartSummaryConverter, pdfUtils);
        vmkSaveCartFacade.setCommerceCartService(commerceCartService);
        vmkSaveCartFacade.setUserService(userService);
        vmkSaveCartFacade.setCartConverter(cartConverter);
        vmkSaveCartFacade.setBaseSiteService(baseSiteService);
        vmkSaveCartFacade.setCommerceSaveCartService(commerceSaveCartService);
    }

    @Test
    public void testExportTechnicalDataSheetsWhenNoCartFound() {
        final UserModel userModel = mock(UserModel.class);

        when(this.userService.getCurrentUser()).thenReturn(userModel);
        when(this.commerceCartService.getCartForCodeAndUser("0101", userModel)).thenReturn(null);

        final InputStream result = this.vmkSaveCartFacade.exportTechnicalDataSheetsForSavedCartId("0101");

        assertThat(result).isNull();
    }

    @Test
    public void testExportTechnicalDataSheets() {
        final UserModel userModel = mock(UserModel.class);
        final CartModel cartModel = mock(CartModel.class);
        final AbstractOrderEntryModel orderEntryModel = mock(AbstractOrderEntryModel.class);
        final ProductModel productModel = mock(ProductModel.class);
        final MediaModel mediaModel = mock(MediaModel.class);
        final InputStream inputStream = mock(InputStream.class);

        when(this.userService.getCurrentUser()).thenReturn(userModel);
        when(this.commerceCartService.getCartForCodeAndUser("0101", userModel)).thenReturn(cartModel);
        when(cartModel.getEntries()).thenReturn(Collections.singletonList(orderEntryModel));
        when(orderEntryModel.getProduct()).thenReturn(productModel);
        when(productModel.getTech_data_sheet()).thenReturn(mediaModel);
        when(mediaModel.getMime()).thenReturn(MediaType.APPLICATION_PDF_VALUE);
        when(this.mediaService.getStreamFromMedia(mediaModel)).thenReturn(inputStream);
        when(pdfUtils.mergePdfFiles(any())).thenReturn(inputStream);

        final InputStream result = this.vmkSaveCartFacade.exportTechnicalDataSheetsForSavedCartId("0101");

        assertThat(result).isNotNull();
    }

    @Test(expected = CommerceSaveCartException.class)
    public void testGetCartForCodeAndCurrentUser_noCartId() throws Exception {
        final CommerceSaveCartParameterData inputParameters = mock(CommerceSaveCartParameterData.class);

        when(inputParameters.getCartId()).thenReturn(StringUtils.EMPTY);
        vmkSaveCartFacade.getCartForCodeAndCurrentUser(inputParameters);

        verifyZeroInteractions(commerceCartService);
    }

    @Test(expected = CommerceSaveCartException.class)
    public void testGetCartForCodeAndCurrentUser_noCartFound() throws Exception {
        final CommerceSaveCartParameterData inputParameters = mock(CommerceSaveCartParameterData.class);
        final UserModel currentUser = mock(UserModel.class);

        when(inputParameters.getCartId()).thenReturn("0012315");
        when(userService.getCurrentUser()).thenReturn(currentUser);
        when(commerceCartService.getCartForCodeAndUser("0012315", currentUser)).thenReturn(null);
        vmkSaveCartFacade.getCartForCodeAndCurrentUser(inputParameters);

        verify(commerceCartService, never()).recalculateCart(any(CommerceCartParameter.class));
    }

    @Test
    public void testGetCartForCodeAndCurrentUser() throws Exception {
        final CommerceSaveCartParameterData inputParameters = mock(CommerceSaveCartParameterData.class);
        final UserModel currentUser = mock(UserModel.class);
        final CartModel cart = mock(CartModel.class);
        final CartData cartData = mock(CartData.class);

        when(inputParameters.getCartId()).thenReturn("0012315");
        when(userService.getCurrentUser()).thenReturn(currentUser);
        when(commerceCartService.getCartForCodeAndUser("0012315", currentUser)).thenReturn(cart);
        when(cartConverter.convert(cart)).thenReturn(cartData);
        final CommerceSaveCartResultData result = vmkSaveCartFacade.getCartForCodeAndCurrentUser(inputParameters);

        verify(commerceCartService).recalculateCart(any(CommerceCartParameter.class));
        assertThat(result.getSavedCartData()).isEqualTo(cartData);
    }

    @Test(expected = CommerceSaveCartException.class)
    public void testGetCartSummary_noCartId() throws Exception {
        final CommerceSaveCartParameterData inputParameters = mock(CommerceSaveCartParameterData.class);

        when(inputParameters.getCartId()).thenReturn(StringUtils.EMPTY);
        vmkSaveCartFacade.getCartSummary(inputParameters);

        verifyZeroInteractions(commerceCartService);
    }

    @Test(expected = CommerceSaveCartException.class)
    public void testGetCartSummary_noCartFound() throws Exception {
        final CommerceSaveCartParameterData inputParameters = mock(CommerceSaveCartParameterData.class);
        final UserModel currentUser = mock(UserModel.class);

        when(inputParameters.getCartId()).thenReturn("0012315");
        when(userService.getCurrentUser()).thenReturn(currentUser);
        when(commerceCartService.getCartForCodeAndUser("0012315", currentUser)).thenReturn(null);

        vmkSaveCartFacade.getCartSummary(inputParameters);

        verify(commerceCartService, never()).recalculateCart(any(CommerceCartParameter.class));
    }

    @Test
    public void testGetCartSummary() throws Exception {
        final CommerceSaveCartParameterData inputParameters = mock(CommerceSaveCartParameterData.class);
        final UserModel currentUser = mock(UserModel.class);
        final CartModel cart = mock(CartModel.class);
        final CartData cartData = mock(CartData.class);

        when(inputParameters.getCartId()).thenReturn("0012315");
        when(userService.getCurrentUser()).thenReturn(currentUser);
        when(commerceCartService.getCartForCodeAndUser("0012315", currentUser)).thenReturn(cart);
        when(cartSummaryConverter.convert(cart)).thenReturn(cartData);

        final CommerceSaveCartResultData result = vmkSaveCartFacade.getCartSummary(inputParameters);

        assertThat(result.getSavedCartData()).isEqualTo(cartData);
    }

    @Test
    public void getSavedCartsForCurrentUser() {
        BaseSiteModel baseSite = mock(BaseSiteModel.class);
        UserModel user = mock(UserModel.class);
        PageableData pageableData = mock(PageableData.class);
        List<OrderStatus> orderStatus = Collections.singletonList(OrderStatus.ORDER_SPLIT);
        SearchPageData<CartModel> searchPageData = mock(SearchPageData.class);
        List<CartModel> cartModels = Collections.singletonList(mock(CartModel.class));
        List<CartData> expected = Collections.singletonList(mock(CartData.class));
        when(userService.getCurrentUser()).thenReturn(user);
        when(baseSiteService.getCurrentBaseSite()).thenReturn(baseSite);
        when(commerceSaveCartService.getSavedCartsForSiteAndUser(pageableData, baseSite, user, orderStatus)).thenReturn(searchPageData);
        when(searchPageData.getResults()).thenReturn(cartModels);
        when(cartSummaryConverter.convertAll(cartModels)).thenReturn(expected);

        SearchPageData<CartData> actual = vmkSaveCartFacade.getSavedCartsForCurrentUser(pageableData, orderStatus);

        assertThat(actual.getResults()).isEqualTo(expected);
    }
}