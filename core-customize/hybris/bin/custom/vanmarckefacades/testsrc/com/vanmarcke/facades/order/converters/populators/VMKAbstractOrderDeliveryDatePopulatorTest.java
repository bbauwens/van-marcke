package com.vanmarcke.facades.order.converters.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Date;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKAbstractOrderDeliveryDatePopulatorTest {

    @InjectMocks
    private VMKAbstractOrderDeliveryDatePopulator blueAbstractOrderDeliveryDatePopulator;

    @Test
    public void populate() {
        final Date deliveryDate = mock(Date.class);
        final AbstractOrderModel source = mock(AbstractOrderModel.class);
        final AbstractOrderData result = new AbstractOrderData();

        when(source.getDeliveryDate()).thenReturn(deliveryDate);

        blueAbstractOrderDeliveryDatePopulator.populate(source, result);

        assertThat(result.getDeliveryDate()).isEqualTo(deliveryDate);
    }

}