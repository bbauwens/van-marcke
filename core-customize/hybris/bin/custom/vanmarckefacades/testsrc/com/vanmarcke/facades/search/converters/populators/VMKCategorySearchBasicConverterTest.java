package com.vanmarcke.facades.search.converters.populators;

import com.vanmarcke.facades.search.converters.VMKCategorySearchBasicConverter;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.solrfacetsearch.search.impl.SolrResult;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKCategorySearchBasicConverterTest extends AbstractSearchConverterTest {

    @InjectMocks
    private VMKCategorySearchBasicConverter vmkCategorySearchBasicConverter;

    @Test
    public void testConvert() {
        SolrResult source = mockSorlResult();

        when(document.getFirstValue("code")).thenReturn("code");
        when(document.getFirstValue("name")).thenReturn("name");
        when(document.getFirstValue("url")).thenReturn("url");
        when(document.getFirstValue("thumbnail")).thenReturn("thumbnail");

        CategoryData result = vmkCategorySearchBasicConverter.convert(source);

        assertThat(result.getCode()).isEqualTo("code");
        assertThat(result.getName()).isEqualTo("name");
        assertThat(result.getUrl()).isEqualTo("url");
        assertThat(result.getThumbnail()).isNotNull();
        assertThat(result.getThumbnail().getUrl()).isEqualTo("thumbnail");
    }

    @Test
    public void testConvert_withoutThumbnail() {
        SolrResult source = mockSorlResult();

        when(document.getFirstValue("code")).thenReturn("code");
        when(document.getFirstValue("name")).thenReturn("name");
        when(document.getFirstValue("url")).thenReturn("url");

        CategoryData result = vmkCategorySearchBasicConverter.convert(source);

        assertThat(result.getCode()).isEqualTo("code");
        assertThat(result.getName()).isEqualTo("name");
        assertThat(result.getUrl()).isEqualTo("url");
        assertThat(result.getThumbnail()).isNull();
    }

}