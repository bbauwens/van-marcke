package com.vanmarcke.facades.basesite.impl;

import com.google.common.collect.Lists;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commercefacades.basesite.data.BaseSiteData;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.site.BaseSiteService;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.text.MessageFormat;
import java.util.Collections;
import java.util.List;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

/**
 * The {@link VMKBaseSiteFacadeImplTest} class contains the unit tests for the {@link VMKBaseSiteFacadeImpl} class.
 *
 * @author Christiaan Janssen
 * @since 12-01-2021
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKBaseSiteFacadeImplTest {

    private static final String UID = RandomStringUtils.random(10);

    @Mock
    private BaseSiteService baseSiteService;

    @Mock
    private Converter<BaseSiteModel, BaseSiteData> baseSiteConverter;

    @InjectMocks
    private VMKBaseSiteFacadeImpl baseSiteFacade;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void testGetActiveBaseSites_withoutBaseSites() {
        // given
        when(baseSiteService.getAllBaseSites()).thenReturn(Collections.emptyList());

        // when
        final List<BaseSiteData> actualBaseSiteDatas = baseSiteFacade.getActiveBaseSites();

        // then
        assertThat(actualBaseSiteDatas).isEmpty();

        verify(baseSiteService).getAllBaseSites();
        verify(baseSiteConverter, never()).convertAll(anyListOf(BaseSiteModel.class));
    }

    @Test
    public void testGetActiveBaseSites_withBaseSite_withEcommerceDisabled() {
        // given
        final BaseSiteModel baseSiteModel = mock(BaseSiteModel.class);
        when(baseSiteModel.getEcommerceEnabled()).thenReturn(Boolean.FALSE);

        when(baseSiteService.getAllBaseSites()).thenReturn(Collections.singletonList(baseSiteModel));

        // when
        final List<BaseSiteData> actualBaseSiteDatas = baseSiteFacade.getActiveBaseSites();

        // then
        assertThat(actualBaseSiteDatas).isEmpty();

        verify(baseSiteService).getAllBaseSites();
        verify(baseSiteConverter).convertAll(Collections.emptyList());
    }

    @Test
    public void testGetActiveBaseSites_withBaseSite_withEcommerceEnabled() {
        // given
        final BaseSiteModel baseSiteModel = mock(BaseSiteModel.class);
        when(baseSiteModel.getEcommerceEnabled()).thenReturn(Boolean.TRUE);

        when(baseSiteService.getAllBaseSites()).thenReturn(Collections.singletonList(baseSiteModel));

        final BaseSiteData expectedBaseSiteData = new BaseSiteData();

        when(baseSiteConverter.convertAll(Collections.singletonList(baseSiteModel))).thenReturn(Collections.singletonList(expectedBaseSiteData));

        // when
        final List<BaseSiteData> actualBaseSiteDatas = baseSiteFacade.getActiveBaseSites();

        // then
        assertThat(actualBaseSiteDatas).containsExactly(expectedBaseSiteData);

        verify(baseSiteService).getAllBaseSites();
        verify(baseSiteConverter).convertAll(Lists.newArrayList(baseSiteModel));
    }

    @Test
    public void testGetBaseSite_withoutBaseSite() {
        // given
        when(baseSiteService.getBaseSiteForUID(UID)).thenReturn(null);

        // then
        thrown.expect(UnknownIdentifierException.class);
        thrown.expectMessage(MessageFormat.format("No site found for uit ''{0}''.", UID));

        // when
        baseSiteFacade.getBaseSite(UID);
    }

    @Test
    public void testGetBaseSite_withBaseSite_withEcommerceDisabled() {
        // given
        final BaseSiteModel baseSiteModel = mock(BaseSiteModel.class);
        when(baseSiteModel.getEcommerceEnabled()).thenReturn(Boolean.FALSE);

        when(baseSiteService.getBaseSiteForUID(UID)).thenReturn(baseSiteModel);

        // then
        thrown.expect(UnknownIdentifierException.class);
        thrown.expectMessage(MessageFormat.format("No site found for uit ''{0}''.", UID));

        // when
        baseSiteFacade.getBaseSite(UID);
    }

    @Test
    public void testGetBaseSite_withBaseSite_withEcommerceEnabled() {
        // given
        final BaseSiteModel baseSiteModel = mock(BaseSiteModel.class);
        when(baseSiteModel.getEcommerceEnabled()).thenReturn(Boolean.TRUE);

        when(baseSiteService.getBaseSiteForUID(UID)).thenReturn(baseSiteModel);

        final BaseSiteData expectedBaseSiteData = new BaseSiteData();

        when(baseSiteConverter.convert(baseSiteModel)).thenReturn(expectedBaseSiteData);

        // when
        final BaseSiteData actualBaseSiteData = baseSiteFacade.getBaseSite(UID);

        // then
        assertThat(actualBaseSiteData).isEqualTo(expectedBaseSiteData);

        verify(baseSiteService).getBaseSiteForUID(UID);
        verify(baseSiteConverter).convert(baseSiteModel);
    }
}