package com.vanmarcke.facades.product.converters.populators;

import com.vanmarcke.facades.analytics.data.AnalyticsData;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKProductAnalyticsPopulatorTest {

    @Mock
    private Converter<ProductModel, AnalyticsData> analyticsConverter;
    @Mock
    private ProductModel source;
    @Mock
    private ProductData target;
    @InjectMocks
    private VMKProductAnalyticsPopulator vmkProductAnalyticsPopulator;

    @Test(expected = IllegalArgumentException.class)
    public void testPopulateWhenSourceNull() {
        vmkProductAnalyticsPopulator.populate(null, target);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testPopulateWhenTargetNull() {
        vmkProductAnalyticsPopulator.populate(source, null);
    }

    @Test
    public void testPopulate() {
        AnalyticsData analyticsData = mock(AnalyticsData.class);
        when(analyticsConverter.convert(source)).thenReturn(analyticsData);
        vmkProductAnalyticsPopulator.populate(source, target);
        verify(target).setAnalyticsData(analyticsData);
    }
}