package com.vanmarcke.facades.cms.converters.populator;

import com.vanmarcke.core.enums.ConsentType;
import com.vanmarcke.core.model.ConsentParagraphComponentModel;
import com.vanmarcke.facades.cms.ConsentParagraphData;
import com.vanmarcke.services.model.builder.ConsentParagraphComponentModelMockBuilder;
import de.hybris.bootstrap.annotations.UnitTest;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.fest.assertions.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

/**
 * The {@link VMKConsentParagraphPopulatorTest} class contains the unit tests for the
 * {@link VMKConsentParagraphPopulator} class.
 *
 * @author Christiaan Janssen
 * @since 17-06-2020
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKConsentParagraphPopulatorTest {

    private static final String TITLE = RandomStringUtils.random(10);
    private static final String CONTENT = RandomStringUtils.random(10);
    private static final Boolean EDITABLE = RandomUtils.nextBoolean();

    @InjectMocks
    private VMKConsentParagraphPopulator consentParagraphPopulator;

    @Test
    public void testPopulate() {
        ConsentParagraphComponentModel paragraphComponentModel = ConsentParagraphComponentModelMockBuilder
                .aContentParagraph()
                .withEditable(EDITABLE)
                .withTitle(TITLE)
                .withContent(CONTENT)
                .withConsentType(ConsentType.FUNCTIONAL_COOKIES)
                .build();

        ConsentParagraphData paragraphData = new ConsentParagraphData();

        consentParagraphPopulator.populate(paragraphComponentModel, paragraphData);

        Assertions
                .assertThat(paragraphData.isEditable())
                .isEqualTo(EDITABLE);

        Assertions
                .assertThat(paragraphData.getTitle())
                .isEqualTo(TITLE);

        Assertions
                .assertThat(paragraphData.getContent())
                .isEqualTo(CONTENT);

        Assertions
                .assertThat(paragraphData.getConsentType())
                .isEqualTo(ConsentType.FUNCTIONAL_COOKIES);
    }
}