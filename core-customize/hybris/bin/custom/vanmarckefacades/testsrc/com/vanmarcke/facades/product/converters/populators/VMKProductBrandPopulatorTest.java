package com.vanmarcke.facades.product.converters.populators;

import com.vanmarcke.core.model.BrandCategoryModel;
import com.vanmarcke.services.category.VMKCategoryService;
import com.vanmarcke.services.model.builder.ProductModelMockBuilder;
import com.vanmarcke.services.model.builder.VariantProductModelMockBuilder;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.variants.model.VariantProductModel;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Optional;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

/**
 * The {@link VMKProductBrandPopulatorTest} class contains then unit tests for the {@link VMKProductBrandPopulator}
 * class.
 *
 * @author Christiaan Janssen
 * @since 11-01-2020
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKProductBrandPopulatorTest {

    private static final String SERIES = RandomStringUtils.random(10);
    private static final String TYPE = RandomStringUtils.random(10);
    private static final String NAME = RandomStringUtils.random(10);

    @Mock
    private VMKCategoryService categoryService;

    @Mock
    private Converter<MediaModel, ImageData> imageConverter;

    @Spy
    @InjectMocks
    private VMKProductBrandPopulator productBrandPopulator;

    @Test
    public void testPopulate_withoutValues() {
        // given
        final ProductModel productModel = ProductModelMockBuilder
                .aBaseProduct(null, null)
                .build();

        doReturn(Optional.empty()).when(productBrandPopulator).getBrandCategory(productModel);

        final ProductData productData = new ProductData();

        // when
        productBrandPopulator.populate(productModel, productData);

        // then
        assertThat(productData.getBrand()).isNotNull();
        assertThat(productData.getBrand().getSeries()).isNull();
        assertThat(productData.getBrand().getType()).isNull();
        assertThat(productData.getBrand().getName()).isNull();
        assertThat(productData.getBrand().getLogo()).isNull();

        verify(productBrandPopulator).getBrandCategory(productModel);
        verify(productBrandPopulator, never()).getBrandLogo(any(BrandCategoryModel.class));
    }

    @Test
    public void testPopulate_withValues() {
        // given
        final ProductModel productModel = ProductModelMockBuilder
                .aBaseProduct(null, null)
                .withBrandSeries(SERIES)
                .withBrandType(TYPE)
                .build();

        final BrandCategoryModel brandCategoryModel = mock(BrandCategoryModel.class);
        when(brandCategoryModel.getName()).thenReturn(NAME);

        doReturn(Optional.of(brandCategoryModel)).when(productBrandPopulator).getBrandCategory(productModel);

        final ImageData brandLogoData = new ImageData();

        doReturn(brandLogoData).when(productBrandPopulator).getBrandLogo(brandCategoryModel);

        final ProductData productData = new ProductData();

        // when
        productBrandPopulator.populate(productModel, productData);

        // then
        assertThat(productData.getBrand()).isNotNull();
        assertThat(productData.getBrand().getSeries()).isEqualTo(SERIES);
        assertThat(productData.getBrand().getType()).isEqualTo(TYPE);
        assertThat(productData.getBrand().getName()).isEqualTo(NAME);
        assertThat(productData.getBrand().getLogo()).isEqualTo(brandLogoData);

        verify(productBrandPopulator).getBrandCategory(productModel);
        verify(productBrandPopulator).getBrandLogo(brandCategoryModel);
    }

    @Test
    public void testGetBrandCategory_withoutVariant() {
        // given
        final ProductModel productModel = ProductModelMockBuilder
                .aBaseProduct(null, null)
                .build();

        doReturn(Optional.empty()).when(productBrandPopulator).getVariant(productModel);

        // when
        final Optional<BrandCategoryModel> actualBrandCategory = productBrandPopulator.getBrandCategory(productModel);

        // then
        assertThat(actualBrandCategory.isPresent()).isFalse();
    }

    @Test
    public void testGetBrandCategory_withVariant_withoutBrandCategory() {
        // given
        final ProductModel productModel = ProductModelMockBuilder
                .aBaseProduct(null, null)
                .build();

        final CategoryModel categoryModel = mock(CategoryModel.class);

        final VariantProductModel variantProductModel = VariantProductModelMockBuilder
                .aVariantProduct(null, null)
                .withSupercategories(categoryModel)
                .build();

        doReturn(Optional.of(variantProductModel)).when(productBrandPopulator).getVariant(productModel);

        // when
        final Optional<BrandCategoryModel> actualBrandCategory = productBrandPopulator.getBrandCategory(productModel);

        // then
        assertThat(actualBrandCategory.isPresent()).isFalse();
    }

    @Test
    public void testGetBrandCategory_withVariant_withBrandCategory() {
        // given
        final ProductModel productModel = ProductModelMockBuilder
                .aBaseProduct(null, null)
                .build();

        final BrandCategoryModel expectedBrandCategoryModel = mock(BrandCategoryModel.class);

        final VariantProductModel variantProductModel = VariantProductModelMockBuilder
                .aVariantProduct(null, null)
                .withSupercategories(expectedBrandCategoryModel)
                .build();

        doReturn(Optional.of(variantProductModel)).when(productBrandPopulator).getVariant(productModel);

        // when
        final Optional<BrandCategoryModel> actualBrandCategory = productBrandPopulator.getBrandCategory(productModel);

        // then
        assertThat(actualBrandCategory.isPresent()).isTrue();
        assertThat(actualBrandCategory.get()).isEqualTo(expectedBrandCategoryModel);
    }

    @Test
    public void testGetVariant_withoutVariant() {
        // given
        final ProductModel productModel = ProductModelMockBuilder
                .aBaseProduct(null, null)
                .build();

        // when
        final Optional<VariantProductModel> actualVariantProductModel = productBrandPopulator.getVariant(productModel);

        // then
        assertThat(actualVariantProductModel.isPresent()).isFalse();
    }

    @Test
    public void testGetVariant_withVariant_sourceIsVariant() {
        // given
        final VariantProductModel expectedVariantProductModel = VariantProductModelMockBuilder
                .aVariantProduct(null, null)
                .build();

        // when
        final Optional<VariantProductModel> actualVariantProductModel = productBrandPopulator.getVariant(expectedVariantProductModel);

        // then
        assertThat(actualVariantProductModel.isPresent()).isTrue();
        assertThat(actualVariantProductModel.get()).isEqualTo(expectedVariantProductModel);
    }

    @Test
    public void testGetVariant_withVariant_sourceIsBaseProduct() {
        // given
        final VariantProductModel otherVariantProductModel = VariantProductModelMockBuilder
                .aVariantProduct(null, null)
                .build();

        final BrandCategoryModel brandCategoryModel = mock(BrandCategoryModel.class);

        final VariantProductModel expectedVariantProductModel = VariantProductModelMockBuilder
                .aVariantProduct(null, null)
                .withSupercategories(brandCategoryModel)
                .build();

        final ProductModel productModel = ProductModelMockBuilder
                .aBaseProduct(null, null)
                .withVariants(otherVariantProductModel, expectedVariantProductModel)
                .build();

        // when
        final Optional<VariantProductModel> actualVariantProductModel = productBrandPopulator.getVariant(productModel);

        // then
        assertThat(actualVariantProductModel.isPresent()).isTrue();
        assertThat(actualVariantProductModel.get()).isEqualTo(expectedVariantProductModel);
    }

    @Test
    public void testGetBrandLogo_withoutValue() {
        // given
        final BrandCategoryModel brandCategoryModel = mock(BrandCategoryModel.class);

        when(categoryService.getLocalizedBrandLogoForFormat(brandCategoryModel, "SMALL")).thenReturn(Optional.empty());

        // when
        ImageData actualBrandLogoData = productBrandPopulator.getBrandLogo(brandCategoryModel);

        // then
        assertThat(actualBrandLogoData).isNull();
    }

    @Test
    public void testGetBrandLogo_withValue() {
        // given
        final BrandCategoryModel brandCategoryModel = mock(BrandCategoryModel.class);

        final MediaModel brandLogoModel = mock(MediaModel.class);

        when(categoryService.getLocalizedBrandLogoForFormat(brandCategoryModel, "SMALL")).thenReturn(Optional.of(brandLogoModel));

        final ImageData expectedBrandLogoData = new ImageData();

        when(imageConverter.convert(brandLogoModel)).thenReturn(expectedBrandLogoData);

        // when
        ImageData actualBrandLogoData = productBrandPopulator.getBrandLogo(brandCategoryModel);

        // then
        assertThat(actualBrandLogoData).isEqualTo(expectedBrandLogoData);
    }
}