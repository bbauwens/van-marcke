package com.vanmarcke.facades.cart.validation.impl;

import com.vanmarcke.services.MessageData;
import com.vanmarcke.services.validation.VMKCartValidationService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * The {@code DefaultVMKCartValidationFacadeTest} class contains the unit tests for the
 * {@link VMKCartValidationFacadeImpl} class.
 *
 * @author Niels Raemaekers, Christiaan Janssen
 * @since 06-02-2020
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKCartValidationFacadeImplTest {

    @Mock
    private CartService cartService;

    @Mock
    private VMKCartValidationService vmkCartValidationService;

    @InjectMocks
    private VMKCartValidationFacadeImpl defaultVMKCartValidationFacade;

    @Test
    public void testCheckCartValidity() {
        CartModel cart = mock(CartModel.class);

        when(cartService.hasSessionCart()).thenReturn(Boolean.TRUE);
        when(cartService.getSessionCart()).thenReturn(cart);

        MessageData expectedValidationMessage = new MessageData();

        when(vmkCartValidationService.isCartValid(cart)).thenReturn(expectedValidationMessage);

        MessageData actualValidationMessage = defaultVMKCartValidationFacade.checkCartValidityAndProcessResponse();

        assertThat(actualValidationMessage).isEqualTo(expectedValidationMessage);

        verify(cartService).hasSessionCart();
        verify(cartService).getSessionCart();
        verify(vmkCartValidationService).isCartValid(cart);
    }

    @Test
    public void testCheckCartValidity_noSessionCart() {
        when(cartService.hasSessionCart()).thenReturn(Boolean.FALSE);

        MessageData result = defaultVMKCartValidationFacade.checkCartValidityAndProcessResponse();

        assertThat(result).isNull();

        verify(cartService).hasSessionCart();
        verify(cartService, never()).getSessionCart();
        verify(vmkCartValidationService, never()).isCartValid(any(CartModel.class));
    }
}
