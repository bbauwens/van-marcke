package com.vanmarcke.facades.basesite.converters.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commercefacades.basesite.data.BaseSiteData;
import de.hybris.platform.commercefacades.storesession.data.LanguageData;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

/**
 * The {@link VMKBaseSitePopulatorTest} class contains the unit tests for the {@link VMKBaseSitePopulator} class.
 *
 * @author Christiaan Janssen
 * @since 12-02-2021
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKBaseSitePopulatorTest {

    private static final String UID = RandomStringUtils.random(10);
    private static final String NAME = RandomStringUtils.random(10);
    private static final String LOCALE = RandomStringUtils.random(10);

    @Mock
    private Converter<LanguageModel, LanguageData> languageConverter;

    @InjectMocks
    private VMKBaseSitePopulator baseSitePopulator;

    @Test
    public void testPopulate_withoutValues() {
        // given
        final BaseSiteModel baseSiteModel = mock(BaseSiteModel.class);

        final BaseSiteData baseSiteData = new BaseSiteData();

        // when
        baseSitePopulator.populate(baseSiteModel, baseSiteData);

        // then
        assertThat(baseSiteData.getUid()).isNull();
        assertThat(baseSiteData.getName()).isNull();
        assertThat(baseSiteData.getDefaultLanguage()).isNull();
        assertThat(baseSiteData.getLocale()).isNull();
        assertThat(baseSiteData.getChannel()).isNull();

        verify(languageConverter, never()).convert(any(LanguageModel.class));
    }

    @Test
    public void testPopulate_withValues() {
        // given
        final LanguageModel languageModel = mock(LanguageModel.class);

        final LanguageData languageData = new LanguageData();

        when(languageConverter.convert(languageModel)).thenReturn(languageData);

        final BaseSiteModel baseSiteModel = mock(BaseSiteModel.class);
        when(baseSiteModel.getUid()).thenReturn(UID);
        when(baseSiteModel.getName()).thenReturn(NAME);
        when(baseSiteModel.getDefaultLanguage()).thenReturn(languageModel);
        when(baseSiteModel.getLocale()).thenReturn(LOCALE);
        when(baseSiteModel.getChannel()).thenReturn(SiteChannel.B2B);

        final BaseSiteData baseSiteData = new BaseSiteData();

        // when
        baseSitePopulator.populate(baseSiteModel, baseSiteData);

        // then
        assertThat(baseSiteData.getUid()).isEqualTo(UID);
        assertThat(baseSiteData.getName()).isEqualTo(NAME);
        assertThat(baseSiteData.getDefaultLanguage()).isEqualTo(languageData);
        assertThat(baseSiteData.getLocale()).isEqualTo(LOCALE);
        assertThat(baseSiteData.getChannel()).isEqualTo(SiteChannel.B2B.getCode());

        verify(languageConverter).convert(languageModel);
    }
}