package com.vanmarcke.facades.cart.impl;

import com.vanmarcke.facades.data.order.SavedCartData;
import com.vanmarcke.services.exception.UnsupportedDeliveryDateException;
import com.vanmarcke.services.order.cart.VMKCommerceCartService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commercefacades.order.data.AddToCartParams;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.order.data.CartRestorationData;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartRestoration;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKCartFacadeImplTest {

    @Mock
    private CartService cartService;

    @Mock
    private VMKCommerceCartService commerceCartService;

    @Mock
    private Converter<AddToCartParams, CommerceCartParameter> commerceCartParameterConverter;

    @Mock
    private Converter<CommerceCartModification, CartModificationData> cartModificationConverter;

    @Mock
    private UserService userService;

    @Mock
    private BaseSiteService baseSiteService;

    @Mock
    private ModelService modelService;

    @Mock
    private Converter<CommerceCartRestoration, CartRestorationData> cartRestorationConverter;

    @Mock
    private Converter<AbstractOrderModel, SavedCartData> savedCartConverter;

    @Mock
    private Converter<CartModel, CartData> cartConverter;

    private VMKCartFacadeImpl cartFacade;

    @Captor
    private ArgumentCaptor<CommerceCartParameter> parameterArgumentCaptor;

    @Captor
    private ArgumentCaptor<AbstractOrderModel> abstractOrderArgumentCaptor;

    @Before
    public void setUp() {
        cartFacade = new VMKCartFacadeImpl(savedCartConverter);
        cartFacade.setCartService(cartService);
        cartFacade.setCommerceCartService(commerceCartService);
        cartFacade.setCommerceCartParameterConverter(commerceCartParameterConverter);
        cartFacade.setCartModificationConverter(cartModificationConverter);
        cartFacade.setUserService(userService);
        cartFacade.setBaseSiteService(baseSiteService);
        cartFacade.setModelService(modelService);
        cartFacade.setCartRestorationConverter(cartRestorationConverter);
        cartFacade.setCartConverter(cartConverter);
    }

    @Test
    public void testAddToCart() throws Exception {
        final List<AddToCartParams> addToCartParamsList = Arrays.asList(mock(AddToCartParams.class));

        final List<CommerceCartParameter> commerceCartParameterList = Arrays.asList(mock(CommerceCartParameter.class));
        when(this.commerceCartParameterConverter.convertAll(addToCartParamsList)).thenReturn(commerceCartParameterList);

        final List<CommerceCartModification> commerceCartModificationList = Arrays.asList(mock(CommerceCartModification.class));
        when(this.commerceCartService.addToCart(commerceCartParameterList)).thenReturn(commerceCartModificationList);

        final List<CartModificationData> cartModificationDataList = Arrays.asList(mock(CartModificationData.class));
        when(this.cartModificationConverter.convertAll(commerceCartModificationList)).thenReturn(cartModificationDataList);

        final List<CartModificationData> result = this.cartFacade.addToCart(addToCartParamsList);
        assertThat(result).containsExactly(cartModificationDataList.toArray());
    }

    @Test
    public void testUpdateCartCheckoutMode() {
        final CartModel cart = mock(CartModel.class);

        when(cartService.hasSessionCart()).thenReturn(true);
        when(cartService.getSessionCart()).thenReturn(cart);

        cartFacade.updateCartCheckoutMode(true);

        verify(commerceCartService).updateCartCheckoutMode(parameterArgumentCaptor.capture());

        final CommerceCartParameter parameterValue = parameterArgumentCaptor.getValue();
        assertThat(parameterValue.getCart()).isEqualTo(cart);
        assertThat(parameterValue.getCheckoutMode()).isTrue();
        assertThat(parameterValue.isEnableHooks()).isTrue();
    }

    @Test
    public void testSaveCart() {
        final CartModel cart = mock(CartModel.class);
        final String cartGuid = "11223344";
        final BaseSiteModel site = mock(BaseSiteModel.class);
        when(baseSiteService.getCurrentBaseSite()).thenReturn(site);
        when(commerceCartService.getCartForGuidAndSite(cartGuid, site)).thenReturn(cart);

        cartFacade.getSavedCartDetails(cartGuid);
        verify(savedCartConverter).convert(abstractOrderArgumentCaptor.capture());
        assertThat(abstractOrderArgumentCaptor.getValue()).isEqualTo(cart);
    }

    @Test
    public void testSetRequestedDeliveryDate_withoutSessionCart() throws UnsupportedDeliveryDateException {
        // given
        when(cartService.hasSessionCart()).thenReturn(false);

        // when
        final CartData actualCartData = cartFacade.setRequestedDeliveryDate(new Date());

        // then
        assertThat(actualCartData).isNull();

        verify(cartService).hasSessionCart();
        verify(cartService, never()).getSessionCart();
        verify(cartConverter, never()).convert(any(CartModel.class));
        verify(commerceCartService, never()).setRequestedDeliveryDate(any(CommerceCartParameter.class), any(Date.class));
    }

    @Test
    public void testSetRequestedDeliveryDate_withSessionCart() throws UnsupportedDeliveryDateException {
        // given
        final Date date = new Date();

        final CartModel cartModel = mock(CartModel.class);

        when(cartService.hasSessionCart()).thenReturn(true);
        when(cartService.getSessionCart()).thenReturn(cartModel);

        final CartData expectedCartData = new CartData();

        when(cartConverter.convert(cartModel)).thenReturn(expectedCartData);

        // when
        final CartData actualCartData = cartFacade.setRequestedDeliveryDate(date);

        // then
        assertThat(actualCartData).isEqualTo(expectedCartData);

        verify(cartService).hasSessionCart();
        verify(cartService).getSessionCart();
        verify(cartConverter).convert(cartModel);
        verify(commerceCartService).setRequestedDeliveryDate(parameterArgumentCaptor.capture(), eq(date));

        final CommerceCartParameter parameterValue = parameterArgumentCaptor.getValue();

        assertThat(parameterValue).isNotNull();
        assertThat(parameterValue.getCart()).isEqualTo(cartModel);
    }
}