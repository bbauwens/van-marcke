package com.vanmarcke.facades.search.builder;

import com.vanmarcke.facades.search.data.VariantSearchStateData;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.enums.SearchQueryContext;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.FilterQueryOperator;
import org.junit.Assert;
import org.junit.Test;

@UnitTest
public class VariantSearchStateDataBuilderTest {

    @Test
    public void testBuild() {
        VariantSearchStateData variantSearchStateData = new VariantSearchStateDataBuilder()
                .withSearchQuery("filterQuery")
                .withCategoryCode("01-01-02")
                .withFilterQuery("baseProductCode", "112233", FilterQueryOperator.AND)
                .withContext(SearchQueryContext.PDP)
                .build();

        Assert.assertEquals("filterQuery", variantSearchStateData.getQuery().getValue());
        Assert.assertEquals(1, variantSearchStateData.getQuery().getFilterQueries().size());
        Assert.assertEquals("baseProductCode", variantSearchStateData.getQuery().getFilterQueries().get(0).getKey());
        Assert.assertEquals("112233", variantSearchStateData.getQuery().getFilterQueries().get(0).getValues().iterator().next());
        Assert.assertEquals(FilterQueryOperator.AND, variantSearchStateData.getQuery().getFilterQueries().get(0).getOperator());
        Assert.assertEquals(SearchQueryContext.PDP, variantSearchStateData.getQuery().getSearchQueryContext());
        Assert.assertEquals("01-01-02", variantSearchStateData.getCategoryCode());
    }
}