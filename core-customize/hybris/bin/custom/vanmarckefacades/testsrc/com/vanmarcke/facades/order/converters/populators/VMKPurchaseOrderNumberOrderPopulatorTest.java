package com.vanmarcke.facades.order.converters.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.core.model.order.OrderModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKPurchaseOrderNumberOrderPopulatorTest {

    @InjectMocks
    private VMKPurchaseOrderNumberOrderPopulator bluePurchaseOrderNumberOrderPopulator;

    @Test
    public void populate() {
        final OrderModel orderModel = mock(OrderModel.class);

        when(orderModel.getPurchaseOrderNumber()).thenReturn("po-number");

        final OrderData result = new OrderData();
        bluePurchaseOrderNumberOrderPopulator.populate(orderModel, result);

        assertThat(result.getPurchaseOrderNumber()).isEqualTo("po-number");
    }

}