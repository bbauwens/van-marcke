package com.vanmarcke.facades.product.converters.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.core.model.media.MediaModel;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKDownloadsPopulatorTest {

    private static final String URL = RandomStringUtils.randomAlphabetic(10);
    private static final String FILE_NAME = RandomStringUtils.randomAlphabetic(10);
    private static final String NAME = RandomStringUtils.randomAlphabetic(10);
    private static final String EXTENSION = ".pdf";

    @InjectMocks
    private VMKDownloadsPopulator vmkDownloadsPopulator;

    @Test
    public void testPopulate() {
        MediaModel source = mock(MediaModel.class);

        when(source.getURL()).thenReturn(URL);
        when(source.getRealFileName()).thenReturn(FILE_NAME + EXTENSION);
        when(source.getName()).thenReturn(NAME);

        ImageData target = new ImageData();
        vmkDownloadsPopulator.populate(source, target);

        assertThat(target.getName()).isEqualTo(NAME);
        assertThat(target.getUrl()).isEqualTo(URL);
    }

    @Test
    public void testPopulate_withoutLocalizedName() {
        MediaModel source = mock(MediaModel.class);

        when(source.getURL()).thenReturn(URL);
        when(source.getRealFileName()).thenReturn(FILE_NAME + EXTENSION);

        ImageData target = new ImageData();
        vmkDownloadsPopulator.populate(source, target);

        assertThat(target.getName()).isEqualTo(FILE_NAME);
        assertThat(target.getUrl()).isEqualTo(URL);
    }
}