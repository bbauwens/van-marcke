package com.vanmarcke.facades.address.converters.populators;

import com.vanmarcke.cpi.data.ptv.PTVAddressData;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.*;

@UnitTest
public class VMKPTVAddressPopulatorTest {

    private VMKPTVAddressPopulator ptvAddressPopulator;

    @Before
    public void setUp() {
        ptvAddressPopulator = new VMKPTVAddressPopulator();
    }

    @Test
    public void populate() {
        AddressData addressData = mock(AddressData.class);
        PTVAddressData ptvAddressData = mock(PTVAddressData.class);
        CountryData country = mock(CountryData.class);
        when(country.getIsocode()).thenReturn("isocode");
        when(addressData.getCountry()).thenReturn(country);
        when(addressData.getTown()).thenReturn("town");
        when(addressData.getPostalCode()).thenReturn("postalCode");
        when(addressData.getLine1()).thenReturn("street");
        when(addressData.getLine2()).thenReturn("number");

        ptvAddressPopulator.populate(addressData, ptvAddressData);

        verify(ptvAddressData).setCity("town");
        verify(ptvAddressData).setCountry("isocode");
        verify(ptvAddressData).setHouseNumber("number");
        verify(ptvAddressData).setStreet("street");
        verify(ptvAddressData).setPostCode("postalCode");
    }
}