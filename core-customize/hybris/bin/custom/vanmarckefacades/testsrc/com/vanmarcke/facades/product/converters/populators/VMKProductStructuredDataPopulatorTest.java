package com.vanmarcke.facades.product.converters.populators;

import com.vanmarcke.core.model.VanMarckeVariantProductModel;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


/**
 * The {@link VMKProductStructuredDataPopulatorTest} class contains the unit test for the
 * {@link VMKProductStructuredDataPopulator} class.
 *
 * @author Giani Ifrim
 * @since 20-07-2021
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKProductStructuredDataPopulatorTest {

    private static final String ITEM_BARCODE = "barcode";
    private static final String ITEM_NUMBER = "number";
    private static final String CATEGORY = "cat";
    private static final String THUMBNAIL = "http://image.here";

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @InjectMocks
    VMKProductStructuredDataPopulator vmkProductStructuredDataPopulator;

    @Test
    public void testPopulate() {
        ProductModel source = mock(ProductModel.class);
        ProductData target = new ProductData();

        final CategoryModel category = mock(CategoryModel.class);
        when(category.getName()).thenReturn(CATEGORY);

        final MediaModel media = mock(MediaModel.class);
        when(media.getURL()).thenReturn(THUMBNAIL);
        when(source.getThumbnail()).thenReturn(media);

        when(source.getSupercategories()).thenReturn(Collections.singleton(category));
        when(source.getVendorItemBarcode_current()).thenReturn(ITEM_BARCODE);
        when(source.getVendorItemNumber_current()).thenReturn(ITEM_NUMBER);

        vmkProductStructuredDataPopulator.populate(source, target);

        assertEquals(CATEGORY, target.getSuperCategory());
        assertEquals(ITEM_BARCODE, target.getGtin());
        assertEquals(ITEM_BARCODE, target.getIdentifier());
        assertEquals(ITEM_NUMBER, target.getMpn());
        assertEquals(ITEM_NUMBER, target.getProductId());
        assertEquals(THUMBNAIL, target.getThumbnail());
    }

    @Test
    public void testPopulateWhenSourceIsNull() {
        ProductData target = new ProductData();

        thrown.expect(NullPointerException.class);

        vmkProductStructuredDataPopulator.populate(null, target);
    }

    @Test
    public void testPopulateWhenTargetIsNull() {
        ProductModel source = mock(ProductModel.class);

        final CategoryModel category = mock(CategoryModel.class);
        when(category.getName()).thenReturn(CATEGORY);
        when(source.getSupercategories()).thenReturn(Collections.singleton(category));

        thrown.expect(NullPointerException.class);

        vmkProductStructuredDataPopulator.populate(source, null);
    }

    @Test
    public void testPopulateThumbnailWhenSourceIsBaseProduct() {
        ProductModel source = mock(ProductModel.class);
        VanMarckeVariantProductModel variant = mock(VanMarckeVariantProductModel.class);
        ProductData target = new ProductData();

        CategoryModel category = mock(CategoryModel.class);
        when(category.getName()).thenReturn(CATEGORY);
        when(source.getSupercategories()).thenReturn(Collections.singleton(category));

        MediaModel media = mock(MediaModel.class);
        when(media.getURL()).thenReturn(THUMBNAIL);
        when(variant.getThumbnail()).thenReturn(media);
        when(source.getThumbnail()).thenReturn(null);
        when(source.getVariants()).thenReturn(Collections.singletonList(variant));

        vmkProductStructuredDataPopulator.populate(source, target);
        assertEquals(THUMBNAIL, target.getThumbnail());
    }

    @Test
    public void testPopulateThumbnailWhenSourceIsBaseProductAndVariantsAreMissingThumbnail() {
        ProductModel source = mock(ProductModel.class);
        VanMarckeVariantProductModel variant = mock(VanMarckeVariantProductModel.class);
        ProductData target = new ProductData();

        CategoryModel category = mock(CategoryModel.class);
        when(category.getName()).thenReturn(CATEGORY);
        when(source.getSupercategories()).thenReturn(Collections.singleton(category));

        when(variant.getThumbnail()).thenReturn(null);
        when(source.getThumbnail()).thenReturn(null);
        when(source.getVariants()).thenReturn(Collections.singletonList(variant));

        vmkProductStructuredDataPopulator.populate(source, target);

        assertNull(target.getThumbnail());
    }
}
