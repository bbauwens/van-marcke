package com.vanmarcke.facades.delivery.impl;

import com.vanmarcke.services.basestore.VMKBaseStoreService;
import com.vanmarcke.services.delivery.VMKDeliveryInfoService;
import com.vanmarcke.services.delivery.VMKPickupInfoService;
import com.vanmarcke.services.delivery.VMKShippingInfoService;
import com.vanmarcke.services.delivery.impl.VMKFirstDateAvailabilityServiceImpl;
import com.vanmarcke.services.forms.delivery.DeliveryMethodForm;
import com.vanmarcke.services.order.cart.VMKTecCollectReservationMessageService;
import com.vanmarcke.services.storelocator.pos.VMKPointOfServiceService;
import com.vanmarcke.services.storelocator.schedule.VMKOpeningScheduleService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.storelocator.data.PointOfServiceData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKDeliveryInformationFacadeImplTest {

    @Mock
    private CartModel cart;
    @Mock
    private Converter<CartModel, DeliveryMethodForm> deliveryMethodFormConverter;
    @Mock
    private VMKDeliveryInfoService vmkDeliveryInfoService;
    @Mock
    private VMKFirstDateAvailabilityServiceImpl vmkFirstDateAvailabilityService;
    @Mock
    private VMKBaseStoreService vmkBaseStoreService;
    @Mock
    private VMKPointOfServiceService pointOfServiceService;
    @Mock
    private VMKPickupInfoService pickupInfoService;
    @Mock
    private VMKTecCollectReservationMessageService tecCollectReservationMessageService;
    @Mock
    private VMKOpeningScheduleService openingScheduleService;
    @Mock
    private Populator<PointOfServiceModel, PointOfServiceData> pointOfServicePopulator;
    @Mock
    private VMKShippingInfoService shippingInfoService;

    @InjectMocks
    private VMKDeliveryInformationFacadeImpl deliveryInformationFacade;

    @Before
    public void setUp() {
        deliveryInformationFacade = new VMKDeliveryInformationFacadeImpl(deliveryMethodFormConverter, pointOfServicePopulator, vmkDeliveryInfoService, vmkFirstDateAvailabilityService, vmkBaseStoreService,
                pointOfServiceService, pickupInfoService, shippingInfoService, tecCollectReservationMessageService, openingScheduleService);
    }
}
