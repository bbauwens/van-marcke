package com.vanmarcke.facades.search.impl;

import com.vanmarcke.facades.search.builder.VariantSearchStateDataBuilder;
import com.vanmarcke.facades.search.data.VariantSearchStateData;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.search.data.SearchQueryData;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commerceservices.enums.SearchQueryContext;
import de.hybris.platform.commerceservices.search.ProductSearchService;
import de.hybris.platform.commerceservices.search.facetdata.ProductCategorySearchPageData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.FilterQueryOperator;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryData;
import de.hybris.platform.commerceservices.threadcontext.ThreadContextService;
import de.hybris.platform.commerceservices.threadcontext.impl.DefaultThreadContextService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKSolrProductSearchFacadeImplTest {

    @Mock
    private ProductSearchService<SolrSearchQueryData, SearchResultValueData, ProductCategorySearchPageData<SolrSearchQueryData, SearchResultValueData, CategoryModel>> productSearchService;

    private ThreadContextService threadContextService = new DefaultThreadContextService();

    @Mock
    private Converter<SearchQueryData, SolrSearchQueryData> searchQueryDecoder;

    @Mock
    private Converter<ProductCategorySearchPageData<SolrSearchQueryData, SearchResultValueData, CategoryModel>, ProductCategorySearchPageData<SearchStateData, ProductData, CategoryData>> productCategorySearchPageConverter;

    @Spy
    @InjectMocks
    VMKSolrProductSearchFacadeImpl vmkSolrProductSearchFacade;

    @Captor
    private ArgumentCaptor<VariantSearchStateData> searchStateDataArgumentCaptor;

    @Captor
    private ArgumentCaptor<PageableData> pageableDataArgumentCaptor;

    @Captor
    ArgumentCaptor<SearchQueryData> searchQueryDataArgumentCaptor;

    @Before
    public void setup() {
        vmkSolrProductSearchFacade.setThreadContextService(threadContextService);
        vmkSolrProductSearchFacade.setSearchQueryDecoder(searchQueryDecoder);
        vmkSolrProductSearchFacade.setProductCategorySearchPageConverter(productCategorySearchPageConverter);
    }

    @Test
    public void testProductSearch_noCategoryCode() {
        String searchQuery = "query";
        String baseProduct = "BP_123456";

        VariantSearchStateData variantSearchStateData = new VariantSearchStateDataBuilder()
                .withSearchQuery(searchQuery)
                .withFilterQuery("baseProductCode", baseProduct, FilterQueryOperator.AND)
                .withContext(SearchQueryContext.PDP)
                .build();

        vmkSolrProductSearchFacade.searchSolrProductVariants(variantSearchStateData);

        verify(vmkSolrProductSearchFacade).textSearch(searchStateDataArgumentCaptor.capture(), pageableDataArgumentCaptor.capture());
        assertEquals(SearchQueryContext.PDP, searchStateDataArgumentCaptor.getValue().getQuery().getSearchQueryContext());
        assertEquals(searchQuery, searchStateDataArgumentCaptor.getValue().getQuery().getValue());
        assertEquals(Set.of(baseProduct), searchStateDataArgumentCaptor.getValue().getQuery().getFilterQueries().get(0).getValues());
        assertEquals(FilterQueryOperator.AND, searchStateDataArgumentCaptor.getValue().getQuery().getFilterQueries().get(0).getOperator());
        assertEquals("baseProductCode", searchStateDataArgumentCaptor.getValue().getQuery().getFilterQueries().get(0).getKey());
    }

    @Test
    public void testProductSearch_withCategoryCode() {
        // given
        String searchQuery = "query";
        String baseProduct = "BP_123456";
        String categoryCode = "01-02-02";

        VariantSearchStateData variantSearchStateData = new VariantSearchStateDataBuilder()
                .withSearchQuery(searchQuery)
                .withFilterQuery("baseProductCode", baseProduct, FilterQueryOperator.AND)
                .withContext(SearchQueryContext.PDP)
                .withCategoryCode(categoryCode)
                .build();

        SolrSearchQueryData solrSearchQueryData = mock(SolrSearchQueryData.class);
        when(searchQueryDecoder.convert(variantSearchStateData.getQuery())).thenReturn(solrSearchQueryData);

        // when
        vmkSolrProductSearchFacade.searchSolrProductVariants(variantSearchStateData);

        // then
        verify(searchQueryDecoder).convert(searchQueryDataArgumentCaptor.capture());
        assertEquals(searchQuery, searchQueryDataArgumentCaptor.getValue().getValue());
        assertEquals(Set.of(baseProduct), searchQueryDataArgumentCaptor.getValue().getFilterQueries().get(0).getValues());
        assertEquals(FilterQueryOperator.AND, searchQueryDataArgumentCaptor.getValue().getFilterQueries().get(0).getOperator());
        assertEquals("baseProductCode", searchQueryDataArgumentCaptor.getValue().getFilterQueries().get(0).getKey());
        verify(solrSearchQueryData).setCategoryCode(categoryCode);
    }
}