package com.vanmarcke.facades.product.converters.populators;

import com.vanmarcke.services.product.price.VMKNetPriceLookupStrategy;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.product.data.VariantOptionData;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.variants.model.VariantProductModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;

import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKProductVariantPricePopulatorTest {

    @Mock
    private CommonI18NService commonI18NService;
    @Mock
    private PriceDataFactory priceDataFactory;
    @Mock
    private VMKNetPriceLookupStrategy blueNetPriceLookupStrategy;

    @InjectMocks
    private VMKProductVariantPricePopulator blueProductVariantPricePopulator;

    @Test
    public void testPopulateWhenPriceIsNull() {
        final VariantProductModel source = mock(VariantProductModel.class);
        final VariantOptionData target = mock(VariantOptionData.class);

        when(this.blueNetPriceLookupStrategy.getNetPriceForProduct(source)).thenReturn(null);
        this.blueProductVariantPricePopulator.doPopulate(source, target);

        verifyZeroInteractions(this.commonI18NService);
        verifyZeroInteractions(this.priceDataFactory);
    }

    @Test
    public void testDoPopulate() {
        final VariantProductModel source = mock(VariantProductModel.class);
        final VariantOptionData target = mock(VariantOptionData.class);
        final PriceData priceData = mock(PriceData.class);
        final CurrencyModel currencyModel = mock(CurrencyModel.class);

        when(this.blueNetPriceLookupStrategy.getNetPriceForProduct(source)).thenReturn(BigDecimal.TEN);
        when(this.commonI18NService.getCurrentCurrency()).thenReturn(currencyModel);
        when(this.commonI18NService.getCurrentCurrency().getIsocode()).thenReturn("EUR");
        when(this.priceDataFactory.create(PriceDataType.BUY, BigDecimal.TEN, "EUR")).thenReturn(priceData);
        this.blueProductVariantPricePopulator.doPopulate(source, target);

        verify(target).setNetPriceData(priceData);
    }
}