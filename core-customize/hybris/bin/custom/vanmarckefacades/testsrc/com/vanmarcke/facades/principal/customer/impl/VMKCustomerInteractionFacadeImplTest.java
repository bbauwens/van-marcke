package com.vanmarcke.facades.principal.customer.impl;

import com.vanmarcke.core.enums.CustomerInteractionStatus;
import com.vanmarcke.core.model.CustomerInteractionModel;
import com.vanmarcke.core.model.OrderCustomerInteractionModel;
import com.vanmarcke.facades.customerinteraction.data.CustomerInteractionData;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.model.ModelService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKCustomerInteractionFacadeImplTest {

    @Mock
    private ModelService modelService;
    @Mock
    private Converter<CustomerInteractionData, CustomerInteractionModel> blueCustomerInteractionReverseConverter;
    @Mock
    private EventService eventService;

    @InjectMocks
    private VMKCustomerInteractionFacadeImpl vmkCustomerInteractionFacade;

    @Test
    public void testTriggerCustomerInteractionMail() {
        final CustomerInteractionData customerInteractionData = mock(CustomerInteractionData.class);
        final OrderCustomerInteractionModel customerInteraction = mock(OrderCustomerInteractionModel.class);

        when(modelService.create(OrderCustomerInteractionModel.class)).thenReturn(customerInteraction);

        vmkCustomerInteractionFacade.submitOrderCustomerInteraction(customerInteractionData);

        verify(blueCustomerInteractionReverseConverter).convert(customerInteractionData, customerInteraction);
        verify(customerInteraction).setStatus(CustomerInteractionStatus.CREATED);
        verify(modelService).save(customerInteraction);
    }
}