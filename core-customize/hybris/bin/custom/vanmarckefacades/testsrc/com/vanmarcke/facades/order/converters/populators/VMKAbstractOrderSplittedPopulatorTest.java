package com.vanmarcke.facades.order.converters.populators;

import com.vanmarcke.core.model.IBMOrderModel;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.IBMOrderData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKAbstractOrderSplittedPopulatorTest {

    @Captor
    ArgumentCaptor<List<IBMOrderData>> ibmSubordersCaptor;

    @InjectMocks
    VMKAbstractOrderSplittedPopulator<OrderModel, OrderData> vmkAbstractOrderSplittedPopulator;

    @Test
    public void testPopulate() {
        // given
        OrderModel orderModel = mock(OrderModel.class);
        OrderData orderData = mock(OrderData.class);
        IBMOrderModel ibmOrderModel1 = mock(IBMOrderModel.class);
        IBMOrderModel ibmOrderModel2 = mock(IBMOrderModel.class);
        OrderEntryModel orderEntry1 = mock(OrderEntryModel.class);
        OrderEntryModel orderEntry2 = mock(OrderEntryModel.class);
        ProductModel productModel1 = mock(ProductModel.class);
        ProductModel productModel2 = mock(ProductModel.class);
        ProductData productData1 = mock(ProductData.class);
        ProductData productData2 = mock(ProductData.class);
        OrderEntryData orderEntryData1 = mock(OrderEntryData.class);
        OrderEntryData orderEntryData2 = mock(OrderEntryData.class);
        when(ibmOrderModel1.getCode()).thenReturn("suborder1");
        when(ibmOrderModel2.getCode()).thenReturn("suborder2");
        when(orderModel.getIbmOrders()).thenReturn(Set.of(ibmOrderModel1, ibmOrderModel2));
        when(orderModel.getIsSplitted()).thenReturn(true);
        when(orderModel.getEntries()).thenReturn(List.of(orderEntry1, orderEntry2));
        when(orderEntry1.getIbmOrder()).thenReturn(ibmOrderModel1);
        when(orderEntry2.getIbmOrder()).thenReturn(ibmOrderModel2);
        when(orderData.getEntries()).thenReturn(List.of(orderEntryData1, orderEntryData2));
        when(productModel1.getCode()).thenReturn("product_code_1");
        when(productModel2.getCode()).thenReturn("product_code_2");
        when(productData1.getCode()).thenReturn("product_code_1");
        when(productData2.getCode()).thenReturn("product_code_2");
        when(orderEntry1.getProduct()).thenReturn(productModel1);
        when(orderEntry2.getProduct()).thenReturn(productModel2);
        when(orderEntryData1.getProduct()).thenReturn(productData1);
        when(orderEntryData2.getProduct()).thenReturn(productData2);

        // when
        vmkAbstractOrderSplittedPopulator.populate(orderModel, orderData);

        // then
        verify(orderData).setIbmSubOrders(ibmSubordersCaptor.capture());
        assertEquals(2, ibmSubordersCaptor.getValue().size());
        assertEquals(List.of(orderEntryData1), ibmSubordersCaptor.getValue().get(0).getOrderEntries());
        assertEquals(List.of(orderEntryData2), ibmSubordersCaptor.getValue().get(1).getOrderEntries());
        assertEquals("suborder1", ibmSubordersCaptor.getValue().get(0).getCode());
        assertEquals("suborder2", ibmSubordersCaptor.getValue().get(1).getCode());
    }
}