package com.vanmarcke.facades.order.converters.populators;

import com.vanmarcke.facades.data.order.SavedCartData;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Date;
import java.util.List;

import static org.mockito.Mockito.*;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKSavedCartDataPopulatorTest {

    @Mock
    private Populator<AbstractOrderEntryModel, OrderEntryData> orderEntryPopulator;

    @Mock
    private Populator<AbstractOrderEntryModel, OrderEntryData> blueOrderEntryPricePopulator;

    @InjectMocks
    private VMKSavedCartDataPopulator savedCartDataPopulator;

    @Captor
    ArgumentCaptor<AbstractOrderEntryModel> orderParameterCaptor;

    private static final String CART_GUID = "test_guid";
    @Test
    public void testPopulate() {
        Date modifiedTime = new Date();
        CartModel cartModel = mock(CartModel.class);
        OrderEntryModel orderEntryModel1 = mock(OrderEntryModel.class);
        OrderEntryModel orderEntryModel2 = mock(OrderEntryModel.class);
        ProductModel product1 = mock(ProductModel.class);
        ProductModel product2 = mock(ProductModel.class);

        when(orderEntryModel1.getProduct()).thenReturn(product1);
        when(orderEntryModel2.getProduct()).thenReturn(product2);
        when(product1.getTech_data_sheet()).thenReturn(mock(MediaModel.class));

        when(cartModel.getGuid()).thenReturn(CART_GUID);
        when(cartModel.getModifiedtime()).thenReturn(modifiedTime);
        when(cartModel.getEntries()).thenReturn(List.of(orderEntryModel1, orderEntryModel2));
        when(orderEntryModel1.getQuantity()).thenReturn(Long.valueOf(1));
        when(orderEntryModel2.getQuantity()).thenReturn(Long.valueOf(2));

        SavedCartData savedCartData = new SavedCartData();

        savedCartDataPopulator.populate(cartModel, savedCartData);

        Assert.assertEquals(CART_GUID, savedCartData.getName());
        Assert.assertTrue(savedCartData.getTechnicalSheetsAvailable());
        Assert.assertEquals(Long.valueOf(3), savedCartData.getQuantity());
        Assert.assertEquals(modifiedTime, savedCartData.getSaveTime());
        verify(orderEntryPopulator, times(2)).populate(orderParameterCaptor.capture(), any(OrderEntryData.class));
        verify(blueOrderEntryPricePopulator, times(2)).populate(orderParameterCaptor.capture(), any(OrderEntryData.class));
        Assert.assertEquals(orderEntryModel1, orderParameterCaptor.getAllValues().get(0));
        Assert.assertEquals(orderEntryModel2, orderParameterCaptor.getAllValues().get(1));
    }
}