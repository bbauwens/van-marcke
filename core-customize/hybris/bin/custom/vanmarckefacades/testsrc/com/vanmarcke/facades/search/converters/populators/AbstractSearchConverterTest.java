package com.vanmarcke.facades.search.converters.populators;

import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import de.hybris.platform.solrfacetsearch.search.impl.SolrResult;
import org.apache.solr.common.SolrDocument;
import org.mockito.Mock;

import static java.util.Collections.emptyMap;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public abstract class AbstractSearchConverterTest {

    @Mock
    protected SolrDocument document;

    protected SolrResult mockSorlResult() {
        SolrResult source = mock(SolrResult.class);
        SearchQuery query = mock(SearchQuery.class);
        IndexedType indexedType = mock(IndexedType.class);

        when(source.getQuery()).thenReturn(query);

        when(query.getIndexedType()).thenReturn(indexedType);

        when(indexedType.getIndexedProperties()).thenReturn(emptyMap());

        when(source.getDocument()).thenReturn(document);

        return source;
    }
}