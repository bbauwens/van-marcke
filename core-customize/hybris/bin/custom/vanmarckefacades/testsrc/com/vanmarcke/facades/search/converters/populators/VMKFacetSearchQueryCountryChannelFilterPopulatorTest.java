package com.vanmarcke.facades.search.converters.populators;

import com.vanmarcke.core.model.CommerceGroupModel;
import com.vanmarcke.services.commercegroup.VMKCommerceGroupService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SearchQueryPageableData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchRequest;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKFacetSearchQueryCountryChannelFilterPopulatorTest {

    @Mock
    private VMKCommerceGroupService commerceGroupService;
    @Mock
    private CMSSiteService cmsSiteService;

    @InjectMocks
    private VMKFacetSearchQueryCountryChannelFilterPopulator populator;

    @Test
    public void testPopulate_withoutCommerceGroup() {
        SearchQueryPageableData source = mock(SearchQueryPageableData.class);
        SolrSearchRequest target = mock(SolrSearchRequest.class);

        populator.populate(source, target);

        verifyZeroInteractions(target);
    }

    @Test
    public void testPopulate() {
        SearchQueryPageableData source = mock(SearchQueryPageableData.class);
        SolrSearchRequest target = mock(SolrSearchRequest.class);
        SearchQuery searchQuery = mock(SearchQuery.class);
        CMSSiteModel cmsSiteModel = mock(CMSSiteModel.class);
        CommerceGroupModel commerceGroupModel = mock(CommerceGroupModel.class);
        CountryModel countryModel = mock(CountryModel.class);

        when(commerceGroupModel.getCountry()).thenReturn(countryModel);

        when(countryModel.getIsocode()).thenReturn("BE");

        when(cmsSiteModel.getChannel()).thenReturn(SiteChannel.B2B);

        when(commerceGroupService.getCurrentCommerceGroup()).thenReturn(commerceGroupModel);

        when(cmsSiteService.getCurrentSite()).thenReturn(cmsSiteModel);

        when(target.getSearchQuery()).thenReturn(searchQuery);

        populator.populate(source, target);

        verify(searchQuery).addFilterQuery("available_be_b2b_boolean", "true");
    }
}