package com.vanmarcke.facades.principal.customer.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

/**
 * The {@link VMKCustomerBillingAddressPopulatorTest} class contains the unit tests for the {@link VMKCustomerBillingAddressPopulator} class.
 *
 * @author Christiaan Janssen
 * @since 13-01-2021
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKCustomerBillingAddressPopulatorTest {

    @Mock
    private Converter<AddressModel, AddressData> addressConverter;

    @InjectMocks
    private VMKCustomerBillingAddressPopulator customerPopulator;

    @Test
    public void testPopulate_withRegularCustomer() {
        // given
        final CustomerModel customerModel = mock(CustomerModel.class);

        final CustomerData customerData = new CustomerData();

        // when
        customerPopulator.populate(customerModel, customerData);

        // then
        assertThat(customerData.getDefaultBillingAddress()).isNull();

        verify(addressConverter, never()).convert(any(AddressModel.class));
    }

    @Test
    public void testPopulate_withB2BCustomer_withoutDefaultB2BUnit() {
        // given
        final B2BCustomerModel b2bCustomerModel = mock(B2BCustomerModel.class);

        final CustomerData customerData = new CustomerData();

        // when
        customerPopulator.populate(b2bCustomerModel, customerData);

        // then
        assertThat(customerData.getDefaultBillingAddress()).isNull();

        verify(addressConverter, never()).convert(any(AddressModel.class));
    }

    @Test
    public void testPopulate_withB2BCustomer_withDefaultB2BUnit_withoutBillingAddress() {
        // given
        final B2BUnitModel b2bUnit = mock(B2BUnitModel.class);

        final B2BCustomerModel b2bCustomerModel = mock(B2BCustomerModel.class);
        when(b2bCustomerModel.getDefaultB2BUnit()).thenReturn(b2bUnit);

        final CustomerData customerData = new CustomerData();

        // when
        customerPopulator.populate(b2bCustomerModel, customerData);

        // then
        assertThat(customerData.getDefaultBillingAddress()).isNull();

        verify(addressConverter, never()).convert(any(AddressModel.class));
    }

    @Test
    public void testPopulate_withB2BCustomer_withDefaultB2BUnit_withBillingAddress() {
        // given
        final AddressModel addressModel = mock(AddressModel.class);

        final AddressData expectedAddressData = new AddressData();

        when(addressConverter.convert(addressModel)).thenReturn(expectedAddressData);

        final B2BUnitModel b2bUnit = mock(B2BUnitModel.class);
        when(b2bUnit.getBillingAddress()).thenReturn(addressModel);

        final B2BCustomerModel b2bCustomerModel = mock(B2BCustomerModel.class);
        when(b2bCustomerModel.getDefaultB2BUnit()).thenReturn(b2bUnit);

        final CustomerData customerData = new CustomerData();

        // when
        customerPopulator.populate(b2bCustomerModel, customerData);

        // then
        assertThat(customerData.getDefaultBillingAddress()).isEqualTo(expectedAddressData);

        verify(addressConverter).convert(addressModel);
    }
}