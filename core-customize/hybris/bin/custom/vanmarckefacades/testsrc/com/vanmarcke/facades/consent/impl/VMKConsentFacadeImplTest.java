package com.vanmarcke.facades.consent.impl;

import com.vanmarcke.core.enums.ConsentType;
import com.vanmarcke.core.enums.OptType;
import com.vanmarcke.core.model.VMKConsentModel;
import com.vanmarcke.facades.data.VMKConsentData;
import com.vanmarcke.services.consent.VMKConsentService;
import com.vanmarcke.services.model.builder.VMKConsentModelMockBuilder;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.assertj.core.util.Lists;
import org.fest.assertions.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKConsentFacadeImplTest {

    @Mock
    private VMKConsentService consentService;

    @Mock
    private Converter<VMKConsentData, VMKConsentModel> consentReverseConverter;

    @Mock
    private Converter<VMKConsentModel, VMKConsentData> consentConverter;

    @InjectMocks
    private VMKConsentFacadeImpl consentFacade;

    @Before
    public void setUp() {
        consentFacade = new VMKConsentFacadeImpl(consentService, consentReverseConverter, consentConverter);
    }

    @Test
    public void testProcess() {
        VMKConsentData consentData = new VMKConsentData();
        HttpServletRequest request = mock(HttpServletRequest.class);

        VMKConsentModel consentModel = VMKConsentModelMockBuilder
                .aConsent()
                .build();

        when(consentReverseConverter.convertAll(Collections.singletonList(consentData))).thenReturn(Collections.singletonList(consentModel));

        consentFacade.process(Collections.singletonList(consentData), request);

        verify(consentReverseConverter).convertAll(Collections.singletonList(consentData));
        verify(consentService).processConsents(Collections.singletonList(consentModel), request);
    }

    @Test
    public void testGetConsentsAsString() {
        VMKConsentModel consentModel = VMKConsentModelMockBuilder
                .aConsent()
                .build();

        when(consentService.getConsents()).thenReturn(Collections.singletonList(consentModel));

        VMKConsentData expectedConsentData = new VMKConsentData();
        expectedConsentData.setConsentType(ConsentType.ANALYTICAL_COOKIES);
        expectedConsentData.setOptType(OptType.OPT_OUT);

        when(consentConverter.convertAll(Collections.singletonList(consentModel))).thenReturn(Collections.singletonList(expectedConsentData));

        String actualConsents = consentFacade.getConsentsAsString();

        Assertions
                .assertThat(actualConsents)
                .isEqualTo("[{\"consentType\":\"ANALYTICAL_COOKIES\",\"optType\":\"OPT_OUT\"}]");

        verify(consentService).getConsents();
        verify(consentConverter).convertAll(Collections.singletonList(consentModel));
    }

    @Test
    public void testGetConsents() {
        VMKConsentModel consentModel = VMKConsentModelMockBuilder
                .aConsent()
                .build();

        when(consentService.getConsents()).thenReturn(Collections.singletonList(consentModel));

        VMKConsentData expectedConsentData = new VMKConsentData();

        when(consentConverter.convertAll(Collections.singletonList(consentModel))).thenReturn(Collections.singletonList(expectedConsentData));

        List<VMKConsentData> actualConsents = consentFacade.getConsents();

        Assertions
                .assertThat(actualConsents)
                .containsExactly(expectedConsentData);

        verify(consentService).getConsents();
        verify(consentConverter).convertAll(Collections.singletonList(consentModel));
    }

    @Test
    public void testGetConsentTypes() {
        when(consentService.getConsentTypes()).thenReturn(Lists.newArrayList(ConsentType.ANALYTICAL_COOKIES.getCode(), ConsentType.FUNCTIONAL_COOKIES.getCode()));

        List<String> actualConsentTypes = consentFacade.getConsentTypes();

        Assertions
                .assertThat(actualConsentTypes)
                .contains(ConsentType.ANALYTICAL_COOKIES.getCode(), ConsentType.FUNCTIONAL_COOKIES.getCode());

        verify(consentService).getConsentTypes();
    }
}