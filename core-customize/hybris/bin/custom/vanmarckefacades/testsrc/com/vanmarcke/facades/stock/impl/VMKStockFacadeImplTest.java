package com.vanmarcke.facades.stock.impl;

import com.vanmarcke.facades.product.data.StockListData;
import com.vanmarcke.services.basestore.VMKBaseStoreService;
import com.vanmarcke.services.builder.ProductModelBuilder;
import com.vanmarcke.services.factories.VMKStockServiceFactory;
import com.vanmarcke.services.product.VMKStockService;
import com.vanmarcke.services.product.VMKVariantProductService;
import com.vanmarcke.services.storelocator.pos.VMKPointOfServiceService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordersplitting.WarehouseService;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.stock.impl.StockLevelDao;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import de.hybris.platform.variants.model.VariantProductModel;
import org.apache.commons.lang.RandomStringUtils;
import org.fest.assertions.Assertions;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;

import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKStockFacadeImplTest {

    private static final String STORE_ID = RandomStringUtils.randomAlphabetic(10);
    private static final String SKU1 = RandomStringUtils.randomAlphabetic(10);

    @Mock
    private ProductService productService;

    @Mock
    private VMKStockServiceFactory stockServiceFactory;

    @Mock
    private VMKPointOfServiceService pointOfServiceService;

    @Mock
    private VMKBaseStoreService baseStoreService;

    @Mock
    private VMKStockService stockService;

    @Mock
    private VMKVariantProductService variantProductService;

    @InjectMocks
    private VMKStockFacadeImpl stockFacade;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void testGetProductDataWithStockForCurrentWareHouse() {
        VariantProductModel product = mock(VariantProductModel.class);
        StockListData stockListData = new StockListData();
        PointOfServiceModel pos = mock(PointOfServiceModel.class);

        when(pointOfServiceService.getCurrentPointOfService()).thenReturn(pos);
        when(pos.getName()).thenReturn(STORE_ID);

        when(productService.getProductForCode(SKU1)).thenReturn(product);
        when(stockServiceFactory.getStockService()).thenReturn(stockService);
        when(stockService.getStockForProductAndWarehouses(product, new String[]{STORE_ID}))
                .thenReturn(stockListData);

        ProductData result = stockFacade.getProductDataWithStockForCurrentWareHouse(SKU1);

        Assertions
                .assertThat(result.getCode())
                .isEqualTo(SKU1);

        Assertions
                .assertThat(result.getStockListData())
                .isEqualTo(stockListData);

        verify(stockServiceFactory).getStockService();
        verify(stockService).getStockForProductAndWarehouses(product, new String[]{STORE_ID});
        verify(productService).getProductForCode(SKU1);
        verify(pointOfServiceService).getCurrentPointOfService();
        verifyZeroInteractions(baseStoreService);
    }

    @Test
    public void testGetProductDataWithStockForCurrentWareHouse_notAVariant() {
        ProductModel product = mock(ProductModel.class);

        when(productService.getProductForCode(SKU1)).thenReturn(product);

        ProductData result = stockFacade.getProductDataWithStockForCurrentWareHouse(SKU1);

        Assertions
                .assertThat(result)
                .isNull();

        verify(productService).getProductForCode(SKU1);
        verifyZeroInteractions(stockService, stockServiceFactory, baseStoreService, pointOfServiceService);
    }

    @Test
    public void testGetStockForProductCodeAndCurrentBaseStore() {
        ProductModel product = mock(ProductModel.class);
        BaseStoreModel baseStore = mock(BaseStoreModel.class);
        PointOfServiceModel pos = mock(PointOfServiceModel.class);
        StockListData stockListData = new StockListData();

        when(baseStore.getPointsOfService()).thenReturn(Collections.singletonList(pos));
        when(pos.getName()).thenReturn(STORE_ID);
        when(baseStoreService.getCurrentBaseStore()).thenReturn(baseStore);

        when(productService.getProductForCode(SKU1)).thenReturn(product);
        when(stockServiceFactory.getStockService()).thenReturn(stockService);
        when(stockService.getStockForProductAndWarehouses(product, new String[]{STORE_ID}))
                .thenReturn(stockListData);

        StockListData result = stockFacade.getStockForProductCodeAndCurrentBaseStore(SKU1);

        Assertions
                .assertThat(result)
                .isEqualTo(stockListData);

        verify(productService).getProductForCode(SKU1);
        verify(baseStoreService).getCurrentBaseStore();
        verify(stockServiceFactory).getStockService();
        verify(stockService).getStockForProductAndWarehouses(product, new String[]{STORE_ID});
        verifyZeroInteractions(pointOfServiceService);
    }

    @Test
    public void testGetStockForProductCodeAndWarehouse() {
        ProductModel product = mock(ProductModel.class);
        StockListData stockListData = new StockListData();

        when(stockServiceFactory.getStockService()).thenReturn(stockService);
        when(stockService.getStockForProductAndWarehouses(product, new String[]{STORE_ID}))
                .thenReturn(stockListData);

        when(productService.getProductForCode(SKU1)).thenReturn(product);
        when(product.getCode()).thenReturn(SKU1);

        Assertions
                .assertThat(stockFacade.getStockForProductCodeAndWarehouse(SKU1, STORE_ID))
                .isEqualTo(stockListData);

        verify(stockServiceFactory).getStockService();
        verify(stockService).getStockForProductAndWarehouses(product, new String[]{STORE_ID});
        verifyZeroInteractions(baseStoreService, pointOfServiceService);
    }


    @Test
    public void testGetStockForProductCodeAndCurrentBaseStore_noProductId() {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Product code cannot be null.");
        stockFacade.getStockForProductCodeAndCurrentBaseStore(null);
    }

    @Test
    public void testGetStockForProductCodeAndWarehouse_productNull() {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Product code cannot be null.");
        stockFacade.getStockForProductCodeAndWarehouse(null, null);
    }

    @Test
    public void testGetStockForProductCodeAndWarehouse_WarehouseNull() {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Warehouse cannot be null.");
        stockFacade.getStockForProductCodeAndWarehouse("", null);
    }

    @Test
    public void testGetProductDataWithStockForCurrentWareHouse_productCodeNull() {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Product code cannot be null.");
        stockFacade.getProductDataWithStockForCurrentWareHouse(null);
    }

    @Test
    public void testGetAvailableEDCStock() {
        ProductModel product = ProductModelBuilder.aProductModel()
                .build();
        when(variantProductService.getAvailableEDCStock(product)).thenReturn(100L);
        when(productService.getProductForCode("product_code")).thenReturn(product);
        Assert.assertEquals(100L, stockFacade.getAvailableEDCStock("product_code"));
    }
}

