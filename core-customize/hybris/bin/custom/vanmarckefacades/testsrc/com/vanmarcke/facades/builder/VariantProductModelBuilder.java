package com.vanmarcke.facades.builder;

import de.hybris.platform.variants.model.VariantProductModel;

/**
 * Builder for the {@link VariantProductModel}.
 *
 * @author Tom van den Berg
 * @since 11-3-2020
 */
public class VariantProductModelBuilder {

    /**
     * Hides the default implicit constructor.
     */
    private VariantProductModelBuilder() {
    }

    /**
     * The entry point for the {@code VariantProductModelBuilder}.
     *
     * @return a {@code VariantProductModelBuilder} instance for method chaining
     */
    public static VariantProductModelBuilder aVariantProductModel() {
        return new VariantProductModelBuilder();
    }

    /**
     * Builds and returns a {@code VariantProductModel} instance.
     *
     * @return the variant product model instance
     */
    public VariantProductModel build() {
        VariantProductModel productData = new VariantProductModel();
        return productData;
    }
}
