package com.vanmarcke.facades.product.converters.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeValueModel;
import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.classification.ClassificationService;
import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.classification.features.FeatureList;
import de.hybris.platform.classification.features.FeatureValue;
import de.hybris.platform.commercefacades.product.data.FeatureData;
import de.hybris.platform.commercefacades.product.data.VariantOptionData;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.variants.model.VariantProductModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKProductVariantDefiningAttributesPopulatorTest {

    @Mock
    private ClassificationService classificationService;
    @Mock
    private Converter<Feature, FeatureData> featureConverter;

    @InjectMocks
    private VMKProductVariantDefiningAttributesPopulator blueProductVariantDefiningAttributesPopulator;

    @Test
    public void testDoPopulate() {
        final VariantProductModel source = mock(VariantProductModel.class);
        final VariantOptionData target = new VariantOptionData();
        final FeatureList productFeatures = mock(FeatureList.class);
        final Feature feature = mock(Feature.class);
        final List<Feature> features = new ArrayList<>();
        features.add(feature);
        final List<ClassificationClassModel> classificationClasses = new ArrayList<>();
        final ClassificationClassModel classificationClass = mock(ClassificationClassModel.class);
        classificationClasses.add(classificationClass);
        final List<ClassAttributeAssignmentModel> definingAttributes = new ArrayList<>();
        final ClassAttributeAssignmentModel classAttributeAssignment = mock(ClassAttributeAssignmentModel.class);
        definingAttributes.add(classAttributeAssignment);
        final ClassificationAttributeModel classificationAttribute = mock(ClassificationAttributeModel.class);
        final ClassificationAttributeModel definingClassificationAttribute = mock(ClassificationAttributeModel.class);
        final List<FeatureValue> featureValues = new ArrayList<>();
        final FeatureValue featureValue = mock(FeatureValue.class);
        featureValues.add(featureValue);
        final ClassificationAttributeValueModel classificationAttributeValue = mock(ClassificationAttributeValueModel.class);
        final FeatureData featureData = mock(FeatureData.class);

        when(this.classificationService.getFeatures(source)).thenReturn(productFeatures);
        when(productFeatures.getFeatures()).thenReturn(features);
        when(source.getClassificationClasses()).thenReturn(classificationClasses);
        when(classificationClass.getDefiningClassificationAttributeAssignments()).thenReturn(definingAttributes);
        when(classAttributeAssignment.getClassificationAttribute()).thenReturn(classificationAttribute);
        when(classificationAttribute.getCode()).thenReturn("CODE");
        when(feature.getClassAttributeAssignment()).thenReturn(classAttributeAssignment);
        when(classAttributeAssignment.getClassificationAttribute()).thenReturn(definingClassificationAttribute);
        when(definingClassificationAttribute.getCode()).thenReturn("CODE");
        when(feature.getValues()).thenReturn(featureValues);
        when(featureValue.getValue()).thenReturn(classificationAttributeValue);
        when(this.featureConverter.convert(feature)).thenReturn(featureData);
        this.blueProductVariantDefiningAttributesPopulator.doPopulate(source, target);

        assertThat(target.getDefiningAttributes()).isNotEmpty().hasSize(1);
    }
}