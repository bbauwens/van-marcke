package com.vanmarcke.facades.paymentmodes.converters.populators;

import com.vanmarcke.facades.payment.data.PaymentModeData;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.order.payment.PaymentModeModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKPaymentModePopulatorTest {

    @Mock
    Converter<MediaModel, ImageData> imageConverter;

    @InjectMocks
    private VMKPaymentModePopulator paymentModePopulator;

    @Test
    public void testPopulate() {
        final PaymentModeModel source = mock(PaymentModeModel.class);
        final PaymentModeData target = new PaymentModeData();
        final MediaModel mediaModel = mock(MediaModel.class);
        final ImageData imageData = mock(ImageData.class);

        when(source.getCode()).thenReturn("CODE");
        when(source.getName()).thenReturn("NAME");
        when(source.getDescription()).thenReturn("DESCRIPTION");
        when(source.getThumbnail()).thenReturn(mediaModel);
        when(this.imageConverter.convert(mediaModel)).thenReturn(imageData);
        this.paymentModePopulator.populate(source, target);

        assertThat(target.getCode()).isEqualTo("CODE");
        assertThat(target.getName()).isEqualTo("NAME");
        assertThat(target.getDescription()).isEqualTo("DESCRIPTION");
        assertThat(target.getThumbnail()).isEqualTo(imageData);
    }
}