package com.vanmarcke.facades.cart.converters.populators;

import com.vanmarcke.facades.analytics.data.AnalyticsData;
import com.vanmarcke.facades.analytics.data.CartAnalyticsData;
import com.vanmarcke.services.model.builder.ProductModelMockBuilder;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import org.apache.commons.lang3.LocaleUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.fest.assertions.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.Locale;

/**
 * The {@code VMKCartAnalyticsPopulatorTest} class contains the unit tests for the
 * {@link VMKCartAnalyticsPopulator} class.
 *
 * @author Christiaan Janssen
 * @since 30-04-2020
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKCartAnalyticsPopulatorTest {

    private static final String CART_CODE = RandomStringUtils.random(10);

    private static final Long QUANTITY = RandomUtils.nextLong();

    private static final String PRODUCT_CODE = RandomStringUtils.random(10);
    private static final String PRODUCT_NAME = RandomStringUtils.random(10);

    private static final BigDecimal PRICE = BigDecimal.valueOf(RandomUtils.nextDouble());

    private static final Locale LOCALE = LocaleUtils.toLocale("nl_BE");

    @InjectMocks
    private VMKCartAnalyticsPopulator populator;

    @Test
    public void testPopulateCartCode() {
        CartModificationData cartModificationData = new CartModificationData();
        cartModificationData.setCartCode(CART_CODE);

        CartAnalyticsData cartAnalyticsData = new CartAnalyticsData();

        populator.populate(cartModificationData, cartAnalyticsData);

        Assertions
                .assertThat(cartAnalyticsData.getCartCode())
                .isEqualTo(CART_CODE);

        Assertions
                .assertThat(cartAnalyticsData.getQuantity())
                .isEqualTo(0L);

        Assertions
                .assertThat(cartAnalyticsData.getProductCode())
                .isNull();

        Assertions
                .assertThat(cartAnalyticsData.getProductName())
                .isNull();

        Assertions
                .assertThat(cartAnalyticsData.getProductPostPrice())
                .isNull();
    }

    @Test
    public void testPopulateQuantity() {
        CartModificationData cartModificationData = new CartModificationData();
        cartModificationData.setQuantityAdded(QUANTITY);

        CartAnalyticsData cartAnalyticsData = new CartAnalyticsData();

        populator.populate(cartModificationData, cartAnalyticsData);

        Assertions
                .assertThat(cartAnalyticsData.getCartCode())
                .isNull();

        Assertions
                .assertThat(cartAnalyticsData.getQuantity())
                .isEqualTo(QUANTITY);

        Assertions
                .assertThat(cartAnalyticsData.getProductCode())
                .isNull();

        Assertions
                .assertThat(cartAnalyticsData.getProductName())
                .isNull();

        Assertions
                .assertThat(cartAnalyticsData.getProductPostPrice())
                .isNull();
    }

    @Test
    public void testPopulateProductNameWithoutEntry() {
        CartModificationData cartModificationData = new CartModificationData();

        CartAnalyticsData cartAnalyticsData = new CartAnalyticsData();

        populator.populate(cartModificationData, cartAnalyticsData);

        Assertions
                .assertThat(cartAnalyticsData.getCartCode())
                .isNull();

        Assertions
                .assertThat(cartAnalyticsData.getQuantity())
                .isEqualTo(0L);

        Assertions
                .assertThat(cartAnalyticsData.getProductCode())
                .isNull();

        Assertions
                .assertThat(cartAnalyticsData.getProductName())
                .isNull();

        Assertions
                .assertThat(cartAnalyticsData.getProductPostPrice())
                .isNull();
    }

    @Test
    public void testPopulateProductNameWithoutProduct() {
        OrderEntryData orderEntryData = new OrderEntryData();

        CartModificationData cartModificationData = new CartModificationData();
        cartModificationData.setEntry(orderEntryData);

        CartAnalyticsData cartAnalyticsData = new CartAnalyticsData();

        populator.populate(cartModificationData, cartAnalyticsData);

        Assertions
                .assertThat(cartAnalyticsData.getCartCode())
                .isNull();

        Assertions
                .assertThat(cartAnalyticsData.getQuantity())
                .isEqualTo(0L);

        Assertions
                .assertThat(cartAnalyticsData.getProductCode())
                .isNull();

        Assertions
                .assertThat(cartAnalyticsData.getProductName())
                .isNull();

        Assertions
                .assertThat(cartAnalyticsData.getProductPostPrice())
                .isNull();
    }

    @Test
    public void testPopulateProductNameWithoutAnalyticsData() {
        ProductData productData = new ProductData();
        productData.setCode(PRODUCT_CODE);

        OrderEntryData orderEntryData = new OrderEntryData();
        orderEntryData.setProduct(productData);

        CartModificationData cartModificationData = new CartModificationData();
        cartModificationData.setEntry(orderEntryData);

        CartAnalyticsData cartAnalyticsData = new CartAnalyticsData();

        populator.populate(cartModificationData, cartAnalyticsData);

        Assertions
                .assertThat(cartAnalyticsData.getCartCode())
                .isNull();

        Assertions
                .assertThat(cartAnalyticsData.getQuantity())
                .isEqualTo(0L);

        Assertions
                .assertThat(cartAnalyticsData.getProductCode())
                .isEqualTo(PRODUCT_CODE);

        Assertions
                .assertThat(cartAnalyticsData.getProductName())
                .isNull();

        Assertions
                .assertThat(cartAnalyticsData.getProductPostPrice())
                .isNull();
    }

    @Test
    public void testPopulateProductName() {
        AnalyticsData analyticsData = new AnalyticsData();
        analyticsData.setName(PRODUCT_NAME);

        ProductData productData = new ProductData();
        productData.setCode(PRODUCT_CODE);
        productData.setAnalyticsData(analyticsData);

        OrderEntryData orderEntryData = new OrderEntryData();
        orderEntryData.setProduct(productData);

        CartModificationData cartModificationData = new CartModificationData();
        cartModificationData.setEntry(orderEntryData);

        ProductModel productModel = ProductModelMockBuilder
                .aBaseProduct(PRODUCT_CODE, LOCALE)
                .build();

        CartAnalyticsData cartAnalyticsData = new CartAnalyticsData();

        populator.populate(cartModificationData, cartAnalyticsData);

        Assertions
                .assertThat(cartAnalyticsData.getCartCode())
                .isNull();

        Assertions
                .assertThat(cartAnalyticsData.getQuantity())
                .isEqualTo(0L);

        Assertions
                .assertThat(cartAnalyticsData.getProductCode())
                .isEqualTo(PRODUCT_CODE);

        Assertions
                .assertThat(cartAnalyticsData.getProductName())
                .isEqualTo(PRODUCT_NAME);

        Assertions
                .assertThat(cartAnalyticsData.getProductPostPrice())
                .isNull();
    }

    @Test
    public void testPopulatePriceWithoutEntry() {
        PriceData priceData = new PriceData();
        priceData.setValue(PRICE);

        CartModificationData cartModificationData = new CartModificationData();

        CartAnalyticsData cartAnalyticsData = new CartAnalyticsData();

        populator.populate(cartModificationData, cartAnalyticsData);

        Assertions
                .assertThat(cartAnalyticsData.getCartCode())
                .isNull();

        Assertions
                .assertThat(cartAnalyticsData.getQuantity())
                .isEqualTo(0L);

        Assertions
                .assertThat(cartAnalyticsData.getProductCode())
                .isNull();

        Assertions
                .assertThat(cartAnalyticsData.getProductName())
                .isNull();

        Assertions
                .assertThat(cartAnalyticsData.getProductPostPrice())
                .isNull();
    }

    @Test
    public void testPopulatePriceWithoutBasePrice() {
        OrderEntryData orderEntryData = new OrderEntryData();

        CartModificationData cartModificationData = new CartModificationData();
        cartModificationData.setEntry(orderEntryData);

        CartAnalyticsData cartAnalyticsData = new CartAnalyticsData();

        populator.populate(cartModificationData, cartAnalyticsData);

        Assertions
                .assertThat(cartAnalyticsData.getCartCode())
                .isNull();

        Assertions
                .assertThat(cartAnalyticsData.getQuantity())
                .isEqualTo(0L);

        Assertions
                .assertThat(cartAnalyticsData.getProductCode())
                .isNull();

        Assertions
                .assertThat(cartAnalyticsData.getProductName())
                .isNull();

        Assertions
                .assertThat(cartAnalyticsData.getProductPostPrice())
                .isNull();
    }

    @Test
    public void testPopulatePrice() {
        PriceData priceData = new PriceData();
        priceData.setValue(PRICE);

        OrderEntryData orderEntryData = new OrderEntryData();
        orderEntryData.setBasePrice(priceData);

        CartModificationData cartModificationData = new CartModificationData();
        cartModificationData.setEntry(orderEntryData);

        CartAnalyticsData cartAnalyticsData = new CartAnalyticsData();

        populator.populate(cartModificationData, cartAnalyticsData);

        Assertions
                .assertThat(cartAnalyticsData.getCartCode())
                .isNull();

        Assertions
                .assertThat(cartAnalyticsData.getQuantity())
                .isEqualTo(0L);

        Assertions
                .assertThat(cartAnalyticsData.getProductCode())
                .isNull();

        Assertions
                .assertThat(cartAnalyticsData.getProductName())
                .isNull();

        Assertions
                .assertThat(cartAnalyticsData.getProductPostPrice())
                .isEqualTo(PRICE.doubleValue());
    }
}