package com.vanmarcke.facades.product.converters.populators;

import com.vanmarcke.services.product.VMKVariantProductService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.VariantOptionData;
import de.hybris.platform.variants.model.VariantProductModel;
import org.fest.assertions.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKProductPurchasablePopulatorTest {

    @Mock
    private VMKVariantProductService variantProductService;
    @InjectMocks
    private VMKProductPurchasablePopulator blueProductVariantStockPopulator;

    @Test
    public void testPopulateWhenPurchasableAndProductStockDataIsNotNull() {
        VariantProductModel source = new VariantProductModel();
        VariantOptionData target = new VariantOptionData();

        when(variantProductService.isPurchasable(source)).thenReturn(Boolean.TRUE);

        blueProductVariantStockPopulator.doPopulate(source, target);

        Assertions
                .assertThat(target.isPurchasable())
                .isTrue();

        verify(variantProductService).isPurchasable(source);
    }

    @Test
    public void testPopulateWhenPurchasableAndProductStockDataIsNull() {
        VariantProductModel source = new VariantProductModel();
        VariantOptionData target = new VariantOptionData();

        when(variantProductService.isPurchasable(source)).thenReturn(Boolean.TRUE);

        blueProductVariantStockPopulator.doPopulate(source, target);

        Assertions
                .assertThat(target.isPurchasable())
                .isTrue();

        verify(variantProductService).isPurchasable(source);
    }

    @Test
    public void testPopulateWhenNotPurchasable() {
        VariantProductModel source = mock(VariantProductModel.class);
        VariantOptionData target = mock(VariantOptionData.class);

        when(variantProductService.isPurchasable(source)).thenReturn(Boolean.FALSE);

        blueProductVariantStockPopulator.doPopulate(source, target);

        Assertions
                .assertThat(target.isPurchasable())
                .isFalse();

        verify(variantProductService).isPurchasable(source);
    }
}