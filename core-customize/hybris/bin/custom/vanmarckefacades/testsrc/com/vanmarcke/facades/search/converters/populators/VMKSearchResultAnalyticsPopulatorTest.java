package com.vanmarcke.facades.search.converters.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKSearchResultAnalyticsPopulatorTest {

    @Mock
    private SearchResultValueData source;
    private ProductData productData;
    @InjectMocks
    private VMKSearchResultAnalyticsPopulator vmkSearchResultAnalyticsPopulator;

    @Before
    public void setUp() {
        productData = new ProductData();
    }

    @Test
    public void testPopulateWhenNoResult() {
        vmkSearchResultAnalyticsPopulator.populate(source, productData);
        assertThat(productData.getAnalyticsData().getCategory()).isNullOrEmpty();
        assertThat(productData.getAnalyticsData().getBrand()).isNullOrEmpty();
    }

    @Test
    public void testPopulateWhenNearestCategory() {
        when(source.getValues()).thenReturn(Collections.singletonMap("nearestCategory", "categoryName"));
        vmkSearchResultAnalyticsPopulator.populate(source, productData);
        assertThat(productData.getAnalyticsData()).isNotNull();
        assertThat(productData.getAnalyticsData().getCategory()).isEqualTo("categoryName");
    }

    @Test
    public void testPopulateWhenBrandCode() {
        when(source.getValues()).thenReturn(Collections.singletonMap("brandName", "brandName"));
        vmkSearchResultAnalyticsPopulator.populate(source, productData);
        assertThat(productData.getAnalyticsData()).isNotNull();
        assertThat(productData.getAnalyticsData().getBrand()).isEqualTo("brandName");
    }
}