package com.vanmarcke.facades.order.converters.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKAbstractOrderDeliveryCommentPopulatorTest {

    @InjectMocks
    private VMKAbstractOrderDeliveryCommentPopulator blueAbstractOrderDeliveryCommentPopulator;

    @Test
    public void populate() {
        final AbstractOrderModel abstractOrderModel = mock(AbstractOrderModel.class);
        final AbstractOrderData result = new AbstractOrderData();

        when(abstractOrderModel.getDeliveryComment()).thenReturn("Hello driver!");

        this.blueAbstractOrderDeliveryCommentPopulator.populate(abstractOrderModel, result);

        assertThat(result.getDeliveryComment()).isEqualTo("Hello driver!");
    }
}