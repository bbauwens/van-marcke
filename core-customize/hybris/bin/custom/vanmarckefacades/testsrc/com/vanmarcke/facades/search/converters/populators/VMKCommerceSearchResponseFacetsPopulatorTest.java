package com.vanmarcke.facades.search.converters.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.search.facetdata.FacetData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryData;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.search.SearchResult;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKCommerceSearchResponseFacetsPopulatorTest {

    @Mock
    private FacetData<SolrSearchQueryData> facetData;
    @InjectMocks
    private MockVMKCommerceSearchResponseFacetsPopulator populator = new MockVMKCommerceSearchResponseFacetsPopulator();

    @Test
    public void testBuildFacets() {
        SearchResult solrSearchResult = mock(SearchResult.class);
        SolrSearchQueryData searchQueryData = mock(SolrSearchQueryData.class);
        IndexedType indexedType = mock(IndexedType.class);

        IndexedProperty indexedProperty = mock(IndexedProperty.class);
        Map<String, IndexedProperty> indexedProperties = new HashMap<>();
        indexedProperties.put("EF0001", indexedProperty);

        when(indexedType.getIndexedProperties()).thenReturn(indexedProperties);
        when(facetData.getCode()).thenReturn("EF0001");
        when(indexedProperty.getUnit()).thenReturn("mm");
        populator.buildFacets(solrSearchResult, searchQueryData, indexedType);

        verify(facetData).setUnitSymbol("mm");
    }

    public class MockVMKCommerceSearchResponseFacetsPopulator extends VMKCommerceSearchResponseFacetsPopulator {

        @Override
        protected List<FacetData<SolrSearchQueryData>> performSuper(SearchResult solrSearchResult, SolrSearchQueryData searchQueryData, IndexedType indexedType) {
            return Collections.singletonList(facetData);
        }
    }
}
