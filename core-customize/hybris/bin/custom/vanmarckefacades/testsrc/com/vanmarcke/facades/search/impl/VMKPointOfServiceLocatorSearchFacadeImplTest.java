package com.vanmarcke.facades.search.impl;

import com.vanmarcke.services.search.solrfacetsearch.VMKSearchService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.storelocator.data.PointOfServiceData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.store.data.GeoPoint;
import de.hybris.platform.commerceservices.storefinder.data.StoreFinderSearchPageData;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfig;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfigService;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.model.config.SolrFacetSearchConfigModel;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import de.hybris.platform.solrfacetsearch.search.SearchResult;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;

import static de.hybris.platform.solrfacetsearch.enums.ConverterType.POINT_OF_SERVICE_LOCATOR;
import static de.hybris.platform.solrfacetsearch.search.OrderField.SortOrder.ASCENDING;
import static java.util.Collections.singletonList;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKPointOfServiceLocatorSearchFacadeImplTest {

    @Mock
    private VMKSearchService searchService;
    @Mock
    private CMSSiteService cmsSiteService;
    @Mock
    private FacetSearchConfigService facetSearchConfigService;

    @InjectMocks
    private VMKPointOfServiceLocatorSearchFacadeImpl defaultVMKPointOfServiceLocatorSearchFacade;

    @Mock
    private FacetSearchConfig facetSearchConfig;
    @Mock
    private IndexedType indexedType;

    @Before
    public void setup() throws Exception {
        defaultVMKPointOfServiceLocatorSearchFacade.setConverterType(POINT_OF_SERVICE_LOCATOR);
        setupSearchConfiguration();
    }

    @Test
    public void testSearch() throws Exception {
        SearchQuery searchQuery = mock(SearchQuery.class);
        SearchResult searchResult = mock(SearchResult.class);
        PointOfServiceData pointOfServiceData = mock(PointOfServiceData.class);
        GeoPoint geoPoint = mock(GeoPoint.class);
        PageableData pageableData = mock(PageableData.class);

        when(geoPoint.getLatitude()).thenReturn(1.0);
        when(geoPoint.getLongitude()).thenReturn(2.0);

        when(pageableData.getPageSize()).thenReturn(20);
        when(pageableData.getCurrentPage()).thenReturn(1);
        when(pageableData.getSort()).thenReturn("sort");

        when(searchResult.getNumberOfResults()).thenReturn(1L);
        when(searchResult.getNumberOfPages()).thenReturn(1L);

        when(searchService.createTextSearchQuery("locationText", facetSearchConfig, indexedType)).thenReturn(searchQuery);

        when(searchService.search(searchQuery)).thenReturn(searchResult);

        when(searchResult.getResultData(POINT_OF_SERVICE_LOCATOR)).thenReturn(singletonList(pointOfServiceData));

        StoreFinderSearchPageData<PointOfServiceData> result = defaultVMKPointOfServiceLocatorSearchFacade.search("locationText", geoPoint, pageableData);
        assertThat(result.getResults()).containsExactly(pointOfServiceData);
        assertThat(result.getPagination().getPageSize()).isEqualTo(20);
        assertThat(result.getPagination().getCurrentPage()).isEqualTo(1);
        assertThat(result.getPagination().getTotalNumberOfResults()).isEqualTo(1);
        assertThat(result.getPagination().getNumberOfPages()).isEqualTo(1);
        assertThat(result.getPagination().getSort()).isEqualTo("sort");

        verify(searchQuery).setPageSize(20);
        verify(searchQuery).setOffset(1);
        verify(searchQuery).addRawParam("fq", "{!geofilt}");
        verify(searchQuery).addRawParam("spatial", "true");
        verify(searchQuery).addRawParam("sfield", "position_coordinate");
        verify(searchQuery).addRawParam("pt", "1.0,2.0");
        verify(searchQuery).addRawParam("d", "1000000");
        verify(searchQuery).addRawParam("fl", "*,distance:geodist()");
        verify(searchQuery).addSort("geodist()", ASCENDING);
    }

    @Test
    public void testSearch_withException() throws Exception {
        GeoPoint geoPoint = mock(GeoPoint.class);
        PageableData pageableData = mock(PageableData.class);

        when(geoPoint.getLatitude()).thenReturn(1.0);
        when(geoPoint.getLongitude()).thenReturn(2.0);

        when(pageableData.getPageSize()).thenReturn(20);
        when(pageableData.getCurrentPage()).thenReturn(1);
        when(pageableData.getSort()).thenReturn("sort");

        when(searchService.createTextSearchQuery("locationText", facetSearchConfig, indexedType)).thenThrow(Exception.class);

        StoreFinderSearchPageData<PointOfServiceData> result = defaultVMKPointOfServiceLocatorSearchFacade.search("locationText", geoPoint, pageableData);
        assertThat(result.getResults()).isEmpty();
        assertThat(result.getPagination()).isNull();
    }

    protected void setupSearchConfiguration() throws Exception {
        CMSSiteModel cmsSiteModel = mock(CMSSiteModel.class);
        SolrFacetSearchConfigModel storeFinderSolrFacetSearchConfigModel = mock(SolrFacetSearchConfigModel.class);
        IndexConfig indexConfig = mock(IndexConfig.class);

        when(storeFinderSolrFacetSearchConfigModel.getName()).thenReturn("storeFinderSolrFacetSearchConfig");

        when(cmsSiteModel.getStoreFinderSolrFacetSearchConfiguration()).thenReturn(storeFinderSolrFacetSearchConfigModel);

        when(cmsSiteService.getCurrentSite()).thenReturn(cmsSiteModel);

        when(facetSearchConfigService.getConfiguration("storeFinderSolrFacetSearchConfig")).thenReturn(facetSearchConfig);

        when(facetSearchConfig.getIndexConfig()).thenReturn(indexConfig);

        when(indexConfig.getIndexedTypes()).thenReturn(Collections.singletonMap("indexedType-key", indexedType));
    }
}