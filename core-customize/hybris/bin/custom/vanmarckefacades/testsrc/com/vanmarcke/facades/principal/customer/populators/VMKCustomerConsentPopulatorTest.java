package com.vanmarcke.facades.principal.customer.populators;

import com.vanmarcke.core.enums.ConsentType;
import com.vanmarcke.core.enums.OptType;
import com.vanmarcke.core.model.VMKConsentModel;
import com.vanmarcke.facades.data.VMKConsentData;
import de.hybris.bootstrap.annotations.UnitTest;
import org.apache.commons.lang.RandomStringUtils;
import org.fest.assertions.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

/**
 * The {@link VMKCustomerConsentPopulatorTest} class contains the unit tests for the
 * {@link VMKCustomerConsentPopulator} class.
 *
 * @author Tom van den Berg
 * @since 05-06-2020
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKCustomerConsentPopulatorTest {

    private static final String IP_ADDRESS = RandomStringUtils.randomAlphabetic(10);
    private static final ConsentType CONSENT_TYPE = ConsentType.ANALYTICAL_COOKIES;
    private static final OptType OPT_TYPE = OptType.OPT_IN;

    @InjectMocks
    private VMKCustomerConsentPopulator populator;

    @Test
    public void populate() {
        VMKConsentModel source = new VMKConsentModel();
        VMKConsentData target = new VMKConsentData();

        source.setConsentType(CONSENT_TYPE);
        source.setOptType(OPT_TYPE);

        populator.populate(source, target);

        Assertions
                .assertThat(target.getConsentType())
                .isEqualTo(CONSENT_TYPE);

        Assertions
                .assertThat(target.getOptType())
                .isEqualTo(OPT_TYPE);
    }
}