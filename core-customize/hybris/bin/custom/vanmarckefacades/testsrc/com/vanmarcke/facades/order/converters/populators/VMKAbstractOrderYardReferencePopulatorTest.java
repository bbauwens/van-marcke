package com.vanmarcke.facades.order.converters.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.*;

@UnitTest
public class VMKAbstractOrderYardReferencePopulatorTest {

    private VMKAbstractOrderYardReferencePopulator blueAbstractOrderYardReferencePopulator;

    @Before
    public void setUp() {
        blueAbstractOrderYardReferencePopulator = new VMKAbstractOrderYardReferencePopulator();
    }

    @Test
    public void populate() {
        AbstractOrderModel source = mock(AbstractOrderModel.class);
        AbstractOrderData target = mock(AbstractOrderData.class);
        when(source.getYardReference()).thenReturn("reference");

        blueAbstractOrderYardReferencePopulator.populate(source, target);

        verify(target).setYardReference("reference");
    }
}