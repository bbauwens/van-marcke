package com.vanmarcke.facades.delivery.methodform.populators;

import com.vanmarcke.core.enums.ReferenceConditionalType;
import com.vanmarcke.services.forms.delivery.DeliveryMethodForm;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.session.SessionService;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.vanmarcke.core.constants.VanmarckeCoreConstants.REQUEST_SEPARATE_INVOICE;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


/**
 * The {@link VMKRequestSeparateInvoiceDeliveryMethodFormPopulatorTest} class contains the unit tests for the
 * {@link VMKRequestSeparateInvoiceDeliveryMethodFormPopulator} class.
 *
 * @author Niels Raemaekers
 * @since 17-03-2021
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKRequestSeparateInvoiceDeliveryMethodFormPopulatorTest {

    private static final String CODE = RandomStringUtils.random(6);

    @Mock
    SessionService sessionService;

    @InjectMocks
    private VMKRequestSeparateInvoiceDeliveryMethodFormPopulator populator;

    @Test
    public void testPopulate_mandatoryRef_checkBoxIsTickedByUser() {
        // given
        B2BUnitModel b2BUnit = mock(B2BUnitModel.class);
        when(b2BUnit.getRef1()).thenReturn(ReferenceConditionalType.MANDATORY);

        B2BCustomerModel currentCustomer = mock(B2BCustomerModel.class);
        when(currentCustomer.getDefaultB2BUnit()).thenReturn(b2BUnit);

        CartModel cart = mock(CartModel.class);
        when(cart.getCode()).thenReturn(CODE);
        when(cart.getUser()).thenReturn(currentCustomer);

        when(sessionService.getAttribute(REQUEST_SEPARATE_INVOICE)).thenReturn(true);
        DeliveryMethodForm form = new DeliveryMethodForm();

        // when
        populator.populate(cart, form);

        // then
        assertThat(form.getRequestSeparateInvoice()).isTrue();
        assertThat(form.getPurchaseOrderNumber()).isEqualTo(CODE);
    }

    @Test
    public void testPopulate_mandatoryRef_checkBoxIsUntickedByUser() {
        // given
        B2BUnitModel b2BUnit = mock(B2BUnitModel.class);
        when(b2BUnit.getRef1()).thenReturn(ReferenceConditionalType.MANDATORY);

        B2BCustomerModel currentCustomer = mock(B2BCustomerModel.class);
        when(currentCustomer.getDefaultB2BUnit()).thenReturn(b2BUnit);

        CartModel cart = mock(CartModel.class);
        when(cart.getCode()).thenReturn(CODE);
        when(cart.getUser()).thenReturn(currentCustomer);

        when(sessionService.getAttribute(REQUEST_SEPARATE_INVOICE)).thenReturn(null);
        DeliveryMethodForm form = new DeliveryMethodForm();

        // when
        populator.populate(cart, form);

        // then
        assertThat(form.getRequestSeparateInvoice()).isTrue();
        assertThat(form.getPurchaseOrderNumber()).isEqualTo(CODE);
    }

    @Test
    public void testPopulate_optionalRef_checkBoxIsTickedByUser() {
        // when
        B2BUnitModel b2BUnit = mock(B2BUnitModel.class);
        when(b2BUnit.getRef1()).thenReturn(ReferenceConditionalType.OPTIONAL);

        B2BCustomerModel currentCustomer = mock(B2BCustomerModel.class);
        when(currentCustomer.getDefaultB2BUnit()).thenReturn(b2BUnit);

        CartModel cart = mock(CartModel.class);
        when(cart.getCode()).thenReturn(CODE);
        when(cart.getUser()).thenReturn(currentCustomer);

        when(sessionService.getAttribute(REQUEST_SEPARATE_INVOICE)).thenReturn(true);
        DeliveryMethodForm form = new DeliveryMethodForm();

        // when
        populator.populate(cart, form);

        // then
        assertThat(form.getRequestSeparateInvoice()).isTrue();
        assertThat(form.getPurchaseOrderNumber()).isNull();
    }

    @Test
    public void testPopulate_optionalRef_checkBoxIsUntickedByUser() {
        // when
        B2BUnitModel b2BUnit = mock(B2BUnitModel.class);
        when(b2BUnit.getRef1()).thenReturn(ReferenceConditionalType.OPTIONAL);

        B2BCustomerModel currentCustomer = mock(B2BCustomerModel.class);
        when(currentCustomer.getDefaultB2BUnit()).thenReturn(b2BUnit);

        CartModel cart = mock(CartModel.class);
        when(cart.getCode()).thenReturn(CODE);
        when(cart.getUser()).thenReturn(currentCustomer);

        when(sessionService.getAttribute(REQUEST_SEPARATE_INVOICE)).thenReturn(null);
        DeliveryMethodForm form = new DeliveryMethodForm();

        // when
        populator.populate(cart, form);

        // then
        assertThat(form.getRequestSeparateInvoice()).isFalse();
        assertThat(form.getPurchaseOrderNumber()).isNull();
    }

    @Test
    public void testPopulate_forbiddenRef_checkBoxIsTickedByUser() {
        // when
        B2BUnitModel b2BUnit = mock(B2BUnitModel.class);
        when(b2BUnit.getRef1()).thenReturn(ReferenceConditionalType.FORBIDDEN);

        B2BCustomerModel currentCustomer = mock(B2BCustomerModel.class);
        when(currentCustomer.getDefaultB2BUnit()).thenReturn(b2BUnit);

        CartModel cart = mock(CartModel.class);
        when(cart.getCode()).thenReturn(CODE);
        when(cart.getUser()).thenReturn(currentCustomer);

        when(sessionService.getAttribute(REQUEST_SEPARATE_INVOICE)).thenReturn(true);
        DeliveryMethodForm form = new DeliveryMethodForm();

        // when
        populator.populate(cart, form);

        // then
        assertThat(form.getRequestSeparateInvoice()).isTrue();
        assertThat(form.getPurchaseOrderNumber()).isNull();
    }

    @Test
    public void testPopulate_forbiddenRef_checkBoxIsUntickedByUser() {
        // when
        B2BUnitModel b2BUnit = mock(B2BUnitModel.class);
        when(b2BUnit.getRef1()).thenReturn(ReferenceConditionalType.FORBIDDEN);

        B2BCustomerModel currentCustomer = mock(B2BCustomerModel.class);
        when(currentCustomer.getDefaultB2BUnit()).thenReturn(b2BUnit);

        CartModel cart = mock(CartModel.class);
        when(cart.getCode()).thenReturn(CODE);
        when(cart.getUser()).thenReturn(currentCustomer);

        when(sessionService.getAttribute(REQUEST_SEPARATE_INVOICE)).thenReturn(null);
        DeliveryMethodForm form = new DeliveryMethodForm();

        // when
        populator.populate(cart, form);

        // then
        assertThat(form.getRequestSeparateInvoice()).isFalse();
        assertThat(form.getPurchaseOrderNumber()).isNull();
    }



    @Test
    public void testPopulate_checkBoxIsTickedByUser() {
        // when
        B2BUnitModel b2BUnit = mock(B2BUnitModel.class);

        B2BCustomerModel currentCustomer = mock(B2BCustomerModel.class);
        when(currentCustomer.getDefaultB2BUnit()).thenReturn(b2BUnit);

        CartModel cart = mock(CartModel.class);
        when(cart.getCode()).thenReturn(CODE);
        when(cart.getUser()).thenReturn(currentCustomer);

        when(sessionService.getAttribute(REQUEST_SEPARATE_INVOICE)).thenReturn(true);
        DeliveryMethodForm form = new DeliveryMethodForm();

        // when
        populator.populate(cart, form);

        // then
        assertThat(form.getRequestSeparateInvoice()).isTrue();
        assertThat(form.getPurchaseOrderNumber()).isNull();
    }

    @Test
    public void testPopulate_noRef_checkBoxIsUntickedByUser() {
        // when
        B2BUnitModel b2BUnit = mock(B2BUnitModel.class);

        B2BCustomerModel currentCustomer = mock(B2BCustomerModel.class);
        when(currentCustomer.getDefaultB2BUnit()).thenReturn(b2BUnit);

        CartModel cart = mock(CartModel.class);
        when(cart.getCode()).thenReturn(CODE);
        when(cart.getUser()).thenReturn(currentCustomer);

        when(sessionService.getAttribute(REQUEST_SEPARATE_INVOICE)).thenReturn(null);
        DeliveryMethodForm form = new DeliveryMethodForm();

        // when
        populator.populate(cart, form);

        // then
        assertThat(form.getRequestSeparateInvoice()).isFalse();
        assertThat(form.getPurchaseOrderNumber()).isNull();
    }

    @Test
    public void testGetRef1Placeholder_lessThan6() {
        // when
        String actualPlaceholder = populator.getRef1Placeholder("1234");

        // then
        assertThat(actualPlaceholder).isEqualTo("1234");
    }

    @Test
    public void testGetRef1Placeholder_equalTo6() {
        // when
        String actualPlaceholder = populator.getRef1Placeholder("123456");

        // then
        assertThat(actualPlaceholder).isEqualTo("123456");
    }

    @Test
    public void testGetRef1Placeholder_greaterThan6() {
        // when
        String actualPlaceholder = populator.getRef1Placeholder("1234567890");

        // then
        assertThat(actualPlaceholder).isEqualTo("567890");
    }
}
