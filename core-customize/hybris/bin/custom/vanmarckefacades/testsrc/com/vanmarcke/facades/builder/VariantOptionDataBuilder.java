package com.vanmarcke.facades.builder;

import de.hybris.platform.commercefacades.product.data.VariantOptionData;

/**
 * Builder implementation for the {@link VariantOptionDataBuilder}.
 *
 * @author Tom van den Berg
 * @since 11-3-2020
 */
public class VariantOptionDataBuilder {


    private boolean purchasable;

    /**
     * Hides the default implicit constructor.
     */
    private VariantOptionDataBuilder() {
    }

    /**
     * The entry point for the {@code VariantOptionDataBuilder}.
     *
     * @return a {@code VariantOptionDataBuilder} instance for method chaining
     */
    public static VariantOptionDataBuilder aVariantOptionData() {
        return new VariantOptionDataBuilder();
    }

    /**
     * Sets the {@code VariantOptionData} to purchasable
     *
     * @return the VariantOptionDataBuilder for method chaining
     */
    public VariantOptionDataBuilder thatIsPurchasable() {
        this.purchasable = true;
        return this;
    }

    /**
     * Sets the {@code VariantOptionData} to not purchasable
     *
     * @return the VariantOptionDataBuilder for method chaining
     */
    public VariantOptionDataBuilder thatIsNotPurchasable() {
        this.purchasable = false;
        return this;
    }

    /**
     * Builds and returns a {@code VariantOptionData} instance.
     *
     * @return the variant option data instance
     */
    public VariantOptionData build() {
        VariantOptionData productData = new VariantOptionData();
        productData.setPurchasable(purchasable);
        return productData;
    }
}
