package com.vanmarcke.facades.delivery.methodform.populators;

import com.vanmarcke.facades.storesession.VMKStoreSessionFacade;
import com.vanmarcke.services.forms.delivery.DeliveryMethodForm;
import com.vanmarcke.services.model.builder.CartModelMockBuilder;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.vanmarcke.core.constants.VanmarckeCoreConstants.DeliveryMethods.PICKUP;
import static com.vanmarcke.core.constants.VanmarckeCoreConstants.DeliveryMethods.SHIPPING;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKSelectedDeliveryMethodPopulatorTest {

    @Mock
    private VMKStoreSessionFacade storeSessionFacade;
    @InjectMocks
    private VMKSelectedDeliveryMethodPopulator populator;

    @Test
    public void testPopulate_shipping() {
        CartModel cart = CartModelMockBuilder.aCart().build();
        ZoneDeliveryModeModel deliveryMode = mock(ZoneDeliveryModeModel.class);

        when(cart.getDeliveryMode()).thenReturn(deliveryMode);

        DeliveryMethodForm form = new DeliveryMethodForm();

        populator.populate(cart, form);

        assertThat(form.getDeliveryMethod()).isEqualTo(SHIPPING);

        verify(cart).getDeliveryMode();
    }

    @Test
    public void testPopulate_pickup() {
        CartModel cart = CartModelMockBuilder.aCart().build();
        DeliveryModeModel deliveryMode = mock(DeliveryModeModel.class);

        when(cart.getDeliveryMode()).thenReturn(deliveryMode);

        DeliveryMethodForm form = new DeliveryMethodForm();

        populator.populate(cart, form);

        assertThat(form.getDeliveryMethod()).isEqualTo(PICKUP);

        verify(cart).getDeliveryMode();
    }


}