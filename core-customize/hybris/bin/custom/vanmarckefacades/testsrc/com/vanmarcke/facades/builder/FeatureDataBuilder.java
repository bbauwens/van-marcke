package com.vanmarcke.facades.builder;

import de.hybris.platform.commercefacades.product.data.FeatureData;
import de.hybris.platform.commercefacades.product.data.FeatureUnitData;
import de.hybris.platform.commercefacades.product.data.FeatureValueData;

import java.util.Collection;
import java.util.List;

public class FeatureDataBuilder {

    private final FeatureData featureData = new FeatureData();

    /**
     * Private constructor to hide the implicit default one.
     */
    private FeatureDataBuilder() {
    }

    /**
     * A new instance of the {@link FeatureDataBuilder} is created and returned
     *
     * @return the created {@link FeatureDataBuilder} instance
     */
    public static FeatureDataBuilder aFeatureData() {
        return new FeatureDataBuilder();
    }

    /**
     * Sets the given {@code featureUnitData} on the current {@link FeatureData} instance.
     *
     * @param featureUnitData the featureUnitData
     * @return the current {@link FeatureDataBuilder} instance for method chaining
     */
    public FeatureDataBuilder withFeatureUnit(FeatureUnitData featureUnitData) {
        featureData.setFeatureUnit(featureUnitData);
        return this;
    }

    /**
     * Sets the given {@code code} on the current {@link FeatureData} instance.
     *
     * @param code the code
     * @return the current {@link FeatureDataBuilder} instance for method chaining
     */
    public FeatureDataBuilder withCode(String code) {
        featureData.setCode(code);
        return this;
    }

    /**
     * Sets the given {@code code} on the current {@link FeatureData} instance.
     *
     * @param featureValues the featureValues
     * @return the current {@link FeatureDataBuilder} instance for method chaining
     */
    public FeatureDataBuilder withFeatureValues(List<FeatureValueData> featureValues) {
        featureData.setFeatureValues(featureValues);
        return this;
    }

    /**
     * Sets the given {@code code} on the current {@link FeatureData} instance.
     *
     * @param featureValueData the featureValueData
     * @return the current {@link FeatureDataBuilder} instance for method chaining
     */
    public FeatureDataBuilder addFeatureValues(FeatureValueData featureValueData) {
        Collection<FeatureValueData> list = featureData.getFeatureValues();
        list.add(featureValueData);
        featureData.setFeatureValues(list);
        return this;
    }

    /**
     * Sets the given {@code classification code} on the current {@link FeatureData} instance.
     *
     * @param classificationCode the classificationCode
     * @return the current {@link FeatureDataBuilder} instance for method chaining
     */
    public FeatureDataBuilder withClassificationCode(String classificationCode) {
        featureData.setClassificationCode(classificationCode);
        return this;
    }

    /**
     * Returns the current {@link FeatureData} instance
     *
     * @return the current {@link FeatureData} instance
     */
    public FeatureData build() {
        return featureData;
    }
}
