package com.vanmarcke.facades.order.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.AdditionalAnswers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;
import java.io.Writer;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKCsvFacadeImplTest {

    private static final String DELIMITER = ";";
    private static final String LINE_SEPERATOR = "\n";

    private static final String MESSAGE1 = RandomStringUtils.randomAlphabetic(10);
    private static final String MESSAGE2 = RandomStringUtils.randomAlphabetic(10);
    private static final String MESSAGE3 = RandomStringUtils.randomAlphabetic(10);
    private static final String CSV_HEADER = MESSAGE1 + DELIMITER + MESSAGE2 + DELIMITER + MESSAGE3 + LINE_SEPERATOR;

    private static final String PRODUCT_CODE = RandomStringUtils.randomAlphabetic(10);
    private static final Long QUANTITY = RandomUtils.nextLong(1, 10);
    private static final String PRODUCT_NAME = RandomStringUtils.randomAlphabetic(10);
    private static final String CURRENCY_ISO = RandomStringUtils.randomAlphabetic(10);
    private static final String BASE_PRICE = RandomStringUtils.randomAlphabetic(10);
    private static final String TOTAL_PRICE = RandomStringUtils.randomAlphabetic(10);

    private static final BigDecimal BASE_PRICE_VALUE = BigDecimal.valueOf(RandomUtils.nextLong(0, 1000));

    private static final String CSV_CONTENT =
            PRODUCT_CODE + DELIMITER +
                    QUANTITY.toString() + DELIMITER +
                    PRODUCT_NAME + DELIMITER +
                    BASE_PRICE + DELIMITER +
                    TOTAL_PRICE + LINE_SEPERATOR;

    @Mock
    private PriceDataFactory priceDataFactory;
    @Mock
    private CommonI18NService commonI18NService;

    @Spy
    @InjectMocks
    private VMKCsvFacadeImpl facade;

    @Before
    public void setUp() {
        when(commonI18NService.roundCurrency(anyDouble(), anyInt())).then(AdditionalAnswers.returnsFirstArg());
    }

    @Test
    public void generateCsvFromCart() throws IOException {
        List<String> headers = new ArrayList<>();
        Collections.addAll(headers, MESSAGE1, MESSAGE2, MESSAGE3);

        Writer writer = mock(Writer.class);

        CartData cartData = new CartData();
        OrderEntryData orderEntry = new OrderEntryData();
        cartData.setEntries(Collections.singletonList(orderEntry));

        doNothing().when(facade).writeOrderEntries(writer, Collections.singletonList(orderEntry));

        facade.generateCsvFromCart(headers, true, cartData, writer);

        verify(writer).write(CSV_HEADER);
        verify(facade).writeOrderEntries(writer, cartData.getEntries());
    }

    @Test
    public void writeOrderEntriesMultiDimensional() throws IOException {
        Writer writer = mock(Writer.class);

        OrderEntryData orderEntry = mock(OrderEntryData.class);
        List<OrderEntryData> entries = Collections.singletonList(orderEntry);

        OrderEntryData subOrderEntry = mock(OrderEntryData.class);
        when(orderEntry.getEntries()).thenReturn(Collections.singletonList(subOrderEntry));

        ProductData product = mock(ProductData.class);
        when(orderEntry.getProduct()).thenReturn(product);
        when(product.getMultidimensional()).thenReturn(Boolean.TRUE);

        doNothing().when(facade).writeOrderEntry(writer, subOrderEntry);

        facade.writeOrderEntries(writer, entries);

        verify(facade).writeOrderEntry(writer, subOrderEntry);
        verify(facade, times(0)).writeOrderEntry(writer, orderEntry);
    }

    @Test
    public void writeOrderEntriesNotMultiDimensional() throws IOException {
        Writer writer = mock(Writer.class);

        OrderEntryData orderEntry = mock(OrderEntryData.class);
        List<OrderEntryData> entries = Collections.singletonList(orderEntry);

        OrderEntryData subOrderEntry = mock(OrderEntryData.class);
        when(orderEntry.getEntries()).thenReturn(Collections.singletonList(subOrderEntry));

        ProductData product = mock(ProductData.class);
        when(orderEntry.getProduct()).thenReturn(product);
        when(product.getMultidimensional()).thenReturn(Boolean.FALSE);

        doNothing().when(facade).writeOrderEntry(writer, orderEntry);

        facade.writeOrderEntries(writer, entries);

        verify(facade).writeOrderEntry(writer, orderEntry);
        verify(facade, times(0)).writeOrderEntry(writer, subOrderEntry);
    }

    @Test
    public void writeWhishListEntries_hideNetPrice() throws IOException {
        boolean showNetPrice = false;
        BigDecimal priceWithValueOne = BigDecimal.ONE;

        Writer writer = mock(Writer.class);

        PriceData priceData = mock(PriceData.class);
        when(priceData.getValue()).thenReturn(priceWithValueOne);

        OrderEntryData orderEntry = mock(OrderEntryData.class);
        when(orderEntry.getTotalPrice()).thenReturn(priceData);
        List<OrderEntryData> entries = Collections.singletonList(orderEntry);

        OrderEntryData subOrderEntry = mock(OrderEntryData.class);
        when(orderEntry.getEntries()).thenReturn(Collections.singletonList(subOrderEntry));

        ProductData product = mock(ProductData.class);
        when(orderEntry.getProduct()).thenReturn(product);
        when(product.getMultidimensional()).thenReturn(Boolean.FALSE);

        doNothing().when(facade).writeOrderEntry(writer, orderEntry, showNetPrice);

        facade.writeWishListEntries(writer, entries, showNetPrice);

        verify(facade).writeOrderEntry(writer, orderEntry, showNetPrice);
        verify(facade, times(0)).writeOrderEntry(writer, subOrderEntry, showNetPrice);
        verify(facade).writeTotals(writer, showNetPrice, priceWithValueOne.floatValue());
    }

    @Test
    public void writeWhishListEntries_showNetPrice() throws IOException {
        boolean showNetPrice = true;
        BigDecimal priceWithValueOne = BigDecimal.ONE;

        Writer writer = mock(Writer.class);

        PriceData priceData = mock(PriceData.class);
        when(priceData.getValue()).thenReturn(priceWithValueOne);

        PriceData netPriceData = mock(PriceData.class);

        OrderEntryData orderEntry = mock(OrderEntryData.class);
        when(orderEntry.getTotalPrice()).thenReturn(priceData);
        when(orderEntry.getNetPriceData()).thenReturn(netPriceData);
        List<OrderEntryData> entries = Collections.singletonList(orderEntry);

        OrderEntryData subOrderEntry = mock(OrderEntryData.class);
        when(orderEntry.getEntries()).thenReturn(Collections.singletonList(subOrderEntry));

        ProductData product = mock(ProductData.class);
        when(orderEntry.getProduct()).thenReturn(product);
        when(product.getMultidimensional()).thenReturn(Boolean.FALSE);

        doNothing().when(facade).writeOrderEntry(writer, orderEntry, showNetPrice);

        facade.writeWishListEntries(writer, entries, showNetPrice);

        verify(facade).writeOrderEntry(writer, orderEntry, showNetPrice);
        verify(facade, times(0)).writeOrderEntry(writer, subOrderEntry, showNetPrice);
        verify(facade).writeTotals(writer, showNetPrice, priceWithValueOne.floatValue());
    }
}
