package com.vanmarcke.facades.order.converters.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commerceservices.model.PickUpDeliveryModeModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.ArrayList;

import static java.util.Collections.singletonList;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKPickupOrderEntryGroupPopulatorTest {

    @Mock
    private PriceDataFactory priceDataFactory;
    @InjectMocks
    private VMKPickupOrderEntryGroupPopulator vmkPickupOrderEntryGroupPopulator;

    @Test
    public void testCreateUpdatePickupGroupData_withPickup() {
        CurrencyModel currencyModel = mock(CurrencyModel.class);
        DeliveryModeModel deliveryModeModel = mock(PickUpDeliveryModeModel.class);
        AbstractOrderModel orderModel = mock(AbstractOrderModel.class);
        AbstractOrderEntryModel entryModel = mock(AbstractOrderEntryModel.class);

        when(orderModel.getCurrency()).thenReturn(currencyModel);
        when(orderModel.getDeliveryMode()).thenReturn(deliveryModeModel);

        when(entryModel.getOrder()).thenReturn(orderModel);
        when(entryModel.getEntryNumber()).thenReturn(1);
        when(entryModel.getTotalPrice()).thenReturn(10D);

        PriceData priceData = mock(PriceData.class);
        when(priceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(10D), currencyModel)).thenReturn(priceData);

        AbstractOrderData result = newResult();
        vmkPickupOrderEntryGroupPopulator.createUpdatePickupGroupData(entryModel, result);

        assertThat(result.getPickupOrderGroups()).hasSize(1);
        assertThat(result.getPickupOrderGroups().get(0).getTotalPriceWithTax()).isEqualTo(priceData);
        assertThat(result.getPickupOrderGroups().get(0).getEntries()).containsOnly(result.getEntries().get(0));
    }

    @Test
    public void testCreateUpdatePickupGroupData_withShipping() {
        CurrencyModel currencyModel = mock(CurrencyModel.class);
        DeliveryModeModel deliveryModeModel = mock(ZoneDeliveryModeModel.class);
        AbstractOrderModel orderModel = mock(AbstractOrderModel.class);
        AbstractOrderEntryModel entryModel = mock(AbstractOrderEntryModel.class);

        when(orderModel.getCurrency()).thenReturn(currencyModel);
        when(orderModel.getDeliveryMode()).thenReturn(deliveryModeModel);

        when(entryModel.getOrder()).thenReturn(orderModel);
        when(entryModel.getEntryNumber()).thenReturn(1);
        when(entryModel.getTotalPrice()).thenReturn(10D);

        AbstractOrderData result = newResult();
        vmkPickupOrderEntryGroupPopulator.createUpdatePickupGroupData(entryModel, result);

        verifyZeroInteractions(priceDataFactory);

        assertThat(result.getPickupOrderGroups()).isEmpty();
    }

    @Test
    public void testSumPickupItemsQuantity_withPickup() {
        DeliveryModeModel deliveryModeModel = mock(PickUpDeliveryModeModel.class);
        AbstractOrderModel orderModel = mock(AbstractOrderModel.class);
        AbstractOrderEntryModel entryModel = mock(AbstractOrderEntryModel.class);

        when(orderModel.getDeliveryMode()).thenReturn(deliveryModeModel);
        when(orderModel.getEntries()).thenReturn(singletonList(entryModel));

        when(entryModel.getOrder()).thenReturn(orderModel);
        when(entryModel.getQuantity()).thenReturn(10L);

        long result = vmkPickupOrderEntryGroupPopulator.sumPickupItemsQuantity(orderModel);
        assertThat(result).isEqualTo(10);
    }

    @Test
    public void testSumPickupItemsQuantity_withShipping() {
        DeliveryModeModel deliveryModeModel = mock(ZoneDeliveryModeModel.class);
        AbstractOrderModel orderModel = mock(AbstractOrderModel.class);
        AbstractOrderEntryModel entryModel = mock(AbstractOrderEntryModel.class);

        when(orderModel.getDeliveryMode()).thenReturn(deliveryModeModel);
        when(orderModel.getEntries()).thenReturn(singletonList(entryModel));

        when(entryModel.getOrder()).thenReturn(orderModel);
        when(entryModel.getQuantity()).thenReturn(10L);

        long result = vmkPickupOrderEntryGroupPopulator.sumPickupItemsQuantity(orderModel);
        assertThat(result).isZero();
    }

    private AbstractOrderData newResult() {
        AbstractOrderData result = new AbstractOrderData();
        result.setPickupOrderGroups(new ArrayList<>(1));
        OrderEntryData entryData = new OrderEntryData();
        entryData.setEntryNumber(1);
        result.setEntries(singletonList(entryData));
        return result;
    }

}