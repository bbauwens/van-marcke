package com.vanmarcke.facades.order.validation.impl;

import com.elision.hybris.l10n.facades.LocalizedMessageFacade;
import com.vanmarcke.facades.order.VMKCheckoutFacade;
import de.hybris.bootstrap.annotations.UnitTest;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.logging.log4j.util.Strings;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.HashMap;
import java.util.Map;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKOrderValidatorImplTest {

    private static final String DELIVERY_NOT_POSSIBLE_KEY = "cart.validation-error.delivery.impossible";
    private static final String DELIVERY_NOT_POSSIBLE_MESSAGE = RandomStringUtils.randomAlphabetic(10);

    private static final String INVALID_ENTRIES_KEY = "cart.validation-error.entries-invalid";
    private static final String INVALID_ENTRIES = RandomStringUtils.randomAlphabetic(10);
    private static final String INVALID_ENTRIES_TEMPLATE = RandomStringUtils.randomAlphabetic(10);
    private static final String INVALID_ENTRIES_FORMATTED = INVALID_ENTRIES_TEMPLATE + INVALID_ENTRIES;
    private static final String ADJUSTED_STOCK_DISCONTINUED_PRODUCTS = "product.details.discontinued.popup";
    private static final String ADJUSTED_STOCK_ENTRIES = "product1,product2";
    @Mock
    private VMKCheckoutFacade checkoutFacade;
    @Mock
    private LocalizedMessageFacade messageFacade;
    @InjectMocks
    private VMKOrderValidatorImpl orderValidator;

    @Test
    public void testCheckCartForErrors_allOk() {
        when(checkoutFacade.getInvalidCartEntries()).thenReturn(Strings.EMPTY);

        Map<String, Boolean> availableDeliveryMethods = new HashMap<>();
        availableDeliveryMethods.put("", true);
        when(checkoutFacade.getAvailableDeliveryMethods()).thenReturn(availableDeliveryMethods);

        assertThat(orderValidator.checkCartForErrors()).isEmpty();

        verifyZeroInteractions(messageFacade);
        verify(checkoutFacade).getInvalidCartEntries();
        verify(checkoutFacade).getAvailableDeliveryMethods();
    }

    @Test
    public void testCheckCartForErrors_withInvalidEntries() {
        when(checkoutFacade.getInvalidCartEntries()).thenReturn(INVALID_ENTRIES);
        when(messageFacade.getMessageForCodeAndCurrentLocale(INVALID_ENTRIES_KEY))
                .thenReturn(INVALID_ENTRIES_TEMPLATE + "{0}");

        assertThat(orderValidator.checkCartForErrors()).isEqualTo(INVALID_ENTRIES_FORMATTED);

        verify(messageFacade).getMessageForCodeAndCurrentLocale(INVALID_ENTRIES_KEY);
        verify(messageFacade, never()).getMessageForCodeAndCurrentLocale(DELIVERY_NOT_POSSIBLE_KEY);
        verify(checkoutFacade).getInvalidCartEntries();
        verify(checkoutFacade, never()).getAvailableDeliveryMethods();
    }

    @Test
    public void testCheckCartForErrors_blockCheckout() {
        when(checkoutFacade.getInvalidCartEntries()).thenReturn(Strings.EMPTY);

        Map<String, Boolean> availableDeliveryMethods = new HashMap<>();
        availableDeliveryMethods.put("", false);
        when(checkoutFacade.getAvailableDeliveryMethods()).thenReturn(availableDeliveryMethods);

        when(messageFacade.getMessageForCodeAndCurrentLocale(DELIVERY_NOT_POSSIBLE_KEY))
                .thenReturn(DELIVERY_NOT_POSSIBLE_MESSAGE);

        assertThat(orderValidator.checkCartForErrors()).isEqualTo(DELIVERY_NOT_POSSIBLE_MESSAGE);

        verify(messageFacade).getMessageForCodeAndCurrentLocale(DELIVERY_NOT_POSSIBLE_KEY);
        verify(messageFacade, never()).getMessageForCodeAndCurrentLocale(INVALID_ENTRIES_KEY);
        verify(checkoutFacade).getInvalidCartEntries();
        verify(checkoutFacade).getAvailableDeliveryMethods();
    }

    @Test
    public void testAdjustCartEntries() {
        when(checkoutFacade.adjustQuantityForDiscontinuedProducts()).thenReturn(ADJUSTED_STOCK_ENTRIES);
        when(messageFacade.getMessageForCodeAndCurrentLocale(ADJUSTED_STOCK_DISCONTINUED_PRODUCTS))
                .thenReturn(INVALID_ENTRIES_TEMPLATE + "{0}");

        assertThat(orderValidator.adjustQuantityForDiscontinuedProducts()).isEqualTo(INVALID_ENTRIES_TEMPLATE + ADJUSTED_STOCK_ENTRIES);

        verify(messageFacade).getMessageForCodeAndCurrentLocale(ADJUSTED_STOCK_DISCONTINUED_PRODUCTS);
        verify(checkoutFacade).adjustQuantityForDiscontinuedProducts();
    }
}