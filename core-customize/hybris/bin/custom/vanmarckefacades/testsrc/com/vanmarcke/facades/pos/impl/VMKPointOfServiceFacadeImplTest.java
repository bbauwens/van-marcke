package com.vanmarcke.facades.pos.impl;

import com.vanmarcke.facades.search.VMKPointOfServiceLocatorSearchFacade;
import com.vanmarcke.services.model.builder.CartModelMockBuilder;
import com.vanmarcke.services.model.builder.PointOfServiceModelMockBuilder;
import com.vanmarcke.services.storelocator.pos.VMKPointOfServiceService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commercefacades.storelocator.data.PointOfServiceData;
import de.hybris.platform.commerceservices.store.data.GeoPoint;
import de.hybris.platform.commerceservices.storefinder.data.StoreFinderSearchPageData;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import org.apache.commons.lang3.RandomStringUtils;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;

import static com.vanmarcke.facades.constants.VanmarckeFacadesConstants.SOLR.DEFAULT_SORT;
import static com.vanmarcke.facades.constants.VanmarckeFacadesConstants.SOLR.POINT_OF_SERVICE.SOLR_OFFSET;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKPointOfServiceFacadeImplTest {

    private static final String POS_NAME1 = RandomStringUtils.randomAlphabetic(10);
    private static final String POS_NAME2 = RandomStringUtils.randomAlphabetic(10);
    private static final String UID = RandomStringUtils.randomAlphabetic(10);

    private CartModel cart;
    private PointOfServiceModel pos1;
    private PointOfServiceModel pos2;

    @Mock
    private VMKPointOfServiceLocatorSearchFacade vmkPointOfServiceLocatorSearchFacade;

    @Mock
    private VMKPointOfServiceService pointOfServiceService;

    @Mock
    private BaseStoreService baseStoreService;

    @InjectMocks
    private VMKPointOfServiceFacadeImpl pointOfServiceFacade;

    @Before
    public void setUp() throws Exception {
        pos1 = PointOfServiceModelMockBuilder
                .aPointOfService()
                .withName(POS_NAME1)
                .build();

        pos2 = PointOfServiceModelMockBuilder
                .aPointOfService()
                .withName(POS_NAME2)
                .build();

        cart = CartModelMockBuilder
                .aCart()
                .withDeliveryPointOfService(pos1)
                .build();
    }

    @Test
    public void testAlternativeTecStoresFromCartWithOpenStores() {
        when(pointOfServiceService.getAlternativeTECStoresWhichAreCurrentlyOpenForCart(cart))
                .thenReturn(Arrays.asList(pos1, pos2));

        GeoPoint geoPoint = new GeoPoint();
        when(pointOfServiceService.getGeoPoint(pos1)).thenReturn(geoPoint);

        PointOfServiceData pos1Data = new PointOfServiceData();
        StoreFinderSearchPageData<PointOfServiceData> searchResult = new StoreFinderSearchPageData<>();
        searchResult.setResults(singletonList(pos1Data));


        when(vmkPointOfServiceLocatorSearchFacade.search(
                null,
                geoPoint,
                1_000_000,
                SOLR_OFFSET,
                DEFAULT_SORT,
                Arrays.asList(POS_NAME1, POS_NAME2))).thenReturn(searchResult);

        List<PointOfServiceData> actualTECStores = pointOfServiceFacade.findAlternativePointOfServicesCloseByCurrentStore(cart);

        Assertions
                .assertThat(actualTECStores)
                .containsExactly(pos1Data);

        verify(pointOfServiceService).getAlternativeTECStoresWhichAreCurrentlyOpenForCart(cart);
        verify(pointOfServiceService).getGeoPoint(pos1);
        verify(pointOfServiceService, never()).getGeoPoint(pos2);
        verify(vmkPointOfServiceLocatorSearchFacade).search(null, geoPoint, 1_000_000,
                SOLR_OFFSET, DEFAULT_SORT, Arrays.asList(POS_NAME1, POS_NAME2));
    }

    @Test
    public void testAlternativeTecStoresFromCartWithoutOpenStores() {
        GeoPoint geoPoint = new GeoPoint();

        when(pointOfServiceService.getAlternativeTECStoresWhichAreCurrentlyOpenForCart(cart)).thenReturn(emptyList());

        List<PointOfServiceData> actualTECStores = pointOfServiceFacade.findAlternativePointOfServicesCloseByCurrentStore(cart);

        Assertions
                .assertThat(actualTECStores)
                .isEmpty();

        verify(pointOfServiceService).getAlternativeTECStoresWhichAreCurrentlyOpenForCart(cart);
        verify(pointOfServiceService, never()).getGeoPoint(pos1);
        verify(pointOfServiceService, never()).getGeoPoint(pos2);
        verify(vmkPointOfServiceLocatorSearchFacade, never()).search(null, geoPoint, 1_000_000, SOLR_OFFSET, DEFAULT_SORT, singletonList(POS_NAME1));
    }

    @Test
    public void testAlternativeTecStoresFromCartWithNoCurrentStore() {
        when(cart.getDeliveryPointOfService()).thenReturn(null);
        when(pointOfServiceService.getAlternativeTECStoresWhichAreCurrentlyOpenForCart(cart)).thenReturn(Arrays.asList(pos1, pos2));

        GeoPoint geoPoint = new GeoPoint();
        when(pointOfServiceService.getGeoPoint(pos1)).thenReturn(geoPoint);


        List<PointOfServiceData> actualTECStores = pointOfServiceFacade.findAlternativePointOfServicesCloseByCurrentStore(cart);

        Assertions
                .assertThat(actualTECStores)
                .isEmpty();

        verify(pointOfServiceService).getAlternativeTECStoresWhichAreCurrentlyOpenForCart(cart);
        verify(pointOfServiceService, never()).getGeoPoint(pos1);
        verify(pointOfServiceService, never()).getGeoPoint(pos2);
        verify(vmkPointOfServiceLocatorSearchFacade, never()).search(null, geoPoint, 1_000_000, SOLR_OFFSET, DEFAULT_SORT, singletonList(POS_NAME1));
    }

    @Test
    public void testGetFallbackPOS() {
        PointOfServiceModel pos = mock(PointOfServiceModel.class);
        BaseStoreModel baseStore = mock(BaseStoreModel.class);

        when(baseStoreService.getCurrentBaseStore()).thenReturn(baseStore);
        when(baseStore.getPointsOfService()).thenReturn(singletonList(pos));

        PointOfServiceModel result = pointOfServiceFacade.getFallbackPOS();

        assertThat(result).isEqualTo(pos);
    }
}
