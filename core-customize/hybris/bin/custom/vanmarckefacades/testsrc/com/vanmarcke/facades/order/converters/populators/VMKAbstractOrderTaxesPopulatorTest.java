package com.vanmarcke.facades.order.converters.populators;

import com.vanmarcke.core.model.CommerceGroupModel;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.price.TaxModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.europe1.enums.UserTaxGroup;
import de.hybris.platform.europe1.model.TaxRowModel;
import de.hybris.platform.testframework.Assert;
import de.hybris.platform.util.TaxValue;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import static org.fest.assertions.Assertions.assertThat;
import static org.fest.assertions.MapAssert.entry;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKAbstractOrderTaxesPopulatorTest {

    @Mock
    private PriceDataFactory priceDataFactory;

    @Spy
    @InjectMocks
    private VMKAbstractOrderTaxesPopulator blueAbstractOrderTaxesPopulator;

    @Test
    public void testPopulate() {
        AbstractOrderModel source = mock(AbstractOrderModel.class);
        AbstractOrderData target = new AbstractOrderData();
        CMSSiteModel currentSite = mock(CMSSiteModel.class);
        CommerceGroupModel commerceGroup = mock(CommerceGroupModel.class);
        CurrencyModel currencyModel = mock(CurrencyModel.class);
        List<AbstractOrderEntryModel> entries = new ArrayList<>();
        AbstractOrderEntryModel entry = mock(AbstractOrderEntryModel.class);
        entries.add(entry);
        ProductModel product = mock(ProductModel.class);
        Collection<TaxRowModel> allTaxes = new ArrayList<>();
        TaxRowModel bebat = mock(TaxRowModel.class);
        TaxRowModel recupel = mock(TaxRowModel.class);
        allTaxes.add(bebat);
        allTaxes.add(recupel);
        Collection<TaxValue> taxValues = new ArrayList<>();
        TaxValue vatTax = mock(TaxValue.class);
        TaxValue valueBebat = mock(TaxValue.class);
        TaxValue valueRecupel = mock(TaxValue.class);
        taxValues.add(vatTax);
        taxValues.add(valueBebat);
        taxValues.add(valueRecupel);
        TaxModel taxBebat = mock(TaxModel.class);
        TaxModel taxRecupel = mock(TaxModel.class);
        PriceData vatPriceData = mock(PriceData.class);
        PriceData otherProductTaxesPriceData = mock(PriceData.class);
        UserModel userModel = mock(UserModel.class);
        AbstractOrderModel orderModel = mock(AbstractOrderModel.class);
        when(orderModel.getUser()).thenReturn(userModel);
        when(source.getSite()).thenReturn(currentSite);
        when(currentSite.getCommerceGroup()).thenReturn(commerceGroup);
        when(commerceGroup.getUserTaxGroup()).thenReturn(UserTaxGroup.valueOf("BE"));
        when(source.getCurrency()).thenReturn(currencyModel);
        when(currencyModel.getIsocode()).thenReturn("EUR");
        when(source.getEntries()).thenReturn(entries);
        when(entry.getProduct()).thenReturn(product);
        when(product.getEurope1Taxes()).thenReturn(allTaxes);

        when(bebat.getUg()).thenReturn(UserTaxGroup.valueOf("BE"));
        when(recupel.getUg()).thenReturn(UserTaxGroup.valueOf("BE"));

        when(bebat.getProduct()).thenReturn(product);
        when(recupel.getProduct()).thenReturn(product);
        when(source.getTotalTaxValues()).thenReturn(taxValues);

        when(bebat.getTax()).thenReturn(taxBebat);
        when(taxBebat.getCode()).thenReturn("BE-BEBAT-0.88");
        when(valueBebat.getCode()).thenReturn("BE-BEBAT-0.88");
        when(recupel.getTax()).thenReturn(taxRecupel);
        when(taxRecupel.getCode()).thenReturn("BE-RECUPEL-0.41");
        when(valueRecupel.getCode()).thenReturn("BE-RECUPEL-0.41");
        when(vatTax.getCode()).thenReturn("VAT");

        when(vatTax.getAppliedValue()).thenReturn(1.21);
        when(valueBebat.getAppliedValue()).thenReturn(0.88);
        when(valueRecupel.getAppliedValue()).thenReturn(0.41);
        when(priceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(1.21), "EUR")).thenReturn(vatPriceData);
        when(priceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(1.29), "EUR")).thenReturn(otherProductTaxesPriceData);
        this.blueAbstractOrderTaxesPopulator.populate(source, target);

        Map<String, PriceData> result = target.getTaxes();
        assertThat(result).isNotEmpty();
        assertThat(result).includes(entry("VAT", vatPriceData), entry("OTHER", otherProductTaxesPriceData));
    }

    @Test
    public void testPopulateVAT() {
        // given
        TaxValue vatTax1 = mock(TaxValue.class);
        TaxValue vatTax2 = mock(TaxValue.class);
        AbstractOrderModel orderModel = mock(AbstractOrderModel.class);
        Map<String, PriceData> taxesByType = mock(Map.class);
        PriceData priceData = mock(PriceData.class);
        when(vatTax1.getAppliedValue()).thenReturn(1.21);
        when(vatTax1.getCode()).thenReturn("VAT");
        when(vatTax2.getAppliedValue()).thenReturn(1.06);
        when(vatTax2.getCode()).thenReturn("VAT");

        when(priceDataFactory.create(any(PriceDataType.class), any(BigDecimal.class), anyString())).thenReturn(priceData);
        doReturn(List.of(vatTax1, vatTax2)).when(orderModel).getTotalTaxValues();

        // when
        blueAbstractOrderTaxesPopulator.calculateVAT(taxesByType, orderModel, "EUR");

        // then
        ArgumentCaptor<PriceData> argumentCaptor = ArgumentCaptor.forClass(PriceData.class);
        ArgumentCaptor<String> keyCaptor = ArgumentCaptor.forClass(String.class);
        verify(priceDataFactory).create(PriceDataType.BUY, BigDecimal.valueOf(2.27), "EUR");
        verify(taxesByType).put(keyCaptor.capture(), argumentCaptor.capture());
        Assert.assertEquals("VAT", keyCaptor.getValue());
        Assert.assertEquals(priceData, argumentCaptor.getValue());
    }


    @Test
    public void testPopulateVAT_noTax() {
        // given
        AbstractOrderModel orderModel = mock(AbstractOrderModel.class);
        Map<String, PriceData> taxesByType = mock(Map.class);
        PriceData priceData = mock(PriceData.class);
        when(priceDataFactory.create(any(PriceDataType.class), any(BigDecimal.class), anyString())).thenReturn(priceData);
        doReturn(List.of()).when(orderModel).getTotalTaxValues();

        // when
        blueAbstractOrderTaxesPopulator.calculateVAT(taxesByType, orderModel, "EUR");

        // then
        ArgumentCaptor<PriceData> argumentCaptor = ArgumentCaptor.forClass(PriceData.class);
        ArgumentCaptor<String> keyCaptor = ArgumentCaptor.forClass(String.class);
        verify(priceDataFactory).create(PriceDataType.BUY, BigDecimal.ZERO, "EUR");
        verify(taxesByType).put(keyCaptor.capture(), argumentCaptor.capture());
        Assert.assertEquals("VAT", keyCaptor.getValue());
        Assert.assertEquals(priceData, argumentCaptor.getValue());
    }


    @Test
    public void testPopulate_noEntries() {
        AbstractOrderModel source = mock(AbstractOrderModel.class);
        AbstractOrderData target = new AbstractOrderData();
        List<AbstractOrderEntryModel> orderEntries = new ArrayList<>();

        when(source.getEntries()).thenReturn(orderEntries);
        this.blueAbstractOrderTaxesPopulator.populate(source, target);

        Map<String, PriceData> result = target.getTaxes();
        assertThat(result).isNull();
    }

}