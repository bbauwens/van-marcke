package com.vanmarcke.facades.stock.converters.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.StockData;
import de.hybris.platform.core.model.product.ProductModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verifyZeroInteractions;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKStockPopulatorTest {

    @InjectMocks
    private VMKStockPopulator<ProductModel, StockData> populator;

    @Test
    public void testPopulate() {
        ProductModel source = mock(ProductModel.class);
        StockData target = mock(StockData.class);

        populator.populate(source, target);

        verifyZeroInteractions(source, target);
    }

}