package com.vanmarcke.facades.order.converters.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.InvoicePaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKAbstractOrderPaymentInfoPopulatorTest {

    @Mock
    private Converter<AddressModel, AddressData> addressConverter;

    @InjectMocks
    private VMKAbstractOrderPaymentInfoPopulator abstractOrderPaymentInfoPopulator;

    @Test
    public void testPopulate_withInvoicePaymentInfo() {
        AddressModel addressModel = mock(AddressModel.class);
        CartModel cartModel = mock(CartModel.class);
        InvoicePaymentInfoModel paymentInfoModel = mock(InvoicePaymentInfoModel.class);
        AddressData addressData = mock(AddressData.class);

        when(cartModel.getPaymentInfo()).thenReturn(paymentInfoModel);

        when(paymentInfoModel.getPk()).thenReturn(PK.BIG_PK);
        when(paymentInfoModel.getBillingAddress()).thenReturn(addressModel);

        when(addressConverter.convert(addressModel)).thenReturn(addressData);

        CartData result = new CartData();
        abstractOrderPaymentInfoPopulator.populate(cartModel, result);

        assertThat(result.getPaymentInfo()).isNotNull();
        assertThat(result.getPaymentInfo().getId()).isEqualTo("8359490977792");
        assertThat(result.getPaymentInfo().getBillingAddress()).isEqualTo(addressData);
    }

    @Test
    public void testPopulate_withCreditCartPaymentInfo() {
        CartModel cartModel = mock(CartModel.class);
        CreditCardPaymentInfoModel paymentInfoModel = mock(CreditCardPaymentInfoModel.class);

        when(cartModel.getPaymentInfo()).thenReturn(paymentInfoModel);

        CartData result = new CartData();
        abstractOrderPaymentInfoPopulator.populate(cartModel, result);

        assertThat(result.getPaymentInfo()).isNull();

        verifyZeroInteractions(paymentInfoModel);
        verifyZeroInteractions(addressConverter);
    }

    @Test
    public void testPopulate_withoutPaymentInfo() {
        CartModel cartModel = mock(CartModel.class);

        CartData result = new CartData();
        abstractOrderPaymentInfoPopulator.populate(cartModel, result);

        assertThat(result.getPaymentInfo()).isNull();

        verifyZeroInteractions(addressConverter);
    }

}