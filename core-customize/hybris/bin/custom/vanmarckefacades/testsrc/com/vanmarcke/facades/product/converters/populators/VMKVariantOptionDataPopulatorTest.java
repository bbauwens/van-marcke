package com.vanmarcke.facades.product.converters.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.product.data.VariantOptionData;
import de.hybris.platform.commerceservices.price.CommercePriceService;
import de.hybris.platform.commerceservices.url.UrlResolver;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.product.VariantsService;
import de.hybris.platform.util.PriceValue;
import de.hybris.platform.variants.model.VariantAttributeDescriptorModel;
import de.hybris.platform.variants.model.VariantProductModel;
import de.hybris.platform.variants.model.VariantTypeModel;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKVariantOptionDataPopulatorTest {

    private static final Double PRICE_VALUE = 12.5D;

    @Mock
    private VariantsService variantsService;
    @Mock
    private UrlResolver<ProductModel> productModelUrlResolver;
    @Mock
    private CommercePriceService commercePriceService;
    @Mock
    private PriceDataFactory priceDataFactory;
    @InjectMocks
    private VMKVariantOptionDataPopulator populator;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        populator = new VMKVariantOptionDataPopulator();
        populator.setCommercePriceService(commercePriceService);
        populator.setPriceDataFactory(priceDataFactory);
        populator.setProductModelUrlResolver(productModelUrlResolver);
        populator.setVariantsService(variantsService);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testConvertWhenSourceIsNull() {
        populator.populate(null, mock(VariantOptionData.class));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testConvertWhenPrototypeIsNull() {
        populator.populate(mock(VariantProductModel.class), null);
    }

    @Test
    public void testConvert() {
        VariantProductModel source = mock(VariantProductModel.class);
        VariantAttributeDescriptorModel variantAttributeDescriptorModel = mock(VariantAttributeDescriptorModel.class);
        ProductModel baseProduct = mock(ProductModel.class);
        VariantTypeModel variantTypeModel = mock(VariantTypeModel.class);
        PriceInformation priceInformation = mock(PriceInformation.class);
        PriceValue priceValue = mock(PriceValue.class);
        PriceData priceData = mock(PriceData.class);

        given(priceValue.getCurrencyIso()).willReturn("eur");
        given(priceValue.getValue()).willReturn(PRICE_VALUE);
        given(priceInformation.getPriceValue()).willReturn(priceValue);
        given(commercePriceService.getWebPriceForProduct(source)).willReturn(priceInformation);
        given(priceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(priceValue.getValue()), "eur")).willReturn(priceData);
        given(baseProduct.getVariantType()).willReturn(variantTypeModel);
        given(source.getBaseProduct()).willReturn(baseProduct);
        given(variantAttributeDescriptorModel.getQualifier()).willReturn("descQual");
        given(variantsService.getVariantAttributesForVariantType(variantTypeModel)).willReturn(Collections.singletonList(variantAttributeDescriptorModel));
        given(variantsService.getVariantAttributeValue(source, "descQual")).willReturn("attrValue");
        given(productModelUrlResolver.resolve(Matchers.any(ProductModel.class))).willReturn("url/proCode");

        VariantOptionData result = new VariantOptionData();
        populator.populate(source, result);

        assertEquals(1L, (long) result.getVariantOptionQualifiers().size());
        assertEquals("attrValue", result.getVariantOptionQualifiers().iterator().next().getValue());
        assertEquals("url/proCode", result.getUrl());
        assertEquals(priceData, result.getPriceData());
    }

    @Test
    public void testConvertFromPrice() {
        VariantProductModel source = mock(VariantProductModel.class);
        VariantAttributeDescriptorModel variantAttributeDescriptorModel = mock(VariantAttributeDescriptorModel.class);
        ProductModel baseProduct = mock(ProductModel.class);
        VariantTypeModel variantTypeModel = mock(VariantTypeModel.class);
        PriceInformation priceInformation = mock(PriceInformation.class);
        PriceValue priceValue = mock(PriceValue.class);
        PriceData priceData = mock(PriceData.class);
        VariantProductModel subVariantProduct = mock(VariantProductModel.class);

        given(source.getVariants()).willReturn(Collections.singleton(subVariantProduct));
        given(priceValue.getCurrencyIso()).willReturn("eur");
        given(priceValue.getValue()).willReturn(PRICE_VALUE);
        given(priceInformation.getPriceValue()).willReturn(priceValue);
        given(commercePriceService.getFromPriceForProduct(source)).willReturn(priceInformation);
        given(priceDataFactory.create(PriceDataType.FROM, BigDecimal.valueOf(priceValue.getValue()), "eur")).willReturn(priceData);
        given(baseProduct.getVariantType()).willReturn(variantTypeModel);
        given(source.getBaseProduct()).willReturn(baseProduct);
        given(variantAttributeDescriptorModel.getQualifier()).willReturn("descQual");
        given(variantsService.getVariantAttributesForVariantType(variantTypeModel)).willReturn(Collections.singletonList(variantAttributeDescriptorModel));
        given(variantsService.getVariantAttributeValue(source, "descQual")).willReturn("attrValue");
        given(productModelUrlResolver.resolve(Matchers.any(ProductModel.class))).willReturn("url/proCode");

        VariantOptionData result = new VariantOptionData();
        populator.populate(source, result);

        assertEquals(1L, (long) result.getVariantOptionQualifiers().size());
        assertEquals("attrValue", result.getVariantOptionQualifiers().iterator().next().getValue());
        assertEquals("url/proCode", result.getUrl());
        assertEquals(priceData, result.getPriceData());
    }

}