package com.vanmarcke.facades.principal.b2bunit.converters.populators;

import com.vanmarcke.core.enums.ReferenceConditionalType;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2bcommercefacades.company.data.B2BUnitData;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.when;

/**
 * The {@link VMKB2BUnitReferencesPopulatorTest} class contains the unit tests for the
 * {@link VMKB2BUnitReferencesPopulator} class.
 *
 * @author Niels Raemaekers
 * @since 20-03-2021
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKB2BUnitReferencesPopulatorTest {

    @Mock
    B2BUnitModel b2BUnitModel;
    @InjectMocks
    private VMKB2BUnitReferencesPopulator populator;

    @Test
    public void testPopulate() {
        B2BUnitData b2BUnitData = new B2BUnitData();

        when(b2BUnitModel.getRef1()).thenReturn(ReferenceConditionalType.MANDATORY);
        when(b2BUnitModel.getRef2()).thenReturn(ReferenceConditionalType.FORBIDDEN);
        this.populator.populate(b2BUnitModel, b2BUnitData);
        
        assertThat(b2BUnitData.getRef1()).isEqualTo("MANDATORY");
        assertThat(b2BUnitData.getRef2()).isEqualTo("FORBIDDEN");
    }
}
