package com.vanmarcke.facades.principal.customer.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.site.BaseSiteService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKCustomerShowNetPricePopulatorTest {

    private VMKCustomerShowNetPricePopulator blueCustomerPopulator;

    @Mock
    BaseSiteService baseSiteService;

    @Before
    public void setUp() {
        blueCustomerPopulator = new VMKCustomerShowNetPricePopulator(baseSiteService);
    }

    @Test
    public void populateSetsShowNetPriceToTrueWhenSourceValueIsTrue() {
        CustomerModel source = mock(CustomerModel.class);
        CustomerData target = mock(CustomerData.class);
        BaseSiteModel baseSite = mock(BaseSiteModel.class);
        when(source.getShowNetPrice()).thenReturn(true);
        when(baseSiteService.getCurrentBaseSite()).thenReturn(baseSite);
        when(baseSite.getChannel()).thenReturn(SiteChannel.B2B);

        blueCustomerPopulator.populate(source, target);

        verify(target).setShowNetPrice(true);
    }

    @Test
    public void populateSetsShowNetPriceToFalseWhenSourceValueIsNull_siteIsB2B() {
        CustomerModel source = mock(CustomerModel.class);
        CustomerData target = mock(CustomerData.class);
        BaseSiteModel baseSite = mock(BaseSiteModel.class);
        when(source.getShowNetPrice()).thenReturn(null);
        when(baseSiteService.getCurrentBaseSite()).thenReturn(baseSite);
        when(baseSite.getChannel()).thenReturn(SiteChannel.B2B);

        blueCustomerPopulator.populate(source, target);

        verify(target).setShowNetPrice(false);
    }

    @Test
    public void populateSetsShowNetPriceToFalseWhenSourceValueIsNull_siteIsDIY() {
        CustomerModel source = mock(CustomerModel.class);
        CustomerData target = mock(CustomerData.class);
        BaseSiteModel baseSite = mock(BaseSiteModel.class);
        when(source.getShowNetPrice()).thenReturn(null);
        when(baseSiteService.getCurrentBaseSite()).thenReturn(baseSite);
        when(baseSite.getChannel()).thenReturn(SiteChannel.DIY);

        blueCustomerPopulator.populate(source, target);

        verify(target).setShowNetPrice(true);
    }

    @Test
    public void populateSetsShowNetPriceToFalseWhenSourceValueIsFalse_siteIsB2B() {
        CustomerModel source = mock(CustomerModel.class);
        CustomerData target = mock(CustomerData.class);
        BaseSiteModel baseSite = mock(BaseSiteModel.class);
        when(source.getShowNetPrice()).thenReturn(false);
        when(baseSiteService.getCurrentBaseSite()).thenReturn(baseSite);
        when(baseSite.getChannel()).thenReturn(SiteChannel.B2B);

        blueCustomerPopulator.populate(source, target);

        verify(target).setShowNetPrice(false);
    }

    @Test
    public void populateSetsShowNetPriceToFalseWhenSourceValueIsFalse_siteIsDIY() {
        CustomerModel source = mock(CustomerModel.class);
        CustomerData target = mock(CustomerData.class);
        BaseSiteModel baseSite = mock(BaseSiteModel.class);
        when(source.getShowNetPrice()).thenReturn(false);
        when(baseSiteService.getCurrentBaseSite()).thenReturn(baseSite);
        when(baseSite.getChannel()).thenReturn(SiteChannel.DIY);

        blueCustomerPopulator.populate(source, target);

        verify(target).setShowNetPrice(true);
    }
}