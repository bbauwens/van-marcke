package com.vanmarcke.facades.product.converters.populators;

import com.vanmarcke.facades.builder.VariantOptionDataBuilder;
import com.vanmarcke.facades.builder.VariantProductModelBuilder;
import com.vanmarcke.services.product.VMKVariantProductService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.VariantOptionData;
import de.hybris.platform.variants.model.VariantProductModel;
import org.fest.assertions.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKVariantProductPurchasablePopulatorTest {

    @Mock
    private VMKVariantProductService variantProductService;

    @InjectMocks
    private VMKVariantProductPurchasablePopulator populator;

    @Test
    public void testPopulateTargetIsInitiallyNotPurchasable() {
        VariantProductModel source = VariantProductModelBuilder
                .aVariantProductModel()
                .build();

        VariantOptionData target = VariantOptionDataBuilder
                .aVariantOptionData()
                .thatIsNotPurchasable()
                .build();

        populator.populate(source, target);

        Assertions
                .assertThat(target.isPurchasable())
                .isFalse();

        verifyZeroInteractions(variantProductService);
    }

    @Test
    public void testPopulateProductIsPurchasable() {
        VariantProductModel source = VariantProductModelBuilder
                .aVariantProductModel()
                .build();

        VariantOptionData target = VariantOptionDataBuilder
                .aVariantOptionData()
                .thatIsPurchasable()
                .build();

        when(variantProductService.isPurchasable(source)).thenReturn(Boolean.TRUE);

        populator.populate(source, target);

        Assertions
                .assertThat(target.isPurchasable())
                .isTrue();

        verify(variantProductService).isPurchasable(source);
    }

    @Test
    public void testPopulateProductIsNotPurchasable() {
        VariantProductModel source = VariantProductModelBuilder
                .aVariantProductModel()
                .build();

        VariantOptionData target = VariantOptionDataBuilder
                .aVariantOptionData()
                .thatIsPurchasable()
                .build();

        when(variantProductService.isPurchasable(source)).thenReturn(Boolean.FALSE);

        populator.populate(source, target);

        Assertions
                .assertThat(target.isPurchasable())
                .isFalse();

        verify(variantProductService).isPurchasable(source);
    }
}