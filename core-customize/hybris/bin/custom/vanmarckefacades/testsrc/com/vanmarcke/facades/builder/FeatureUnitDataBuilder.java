package com.vanmarcke.facades.builder;

import de.hybris.platform.commercefacades.product.data.FeatureData;
import de.hybris.platform.commercefacades.product.data.FeatureUnitData;

public class FeatureUnitDataBuilder {

    private final FeatureUnitData featureUnitData = new FeatureUnitData();

    /**
     * Private constructor to hide the implicit default one.
     */
    private FeatureUnitDataBuilder() {
    }

    /**
     * A new instance of the {@link FeatureUnitDataBuilder} is created and returned
     *
     * @return the created {@link FeatureUnitDataBuilder} instance
     */
    public static FeatureUnitDataBuilder aFeatureUnitData() {
        return new FeatureUnitDataBuilder();
    }

    /**
     * Sets the given {@code symbol} on the current {@link FeatureUnitData} instance.
     *
     * @param symbol the symbol
     * @return the current {@link FeatureDataBuilder} instance for method chaining
     */
    public FeatureUnitDataBuilder withFeatureValues(String symbol) {
        featureUnitData.setSymbol(symbol);
        return this;
    }

    /**
     * Returns the current {@link FeatureData} instance
     *
     * @return the current {@link FeatureData} instance
     */
    public FeatureUnitData build() {
        return featureUnitData;
    }
}
