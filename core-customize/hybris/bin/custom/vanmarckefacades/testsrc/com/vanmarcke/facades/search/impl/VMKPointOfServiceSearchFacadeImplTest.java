package com.vanmarcke.facades.search.impl;

import com.vanmarcke.core.model.CommerceGroupModel;
import com.vanmarcke.services.commercegroup.VMKCommerceGroupService;
import com.vanmarcke.services.search.solrfacetsearch.VMKSearchService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.storelocator.data.PointOfServiceData;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfig;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfigService;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.model.config.SolrFacetSearchConfigModel;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import de.hybris.platform.solrfacetsearch.search.SearchResult;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;

import static de.hybris.platform.solrfacetsearch.enums.ConverterType.POINT_OF_SERVICE;
import static java.util.Collections.singletonList;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKPointOfServiceSearchFacadeImplTest {

    private static final int PAGE_SIZE = 1_000_000;

    @Mock
    private VMKSearchService searchService;
    @Mock
    private CMSSiteService cmsSiteService;
    @Mock
    private VMKCommerceGroupService vmkCommerceGroupService;
    @Mock
    private FacetSearchConfigService facetSearchConfigService;

    @InjectMocks
    private VMKPointOfServiceSearchFacadeImpl defaultVMKPointOfServiceSearchFacade;

    @Before
    public void setup() {
        this.defaultVMKPointOfServiceSearchFacade.setConverterType(POINT_OF_SERVICE);
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testSearch() throws Exception {
        final CMSSiteModel cmsSiteModel = mock(CMSSiteModel.class);
        final SolrFacetSearchConfigModel storeFinderSolrFacetSearchConfigModel = mock(SolrFacetSearchConfigModel.class);
        final FacetSearchConfig facetSearchConfig = mock(FacetSearchConfig.class);
        final IndexConfig indexConfig = mock(IndexConfig.class);
        final IndexedType indexedType = mock(IndexedType.class);
        final SearchQuery searchQuery = mock(SearchQuery.class);
        final SearchResult searchResult = mock(SearchResult.class);
        final PointOfServiceData pointOfServiceData = mock(PointOfServiceData.class);
        final CommerceGroupModel commerceGroup = mock(CommerceGroupModel.class);
        final CountryModel country = mock(CountryModel.class);

        when(storeFinderSolrFacetSearchConfigModel.getName()).thenReturn("storeFinderSolrFacetSearchConfig");

        when(cmsSiteModel.getStoreFinderSolrFacetSearchConfiguration()).thenReturn(storeFinderSolrFacetSearchConfigModel);

        when(this.cmsSiteService.getCurrentSite()).thenReturn(cmsSiteModel);

        when(this.facetSearchConfigService.getConfiguration("storeFinderSolrFacetSearchConfig")).thenReturn(facetSearchConfig);

        when(facetSearchConfig.getIndexConfig()).thenReturn(indexConfig);

        when(indexConfig.getIndexedTypes()).thenReturn(Collections.singletonMap("indexedType-key", indexedType));

        when(this.searchService.createSearchQuery(eq(facetSearchConfig), eq(indexedType))).thenReturn(searchQuery);

        when(this.vmkCommerceGroupService.getCurrentCommerceGroup()).thenReturn(commerceGroup);

        when(commerceGroup.getCountry()).thenReturn(country);

        when(country.getIsocode()).thenReturn("BE");

        when(this.searchService.search(searchQuery)).thenReturn(searchResult);

        when(searchResult.getResultData(POINT_OF_SERVICE)).thenReturn(singletonList(pointOfServiceData));

        final List<PointOfServiceData> result = this.defaultVMKPointOfServiceSearchFacade.search();
        assertThat(result).containsExactly(pointOfServiceData);

        verify(searchQuery).setPageSize(PAGE_SIZE);
    }

}