package com.vanmarcke.facades.storesession.converters.populator;

import com.vanmarcke.core.model.CommerceGroupModel;
import com.vanmarcke.facades.site.data.BaseSiteData;
import com.vanmarcke.facades.storesession.converters.populators.VMKSitePopulator;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cms2.model.navigation.CMSNavigationNodeModel;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKSitePopulatorTest {

    private final String UID = "blue-be";
    private final String NAME = "Blue België";

    @Mock
    private Converter<CountryModel, CountryData> countryConverter;
    @Mock
    private CountryData countryData;

    @InjectMocks
    public VMKSitePopulator sitePopulator = new VMKSitePopulator(this.countryConverter);

    @Test
    public void testPopulate() {
        final CMSSiteModel source = createBaseSite();
        final BaseSiteData target = new BaseSiteData();

        this.sitePopulator.populate(source, target);

        assertThat(target.getUid()).isEqualTo(this.UID);
        assertThat(target.getName()).isEqualTo(this.NAME);
        assertThat(target.getCountry()).isEqualTo(this.countryData);
        assertThat(target.getStartNavigationNode()).isEqualTo("start-navigation-node");
    }

    private CMSSiteModel createBaseSite() {
        final CMSSiteModel baseSite = mock(CMSSiteModel.class);
        final CommerceGroupModel commerceGroupModel = mock(CommerceGroupModel.class);
        final CountryModel country = mock(CountryModel.class);
        CMSNavigationNodeModel startNavigationNode = mock(CMSNavigationNodeModel.class);

        when(commerceGroupModel.getCountry()).thenReturn(country);

        when(startNavigationNode.getUid()).thenReturn("start-navigation-node");

        when(baseSite.getUid()).thenReturn(this.UID);
        when(baseSite.getName()).thenReturn(this.NAME);
        when(baseSite.getCommerceGroup()).thenReturn(commerceGroupModel);
        when(baseSite.getStartNavigationNode()).thenReturn(startNavigationNode);

        when(this.countryConverter.convert(country)).thenReturn(this.countryData);

        return baseSite;
    }
}