package com.vanmarcke.facades.product.converters.populators;

import com.vanmarcke.services.product.price.VMKNetPriceLookupStrategy;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.commerceservices.price.CommercePriceService;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.util.PriceValue;
import de.hybris.platform.variants.model.VariantProductModel;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;

import static de.hybris.platform.commercefacades.product.data.PriceDataType.BUY;
import static java.math.BigDecimal.TEN;
import static java.util.Collections.singletonList;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKOrderEntryPricePopulatorTest {

    @Mock
    private BaseSiteService baseSiteService;
    @Mock
    private PriceDataFactory priceDataFactory;
    @Mock
    private VMKNetPriceLookupStrategy blueNetPriceLookupStrategy;
    @Mock
    private CommercePriceService commercePriceService;
    @Spy
    @InjectMocks
    private VMKOrderEntryPricePopulator blueOrderEntryPricePopulator;

    @Before
    public void setup() {
        blueOrderEntryPricePopulator.setBaseSiteService(baseSiteService);
        blueOrderEntryPricePopulator.setSiteChannels(singletonList(SiteChannel.B2B));
    }

    @Test
    public void testShouldPopulate_order() {
        final OrderModel order = mock(OrderModel.class);
        final OrderEntryModel source = mock(OrderEntryModel.class);
        BaseSiteModel baseSiteModel = mock(BaseSiteModel.class);

        when(source.getOrder()).thenReturn(order);

        when(baseSiteModel.getChannel()).thenReturn(SiteChannel.B2B);

        when(baseSiteService.getCurrentBaseSite()).thenReturn(baseSiteModel);

        boolean result = blueOrderEntryPricePopulator.shouldPopulate(source);
        assertThat(result).isFalse();
    }

    @Test
    public void testShouldPopulate_cart() {
        final CartModel cart = mock(CartModel.class);
        final CartEntryModel source = mock(CartEntryModel.class);
        BaseSiteModel baseSiteModel = mock(BaseSiteModel.class);

        when(source.getOrder()).thenReturn(cart);

        when(baseSiteModel.getChannel()).thenReturn(SiteChannel.B2B);

        when(baseSiteService.getCurrentBaseSite()).thenReturn(baseSiteModel);

        boolean result = blueOrderEntryPricePopulator.shouldPopulate(source);
        assertThat(result).isTrue();
    }

    @Test
    public void testShouldNotPopulate() {
        final OrderEntryModel source = mock(OrderEntryModel.class);
        BaseSiteModel baseSiteModel = mock(BaseSiteModel.class);

        when(baseSiteModel.getChannel()).thenReturn(SiteChannel.B2B);

        when(baseSiteService.getCurrentBaseSite()).thenReturn(baseSiteModel);

        boolean result = blueOrderEntryPricePopulator.shouldPopulate(source);
        assertThat(result).isFalse();
    }

    @Test
    public void testDoPopulate_withCheckoutMode() {
        final OrderModel order = mock(OrderModel.class);
        final OrderEntryModel source = mock(OrderEntryModel.class);
        final VariantProductModel variantProductModel = mock(VariantProductModel.class);

        when(order.getCheckoutMode()).thenReturn(true);

        when(source.getOrder()).thenReturn(order);
        when(source.getProduct()).thenReturn(variantProductModel);
        doReturn(false).when(blueOrderEntryPricePopulator).shouldPopulateNetPrice(source);
        final OrderEntryData target = mock(OrderEntryData.class);
        blueOrderEntryPricePopulator.doPopulate(source, target);

        verifyZeroInteractions(target);
        verifyZeroInteractions(blueNetPriceLookupStrategy);
        verifyZeroInteractions(commercePriceService);
        verifyZeroInteractions(priceDataFactory);
    }

    @Test
    public void testDoPopulate_withCartModeAndGrossView() {
        final OrderModel order = mock(OrderModel.class);
        final OrderEntryModel source = mock(OrderEntryModel.class);
        final VariantProductModel variantProductModel = mock(VariantProductModel.class);
        final CurrencyModel currencyModel = mock(CurrencyModel.class);

        when(currencyModel.getIsocode()).thenReturn("EUR");

        doReturn(false).when(blueOrderEntryPricePopulator).shouldPopulateNetPrice(source);
        when(order.getCheckoutMode()).thenReturn(false);
        when(order.getCurrency()).thenReturn(currencyModel);

        when(source.getOrder()).thenReturn(order);
        when(source.getProduct()).thenReturn(variantProductModel);

        when(blueNetPriceLookupStrategy.getNetPriceForProduct(variantProductModel)).thenReturn(TEN);

        final PriceData priceData = mock(PriceData.class);

        when(priceDataFactory.create(BUY, TEN, "EUR")).thenReturn(priceData);

        final OrderEntryData target = mock(OrderEntryData.class);
        blueOrderEntryPricePopulator.doPopulate(source, target);

        verify(target).setNetPriceData(priceData);

        verifyZeroInteractions(commercePriceService);
    }

    @Test
    public void testDoPopulate_withCartModeAndNetView() {
        final OrderModel order = mock(OrderModel.class);
        final OrderEntryModel source = mock(OrderEntryModel.class);
        final VariantProductModel variantProductModel = mock(VariantProductModel.class);
        final CurrencyModel currencyModel = mock(CurrencyModel.class);

        when(currencyModel.getIsocode()).thenReturn("EUR");

        doReturn(true).when(blueOrderEntryPricePopulator).shouldPopulateNetPrice(source);

        when(order.getCheckoutMode()).thenReturn(false);
        when(order.getCurrency()).thenReturn(currencyModel);

        when(source.getOrder()).thenReturn(order);
        when(source.getProduct()).thenReturn(variantProductModel);

        PriceInformation priceInformation = mock(PriceInformation.class);
        PriceValue priceValue = mock(PriceValue.class);

        when(priceValue.getValue()).thenReturn(10D);
        when(priceInformation.getPriceValue()).thenReturn(priceValue);
        when(commercePriceService.getWebPriceForProduct(variantProductModel)).thenReturn(priceInformation);

        final PriceData priceData = mock(PriceData.class);

        when(priceDataFactory.create(BUY, BigDecimal.valueOf(10D), "EUR")).thenReturn(priceData);

        final OrderEntryData target = mock(OrderEntryData.class);
        PriceData netPriceData = mock(PriceData.class);
        when(target.getBasePrice()).thenReturn(netPriceData);

        blueOrderEntryPricePopulator.doPopulate(source, target);

        verify(target).setBasePrice(priceData);
        verify(target).setNetPriceData(netPriceData);

        verifyZeroInteractions(blueNetPriceLookupStrategy);
    }

    @Test
    public void testShouldPopulateNetPrice() {
        final OrderModel order = mock(OrderModel.class);
        final OrderEntryModel source = mock(OrderEntryModel.class);
        final BaseSiteModel baseSiteModel = mock(BaseSiteModel.class);

        when(baseSiteModel.getChannel()).thenReturn(SiteChannel.DIY);
        when(baseSiteService.getCurrentBaseSite()).thenReturn(baseSiteModel);

        Assert.assertTrue(blueOrderEntryPricePopulator.shouldPopulateNetPrice(source));

        verify(order, times(0)).getUser();
    }

    @Test
    public void testShouldNotPopulateNetPrice() {
        final OrderModel order = mock(OrderModel.class);
        final OrderEntryModel source = mock(OrderEntryModel.class);
        final UserModel userModel = mock(UserModel.class);
        final BaseSiteModel baseSiteModel = mock(BaseSiteModel.class);

        when(source.getOrder()).thenReturn(order);
        when(order.getIsOrderExport()).thenReturn(false);
        when(order.getUser()).thenReturn(userModel);
        when(userModel.getShowNetPrice()).thenReturn(false);
        when(baseSiteModel.getChannel()).thenReturn(SiteChannel.B2B);
        when(baseSiteService.getCurrentBaseSite()).thenReturn(baseSiteModel);

        Assert.assertFalse(blueOrderEntryPricePopulator.shouldPopulateNetPrice(source));

        verify(order, times(1)).getUser();
    }

    @Test
    public void testShouldPopulateNetPriceWhenIsDIY() {
        final OrderModel order = mock(OrderModel.class);
        final OrderEntryModel source = mock(OrderEntryModel.class);
        final UserModel userModel = mock(UserModel.class);
        final BaseSiteModel baseSiteModel = mock(BaseSiteModel.class);

        when(source.getOrder()).thenReturn(order);
        when(order.getIsOrderExport()).thenReturn(false);
        when(order.getUser()).thenReturn(userModel);
        when(userModel.getShowNetPrice()).thenReturn(false);
        when(baseSiteModel.getChannel()).thenReturn(SiteChannel.B2B);
        when(baseSiteService.getCurrentBaseSite()).thenReturn(baseSiteModel);

        Assert.assertFalse(blueOrderEntryPricePopulator.shouldPopulateNetPrice(source));

        verify(order, times(1)).getUser();
    }
}