package com.vanmarcke.facades.search.converters.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.core.model.c2l.LanguageModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.text.DateFormat;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKTimeDataPopulatorTest {

    @Mock
    private CommerceCommonI18NService commerceCommonI18NService;

    @InjectMocks
    private VMKTimeDataPopulator vmkTimeDataPopulator;

    @Test
    public void testGetDateFormat() {
        LanguageModel languageModel = mock(LanguageModel.class);

        when(commerceCommonI18NService.getCurrentLanguage()).thenReturn(languageModel);

        when(commerceCommonI18NService.getLocaleForLanguage(languageModel)).thenReturn(Locale.FRANCE);

        DateFormat result = vmkTimeDataPopulator.getDateFormat();
        assertThat(result).isNotNull();
        assertThat(result.getTimeZone()).isEqualTo(TimeZone.getTimeZone("UTC"));

        Date date = Date.from(LocalDateTime.of(2019, 8, 4, 20, 45, 16).toInstant(ZoneOffset.of("+02:00")));
        assertThat(result.format(date)).isEqualTo("18:45");
    }

}