package com.vanmarcke.facades.delivery.methodform.populators;

import com.vanmarcke.facades.storesession.VMKStoreSessionFacade;
import com.vanmarcke.services.forms.delivery.DeliveryMethodForm;
import com.vanmarcke.services.model.builder.PointOfServiceModelMockBuilder;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.storelocator.data.PointOfServiceData;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import org.apache.commons.lang.RandomStringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKPickupCurrentTecStorePopulatorTest {

    private static final String POS_NAME = RandomStringUtils.randomAlphabetic(10);

    @Mock
    private VMKStoreSessionFacade storeSessionFacade;

    @InjectMocks
    private VMKPickupCurrentTecStorePopulator populator;

    @Test
    public void testPopulate_withPosOnCart() {
        CartModel cart = mock(CartModel.class);
        DeliveryMethodForm form = new DeliveryMethodForm();

        PointOfServiceModel pos = PointOfServiceModelMockBuilder
                .aPointOfService().withName(POS_NAME).build();
        when(cart.getDeliveryPointOfService()).thenReturn(pos);

        populator.populate(cart, form);

        assertThat(form.getTecUid()).isEqualTo(POS_NAME);
        verifyZeroInteractions(storeSessionFacade);
    }

    @Test
    public void testPopulate_noPosOnCart() {
        CartModel cart = mock(CartModel.class);
        DeliveryMethodForm form = new DeliveryMethodForm();

        PointOfServiceData pos = new PointOfServiceData();
        pos.setName(POS_NAME);

        when(cart.getDeliveryPointOfService()).thenReturn(null);
        when(storeSessionFacade.getCurrentStore()).thenReturn(pos);

        populator.populate(cart, form);

        assertThat(form.getTecUid()).isEqualTo(POS_NAME);
        verify(storeSessionFacade, times(2)).getCurrentStore();
    }
}