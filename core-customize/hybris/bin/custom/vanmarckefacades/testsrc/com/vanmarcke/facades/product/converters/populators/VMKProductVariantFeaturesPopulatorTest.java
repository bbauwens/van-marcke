package com.vanmarcke.facades.product.converters.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.VariantOptionData;
import de.hybris.platform.variants.model.VariantProductModel;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verifyZeroInteractions;

@UnitTest
public class VMKProductVariantFeaturesPopulatorTest {

    private VMKProductVariantFeaturesPopulator blueProductVariantFeaturesPopulator;

    @Before
    public void setUp() {
        blueProductVariantFeaturesPopulator = new VMKProductVariantFeaturesPopulator();
    }

    @Test
    public void doPopulateDoesNothing() {
        VariantProductModel variantProductModel = mock(VariantProductModel.class);
        VariantOptionData variantOptionData = mock(VariantOptionData.class);

        blueProductVariantFeaturesPopulator.doPopulate(variantProductModel, variantOptionData);

        verifyZeroInteractions(variantProductModel);
        verifyZeroInteractions(variantOptionData);
    }
}