package com.vanmarcke.facades.order.converters.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.storelocator.data.PointOfServiceData;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKAbstractOrderDeliveryPointOfServicePopulatorTest {

    @Mock
    private Converter<PointOfServiceModel, PointOfServiceData> pointOfServiceConverter;
    @InjectMocks
    private VMKAbstractOrderDeliveryPointOfServicePopulator blueAbstractOrderDeliveryPointOfServicePopulator;

    @Test
    public void populate() {
        PointOfServiceModel pointOfServiceModel = mock(PointOfServiceModel.class);
        AbstractOrderModel source = mock(AbstractOrderModel.class);
        PointOfServiceData pointOfServiceData = mock(PointOfServiceData.class);

        when(source.getDeliveryPointOfService()).thenReturn(pointOfServiceModel);

        AbstractOrderData result = new AbstractOrderData();
        when(pointOfServiceConverter.convert(pointOfServiceModel)).thenReturn(pointOfServiceData);

        blueAbstractOrderDeliveryPointOfServicePopulator.populate(source, result);

        assertThat(result.getDeliveryPointOfService()).isEqualTo(pointOfServiceData);
    }

    @Test
    public void populate_withoutPointOfService() {
        AbstractOrderModel source = mock(AbstractOrderModel.class);

        AbstractOrderData result = new AbstractOrderData();
        blueAbstractOrderDeliveryPointOfServicePopulator.populate(source, result);

        assertThat(result.getDeliveryPointOfService()).isNull();

        verifyZeroInteractions(pointOfServiceConverter);
    }

}