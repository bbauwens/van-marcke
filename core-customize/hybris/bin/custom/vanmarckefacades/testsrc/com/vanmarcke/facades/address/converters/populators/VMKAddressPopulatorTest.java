package com.vanmarcke.facades.address.converters.populators;

import com.vanmarcke.services.model.builder.AddressModelMockBuilder;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.apache.commons.lang3.RandomStringUtils;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Map;

import static org.mockito.Mockito.when;

/**
 * The {@link VMKAddressPopulatorTest} class contain the unit tests for the {@link VMKAddressPopulator}
 * class.
 *
 * @author Christiaan Janssen
 * @since 23/09/2020
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKAddressPopulatorTest {

    private static final String APARTMENT = RandomStringUtils.random(10);
    private static final String MOBILE = RandomStringUtils.random(10);

    @Mock
    private Map<String, Converter<AddressModel, StringBuilder>> addressFormatConverterMap;

    @Mock
    private Converter<AddressModel, StringBuilder> defaultAddressFormatConverter;

    @InjectMocks
    private VMKAddressPopulator addressPopulator;

    @Test
    public void testPopulate() {
        AddressModel addressModel = AddressModelMockBuilder
                .anAddress()
                .withApartment(APARTMENT)
                .withPhone2(MOBILE)
                .build();

        AddressData addressData = new AddressData();

        when(defaultAddressFormatConverter.convert(addressModel)).thenReturn(new StringBuilder());

        addressPopulator.populate(addressModel, addressData);

        Assertions
                .assertThat(addressData.getApartment())
                .isEqualTo(APARTMENT);

        Assertions
                .assertThat(addressData.getMobile())
                .isEqualTo(MOBILE);
    }
}