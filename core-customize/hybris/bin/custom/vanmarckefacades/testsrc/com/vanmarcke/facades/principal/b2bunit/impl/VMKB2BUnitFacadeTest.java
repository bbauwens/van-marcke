package com.vanmarcke.facades.principal.b2bunit.impl;

import com.vanmarcke.facades.principal.b2bunit.VMKB2BUnitFacade;
import com.vanmarcke.services.b2b.VMKB2BUnitService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2b.company.B2BCommerceUnitService;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * The {@link VMKB2BUnitFacadeTest} class contains the unit tests for the
 * {@link VMKB2BUnitFacade} class.
 *
 * @author Niels Raemaekers, Tom van den Berg
 * @since 20-09-2019
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKB2BUnitFacadeTest {

    @Mock
    private VMKB2BUnitService vmkB2BUnitService;

    @Mock
    private Converter<AddressModel, AddressData> addressConverter;

    @Mock
    private ModelService modelService;

    @Mock
    private B2BCommerceUnitService b2BCommerceUnitService;

    @Mock
    private B2BUnitModel b2BUnit;

    @Mock
    private Converter<AddressData, AddressModel> addressReverseConverter;

    @Spy
    @InjectMocks
    private final VMKB2BUnitFacadeImpl vmkB2BUnitFacade = new VMKB2BUnitFacadeImpl(vmkB2BUnitService, addressConverter);

    @Before
    public void setup() {
        when(vmkB2BUnitService.getB2BUnitModelForCurrentUser()).thenReturn(b2BUnit);
    }

    @Test
    public void testGetB2BUnitAddressBook() {
        final AddressModel shippingAddress = mock(AddressModel.class);
        final AddressModel shippingAddress2 = mock(AddressModel.class);
        final AddressModel billingAddress = mock(AddressModel.class);
        final List<AddressModel> allAddresses = new ArrayList<>();
        allAddresses.add(shippingAddress);
        allAddresses.add(shippingAddress2);
        allAddresses.add(billingAddress);

        when(b2BUnit.getAddresses()).thenReturn(allAddresses);
        when(shippingAddress.getShippingAddress()).thenReturn(true);
        when(shippingAddress2.getShippingAddress()).thenReturn(true);
        when(shippingAddress.getVisibleInAddressBook()).thenReturn(true);
        when(shippingAddress2.getVisibleInAddressBook()).thenReturn(false);
        when(billingAddress.getBillingAddress()).thenReturn(true);
        when(billingAddress.getVisibleInAddressBook()).thenReturn(true);

        final List<AddressData> result = vmkB2BUnitFacade.getB2BUnitAddressBook();

        assertThat(result).hasSize(1);
    }

    @Test
    public void testAddDeliveryAddressToUnit() {
        final AddressData addressData = mock(AddressData.class);
        final AddressModel addressModel = mock(AddressModel.class);

        when(modelService.create(AddressModel.class)).thenReturn(addressModel);
        when(addressModel.getPk()).thenReturn(PK.fromLong(1234L));
        when(addressData.isDefaultAddress()).thenReturn(true);
        doNothing().when(vmkB2BUnitFacade).setDefaultDeliveryAddress(addressData);
        vmkB2BUnitFacade.addDeliveryAddressToUnit(addressData);

        verify(vmkB2BUnitFacade).addDeliveryAddressToUnit(addressData);
    }

    @Test
    public void testSetDefaultDeliveryAddress() {
        final AddressData addressData = mock(AddressData.class);
        final AddressModel addressModel = mock(AddressModel.class);

        when(addressData.getId()).thenReturn("1234");
        when(b2BCommerceUnitService.getAddressForCode(b2BUnit, "1234")).thenReturn(addressModel);
        doNothing().when(vmkB2BUnitService).setDefaultDeliveryAddressEntry(b2BUnit, addressModel);
        vmkB2BUnitFacade.setDefaultDeliveryAddress(addressData);

        verify(vmkB2BUnitService).setDefaultDeliveryAddressEntry(b2BUnit, addressModel);
    }

    @Test
    public void testIsDefaultDeliveryAddress() {
        final AddressModel defaultShippingAddress = mock(AddressModel.class);

        when(b2BUnit.getShippingAddress()).thenReturn(defaultShippingAddress);
        when(defaultShippingAddress.getPk()).thenReturn(PK.fromLong(1234L));

        final boolean result = vmkB2BUnitFacade.isDefaultDeliveryAddress("1234");

        assertThat(result).isTrue();
    }

    @Test
    public void testIsDefaultDeliveryAddress_noShipping() {
        final boolean result = vmkB2BUnitFacade.isDefaultDeliveryAddress("1234");

        assertThat(result).isFalse();
    }

    @Test
    public void testEditDeliveryAddress() {
        final AddressData addressData = mock(AddressData.class);
        final AddressModel addressModel = mock(AddressModel.class);

        when(b2BUnit.getUid()).thenReturn("UID");
        when(vmkB2BUnitService.getUnitForUid("UID")).thenReturn(b2BUnit);
        when(addressData.getId()).thenReturn("1234");
        when(b2BCommerceUnitService.getAddressForCode(b2BUnit, "1234")).thenReturn(addressModel);
        when(addressReverseConverter.convert(addressData, addressModel)).thenReturn(addressModel);
        doNothing().when(b2BCommerceUnitService).editAddressEntry(b2BUnit, addressModel);
        when(addressData.isDefaultAddress()).thenReturn(true);
        vmkB2BUnitFacade.editDeliveryAddress(addressData);

        verify(addressReverseConverter).convert(addressData, addressModel);
        verify(b2BCommerceUnitService).editAddressEntry(b2BUnit, addressModel);
        verify(vmkB2BUnitService).setDefaultDeliveryAddressEntry(b2BUnit, addressModel);
    }

    @Test
    public void testRemoveDeliveryAddress() {
        final AddressData addressData = mock(AddressData.class);

        when(b2BUnit.getUid()).thenReturn("UID");
        when(addressData.getId()).thenReturn("1234");
        vmkB2BUnitFacade.removeDeliveryAddress(addressData);
    }

    @Test
    public void testCanAddReference() {
        when(vmkB2BUnitService.mustAddReference()).thenReturn(true);
        boolean result = vmkB2BUnitFacade.mustAddReference();

        verify(vmkB2BUnitService).mustAddReference();
        assertThat(result).isEqualTo(true);
    }

    @Test
    public void testMandatorySeparateInvoiceReference() {
        when(vmkB2BUnitService.mandatorySeparateInvoiceReference()).thenReturn(true);
        boolean result = vmkB2BUnitFacade.mandatorySeparateInvoiceReference();

        verify(vmkB2BUnitService).mandatorySeparateInvoiceReference();
        assertThat(result).isEqualTo(true);
    }
}
