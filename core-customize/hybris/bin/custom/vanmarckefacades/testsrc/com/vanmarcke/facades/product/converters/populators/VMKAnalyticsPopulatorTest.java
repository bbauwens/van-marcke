package com.vanmarcke.facades.product.converters.populators;

import com.vanmarcke.core.model.BrandCategoryModel;
import com.vanmarcke.facades.analytics.data.AnalyticsData;
import com.vanmarcke.services.model.builder.ProductModelMockBuilder;
import com.vanmarcke.services.model.builder.VariantProductModelMockBuilder;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cmsfacades.util.builder.CategoryModelBuilder;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.variants.model.VariantProductModel;
import org.apache.commons.lang3.LocaleUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.fest.assertions.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;
import java.util.Locale;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * THe {@code VMKAnalyticsPopulatorTest} class contains the unit tests for the {@link VMKAnalyticsPopulator} class.
 *
 * @author Joris Cryns, Christiaan Janssen
 * @since 27-09-2019
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKAnalyticsPopulatorTest {

    private static final String CATEGORY = RandomStringUtils.random(10);
    private static final String BRAND = RandomStringUtils.random(10);
    private static final String NAME = RandomStringUtils.random(10);
    private static final Locale LOCALE = LocaleUtils.toLocale("nl_BE");

    @Mock
    private CategoryService categoryService;

    @Spy
    @InjectMocks
    private VMKAnalyticsPopulator vmkAnalyticsPopulator;

    @Test
    public void testPopulate() {
        CategoryModel category = CategoryModelBuilder
                .aModel()
                .withName(CATEGORY, LOCALE)
                .build();

        CategoryModel brand = CategoryModelBuilder
                .aModel()
                .withName(BRAND, LOCALE)
                .build();

        ProductModel product = ProductModelMockBuilder
                .aBaseProduct(RandomStringUtils.random(10), LOCALE)
                .withName(NAME, LOCALE)
                .build();

        doReturn(product).when(vmkAnalyticsPopulator).getBaseProduct(product);

        when(categoryService.getCategoryPathForProduct(product, CategoryModel.class)).thenReturn(Collections.singletonList(category));
        when(categoryService.getCategoryPathForProduct(product, BrandCategoryModel.class)).thenReturn(Collections.singletonList(brand));

        AnalyticsData analyticsData = new AnalyticsData();

        vmkAnalyticsPopulator.populate(product, analyticsData);

        Assertions
                .assertThat(analyticsData.getCategory())
                .isEqualTo(CATEGORY);

        Assertions
                .assertThat(analyticsData.getBrand())
                .isEqualTo(BRAND);

        verify(vmkAnalyticsPopulator).getBaseProduct(product);
        verify(categoryService).getCategoryPathForProduct(product, CategoryModel.class);
        verify(categoryService).getCategoryPathForProduct(product, BrandCategoryModel.class);
    }

    @Test
    public void testGetBaseProductWithBaseProduct() {
        ProductModel expected = ProductModelMockBuilder
                .aBaseProduct(RandomStringUtils.random(10), LOCALE)
                .build();

        ProductModel actual = vmkAnalyticsPopulator.getBaseProduct(expected);

        Assertions
                .assertThat(actual)
                .isEqualTo(expected);
    }

    @Test
    public void testGetBaseProductWithVariantProduct() {
        ProductModel expected = ProductModelMockBuilder
                .aBaseProduct(RandomStringUtils.random(10), LOCALE)
                .build();

        VariantProductModel variant = VariantProductModelMockBuilder
                .aVariantProduct(RandomStringUtils.random(10), Locale.CANADA)
                .withBaseProduct(expected)
                .build();

        ProductModel actual = vmkAnalyticsPopulator.getBaseProduct(variant);

        Assertions
                .assertThat(actual)
                .isEqualTo(expected);
    }
}