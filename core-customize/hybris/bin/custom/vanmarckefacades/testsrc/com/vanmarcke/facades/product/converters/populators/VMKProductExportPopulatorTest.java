package com.vanmarcke.facades.product.converters.populators;

import com.vanmarcke.core.constants.VanmarckeCoreConstants;
import com.vanmarcke.core.enums.PackagingType;
import com.vanmarcke.core.model.BrandCategoryModel;
import com.vanmarcke.core.model.CommerceGroupModel;
import com.vanmarcke.core.model.VanMarckeVariantProductModel;
import com.vanmarcke.core.model.VanmarckeCountryChannelModel;
import com.vanmarcke.services.data.ProductExportItem;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.urlresolver.SiteBaseUrlResolutionService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaFormatModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.europe1.enums.UserTaxGroup;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.store.BaseStoreModel;
import org.apache.commons.lang3.time.FastDateFormat;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.*;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.fest.assertions.Assertions.assertThat;
import static org.fest.assertions.MapAssert.entry;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@UnitTest
public class VMKProductExportPopulatorTest {

    private static final String DOCUMENT_URL1 = "https://someDomain.be/document1_en.pdf";
    private static final String DOCUMENT_URL2 = "https://someDomain.be/document2_en.pdf";
    private static final Locale LOCALE = Locale.ENGLISH;
    private static final String DELIMITER = ",";
    private static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    private static final String BASE_SITE_URL = "https://blue.vanmarcke.com/";

    @Mock
    private CommonI18NService commonI18NService;

    @Mock
    private PriceDataFactory priceDataFactory;

    @Mock
    private SiteBaseUrlResolutionService siteBaseUrlResolutionService;

    @Mock
    private CMSSiteService cmsSiteService;

    @InjectMocks
    private VMKProductExportPopulator productExportPopulator;

    @Test
    public void testPopulate_success() throws ParseException {
        Date creationTime = Date.from(LocalDateTime.of(2020, 1, 4, 12, 45, 16).toInstant(ZoneOffset.of("+01:00")));
        Date modifiedTime = Date.from(LocalDateTime.of(2020, 1, 8, 12, 45, 16).toInstant(ZoneOffset.of("+01:00")));

        LanguageModel activeLanguage = mock(LanguageModel.class);
        when(activeLanguage.getActive()).thenReturn(true);

        LanguageModel inactiveLanguage = mock(LanguageModel.class);
        when(inactiveLanguage.getActive()).thenReturn(false);

        when(commonI18NService.getAllLanguages()).thenReturn(asList(activeLanguage, inactiveLanguage));
        when(commonI18NService.getLocaleForLanguage(activeLanguage)).thenReturn(Locale.ENGLISH);

        CategoryModel category1 = mock(CategoryModel.class);
        when(category1.getCode()).thenReturn("category-code-1");

        CategoryModel category2 = mock(CategoryModel.class);
        when(category2.getCode()).thenReturn("category-code-2");

        CategoryModel brandCategory = mock(BrandCategoryModel.class);
        when(brandCategory.getCode()).thenReturn("brand-cat");
        when(brandCategory.getName(LOCALE)).thenReturn("brand-en-name");

        MediaModel techDataSheet = mock(MediaModel.class);
        when(techDataSheet.getURL()).thenReturn("https://someDomain.be/techDataSheet_en.pdf");

        MediaModel certificate1 = mock(MediaModel.class);
        when(certificate1.getURL()).thenReturn(DOCUMENT_URL1);

        MediaModel certificate2 = mock(MediaModel.class);
        when(certificate2.getURL()).thenReturn(DOCUMENT_URL2);

        Collection<MediaModel> certificates = new ArrayList<>();
        certificates.add(certificate1);
        certificates.add(certificate2);

        MediaModel instructionManual1 = mock(MediaModel.class);
        when(instructionManual1.getURL()).thenReturn(DOCUMENT_URL1);

        MediaModel instructionManual2 = mock(MediaModel.class);
        when(instructionManual2.getURL()).thenReturn(DOCUMENT_URL2);

        Collection<MediaModel> instructionManuals = new ArrayList<>();
        instructionManuals.add(instructionManual1);
        instructionManuals.add(instructionManual2);

        MediaModel maintenanceManual1 = mock(MediaModel.class);
        when(maintenanceManual1.getURL()).thenReturn(DOCUMENT_URL1);

        MediaModel maintenanceManual2 = mock(MediaModel.class);
        when(maintenanceManual2.getURL()).thenReturn(DOCUMENT_URL2);

        Collection<MediaModel> maintenanceManuals = new ArrayList<>();
        maintenanceManuals.add(maintenanceManual1);
        maintenanceManuals.add(maintenanceManual2);

        MediaModel userManual1 = mock(MediaModel.class);
        when(userManual1.getURL()).thenReturn(DOCUMENT_URL1);

        MediaModel userManual2 = mock(MediaModel.class);
        when(userManual2.getURL()).thenReturn(DOCUMENT_URL2);

        Collection<MediaModel> userManuals = new ArrayList<>();
        userManuals.add(userManual1);
        userManuals.add(userManual2);

        MediaModel generalManual1 = mock(MediaModel.class);
        when(generalManual1.getURL()).thenReturn(DOCUMENT_URL1);

        MediaModel generalManual2 = mock(MediaModel.class);
        when(generalManual2.getURL()).thenReturn(DOCUMENT_URL2);

        Collection<MediaModel> generalManuals = new ArrayList<>();
        generalManuals.add(generalManual1);
        generalManuals.add(generalManual2);

        ProductModel baseProduct = mock(ProductModel.class);
        CategoryModel baseProductCategory = mock(CategoryModel.class);
        when(baseProduct.getSupercategories()).thenReturn(List.of(baseProductCategory));
        when(baseProductCategory.getName(Locale.ENGLISH)).thenReturn("cat_name_en");
        when(baseProductCategory.getCode()).thenReturn("cat_code");

        CountryModel productCountry = mock(CountryModel.class);
        VanmarckeCountryChannelModel vanmarckeCountryChannel = mock(VanmarckeCountryChannelModel.class);
        when(vanmarckeCountryChannel.getChannels()).thenReturn(Set.of(SiteChannel.B2B, SiteChannel.B2C));
        when(vanmarckeCountryChannel.getCountry()).thenReturn(productCountry);
        when(productCountry.getIsocode()).thenReturn("BE");

        PriceRowModel priceRow1 = mock(PriceRowModel.class);
        CurrencyModel currency = mock(CurrencyModel.class);
        when(priceRow1.getCurrency()).thenReturn(currency);
        when(priceRow1.getUg()).thenReturn(UserTaxGroup.valueOf("BE"));
        Double priceValue = 10.00;
        when(priceRow1.getPrice()).thenReturn(priceValue);
        PriceData mockPriceData = mock(PriceData.class);
        when(priceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(priceValue), currency)).thenReturn(mockPriceData);
        when(mockPriceData.getFormattedValue()).thenReturn("10,00 €");

        CommerceGroupModel commerceGroup = mock(CommerceGroupModel.class);
        CMSSiteModel site = mock(CMSSiteModel.class);
        LanguageModel lang = mock(LanguageModel.class);
        BaseStoreModel baseStore = mock(BaseStoreModel.class);
        when(cmsSiteService.getSites()).thenReturn(Set.of(site));
        when(site.getCommerceGroup()).thenReturn(commerceGroup);
        when(site.getStores()).thenReturn(List.of(baseStore));
        when(lang.getIsocode()).thenReturn("BE_nl");
        when(baseStore.getLanguages()).thenReturn(Set.of(lang));
        when(commerceGroup.getCountry()).thenReturn(productCountry);
        when(siteBaseUrlResolutionService.getWebsiteUrlForSite(site, true, "/")).thenReturn(BASE_SITE_URL);

        VanMarckeVariantProductModel product = mock(VanMarckeVariantProductModel.class);
        when(product.getCode()).thenReturn("variant-product-code");
        when(product.getName(Locale.ENGLISH)).thenReturn("variant product name");
        when(product.getSupercategories()).thenReturn(asList(category1, category2, brandCategory));
        when(product.getVendorItemNumber_current()).thenReturn("123");
        when(product.getVendorItemBarcode_current()).thenReturn("456");
        when(product.getVanmarckeItemBarcode()).thenReturn("789");
        when(product.getPackaging()).thenReturn(PackagingType.ZAKJE_KOPKAART);
        when(product.getContentPackaging()).thenReturn("contentPackaging");
        when(product.getTech_data_sheet(Locale.ENGLISH)).thenReturn(techDataSheet);
        when(product.getCreationtime()).thenReturn(creationTime);
        when(product.getModifiedtime()).thenReturn(modifiedTime);
        when(product.getCertificates(LOCALE)).thenReturn(certificates);
        when(product.getInstructionManuals(LOCALE)).thenReturn(instructionManuals);
        when(product.getMaintenanceManuals(LOCALE)).thenReturn(maintenanceManuals);
        when(product.getUserManuals(LOCALE)).thenReturn(userManuals);
        when(product.getGeneralManuals(LOCALE)).thenReturn(generalManuals);
        when(product.getBaseProduct()).thenReturn(baseProduct);
        when(product.getCalculatedStatus()).thenReturn("calculated_status");
        when(product.getDeliveryMethod()).thenReturn(VanmarckeCoreConstants.DeliveryMethods.SHIPPING);
        when(product.getSparePart()).thenReturn("spare_part");
        when(product.getVanmarckeCountryChannel()).thenReturn(Set.of(vanmarckeCountryChannel));
        when(product.getEurope1Prices()).thenReturn(Set.of(priceRow1));
        MediaModel thumbnail = mock(MediaModel.class);
        when(product.getThumbnail()).thenReturn(thumbnail);
        when(thumbnail.getURL()).thenReturn("/thumb/url");

        MediaFormatModel xsMediaFormat = mock(MediaFormatModel.class);
        MediaFormatModel mMediaFormat = mock(MediaFormatModel.class);
        MediaFormatModel lMediaFormat = mock(MediaFormatModel.class);
        MediaFormatModel xlMediaFormat = mock(MediaFormatModel.class);
        MediaFormatModel oMediaFormat = mock(MediaFormatModel.class);
        MediaFormatModel scrMediaFormat = mock(MediaFormatModel.class);

        when(xsMediaFormat.getQualifier()).thenReturn("EXTRA_SMALL");
        when(mMediaFormat.getQualifier()).thenReturn("MEDIUM");
        when(lMediaFormat.getQualifier()).thenReturn("LARGE");
        when(xlMediaFormat.getQualifier()).thenReturn("EXTRA_LARGE");
        when(oMediaFormat.getQualifier()).thenReturn("ORIGINAL");
        when(scrMediaFormat.getQualifier()).thenReturn("SCREEN");

        MediaModel image1 = mock(MediaModel.class);
        MediaModel image2 = mock(MediaModel.class);
        MediaModel image3 = mock(MediaModel.class);
        MediaModel image4 = mock(MediaModel.class);
        MediaModel image5 = mock(MediaModel.class);
        MediaModel image6 = mock(MediaModel.class);

        when(image1.getMediaFormat()).thenReturn(xsMediaFormat);
        when(image1.getDownloadURL()).thenReturn("xs/url");

        when(image2.getMediaFormat()).thenReturn(mMediaFormat);
        when(image2.getDownloadURL()).thenReturn("m/url");

        when(image3.getMediaFormat()).thenReturn(lMediaFormat);
        when(image3.getDownloadURL()).thenReturn("l/url");

        when(image4.getMediaFormat()).thenReturn(xlMediaFormat);
        when(image4.getDownloadURL()).thenReturn("xl/url");

        when(image5.getMediaFormat()).thenReturn(oMediaFormat);
        when(image5.getDownloadURL()).thenReturn("o/url");

        when(image6.getMediaFormat()).thenReturn(scrMediaFormat);
        when(image6.getDownloadURL()).thenReturn("scr/url");

        MediaContainerModel galleryImageContainer = mock(MediaContainerModel.class);
        when(galleryImageContainer.getMedias()).thenReturn(asList(image1, image2, image3, image4, image5, image6));

        when(product.getGalleryImages()).thenReturn(singletonList(galleryImageContainer));

        ProductExportItem result = new ProductExportItem();
        productExportPopulator.populate(product, result);

        assertThat(result.getArticleNumber()).isEqualTo("variant-product-code");
        assertThat(result.getIdentifier()).hasSize(1).includes(entry("en", "variant product name"));
        assertThat(result.getCategories()).isEqualTo("category-code-1" + DELIMITER + "category-code-2" + DELIMITER + "brand-cat");
        assertThat(result.getProperties()).isNullOrEmpty();

        assertThat(result.getThumbnailUrl()).isNotEmpty().isEqualTo("/thumb/url");
        assertThat(result.getVendorItemBarcode()).isNotEmpty().isEqualTo("456");
        assertThat(result.getVendorItemNumber()).isNotEmpty().isEqualTo("123");
        assertThat(result.getVmItemBarcode()).isNotEmpty().isEqualTo("789");
        assertThat(result.getPackaging()).isNotEmpty().isEqualTo("ZAKJE_KOPKAART");
        assertThat(result.getContentPackaging()).isNotEmpty().isEqualTo("contentPackaging");
        assertThat(result.getTechnicalDataSheet()).hasSize(1).includes(entry("en", "https://someDomain.be/techDataSheet_en.pdf"));

        assertThat(result.getSmallImage()).isEqualTo("xs/url");
        assertThat(result.getNormalImage()).isEqualTo("l/url");
        assertThat(result.getLargeImage()).isEqualTo("l/url");
        assertThat(result.getExtraLargeImage()).isEqualTo("xl/url");
        assertThat(result.getScreenImage()).isEqualTo("scr/url");

        assertThat(result.getCreationTime()).isEqualTo(FastDateFormat.getInstance(DEFAULT_DATE_FORMAT).format(creationTime));
        assertThat(result.getModifiedTime()).isEqualTo(FastDateFormat.getInstance(DEFAULT_DATE_FORMAT).format(modifiedTime));

        assertThat(result.getCalculatedStatus()).isEqualTo("calculated_status");
        assertThat(result.getDeliveryMethod()).isEqualTo(VanmarckeCoreConstants.DeliveryMethods.SHIPPING);
        assertThat(result.getSparePart()).isEqualTo("spare_part");

        assertThat(result.getCertification().get(LOCALE.toString())).isEqualTo(DOCUMENT_URL1 + DELIMITER + DOCUMENT_URL2);
        assertThat(result.getInstructionManual().get(LOCALE.toString())).isEqualTo(DOCUMENT_URL1 + DELIMITER + DOCUMENT_URL2);
        assertThat(result.getMaintenanceManual().get(LOCALE.toString())).isEqualTo(DOCUMENT_URL1 + DELIMITER + DOCUMENT_URL2);
        assertThat(result.getUserManual().get(LOCALE.toString())).isEqualTo(DOCUMENT_URL1 + DELIMITER + DOCUMENT_URL2);
        assertThat(result.getGeneralManual().get(LOCALE.toString())).isEqualTo(DOCUMENT_URL1 + DELIMITER + DOCUMENT_URL2);
        assertThat(result.getBrandCategory()).isEqualTo("brand-cat");
        assertThat(result.getBrandCategoryName().get(LOCALE.toString())).isEqualTo("brand-en-name");
        assertThat(result.getBaseProductSuperCategory()).isEqualTo("cat_code");
        assertThat(result.getBaseProductSuperCategoryName().get(LOCALE.toString())).isEqualTo("cat_name_en");
        assertThat(result.getCountryChannels().split(",")).contains("BE-B2B", "BE-B2C");
        assertThat(result.getPrices().get("BE")).isEqualTo("10,00 €");
        assertThat(result.getUrls().get("BE_nl")).isEqualTo(BASE_SITE_URL + "BE_nl/p/variant-product-code");
    }
}