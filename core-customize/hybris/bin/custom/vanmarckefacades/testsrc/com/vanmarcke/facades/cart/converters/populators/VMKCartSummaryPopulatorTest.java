package com.vanmarcke.facades.cart.converters.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import org.apache.commons.lang.RandomStringUtils;
import org.fest.assertions.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKCartSummaryPopulatorTest {

    private static final String DESCRIPTION = RandomStringUtils.randomAlphabetic(10);
    private static final String NAME = RandomStringUtils.randomAlphabetic(10);
    private static final String CODE = RandomStringUtils.randomAlphabetic(10);
    private static final long EPOCH_SECOND = 1598356729;
    private static final Date SAVE_TIME = Date.from(Instant.ofEpochSecond(EPOCH_SECOND));

    @InjectMocks
    private VMKCartSummaryPopulator populator;

    @Test
    public void testPopulate() {
        CartModel source = mock(CartModel.class);
        CartData target = new CartData();
        List<AbstractOrderEntryModel> orderEntries = new ArrayList<>();
        AbstractOrderEntryModel entry = mock(AbstractOrderEntryModel.class);
        orderEntries.add(entry);
        orderEntries.add(entry);
        orderEntries.add(entry);

        when(source.getDescription()).thenReturn(DESCRIPTION);
        when(source.getName()).thenReturn(NAME);
        when(source.getSaveTime()).thenReturn(SAVE_TIME);
        when(source.getEntries()).thenReturn(orderEntries);
        when(source.getCode()).thenReturn(CODE);

        populator.populate(source, target);

        Assertions
                .assertThat(target.getTotalUnitCount())
                .isEqualTo(3);

        Assertions
                .assertThat(target.getName())
                .isEqualTo(NAME);

        Assertions
                .assertThat(target.getDescription())
                .isEqualTo(DESCRIPTION);

        Assertions
                .assertThat(target.getCode())
                .isEqualTo(CODE);
    }
}