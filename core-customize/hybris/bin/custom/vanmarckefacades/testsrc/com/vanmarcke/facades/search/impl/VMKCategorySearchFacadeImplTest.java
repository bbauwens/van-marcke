package com.vanmarcke.facades.search.impl;

import com.vanmarcke.services.search.solrfacetsearch.VMKSearchService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.core.model.type.ComposedTypeModel;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfig;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfigService;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.config.exceptions.FacetConfigServiceException;
import de.hybris.platform.solrfacetsearch.enums.ConverterType;
import de.hybris.platform.solrfacetsearch.model.config.SolrFacetSearchConfigModel;
import de.hybris.platform.solrfacetsearch.model.config.SolrIndexedTypeModel;
import de.hybris.platform.solrfacetsearch.search.FacetSearchException;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import de.hybris.platform.solrfacetsearch.search.SearchResult;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKCategorySearchFacadeImplTest {

    @Mock
    private VMKSearchService vmkSearchService;
    @Mock
    private CMSSiteService cmsSiteService;
    @Mock
    private FacetSearchConfigService facetSearchConfigService;
    @InjectMocks
    private VMKCategorySearchFacadeImpl defaultCategorySearchFacade;

    @Test
    public void testTextSearch() throws FacetSearchException, FacetConfigServiceException {
        SearchQuery searchQuery = mock(SearchQuery.class);
        SearchResult searchResult = mock(SearchResult.class);
        CMSSiteModel cmsSiteModel = mock(CMSSiteModel.class);
        IndexedType indexedType = mock(IndexedType.class);
        IndexConfig indexConfig = mock(IndexConfig.class);
        SolrFacetSearchConfigModel searchConfigModel = mock(SolrFacetSearchConfigModel.class);
        FacetSearchConfig facetSearchConfig = mock(FacetSearchConfig.class);
        SolrIndexedTypeModel indexedTypeModel = mock(SolrIndexedTypeModel.class);
        ComposedTypeModel composedTypeModel = mock(ComposedTypeModel.class);
        List<SolrIndexedTypeModel> indexedTypeModelList = Collections.singletonList(indexedTypeModel);
        Map<String, IndexedType> indexedTypeMap = new HashMap<>();
        indexedTypeMap.put("Category_IndexName", indexedType);

        when(vmkSearchService.createTextSearchQuery("test", facetSearchConfig, indexedType)).thenReturn(searchQuery);
        when(cmsSiteService.getCurrentSite()).thenReturn(cmsSiteModel);
        when(vmkSearchService.search(searchQuery)).thenReturn(searchResult);
        when(facetSearchConfigService.getConfiguration("SearchConfig")).thenReturn(facetSearchConfig);
        when(cmsSiteModel.getCategorySolrFacetSearchConfiguration()).thenReturn(searchConfigModel);
        when(searchConfigModel.getName()).thenReturn("SearchConfig");
        when(searchConfigModel.getSolrIndexedTypes()).thenReturn(indexedTypeModelList);
        when(indexedTypeModel.getType()).thenReturn(composedTypeModel);
        when(indexedTypeModel.getIndexName()).thenReturn("IndexName");
        when(composedTypeModel.getName()).thenReturn("Category");
        when(facetSearchConfig.getIndexConfig()).thenReturn(indexConfig);
        when(indexConfig.getIndexedTypes()).thenReturn(indexedTypeMap);

        defaultCategorySearchFacade.setConverterType(ConverterType.CATEGORY);
        defaultCategorySearchFacade.textSearch("test");

        verify(searchResult).getResultData(ConverterType.CATEGORY);
    }

    @Test
    public void testTextSearchWhenError() throws FacetSearchException, FacetConfigServiceException {
        SearchQuery searchQuery = mock(SearchQuery.class);
        SearchResult searchResult = mock(SearchResult.class);
        CMSSiteModel cmsSiteModel = mock(CMSSiteModel.class);
        IndexedType indexedType = mock(IndexedType.class);
        SolrFacetSearchConfigModel searchConfigModel = mock(SolrFacetSearchConfigModel.class);
        FacetSearchConfig facetSearchConfig = mock(FacetSearchConfig.class);

        when(vmkSearchService.createTextSearchQuery("test", facetSearchConfig, indexedType)).thenReturn(searchQuery);
        when(cmsSiteService.getCurrentSite()).thenReturn(cmsSiteModel);
        when(vmkSearchService.search(searchQuery)).thenReturn(searchResult);
        when(facetSearchConfigService.getConfiguration("SearchConfig")).thenThrow(FacetConfigServiceException.class);
        when(cmsSiteModel.getCategorySolrFacetSearchConfiguration()).thenReturn(searchConfigModel);
        when(searchConfigModel.getName()).thenReturn("SearchConfig");

        defaultCategorySearchFacade.setConverterType(ConverterType.CATEGORY);
        List<CategoryData> result = defaultCategorySearchFacade.textSearch("test");

        assertThat(result).isEmpty();
    }
}