package com.vanmarcke.facades.wishlist2.populators;

import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.commerceservices.price.CommercePriceService;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.wishlist2.model.Wishlist2EntryModel;

import java.math.BigDecimal;

public class VMKWishListEntryPricePopulator implements Populator<Wishlist2EntryModel, OrderEntryData> {

    private final CommerceCommonI18NService commerceCommonI18NService;
    private final CommercePriceService commercePriceService;
    private final PriceDataFactory priceDataFactory;

    public VMKWishListEntryPricePopulator(CommerceCommonI18NService commerceCommonI18NService,
                                          CommercePriceService commercePriceService,
                                          PriceDataFactory priceDataFactory) {
        this.commerceCommonI18NService = commerceCommonI18NService;
        this.commercePriceService = commercePriceService;
        this.priceDataFactory = priceDataFactory;
    }

    @Override
    public void populate(Wishlist2EntryModel source, OrderEntryData target) throws ConversionException {
        final ProductModel productModel = source.getProduct();
        final CurrencyModel currencyModel = commerceCommonI18NService.getCurrentCurrency();

        // base price
        final PriceInformation basePriceInfo = commercePriceService.getWebPriceForProduct(productModel);
        final BigDecimal basePriceValue = BigDecimal.valueOf(basePriceInfo.getPriceValue().getValue());
        target.setBasePrice(priceDataFactory.create(PriceDataType.BUY, basePriceValue, currencyModel));

        // total price
        final BigDecimal totalPriceValue = BigDecimal.valueOf(basePriceInfo.getPriceValue().getValue() * source.getDesired());
        target.setTotalPrice(priceDataFactory.create(PriceDataType.BUY, totalPriceValue, currencyModel));
    }
}