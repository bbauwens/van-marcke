package com.vanmarcke.facades.product.converters.populators;

import com.vanmarcke.facades.sitechannel.AbstractSiteChannelAwarePopulator;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import org.springframework.beans.factory.annotation.Required;

/**
 * Populates the selected variant
 *
 * @author Joris Cryns
 * @since 11-7-2019
 */
public class VMKVariantSelectedPopulator<SOURCE extends ProductModel, TARGET extends ProductData> extends AbstractSiteChannelAwarePopulator<SOURCE, TARGET> {

    private Populator<SOURCE, TARGET> variantSelectedPopulator;

    /**
     * {@inheritDoc}
     */
    @Override
    protected void doPopulate(final SOURCE source, final TARGET target) {
        variantSelectedPopulator.populate(source, target);
    }

    /**
     * {@inheritDoc}
     */
    @Required
    public void setVariantSelectedPopulator(final Populator<SOURCE, TARGET> variantSelectedPopulator) {
        this.variantSelectedPopulator = variantSelectedPopulator;
    }
}
