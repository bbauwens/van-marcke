package com.vanmarcke.facades.exportcart.impl;

import com.vanmarcke.facades.csv.VMKCsvFacade;
import com.vanmarcke.facades.data.order.SavedCartData;
import com.vanmarcke.facades.exportcart.VMKExportSavedCartFacade;
import com.vanmarcke.facades.wishlist2.VMKWishListFacade;
import com.vanmarcke.services.exportcart.VMKExportSavedCartsService;
import com.vanmarcke.services.order.cart.conversion.VMKCsvExcelConversionService;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.user.UserService;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.List;

public class VMKExportSavedCartFacadeImpl implements VMKExportSavedCartFacade {

    private final VMKExportSavedCartsService vmkExportSavedCartsService;
    private final VMKCsvFacade vmkCsvFacade;
    private final UserService userService;
    private final VMKCsvExcelConversionService excelService;

    public VMKExportSavedCartFacadeImpl(
            VMKExportSavedCartsService vmkExportSavedCartsService,
            VMKCsvFacade vmkCsvFacade,
            UserService userService, VMKCsvExcelConversionService excelService) {
        this.vmkExportSavedCartsService = vmkExportSavedCartsService;
        this.vmkCsvFacade = vmkCsvFacade;
        this.userService = userService;
        this.excelService = excelService;
    }

    /**
     *
     */
    @Override
    public byte[] generatePdfFromSavedCart(List<String> headers, SavedCartData savedCartData, boolean includeNet, boolean showImages, boolean includeTechnical, int emptyLines) {
        final UserModel currentUser = userService.getCurrentUser();
        return vmkExportSavedCartsService.generatePdf(headers, currentUser, savedCartData, includeNet, showImages, includeTechnical, emptyLines);
    }

    @Override
    public byte[] generateExcelFromSavedCart(List<String> headers, SavedCartData savedCartData, boolean netPrice, int emptyLines) throws IOException {
        try (StringWriter csvWriter = new StringWriter()) {
            // generate a CSV file for the provided headers
            vmkCsvFacade.generateCsvFromWishlist(headers, true, savedCartData, netPrice, csvWriter, emptyLines);

            // convert this CSV data into an Excel workbook
            int[] currencyColumns = netPrice ? new int[]{3, 4, 5} : new int[]{3, 4};
            return excelService.convertCsvToXls(new StringReader(csvWriter.toString()), currencyColumns);
        }
    }
}