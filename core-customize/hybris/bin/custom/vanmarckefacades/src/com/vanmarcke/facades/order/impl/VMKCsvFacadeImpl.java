package com.vanmarcke.facades.order.impl;

import com.elision.hybris.l10n.facades.LocalizedMessageFacade;
import com.vanmarcke.facades.constants.VanmarckeFacadesConstants.I18N;
import com.vanmarcke.facades.csv.VMKCsvFacade;
import com.vanmarcke.facades.data.order.SavedCartData;
import com.vanmarcke.services.constants.VanmarckeservicesConstants.Export;
import com.vanmarcke.services.wishlist2.VMKWishlist2Service;
import com.vanmarcke.services.wishlist2.impl.UpdateWishlistException;
import de.hybris.platform.acceleratorfacades.csv.impl.DefaultCsvFacade;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.wishlist2.model.Wishlist2EntryModel;
import de.hybris.platform.wishlist2.model.Wishlist2Model;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.time.FastDateFormat;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Writer;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.lang.String.format;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.summingLong;

/**
 * This class extends the {@link DefaultCsvFacade} to change the {@code delimiter}.
 * It implements methods to export carts to CSV files.
 *
 * @author Tom van den Berg
 * @since 10-04-2020
 */
public class VMKCsvFacadeImpl extends DefaultCsvFacade implements VMKCsvFacade {

    public static final String DELIMITER = ";";
    private static final long NUMBER_OF_LINES_TO_SKIP_FOR_CSV_IMPORT = 1;

    private static final int WISH_LIST_CSV_PRODUCT_CODE_IDX = 0;
    private static final int WISH_LIST_CSV_QUANTITY_IDX = 1;

    private static final FastDateFormat CSV_IMPORT_DATE_FORMAT = FastDateFormat.getInstance("yyyyMMddHHmmss");
    private static final Logger LOG = Logger.getLogger(VMKCsvFacadeImpl.class);

    private final PriceDataFactory priceDataFactory;
    private final ModelService modelService;
    private final ProductService productService;
    private final VMKWishlist2Service vmkWishlist2Service;
    private final LocalizedMessageFacade localizedMessageFacade;
    private final CommonI18NService commonI18NService;

    /**
     * Creates a new instance of the {@link } class.
     *
     * @param priceDataFactory       the price data factory
     * @param localizedMessageFacade the localized message facade
     * @param commonI18NService      the common i18n service
     */
    public VMKCsvFacadeImpl(final PriceDataFactory priceDataFactory,
                            final ModelService modelService,
                            final ProductService productService,
                            final VMKWishlist2Service vmkWishlist2Service, LocalizedMessageFacade localizedMessageFacade, CommonI18NService commonI18NService) {
        this.priceDataFactory = priceDataFactory;
        this.modelService = modelService;
        this.productService = productService;
        this.vmkWishlist2Service = vmkWishlist2Service;
        this.localizedMessageFacade = localizedMessageFacade;
        this.commonI18NService = commonI18NService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void generateCsvFromCart(List<String> headers, boolean includeHeader, CartData cartData, Writer writer) throws IOException {
        if (includeHeader && CollectionUtils.isNotEmpty(headers)) {
            final StringBuilder csvHeader = new StringBuilder();
            int i = 0;
            for (; i < headers.size() - 1; i++) {
                csvHeader.append(StringEscapeUtils.escapeCsv(headers.get(i))).append(DELIMITER);
            }
            csvHeader.append(StringEscapeUtils.escapeCsv(headers.get(i))).append(LINE_SEPERATOR);
            writer.write(csvHeader.toString());
        }

        if (cartData != null && CollectionUtils.isNotEmpty(cartData.getEntries())) {
            writeOrderEntries(writer, cartData.getEntries());
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void generateCsvFromWishlist(List<String> headers, boolean includeHeader, SavedCartData savedCartData, boolean netPrice, Writer writer, int emptyLines) throws IOException {
        // first empty lines if required
        for (int i = 0; i < emptyLines; i++) {
            writer.append(DELIMITER).append(LINE_SEPERATOR);
        }

        if (includeHeader && CollectionUtils.isNotEmpty(headers)) {
            // concatenate all headers
            final String headersConcatenated = headers.stream()
                    .map(StringEscapeUtils::escapeCsv)
                    .collect(Collectors.joining(DELIMITER));

            // write the header to the buffer
            writer.append(headersConcatenated);
            writer.append(LINE_SEPERATOR);
        }

        if (savedCartData != null && CollectionUtils.isNotEmpty(savedCartData.getEntries())) {
            writeWishListEntries(writer, savedCartData.getEntries(), netPrice);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void writeOrderEntries(final Writer writer, final List<OrderEntryData> entries) throws IOException {
        for (final OrderEntryData entry : entries) {
            if (Boolean.TRUE.equals(entry.getProduct().getMultidimensional())) {
                for (final OrderEntryData subEntry : entry.getEntries()) {
                    writeOrderEntry(writer, subEntry);
                }
            } else {
                writeOrderEntry(writer, entry);
            }
        }
    }

    @Override
    protected void writeOrderEntry(final Writer writer, final OrderEntryData entry) throws IOException {
        super.writeOrderEntry(writer, entry);
    }

    /**
     * {@inheritDoc}
     */
    protected void writeOrderEntry(final Writer writer, final OrderEntryData entry, final boolean showNetPrice) throws IOException {

        final String basePrice = (entry.getBasePrice() == null) ? "-" : String.valueOf(commonI18NService.roundCurrency(entry.getBasePrice().getValue().doubleValue(), Export.CURRENCY_DIGITS));
        final String netPrice = (entry.getNetPriceData() == null) ? "-" : String.valueOf(commonI18NService.roundCurrency(entry.getNetPriceData().getValue().doubleValue(), Export.CURRENCY_DIGITS));
        final String totalPrice = (entry.getTotalPrice() == null) ? "-" : String.valueOf(commonI18NService.roundCurrency(entry.getTotalPrice().getValue().doubleValue(), Export.CURRENCY_DIGITS));

        // generate the content
        final StringBuilder builder = new StringBuilder()
                .append(StringEscapeUtils.escapeCsv(entry.getProduct().getCode())).append(DELIMITER)
                .append(StringEscapeUtils.escapeCsv(entry.getQuantity().toString())).append(DELIMITER)
                .append(StringEscapeUtils.escapeCsv(entry.getProduct().getName())).append(DELIMITER)
                .append(StringEscapeUtils.escapeCsv(basePrice)).append(DELIMITER);
        if (showNetPrice) {
            builder.append(StringEscapeUtils.escapeCsv(netPrice)).append(DELIMITER);
        }
        builder.append(StringEscapeUtils.escapeCsv(totalPrice)).append(LINE_SEPERATOR);

        // write out the full line
        writer.write(builder.toString());
    }

    @Override
    public Wishlist2Model createFromCsv(final UserModel userModel, byte[] csvFileData) throws IOException {

        final Wishlist2Model target = modelService.create(Wishlist2Model.class);

        final String wishListName = format("imported-%s", CSV_IMPORT_DATE_FORMAT.format(new Date()));
        vmkWishlist2Service.addRequiredWishListFields(target, wishListName, userModel);

        final List<String> failedProducts = new ArrayList<>();
        AtomicInteger successCount = new AtomicInteger(0);
        AtomicInteger failedCount = new AtomicInteger(0);

        try (final BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(csvFileData)));
             final Stream<String> lines = bufferedReader.lines()) {

            lines.skip(NUMBER_OF_LINES_TO_SKIP_FOR_CSV_IMPORT)
                    .filter(StringUtils::isNotBlank)
                    .map(line -> line.split(DELIMITER))
                    .filter(lineTokens -> lineTokens.length >= 2)
                    .map(lineTokens -> Pair.of(lineTokens[WISH_LIST_CSV_PRODUCT_CODE_IDX], lineTokens[WISH_LIST_CSV_QUANTITY_IDX]))
                    .collect(groupingBy(Pair::getLeft, summingLong(pair -> Long.parseLong(StringUtils.trim(pair.getRight())))))
                    .forEach((product, qty) -> {
                        try {
                            vmkWishlist2Service.uploadProductToWishlist(product, qty, target);
                            successCount.incrementAndGet();
                        } catch (final UpdateWishlistException | NumberFormatException | UnknownIdentifierException e) {
                            LOG.warn(String.format("Error importing product: %s with quantity: %s from CSV file. Exception: %s", product, qty, e.getMessage()));
                            failedProducts.add(product);
                            failedCount.incrementAndGet();
                        }
                    });
        }
        target.setDescription(geImportSummaryMessage(wishListName, successCount.get(), failedProducts));
        modelService.save(target);
        return target;
    }

    protected void addLinesToCart(String productCode, Long qty, final Wishlist2Model wishlist2Model) {
        // conversion
        final ProductModel product = productService.getProductForCode(productCode);

        // create a new wishlist entry
        final Wishlist2EntryModel wishlist2EntryModel = modelService.create(Wishlist2EntryModel._TYPECODE);
        vmkWishlist2Service.addRequiredWishListEntryFields(wishlist2EntryModel, product, qty.intValue());
        wishlist2Model.getEntries().add(wishlist2EntryModel);
    }

    protected void writeWishListEntries(final Writer writer, final List<OrderEntryData> entries, final boolean netPrice) throws IOException {
        double totalGross = 0.0d;

        for (final OrderEntryData entry : entries) {
            if (Boolean.TRUE.equals(entry.getProduct().getMultidimensional())) {
                for (final OrderEntryData subEntry : entry.getEntries()) {
                    writeOrderEntry(writer, subEntry, netPrice);

                    totalGross = totalGross + subEntry.getTotalPrice().getValue().doubleValue();
                }
            } else {
                writeOrderEntry(writer, entry, netPrice);

                totalGross = totalGross + entry.getTotalPrice().getValue().doubleValue();
            }
        }
        writeTotals(writer, netPrice, commonI18NService.roundCurrency(totalGross, Export.CURRENCY_DIGITS));
    }

    /**
     * We're not allowed to display the net total, so instead we'll have to calculate the gross total.
     *
     * @param quantity  the entry quantity
     * @param basePrice the entry base price
     * @return the gross total
     */
    private PriceData calculateTotal(Long quantity, PriceData basePrice) {
        return priceDataFactory.create(
                basePrice.getPriceType(),
                basePrice.getValue().multiply(BigDecimal.valueOf(quantity)),
                basePrice.getCurrencyIso());
    }

    /**
     * Write the total prices to the exported file depending on showNetPrice flag
     *
     * @param writer       the writer
     * @param showNetPrice the showNetPrice flag
     * @param gross        the gross amount
     * @throws IOException
     */
    protected void writeTotals(final Writer writer, final boolean showNetPrice, final double gross) throws IOException {
        final StringBuilder builder = new StringBuilder()
                .append(DELIMITER)
                .append(DELIMITER)
                .append(DELIMITER)
                .append(DELIMITER);

        if (showNetPrice) {
            builder.append(DELIMITER);
        }

        builder.append(gross).append(DELIMITER).append(LINE_SEPERATOR);

        writer.write(builder.toString());

    }

    private String geImportSummaryMessage(final String wishListName, final int successCount, final List<String> failedProducts) {
        final String summaryMessageKey;
        final Object[] localizationArguments;

        if (CollectionUtils.isNotEmpty(failedProducts)) {
            summaryMessageKey = I18N.TEXT_SAVEDCART_UPLOAD_SUMMARY_NOT_IMPORTED;
            localizationArguments = new Object[]{wishListName, successCount, 0, String.join(", ", failedProducts)};
        } else {
            summaryMessageKey = I18N.TEXT_SAVEDCART_UPLOAD_SUMMARY;
            localizationArguments = new Object[]{wishListName, successCount, 0};
        }
        return localizedMessageFacade.getFormattedMessageForCodeAndCurrentLocale(summaryMessageKey, localizationArguments);
    }
}
