package com.vanmarcke.facades.order.converters.populators;

import com.vanmarcke.core.model.IBMOrderModel;
import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.order.data.IBMOrderData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Populates order data with order split details
 *
 * @param <SOURCE> source order type
 * @param <TARGET> target order data type
 *
 * @author Cristi Stoica
 * @since 06-09-2021
 */
public class VMKAbstractOrderSplittedPopulator<SOURCE extends AbstractOrderModel, TARGET extends AbstractOrderData> implements Populator<SOURCE, TARGET> {

    /**
     * {@inheritDoc}
     */
    @Override
    public void populate(SOURCE source, TARGET target) throws ConversionException {
        target.setIsSplitted(source.getIsSplitted());
        if (source instanceof OrderModel && source.getIsSplitted()) {

            target.setIbmSubOrders(((OrderModel) source).getIbmOrders().stream()
                    .map(subOrder -> {
                        IBMOrderData ibmOrderData = new IBMOrderData();
                        ibmOrderData.setCode(subOrder.getCode());
                        ibmOrderData.setOrderEntries(getEntryDataForSuborder((OrderModel) source, target, subOrder));
                        return ibmOrderData;
                    }).collect(Collectors.toList()));
        }
    }

    private List<OrderEntryData> getEntryDataForSuborder(OrderModel sourceOrder, TARGET targetOrder, IBMOrderModel ibmOrderModel) {
        List<AbstractOrderEntryModel> orderEntriesForIbmOrder = sourceOrder.getEntries().stream()
                .filter(entry -> entry.getIbmOrder().getCode().equals(ibmOrderModel.getCode()))
                .collect(Collectors.toList());

        return orderEntriesForIbmOrder.stream().map(entry -> targetOrder.getEntries().stream()
                .filter(e -> entry.getProduct().getCode().equals(e.getProduct().getCode()))
                .findAny().orElse(null))
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
        }
}
