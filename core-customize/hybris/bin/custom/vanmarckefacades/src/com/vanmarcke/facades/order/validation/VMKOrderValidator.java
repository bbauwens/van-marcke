package com.vanmarcke.facades.order.validation;

/**
 * Defines methods to validate the current cart for invalid entries.
 *
 * @author Tom van den Berg
 * @since 31-12-2020
 */
public interface VMKOrderValidator {

    /**
     * Checks if the cart is valid to be processed, returns a formatted error message if there is an error,
     * which should block the customer from going to the checkout.
     *
     * @return Empty string if cart is valid, or error message if it is invalid
     */
    String checkCartForErrors();

    String removeInvalidEntriesFromCart();

    /**
     * Adjusts quantity in the cart for discontinued products, given the remaining EDC stock
     *
     * @return Empty string if no adjustment is needed, info message if adjustment was necessary
     */
    String adjustQuantityForDiscontinuedProducts();
}
