package com.vanmarcke.facades.search.converters;

import com.vanmarcke.services.storelocator.pos.VMKDistanceFormatterService;
import de.hybris.platform.basecommerce.enums.WeekDay;
import de.hybris.platform.commercefacades.storelocator.data.OpeningScheduleData;
import de.hybris.platform.commercefacades.storelocator.data.PointOfServiceData;
import de.hybris.platform.commercefacades.storelocator.data.TimeData;
import de.hybris.platform.commercefacades.storelocator.data.WeekdayOpeningDayData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commerceservices.store.data.GeoPoint;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.solrfacetsearch.search.AbstractSolrConverter;
import de.hybris.platform.solrfacetsearch.search.impl.SolrResult;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.core.convert.converter.Converter;

import java.util.Date;
import java.util.stream.Collectors;

import static com.vanmarcke.facades.constants.VanmarckeFacadesConstants.SOLR.POINT_OF_SERVICE.*;
import static java.lang.String.format;
import static java.util.Arrays.stream;

/**
 * Converts {@link SolrResult} instances to {@link PointOfServiceData} instances.
 *
 * @author Taki Korovessis
 * @since 19-8-2020
 */
public class VMKPointOfServiceLocatorSearchConverter extends AbstractSolrConverter<PointOfServiceData> {

    private Converter<Date, TimeData> timeDataConverter;
    private VMKDistanceFormatterService distanceFormatterService;

    /**
     * {@inheritDoc}
     */
    @Override
    protected PointOfServiceData createDataObject() {
        return new PointOfServiceData();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PointOfServiceData convert(final SolrResult source, final PointOfServiceData target) throws ConversionException {
        target.setDisplayName(getValue(source, INDEX_DISPLAY_NAME));
        target.setName(getValue(source, INDEX_NAME));

        populateAddress(source, target);

        populateOpeningHours(source, target);

        populateCoordinates(source, target);

        populateDistance(source, target);

        return target;
    }

    /**
     * Populates the address from the given {@link SolrResult} to the given {@link PointOfServiceData}.
     *
     * @param source the solr result
     * @param target the point of service data
     */
    protected void populateAddress(final SolrResult source, final PointOfServiceData target) {
        final AddressData addressData = new AddressData();
        addressData.setLine1(getValue(source, INDEX_STREET_NAME));
        addressData.setLine2(getValue(source, INDEX_STREET_NUMBER));
        addressData.setPhone(getValue(source, INDEX_PHONE_1));
        addressData.setPostalCode(getValue(source, INDEX_POSTAL_CODE));
        addressData.setTown(getValue(source, INDEX_TOWN));

        final CountryData countryData = new CountryData();
        countryData.setIsocode(getValue(source, INDEX_COUNTRY));
        addressData.setCountry(countryData);

        target.setAddress(addressData);
    }

    /**
     * Populates the opening hours from the given {@link SolrResult} to the given {@link PointOfServiceData}.
     *
     * @param source the solr result
     * @param target the point of service data
     */
    protected void populateOpeningHours(final SolrResult source, final PointOfServiceData target) {
        final OpeningScheduleData openingScheduleData = new OpeningScheduleData();
        openingScheduleData.setWeekDayOpeningList(stream(WeekDay.values())
                .map(e -> {
                    final WeekdayOpeningDayData weekdayOpeningDayData = new WeekdayOpeningDayData();
                    weekdayOpeningDayData.setWeekDay(e.getCode());

                    final Date openingDate = getValue(source, format(INDEX_OPENING_TIME_TEMPLATE, e.getCode().toLowerCase()));
                    final Date closingDate = getValue(source, format(INDEX_CLOSING_TIME_TEMPLATE, e.getCode().toLowerCase()));

                    if (openingDate != null && closingDate != null && !openingDate.equals(closingDate)) {
                        weekdayOpeningDayData.setOpeningTime(timeDataConverter.convert(openingDate));
                        weekdayOpeningDayData.setClosingTime(timeDataConverter.convert(closingDate));
                    } else {
                        weekdayOpeningDayData.setClosed(true);
                    }

                    return weekdayOpeningDayData;
                })
                .collect(Collectors.toList()));

        target.setOpeningHours(openingScheduleData);
    }

    /**
     * Populates the address from the given {@link SolrResult} to the given {@link PointOfServiceData}.
     *
     * @param source the solr result
     * @param target the point of service data
     */
    protected void populateCoordinates(final SolrResult source, final PointOfServiceData target) {
        final GeoPoint geoPoint = new GeoPoint();
        geoPoint.setLatitude(getValue(source, INDEX_LATITUDE));
        geoPoint.setLongitude(getValue(source, INDEX_LONGITUDE));

        target.setGeoPoint(geoPoint);
    }

    /**
     * Populates the distance from the given {@link SolrResult} to the given {@link PointOfServiceData}.
     *
     * @param source the solr result
     * @param target the point of service data
     */
    protected void populateDistance(final SolrResult source, final PointOfServiceData target) {
        final Float distance = getValue(source, INDEX_DISTANCE);
        if (distance != null) {
            target.setFormattedDistance(distanceFormatterService.formatDistance(distance));
        }
    }

    @Required
    public void setTimeDataConverter(final Converter<Date, TimeData> timeDataConverter) {
        this.timeDataConverter = timeDataConverter;
    }

    @Required
    public void setDistanceFormatterService(final VMKDistanceFormatterService distanceFormatterService) {
        this.distanceFormatterService = distanceFormatterService;
    }
}