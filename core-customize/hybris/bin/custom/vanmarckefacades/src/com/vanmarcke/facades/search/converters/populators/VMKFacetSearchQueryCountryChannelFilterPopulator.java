package com.vanmarcke.facades.search.converters.populators;

import com.vanmarcke.core.model.CommerceGroupModel;
import com.vanmarcke.services.commercegroup.VMKCommerceGroupService;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SearchQueryPageableData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchRequest;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;

import static com.vanmarcke.facades.constants.VanmarckeFacadesConstants.SOLR;

/**
 * Populator to add the CountryChannelFilter to the product searches.
 * We don't want to see products which are not available in the selected country.
 */
public class VMKFacetSearchQueryCountryChannelFilterPopulator<INDEXED_PROPERTY_TYPE, INDEXED_TYPE_SORT_TYPE> implements Populator<SearchQueryPageableData<SolrSearchQueryData>, SolrSearchRequest<FacetSearchConfig, IndexedType, INDEXED_PROPERTY_TYPE, SearchQuery, INDEXED_TYPE_SORT_TYPE>> {

    private final VMKCommerceGroupService commerceGroupService;
    private final CMSSiteService cmsSiteService;

    public VMKFacetSearchQueryCountryChannelFilterPopulator(final VMKCommerceGroupService commerceGroupService, final CMSSiteService cmsSiteService) {
        this.commerceGroupService = commerceGroupService;
        this.cmsSiteService = cmsSiteService;
    }

    @Override
    public void populate(SearchQueryPageableData<SolrSearchQueryData> source,
                         SolrSearchRequest<FacetSearchConfig, IndexedType, INDEXED_PROPERTY_TYPE, SearchQuery, INDEXED_TYPE_SORT_TYPE> target) throws ConversionException {
        final CommerceGroupModel currentCommerceGroup = commerceGroupService.getCurrentCommerceGroup();
        if (currentCommerceGroup != null) {
            final CMSSiteModel currentSite = cmsSiteService.getCurrentSite();
            target.getSearchQuery().addFilterQuery(String.format(SOLR.PRODUCTS.INDEX_AVAILABILITY, currentCommerceGroup.getCountry().getIsocode().toLowerCase(), currentSite.getChannel().getCode().toLowerCase()), "true");
        }
    }
}