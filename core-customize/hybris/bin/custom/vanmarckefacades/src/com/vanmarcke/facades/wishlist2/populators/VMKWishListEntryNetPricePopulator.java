package com.vanmarcke.facades.wishlist2.populators;

import com.vanmarcke.services.product.price.VMKNetPriceLookupStrategy;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.wishlist2.model.Wishlist2EntryModel;

import java.math.BigDecimal;

public class VMKWishListEntryNetPricePopulator implements Populator<Wishlist2EntryModel, OrderEntryData> {

    private final CommerceCommonI18NService commerceCommonI18NService;
    private final VMKNetPriceLookupStrategy blueNetPriceLookupStrategy;
    private final PriceDataFactory priceDataFactory;

    public VMKWishListEntryNetPricePopulator(CommerceCommonI18NService commerceCommonI18NService,
                                             VMKNetPriceLookupStrategy blueNetPriceLookupStrategy,
                                             PriceDataFactory priceDataFactory) {
        this.commerceCommonI18NService = commerceCommonI18NService;
        this.blueNetPriceLookupStrategy = blueNetPriceLookupStrategy;
        this.priceDataFactory = priceDataFactory;
    }

    @Override
    public void populate(Wishlist2EntryModel source, OrderEntryData target) throws ConversionException {
        final ProductModel product = source.getProduct();
        final int quantity = (source.getDesired() != null && source.getDesired() > 0) ? source.getDesired() : 1;
        final CurrencyModel currency = commerceCommonI18NService.getCurrentCurrency();

        final BigDecimal netPrice = blueNetPriceLookupStrategy.getNetPriceForProduct(product);
        final PriceData priceData = priceDataFactory.create(PriceDataType.NET, netPrice, currency);
        target.setNetPriceData(priceData);

        final BigDecimal totalFromNet = netPrice.multiply(new BigDecimal(quantity));
        final PriceData totalPriceData = priceDataFactory.create(PriceDataType.NET, totalFromNet, currency);
        target.setTotalPrice(totalPriceData);
    }
}
