package com.vanmarcke.facades.paymentmodes.impl;

import com.vanmarcke.facades.payment.data.PaymentModeData;
import com.vanmarcke.facades.paymentmodes.VMKPaymentModeFacade;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.payment.PaymentModeModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.Collections;
import java.util.List;

import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang3.BooleanUtils.isTrue;

/**
 * Implementation for {@link VMKPaymentModeFacade}
 */
public class DefaultVMKPaymentModeFacade implements VMKPaymentModeFacade {

    private final Converter<PaymentModeModel, PaymentModeData> vmkPaymentModeConverter;
    private final CartService cartService;

    /**
     * Creates a new instance of {@link DefaultVMKPaymentModeFacade}
     *
     * @param vmkPaymentModeConverter the vmkPaymentModeConverter
     * @param cartService             the cartService
     */
    public DefaultVMKPaymentModeFacade(Converter<PaymentModeModel, PaymentModeData> vmkPaymentModeConverter, CartService cartService) {
        this.vmkPaymentModeConverter = vmkPaymentModeConverter;
        this.cartService = cartService;
    }

    /**
     * Set the payment modes which are available for the given baseStore on the model
     */
    @Override
    public List<PaymentModeData> getSupportedPaymentModes() {
        CartModel cart = cartService.hasSessionCart() ? cartService.getSessionCart() : null;
        if (cart != null) {
            return cart.getStore().getPaymentModes()
                    .stream()
                    .filter(e -> isTrue(e.getActive()))
                    .map(vmkPaymentModeConverter::convert)
                    .collect(toList());
        }
        return Collections.emptyList();
    }
}
