package com.vanmarcke.facades.exportcart;

import com.vanmarcke.facades.data.order.SavedCartData;

import java.io.IOException;
import java.util.List;

/**
 * interface for exporting a saved cart - wishlist or cart - to pdf or excel
 *
 */
public interface VMKExportSavedCartFacade {
    /**
     * Generate a PDF file for the specified wishlist
     *
     * @param savedCartData the saved cart information
     * @return PDF representation for the saved cart
     */
    byte[] generatePdfFromSavedCart(List<String> headers, SavedCartData savedCartData, boolean includeNet, boolean showImages, boolean includeTechnical, int emptyLines);

    /**
     * Generate an Excel file from the specified saved cart data
     *
     * @param headers      Translated headers for the excel file
     * @param savedCartData the saved cart information
     * @param netPrice     Whether or not to show net-prices
     * @param emptyLines   the number of empty lines
     * @return Excel file export for the specified saved cart
     */
    byte[] generateExcelFromSavedCart(List<String> headers, SavedCartData savedCartData, boolean netPrice, int emptyLines) throws IOException;
}
