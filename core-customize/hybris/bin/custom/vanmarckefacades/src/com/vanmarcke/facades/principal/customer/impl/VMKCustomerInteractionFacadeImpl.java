package com.vanmarcke.facades.principal.customer.impl;

import com.vanmarcke.core.model.CustomerInteractionModel;
import com.vanmarcke.core.model.OrderCustomerInteractionModel;
import com.vanmarcke.facades.customerinteraction.data.CustomerInteractionData;
import com.vanmarcke.facades.principal.customer.VMKCustomerInteractionFacade;
import com.vanmarcke.services.event.events.SubmitCustomerInteractionEvent;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.model.ModelService;

import static com.vanmarcke.core.enums.CustomerInteractionStatus.CREATED;

/**
 * Implementation for BlueCustomerInteractionFacade
 */
public class VMKCustomerInteractionFacadeImpl implements VMKCustomerInteractionFacade {

    private final ModelService modelService;
    private final Converter<CustomerInteractionData, CustomerInteractionModel> blueCustomerInteractionReverseConverter;
    private final EventService eventService;

    /**
     * Constructor for VMKCustomerInteractionFacadeImpl
     *
     * @param modelService                            the modelService
     * @param blueCustomerInteractionReverseConverter the blueCustomerInteractionReverseConverter
     * @param eventService                            the eventService
     */
    public VMKCustomerInteractionFacadeImpl(final ModelService modelService, final Converter<CustomerInteractionData, CustomerInteractionModel> blueCustomerInteractionReverseConverter, final EventService eventService) {
        this.modelService = modelService;
        this.blueCustomerInteractionReverseConverter = blueCustomerInteractionReverseConverter;
        this.eventService = eventService;
    }

    @Override
    public void submitOrderCustomerInteraction(final CustomerInteractionData customerInteractionData) {
        final CustomerInteractionModel customerInteraction = createOrderCustomerInteraction(customerInteractionData);
        eventService.publishEvent(new SubmitCustomerInteractionEvent(customerInteraction));
    }

    /**
     * Create the actual Customer Interaction
     *
     * @param customerInteractionData the customerInteractionData
     * @return the newly created CustomerInteractionModel
     */
    protected CustomerInteractionModel createOrderCustomerInteraction(final CustomerInteractionData customerInteractionData) {
        final CustomerInteractionModel customerInteraction = modelService.create(OrderCustomerInteractionModel.class);
        blueCustomerInteractionReverseConverter.convert(customerInteractionData, customerInteraction);
        customerInteraction.setStatus(CREATED);
        modelService.save(customerInteraction);
        return customerInteraction;
    }
}
