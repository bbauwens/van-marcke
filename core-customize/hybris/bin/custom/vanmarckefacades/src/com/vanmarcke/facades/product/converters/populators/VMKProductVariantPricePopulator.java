package com.vanmarcke.facades.product.converters.populators;

import com.vanmarcke.facades.sitechannel.AbstractSiteChannelAwarePopulator;
import com.vanmarcke.services.product.price.VMKNetPriceLookupStrategy;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.product.data.VariantOptionData;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.variants.model.VariantProductModel;
import org.springframework.beans.factory.annotation.Required;

import java.math.BigDecimal;

/**
 * Populator to populate Price for VariantOptionData
 */
public class VMKProductVariantPricePopulator extends AbstractSiteChannelAwarePopulator<VariantProductModel, VariantOptionData> {

    private CommonI18NService commonI18NService;
    private PriceDataFactory priceDataFactory;
    private VMKNetPriceLookupStrategy blueNetPriceLookupStrategy;

    @Override
    protected void doPopulate(final VariantProductModel source, final VariantOptionData target) {
        final BigDecimal price = this.blueNetPriceLookupStrategy.getNetPriceForProduct(source);
        if (price != null) {
            final String isoCode = this.commonI18NService.getCurrentCurrency().getIsocode();
            target.setNetPriceData(this.priceDataFactory.create(PriceDataType.BUY, price, isoCode));
        }
    }

    @Required
    public void setCommonI18NService(final CommonI18NService commonI18NService) {
        this.commonI18NService = commonI18NService;
    }

    @Required
    public void setPriceDataFactory(final PriceDataFactory priceDataFactory) {
        this.priceDataFactory = priceDataFactory;
    }

    @Required
    public void setBlueNetPriceLookupStrategy(final VMKNetPriceLookupStrategy blueNetPriceLookupStrategy) {
        this.blueNetPriceLookupStrategy = blueNetPriceLookupStrategy;
    }
}
