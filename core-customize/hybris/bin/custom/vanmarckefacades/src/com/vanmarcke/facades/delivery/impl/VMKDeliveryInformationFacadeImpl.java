package com.vanmarcke.facades.delivery.impl;

import com.vanmarcke.cpi.data.order.EntryInfoResponseData;
import com.vanmarcke.cpi.data.order.PickupDateInfoResponseData;
import com.vanmarcke.cpi.data.order.ShippingDateInfoResponseData;
import com.vanmarcke.cpi.data.order.ShippingEntryInfoResponseData;
import com.vanmarcke.facades.data.PickupInfoData;
import com.vanmarcke.facades.data.ShippingInfoData;
import com.vanmarcke.facades.data.deliverypickup.product.DeliveryPickupProductData;
import com.vanmarcke.facades.delivery.VMKDeliveryInformationFacade;
import com.vanmarcke.services.basestore.VMKBaseStoreService;
import com.vanmarcke.services.delivery.VMKDeliveryInfoService;
import com.vanmarcke.services.delivery.VMKFirstDateAvailabilityService;
import com.vanmarcke.services.delivery.VMKPickupInfoService;
import com.vanmarcke.services.delivery.VMKShippingInfoService;
import com.vanmarcke.services.forms.delivery.DeliveryMethodForm;
import com.vanmarcke.services.order.cart.VMKTecCollectReservationMessageService;
import com.vanmarcke.services.storelocator.pos.VMKPointOfServiceService;
import com.vanmarcke.services.storelocator.schedule.VMKOpeningScheduleService;
import com.vanmarcke.services.util.VMKDateUtils;
import de.hybris.platform.commercefacades.storelocator.data.PointOfServiceData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static com.vanmarcke.services.constants.VanmarckeservicesConstants.DATE_PATTERN;

/**
 * This facade provides delivery info.
 *
 * @author Tom van den Berg, Niels Raemaekers
 * @since 30-12-2020
 */
public class VMKDeliveryInformationFacadeImpl implements VMKDeliveryInformationFacade {

    private static final Logger LOG = LogManager.getLogger();

    private final Converter<CartModel, DeliveryMethodForm> deliveryMethodFormConverter;
    private final Populator<PointOfServiceModel, PointOfServiceData> pointOfServicePopulator;
    private final VMKDeliveryInfoService vmkDeliveryInfoService;
    private final VMKFirstDateAvailabilityService vmkFirstDateAvailabilityService;
    private final VMKBaseStoreService baseStoreService;
    private final VMKPointOfServiceService pointOfServiceService;
    private final VMKPickupInfoService pickupInfoService;
    private final VMKShippingInfoService shippingInfoService;
    private final VMKTecCollectReservationMessageService tecCollectReservationMessageService;
    private final VMKOpeningScheduleService openingScheduleService;

    /**
     * Provides an instance of the VMKDeliveryInformationFacadeImpl.
     *
     * @param deliveryMethodFormConverter         the delivery method form converter
     * @param pointOfServicePopulator             the point of service populator
     * @param vmkDeliveryInfoService              the delivery info service
     * @param vmkFirstDateAvailabilityService     the first available day service
     * @param baseStoreService                    the base store service
     * @param pointOfServiceService               the point of service service
     * @param pickupInfoService                   the pickup info service
     * @param shippingInfoService                 the shipping info service
     * @param tecCollectReservationMessageService the tec collect reservation message service
     * @param openingScheduleService              the opening schedule service
     */

    public VMKDeliveryInformationFacadeImpl(Converter<CartModel, DeliveryMethodForm> deliveryMethodFormConverter, Populator<PointOfServiceModel, PointOfServiceData> pointOfServicePopulator, VMKDeliveryInfoService vmkDeliveryInfoService,
                                            VMKFirstDateAvailabilityService vmkFirstDateAvailabilityService, VMKBaseStoreService baseStoreService, VMKPointOfServiceService pointOfServiceService, VMKPickupInfoService pickupInfoService,
                                            VMKShippingInfoService shippingInfoService, VMKTecCollectReservationMessageService tecCollectReservationMessageService, VMKOpeningScheduleService openingScheduleService) {
        this.deliveryMethodFormConverter = deliveryMethodFormConverter;
        this.pointOfServicePopulator = pointOfServicePopulator;
        this.vmkDeliveryInfoService = vmkDeliveryInfoService;
        this.vmkFirstDateAvailabilityService = vmkFirstDateAvailabilityService;
        this.baseStoreService = baseStoreService;
        this.pointOfServiceService = pointOfServiceService;
        this.pickupInfoService = pickupInfoService;
        this.shippingInfoService = shippingInfoService;
        this.tecCollectReservationMessageService = tecCollectReservationMessageService;
        this.openingScheduleService = openingScheduleService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ShippingInfoData getShippingInfo(final CartModel cart) {
        final ShippingInfoData shippingInfo = new ShippingInfoData();
        vmkDeliveryInfoService.getShippingDateInformation(cart).ifPresent(shippingDateInfoResponse -> {
            shippingInfo.setFirstPossibleShippingDate(shippingInfoService.getFirstPossibleShippingDate(cart, shippingDateInfoResponse));
            shippingInfo.setProducts(getDeliveryProductData(cart, shippingDateInfoResponse));
        });
        return shippingInfo;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PickupInfoData getPickupInfo(final CartModel cart, final String selectedDate) {
        final PickupInfoData pickupInfoData = new PickupInfoData();
        vmkDeliveryInfoService.getPickupDateInformation(cart).ifPresent(pickupDateResponse -> {
            final Optional<Date> firstPossiblePickupDate = pickupInfoService.getFirstPossiblePickupDate(cart, pickupDateResponse);
            final Optional<Date> lastPossiblePickupDate = pickupInfoService.getLastPossiblePickupDate(pickupDateResponse);
            final boolean tecCollectPossible = pickupInfoService.isTecCollectPossible(cart, pickupDateResponse);

            final List<PointOfServiceModel> allPointOfService = baseStoreService.getCurrentBaseStore().getPointsOfService();
            final Set<PointOfServiceModel> alternativeTec = new HashSet<>(pointOfServiceService.getAlternativeTECStoresWhichAreCurrentlyOpenForCart(cart));
            final List<PointOfServiceData> allPointOfServiceWithPickupDates = getPointOfServiceData(cart, selectedDate, lastPossiblePickupDate.orElse(null), allPointOfService, alternativeTec, tecCollectPossible);

            pickupInfoData.setTecCollectPossible(tecCollectPossible);
            pickupInfoData.setAlternativePointOfService(allPointOfServiceWithPickupDates);

            firstPossiblePickupDate.ifPresent(pickupDate -> {
                pickupInfoData.setFirstPossiblePickupDate(pickupDate);
                pickupInfoData.setReservationMessage(tecCollectReservationMessageService.generateTecCollectReservationMessage(cart, pickupDate, tecCollectPossible));

            });
            lastPossiblePickupDate.ifPresent(pickupDate -> pickupInfoData.setProducts(getPickupProductData(cart, pickupDateResponse, pickupDate)));
        });
        if (cart.getDeliveryPointOfService() != null) {
            pickupInfoData.setOpenOnSaturday(openingScheduleService.isStoreOpenOnSaturday(cart.getDeliveryPointOfService()));
        }
        return pickupInfoData;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DeliveryMethodForm getDeliveryMethodForm(final CartModel cart) {
        return deliveryMethodFormConverter.convert(cart);
    }

    /**
     * Convert Pickup response to Data
     *
     * @param cart the current cart
     * @return List with pickup product data
     */
    private List<DeliveryPickupProductData> getPickupProductData(final CartModel cart, final PickupDateInfoResponseData response, final Date lastPossiblePickupDate) {
        final Map<String, Date> productsByPickupDate = CollectionUtils.emptyIfNull(response.getProducts()).stream()
                .filter(pickupEntry -> ObjectUtils.allNotNull(pickupEntry.getProductNumber(), pickupEntry.getPickupDate()))
                .collect(Collectors.toMap(EntryInfoResponseData::getProductNumber, EntryInfoResponseData::getPickupDate));

        return getDeliveryPickupProductData(productsByPickupDate, lastPossiblePickupDate, cart);
    }

    private List<PointOfServiceData> getPointOfServiceData(final CartModel cart, final String selectedDate, final Date lastPossiblePickupDate, final List<PointOfServiceModel> allPointOfService, final Set<PointOfServiceModel> alternativeTec, final boolean tecCollectPossible) {
        final Date todayDate = VMKDateUtils.getTodaysDate();
        final PointOfServiceModel currentPointOfService = pointOfServiceService.getCurrentPointOfService(cart);

        final List<PointOfServiceData> pointsOfService = allPointOfService.stream()
                .map(pos -> {
                    final PointOfServiceData pointOfService = new PointOfServiceData();
                    final boolean isTecCollect = alternativeTec.contains(pos) || pos.equals(currentPointOfService) && tecCollectPossible;

                    pointOfServicePopulator.populate(pos, pointOfService);
                    openingScheduleService.findOpenPickupDateForPointOfService(pos, isTecCollect ? todayDate : lastPossiblePickupDate).ifPresent(pointOfService::setPickupDate);

                    return pointOfService;
                })
                .sorted(Comparator.comparing(PointOfServiceData::getDisplayName))
                .collect(Collectors.toList());

        final Date currentPickupDate = getCurrentPickupDate(cart, selectedDate, todayDate, pointsOfService);

        pointsOfService.forEach(pointOfService -> {
            final boolean isPickupPossibleOnCurrentPickupDate = VMKDateUtils.daysBetween(pointOfService.getPickupDate(), currentPickupDate) >= 0;

            pointOfService.setDaysUntilPickup(VMKDateUtils.daysBetween(todayDate, pointOfService.getPickupDate()));
            pointOfService.setEnabledOnSelectedDate(isPickupPossibleOnCurrentPickupDate && openingScheduleService.isStoreOpenOnGivenDate(pointOfService.getName(), currentPickupDate));
        });
        return pointsOfService;
    }

    /**
     * Use pickup date selected by user if possible, otherwise use first possible pickup date for current point of service.
     *
     * @param cart            the cart
     * @param selectedDate    the selected date
     * @param todayDate       today's date
     * @param pointsOfService available points of service
     * @return the current pickup date
     */
    private Date getCurrentPickupDate(final CartModel cart, final String selectedDate, final Date todayDate, final List<PointOfServiceData> pointsOfService) {
        return selectedDate != null ? parseSelectedDate(selectedDate) : findPickupDateForCurrentPointOfService(cart, pointsOfService).orElse(todayDate);
    }

    /**
     * Returns pickup date assigned to currently selected point of service.
     *
     * @param cart            the cart
     * @param pointsOfService available points of service
     * @return pickup date if found
     */
    private Optional<Date> findPickupDateForCurrentPointOfService(final CartModel cart, final List<PointOfServiceData> pointsOfService) {
        final String currentPOSName = cart.getDeliveryPointOfService().getName();
        return pointsOfService.stream()
                .filter(pos -> pos.getName().equals(currentPOSName))
                .map(PointOfServiceData::getPickupDate)
                .findFirst();
    }

    /**
     * Convert Shipping response to Data
     *
     * @param cart                     the current cart
     * @param shippingDateInfoResponse the shipping date info response
     * @return List with shipping product data
     */
    private List<DeliveryPickupProductData> getDeliveryProductData(final CartModel cart, final ShippingDateInfoResponseData shippingDateInfoResponse) {
        final Date lastPossibleShippingDate = shippingInfoService.getLastPossibleShippingDate(shippingDateInfoResponse);
        final Map<String, Date> productMap = CollectionUtils.emptyIfNull(shippingDateInfoResponse.getResults()).stream()
                .filter(shippingEntry -> ObjectUtils.allNotNull(shippingEntry.getProductNumber(), shippingEntry.getDeliveryDate()))
                .collect(Collectors.toMap(ShippingEntryInfoResponseData::getProductNumber, ShippingEntryInfoResponseData::getDeliveryDate));

        return getDeliveryPickupProductData(productMap, lastPossibleShippingDate, cart);
    }

    /**
     * Get the delivery/pickup product data
     *
     * @param source           the data in raw format
     * @param lastPossibleDate the last possible date
     * @return list with the data
     */
    private List<DeliveryPickupProductData> getDeliveryPickupProductData(final Map<String, Date> source,
                                                                         final Date lastPossibleDate,
                                                                         final CartModel cart) {
        List<DeliveryPickupProductData> target = new ArrayList<>();

        source.forEach((productCode, deliveryPickupDate) -> {
            DeliveryPickupProductData deliveryPickupProductData = new DeliveryPickupProductData();
            deliveryPickupProductData.setProductCode(productCode);

            if (deliveryPickupDate != null) {
                final Date firstPossibleDate = vmkFirstDateAvailabilityService.getFirstValidNoClosingDate(deliveryPickupDate);
                deliveryPickupProductData.setFirstPossibleDate(firstPossibleDate);

                if (lastPossibleDate != null) {
                    final Date lastValidPossibleDate = firstPossibleDate.compareTo(lastPossibleDate) > 0 ? firstPossibleDate : lastPossibleDate;
                    deliveryPickupProductData.setLastPossibleDate(lastValidPossibleDate);
                }
            }
            deliveryPickupProductData.setSelectDate(cart.getDeliveryDate());
            target.add(deliveryPickupProductData);
        });

        return target;
    }

    private Date parseSelectedDate(final String date) {
        try {
            return new SimpleDateFormat(DATE_PATTERN).parse(date);
        } catch (final ParseException e) {
            LOG.warn("Failed to parse selected date, falling back to today's date.");
            return VMKDateUtils.getTodaysDate();
        }
    }
}
