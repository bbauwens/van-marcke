package com.vanmarcke.facades.order.impl;

import com.vanmarcke.facades.data.order.SavedCartData;
import com.vanmarcke.facades.order.VMKB2BOrderFacade;
import de.hybris.platform.b2bacceleratorfacades.order.impl.DefaultB2BOrderFacade;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

/**
 * Custom extension for DefaultB2BOrderFacade and implementation for VMKB2BOrderFacade
 *
 * @author Cristi Stoica
 * @since 16-05-2021
 */
public class VMKB2BOrderFacadeImpl extends DefaultB2BOrderFacade implements VMKB2BOrderFacade {
    private final Converter<AbstractOrderModel, SavedCartData> savedCartConverter;

    public VMKB2BOrderFacadeImpl(Converter<AbstractOrderModel, SavedCartData> savedCartConverter) {
        this.savedCartConverter = savedCartConverter;
    }

    @Override
    public SavedCartData getOrderDetailsForExport(String orderGuid) {
        OrderModel actualOrder = getCustomerAccountService().getOrderDetailsForGUID(orderGuid,
                getBaseStoreService().getCurrentBaseStore());
        return savedCartConverter.convert(actualOrder);
    }


}
