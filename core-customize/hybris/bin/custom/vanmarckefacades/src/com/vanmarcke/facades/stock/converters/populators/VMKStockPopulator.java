package com.vanmarcke.facades.stock.converters.populators;

import de.hybris.platform.commercefacades.product.converters.populator.StockPopulator;
import de.hybris.platform.commercefacades.product.data.StockData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;


/**
 * Populator to populate a {@link StockData} instance from a {@link ProductModel} instance.
 *
 * @param <SOURCE> the product model
 * @param <TARGET> the stock data
 */
public class VMKStockPopulator<SOURCE extends ProductModel, TARGET extends StockData> extends StockPopulator<SOURCE, TARGET> implements Populator<SOURCE, TARGET> {

    /**
     * {@inheritDoc}
     */
    @Override
    public void populate(SOURCE source, TARGET target) {
        // this method is empty to disable the default stock populator.
        // FYI: all stock information will be retrieved through AJAX calls.
    }
}
