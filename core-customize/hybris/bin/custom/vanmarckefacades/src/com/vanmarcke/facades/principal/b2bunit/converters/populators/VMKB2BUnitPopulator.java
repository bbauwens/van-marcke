package com.vanmarcke.facades.principal.b2bunit.converters.populators;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2bcommercefacades.company.data.B2BUnitData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

/**
 * Populator that adds email addresses on a B2BUnit
 */
public class VMKB2BUnitPopulator implements Populator<B2BUnitModel, B2BUnitData> {

    @Override
    public void populate(B2BUnitModel source, B2BUnitData target) throws ConversionException {
        target.setCreditControllerEmail(source.getCreditControllerEmail());
        target.setOrderManagerEmail(source.getOrderManagerEmail());
    }
}
