package com.vanmarcke.facades.order.impl;

import com.vanmarcke.core.exception.NoSessionCartException;
import com.vanmarcke.cpi.data.order.PickupDateInfoResponseData;
import com.vanmarcke.facades.data.DeliveryInfoData;
import com.vanmarcke.facades.delivery.VMKDeliveryInformationFacade;
import com.vanmarcke.facades.order.VMKCheckoutFacade;
import com.vanmarcke.facades.stock.VMKStockFacade;
import com.vanmarcke.services.delivery.VMKDeliveryInfoService;
import com.vanmarcke.services.delivery.VMKDeliveryMethodService;
import com.vanmarcke.services.forms.delivery.DeliveryMethodForm;
import com.vanmarcke.services.order.VMKCommerceCheckoutService;
import com.vanmarcke.services.order.payment.VMKPaymentInfoFactory;
import com.vanmarcke.services.order.payment.VMKPaymentTypeService;
import com.vanmarcke.services.product.VMKVariantProductService;
import de.hybris.platform.b2bacceleratorfacades.exception.EntityValidationException;
import de.hybris.platform.b2bacceleratorfacades.order.data.B2BPaymentTypeData;
import de.hybris.platform.b2bacceleratorfacades.order.impl.DefaultB2BAcceleratorCheckoutFacade;
import de.hybris.platform.b2bacceleratorservices.enums.CheckoutPaymentType;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.DeliveryModeData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.variants.model.VariantProductModel;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.vanmarcke.core.constants.VanmarckeCoreConstants.DeliveryMethods.PICKUP;
import static com.vanmarcke.core.constants.VanmarckeCoreConstants.DeliveryMethods.SHIPPING;
import static java.util.Collections.emptyList;
import static org.apache.commons.collections4.CollectionUtils.emptyIfNull;
import static org.apache.commons.lang3.StringUtils.trimToNull;

/**
 * The {@link VMKCheckoutFacadeImpl} class implements the business logic that enables the checkout process.
 *
 * @author Niels Raemaekers, Taki Korovessis, Tom van den Berg
 * @since 04-07-2019
 */
public class VMKCheckoutFacadeImpl extends DefaultB2BAcceleratorCheckoutFacade implements VMKCheckoutFacade {

    private static final Logger LOG = Logger.getLogger(VMKCheckoutFacadeImpl.class);

    private final VMKPaymentTypeService paymentTypeService;
    private final VMKCommerceCheckoutService commerceCheckoutService;
    private final VMKDeliveryMethodService deliveryMethodService;
    private final VMKPaymentInfoFactory paymentInfoFactory;
    private final VMKDeliveryInformationFacade deliveryInfoFacade;
    private final VMKStockFacade vmkStockFacade;
    private final VMKVariantProductService variantProductService;

    /**
     * Constructor for {@link VMKCheckoutFacadeImpl}
     *
     * @param paymentInfoFactory      the payment info factory
     * @param paymentTypeService      the payment type service
     * @param commerceCheckoutService the commerce checkout service
     * @param deliveryMethodService   the delivery mode service
     * @param deliveryInfoFacade      the delivery info facade
     * @param vmkStockFacade          the vmk stock facade
     * @param variantProductService   the product variant service
     */
    public VMKCheckoutFacadeImpl(VMKPaymentInfoFactory paymentInfoFactory,
                                 VMKPaymentTypeService paymentTypeService,
                                 VMKCommerceCheckoutService commerceCheckoutService,
                                 VMKDeliveryMethodService deliveryMethodService,
                                 VMKDeliveryInformationFacade deliveryInfoFacade,
                                 VMKStockFacade vmkStockFacade,
                                 VMKVariantProductService variantProductService) {
        this.paymentInfoFactory = paymentInfoFactory;
        this.paymentTypeService = paymentTypeService;
        this.commerceCheckoutService = commerceCheckoutService;
        this.deliveryMethodService = deliveryMethodService;
        this.deliveryInfoFacade = deliveryInfoFacade;
        this.vmkStockFacade = vmkStockFacade;
        this.variantProductService = variantProductService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DeliveryInfoData getDeliveryInformation(final String deliveryMethod, final String selectedDate) {
        DeliveryInfoData deliveryInfo = null;
        final CartModel currentCart = getCart();
        if (PICKUP.equals(deliveryMethod)) {
            deliveryInfo = deliveryInfoFacade.getPickupInfo(currentCart, selectedDate);
        } else if (SHIPPING.equals(deliveryMethod)) {
            deliveryInfo = deliveryInfoFacade.getShippingInfo(currentCart);
        }
        return deliveryInfo;
    }

    /**
     * Generates the {@link DeliveryMethodForm} for the current session cart.
     *
     * @return the delivery method form
     */
    @Override
    public DeliveryMethodForm getDeliveryMethodForm() {
        return deliveryInfoFacade.getDeliveryMethodForm(getCart());
    }

    /**
     * Checks whether there's a pick-up date available for the given {@code response}.
     *
     * @param response the response
     * @return {@code true} in case there's no pick-up date available for the given {@code response}, {@code false}
     * otherwise
     */
    private boolean missingPickupDate(final PickupDateInfoResponseData response) {
        return emptyIfNull(response.getProducts())
                .stream()
                .anyMatch(e -> e.getPickupDate() == null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<String, Boolean> getAvailableDeliveryMethods() {
        return deliveryMethodService.getSupportedDeliveryMethods(getCart());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<String, String> getDeliveryMethodCodes() {
        return deliveryMethodService.getDeliveryMethodCodes(getCart());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setFavoriteAlternativeStoreOnSessionCart(String tecUid) {
        commerceCheckoutService.setFavoriteAlternativeStoreOnSessionCart(tecUid);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void saveDeliveryInformation(DeliveryMethodForm form) {
        ((VMKCommerceCheckoutService) getCommerceCheckoutService()).setDeliveryDetailsOnCart(form);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void beforePlaceOrder(CartModel cartModel) {
        cartModel.setStatus(OrderStatus.CREATED);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CartData updateCheckoutCart(CartData cartData) {
        CartModel cartModel = getCart();
        if (cartModel == null) {
            return null;
        }

        if (cartData.getPaymentType() != null) {
            String newPaymentTypeCode = cartData.getPaymentType().getCode();
            setPaymentTypeForCart(newPaymentTypeCode, cartModel);
        }

        if (cartData.getPurchaseOrderNumber() != null) {
            cartModel.setPurchaseOrderNumber(trimToNull(cartData.getPurchaseOrderNumber()));
        }

        getModelService().save(cartModel);
        return getCheckoutCart();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void setPaymentTypeForCart(String paymentType, CartModel cartModel) {
        final List<CheckoutPaymentType> checkoutPaymentTypes = paymentTypeService.getSupportedPaymentTypeListForOrder(cartModel);
        if (!checkoutPaymentTypes.contains(CheckoutPaymentType.valueOf(paymentType))) {
            throw new EntityValidationException("Payment type is not a valid value.");
        }

        cartModel.setPaymentType(CheckoutPaymentType.valueOf(paymentType));
        cartModel.setPaymentInfo(paymentInfoFactory.createPaymentInfoForOrder(cartModel));

        getCommerceCartService().calculateCartForPaymentTypeChange(cartModel);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<B2BPaymentTypeData> getPaymentTypes() {
        CartModel cartModel = getCart();
        if (cartModel == null) {
            return emptyList();
        }
        List<CheckoutPaymentType> checkoutPaymentTypes = paymentTypeService.getSupportedPaymentTypeListForOrder(cartModel);
        return getB2bPaymentTypeDataConverter().convertAll(checkoutPaymentTypes);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getInvalidCartEntries() {
        return String.join(", ", commerceCheckoutService.getInvalidCartEntries());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean removeInvalidCartEntries(String invalidCartEntries) {
        try {
            CartModel cart = getCartService().getSessionCart();
            List<AbstractOrderEntryModel> entriesToRemove = new ArrayList<>();

            List<String> codesToRemove = new ArrayList<>(Arrays.asList(invalidCartEntries.split(", ")));
            codesToRemove.forEach(c -> {
                Optional<AbstractOrderEntryModel> entry = cart.getEntries().stream()
                        .filter(e -> e.getProduct().getCode().equals(c))
                        .findFirst();
                if (entry.isPresent()) {
                    entriesToRemove.add(entry.get());
                }
            });

            getModelService().removeAll(entriesToRemove);
            getModelService().refresh(cart);
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String adjustQuantityForDiscontinuedProducts() {
        CartModel cart = getCart();
        Map<AbstractOrderEntryModel, Long> stockLevels = cart.getEntries().stream()
                .filter(entry -> variantProductService.isDiscontinuedProduct((VariantProductModel) entry.getProduct())
                        || variantProductService.isSellingOffAlmostNOS((VariantProductModel) entry.getProduct()))
                .collect(Collectors.toMap(Function.identity(), entry -> vmkStockFacade.getAvailableEDCStock(entry.getProduct().getCode())));

        List<AbstractOrderEntryModel> adjustedEntries = cart.getEntries().stream()
                .filter(entry -> stockLevels.containsKey(entry) && stockLevels.get(entry) < entry.getQuantity())
                .collect(Collectors.toList());


        List<AbstractOrderEntryModel> finalEntries = new ArrayList<>();
        adjustedEntries.forEach(entry ->
        {
            try {
                getCartFacade().updateCartEntry(entry.getEntryNumber(), stockLevels.get(entry).longValue());
                finalEntries.add(entry);
            } catch (final CommerceCartModificationException ex) {
                LOG.warn("Couldn't update product with the entry number: " + entry.getEntryNumber() + ".", ex);
            }
        });

        return finalEntries.stream()
                .map(entry -> entry.getProduct().getCode())
                .collect(Collectors.joining(","));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CartData getCheckoutCart() {
        CartData cart = getCartFacade().getSessionCart();
        if (cart != null) {
            cart.setDeliveryAddress(getDeliveryAddress());
            cart.setDeliveryMode(getDeliveryMode());
        }
        return cart;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<? extends DeliveryModeData> getSupportedDeliveryModes() {
        // This method has been overridden for testing purposes.
        return super.getSupportedDeliveryModes();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected CartModel getCart() {
        CartModel cart = super.getCart();
        if (cart == null) {
            LOG.error("No session cart was found.");
            throw new NoSessionCartException("No session cart was found.");
        }
        return cart;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected AddressData getDeliveryAddress() {
        // This method has been overridden for testing purposes.
        return super.getDeliveryAddress();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected DeliveryModeData getDeliveryMode() {
        // This method has been overridden for testing purposes.
        return super.getDeliveryMode();
    }
}
