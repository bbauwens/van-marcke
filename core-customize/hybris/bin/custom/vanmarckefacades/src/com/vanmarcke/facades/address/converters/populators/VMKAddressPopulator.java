package com.vanmarcke.facades.address.converters.populators;

import de.hybris.platform.commercefacades.user.converters.populator.AddressPopulator;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.core.model.user.AddressModel;

/**
 * The {@code VMKAddressPopulator} class is used populate {@link AddressData} instances with the data of the
 * {@link AddressModel} instances.
 *
 * @author Christiaan Janssen
 * @since 23/09/2020
 */
public class VMKAddressPopulator extends AddressPopulator {

    /**
     * {@inheritDoc}
     */
    @Override
    public void populate(AddressModel source, AddressData target) {
        super.populate(source, target);

        target.setMobile(source.getPhone2());
        target.setApartment(source.getAppartment());
    }
}
