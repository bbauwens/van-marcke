package com.vanmarcke.facades.productcarousel.impl;

import de.hybris.platform.acceleratorfacades.productcarousel.impl.DefaultProductCarouselFacade;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2lib.model.components.ProductCarouselComponentModel;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Custom implementation for DefaultProductCarouselFacade
 */
public class VMKProductCarouselFacade extends DefaultProductCarouselFacade {

    private static final Logger LOG = LoggerFactory.getLogger(VMKProductCarouselFacade.class);

    /**
     * Overridden so we can catch the UnknownIdentifierException.
     * No functionality was changed in comparison to the default code.
     *
     * @param component the product carousel component
     * @return a list of Product Data
     */
    @Override
    protected List<ProductData> fetchProductsForNonPreviewMode(ProductCarouselComponentModel component) {
        List<ProductData> products = new ArrayList<>();

        for (ProductModel productModel : component.getProducts()) {
            addProductToList(products, productModel);
        }
        for (CategoryModel categoryModel : component.getCategories()) {
            for (ProductModel productModel : categoryModel.getProducts()) {
                addProductToList(products, productModel);
            }
        }
        return products;
    }

    /**
     * Add the product to the list or catch the UnknownIdentifierException and log a warning for it.
     *
     * @param products     the list with products to show in the product carousel
     * @param productModel the current productModel
     */
    private void addProductToList(List<ProductData> products, ProductModel productModel) {
        try {
            products.add(getProductFacade().getProductForCodeAndOptions(productModel.getCode(), PRODUCT_OPTIONS));
        } catch (UnknownIdentifierException ex) {
            LOG.debug("Could not add product with code " + productModel.getCode() + " to the product carousel due to constraints.", ex);
        }
    }
}
