package com.vanmarcke.facades.order.converters.populators;

import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.storelocator.data.PointOfServiceData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

public class VMKAbstractOrderDeliveryPointOfServicePopulator<S extends AbstractOrderModel, T extends AbstractOrderData> implements Populator<S, T> {

    private final Converter<PointOfServiceModel, PointOfServiceData> pointOfServiceConverter;

    public VMKAbstractOrderDeliveryPointOfServicePopulator(final Converter<PointOfServiceModel, PointOfServiceData> pointOfServiceConverter) {
        this.pointOfServiceConverter = pointOfServiceConverter;
    }

    @Override
    public void populate(final S source, final T target) {
        if (source.getDeliveryPointOfService() != null) {
            target.setDeliveryPointOfService(pointOfServiceConverter.convert(source.getDeliveryPointOfService()));
        }
    }
}