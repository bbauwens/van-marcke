package com.vanmarcke.facades.product.converters.populators;

import com.vanmarcke.facades.sitechannel.AbstractSiteChannelAwarePopulator;
import com.vanmarcke.services.product.price.VMKNetPriceLookupStrategy;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import java.math.BigDecimal;

/**
 * Populator to populate Price for VariantOptionData
 */
public class VMKProductNetPricePopulator extends AbstractSiteChannelAwarePopulator<ProductModel, ProductData> {

    private CommonI18NService commonI18NService;
    private PriceDataFactory priceDataFactory;
    private VMKNetPriceLookupStrategy blueNetPriceLookupStrategy;

    public VMKProductNetPricePopulator(final CommonI18NService commonI18NService, final PriceDataFactory priceDataFactory, final VMKNetPriceLookupStrategy blueNetPriceLookupStrategy) {
        this.commonI18NService = commonI18NService;
        this.priceDataFactory = priceDataFactory;
        this.blueNetPriceLookupStrategy = blueNetPriceLookupStrategy;
    }

    @Override
    protected void doPopulate(final ProductModel source, final ProductData target) {
        final BigDecimal price = this.blueNetPriceLookupStrategy.getNetPriceForProduct(source);
        if (price != null) {
            final String isoCode = this.commonI18NService.getCurrentCurrency().getIsocode();
            target.setNetPriceData(this.priceDataFactory.create(PriceDataType.BUY, price, isoCode));
        }
    }
}
