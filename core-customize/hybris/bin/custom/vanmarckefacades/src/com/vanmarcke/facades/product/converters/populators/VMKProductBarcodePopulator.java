package com.vanmarcke.facades.product.converters.populators;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;

/**
 * The {@link VMKProductBarcodePopulator} class is used to populat the product barcodes.
 *
 * @author Christiaan Janssen
 * @since 10-01-2020
 */
public class VMKProductBarcodePopulator<SOURCE extends ProductModel, TARGET extends ProductData> implements Populator<SOURCE, TARGET> {

    /**
     * {@inheritDoc}
     */
    @Override
    public void populate(final SOURCE source, final TARGET target) {
        target.setBarcode(source.getVanmarckeItemBarcode());
        target.setSupplierBarcode(source.getVendorItemBarcode_current());
    }
}
