package com.vanmarcke.facades.product.converters.populators;

import com.vanmarcke.facades.sitechannel.AbstractSiteChannelAwarePopulator;
import de.hybris.platform.commercefacades.product.data.VariantOptionData;
import de.hybris.platform.variants.model.VariantProductModel;

/**
 * Basic populator for VariantOptionData
 */
public class VMKProductVariantBasicPopulator extends AbstractSiteChannelAwarePopulator<VariantProductModel, VariantOptionData> {

    @Override
    protected void doPopulate(final VariantProductModel source, final VariantOptionData target) {
        target.setCode(source.getCode());
        target.setName(source.getName());
        target.setDescription(source.getDescription());
    }
}
