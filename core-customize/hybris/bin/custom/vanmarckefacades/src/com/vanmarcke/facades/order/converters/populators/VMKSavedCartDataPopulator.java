package com.vanmarcke.facades.order.converters.populators;

import com.vanmarcke.facades.data.order.SavedCartData;
import com.vanmarcke.facades.product.converters.populators.VMKOrderEntryPricePopulator;
import de.hybris.platform.acceleratorservices.enums.ImportStatus;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.converters.ConfigurablePopulator;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

public class VMKSavedCartDataPopulator implements Populator<AbstractOrderModel, SavedCartData> {

    private Populator<AbstractOrderEntryModel, OrderEntryData> orderEntryPopulator;
    private Populator<AbstractOrderEntryModel, OrderEntryData> blueOrderEntryPricePopulator;

    @Override
    public void populate(AbstractOrderModel cartModel, SavedCartData savedCartData) throws ConversionException {

        savedCartData.setImportStatus(ImportStatus.COMPLETED);
        savedCartData.setName(cartModel.getGuid());
        savedCartData.setSaveTime(cartModel.getModifiedtime());

        // technical sheet data
        boolean hasTechnicalDataSheet = cartModel.getEntries()
                .stream()
                .anyMatch(entry -> entry.getProduct().getTech_data_sheet() != null);
        savedCartData.setTechnicalSheetsAvailable(hasTechnicalDataSheet);

        savedCartData.setQuantity(getQuantity(cartModel));

        final List<OrderEntryData> entryDataList = Optional.ofNullable(cartModel.getEntries())
                .orElseGet(Collections::emptyList)
                .stream()
                .map(entryModel -> {
                    final OrderEntryData entryData = new OrderEntryData();
                    orderEntryPopulator.populate(entryModel, entryData);
                    blueOrderEntryPricePopulator.populate(entryModel, entryData);
                    return entryData;
                })
                .collect(Collectors.toList());
        savedCartData.setEntries(entryDataList);
    }

    /**
     * Returns the quantity.
     *
     * @param cartModel the cart
     * @return the quantity
     */
    private long getQuantity(final AbstractOrderModel cartModel) {
        return cartModel.getEntries()
                .stream()
                .mapToLong(AbstractOrderEntryModel::getQuantity)
                .sum();
    }


    public void setOrderEntryPopulator(Populator<AbstractOrderEntryModel, OrderEntryData> orderEntryPopulator) {
        this.orderEntryPopulator = orderEntryPopulator;
    }

    public void setBlueOrderEntryPricePopulator(Populator<AbstractOrderEntryModel, OrderEntryData> blueOrderEntryPricePopulator) {
        this.blueOrderEntryPricePopulator = blueOrderEntryPricePopulator;
    }
}
