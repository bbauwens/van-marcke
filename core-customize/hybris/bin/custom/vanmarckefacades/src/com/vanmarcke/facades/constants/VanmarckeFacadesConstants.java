/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.vanmarcke.facades.constants;

import com.google.common.collect.ImmutableList;

import java.util.List;

/**
 * Global class for all VanmarckeFacades constants.
 */
@SuppressWarnings("PMD")
public class VanmarckeFacadesConstants extends GeneratedVanmarckeFacadesConstants {
    public static final String EXTENSIONNAME = "vanmarckefacades";

    private VanmarckeFacadesConstants() {
        //empty
    }

    public interface SOLR {

        interface POINT_OF_SERVICE {

            int SOLR_OFFSET = 0;

            String INDEX_DISPLAY_NAME = "displayName_text";
            String INDEX_NAME = "name_text";
            String INDEX_COUNTRY = "country_string";
            String INDEX_TOWN = "town_text";
            String INDEX_STREET_NUMBER = "streetNumber_string";
            String INDEX_POSTAL_CODE = "postalCode_text";
            String INDEX_PHONE_1 = "phone1_string";
            String INDEX_STREET_NAME = "streetName_string";
            String INDEX_LATITUDE = "latitude_double";
            String INDEX_LONGITUDE = "longitude_double";
            String INDEX_OPENING_TIME_TEMPLATE = "openingTime_%s_date";
            String INDEX_CLOSING_TIME_TEMPLATE = "closingTime_%s_date";
            String INDEX_COORDINATE = "position_coordinate";
            String INDEX_DISTANCE = "distance";
        }

        interface PRODUCTS {
            // '%s' should be replaced by country on the session...
            String INDEX_AVAILABILITY = "available_%s_%s_boolean";
        }

        String DEFAULT_SORT = "default";
    }


    public interface SESSION {

        interface ATTRIBUTES {
            String STORE_FRONT_FILTER = "com.vanmarcke.storefront.filters.StorefrontFilter";
        }
    }

    public interface I18N {
        String TEXT_SAVEDCART_UPLOAD_SUMMARY_NOT_IMPORTED = "text.savedcart.upload.summary.notImported";
        String TEXT_SAVEDCART_UPLOAD_SUMMARY = "text.savedcart.upload.summary";
    }

}
