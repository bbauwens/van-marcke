package com.vanmarcke.facades.basesite;

import de.hybris.platform.commercefacades.basesite.data.BaseSiteData;

import java.util.List;

/**
 * The {@link VMKBaseSiteFacade} interface defines the methods to hide the business logic concerning base sites.
 *
 * @author Christiaan Janssen
 * @since 03-11-2020
 */
public interface VMKBaseSiteFacade {

    /**
     * Returns the base sites.
     *
     * @return the base sites
     */
    List<BaseSiteData> getActiveBaseSites();

    /**
     * Returns the base site for the given {@code uid}.
     *
     * @param uid the base site's unique identifier
     * @return the base site
     */
    BaseSiteData getBaseSite(String uid);
}
