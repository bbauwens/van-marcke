package com.vanmarcke.facades.wishlist2;

import com.vanmarcke.facades.data.wishlist2.RestoreProductData;
import com.vanmarcke.facades.data.wishlist2.WishListData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.core.servicelayer.data.PaginationData;
import de.hybris.platform.core.servicelayer.data.SearchPageData;
import de.hybris.platform.core.servicelayer.data.SortData;
import de.hybris.platform.wishlist2.model.Wishlist2Model;
import org.apache.commons.lang3.tuple.Pair;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public interface VMKWishListFacade {

    /**
     * Returns a {@link List} of {@link WishListData} instances for the current user.
     *
     * @return the {@link List} of {@link WishListData} instances
     */
    SearchPageData<WishListData> getWishListsOverview(PaginationData paginationData, SortData sortData);

    /**
     * Returns the {@link WishListData} instance for the current user with the given {@code name}.
     *
     * @param name the name
     * @return the {@link WishListData} instance
     */
    WishListData getWishList(String name);

    /**
     * Returns the {@link Wishlist2Model} instance for the current user with the given {@code name}.
     *
     * @param name the name
     * @return the {@link Wishlist2Model} instance
     */
    Wishlist2Model getWishListModel(String name);


    WishListData getWishListWithNetValues(String name, boolean showNetPrice);

    /**
     * Removes the {@link WishListData} instance with the given name.
     *
     * @param name the name
     */
    void removeWishList(String name);

        /**
         * Removes the {@link OrderEntryData} instance the given {@code productCode} from the wish list for the current user with the given {@code wishlistName}.
         *
         * @param id the wish list entry id
         */
    void removeWishListEntry(String id);

    /**
     * Restore the specified wishlist to the cart
     *
     * @param name         wish list name
     * @param products    the products data with codes and quantities to restore
     */
    String restoreWishlist(String name, List<RestoreProductData> products);

    /**
     * Save the current cart to a wish list
     *
     * @param wishListName Wish list name
     * @param wishListDescription Wish list description
     */
    void saveCartToWishList(String wishListName, String wishListDescription);

    /**
     * Import a wishlist from the specified CSV data
     *
     * @param csvFileData CSV file to import
     */
    void importWishlistFromCsv(byte[] csvFileData) throws IOException;

    /**
     * Add specified entries to the wishlist
     *
     * @param name        the name of the wishlist
     * @param description the description of the wishlist
     * @param entries     Entries (product and quantity) to include
     * @param createNew   true if a new wishlist is to be created, false if adding to an existing wishlist
     * @return The updated wish list
     */
    WishListData addToWishList(String name, String description, List<Pair<String, Integer>> entries, boolean createNew);

    /**
     * Update the wishlist
     *
     * @param original    The original name of the wishlist
     * @param name        The new name for the wishlist
     * @param description The new description
     * @param entries     Updated entries
     * @return Updated wish list
     */
    WishListData update(WishListData original, String name, String description, List<OrderEntryData> entries);

    /**
     * Get number of wish lists for the current user
     *
     * @return Number of wish lists for the current user
     */
    Integer getWishListCount();

    /**
     * Export the technical sheets for the specified wishlist
     *
     * @param name Wishlist to export the sheets for
     * @return Exported sheets.
     */
    InputStream exportTechnicalDataSheetsForWishList(String name);
}