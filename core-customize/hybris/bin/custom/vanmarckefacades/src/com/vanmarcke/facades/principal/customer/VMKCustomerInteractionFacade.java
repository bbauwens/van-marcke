package com.vanmarcke.facades.principal.customer;

import com.vanmarcke.facades.customerinteraction.data.CustomerInteractionData;

/**
 * Interface for the Blue Customer Interaction Facade
 */
@FunctionalInterface
public interface VMKCustomerInteractionFacade {

    /**
     * Triggers the customer interaction mail
     *
     * @param customerInteractionData the customerInteractionData
     */
    void submitOrderCustomerInteraction(CustomerInteractionData customerInteractionData);
}
