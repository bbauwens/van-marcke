package com.vanmarcke.facades.search.converters;

import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.solrfacetsearch.search.AbstractSolrConverter;
import de.hybris.platform.solrfacetsearch.search.impl.SolrResult;

import static org.apache.commons.lang3.StringUtils.isNotEmpty;

/**
 * Basic Category Solr Search Result Converter
 */
public class VMKCategorySearchBasicConverter extends AbstractSolrConverter<CategoryData> {

    /**
     * {@inheritDoc}
     */
    @Override
    protected CategoryData createDataObject() {
        return new CategoryData();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CategoryData convert(SolrResult solrResult, CategoryData categoryData) throws ConversionException {
        categoryData.setCode(getValue(solrResult, "code"));
        categoryData.setName(getValue(solrResult, "name"));
        categoryData.setUrl(getValue(solrResult, "url"));

        populateThumbnail(solrResult, categoryData);
        return categoryData;
    }

    /**
     * Populates the thumbnail from the given {@link SolrResult} to the {@link CategoryData}.
     *
     * @param source the solr result
     * @param target the category data
     */
    protected void populateThumbnail(final SolrResult source, final CategoryData target) {
        final String thumbnail = getValue(source, "thumbnail");
        if (isNotEmpty(thumbnail)) {
            target.setThumbnail(createImageData(thumbnail));
        }
    }

    /**
     * Creates an {@link ImageData} instance from the given image url.
     *
     * @param imageUrl the image url
     * @return the image data
     */
    private ImageData createImageData(String imageUrl) {
        ImageData imageData = new ImageData();
        imageData.setUrl(imageUrl);
        return imageData;
    }

}
