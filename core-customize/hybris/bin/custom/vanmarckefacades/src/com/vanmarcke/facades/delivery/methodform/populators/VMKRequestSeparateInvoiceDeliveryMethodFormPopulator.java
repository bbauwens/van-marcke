package com.vanmarcke.facades.delivery.methodform.populators;

import com.vanmarcke.services.forms.delivery.DeliveryMethodForm;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.session.SessionService;
import org.apache.commons.lang.BooleanUtils;

import static com.vanmarcke.core.constants.VanmarckeCoreConstants.REQUEST_SEPARATE_INVOICE;
import static com.vanmarcke.core.enums.ReferenceConditionalType.MANDATORY;

/**
 * Populates the separate Invoice Delivery boolean on the {@link DeliveryMethodForm}.
 *
 * @author Niels Raemaekers
 * @since 17-03-2021
 */
public class VMKRequestSeparateInvoiceDeliveryMethodFormPopulator implements Populator<CartModel, DeliveryMethodForm> {

    private static final Integer LENGTH = 6;

    private final SessionService sessionService;

    /**
     * Creates a new instance of the {@link VMKRequestSeparateInvoiceDeliveryMethodFormPopulator} class.
     * @param sessionService the session service
     */
    public VMKRequestSeparateInvoiceDeliveryMethodFormPopulator (SessionService sessionService) {
        this.sessionService = sessionService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void populate(CartModel source, DeliveryMethodForm target) {
        B2BCustomerModel currentCustomer = (B2BCustomerModel) source.getUser();
        boolean ref1Mandatory = MANDATORY.equals(currentCustomer.getDefaultB2BUnit().getRef1());
        target.setRequestSeparateInvoice(ref1Mandatory
                || BooleanUtils.toBooleanDefaultIfNull(sessionService.getAttribute(REQUEST_SEPARATE_INVOICE), false));
        if (ref1Mandatory) {
            target.setPurchaseOrderNumber(getRef1Placeholder(source.getCode()));
        }
    }

    /**
     * Returns the placeholder for the REF 1 field.
     *
     * @param code the code
     * @return the ref1 placeholder
     */
    protected String getRef1Placeholder(String code) {
        String ref1 = code;
        if (code.length() > LENGTH) {
            ref1 = code.substring(code.length() - LENGTH);
        }
        return ref1;
    }
}
