package com.vanmarcke.facades.cms;

import java.util.List;

/**
 * The {@link VMKConsentParagraphComponentFacade} class hides the business logic concerning instances of the
 * {@link ConsentParagraphComponentModel} class.
 *
 * @author Christiaan Janssen
 * @since 16-07-2020
 */
public interface VMKConsentParagraphComponentFacade {

    /**
     * Returns the {@link ConsentParagraphData} instances.
     *
     * @return the {@link ConsentParagraphData} instances
     */
    List<ConsentParagraphData> getConsentParagraphs();
}
