package com.vanmarcke.facades.product.converters.populators;

import com.vanmarcke.facades.analytics.data.AnalyticsData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.springframework.util.Assert;

/**
 * Populates {@link ProductData} with {@link AnalyticsData}.
 *
 * @author Joris Cryns, Christiaan Janssen
 * @since 27-9-2019
 */
public class VMKProductAnalyticsPopulator implements Populator<ProductModel, ProductData> {

    private final Converter<ProductModel, AnalyticsData> analyticsConverter;

    /**
     * Creates a new instance of the {@link VMKProductAnalyticsPopulator} class.
     *
     * @param analyticsConverter the analytics converter
     */
    public VMKProductAnalyticsPopulator(Converter<ProductModel, AnalyticsData> analyticsConverter) {
        this.analyticsConverter = analyticsConverter;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void populate(ProductModel source, ProductData target) {
        Assert.notNull(source, "Parameter source cannot be null.");
        Assert.notNull(target, "Parameter target cannot be null.");

        target.setAnalyticsData(analyticsConverter.convert(source));
    }
}
