package com.vanmarcke.facades.delivery.methodform.populators;

import com.vanmarcke.services.forms.delivery.DeliveryMethodForm;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.CartModel;

/**
 * Populates the delivery method code on the {@link DeliveryMethodForm}.
 * <p>
 * This code is dependent on the delivery method and the country, e.g.: be-tec, fr-standard.
 */
public class VMKDeliveryMethodCodePopulator implements Populator<CartModel, DeliveryMethodForm> {

    /**
     * {@inheritDoc}
     */
    @Override
    public void populate(CartModel source, DeliveryMethodForm target) {
        target.setDeliveryOption(source.getDeliveryMode() != null ? source.getDeliveryMode().getCode() : null);
    }
}
