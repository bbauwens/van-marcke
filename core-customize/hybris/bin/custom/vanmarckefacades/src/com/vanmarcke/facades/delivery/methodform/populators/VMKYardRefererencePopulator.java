package com.vanmarcke.facades.delivery.methodform.populators;

import com.vanmarcke.services.forms.delivery.DeliveryMethodForm;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.CartModel;

/**
 * Provides the {@link DeliveryMethodForm} with the yard reference.
 *
 * @author Tom van den Berg
 * @since 30-01-2020
 */
public class VMKYardRefererencePopulator implements Populator<CartModel, DeliveryMethodForm> {

    /**
     * {@inheritDoc}
     */
    @Override
    public void populate(CartModel source, DeliveryMethodForm target) {
        target.setYardReference(source.getYardReference());
    }
}
