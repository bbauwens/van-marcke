package com.vanmarcke.facades.search.impl;

import com.vanmarcke.facades.search.VMKSolrProductSearchFacade;
import com.vanmarcke.facades.search.data.VariantSearchStateData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.search.data.SearchFilterQueryData;
import de.hybris.platform.commercefacades.search.data.SearchQueryData;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commercefacades.search.solrfacetsearch.impl.DefaultSolrProductSearchFacade;
import de.hybris.platform.commerceservices.enums.SearchQueryContext;
import de.hybris.platform.commerceservices.search.facetdata.ProductSearchPageData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.FilterQueryOperator;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryData;
import de.hybris.platform.commerceservices.threadcontext.ThreadContextService;

import java.util.List;
import java.util.Set;

/**
 * Custom extension for solr product search facade
 *
 * @author Cristi Stoica
 * @since 20-07-2021
 */
public class VMKSolrProductSearchFacadeImpl extends DefaultSolrProductSearchFacade<ProductData> implements VMKSolrProductSearchFacade<ProductData> {

    /**
     * {@inheritDoc}
     */
    @Override
    public ProductSearchPageData<SearchStateData, ProductData> searchSolrProductVariants(VariantSearchStateData variantSearchStateData) {
        final PageableData pageableData = new PageableData();
        pageableData.setPageSize(100);
        if (variantSearchStateData.getCategoryCode() != null) {
            return getThreadContextService().executeInContext(
                    (ThreadContextService.Executor<ProductSearchPageData<SearchStateData, ProductData>, ThreadContextService.Nothing>) () -> getProductCategorySearchPageConverter()
                            .convert(getProductSearchService().searchAgain(decodeState(variantSearchStateData, variantSearchStateData.getCategoryCode()), pageableData)));
        } else {
            return this.textSearch(variantSearchStateData, pageableData);
        }
    }
}
