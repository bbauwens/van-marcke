package com.vanmarcke.facades.csv;

import com.vanmarcke.facades.data.order.SavedCartData;
import de.hybris.platform.acceleratorfacades.csv.CsvFacade;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.wishlist2.model.Wishlist2Model;

import java.io.IOException;
import java.io.Writer;
import java.util.List;

/**
 * CSV Related operations for WishLists
 */
public interface VMKCsvFacade extends CsvFacade {

    /**
     * Generate a CSV file from a provided wish list instance
     *
     * @param headers Headers, which should already be translated
     * @param includeHeader Include the header or not
     * @param savedCartData Information about the saved cart
     * @param netPrice whether or not to show the net price
     * @param writer Writer to put the information into
     *
     * @throws IOException
     */
    void generateCsvFromWishlist(final List<String> headers, final boolean includeHeader, final SavedCartData savedCartData, boolean netPrice, final Writer writer, int emptyLines)
            throws IOException;

    /**
     * Create a new wish list from the provided CSV file
     *
     * @param userModel The user to create the wish list for
     * @param csvFileData CSF file containing the data to be imported
     * @return A new WishList instance from the provided data
     *
     * @throws IOException
     */
    Wishlist2Model createFromCsv(UserModel userModel, byte[] csvFileData) throws IOException;
}
