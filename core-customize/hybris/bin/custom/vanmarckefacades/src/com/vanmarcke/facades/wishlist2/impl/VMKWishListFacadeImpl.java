package com.vanmarcke.facades.wishlist2.impl;

import com.vanmarcke.facades.csv.VMKCsvFacade;
import com.vanmarcke.facades.data.wishlist2.RestoreProductData;
import com.vanmarcke.facades.data.wishlist2.WishListData;
import com.vanmarcke.facades.wishlist2.VMKWishListFacade;
import com.vanmarcke.services.util.PdfUtils;
import com.vanmarcke.services.wishlist2.VMKWishlist2Service;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.core.servicelayer.data.PaginationData;
import de.hybris.platform.core.servicelayer.data.SearchPageData;
import de.hybris.platform.core.servicelayer.data.SortData;
import de.hybris.platform.order.CartService;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.wishlist2.enums.Wishlist2EntryPriority;
import de.hybris.platform.wishlist2.model.Wishlist2EntryModel;
import de.hybris.platform.wishlist2.model.Wishlist2Model;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.http.MediaType;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class VMKWishListFacadeImpl implements VMKWishListFacade {

    private final VMKWishlist2Service wishlistService;
    private final ModelService modelService;
    private final ProductService productService;
    private final Converter<Wishlist2Model, WishListData> wishlistOverviewConverter;
    private final Converter<Wishlist2Model, WishListData> wishlistGrossConverter;
    private final Converter<Wishlist2Model, WishListData> wishlistNetConverter;
    private final VMKCsvFacade vmkCsvFacade;
    private final CartService cartService;
    private final UserService userService;
    private final MediaService mediaService;
    private final PdfUtils pdfUtils;
    private final BaseSiteService baseSiteService;

    public static final String WISHLIST_SORT_BY_NAME = "byName";
    public static final String WISHLIST_SORT_BY_MODIFIED_TIME = "byDateModified";

    /**
     * Creates a new instance of the {@link VMKWishListFacadeImpl} class.
     *  @param wishlistService the wish list service
     * @param modelService    the model service
     */
    public VMKWishListFacadeImpl(VMKWishlist2Service wishlistService,
                                 ModelService modelService,
                                 Converter<Wishlist2Model, WishListData> wishlistGrossConverter,
                                 Converter<Wishlist2Model, WishListData> wishlistNetConverter,
                                 Converter<Wishlist2Model, WishListData> wishlistOverviewConverter,
                                 ProductService productService,
                                 VMKCsvFacade vmkCsvFacade,
                                 CartService cartService,
                                 UserService userService,
                                 MediaService mediaService,
                                 PdfUtils pdfUtils,
                                 BaseSiteService baseSiteService) {
        this.wishlistService = wishlistService;
        this.modelService = modelService;
        this.productService = productService;
        this.wishlistOverviewConverter = wishlistOverviewConverter;
        this.vmkCsvFacade = vmkCsvFacade;
        this.cartService = cartService;
        this.userService = userService;
        this.mediaService = mediaService;
        this.pdfUtils = pdfUtils;
        this.wishlistGrossConverter = wishlistGrossConverter;
        this.wishlistNetConverter = wishlistNetConverter;
        this.baseSiteService = baseSiteService;
    }

    @Override
    public SearchPageData<WishListData> getWishListsOverview(PaginationData paginationData, SortData sortData) {
        return getWishListsInternal(paginationData, sortData, wishlistOverviewConverter);
    }

    protected SearchPageData<WishListData> getWishListsInternal(final PaginationData paginationData,
                                                                final SortData sortData,
                                                                final Converter<Wishlist2Model, WishListData> converter) {
        // perform the search
        final SearchPageData<Wishlist2Model> wishListModels = wishlistService.getWishlists(paginationData, toInternalSortData(sortData));
        final List<WishListData> wishListDatas = Converters.convertAll(wishListModels.getResults(), converter);

        // and convert
        final SearchPageData<WishListData> result = new SearchPageData<>();

        result.setPagination(wishListModels.getPagination());
        result.setSorts(wishListModels.getSorts());
        result.setResults(wishListDatas);

        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public WishListData getWishList(String name) {
        final boolean showNetPrice = BooleanUtils.isTrue(userService.getCurrentUser().getShowNetPrice()
                || baseSiteService.getCurrentBaseSite().getChannel() == SiteChannel.DIY);
        return getWishListWithNetValues(name, showNetPrice);
    }


    public Wishlist2Model getWishListModel(String name) {
        return wishlistService.getWishlistForCurrentUserAndName(name);
    }

    @Override
    public WishListData getWishListWithNetValues(final String name, final boolean showNetPrice) {
        // fetch the wishlist object
        final Wishlist2Model wishlist2Model = wishlistService.getWishlistForCurrentUserAndName(name);

        return showNetPrice ?
                wishlistNetConverter.convert(wishlist2Model) :
                wishlistGrossConverter.convert(wishlist2Model);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeWishList(String name) {
        wishlistService.removeWishlistForCurrentUserAndName(name);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeWishListEntry(String id) {
        wishlistService.removeWishlistEntryForPK(id);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String restoreWishlist(String name, List<RestoreProductData> products) {
        final WishListData wishListData = getWishList(name);
        final CartModel sessionCart = cartService.getSessionCart();
        // add all entries to the current cart

        Map<String, RestoreProductData> productQuantities = products != null ? products.stream()
                    .collect(Collectors.toMap(RestoreProductData::getProductCode, Function.identity())) : Collections.emptyMap();

        CollectionUtils.emptyIfNull(wishListData.getEntries()).stream()
                .filter(entryData -> CollectionUtils.isEmpty(products) || productQuantities.containsKey(entryData.getProduct().getCode()))
                .forEach(entryData -> {
                    // prepare all parameters
                    String productCode = entryData.getProduct().getCode();
                    final ProductModel productModel = productService.getProductForCode(productCode);
                    long quantity;
                    if (productQuantities.containsKey(productCode) && productQuantities.get(productCode).getQuantity() != null) {
                        quantity = productQuantities.get(productCode).getQuantity();
                    } else {
                        quantity = entryData.getQuantity() != null ? entryData.getQuantity() : 1L;
                    }
                    final UnitModel unitModel = productService.getOrderableUnit(productModel);

                    // and add to the cart
                    cartService.addNewEntry(sessionCart, productModel, quantity, unitModel, -1, true);
                });
        cartService.saveOrder(sessionCart);
        return sessionCart.getGuid();
    }

    public void saveCartToWishList(String wishListName, String wishListDescription) {
        final CartModel cartModel = cartService.getSessionCart();
        final UserModel currentUser = userService.getCurrentUser();

        wishlistService.convertCartToWishlist(wishListName, wishListDescription, cartModel, currentUser);
    }

    @Override
    public void importWishlistFromCsv(final byte[] csvFileData) throws IOException {
        final UserModel currentUser = userService.getCurrentUser();
        vmkCsvFacade.createFromCsv(currentUser, csvFileData);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public WishListData addToWishList(String name, String description, List<Pair<String, Integer>> entries, boolean createNew) {
        final UserModel user = userService.getCurrentUser();
        final boolean showNetPrice = BooleanUtils.isTrue(user.getShowNetPrice() || baseSiteService.getCurrentBaseSite().getChannel() == SiteChannel.DIY);

        Wishlist2Model wishlist2Model = createNew ? wishlistService.createWishlistForCurrentUserAndNameAndDescription(name, description)
                : wishlistService.findWishListByUserAndName(user, name);

        // add the required entries
        for (Pair<String, Integer> entry : entries) {
            try {
                final ProductModel productModel = productService.getProductForCode(entry.getLeft());
                final int quantity = entry.getRight() != null ? entry.getRight() : 1;
                wishlistService.addWishlistEntry(wishlist2Model, productModel, quantity, Wishlist2EntryPriority.HIGH, null);
            } catch (Exception e) {
            }
        }

        modelService.save(wishlist2Model);

        return showNetPrice ? wishlistNetConverter.convert(wishlist2Model) : wishlistGrossConverter.convert(wishlist2Model);
    }

    @Override
    public WishListData update(WishListData original, String name, String description, List<OrderEntryData> entries) {

        final UserModel user = userService.getCurrentUser();
        final boolean showNetPrice = BooleanUtils.isTrue(user.getShowNetPrice()
                || baseSiteService.getCurrentBaseSite().getChannel() == SiteChannel.DIY);

        // get the proper wishlist
        final Wishlist2Model wishlist2Model = wishlistService.getWishlistForCurrentUserAndName(original.getName());
        wishlist2Model.setName(name);
        wishlist2Model.setDescription(description);

        final List<Wishlist2EntryModel> newEntries = new ArrayList<>(wishlist2Model.getEntries());

        // update the quantity or remove the lines. Therefore, we start from the last so we can remove without impact.
        for (int ctr = CollectionUtils.size(wishlist2Model.getEntries()); ctr >= 1; ctr--) {
            // easy references
            final Wishlist2EntryModel originalEntry = newEntries.get(ctr - 1);
            final Long updatedQuantity = CollectionUtils.size(entries) >= ctr ? entries.get(ctr - 1).getQuantity() : null;

            // update the entry
            if (updatedQuantity != null && updatedQuantity > 0) {
                originalEntry.setDesired(updatedQuantity.intValue());
                modelService.save(originalEntry);
            } else {
                // we remove the line
                newEntries.remove(ctr - 1);
                modelService.remove(originalEntry);
            }
        }

        wishlist2Model.setEntries(newEntries);
        modelService.save(wishlist2Model);

        return showNetPrice ? wishlistNetConverter.convert(wishlist2Model) : wishlistGrossConverter.convert(wishlist2Model);
    }

    @Override
    public Integer getWishListCount() {
        return wishlistService.getWishListsCountForUser(userService.getCurrentUser());
    }

    @Override
    public InputStream exportTechnicalDataSheetsForWishList(String name) {
        final Wishlist2Model wishlist2Model = wishlistService.getWishlistForCurrentUserAndName(name);

        if (wishlist2Model != null) {
            final List<InputStream> dataSheets = wishlist2Model.getEntries()
                    .stream()
                    .map(Wishlist2EntryModel::getProduct)
                    .map(ProductModel::getTech_data_sheet)
                    .filter(Objects::nonNull)
                    .filter(m -> (MediaType.APPLICATION_PDF_VALUE).equals(m.getMime()))
                    .map(this.mediaService::getStreamFromMedia)
                    .collect(Collectors.toList());

            if (CollectionUtils.isNotEmpty(dataSheets)) {
                return pdfUtils.mergePdfFiles(dataSheets);
            }
        }

        return null;
    }

    private SortData toInternalSortData(final SortData sortData) {
        final SortData sd = new SortData();

        switch (sortData.getCode()) {
            case WISHLIST_SORT_BY_NAME:
                sd.setCode(Wishlist2Model.NAME);
                sd.setAsc(sortData.isAsc());
                break;
            case WISHLIST_SORT_BY_MODIFIED_TIME:
            default:
                sd.setCode(Wishlist2Model.MODIFIEDTIME);
                sd.setAsc(sortData.isAsc());
        }
        return sd;
    }
}