package com.vanmarcke.facades.principal.b2bunit.converters.populators;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2bcommercefacades.company.data.B2BUnitData;
import de.hybris.platform.converters.Populator;

/**
 * The {@link VMKB2BUnitReferencesPopulator} class populates the B2BUnit references information.
 *
 * @author Niels Raemaekers
 * @since 18-03-2021
 */
public class VMKB2BUnitReferencesPopulator implements Populator<B2BUnitModel, B2BUnitData> {

    /**
     * {@inheritDoc}
     */
    @Override
    public void populate(B2BUnitModel source, B2BUnitData target) {
        target.setRef1(source.getRef1().getCode());
        target.setRef2(source.getRef2().getCode());
    }
}
