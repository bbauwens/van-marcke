package com.vanmarcke.facades.basesite.impl;

import com.vanmarcke.facades.basesite.VMKBaseSiteFacade;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commercefacades.basesite.data.BaseSiteData;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.site.BaseSiteService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * The {@link VMKBaseSiteFacadeImpl} class implements the methods to hide the business logic concerning base sites.
 *
 * @author Christiaan Janssen
 * @since 03-11-2020
 */
public class VMKBaseSiteFacadeImpl implements VMKBaseSiteFacade {

    private final BaseSiteService baseSiteService;
    private final Converter<BaseSiteModel, BaseSiteData> baseSiteConverter;

    /**
     * Creates a new instance of the {@link VMKBaseSiteFacadeImpl} class.
     *
     * @param baseSiteService   the base site service
     * @param baseSiteConverter the base site converter
     */
    public VMKBaseSiteFacadeImpl(final BaseSiteService baseSiteService,
                                 final Converter<BaseSiteModel, BaseSiteData> baseSiteConverter) {
        this.baseSiteService = baseSiteService;
        this.baseSiteConverter = baseSiteConverter;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<BaseSiteData> getActiveBaseSites() {
        List<BaseSiteData> baseSitesDatas = new ArrayList<>();

        final Collection<BaseSiteModel> baseSiteModels = baseSiteService.getAllBaseSites();
        if (CollectionUtils.isNotEmpty(baseSiteModels)) {
            baseSitesDatas = baseSiteConverter.convertAll(baseSiteModels
                    .stream()
                    .filter(site -> BooleanUtils.isTrue(site.getEcommerceEnabled()))
                    .collect(Collectors.toList()));
        }

        return baseSitesDatas;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BaseSiteData getBaseSite(final String uid) {
        final BaseSiteModel baseSiteModel = baseSiteService.getBaseSiteForUID(uid);
        if (baseSiteModel == null || BooleanUtils.isNotTrue(baseSiteModel.getEcommerceEnabled())) {
            throw new UnknownIdentifierException(MessageFormat.format("No site found for uit ''{0}''.", uid));
        }
        return baseSiteConverter.convert(baseSiteModel);
    }
}
