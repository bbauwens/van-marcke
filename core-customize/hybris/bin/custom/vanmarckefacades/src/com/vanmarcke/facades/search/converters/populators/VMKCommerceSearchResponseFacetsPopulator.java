package com.vanmarcke.facades.search.converters.populators;

import de.hybris.platform.commerceservices.search.facetdata.FacetData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.populators.SearchResponseFacetsPopulator;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.search.SearchResult;

import java.util.List;
import java.util.Objects;

/**
 * Custom implementation of {@link SearchResponseFacetsPopulator} to add more information to the {@link FacetData}
 *
 * @author Niels Raemaekers
 * @since 18-06-2021
 */
public class VMKCommerceSearchResponseFacetsPopulator<FACET_SEARCH_CONFIG_TYPE, INDEXED_TYPE_TYPE, INDEXED_PROPERTY_TYPE, INDEXED_TYPE_SORT_TYPE, ITEM> extends SearchResponseFacetsPopulator<FACET_SEARCH_CONFIG_TYPE, INDEXED_TYPE_TYPE, INDEXED_PROPERTY_TYPE, INDEXED_TYPE_SORT_TYPE, ITEM> {

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<FacetData<SolrSearchQueryData>> buildFacets(SearchResult solrSearchResult, SolrSearchQueryData searchQueryData, IndexedType indexedType) {
        List<FacetData<SolrSearchQueryData>> facets = performSuper(solrSearchResult, searchQueryData, indexedType);

        for (FacetData<SolrSearchQueryData> facetData : facets) {
            final IndexedProperty indexedProperty = indexedType.getIndexedProperties().get(facetData.getCode());
            if (Objects.nonNull(indexedProperty) && Objects.nonNull(indexedProperty.getUnit())) {
                facetData.setUnitSymbol(indexedProperty.getUnit());
            }
        }
        return facets;
    }

    /**
     * Perform super separately for testing ease
     *
     * @param solrSearchResult the solr search result
     * @param searchQueryData  the search query data
     * @param indexedType      the indexed type
     * @return the list with facet data
     */
    protected List<FacetData<SolrSearchQueryData>> performSuper(SearchResult solrSearchResult, SolrSearchQueryData searchQueryData, IndexedType indexedType) {
        return super.buildFacets(solrSearchResult, searchQueryData, indexedType);
    }
}
