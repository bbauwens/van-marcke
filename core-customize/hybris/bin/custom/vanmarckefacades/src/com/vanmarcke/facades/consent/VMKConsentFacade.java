package com.vanmarcke.facades.consent;

import com.vanmarcke.facades.data.VMKConsentData;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Defines methods for handling consents related to the cookie policy.
 *
 * @author Tom van den Berg
 * @since 12-06-2020
 */
public interface VMKConsentFacade {

    /**
     * Processes the given list of {@link VMKConsentData} instances.
     *
     * @param consents the consents
     * @param request  the HTTP request
     */
    void process(List<VMKConsentData> consents, HttpServletRequest request);

    /**
     * Returns the consents for the current user.
     *
     * @return the consents
     */
    String getConsentsAsString();

    /**
     * Returns the consents for the current user.
     *
     * @return the consents
     */
    List<VMKConsentData> getConsents();

    /**
     * Returns the consent types.
     *
     * @return the consent types
     */
    List<String> getConsentTypes();
}
