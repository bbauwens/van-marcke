package com.vanmarcke.facades.principal.b2bunit.impl;

import com.vanmarcke.facades.principal.b2bunit.VMKB2BUnitFacade;
import com.vanmarcke.services.b2b.VMKB2BUnitService;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2bcommercefacades.company.impl.DefaultB2BUnitFacade;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

import static java.util.stream.Collectors.toList;

/**
 * Custom implementation for B2BUnitFacade
 *
 * @author Niels Raemaekers, Tom van den Berg
 * @since 18-09-2019
 */
public class VMKB2BUnitFacadeImpl extends DefaultB2BUnitFacade implements VMKB2BUnitFacade {

    private final VMKB2BUnitService b2bUnitService;
    private final Converter<AddressModel, AddressData> addressConverter;

    /**
     * Creates a new instance of the {@link VMKB2BUnitFacadeImpl} class.
     *
     * @param b2bUnitService   the B2B unit service
     * @param addressConverter the address converter
     */
    public VMKB2BUnitFacadeImpl(final VMKB2BUnitService b2bUnitService,
                                final Converter<AddressModel, AddressData> addressConverter) {
        this.b2bUnitService = b2bUnitService;
        this.addressConverter = addressConverter;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<AddressData> getB2BUnitAddressBook() {
        return b2bUnitService.getB2BUnitModelForCurrentUser().getAddresses().stream()
                .filter(address -> BooleanUtils.isTrue(address.getShippingAddress()))
                .filter(address -> BooleanUtils.isTrue(address.getVisibleInAddressBook()))
                .filter(address -> BooleanUtils.isNotTrue(address.getBillingAddress()))
                .map(addressConverter::convert)
                .collect(toList());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addDeliveryAddressToUnit(final AddressData newAddress) {
        final B2BUnitModel b2BUnitModel = b2bUnitService.getB2BUnitModelForCurrentUser();

        super.addAddressToUnit(newAddress, b2BUnitModel.getUid());

        if (newAddress.isDefaultAddress()) {
            setDefaultDeliveryAddress(newAddress);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setDefaultDeliveryAddress(final AddressData addressData) {
        final B2BUnitModel b2BUnit = b2bUnitService.getB2BUnitModelForCurrentUser();
        final AddressModel address = getB2BCommerceUnitService().getAddressForCode(b2BUnit, addressData.getId());
        b2bUnitService.setDefaultDeliveryAddressEntry(b2BUnit, address);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isDefaultDeliveryAddress(final String addressId) {
        final AddressModel defaultShippingAddress = b2bUnitService.getB2BUnitModelForCurrentUser().getShippingAddress();
        if (defaultShippingAddress != null) {
            return (defaultShippingAddress.getPk() != null && StringUtils.equals(defaultShippingAddress.getPk().toString(), addressId));
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void editDeliveryAddress(final AddressData addressData) {
        final B2BUnitModel b2BUnit = b2bUnitService.getB2BUnitModelForCurrentUser();
        super.editAddressOfUnit(addressData, b2BUnit.getUid());
        if (addressData.isDefaultAddress()) {
            setDefaultDeliveryAddress(addressData);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeDeliveryAddress(final AddressData addressData) {
        super.removeAddressFromUnit(b2bUnitService.getB2BUnitModelForCurrentUser().getUid(), addressData.getId());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean mustAddReference() {
        return b2bUnitService.mustAddReference();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean mandatorySeparateInvoiceReference() {
        return b2bUnitService.mandatorySeparateInvoiceReference();
    }
}
