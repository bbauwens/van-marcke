package com.vanmarcke.facades.order.validation.impl;

import com.elision.hybris.l10n.facades.LocalizedMessageFacade;
import com.vanmarcke.facades.order.VMKCheckoutFacade;
import com.vanmarcke.facades.order.validation.VMKOrderValidator;
import com.vanmarcke.facades.stock.VMKStockFacade;
import de.hybris.platform.servicelayer.user.UserService;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.util.Strings;

import java.text.MessageFormat;
import java.util.Map;

import static org.apache.commons.lang.StringUtils.isNotEmpty;

/**
 * Validates the current cart for invalid entries.
 *
 * @author Tom van den Berg
 * @since 31-12-2020
 */
public class VMKOrderValidatorImpl implements VMKOrderValidator {

    private static final String INVALID_ENTRIES_MESSAGE_KEY = "cart.validation-error.entries-invalid";
    private static final String REMOVED_ENTRIES_MESSAGE_KEY = "cart.validation-error.entries-removed";
    private static final String DELIVERY_NOT_POSSIBLE_KEY = "cart.validation-error.delivery.impossible";
    private static final String ADJUSTED_STOCK_DISCONTINUED_PRODUCTS = "product.details.discontinued.popup";

    private final VMKCheckoutFacade checkoutFacade;
    private final LocalizedMessageFacade messageFacade;
    private final UserService userService;

    /**
     * Provides an instance of the VMKOrderValidatorImpl.
     *  @param checkoutFacade the checkout facade
     * @param messageFacade  the message facade
     * @param userService    the user service
     */
    public VMKOrderValidatorImpl(VMKCheckoutFacade checkoutFacade,
                                 LocalizedMessageFacade messageFacade, UserService userService) {
        this.checkoutFacade = checkoutFacade;
        this.messageFacade = messageFacade;
        this.userService = userService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String checkCartForErrors() {
        String message = "";

        String invalidCartEntries = checkoutFacade.getInvalidCartEntries();
        if (isNotEmpty(invalidCartEntries)) {
            return getActionForEntriesMessage(invalidCartEntries, INVALID_ENTRIES_MESSAGE_KEY);
        }

        Map<String, Boolean> availableDeliveryMethods = checkoutFacade.getAvailableDeliveryMethods();
        if (!availableDeliveryMethods.containsValue(true)) {
            return messageFacade.getMessageForCodeAndCurrentLocale(DELIVERY_NOT_POSSIBLE_KEY);
        }
        return message;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String removeInvalidEntriesFromCart() {
        String message = StringUtils.EMPTY;

        if (!userService.isAnonymousUser(userService.getCurrentUser())) {
            String invalidCartEntries = checkoutFacade.getInvalidCartEntries();
            if (StringUtils.isNotBlank(invalidCartEntries)) {
                boolean removeSuccess = checkoutFacade.removeInvalidCartEntries(invalidCartEntries);
                if (removeSuccess) {
                    return getActionForEntriesMessage(invalidCartEntries, REMOVED_ENTRIES_MESSAGE_KEY);
                }
            }
        }

        return message;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String adjustQuantityForDiscontinuedProducts() {
        String message = StringUtils.EMPTY;
        String adjustedProducts = checkoutFacade.adjustQuantityForDiscontinuedProducts();
        if (isNotEmpty(adjustedProducts)) {
            return getActionForEntriesMessage(adjustedProducts, ADJUSTED_STOCK_DISCONTINUED_PRODUCTS);
        }
        return message;
    }

    /**
     * Retrieve a message for the given invalid cart entries and code
     *
     * @param invalidCartEntries the invalid cart entries.
     * @param messageCode        the message code.
     * @return the message
     */
    private String getActionForEntriesMessage(String invalidCartEntries, String messageCode) {
        String message = messageFacade.getMessageForCodeAndCurrentLocale(messageCode);
        if (isNotEmpty(message)) {
            return MessageFormat.format(message, invalidCartEntries);
        }
        return Strings.EMPTY;
    }
}
