package com.vanmarcke.facades.product.converters.populators;

import com.google.common.collect.Iterables;
import de.hybris.platform.commercefacades.product.converters.populator.ProductGalleryImagesPopulator;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.ImageDataType;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaFormatModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import org.apache.commons.collections4.CollectionUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * This class extends a method from the {@link ProductGalleryImagesPopulator} to add different medias to the gallery images.
 * <p>
 * Note: images in this class denotes everything that is shown in the carousel on the PDP (including videos).
 *
 * @param <SOURCE> the product model
 * @param <TARGET> the product data
 * @author Tom van den Berg
 * @since 16-07-2020
 */
public class VMKProductGalleryImagesPopulator<SOURCE extends ProductModel, TARGET extends ProductData> extends ProductGalleryImagesPopulator<SOURCE, TARGET> {

    private static final String VIDEO = "VIDEO";

    /**
     * {@inheritDoc}
     */
    @Override
    public void populate(ProductModel source, ProductData target) {
        populateGalleryImages(source, target);

        if (CollectionUtils.isEmpty(target.getImages())) {
            populateFallbackImage(source, target);
        }
    }

    /**
     * Populates the gallery images on from the given {@link ProductModel} on the {@link ProductData}.
     *
     * @param source the product model
     * @param target the product data
     */
    protected void populateGalleryImages(ProductModel source, ProductData target) {
        List<ImageData> mediaList = getExistingImages(target);

        int galleryIndex = 0;

        for (MediaContainerModel mediaContainer : getMediaContainers(source)) {
            addMediaContainersInFormats(mediaContainer, galleryIndex++, mediaList);
        }

        for (MediaModel mediaModel : getMediaModels(source)) {
            addMedia(mediaModel, galleryIndex++, mediaList);
        }

        setAltText(source, mediaList);
        target.setImages(mediaList.isEmpty() ? Collections.emptyList() : mediaList);
    }

    /**
     * Populates the fallback gallery images for the given {@code target}.
     * <p>
     * If the given {@code source} has no gallery images, the images of the first variant are used.
     *
     * @param source the product model
     * @param target the product DTO
     */
    protected void populateFallbackImage(ProductModel source, ProductData target) {
        if (CollectionUtils.isNotEmpty(source.getVariants())) {
            populateGalleryImages(Iterables.get(source.getVariants(), 0), target);
        }
    }

    /**
     * Retrieves the existing images on the {@link ProductData} instance.
     *
     * @param target the product data
     * @return a list of ImageData
     */
    protected List<ImageData> getExistingImages(ProductData target) {
        List<ImageData> mediaList = new ArrayList<>();

        if (target.getImages() != null) {
            mediaList.addAll(target.getImages());
        }
        return mediaList;
    }

    /**
     * Retrieves the required media container models and adds them to the given list.
     *
     * @param source the product model
     * @return the media container models
     */
    protected List<MediaContainerModel> getMediaContainers(ProductModel source) {
        List<MediaContainerModel> allImages = new ArrayList<>();

        allImages.addAll(getImages(source, ProductModel.GALLERYIMAGES));
        allImages.addAll(getImages(source, ProductModel.TECH_DRAWING));
        allImages.addAll(getImages(source, ProductModel.ECO_LABEL));

        return filterImages(allImages);
    }


    /**
     * Retrieves the MediaContainers from the given {@link ProductModel} for the given {@code attribute}.
     *
     * @param source    the product model
     * @param attribute the attribute
     * @return a collection of media containers
     */
    protected List<MediaContainerModel> getImages(ProductModel source, String attribute) {
        List<MediaContainerModel> images = (List<MediaContainerModel>) getProductAttribute(source, attribute);
        if (images != null) {
            return images;
        }
        return new ArrayList<>();
    }

    /**
     * Filters images as to prevent duplicates.
     *
     * @param allImages the list of media containers
     * @return the list of media containers
     */
    private List<MediaContainerModel> filterImages(List<MediaContainerModel> allImages) {
        List<MediaContainerModel> images = new ArrayList<>();
        for (final MediaContainerModel image : allImages) {
            if (!images.contains(image)) {
                images.add(image);
            }
        }
        return images;
    }

    /**
     * Adds all required media models to a list.
     *
     * @param source the product model
     * @return the media model list
     */
    protected List<MediaModel> getMediaModels(ProductModel source) {
        List<MediaModel> medias = new ArrayList<>();
        medias.addAll(getVideo(source));
        return medias;
    }

    /**
     * Retrieves the videos for the given product.
     *
     * @param source the product model
     * @return the list of videos
     */
    protected List<MediaModel> getVideo(ProductModel source) {
        List<MediaModel> medias = new ArrayList<>();
        MediaModel media = (MediaModel) getProductAttribute(source, ProductModel.VIDEO);
        if (media != null) {
            medias.add(media);
        }
        return medias;
    }


    /**
     * Converts the given media to an {@link ImageData} instance for the required format.
     *
     * @param mediaContainer the media container
     * @param galleryIndex   the gallery index
     * @param mediaList      the media list
     */
    protected void addMediaContainersInFormats(MediaContainerModel mediaContainer, int galleryIndex, List<ImageData> mediaList) {
        for (String imageFormat : getImageFormats()) {

            String mediaFormatQualifier = getImageFormatMapping().getMediaFormatQualifierForImageFormat(imageFormat);
            if (mediaFormatQualifier == null) {
                continue;
            }

            MediaFormatModel mediaFormat = getMediaFormat(mediaFormatQualifier);
            if (mediaFormat == null) {
                continue;
            }

            MediaModel media = getMedia(mediaContainer, mediaFormat);
            if (media == null) {
                continue;
            }

            mediaList.add(convert(galleryIndex, imageFormat, media));
        }
    }

    /**
     * Adds the converted media models to the media list with the given gallery index.
     *
     * @param media        the media model
     * @param galleryIndex the gallery index
     * @param mediaList    the media list
     */
    protected void addMedia(MediaModel media, int galleryIndex, List<ImageData> mediaList) {
        if (media.getMediaFormat() != null && VIDEO.equals(media.getMediaFormat().getQualifier())) {
            mediaList.add(convert(galleryIndex, VIDEO, media));
        } else {
            mediaList.add(convert(galleryIndex, null, media));
        }
    }

    /**
     * Sets the alt text on the images.
     *
     * @param source    the product model
     * @param mediaList the list of images
     */
    protected void setAltText(ProductModel source, List<ImageData> mediaList) {
        for (ImageData imageData : mediaList) {
            if (imageData.getAltText() == null) {
                imageData.setAltText(source.getName());
            }
        }
    }


    /**
     * Converts the given {@link MediaModel} to an {@link ImageData} instance.
     *
     * @param galleryIndex the gallery index
     * @param imageFormat  the image format
     * @param media        the media instance
     * @return the image data instance
     */
    protected ImageData convert(int galleryIndex, String imageFormat, MediaModel media) {
        ImageData imageData = getImageConverter().convert(media);
        imageData.setFormat(imageFormat);
        imageData.setImageType(ImageDataType.GALLERY);
        imageData.setGalleryIndex(galleryIndex);
        return imageData;
    }

    /**
     * Retrieves the {@link MediaModel} instance for the given media object and media format.
     *
     * @param mediaObject the media object
     * @param mediaFormat the media format
     * @return the media model
     */
    protected MediaModel getMedia(ItemModel mediaObject, MediaFormatModel mediaFormat) {
        MediaModel media = null;

        if (mediaObject instanceof MediaContainerModel) {
            media = getMediaContainer((MediaContainerModel) mediaObject, mediaFormat);
        }

        if (mediaObject instanceof MediaModel) {
            media = getMediaModel((MediaModel) mediaObject, mediaFormat);
        }
        return media;
    }

    /**
     * Retrieves the media model for the given media format.
     *
     * @param mediaObject the media model
     * @param mediaFormat the media format
     * @return the media model
     */
    protected MediaModel getMediaModel(MediaModel mediaObject, MediaFormatModel mediaFormat) {
        MediaModel media = null;
        try {
            media = getMediaService().getMediaByFormat(mediaObject, mediaFormat);
        } catch (ModelNotFoundException e) {
            //ignore
        }
        return media;
    }

    /**
     * Retrieves the media container for the given format.
     *
     * @param mediaObject the media container
     * @param mediaFormat the media format
     * @return the media container
     */
    protected MediaModel getMediaContainer(MediaContainerModel mediaObject, MediaFormatModel mediaFormat) {
        MediaModel media = null;
        try {
            media = getMediaContainerService().getMediaForFormat(mediaObject, mediaFormat);
        } catch (ModelNotFoundException e) {
            // Ignore
        }
        return media;
    }

    /**
     * Retrieves the {@link MediaFormatModel} for the given media format qualifier.
     *
     * @param mediaFormatQualifier the media format qualifier
     * @return the media format model
     */
    protected MediaFormatModel getMediaFormat(String mediaFormatQualifier) {
        MediaFormatModel mediaFormat = null;
        try {
            mediaFormat = getMediaService().getFormat(mediaFormatQualifier);
        } catch (UnknownIdentifierException | AmbiguousIdentifierException e) {
            //ignore
        }
        return mediaFormat;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Object getProductAttribute(ProductModel productModel, String attribute) {
        // Added for testing purposes
        return super.getProductAttribute(productModel, attribute);
    }
}


