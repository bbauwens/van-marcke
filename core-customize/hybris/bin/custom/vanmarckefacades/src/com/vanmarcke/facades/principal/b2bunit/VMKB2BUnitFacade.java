package com.vanmarcke.facades.principal.b2bunit;

import de.hybris.platform.b2bcommercefacades.company.B2BUnitFacade;
import de.hybris.platform.commercefacades.user.data.AddressData;

import java.util.List;

/**
 * Custom interface for B2BUnitFacade
 *
 * @author Niels Raemaekers, Tom van den Berg
 * @since 18-09-2019
 */
public interface VMKB2BUnitFacade extends B2BUnitFacade {

    /**
     * Get the list of delivery addresses.
     *
     * @return the delivery addresses
     */
    List<AddressData> getB2BUnitAddressBook();

    /**
     * Associates an address to a business unit
     *
     * @param newAddress Address data object
     */
    void addDeliveryAddressToUnit(final AddressData newAddress);

    /**
     * Sets the default address
     *
     * @param addressData the addressData
     */
    void setDefaultDeliveryAddress(final AddressData addressData);

    /**
     * Checks to see if this Address is the Default Address
     *
     * @param addressId the address id
     * @return
     */
    boolean isDefaultDeliveryAddress(String addressId);

    /**
     * Updates the current address
     *
     * @param addressData the address to update
     */
    void editDeliveryAddress(AddressData addressData);

    /**
     * Removes the address for the current unit
     *
     * @param addressData the address to remove
     */
    void removeDeliveryAddress(AddressData addressData);

    /**
     * Check whether the customer must add a reference
     *
     * @return the decision
     */
    boolean mustAddReference();

    /**
     * Decisions if the customer must add the reference for a separate invoice
     *
     * @return the decision
     */
    boolean mandatorySeparateInvoiceReference();
}
