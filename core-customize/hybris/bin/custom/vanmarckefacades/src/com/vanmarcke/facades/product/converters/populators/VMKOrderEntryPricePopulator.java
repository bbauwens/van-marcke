package com.vanmarcke.facades.product.converters.populators;

import com.vanmarcke.services.product.price.VMKNetPriceLookupStrategy;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.commerceservices.price.CommercePriceService;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import org.apache.commons.lang3.BooleanUtils;

import java.math.BigDecimal;

public class VMKOrderEntryPricePopulator extends VMKAbstractEntryPricePopulator<AbstractOrderEntryModel, OrderEntryData> {

    private final CommonI18NService commonI18NService;

    public VMKOrderEntryPricePopulator(PriceDataFactory priceDataFactory, VMKNetPriceLookupStrategy blueNetPriceLookupStrategy, CommercePriceService commercePriceService, CommonI18NService commonI18NService) {
        super(priceDataFactory, blueNetPriceLookupStrategy, commercePriceService);
        this.commonI18NService = commonI18NService;
    }

    @Override
    protected void prePopulatePricing(AbstractOrderEntryModel source, OrderEntryData target) {
        // do nothing here, as pre population is already done using OOTB pricing populators
    }

    @Override
    protected boolean shouldPopulate(final AbstractOrderEntryModel source) {
        return super.shouldPopulate(source) && source instanceof CartEntryModel && source.getOrder() != null;
    }

    @Override
    protected void doPopulate(final AbstractOrderEntryModel source, final OrderEntryData target) {
        final AbstractOrderModel order = source.getOrder();

        populate(source, target, source.getProduct(), order.getCheckoutMode(), order.getCurrency(), shouldPopulateNetPrice(source), source.getOrder().getIsOrderExport() && source.getOrder().getIncludeNetPrice());
    }

    @Override
    protected Boolean shouldPopulateNetPrice(AbstractOrderEntryModel source) {
        return getBaseSiteService().getCurrentBaseSite().getChannel().equals(SiteChannel.DIY)
                || BooleanUtils.isTrue(source.getOrder().getUser().getShowNetPrice());
    }

    @Override
    protected void populateNetPrice(final OrderEntryData target, final ProductModel product, final CurrencyModel currency) {
        final BigDecimal netPrice = getBlueNetPriceLookupStrategy().getNetPriceForProduct(product);
        if (netPrice != null) {
            target.setNetPriceData(getPriceDataFactory().create(PriceDataType.BUY, netPrice, currency.getIsocode()));
        }
    }

    @Override
    protected void populateBasePrice(final OrderEntryData target, final ProductModel product, final CurrencyModel currency) {
        final PriceInformation priceInformation = getCommercePriceService().getWebPriceForProduct(product);
        if (priceInformation != null && priceInformation.getPriceValue() != null) {
            target.setBasePrice(getPriceDataFactory().create(PriceDataType.BUY, BigDecimal.valueOf(priceInformation.getPriceValue().getValue()), currency.getIsocode()));
        }
    }

    @Override
    protected void populateTotalFromBasePrice(final AbstractOrderEntryModel source, final OrderEntryData target, final CurrencyModel currency) {
        populateTotalFromPrice(source, target, currency, target.getBasePrice());
    }

    @Override
    protected void populateTotalFromNetPrice(final AbstractOrderEntryModel source, final OrderEntryData target, final CurrencyModel currency) {
        populateTotalFromPrice(source, target, currency, target.getNetPriceData());
    }

    private void populateTotalFromPrice(final AbstractOrderEntryModel source, final OrderEntryData target, final CurrencyModel currency, final PriceData priceData) {
        if (priceData != null && source.getQuantity() != null) {
            final double totalPrice = commonI18NService.roundCurrency(priceData.getValue().doubleValue() * source.getQuantity(), currency.getDigits());
            target.setTotalPrice(getPriceDataFactory().create(PriceDataType.NET, BigDecimal.valueOf(totalPrice), currency.getIsocode()));
        }
    }
}
