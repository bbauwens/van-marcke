package com.vanmarcke.facades.address.converters.populators;

import com.vanmarcke.cpi.data.ptv.PTVResponseData;
import com.vanmarcke.cpi.data.ptv.ResultAddressData;
import com.vanmarcke.facades.addressvalidation.data.AddressValidationResponseData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import org.apache.commons.lang3.StringUtils;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.vanmarcke.cpi.data.ptv.ClassificationDescriptionEnum.EXACT;
import static com.vanmarcke.cpi.data.ptv.ClassificationDescriptionEnum.UNIQUE;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;
import static java.util.Objects.isNull;

/**
 * Populator for the PTV xServer Address Validation object
 *
 * @author David Vangeneugden, Taki Korovessis, Christiaan Janssen
 * @since 14-06-2019
 */
public class VMKPTVAddressResponsePopulator implements Populator<PTVResponseData, AddressValidationResponseData> {

    private final CommonI18NService commonI18NService;
    private final Converter<CountryModel, CountryData> countryConverter;

    /**
     * Creates a new instance of the {@link VMKPTVAddressResponsePopulator} class.
     *
     * @param commonI18NService the I18N service
     * @param countryConverter  the country converter
     */
    public VMKPTVAddressResponsePopulator(CommonI18NService commonI18NService,
                                          Converter<CountryModel, CountryData> countryConverter) {
        this.commonI18NService = commonI18NService;
        this.countryConverter = countryConverter;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void populate(PTVResponseData source, AddressValidationResponseData target) {
        target.setErrorCode(source.getErrorCode());
        target.setErrorDescription(source.getErrorDescription());
        target.setResultList(getResults(source, target));
    }

    /**
     * Retrieves a list of results.
     *
     * @param source the PTV response data
     * @param target the address validation response data
     * @return a list of address data
     */
    private List<AddressData> getResults(PTVResponseData source, AddressValidationResponseData target) {
        AddressData exactMatch = getExactMatch(source);

        if (isNull(exactMatch)) {
            target.setExactMatch(false);
            return getAddressSuggestions(source);
        }

        target.setExactMatch(true);
        return Collections.singletonList(exactMatch);
    }

    /**
     * Checks for and retrieves for an exact address match in the given {@link PTVResponseData}.
     *
     * @param source the PTV response data
     * @return the R
     */
    private AddressData getExactMatch(PTVResponseData source) {
        return source.getResultList()
                .stream()
                .filter(this::isAddressExactMatch)
                .findFirst()
                .map(this::populateAddressData)
                .orElse(null);
    }

    /**
     * Retrieves a list of address suggestions.
     *
     * @param source the PTV response data
     * @return a list of addresses
     */
    private List<AddressData> getAddressSuggestions(PTVResponseData source) {
        return source.getResultList()
                .stream()
                .map(this::populateAddressData)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    /**
     * Converts the address information
     *
     * @param source the source address
     * @return the converted address
     */
    private AddressData populateAddressData(ResultAddressData source) {
        if (StringUtils.isBlank(source.getStreet())
                || StringUtils.isBlank(source.getHouseNumber())) {
            return null;
        }

        AddressData addressData = new AddressData();
        addressData.setCountry(getCountryForISOcode(source.getCountry()));
        addressData.setTown(source.getCity());
        addressData.setTown2(source.getCity2());
        addressData.setPostalCode(source.getPostCode());
        addressData.setLine1(source.getStreet());
        addressData.setLine2(source.getHouseNumber());
        return addressData;
    }

    /**
     * Returns the country information for the given {@code isoCode}.
     *
     * @param isoCode the ISO code
     * @return the country information
     */
    private CountryData getCountryForISOcode(String isoCode) {
        validateParameterNotNullStandardMessage("countryIso", isoCode);
        return countryConverter.convert(commonI18NService.getCountry(isoCode));
    }

    /**
     * Checks if the returned address is an exact match.
     *
     * @param address the source address
     * @return {@code true} in case the returned address is an exact match, {@code false} otherwise
     */
    private boolean isAddressExactMatch(ResultAddressData address) {
        return StringUtils.isNotEmpty(address.getStreet())
                && StringUtils.isNotEmpty(address.getHouseNumber())
                && address.getTotalScore() == 100
                && (EXACT.equals(address.getClassificationDescription())
                || UNIQUE.equals(address.getClassificationDescription()));
    }
}
