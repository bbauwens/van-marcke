package com.vanmarcke.facades.principal.customer.populators;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

/**
 * The {@link VMKCustomerBillingAddressPopulator} class is used to populate {@link CustomerData} instances with the information of the
 * {@link UserModel} instances.
 *
 * @author Christiaan Janssen
 * @since 13-01-2021
 */
public class VMKCustomerBillingAddressPopulator implements Populator<UserModel, CustomerData> {

    private final Converter<AddressModel, AddressData> addressConverter;

    /**
     * Creates a new instance of the {@link VMKCustomerBillingAddressPopulator} class.
     *
     * @param addressConverter the address converter
     */
    public VMKCustomerBillingAddressPopulator(final Converter<AddressModel, AddressData> addressConverter) {
        this.addressConverter = addressConverter;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void populate(final UserModel source, final CustomerData target) {
        target.setDefaultBillingAddress(getDefaultBillingAddress(source));
    }

    /**
     * Returns the default billing address for the given {@code source}.
     * <p>
     * The default billing address is the user's company billing address. Returns {@code null} in a B2C scenario.
     *
     * @param source the source
     * @return the default billing address
     */
    private AddressData getDefaultBillingAddress(final UserModel source) {
        AddressData billingAddress = null;
        if (source instanceof B2BCustomerModel) {
            final B2BCustomerModel b2bCustomer = (B2BCustomerModel) source;
            billingAddress = getDefaultBillingAddress(b2bCustomer);
        }
        return billingAddress;
    }

    /**
     * Returns the default billing address for the given {@code b2bCustomer}.
     * <p>
     * The default billing address is the user's company billing address.
     *
     * @param b2bCustomer the B2B customer
     * @return the default billing address
     */
    private AddressData getDefaultBillingAddress(final B2BCustomerModel b2bCustomer) {
        AddressData billingAddress = null;
        final B2BUnitModel b2bUnit = b2bCustomer.getDefaultB2BUnit();
        if (b2bUnit != null && b2bUnit.getBillingAddress() != null) {
            billingAddress = addressConverter.convert(b2bUnit.getBillingAddress());
        }
        return billingAddress;
    }
}
