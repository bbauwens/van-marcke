package com.vanmarcke.facades.search;

import com.vanmarcke.facades.search.data.VariantSearchStateData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.search.ProductSearchFacade;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commerceservices.enums.SearchQueryContext;
import de.hybris.platform.commerceservices.search.facetdata.ProductSearchPageData;

/**
 * Custom extension for solr product search facade
 *
 * @author Cristi Stoica
 * @since 20-07-2021
 * @param <ITEM> target result dto
 */
public interface VMKSolrProductSearchFacade<ITEM extends ProductData> extends ProductSearchFacade<ITEM> {
    /**
     * search and return variants data from solr for a given search state data
     *
     * @param variantSearchStateData search state containing search query, filter terms like base product, category code, etc
     * @return search object populated with the variants data
     */
    ProductSearchPageData<SearchStateData, ITEM> searchSolrProductVariants(VariantSearchStateData variantSearchStateData);
}
