package com.vanmarcke.facades.order.converters.populators;

import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;

public class VMKAbstractOrderExternalCodePopulator<S extends AbstractOrderModel, T extends AbstractOrderData> implements Populator<S, T> {

    @Override
    public void populate(final S source, final T target) {
        if (source instanceof OrderModel) {
            target.setExternalCode((((OrderModel) source).getExternalCode()));
        }
    }
}