package com.vanmarcke.facades.cart.converters.populators;

import com.vanmarcke.services.order.credit.VMKCreditCheckService;
import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import org.apache.commons.lang3.BooleanUtils;

/**
 * Populator that adds the creditworthiness to the cartData
 */
public class VMKCartCreditCheckPopulator<S extends AbstractOrderModel, T extends AbstractOrderData> implements Populator<S, T> {

    private final VMKCreditCheckService vmkCreditCheckService;

    /**
     * Creates a new instance of the {@link VMKCartCreditCheckPopulator} class.
     *
     * @param vmkCreditCheckService the credit check service
     */
    public VMKCartCreditCheckPopulator(final VMKCreditCheckService vmkCreditCheckService) {
        this.vmkCreditCheckService = vmkCreditCheckService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void populate(final S source, final T target) throws ConversionException {
        target.setCreditworthy(source.getCheckoutMode()
                && vmkCreditCheckService.validateCreditworthinessForCart(source));
        target.setCreditLimitWarning(BooleanUtils.toBooleanDefaultIfNull(source.getCreditLimitWarning(), false));
        target.setCreditLimitExceeded(BooleanUtils.toBooleanDefaultIfNull(source.getCreditLimitExceeded(), false));
    }
}
