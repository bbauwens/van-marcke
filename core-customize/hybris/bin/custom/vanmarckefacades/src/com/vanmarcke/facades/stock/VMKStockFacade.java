package com.vanmarcke.facades.stock;

import com.vanmarcke.facades.product.data.StockListData;
import de.hybris.platform.commercefacades.product.data.ProductData;

/**
 * Van Marcke Stock Facade, to fetch stock for products.
 *
 * @author Tom van den Berg
 * @since 19-10-2020
 */
public interface VMKStockFacade {

    /**
     * Retrieve the stock data for the given product code and current warehouse
     *
     * @param productCode the product code
     * @return the stock data
     */
    ProductData getProductDataWithStockForCurrentWareHouse(String productCode);

    /**
     * Get stock data for the given product code and warehouse.
     *
     * @param code      The code of a product
     * @param wareHouse The uid of the wareHouse
     * @return the stock list data
     */
    StockListData getStockForProductCodeAndWarehouse(String code, String wareHouse);

    /**
     * Get stock data for the given product code and all warehouses for the current basestore
     *
     * @param productCode The code of a product
     * @return the stock list data
     */
    StockListData getStockForProductCodeAndCurrentBaseStore(String productCode);

    /**
     * Get the EDC stoc
     * @param code The product code
     * @return the EDC stock
     */
    long getAvailableEDCStock(String code);
}
