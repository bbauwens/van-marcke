package com.vanmarcke.facades.address.validation;

import com.vanmarcke.facades.addressvalidation.data.AddressValidationResponseData;
import de.hybris.platform.commercefacades.user.data.AddressData;

/**
 * Address Validation Facade
 */
public interface VMKAddressValidationFacade {

    /**
     * validate the provided address and return the validationResult.
     * This can be 1 or more addresses as result, depends on the quality of the provided address.
     *
     * @param addressData addressData object to validate
     * @return List of ResultAddressData objects
     */
    AddressValidationResponseData validateAddress(AddressData addressData);
}
