package com.vanmarcke.facades.search;

import com.vanmarcke.services.search.solrfacetsearch.VMKSearchService;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfig;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfigService;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.config.exceptions.FacetConfigServiceException;
import de.hybris.platform.solrfacetsearch.enums.ConverterType;
import de.hybris.platform.solrfacetsearch.search.FacetSearchException;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import de.hybris.platform.solrfacetsearch.search.SearchResult;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import java.util.Collection;
import java.util.List;

import static java.util.Collections.emptyList;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;

/**
 * Abstract implementation of the {@link VMKSearchFacade}
 */
public abstract class AbstractVMKSearchFacade<T> implements VMKSearchFacade<T> {

    private static final Logger LOGGER = Logger.getLogger(AbstractVMKSearchFacade.class);

    private VMKSearchService searchService;
    private CMSSiteService cmsSiteService;
    private FacetSearchConfigService facetSearchConfigService;
    private ConverterType converterType;

    @Override
    public List<T> search() {
        try {
            return doSearch(searchService.createSearchQuery(getFacetSearchConfig(), getIndexedType()));
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return emptyList();
    }

    @Override
    public List<T> textSearch(String term) {
        try {
            return doSearch(searchService.createTextSearchQuery(term, getFacetSearchConfig(), getIndexedType()));
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return emptyList();
    }

    protected List<T> doSearch(final SearchQuery searchQuery) throws FacetSearchException {
        final SearchResult searchResult = searchService.search(searchQuery);
        return searchResult.getResultData(converterType);
    }

    protected abstract FacetSearchConfig getFacetSearchConfig() throws FacetConfigServiceException;

    protected IndexedType getIndexedType() throws FacetConfigServiceException {
        final Collection<IndexedType> indexedTypes = getFacetSearchConfig().getIndexConfig().getIndexedTypes().values();
        return isNotEmpty(indexedTypes) ? indexedTypes.iterator().next() : null;
    }

    @Required
    public void setSearchService(VMKSearchService searchService) {
        this.searchService = searchService;
    }

    protected VMKSearchService getSearchService() {
        return searchService;
    }

    @Required
    public void setCmsSiteService(CMSSiteService cmsSiteService) {
        this.cmsSiteService = cmsSiteService;
    }

    protected CMSSiteService getCmsSiteService() {
        return cmsSiteService;
    }

    @Required
    public void setFacetSearchConfigService(FacetSearchConfigService facetSearchConfigService) {
        this.facetSearchConfigService = facetSearchConfigService;
    }

    protected FacetSearchConfigService getFacetSearchConfigService() {
        return facetSearchConfigService;
    }

    public void setConverterType(ConverterType converterType) {
        this.converterType = converterType;
    }

    protected ConverterType getConverterType() {
        return converterType;
    }
}