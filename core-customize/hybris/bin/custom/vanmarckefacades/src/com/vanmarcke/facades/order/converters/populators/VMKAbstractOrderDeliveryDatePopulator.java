package com.vanmarcke.facades.order.converters.populators;

import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;

/**
 * Populator that adds a delivery date to the abstract order data
 */
public class VMKAbstractOrderDeliveryDatePopulator<S extends AbstractOrderModel, T extends AbstractOrderData> implements Populator<S, T> {

    @Override
    public void populate(final S source, final T target) {
        target.setDeliveryDate(source.getDeliveryDate());
    }
}