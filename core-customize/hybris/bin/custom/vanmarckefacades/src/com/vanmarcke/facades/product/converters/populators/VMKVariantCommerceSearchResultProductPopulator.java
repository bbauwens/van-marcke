package com.vanmarcke.facades.product.converters.populators;

import com.vanmarcke.facades.product.data.BrandData;
import de.hybris.platform.commercefacades.product.ProductStockStatus;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;
import de.hybris.platform.converters.Populator;
import org.apache.commons.lang.StringUtils;

/**
 * Custom SearchResultProductPopulator
 */
public class VMKVariantCommerceSearchResultProductPopulator implements Populator<SearchResultValueData, ProductData> {

    @Override
    public void populate(final SearchResultValueData source, final ProductData target) {
        // we need the name of the BaseProduct, so add this instead of the default name...
        if (StringUtils.isNotBlank(getValue(source, "baseProductName"))) {
            target.setBaseProductName(this.<String>getValue(source, "baseProductName"));
        } else {
            target.setBaseProductName(this.<String>getValue(source, "name"));
        }

        // set the brand as well so that we don t need to fetch the product model from the db just to retrieve the brand code
        if (StringUtils.isNotBlank(getValue(source, "brandCode"))) {
            if (target.getBrand() == null) {
                target.setBrand(new BrandData());
            }

            target.getBrand().setCode(getValue(source, "brandCode"));
            target.getBrand().setName(getValue(source, "brandName"));
            target.getBrand().setSeries(getValue(source, "brandSeries"));
        }

        // set number of variants...
        // if there are variants, we need to add 1, since this 'product' is also a variant, which is not in the list of variants...
        target.setNumberOfVariants(source.getVariants() != null ? source.getVariants().size() + 1 : 0);

        target.setVendorItemNumber(getValue(source, "vendorItemNumber_current"));

        if (target.getNumberOfVariants() > 1 && StringUtils.isNotBlank(getValue(source, "baseProductUrl"))) {
            target.setUrl(getValue(source, "baseProductUrl"));
        }

        if (StringUtils.isNotBlank(getValue(source, "inStockStatus"))) {
            target.setStockStatus(ProductStockStatus.valueOf(getValue(source, "inStockStatus")));
        }
    }

    protected <T> T getValue(final SearchResultValueData source, final String propertyName) {
        if (source.getValues() == null) {
            return null;
        }

        // DO NOT REMOVE the cast (T) below, while it should be unnecessary it is required by the javac compiler
        return (T) source.getValues().get(propertyName);
    }
}
