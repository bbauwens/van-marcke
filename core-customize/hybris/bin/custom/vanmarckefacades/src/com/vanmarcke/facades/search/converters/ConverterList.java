package com.vanmarcke.facades.search.converters;

import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.List;

/**
 * Interface for a list of converters.
 *
 * @param <SOURCE> the type of the source object
 * @param <TARGET> the type of the destination object
 */
public interface ConverterList<SOURCE, TARGET> {

    /**
     * Get the list of converters.
     *
     * @return the populators.
     */
    List<Converter<SOURCE, TARGET>> getConverters();

    /**
     * Set the list of converters.
     *
     * @param converters the converters
     */
    void setConverters(List<Converter<SOURCE, TARGET>> converters);
}
