package com.vanmarcke.facades.order.converters.populators;

import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

/**
 * Populator that checks if a technicalDataSheet is available
 */
public class VMKAbstractOrderTechnicalSheetsAvailabilityPopulator<S extends AbstractOrderModel, T extends AbstractOrderData> implements Populator<S, T> {

    /**
     * {@inheritDoc}
     */
    @Override
    public void populate(S source, T target) throws ConversionException {
        boolean hasTechnicalDataSheet = source.getEntries()
                .stream()
                .anyMatch(entry -> entry.getProduct().getTech_data_sheet() != null);
        target.setTechnicalSheetsAvailable(hasTechnicalDataSheet);
    }
}
