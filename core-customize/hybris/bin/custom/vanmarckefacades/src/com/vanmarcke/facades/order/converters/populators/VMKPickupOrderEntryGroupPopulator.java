package com.vanmarcke.facades.order.converters.populators;

import de.hybris.platform.commercefacades.order.converters.populator.PickupOrderEntryGroupPopulator;
import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.order.data.PickupOrderEntryGroupData;
import de.hybris.platform.commerceservices.model.PickUpDeliveryModeModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;

import java.util.ArrayList;

import static org.apache.commons.collections.CollectionUtils.isNotEmpty;

/**
 * Populates {@link AbstractOrderData} with {@link PickupOrderEntryGroupData}.
 *
 * @author Taki Korovessis
 * @since 14-12-2019
 */
public class VMKPickupOrderEntryGroupPopulator extends PickupOrderEntryGroupPopulator {

    /**
     * {@inheritDoc}
     */
    @Override
    protected void createUpdatePickupGroupData(final AbstractOrderEntryModel entryModel, final AbstractOrderData target) {
        final AbstractOrderModel orderModel = entryModel.getOrder();
        if (orderModel.getDeliveryMode() instanceof PickUpDeliveryModeModel) {
            PickupOrderEntryGroupData groupData = null;
            if (isNotEmpty(target.getPickupOrderGroups())) {
                groupData = target.getPickupOrderGroups().iterator().next();
            }
            if (groupData == null) {
                groupData = new PickupOrderEntryGroupData();
                groupData.setEntries(new ArrayList<>());
                target.getPickupOrderGroups().add(groupData);
            }

            updateGroupTotalPriceWithTax(entryModel, groupData);
            groupData.getEntries().add(getOrderEntryData(target, entryModel.getEntryNumber()));
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long sumPickupItemsQuantity(AbstractOrderModel source) {
        long sum = 0;
        if (source.getDeliveryMode() instanceof PickUpDeliveryModeModel) {
            for (final AbstractOrderEntryModel entryModel : source.getEntries()) {
                sum += entryModel.getQuantity().longValue();
            }
        }
        return sum;
    }
}