package com.vanmarcke.facades.order;

import com.vanmarcke.facades.data.order.SavedCartData;
import de.hybris.platform.b2bacceleratorfacades.order.B2BOrderFacade;

/**
 * Custom interface for the B2BOrderFacade
 *
 * @author Cristi Stoica
 * @since 16-05-2021
 */
public interface VMKB2BOrderFacade extends B2BOrderFacade {

    SavedCartData getOrderDetailsForExport(String orderGuid);
}