package com.vanmarcke.facades.order.converters.populators;

import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

public class VMKPurchaseOrderNumberCartPopulator implements Populator<CartModel, CartData> {

    @Override
    public void populate(final CartModel source, final CartData target) throws ConversionException {
        target.setPurchaseOrderNumber(source.getPurchaseOrderNumber());
    }
}