package com.vanmarcke.facades.sitechannel;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.site.BaseSiteService;
import org.springframework.beans.factory.annotation.Required;

import java.util.List;

import static org.apache.commons.collections4.IterableUtils.contains;

/**
 * Interface for a {@link SiteChannel} aware populator.
 * A populator sets values in a target instance based on values in the source instance.
 * Populators are similar to converters except that unlike converters the target instance must already exist.
 *
 * @param <SOURCE> the type of the source object
 * @param <TARGET> the type of the destination object
 */
public abstract class AbstractSiteChannelAwarePopulator<SOURCE, TARGET> implements Populator<SOURCE, TARGET> {

    private BaseSiteService baseSiteService;
    private List<SiteChannel> siteChannels;

    /**
     * {@inheritDoc}
     */
    @Override
    public final void populate(final SOURCE source, final TARGET target) {
        if (shouldPopulate(source)) {
            doPopulate(source, target);
        }
    }

    /**
     * Determines if the populator should set the values in a target instance based on values in the source instance.
     *
     * @param source the source object
     * @return <code>true</code> if the populator should populate otherwise <code>false</code>
     */
    protected boolean shouldPopulate(final SOURCE source) {
        final BaseSiteModel siteModel = baseSiteService.getCurrentBaseSite();
        return siteModel != null && contains(siteChannels, siteModel.getChannel());
    }

    /**
     * Populate the target instance with values from the source instance.
     *
     * @param source the source object
     * @param target the target to fill
     * @throws ConversionException if an error occurs
     */
    protected abstract void doPopulate(final SOURCE source, final TARGET target);

    @Required
    public void setBaseSiteService(final BaseSiteService baseSiteService) {
        this.baseSiteService = baseSiteService;
    }

    @Required
    public void setSiteChannels(final List<SiteChannel> siteChannels) {
        this.siteChannels = siteChannels;
    }

    public BaseSiteService getBaseSiteService() {
        return baseSiteService;
    }
}