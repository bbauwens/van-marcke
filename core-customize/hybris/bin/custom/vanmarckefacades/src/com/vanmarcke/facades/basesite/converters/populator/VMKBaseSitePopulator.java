package com.vanmarcke.facades.basesite.converters.populator;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commercefacades.basesite.data.BaseSiteData;
import de.hybris.platform.commercefacades.storesession.data.LanguageData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

/**
 * The {@link VMKBaseSitePopulator} class populates instances of the {@link BaseSiteData} class with the information
 * of the {@link BaseSiteModel} class.
 *
 * @author Christiaan Janssen
 * @since 12-01-2021
 */
public class VMKBaseSitePopulator implements Populator<BaseSiteModel, BaseSiteData> {

    private final Converter<LanguageModel, LanguageData> languageConverter;

    /**
     * Creates a new instance of the {@link VMKBaseSitePopulator} class.
     *
     * @param languageConverter the language converter
     */
    public VMKBaseSitePopulator(final Converter<LanguageModel, LanguageData> languageConverter) {
        this.languageConverter = languageConverter;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void populate(final BaseSiteModel source, final BaseSiteData target) {
        target.setUid(source.getUid());
        target.setName(source.getName());
        target.setDefaultLanguage(source.getDefaultLanguage() == null ? null : languageConverter.convert(source.getDefaultLanguage()));
        target.setLocale(source.getLocale());
        target.setChannel(source.getChannel() == null ? null : source.getChannel().getCode());
    }
}
