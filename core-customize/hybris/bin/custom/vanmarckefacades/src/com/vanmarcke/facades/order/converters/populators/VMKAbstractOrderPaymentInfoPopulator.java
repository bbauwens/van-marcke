package com.vanmarcke.facades.order.converters.populators;

import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

/**
 * Populates payment info to {@link AbstractOrderData}.
 *
 * @author Taki Korovessis
 * @since 20-1-2020
 */
public class VMKAbstractOrderPaymentInfoPopulator<SOURCE extends AbstractOrderModel, TARGET extends AbstractOrderData> implements Populator<SOURCE, TARGET> {

    private final Converter<AddressModel, AddressData> addressConverter;

    /**
     * Provides an instance of the VMKAbstractOrderPaymentInfoPopulator.
     *
     * @param addressConverter the address converter
     */
    public VMKAbstractOrderPaymentInfoPopulator(Converter<AddressModel, AddressData> addressConverter) {
        this.addressConverter = addressConverter;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void populate(SOURCE source, TARGET target) {
        PaymentInfoModel paymentInfo = source.getPaymentInfo();
        if (paymentInfo != null && !(paymentInfo instanceof CreditCardPaymentInfoModel)) {
            CCPaymentInfoData ccPaymentInfoData = new CCPaymentInfoData();
            ccPaymentInfoData.setId(paymentInfo.getPk().toString());
            ccPaymentInfoData.setBillingAddress(addressConverter.convert(paymentInfo.getBillingAddress()));
            target.setPaymentInfo(ccPaymentInfoData);
        }
    }
}