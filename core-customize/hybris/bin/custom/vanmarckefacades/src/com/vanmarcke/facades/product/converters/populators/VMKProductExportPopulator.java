package com.vanmarcke.facades.product.converters.populators;

import com.vanmarcke.core.model.BrandCategoryModel;
import com.vanmarcke.core.model.CommerceGroupModel;
import com.vanmarcke.core.model.VanMarckeVariantProductModel;
import com.vanmarcke.services.data.ProductExportItem;
import de.hybris.platform.acceleratorservices.urlresolver.SiteBaseUrlResolutionService;
import de.hybris.platform.catalog.model.ProductReferenceModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.time.FastDateFormat;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

import static com.vanmarcke.services.util.ProductExportUtil.trimHTMLTags;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;
import static org.apache.commons.lang3.BooleanUtils.toBoolean;
import static org.apache.commons.lang3.StringUtils.defaultString;

/**
 * Export Product populator which populates all export fields.
 */
public class VMKProductExportPopulator implements Populator<VanMarckeVariantProductModel, ProductExportItem> {

    private static final String FORMAT_EXTRA_SMALL = "EXTRA_SMALL";
    private static final String FORMAT_LARGE = "LARGE";
    private static final String FORMAT_EXTRA_LARGE = "EXTRA_LARGE";
    private static final String FORMAT_SCREEN = "SCREEN";
    private static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    private static final String DELIMITER = ",";
    private static final String FIELD_VALUES_DELIMITER = "-";

    private CommonI18NService commonI18NService;
    private PriceDataFactory priceDataFactory;
    private SiteBaseUrlResolutionService siteBaseUrlResolutionService;
    private CMSSiteService cmsSiteService;

    public VMKProductExportPopulator(CommonI18NService commonI18NService, PriceDataFactory priceDataFactory, SiteBaseUrlResolutionService siteBaseUrlResolutionService, CMSSiteService cmsSiteService) {
        this.commonI18NService = commonI18NService;
        this.priceDataFactory = priceDataFactory;
        this.siteBaseUrlResolutionService = siteBaseUrlResolutionService;
        this.cmsSiteService = cmsSiteService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void populate(VanMarckeVariantProductModel product, ProductExportItem productExportItem) {

        this.commonI18NService.getAllLanguages()
                .stream()
                .filter(languageModel -> languageModel.getActive())
                .forEach(language -> {
                    Locale locale = commonI18NService.getLocaleForLanguage(language);
                    productExportItem.getIdentifier().put(locale.toString(), defaultString(product.getName(locale)));
                    productExportItem.getDescription().put(locale.toString(), trimHTMLTags(defaultString(product.getDescription(locale))));
                    productExportItem.getSummary().put(locale.toString(), defaultString(product.getSummary(locale)));
                    productExportItem.getName().put(locale.toString(), defaultString(product.getShortDescription(locale)));
                    productExportItem.getTechnicalDataSheet().put(locale.toString(), defaultString(product.getTech_data_sheet(locale) != null ? product.getTech_data_sheet(locale).getURL() : null));
                    productExportItem.getEcoDataSheet().put(locale.toString(), defaultString(product.getEco_data_sheet(locale) != null ? product.getEco_data_sheet(locale).getURL() : null));
                    productExportItem.getSafetyDataSheet().put(locale.toString(), defaultString(product.getSafety_data_sheet(locale) != null ? product.getSafety_data_sheet(locale).getURL() : null));
                    productExportItem.getNormalisation().put(locale.toString(), defaultString(product.getNormalisation(locale) != null ? product.getNormalisation(locale).getURL() : null));
                    productExportItem.getProductSpecificationSheet().put(locale.toString(), defaultString(product.getProduct_specification_sheet(locale) != null ? product.getProduct_specification_sheet(locale).getURL() : null));
                    productExportItem.getWarranty().put(locale.toString(), defaultString(product.getWarranty(locale) != null ? product.getWarranty(locale).getURL() : null));
                    productExportItem.getDop().put(locale.toString(), defaultString(product.getDOP(locale) != null ? product.getDOP(locale).getURL() : null));
                    productExportItem.getSparePartsList().put(locale.toString(), defaultString(product.getSpare_parts_list(locale) != null ? product.getSpare_parts_list(locale).getURL() : null));

                    productExportItem.getCertification().put(locale.toString(), defaultString(product.getCertificates(locale) != null ? getUrls(product.getCertificates(locale)) : null));
                    productExportItem.getInstructionManual().put(locale.toString(), defaultString(product.getInstructionManuals(locale) != null ? getUrls(product.getInstructionManuals(locale)) : null));
                    productExportItem.getMaintenanceManual().put(locale.toString(), defaultString(product.getMaintenanceManuals(locale) != null ? getUrls(product.getMaintenanceManuals(locale)) : null));
                    productExportItem.getUserManual().put(locale.toString(), defaultString(product.getUserManuals(locale) != null ? getUrls(product.getUserManuals(locale)) : null));
                    productExportItem.getGeneralManual().put(locale.toString(), defaultString(product.getGeneralManuals(locale) != null ? getUrls(product.getGeneralManuals(locale)) : null));
                    productExportItem.getBrandCategoryName().put(locale.toString(), getBrandCategory(locale, product));
                    if (product.getBaseProduct() != null) {
                        productExportItem.getBaseProductSuperCategoryName().put(locale.toString(), defaultString(getBaseProductCategory(locale, product)));
                    }
                });

        productExportItem.setArticleNumber(product.getCode());

        if (product.getApprovalStatus() != null) {
            productExportItem.setApproval(product.getApprovalStatus().getCode());
        }

        if (product.getBaseProduct() != null) {
            productExportItem.setBaseProduct(product.getBaseProduct().getCode());
        }

        String productReferences = product.getProductReferences().stream()
                .map(ProductReferenceModel::getTarget)
                .map(ProductModel::getCode)
                .collect(Collectors.joining(DELIMITER));
        productExportItem.setProductReferences(productReferences);

        String categories = product.getSupercategories().stream()
                .map(CategoryModel::getCode)
                .collect(Collectors.joining(DELIMITER));
        productExportItem.setCategories(categories);

        productExportItem.setCalculatedStatus(product.getCalculatedStatus());
        productExportItem.setDeliveryMethod(product.getDeliveryMethod());
        productExportItem.setSparePart(product.getSparePart());

        if (product.getVanmarckeCountryChannel() != null) {
            populateCountryChannels(product, productExportItem);
        }
        if (product.getEurope1Prices() != null) {
            populatePrices(product, productExportItem);
        }
        populateProductUrls(product, productExportItem);

        productExportItem.setThumbnailUrl(defaultString(product.getThumbnail() != null ? product.getThumbnail().getURL() : null));
        productExportItem.setManufacturer(defaultString(product.getManufacturerName()));
        productExportItem.setVmArticleNumber_old(defaultString(product.getVmArticleNumber_old()));
        productExportItem.setManufacturerArticleNumber_old(defaultString(product.getVendorItemNumber_old()));
        productExportItem.setManufacturerArticleBarcode_old(defaultString(product.getVendorItemBarcode_old()));
        productExportItem.setVendorItemNumber(defaultString(product.getVendorItemNumber_current()));
        productExportItem.setVendorItemBarcode(defaultString(product.getVendorItemBarcode_current()));
        productExportItem.setVmItemBarcode(defaultString(product.getVanmarckeItemBarcode()));
        productExportItem.setPackaging(defaultString(product.getPackaging() != null ? product.getPackaging().toString() : null));
        productExportItem.setContentPackaging(defaultString(product.getContentPackaging()));
        productExportItem.setVmWarrantyPeriod(defaultString(product.getVmWarrantyTerm()));
        productExportItem.setEnergyClass(defaultString(product.getEnergyClass()));
        productExportItem.setOwner(defaultString(product.getOwner_name()));
        if (product.getVmBranch() != null) {
            productExportItem.setVmBranch(product.getVmBranch().getCode());
        }
        productExportItem.setTechnicalSpecification(defaultString(product.getTechnicalSpecification()));
        productExportItem.setBrandType(defaultString(product.getBrandType()));
        productExportItem.setBrandSeries(defaultString(product.getBrandSeries()));
        productExportItem.setCatalog_VanMarcke_B2B(toBoolean(product.getCatalog_VanMarcke_B2B()));
        productExportItem.setCatalog_VanMarcke_B2C(toBoolean(product.getCatalog_VanMarcke_B2C()));
        productExportItem.setUsp(product.getUsp());
        productExportItem.setCatalog_DIY(toBoolean(product.getCatalog_DIY()));
        productExportItem.setCreationTime(FastDateFormat.getInstance(DEFAULT_DATE_FORMAT).format(product.getCreationtime()));
        productExportItem.setModifiedTime(FastDateFormat.getInstance(DEFAULT_DATE_FORMAT).format(product.getModifiedtime()));

        productExportItem.setBrandCategory(product.getSupercategories().stream()
                .filter(cat -> cat instanceof BrandCategoryModel)
                .map(CategoryModel::getCode)
                .findFirst().orElse(""));

        if (product.getBaseProduct() != null) {
            productExportItem.setBaseProductSuperCategory(product.getBaseProduct()
                    .getSupercategories()
                    .stream()
                    .map(CategoryModel::getCode)
                    .collect(Collectors.joining(DELIMITER)));
        }

        populateImages(product, productExportItem);
        populateTechnicalDrawing(product, productExportItem);
        populateEcoLabel(product, productExportItem);
        populateIcon(product, productExportItem);
        populateProductLogo(product, productExportItem);
    }

    /**
     * Populate country channels as list of country isocode - channel code, e.g. BE-B2B, BE-DIY, FR-B2B
     * @param product the variant
     * @param productExportItem the product export item
     */
    private void populateCountryChannels(VanMarckeVariantProductModel product, ProductExportItem productExportItem) {
        String countryChannels = product.getVanmarckeCountryChannel()
                .stream()
                .flatMap(channel -> channel.getChannels()
                        .stream()
                        .map(siteChannel -> String.join(FIELD_VALUES_DELIMITER, channel.getCountry().getIsocode(), siteChannel.getCode()))
                ).collect(Collectors.joining(DELIMITER));
        productExportItem.setCountryChannels(countryChannels);
    }

    /**
     * Populates the product prices, as a map with the country isocode as key and formmatted price as value
     *
     * @param product the variant
     * @param productExportItem the product export item
     */
    private void populatePrices(VanMarckeVariantProductModel product, ProductExportItem productExportItem) {
        Map<String, String> formattedPrices = product.getEurope1Prices().stream()
                .collect(Collectors.toMap(price -> price.getUg().getCode(),
                        price -> priceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(price.getPrice()), price.getCurrency()).getFormattedValue()));

        productExportItem.setPrices(formattedPrices);
    }

    /**
     * Populate URLs for all available stores for the product
     *
     * @param product the source product variant
     * @param productExportItem the product export item
     */
    private void populateProductUrls(VanMarckeVariantProductModel product, ProductExportItem productExportItem) {
        cmsSiteService.getSites().forEach(site -> {
            CommerceGroupModel commerceGroup = site.getCommerceGroup();
            Boolean productMatch = false;
            if (commerceGroup != null) {
                String siteCountry = commerceGroup.getCountry().getIsocode();
                productMatch = product.getVanmarckeCountryChannel()
                        .stream()
                        .anyMatch(channel -> channel.getCountry().getIsocode().equals(siteCountry));
            }

            if (productMatch) {
                site.getStores().forEach(store -> {
                    store.getLanguages().forEach(lang -> {
                        StringBuilder urlBuilder = new StringBuilder(siteBaseUrlResolutionService.getWebsiteUrlForSite(site, true, "/"));
                        urlBuilder.append(lang.getIsocode());
                        urlBuilder.append("/p/");
                        urlBuilder.append(product.getCode());
                        productExportItem.getUrls().put(lang.getIsocode(), urlBuilder.toString());
                    });
                });

            }
        });
    }

    /**
     * get product brand category; only one category expected
     * @param loc the locale
     * @param product
     * @return product brand category as identifier - name values
     */
    private String getBrandCategory(Locale loc, VanMarckeVariantProductModel product) {
        return product.getSupercategories().stream()
                .filter(cat -> cat instanceof BrandCategoryModel)
                .map(cat -> defaultString(cat.getName(loc)))
                .findFirst().orElse("");
    }

    /**
     * get Base product super categories
     * @param loc the locale
     * @param product
     * @return Base product categories as comma separated list of identifier - name values
     */
    private String getBaseProductCategory(Locale loc, VanMarckeVariantProductModel product) {
        return product.getBaseProduct().getSupercategories().stream()
                .map(cat ->  defaultString(cat.getName(loc)))
                .collect(Collectors.joining(DELIMITER));
    }

    /**
     * Extracts the download urls from the given Media collection.
     *
     * @param medias a collection of medias
     * @return a string of media model urls
     */
    protected String getUrls(Collection<MediaModel> medias) {
        List<String> urls = medias.stream()
                .map(MediaModel::getURL)
                .collect(Collectors.toList());

        return StringUtils.join(urls, DELIMITER);
    }

    /**
     * Populates the product logo.
     *
     * @param source the variant
     * @param target the product export item
     */
    protected void populateProductLogo(VanMarckeVariantProductModel source, ProductExportItem target) {
        if (isNotEmpty(source.getProductLogo()) && isNotEmpty(source.getProductLogo().get(0).getMedias())) {
            Collection<MediaModel> mediaModelCollection = source.getProductLogo().get(0).getMedias();
            target.setSmallProductLogoImage(getImageUrlForImageFormat(mediaModelCollection, FORMAT_EXTRA_SMALL));
            target.setNormalProductLogoImage(getImageUrlForImageFormat(mediaModelCollection, FORMAT_LARGE));
            target.setLargeProductLogoImage(getImageUrlForImageFormat(mediaModelCollection, FORMAT_LARGE));
            target.setExtraLargeProductLogoImage(getImageUrlForImageFormat(mediaModelCollection, FORMAT_EXTRA_LARGE));
            target.setScreenProductLogoImage(getImageUrlForImageFormat(mediaModelCollection, FORMAT_SCREEN));
        }
    }

    /**
     * Populates the icon.
     *
     * @param source the variant
     * @param target the product export item
     */
    protected void populateIcon(VanMarckeVariantProductModel source, ProductExportItem target) {
        if (isNotEmpty(source.getIcon()) && isNotEmpty(source.getIcon().get(0).getMedias())) {
            Collection<MediaModel> mediaModelCollection = source.getIcon().get(0).getMedias();
            target.setSmallIconImage(getImageUrlForImageFormat(mediaModelCollection, FORMAT_EXTRA_SMALL));
            target.setNormalIconImage(getImageUrlForImageFormat(mediaModelCollection, FORMAT_LARGE));
            target.setLargeIconImage(getImageUrlForImageFormat(mediaModelCollection, FORMAT_LARGE));
            target.setExtraLargeIconImage(getImageUrlForImageFormat(mediaModelCollection, FORMAT_EXTRA_LARGE));
            target.setScreenIconImage(getImageUrlForImageFormat(mediaModelCollection, FORMAT_SCREEN));
        }
    }

    /**
     * Populates the eco label.
     *
     * @param source the variant
     * @param target the product export item
     */
    protected void populateEcoLabel(VanMarckeVariantProductModel source, ProductExportItem target) {
        if (isNotEmpty(source.getEco_label()) && isNotEmpty(source.getEco_label().get(0).getMedias())) {
            Collection<MediaModel> mediaModelCollection = source.getEco_label().get(0).getMedias();
            target.setSmallEcoLabelImage(getImageUrlForImageFormat(mediaModelCollection, FORMAT_EXTRA_SMALL));
            target.setNormalEcoLabelImage(getImageUrlForImageFormat(mediaModelCollection, FORMAT_SCREEN));
            target.setLargeEcoLabelImage(getImageUrlForImageFormat(mediaModelCollection, FORMAT_LARGE));
            target.setExtraLargeEcoLabelImage(getImageUrlForImageFormat(mediaModelCollection, FORMAT_EXTRA_LARGE));
            target.setScreenEcoLabelImage(getImageUrlForImageFormat(mediaModelCollection, FORMAT_SCREEN));
        }
    }

    /**
     * Populates the technical drawing.
     *
     * @param source the variant
     * @param target the product export item
     */
    protected void populateTechnicalDrawing(VanMarckeVariantProductModel source, ProductExportItem target) {
        if (isNotEmpty(source.getTech_drawing()) && isNotEmpty(source.getTech_drawing().get(0).getMedias())) {
            Collection<MediaModel> mediaModelCollection = source.getTech_drawing().get(0).getMedias();
            target.setSmallTechnicalDrawingImage(getImageUrlForImageFormat(mediaModelCollection, FORMAT_EXTRA_SMALL));
            target.setNormalTechnicalDrawingImage(getImageUrlForImageFormat(mediaModelCollection, FORMAT_SCREEN));
            target.setLargeTechnicalDrawingImage(getImageUrlForImageFormat(mediaModelCollection, FORMAT_LARGE));
            target.setExtraLargeTechnicalDrawingImage(getImageUrlForImageFormat(mediaModelCollection, FORMAT_EXTRA_LARGE));
            target.setScreenTechnicalDrawingImage(getImageUrlForImageFormat(mediaModelCollection, FORMAT_SCREEN));
        }
    }

    /**
     * Populates the images.
     *
     * @param source the variant
     * @param target the product export item
     */
    protected void populateImages(VanMarckeVariantProductModel source, ProductExportItem target) {
        if (isNotEmpty(source.getGalleryImages()) && isNotEmpty(source.getGalleryImages().get(0).getMedias())) {
            Collection<MediaModel> mediaModelCollection = source.getGalleryImages().get(0).getMedias();
            target.setSmallImage(getImageUrlForImageFormat(mediaModelCollection, FORMAT_EXTRA_SMALL));
            target.setNormalImage(getImageUrlForImageFormat(mediaModelCollection, FORMAT_LARGE));
            target.setLargeImage(getImageUrlForImageFormat(mediaModelCollection, FORMAT_LARGE));
            target.setExtraLargeImage(getImageUrlForImageFormat(mediaModelCollection, FORMAT_EXTRA_LARGE));
            target.setScreenImage(getImageUrlForImageFormat(mediaModelCollection, FORMAT_SCREEN));
        }
    }

    /**
     * Retrieves the image urls for the given image format
     *
     * @param images      the images
     * @param imageFormat the image format
     */
    private String getImageUrlForImageFormat(Collection<MediaModel> images, String imageFormat) {
        return images.stream()
                .filter(e -> Objects.nonNull(e.getMediaFormat()))
                .filter(e -> StringUtils.equals(e.getMediaFormat().getQualifier(), imageFormat))
                .map(MediaModel::getDownloadURL)
                .findFirst()
                .orElse(null);
    }
}

