package com.vanmarcke.facades.product.impl;

import com.vanmarcke.core.model.BrandCategoryModel;
import com.vanmarcke.facades.product.VMKProductFacade;
import com.vanmarcke.facades.product.data.PriceListData;
import com.vanmarcke.services.category.VMKCategoryService;
import com.vanmarcke.services.product.price.VMKNetPriceLookupStrategy;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.*;
import de.hybris.platform.commercefacades.product.impl.DefaultProductFacade;
import de.hybris.platform.commerceservices.category.CommerceCategoryService;
import de.hybris.platform.commerceservices.price.CommercePriceService;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.variants.model.VariantProductModel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Implementation of com.vanmarcke.blue.facades.product.BlueProductFacade.
 * Extends de.hybris.platform.commercefacades.product.impl.DefaultProductFacade, but does not override the bean.
 *
 * @author marteto
 */
public class VMKProductFacadeImpl<REF_TARGET> extends DefaultProductFacade<REF_TARGET> implements VMKProductFacade {

    private static final String MEDIA_FORMAT = "SMALL";

    private final VMKNetPriceLookupStrategy blueNetPriceLookupStrategy;
    private final PriceDataFactory priceDataFactory;
    private final CommercePriceService commercePriceService;
    private Converter<MediaModel, ImageData> imageConverter;
    private VMKCategoryService categoryService;
    private CommerceCategoryService commerceCategoryService;
    private Converter<VariantProductModel, VariantOptionData> variantOptionDataConverter;
    private Converter<VariantProductModel, VariantOptionData> productListVariantOptionDataConverter;
    private Populator<ProductData, VariantOptionData> vmkSolrProductDataVariantPopulator;

    /**
     * Constructor
     * @param blueNetPriceLookupStrategy The blueNetPriceLookupStrategy
     * @param priceDataFactory           The priceDataFactory
     * @param commercePriceService       The commercePriceService
     * @param imageConverter             The imageConverter
     * @param categoryService            The categoryService
     * @param commerceCategoryService    The commerceCategoryService
     * @param variantOptionDataConverter Variant converter for populating variant data on the PDP
     * @param productListVariantOptionDataConverter Variant converter for populating variant data on the PLP - product list view
     * @param vmkSolrProductDataVariantPopulator
     */
    public VMKProductFacadeImpl(VMKNetPriceLookupStrategy blueNetPriceLookupStrategy,
                                PriceDataFactory priceDataFactory,
                                CommercePriceService commercePriceService,
                                Converter<MediaModel, ImageData> imageConverter,
                                VMKCategoryService categoryService,
                                CommerceCategoryService commerceCategoryService,
                                Converter<VariantProductModel, VariantOptionData> variantOptionDataConverter,
                                Converter<VariantProductModel, VariantOptionData> productListVariantOptionDataConverter,
                                Populator<ProductData, VariantOptionData> vmkSolrProductDataVariantPopulator) {
        this.blueNetPriceLookupStrategy = blueNetPriceLookupStrategy;
        this.priceDataFactory = priceDataFactory;
        this.commercePriceService = commercePriceService;
        this.imageConverter = imageConverter;
        this.categoryService = categoryService;
        this.commerceCategoryService = commerceCategoryService;
        this.variantOptionDataConverter = variantOptionDataConverter;
        this.productListVariantOptionDataConverter = productListVariantOptionDataConverter;
        this.vmkSolrProductDataVariantPopulator = vmkSolrProductDataVariantPopulator;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PriceListData getPricesForProduct(String productCode) {
        PriceListData priceListData = createEmptyPriceListData();
        ProductModel product = getProductService().getProductForCode(productCode);

        Optional<PriceData> listPriceData = getListPriceData(product);
        listPriceData.ifPresent(listPrice -> priceListData.getPrices().add(listPrice));

        Optional<PriceData> netPriceData = getNetPriceData(product);
        netPriceData.ifPresent(netPrice -> priceListData.getPrices().add(netPrice));

        return priceListData;
    }

    /**
     * {@inheritDoc}
     */
    public void enrichProductDataWithSolrVariants(ProductData productData, List<ProductData> solrVariantData, boolean isListView) {
        BaseOptionData baseVariant;
        if (isListView) {
            baseVariant = getEnrichedVariantData(productData, solrVariantData, productListVariantOptionDataConverter);
        } else {
            baseVariant = getEnrichedVariantData(productData, solrVariantData, variantOptionDataConverter);

        }
        productData.setBaseOptions(List.of(baseVariant));
    }

    /**
     * {@inheritDoc}
     */
    public void enrichProductDataWithBrandLogo(ProductData product) {
        if (product.getBrand() != null) {
            try {
                BrandCategoryModel brandCategory = (BrandCategoryModel) commerceCategoryService.getCategoryForCode(product.getBrand().getCode());
                ImageData brandLogoData = null;
                final Optional<MediaModel> brandLogoModel = categoryService.getLocalizedBrandLogoForFormat(brandCategory, MEDIA_FORMAT);
                if (brandLogoModel.isPresent()) {
                    brandLogoData = imageConverter.convert(brandLogoModel.get());
                }

                product.getBrand().setLogo(brandLogoData);
            } catch (UnknownIdentifierException e) {
                // nothing to do
            }
        }
    }

    /**
     * Enrich solr returned product variants using a given converter
     *
     * @param productData base product data to be enriched with option (variant) data
     * @param solrVariantData variant data returned from solr containing basic information
     * @param variantConverter converter to be used for enriching solr variants
     *
     * @return Base option data containing enriched variant data
     */
    protected BaseOptionData getEnrichedVariantData(ProductData productData, List<ProductData> solrVariantData, Converter<VariantProductModel, VariantOptionData> variantConverter) {
        final List<VariantOptionData> variantOptions = new ArrayList<>();
        BaseOptionData baseVariant = new BaseOptionData();

        for (ProductData variant : solrVariantData) {
            try {
                ProductModel productVariant = getProductService().getProductForCode(variant.getCode());
                VariantOptionData convertedVariant = variantConverter.convert((VariantProductModel) productVariant);
                vmkSolrProductDataVariantPopulator.populate(variant, convertedVariant);
                variantOptions.add(convertedVariant);
                if (convertedVariant.getCode().equals(productData.getCode())) {
                    baseVariant.setSelected(convertedVariant);
                }
            } catch (UnknownIdentifierException e) {
                // do nothing
            }
        }

        productData.setVariantOptions(variantOptions);
        baseVariant.setOptions(variantOptions);

        return baseVariant;
    }


    /**
     * Create a new PriceListData and initialize the prices property with an empty ArrayList
     *
     * @return PriceListData
     */
    protected PriceListData createEmptyPriceListData() {
        PriceListData priceListData = new PriceListData();
        priceListData.setPrices(new ArrayList<>());
        return priceListData;
    }

    /**
     * Find the NET product price.
     * Optional since the NET price is only available for logged in users.
     *
     * @param product The product
     * @return An optional of PriceData containing the NET price
     */
    protected Optional<PriceData> getNetPriceData(ProductModel product) {
        Optional<PriceData> optionalPriceData = Optional.empty();
        BigDecimal bigDecimal = blueNetPriceLookupStrategy.getNetPriceForProduct(product);
        if (bigDecimal != null) {
            optionalPriceData = Optional.of(priceDataFactory.create(PriceDataType.NET, bigDecimal, getCommonI18NService().getCurrentCurrency()));
        }
        return optionalPriceData;
    }

    /**
     * Find the LIST price for the given product
     *
     * @param product The product
     * @return An optional of PriceData containing the LIST price
     */
    protected Optional<PriceData> getListPriceData(ProductModel product) {
        Optional<PriceData> optionalPriceData = Optional.empty();
        PriceInformation priceInformation = commercePriceService.getFromPriceForProduct(product);
        if (priceInformation != null) {
            optionalPriceData = Optional.of(priceDataFactory.create(PriceDataType.LIST, BigDecimal.valueOf(priceInformation.getPriceValue().getValue()), priceInformation.getPriceValue().getCurrencyIso()));
        }
        return optionalPriceData;
    }
}
