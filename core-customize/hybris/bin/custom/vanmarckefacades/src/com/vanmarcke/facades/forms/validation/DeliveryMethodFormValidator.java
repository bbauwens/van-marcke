package com.vanmarcke.facades.forms.validation;

import com.vanmarcke.core.enums.ReferenceConditionalType;
import com.vanmarcke.core.exception.NoSessionCartException;
import com.vanmarcke.services.b2b.VMKB2BUnitService;
import com.vanmarcke.services.delivery.VMKDeliveryMethodService;
import com.vanmarcke.services.delivery.VMKPickupInfoService;
import com.vanmarcke.services.delivery.VMKShippingInfoService;
import com.vanmarcke.services.forms.delivery.DeliveryMethodForm;
import com.vanmarcke.services.storelocator.pos.VMKPointOfServiceService;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.services.BaseStoreService;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Map;
import java.util.Optional;

import static com.vanmarcke.core.constants.VanmarckeCoreConstants.DeliveryMethods.PICKUP;
import static com.vanmarcke.core.constants.VanmarckeCoreConstants.DeliveryMethods.SHIPPING;

/**
 * The {@link DeliveryMethodFormValidator} class is used to validate {@link DeliveryMethodForm} instances.
 *
 * @author Niels Raemaekers, Tom van den Berg, Christiaan Janssen
 * @since 18-08-2020
 */
public class DeliveryMethodFormValidator implements Validator {

    private static final Logger LOG = Logger.getLogger(DeliveryMethodFormValidator.class);

    private final VMKPointOfServiceService pointOfServiceService;
    private final VMKPickupInfoService pickupInfoService;
    private final VMKShippingInfoService shippingInfoService;
    private final VMKDeliveryMethodService deliveryMethodService;
    private final VMKB2BUnitService b2BUnitService;
    private final CartService cartService;
    private final BaseStoreService baseStoreService;
    private final BaseSiteService baseSiteService;

    /**
     * Creates a new instance of the {@link DeliveryMethodFormValidator} class.
     *
     * @param pointOfServiceService the point of service service
     * @param pickupInfoService     the pickup info service
     * @param shippingInfoService   the shipping info service
     * @param deliveryMethodService the delivery method service
     * @param b2BUnitService        the B2B unit service
     * @param cartService           the cart service
     */
    public DeliveryMethodFormValidator(VMKPointOfServiceService pointOfServiceService,
                                       VMKPickupInfoService pickupInfoService,
                                       VMKShippingInfoService shippingInfoService,
                                       VMKDeliveryMethodService deliveryMethodService,
                                       VMKB2BUnitService b2BUnitService,
                                       CartService cartService,
                                       BaseStoreService baseStoreService,
                                       BaseSiteService baseSiteService) {
        this.pointOfServiceService = pointOfServiceService;
        this.pickupInfoService = pickupInfoService;
        this.shippingInfoService = shippingInfoService;
        this.deliveryMethodService = deliveryMethodService;
        this.b2BUnitService = b2BUnitService;
        this.cartService = cartService;
        this.baseStoreService = baseStoreService;
        this.baseSiteService = baseSiteService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean supports(Class<?> aClass) {
        return DeliveryMethodForm.class.equals(aClass);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void validate(Object object, Errors errors) {
        DeliveryMethodForm deliveryMethodForm = (DeliveryMethodForm) object;

        validateDeliveryOption(errors, deliveryMethodForm.getDeliveryOption());
        validateDeliveryMethod(errors, deliveryMethodForm.getDeliveryMethod());

        if (!baseSiteService.getCurrentBaseSite().getChannel().equals(SiteChannel.DIY)) {
            validateDeliveryDate(
                    errors,
                    deliveryMethodForm.getActualDate(),
                    deliveryMethodForm.getDeliveryMethod(),
                    deliveryMethodForm.getTecUid());
        }

        validateDeliveryComment(errors, deliveryMethodForm.getDeliveryComment());
        validateYardReference(errors, b2BUnitService.getB2BUnitModelForCurrentUser(), deliveryMethodForm.getYardReference());

        if (BooleanUtils.isTrue(b2BUnitService.mandatorySeparateInvoiceReference())) {
            validateMandatorySeparateInvoiceReference(errors, deliveryMethodForm.getRequestSeparateInvoice(), deliveryMethodForm.getPurchaseOrderNumber());
        }
    }

    private void validateYardReference(Errors errors, B2BUnitModel b2BUnit, String yardReference) {
        if (ReferenceConditionalType.MANDATORY.equals(b2BUnit.getRef2()) && StringUtils.isBlank(yardReference)) {
            errors.rejectValue("yardReference", "cart.validation-error.reference-empty");
            LOG.error("Error while validating yard reference: required but was empty");
        }
    }

    private void validateMandatorySeparateInvoiceReference(Errors errors, Boolean requestSeparateInvoice, String poNumber) {
        if (BooleanUtils.isFalse(requestSeparateInvoice) && (StringUtils.isBlank(poNumber) || poNumber.length() > 6)) {
            errors.rejectValue("mandatorySeparateInvoiceReference", "cart.validation-error.mandatory-separate-invoice-reference");
            LOG.error("Error while validating request separate invoice: customer has mandatory separate invoice but passed boolean was false and/or poNumber was empty");
        }
    }

    /**
     * Validate the length of the delivery comment.
     *
     * @param errors          the errors
     * @param deliveryComment the delivery comment
     */
    private void validateDeliveryComment(Errors errors, String deliveryComment) {
        if (deliveryComment != null && deliveryComment.length() > 255) {
            errors.rejectValue("deliveryComment", "cart.validation-error.delivery-comment-length");
            LOG.error("Error while validating delivery comment: too many characters");
        }
    }

    /**
     * Validates the given {@code deliveryOption}.
     *
     * @param errors         the errors
     * @param deliveryOption the delivery option
     */
    private void validateDeliveryOption(Errors errors, String deliveryOption) {
        if (StringUtils.isBlank(deliveryOption)) {
            errors.rejectValue("deliveryOption", "delivery.form.option.none.selected");
            LOG.error("Error while validating delivery option: empty delivery option");
        }
    }

    /**
     * Validates the given {@code deliveryMethod}.
     *
     * @param errors         the errors
     * @param deliveryMethod the delivery method
     */
    private void validateDeliveryMethod(Errors errors, String deliveryMethod) {
        boolean isValid;

        Map<String, Boolean> availableDeliveryMethods = deliveryMethodService.getSupportedDeliveryMethods(getCart());

        isValid = availableDeliveryMethods.get(deliveryMethod);

        if (!isValid) {
            errors.rejectValue("deliveryMethod", "delivery.form.method.invalid");
            LOG.error("Error while validating delivery method: empty delivery method");
        }
    }

    /**
     * Checks if the selected POS belongs to the current BaseStore
     *
     * @param pos point of service to be checked
     * @return {@code true} in case the POS belongs to the current BaseStore, {@code false} otherwise
     */
    private boolean isInvalidPos(PointOfServiceModel pos) {
        return pos == null || !baseStoreService.getCurrentBaseStore().getPointsOfService().contains(pos);
    }

    /**
     * Validates the requested {@code deliveryDate}.
     *
     * @param errors                the errors
     * @param requestedDeliveryDate the requested delivery date
     * @param deliveryMethod        the delivery method
     * @param tecUid                the TEC uid
     */
    private void validateDeliveryDate(final Errors errors, final String requestedDeliveryDate, final String deliveryMethod, final String tecUid) {
        Optional<Date> firstPossibleDate = Optional.empty();
        PointOfServiceModel pos = null;
        if (PICKUP.equals(deliveryMethod)) {
            firstPossibleDate = pickupInfoService.getFirstPossiblePickupDate(getCart());

            if (firstPossibleDate.isEmpty() && pickupInfoService.isTecCollectPossible(getCart())) {
                firstPossibleDate = Optional.of(new Date());
            }
            pos = pointOfServiceService.getPointOfServiceForName(tecUid);
        } else if (SHIPPING.equals(deliveryMethod)) {
            firstPossibleDate = shippingInfoService.getFirstPossibleShippingDate(getCart());
            pos = pointOfServiceService.getCurrentPointOfService();
        }

        if (firstPossibleDate.isEmpty()
                || isInvalidPos(pos)
                || requestedDeliveryDate == null
                || isInvalidDate(requestedDeliveryDate, firstPossibleDate.get())) {
            errors.rejectValue(null, "delivery.form.date.invalid");
            LOG.error(String.format("Error while validating delivery date: invalid date, POS or opening hours. DeliveryDate: %s, firstPossibleDate: %s", requestedDeliveryDate,
                    firstPossibleDate
                            .map(Date::toString)
                            .orElse("no firstPossibleDate")));
        }
    }

    /**
     * Checks whether the current date is equal to or larger than the first possible date.
     *
     * @param deliveryDate      the delivery date
     * @param firstPossibleDate the first possible date
     * @return {@code true} in case the current date is equal to or larger than the first possible date, {@code false}
     * otherwise
     */
    protected boolean isInvalidDate(final String deliveryDate, final Date firstPossibleDate) {
        if (firstPossibleDate == null) {
            return true;
        }

        LocalDate firstPossible = firstPossibleDate.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDate();

        LocalDate actualDate = LocalDate.parse(deliveryDate, DateTimeFormatter.ofPattern("yyyy-MM-dd"));

        // valid if TEC collect day
        if (actualDate.isEqual(getCurrentDay()) && pickupInfoService.isTecCollectPossible(getCart())) {
            return false;
        }

        return actualDate.isBefore(firstPossible);
    }

    /**
     * Get the current cart
     *
     * @return the current cart
     */
    protected CartModel getCart() {
        CartModel cart = cartService.getSessionCart();
        if (cart == null) {
            LOG.error("No session cart was found.");
            throw new NoSessionCartException("No session cart was found.");
        }
        return cart;
    }

    /**
     * Retrieve a {@link LocalDate} instance for the current day.
     *
     * @return the LocalDate
     */
    protected LocalDate getCurrentDay() {
        // added for testing purposes
        return LocalDate.now();
    }
}
