package com.vanmarcke.facades.address.converters.populators;

import com.vanmarcke.cpi.data.ptv.PTVAddressData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

/**
 * Populator for the PTV xServer Address Validation object
 *
 * @author David Vangeneugden, Taki Korovessis, Christiaan Janssen
 * @since 14-06-2019
 */
public class VMKPTVAddressPopulator implements Populator<AddressData, PTVAddressData> {

    /**
     * {@inheritDoc}
     */
    @Override
    public void populate(AddressData source, PTVAddressData target) throws ConversionException {
        target.setCountry(source.getCountry().getIsocode());
        target.setCity(source.getTown());
        target.setPostCode(source.getPostalCode());
        target.setStreet(source.getLine1());
        target.setHouseNumber(source.getLine2());
    }
}
