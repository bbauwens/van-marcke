package com.vanmarcke.facades.cart.converters.populators;

import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.CartModel;

/**
 * Populates summary data on cart data.
 *
 * @author Tom van den Berg
 * @since 24-08-2020
 */
public class VMKCartSummaryPopulator implements Populator<CartModel, CartData> {

    /**
     * {@inheritDoc}
     */
    @Override
    public void populate(CartModel source, CartData target) {
        target.setCode(source.getCode());
        target.setDescription(source.getDescription());
        target.setName(source.getName());
        target.setSaveTime(source.getSaveTime());
        target.setTotalUnitCount(source.getEntries().size());
    }
}
