package com.vanmarcke.facades.principal.b2bunit.converters.populators;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

/**
 * Populator that adds the boolean to show if the address is the default one or not
 */
public class VMKB2BAddressDefaultShippingAddressPopulator implements Populator<AddressModel, AddressData> {

    @Override
    public void populate(final AddressModel source, final AddressData target) throws ConversionException {

        if (source.getOwner() instanceof B2BUnitModel) {
            final B2BUnitModel owner = (B2BUnitModel) source.getOwner();
            target.setDefaultAddress(owner.getShippingAddress() != null && owner.getShippingAddress().equals(source));
        }
    }
}
