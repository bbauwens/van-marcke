package com.vanmarcke.facades.product.converters.populators;

import com.elision.hybris.l10n.facades.LocalizedMessageFacade;
import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeUnitModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeValueModel;
import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.classification.features.FeatureValue;
import de.hybris.platform.commercefacades.product.converters.populator.FeaturePopulator;
import de.hybris.platform.commercefacades.product.data.FeatureData;
import de.hybris.platform.commercefacades.product.data.FeatureUnitData;
import de.hybris.platform.commercefacades.product.data.FeatureValueData;
import de.hybris.platform.servicelayer.i18n.I18NService;
import org.apache.commons.lang.math.NumberUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Van Marcke Feature Populator that populates the classification code on {@FeatureData}
 */
public class VMKFeaturePopulator extends FeaturePopulator {

    private final LocalizedMessageFacade messageFacade;
    private final I18NService i18NService;

    /**
     * Provides an instance of the VanmarckeFeaturePopulator.
     *
     * @param messageFacade the localized message facade
     * @param i18NService   the i18n service
     */
    public VMKFeaturePopulator(LocalizedMessageFacade messageFacade, I18NService i18NService) {
        this.messageFacade = messageFacade;
        this.i18NService = i18NService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void populate(final Feature source, final FeatureData target) {
        final ClassAttributeAssignmentModel classAttributeAssignment = source.getClassAttributeAssignment();

        // Create the feature
        target.setCode(source.getCode());
        target.setComparable(Boolean.TRUE.equals(classAttributeAssignment.getComparable()));
        target.setDescription(classAttributeAssignment.getDescription());
        target.setName(source.getName());
        target.setRange(Boolean.TRUE.equals(classAttributeAssignment.getRange()));

        final ClassificationAttributeUnitModel unit = classAttributeAssignment.getUnit();
        if (unit != null) {
            final FeatureUnitData featureUnitData = new FeatureUnitData();
            featureUnitData.setName(unit.getName());
            featureUnitData.setSymbol(unit.getSymbol());
            featureUnitData.setUnitType(unit.getUnitType());
            target.setFeatureUnit(featureUnitData);
        }

        // Create the feature data items
        final List<FeatureValueData> featureValueDataList = new ArrayList<FeatureValueData>();
        for (final FeatureValue featureValue : source.getValues()) {
            final FeatureValueData featureValueData = new FeatureValueData();
            final Object value = featureValue.getValue();
            if (value instanceof ClassificationAttributeValueModel) {
                ClassificationAttributeValueModel classificationAttributeValueModel = ((ClassificationAttributeValueModel) value);
                featureValueData.setValue(classificationAttributeValueModel.getName());
                featureValueData.setCode(classificationAttributeValueModel.getCode());
            } else if (NumberUtils.isNumber(String.valueOf(value))) {
                featureValueData.setValue(String.valueOf(value).replaceAll("\\.0*$", ""));
            } else if ("true".equalsIgnoreCase(String.valueOf(value)) || "false".equalsIgnoreCase(String.valueOf(value))) {
                featureValueData.setValue(messageFacade.getMessageForCodeAndLocale("facet.boolean." + String.valueOf(value).toLowerCase(), i18NService.getCurrentLocale()));
            } else {
                featureValueData.setValue(String.valueOf(value));
            }

            featureValueDataList.add(featureValueData);
        }
        target.setFeatureValues(featureValueDataList);

        if (classAttributeAssignment.getClassificationAttribute() != null) {
            target.setClassificationCode(classAttributeAssignment.getClassificationAttribute().getCode());
        }
    }
}