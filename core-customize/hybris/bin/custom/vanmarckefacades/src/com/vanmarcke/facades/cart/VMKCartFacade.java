package com.vanmarcke.facades.cart;

import com.vanmarcke.facades.data.order.SavedCartData;
import com.vanmarcke.services.exception.UnsupportedDeliveryDateException;
import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commercefacades.order.data.AddToCartParams;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commerceservices.order.CommerceCartMergingException;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;

import java.util.Date;
import java.util.List;

/**
 * Custom interface for the CartFacade
 */
public interface VMKCartFacade extends CartFacade {

    /**
     * Method for adding a list of products to the cart
     *
     * @param addToCartParamsList the submitted cart
     * @return the cart modification data that includes a statusCode and the actual quantity added to the cart
     * @throws CommerceCartModificationException if the cart cannot be modified
     */
    List<CartModificationData> addToCart(List<AddToCartParams> addToCartParamsList) throws CommerceCartMergingException;

    /**
     * Method for updating the checkout mode for the cart
     *
     * @param checkoutMode the checkoutMode
     */
    void updateCartCheckoutMode(Boolean checkoutMode);

    /**
     * Method for converting a cart model to a saved cart dto for the purpose of export
     *
     * @param cartGuid the cart guid
     */
    SavedCartData getSavedCartDetails(String cartGuid);

    /**
     * Sets the given {@code requestedDeliveryDate} on the current shopping cart in case the requested delivery date
     * is valid.
     *
     * @param requestedDeliveryDate the requested delivery date
     * @return the current shopping cart
     * @throws UnsupportedDeliveryDateException in case the {@code requestedDeliveryDate} is invalid
     */
    CartData setRequestedDeliveryDate(final Date requestedDeliveryDate) throws UnsupportedDeliveryDateException;
}