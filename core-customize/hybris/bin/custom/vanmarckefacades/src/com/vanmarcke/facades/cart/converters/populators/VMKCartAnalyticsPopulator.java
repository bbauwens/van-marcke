package com.vanmarcke.facades.cart.converters.populators;

import com.vanmarcke.facades.analytics.data.CartAnalyticsData;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.converters.Populator;

/**
 * The {@code VMKCartAnalyticsPopulator} class is used to populate {@link CartAnalyticsData} instances with the data of
 * the corresponding {@link CartModificationData} instances.
 *
 * @author Christiaan Janssen
 * @since 30-04-2020
 */
public class VMKCartAnalyticsPopulator implements Populator<CartModificationData, CartAnalyticsData> {

    /**
     * {@inheritDoc}
     */
    @Override
    public void populate(CartModificationData cartModificationData, CartAnalyticsData cartAnalyticsData) {
        cartAnalyticsData.setCartCode(cartModificationData.getCartCode());
        cartAnalyticsData.setQuantity(cartModificationData.getQuantityAdded());
        cartAnalyticsData.setProductCode(getProductCode(cartModificationData.getEntry()));
        cartAnalyticsData.setProductName(getProductName(cartModificationData.getEntry()));
        cartAnalyticsData.setProductPostPrice(getProductPostPrice(cartModificationData.getEntry()));
    }

    /**
     * Returns te product name for the given {@code code}.
     *
     * @param entry the entry
     * @return the product name
     */
    protected String getProductName(OrderEntryData entry) {
        return entry == null || entry.getProduct() == null || entry.getProduct().getAnalyticsData() == null
                ? null
                : entry.getProduct().getAnalyticsData().getName();
    }

    /**
     * Returns te product code for the given {@code entry}.
     *
     * @param entry the entry
     * @return the product code
     */
    protected String getProductCode(OrderEntryData entry) {
        return entry == null || entry.getProduct() == null ? null : entry.getProduct().getCode();
    }

    /**
     * Returns the price for the given {@code entry}.
     *
     * @param entry the entry
     * @return the price
     */
    protected Double getProductPostPrice(OrderEntryData entry) {
        return entry == null || entry.getBasePrice() == null || entry.getBasePrice().getValue() == null ? null : entry.getBasePrice().getValue().doubleValue();
    }
}
