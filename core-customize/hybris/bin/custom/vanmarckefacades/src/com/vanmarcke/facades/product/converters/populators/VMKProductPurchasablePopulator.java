package com.vanmarcke.facades.product.converters.populators;

import com.vanmarcke.facades.sitechannel.AbstractSiteChannelAwarePopulator;
import com.vanmarcke.services.product.VMKVariantProductService;
import de.hybris.platform.commercefacades.product.data.VariantOptionData;
import de.hybris.platform.variants.model.VariantProductModel;

/**
 * Populate whether the a product is purchasable or not.
 *
 * @author Tom van den Berg
 * @since 21-10-2020
 */
public class VMKProductPurchasablePopulator extends AbstractSiteChannelAwarePopulator<VariantProductModel, VariantOptionData> {

    private final VMKVariantProductService variantProductService;

    /**
     * Provides an instance of the VMKProductPurchasablePopulator.
     *
     * @param variantProductService the variant product service
     */
    public VMKProductPurchasablePopulator(VMKVariantProductService variantProductService) {
        this.variantProductService = variantProductService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void doPopulate(VariantProductModel source, VariantOptionData target) {
        target.setPurchasable(variantProductService.isPurchasable(source));
    }
}
