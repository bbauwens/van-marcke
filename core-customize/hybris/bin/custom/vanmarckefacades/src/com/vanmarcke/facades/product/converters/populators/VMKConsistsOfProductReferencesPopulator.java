package com.vanmarcke.facades.product.converters.populators;

import de.hybris.platform.catalog.enums.ProductReferenceTypeEnum;
import de.hybris.platform.catalog.model.ProductReferenceModel;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.ProductReferenceData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import static java.util.stream.Collectors.toList;

/**
 * Populates product references (List of {@link ProductReferenceData}) for product ({@link ProductData}).
 */
public class VMKConsistsOfProductReferencesPopulator implements Populator<ProductModel, ProductData> {

    private Converter<ProductReferenceModel, ProductReferenceData> blueProductReferenceConsistsOfConverter;

    public VMKConsistsOfProductReferencesPopulator(final Converter<ProductReferenceModel, ProductReferenceData> blueProductReferenceConsistsOfConverter) {
        this.blueProductReferenceConsistsOfConverter = blueProductReferenceConsistsOfConverter;
    }

    @Override
    public void populate(final ProductModel source, final ProductData target) throws ConversionException {
        target.setConsistsOfReferences(source.getProductReferences()
                .stream()
                .filter(ref -> ProductReferenceTypeEnum.CONSISTS_OF.equals(ref.getReferenceType()))
                .map(ref -> blueProductReferenceConsistsOfConverter.convert(ref))
                .collect(toList()));
    }
}
