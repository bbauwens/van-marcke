package com.vanmarcke.facades.address.converters.populators;

import de.hybris.platform.commercefacades.user.converters.populator.AddressReversePopulator;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

/**
 * The {@code VMKAddressReversePopulator} class is used populate {@link AddressModel} instances with the data of the
 * {@link AddressData} instances.
 *
 * @author Christiaan Janssen
 * @since 23/09/2020
 */
public class VMKAddressReversePopulator extends AddressReversePopulator {

    /**
     * {@inheritDoc}
     */
    @Override
    public void populate(AddressData source, AddressModel target) throws ConversionException {
        super.populate(source, target);

        target.setAppartment(source.getApartment());
        target.setPhone2(source.getMobile());
    }
}
