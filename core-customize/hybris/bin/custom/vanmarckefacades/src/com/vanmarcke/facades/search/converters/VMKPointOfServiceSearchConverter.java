package com.vanmarcke.facades.search.converters;

import de.hybris.platform.commercefacades.storelocator.data.PointOfServiceData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.solrfacetsearch.search.AbstractSolrConverter;
import de.hybris.platform.solrfacetsearch.search.impl.SolrResult;

import static com.vanmarcke.facades.constants.VanmarckeFacadesConstants.SOLR.POINT_OF_SERVICE.*;

/**
 * Converts {@link SolrResult} instances to {@link PointOfServiceData} instances.
 *
 * @author Taki Korovessis
 * @since 19-8-2020
 */
public class VMKPointOfServiceSearchConverter extends AbstractSolrConverter<PointOfServiceData> {

    /**
     * {@inheritDoc}
     */
    @Override
    protected PointOfServiceData createDataObject() {
        return new PointOfServiceData();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PointOfServiceData convert(final SolrResult source, final PointOfServiceData target) throws ConversionException {
        target.setDisplayName(getValue(source, INDEX_DISPLAY_NAME));
        target.setName(getValue(source, INDEX_NAME));

        final AddressData addressData = new AddressData();
        addressData.setPhone(getValue(source, INDEX_PHONE_1));
        target.setAddress(addressData);

        return target;
    }
}