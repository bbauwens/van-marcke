package com.vanmarcke.facades.product.converters.populators;

import com.vanmarcke.services.product.VMKVariantProductService;
import de.hybris.platform.commercefacades.product.data.VariantOptionData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.variants.model.VariantProductModel;

/**
 * This class implements the {@link Populator} interface.
 * <p>
 * It handles No Stock items, which should not be purchasable.
 *
 * @author Tom van den Berg
 * @since 3-3-2020
 */
public class VMKVariantProductPurchasablePopulator implements Populator<VariantProductModel, VariantOptionData> {

    private final VMKVariantProductService variantProductService;

    /**
     * Returns an instance of the {@code BlueVariantProductPurchasablePopulator}
     *
     * @param variantProductService the variant product service
     */
    public VMKVariantProductPurchasablePopulator(VMKVariantProductService variantProductService) {
        this.variantProductService = variantProductService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void populate(VariantProductModel source, VariantOptionData target) {
        target.setPurchasable(target.isPurchasable() ? variantProductService.isPurchasable(source) : Boolean.FALSE);
    }
}
