package com.vanmarcke.facades.order.converters.populators;

import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

/**
 * Populator that adds a yard reference to the abstract order data
 */
public class VMKAbstractOrderYardReferencePopulator implements Populator<AbstractOrderModel, AbstractOrderData> {

    @Override
    public void populate(AbstractOrderModel source, AbstractOrderData target) throws ConversionException {
        target.setYardReference(source.getYardReference());
    }

}
