package com.vanmarcke.facades.search.converters.populators;

import de.hybris.platform.commercefacades.storelocator.converters.populator.TimeDataPopulator;

import java.text.DateFormat;
import java.util.TimeZone;

/**
 * Populates the time data.
 *
 * @author Joris Cryns
 * @since 1-8-2019
 */
public class VMKTimeDataPopulator extends TimeDataPopulator {

    /**
     * {@inheritDoc}
     */
    @Override
    protected DateFormat getDateFormat() {
        final DateFormat dateFormat = DateFormat.getTimeInstance(DateFormat.SHORT, getCurrentLocale());
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        return dateFormat;
    }
}
