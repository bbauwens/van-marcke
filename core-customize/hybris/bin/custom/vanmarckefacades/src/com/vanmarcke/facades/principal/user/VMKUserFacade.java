package com.vanmarcke.facades.principal.user;

import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

/**
 * Custom interface for UserFacade
 */
public interface VMKUserFacade extends UserFacade {

    /**
     * Sets users preferred site to the current session site
     */
    void syncSessionSite();

    /**
     * Sets users favorite store
     */
    void syncSessionStore();

    /**
     * Save the ShowNetPrice value for Customer
     */
    boolean saveNetPricePreference(boolean showNetPrice);

    /**
     * Get the nearest store for the current B2B Customer
     *
     * @return the nearest store
     */
    PointOfServiceModel getNearestStoreForB2BCustomer();

    /**
     * Get the nearest store for the current B2B Customer and given base store
     *
     * @param baseStore the base store
     * @return the nearest store
     */
    PointOfServiceModel getNearestStoreForB2BCustomer(BaseStoreModel baseStore);
}
