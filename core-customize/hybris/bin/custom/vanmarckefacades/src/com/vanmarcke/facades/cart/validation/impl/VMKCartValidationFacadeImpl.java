package com.vanmarcke.facades.cart.validation.impl;

import com.vanmarcke.facades.cart.validation.VMKCartValidationFacade;
import com.vanmarcke.services.MessageData;
import com.vanmarcke.services.validation.VMKCartValidationService;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartService;

/**
 * The {@code DefaultVMKCartValidationFacade} class implements the facade methods concerning the cart validation.
 *
 * @author Niels Raemaekers, Christiaan Janssen
 * @since 03-02-2020
 */
public class VMKCartValidationFacadeImpl implements VMKCartValidationFacade {

    private final CartService cartService;
    private final VMKCartValidationService vmkCartValidationService;

    /**
     * Creates a new instance of the {@link VMKCartValidationFacadeImpl} class.
     *
     * @param cartService              the cart service
     * @param vmkCartValidationService the cart validation service
     */
    public VMKCartValidationFacadeImpl(CartService cartService, VMKCartValidationService vmkCartValidationService) {
        this.cartService = cartService;
        this.vmkCartValidationService = vmkCartValidationService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MessageData checkCartValidityAndProcessResponse() {
        if (cartService.hasSessionCart()) {
            CartModel cart = cartService.getSessionCart();
            return vmkCartValidationService.isCartValid(cart);
        }
        return null;
    }
}
