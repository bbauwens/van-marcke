package com.vanmarcke.facades.principal.customer.populators;

import com.vanmarcke.core.model.VMKConsentModel;
import com.vanmarcke.facades.data.VMKConsentData;
import de.hybris.platform.converters.Populator;

/**
 * This populator populates {@link VMKConsentModel} to {@link VMKConsentData} instances.
 *
 * @author Tom van den Berg
 * @since 05-06-2020
 */
public class VMKCustomerConsentPopulator implements Populator<VMKConsentModel, VMKConsentData> {

    /**
     * {@inheritDoc}
     */
    @Override
    public void populate(VMKConsentModel source, VMKConsentData target) {
        target.setConsentType(source.getConsentType());
        target.setOptType(source.getOptType());
    }
}
