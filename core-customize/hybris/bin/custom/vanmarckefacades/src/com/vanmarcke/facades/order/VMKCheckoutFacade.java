package com.vanmarcke.facades.order;

import com.vanmarcke.facades.data.DeliveryInfoData;
import com.vanmarcke.facades.delivery.data.DeliveryResponseData;
import com.vanmarcke.services.forms.delivery.DeliveryMethodForm;
import de.hybris.platform.b2bacceleratorfacades.api.cart.CheckoutFacade;

import java.util.Map;

/**
 * The {@code BlueCheckoutFacade} interface defines the methods that enable the checkout process.
 *
 * @author Niels Raemaekers, Taki Korovessis, Tom van den Berg
 * @since 04-07-2019
 */
public interface VMKCheckoutFacade extends CheckoutFacade, de.hybris.platform.commercefacades.order.CheckoutFacade {

    /**
     * Returns the available delivery modes as a map.
     *
     * @return the delivery modes
     */
    Map<String, Boolean> getAvailableDeliveryMethods();

    /**
     * Returns the mapping between the delivery mode and delivery method.
     *
     * @return the mapping between the delivery mode and delivery method
     */
    Map<String, String> getDeliveryMethodCodes();

    /**
     * Returns the first possible delivery date based on the given delivery method and selected date.
     *
     * @param deliveryMethod the delivery method
     * @param selectedDate   the selected date
     * @return the {@link DeliveryResponseData} instance
     */
    DeliveryInfoData getDeliveryInformation(String deliveryMethod, String selectedDate);

    /**
     * Generates the {@link DeliveryMethodForm} for the current session cart.
     *
     * @return the delivery method form
     */
    DeliveryMethodForm getDeliveryMethodForm();

    /**
     * Stores the delivery information wich is part of the fiven {@code form}.
     *
     * @param form the form
     */
    void saveDeliveryInformation(DeliveryMethodForm form);

    /**
     * Adds the given TEC store uid to the session cart.
     *
     * @param tecUid the TEC store uid
     */
    void setFavoriteAlternativeStoreOnSessionCart(String tecUid);

    /**
     * Returns a joined string of cart entry codes which cannot be ordered.
     *
     * @return the string of product codes
     */
    String getInvalidCartEntries();

    /**
     * Remove the invalid cart entries
     *
     * @param invalidCartEntries the invalid cart entries
     * @return decision if the remove happened or not
     */
    boolean removeInvalidCartEntries(String invalidCartEntries);


    /**
     * Adjusts quantity in the cart for discontinued products, given the remaining EDC stock
     *
     * @return joined string of cart entry codes for which we adjusted the quantity, or null if the adjustment was not needed
     */
    String adjustQuantityForDiscontinuedProducts();
}
