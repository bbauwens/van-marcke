package com.vanmarcke.facades.wishlist2.populators;

import com.vanmarcke.facades.data.order.SavedCartData;
import com.vanmarcke.facades.data.wishlist2.WishListEntryData;
import de.hybris.platform.acceleratorservices.enums.ImportStatus;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.converters.ConfigurablePopulator;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.wishlist2.model.Wishlist2EntryModel;
import de.hybris.platform.wishlist2.model.Wishlist2Model;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class VMKWishListPopulator implements Populator<Wishlist2Model, SavedCartData> {

    private ConfigurablePopulator<Wishlist2EntryModel, WishListEntryData, String> wishListEntryPopulator;
    private Collection<String> entryOptions;

    /**
     * {@inheritDoc}
     */
    @Override
    public void populate(Wishlist2Model wishlistModel, SavedCartData wishlistData) throws ConversionException {
        wishlistData.setImportStatus(ImportStatus.COMPLETED);
        wishlistData.setName(wishlistModel.getName());
        wishlistData.setSaveTime(wishlistModel.getModifiedtime());
        wishlistData.setDescription(wishlistModel.getDescription());

        // technical sheet data
        boolean hasTechnicalDataSheet = wishlistModel.getEntries()
                .stream()
                .anyMatch(entry -> entry.getProduct().getTech_data_sheet() != null);
        wishlistData.setTechnicalSheetsAvailable(hasTechnicalDataSheet);

        // entries if required
        wishlistData.setQuantity(getQuantity(wishlistModel));


        if (wishListEntryPopulator != null) {
            final List<OrderEntryData> entryDataList = Optional.ofNullable(wishlistModel.getEntries())
                    .orElseGet(Collections::emptyList)
                    .stream()
                    .map(entryModel -> {
                        final WishListEntryData entryData = new WishListEntryData();
                        wishListEntryPopulator.populate(entryModel, entryData, entryOptions);
                        return entryData;
                    })
                    .collect(Collectors.toList());
            wishlistData.setEntries(entryDataList);
        }
    }

    /**
     * Returns the quantity.
     *
     * @param wishList the wish list
     * @return the quantity
     */
    private long getQuantity(final Wishlist2Model wishList) {
        return wishList.getEntries().stream().mapToLong(Wishlist2EntryModel::getDesired).sum();
    }

    public void setWishListEntryPopulator(ConfigurablePopulator<Wishlist2EntryModel, WishListEntryData, String> wishListEntryPopulator) {
        this.wishListEntryPopulator = wishListEntryPopulator;
    }

    public void setEntryOptions(Collection<String> entryOptions) {
        this.entryOptions = entryOptions;
    }
}