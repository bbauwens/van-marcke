package com.vanmarcke.facades.order.converters.populators;

import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

public class VMKPurchaseOrderNumberOrderPopulator implements Populator<OrderModel, OrderData> {

    @Override
    public void populate(final OrderModel source, final OrderData target) throws ConversionException {
        target.setPurchaseOrderNumber(source.getPurchaseOrderNumber());
    }
}