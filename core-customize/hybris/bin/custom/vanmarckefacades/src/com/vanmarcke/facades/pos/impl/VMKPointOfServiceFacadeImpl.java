package com.vanmarcke.facades.pos.impl;

import com.vanmarcke.facades.pos.VMKPointOfServiceFacade;
import com.vanmarcke.facades.search.VMKPointOfServiceLocatorSearchFacade;
import com.vanmarcke.services.storelocator.pos.VMKPointOfServiceService;
import de.hybris.platform.commercefacades.storelocator.data.PointOfServiceData;
import de.hybris.platform.commerceservices.storefinder.data.StoreFinderSearchPageData;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.services.BaseStoreService;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static com.vanmarcke.facades.constants.VanmarckeFacadesConstants.SOLR.DEFAULT_SORT;
import static com.vanmarcke.facades.constants.VanmarckeFacadesConstants.SOLR.POINT_OF_SERVICE.SOLR_OFFSET;
import static java.util.Objects.nonNull;
import static org.apache.commons.collections.CollectionUtils.isNotEmpty;

/**
 * Facade which provides methods related to {@link PointOfServiceModel} instances.
 *
 * @author Tom van den Berg, Giani Ifrim
 * @since 30-12-2020
 */
public class VMKPointOfServiceFacadeImpl implements VMKPointOfServiceFacade {

    private final VMKPointOfServiceService pointOfServiceService;
    private final VMKPointOfServiceLocatorSearchFacade pointOfServiceLocatorSearchFacade;
    private final BaseStoreService baseStoreService;

    /**
     * Provides an instance of the VMKPointOfServiceFacadeImpl.
     *
     * @param pointOfServiceService             the point of service service
     * @param pointOfServiceLocatorSearchFacade the point of service locator search facade
     */
    public VMKPointOfServiceFacadeImpl(VMKPointOfServiceService pointOfServiceService,
                                       VMKPointOfServiceLocatorSearchFacade pointOfServiceLocatorSearchFacade,
                                       BaseSiteService baseSiteService,
                                       BaseStoreService baseStoreService) {
        this.pointOfServiceService = pointOfServiceService;
        this.pointOfServiceLocatorSearchFacade = pointOfServiceLocatorSearchFacade;
        this.baseStoreService = baseStoreService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<PointOfServiceData> findAlternativePointOfServicesCloseByCurrentStore(CartModel cart) {
        List<PointOfServiceModel> alternativeStores = pointOfServiceService.getAlternativeTECStoresWhichAreCurrentlyOpenForCart(cart);
        PointOfServiceModel currentStore = cart.getDeliveryPointOfService();

        if (isNotEmpty(alternativeStores) && nonNull(currentStore)) {
            StoreFinderSearchPageData<PointOfServiceData> searchPageData = pointOfServiceLocatorSearchFacade.search(
                    null,
                    pointOfServiceService.getGeoPoint(currentStore),
                    1_000_000,
                    SOLR_OFFSET,
                    DEFAULT_SORT,
                    getAlternativeStoreIDs(alternativeStores));
            return searchPageData.getResults();
        }
        return Collections.emptyList();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PointOfServiceModel getFallbackPOS() {
        return baseStoreService.getCurrentBaseStore().getPointsOfService().get(0);
    }

    /**
     * Maps the given list of {@link PointOfServiceModel} to a list of store names.
     *
     * @param stores the list of stores
     * @return the list of store names
     */
    private List<String> getAlternativeStoreIDs(List<PointOfServiceModel> stores) {
        return stores.stream()
                .map(PointOfServiceModel::getName)
                .collect(Collectors.toList());
    }
}
