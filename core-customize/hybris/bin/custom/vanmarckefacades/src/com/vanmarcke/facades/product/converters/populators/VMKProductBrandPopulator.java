package com.vanmarcke.facades.product.converters.populators;

import com.vanmarcke.core.model.BrandCategoryModel;
import com.vanmarcke.facades.product.data.BrandData;
import com.vanmarcke.services.category.VMKCategoryService;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.Optional;

/**
 * The {@link VMKProductBrandPopulator} class is used to populate the product brand information.
 *
 * @author Christiaan Janssen
 * @since 10-01-2020
 */
public class VMKProductBrandPopulator<SOURCE extends ProductModel, TARGET extends ProductData> implements Populator<SOURCE, TARGET> {

    private static final String MEDIA_FORMAT = "SMALL";

    private final Converter<MediaModel, ImageData> imageConverter;
    private final VMKCategoryService categoryService;

    /**
     * Creates a new instance of the {@link VMKProductBrandPopulator} class.
     *
     * @param imageConverter  the image converter
     * @param categoryService the category service
     */
    public VMKProductBrandPopulator(final Converter<MediaModel, ImageData> imageConverter,
                                    final VMKCategoryService categoryService) {
        this.imageConverter = imageConverter;
        this.categoryService = categoryService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void populate(final SOURCE source, final TARGET target) {
        final BrandData brand = new BrandData();
        brand.setSeries(source.getBrandSeries());
        brand.setType(source.getBrandType());

        final Optional<BrandCategoryModel> brandCategory = getBrandCategory(source);
        if (brandCategory.isPresent()) {
            brand.setName(brandCategory.get().getName());
            brand.setLogo(getBrandLogo(brandCategory.get()));
        }

        target.setBrand(brand);
    }

    /**
     * Returns the brand category.
     *
     * @param source the product model
     * @return the brand category
     */
    protected Optional<BrandCategoryModel> getBrandCategory(final SOURCE source) {
        Optional<BrandCategoryModel> brandCategory = Optional.empty();
        final Optional<VariantProductModel> variant = getVariant(source);
        if (variant.isPresent()) {
            brandCategory = variant.get().getSupercategories()
                    .stream()
                    .filter(BrandCategoryModel.class::isInstance)
                    .map(BrandCategoryModel.class::cast)
                    .findAny();
        }
        return brandCategory;
    }

    /**
     * Returns the variant for the given {@code source}.
     * <p>
     * Returns the {@code source} if it's a product variant. If the given {@code source} is a base product, it returns
     * the product variant with a brand category.
     *
     * @param source the product model
     * @return the product variant model
     */
    protected Optional<VariantProductModel> getVariant(final SOURCE source) {
        Optional<VariantProductModel> variant;
        if (source instanceof VariantProductModel) {
            variant = Optional.of((VariantProductModel) source);
        } else {
            variant = source.getVariants()
                    .stream()
                    .filter(this::hasBrandCategory)
                    .findFirst();
        }
        return variant;
    }

    /**
     * Checks whether the given {@code variant} is assigned to a brand category.
     *
     * @param variant the product variant
     * @return {@code true} in case the given {@code variant} is assigned to a brand category, {@code false} otherwise
     */
    private boolean hasBrandCategory(final VariantProductModel variant) {
        return variant.getSupercategories()
                .stream()
                .anyMatch(BrandCategoryModel.class::isInstance);
    }

    /**
     * Returns the logo for the given {@code brandCategory}.
     *
     * @param brandCategory the brand category
     * @return the brand logo
     */
    protected ImageData getBrandLogo(final BrandCategoryModel brandCategory) {
        ImageData brandLogoData = null;
        final Optional<MediaModel> brandLogoModel = categoryService.getLocalizedBrandLogoForFormat(brandCategory, MEDIA_FORMAT);
        if (brandLogoModel.isPresent()) {
            brandLogoData = imageConverter.convert(brandLogoModel.get());
        }
        return brandLogoData;
    }
}

