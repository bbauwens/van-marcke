package com.vanmarcke.facades.stock.impl;

import com.vanmarcke.facades.product.data.StockListData;
import com.vanmarcke.facades.stock.VMKStockFacade;
import com.vanmarcke.services.basestore.VMKBaseStoreService;
import com.vanmarcke.services.factories.VMKStockServiceFactory;
import com.vanmarcke.services.product.VMKStockService;
import com.vanmarcke.services.product.VMKVariantProductService;
import com.vanmarcke.services.storelocator.pos.VMKPointOfServiceService;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import de.hybris.platform.variants.model.VariantProductModel;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.Assert;

import static org.apache.commons.lang3.ArrayUtils.toArray;

/**
 * Van Marcke Stock Facade implementation
 *
 * @author Tom van den Berg
 * @since 21-10-2020
 */
public class VMKStockFacadeImpl implements VMKStockFacade {

    private final ProductService productService;
    private final VMKBaseStoreService baseStoreService;
    private final VMKPointOfServiceService pointOfServiceService;
    private final VMKStockServiceFactory stockServiceFactory;
    private final VMKVariantProductService variantProductService;

    /**
     * Provides an implementation of the VMKStockFacadeImpl.
     * @param productService        the product service
     * @param baseStoreService      the base store service
     * @param pointOfServiceService the point of service service
     * @param stockServiceFactory   the stock service factory
     * @param variantProductService the variant product service
     */
    public VMKStockFacadeImpl(ProductService productService,
                              VMKBaseStoreService baseStoreService,
                              VMKPointOfServiceService pointOfServiceService,
                              VMKStockServiceFactory stockServiceFactory, VMKVariantProductService variantProductService) {
        this.productService = productService;
        this.baseStoreService = baseStoreService;
        this.pointOfServiceService = pointOfServiceService;
        this.stockServiceFactory = stockServiceFactory;
        this.variantProductService = variantProductService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ProductData getProductDataWithStockForCurrentWareHouse(String productCode) {
        Assert.notNull(productCode, "Product code cannot be null.");
        ProductModel product = productService.getProductForCode(productCode);

        ProductData productData = null;
        if (product instanceof VariantProductModel) {
            StockListData stockList = getStock(product, toArray(getCurrentStoreCode()));
            productData = new ProductData();
            productData.setCode(productCode);
            productData.setStockListData(stockList);
        }
        return productData;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public StockListData getStockForProductCodeAndCurrentBaseStore(String productCode) {
        Assert.notNull(productCode, "Product code cannot be null.");
        String[] stores = findStoresForCurrentBaseStore();
        ProductModel product = productService.getProductForCode(productCode);
        return getStock(product, stores);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public StockListData getStockForProductCodeAndWarehouse(String code, String wareHouse) {
        Assert.notNull(code, "Product code cannot be null.");
        Assert.notNull(wareHouse, "Warehouse cannot be null.");
        ProductModel product = productService.getProductForCode(code);
        return getStock(product, toArray(wareHouse));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long getAvailableEDCStock(String code) {
        return variantProductService.getAvailableEDCStock(productService.getProductForCode(code));
    }

    /**
     * Get StockListData for the given Product and warehouses
     *
     * @param product              The {@link ProductModel} for which to retrieve stock
     * @param wareHouseIdentifiers The warehouses for which to retrieve stock
     * @return StockListData containing stock for EDC and the stock of the given wareHouseIdentifiers
     */
    protected StockListData getStock(ProductModel product, String[] wareHouseIdentifiers) {
        VMKStockService stockService = stockServiceFactory.getStockService();
        return stockService.getStockForProductAndWarehouses(product, wareHouseIdentifiers);
    }

    /**
     * Get all warehouse names for the current BaseStore
     *
     * @return An array of Strings containing the names of the warehouses for the current BaseStore
     */
    protected String[] findStoresForCurrentBaseStore() {
        BaseStoreModel currentBaseStore = baseStoreService.getCurrentBaseStore();
        return currentBaseStore.getPointsOfService().stream()
                .map(PointOfServiceModel::getName)
                .toArray(String[]::new);
    }

    /**
     * Retrieves the TEC store ID for the current point of service.
     *
     * @return the TEC store ID
     */
    private String getCurrentStoreCode() {
        PointOfServiceModel sessionStore = pointOfServiceService.getCurrentPointOfService();
        return sessionStore != null ? sessionStore.getName() : StringUtils.EMPTY;
    }
}
