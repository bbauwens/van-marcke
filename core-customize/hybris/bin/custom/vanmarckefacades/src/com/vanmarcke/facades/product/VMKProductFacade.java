package com.vanmarcke.facades.product;

import com.vanmarcke.facades.product.data.PriceListData;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.Collection;
import java.util.List;

/**
 * Extension of de.hybris.platform.commercefacades.product.ProductFacade
 *
 * @author marteto
 */
public interface VMKProductFacade extends ProductFacade {

    /**
     * Get the LIST and NET price for the given productCode
     *
     * @param productCode The code of the product
     * @return PriceListData containing LIST and (optional) NET price
     */
    PriceListData getPricesForProduct(String productCode);

    /**
     * Populates an already existing product data by using the configured populators
     *
     * @param productData the already created Product data object
     * @param solrVariantData variants data retrieved from solr
     * @param isListView true if the enrichment is needed on the category / product list view page, and false if it is needed on product detail page
     */
    void enrichProductDataWithSolrVariants(ProductData productData, List<ProductData> solrVariantData, boolean isListView);

    /**
     * Enrich product data with brand logo, given that the brand category code is already populated
     * @param productData the already created Product data object
     */
    void enrichProductDataWithBrandLogo(ProductData productData);
}
