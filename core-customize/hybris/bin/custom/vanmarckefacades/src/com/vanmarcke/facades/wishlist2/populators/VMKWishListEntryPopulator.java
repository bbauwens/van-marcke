package com.vanmarcke.facades.wishlist2.populators;

import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.wishlist2.model.Wishlist2EntryModel;

public class VMKWishListEntryPopulator implements Populator<Wishlist2EntryModel, OrderEntryData> {

    private final Converter<ProductModel, ProductData> productConverter;
    private final ProductService productService;

    /**
     * Creates a new instance of the {@link VMKWishListEntryPopulator} class.
     *
     * @param productConverter product converter
     * @param productService
     */
    public VMKWishListEntryPopulator(Converter<ProductModel, ProductData> productConverter, ProductService productService) {
        this.productConverter = productConverter;
        this.productService = productService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void populate(Wishlist2EntryModel wishlistEntryModel, OrderEntryData wishlistEntryData) throws ConversionException {

        final long quantity = wishlistEntryModel.getDesired() == null? 1L : wishlistEntryModel.getDesired().longValue();

        wishlistEntryData.setQuantity(quantity);

        ProductModel productModel = wishlistEntryModel.getProduct();
        if (productModel != null) {
            // retrieve the product by code to trigger search restriction if product is unavailable
            productModel = productService.getProductForCode(productModel.getCode());
            wishlistEntryData.setProduct(productConverter.convert(productModel));
        }
    }
}