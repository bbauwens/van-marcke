package com.vanmarcke.facades.search;

import de.hybris.platform.commercefacades.storelocator.data.PointOfServiceData;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfig;
import de.hybris.platform.solrfacetsearch.config.exceptions.FacetConfigServiceException;

public abstract class AbstractVMKPointOfServiceSearchFacade extends AbstractVMKSearchFacade<PointOfServiceData> {

    @Override
    protected FacetSearchConfig getFacetSearchConfig() throws FacetConfigServiceException {
        return getFacetSearchConfigService().getConfiguration(getCmsSiteService().getCurrentSite().getStoreFinderSolrFacetSearchConfiguration().getName());
    }
}