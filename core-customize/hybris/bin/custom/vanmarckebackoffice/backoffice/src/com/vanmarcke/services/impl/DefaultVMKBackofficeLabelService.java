package com.vanmarcke.services.impl;

import com.hybris.backoffice.labels.impl.BackofficeLabelService;
import com.vanmarcke.services.VMKBackofficeLabelService;
import org.apache.commons.lang3.time.FastDateFormat;

import java.util.Date;

import static java.util.TimeZone.getTimeZone;
import static org.apache.commons.lang.StringUtils.EMPTY;
import static org.apache.commons.lang3.StringUtils.defaultString;
import static org.apache.commons.lang3.time.FastDateFormat.getInstance;

/**
 * Custom implementation of {@link VMKBackofficeLabelService}
 */
public class DefaultVMKBackofficeLabelService extends BackofficeLabelService implements VMKBackofficeLabelService {

    private static final String DEFAULT_DATE_PATTERN = "yyyy-MM-dd";
    private static final String DEFAULT_TIME_ZONE_ID = "UTC";

    @Override
    public String getDateLabel(final Object object, final String datePattern, final String timeZoneId) {
        if (object instanceof Date) {
            final String pattern = defaultString(datePattern, DEFAULT_DATE_PATTERN);
            final String timeZone = defaultString(timeZoneId, DEFAULT_TIME_ZONE_ID);

            final FastDateFormat fastDateFormat = getInstance(pattern, getTimeZone(timeZone));
            return fastDateFormat.format(object);
        }
        return EMPTY;
    }
}
