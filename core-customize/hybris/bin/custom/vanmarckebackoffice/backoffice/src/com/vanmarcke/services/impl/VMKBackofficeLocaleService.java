package com.vanmarcke.services.impl;

import com.hybris.backoffice.i18n.BackofficeLocaleService;
import com.hybris.cockpitng.config.locales.jaxb.CockpitLocale;

import java.util.List;
import java.util.stream.Collectors;

/**
 * custom class to filter out languages that are not defined specifically as default=true in the available-locales config component
 *
 * @author Cristian Stoica
 * @since 04-08-2021
 */
public class VMKBackofficeLocaleService extends BackofficeLocaleService {
    /**
     * return available locales to display in the quick toggle widget
     * @param principal the principal
     * @return the filtered list of languages
     */
    @Override
    protected List<CockpitLocale> getAvailableCockpitLocales(final String principal) {
        List<CockpitLocale> availableLocales = super.getAvailableCockpitLocales(principal);
        return availableLocales.stream()
                .filter(CockpitLocale::isDefaultLocale)
                .collect(Collectors.toList());
    }
}
