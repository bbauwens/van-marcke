package com.vanmarcke.dto;

import de.hybris.platform.core.model.type.AtomicTypeModel;
import de.hybris.platform.core.model.type.AttributeDescriptorModel;
import de.hybris.platform.core.model.type.CollectionTypeModel;
import de.hybris.platform.core.model.type.MapTypeModel;
import de.hybris.platform.core.model.type.TypeModel;
import de.hybris.platform.integrationbackoffice.dto.ListItemAttributeDTO;
import de.hybris.platform.integrationbackoffice.dto.ListItemStructureType;

public class VMKListItemAttributeDTO extends ListItemAttributeDTO {
    private boolean isSupported;

    public VMKListItemAttributeDTO(boolean selected, boolean customUnique, boolean autocreate, AttributeDescriptorModel attributeDescriptor, ListItemStructureType structureType, String alias, TypeModel type) {
        super(selected, customUnique, autocreate, attributeDescriptor, structureType, alias, type);
        this.isSupported = determineIsSupported();
    }

    private boolean determineIsSupported() {
        final AttributeDescriptorModel descriptor = getAttributeDescriptor();

        if (descriptor.getAttributeType() instanceof MapTypeModel) {
            final MapTypeModel mapTypeModel = (MapTypeModel) descriptor.getAttributeType();
            final boolean isPrimitiveMap = mapTypeModel.getReturntype() instanceof AtomicTypeModel && mapTypeModel.getArgumentType() instanceof AtomicTypeModel;
            final boolean isLocalized = descriptor.getLocalized() && (mapTypeModel.getReturntype() instanceof AtomicTypeModel || mapTypeModel.getReturntype() instanceof CollectionTypeModel || mapTypeModel.getReturntype().getCode().equals("Media"));

            return isPrimitiveMap || isLocalized;
        } else {
            return true;
        }
    }

    @Override
    public boolean isSupported() {
        return isSupported;
    }
}
