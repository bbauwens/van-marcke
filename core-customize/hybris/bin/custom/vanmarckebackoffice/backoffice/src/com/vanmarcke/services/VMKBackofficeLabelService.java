package com.vanmarcke.services;

import com.hybris.cockpitng.labels.LabelService;

public interface VMKBackofficeLabelService extends LabelService {

    /**
     * Formats an {@link Object} with the given date pattern and timezone if it's of type {@link java.util.Date}
     *
     * @param object      the object to format
     * @param datePattern the date pattern
     * @param timeZoneId  the timezone unique identifier
     * @return the formatted date if the specified {@link Object} is of type {@link java.util.Date}, otherwise an empty {@link String}
     */
    String getDateLabel(Object object, String datePattern, String timeZoneId);
}