package com.vanmarcke.widgets.editors;

import de.hybris.platform.platformbackoffice.classification.ClassificationInfo;

public class VMKClassificationInfo extends ClassificationInfo {

    public VMKClassificationInfo(final ClassificationInfo info) {
        super(info.getAssignment(), info.getValue());
    }

    @Override
    public boolean isUnitDisplayed() {
        return hasUnit();
    }
}
