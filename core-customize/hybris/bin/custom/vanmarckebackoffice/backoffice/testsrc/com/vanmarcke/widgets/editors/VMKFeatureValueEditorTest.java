package com.vanmarcke.widgets.editors;

import com.hybris.cockpitng.editors.EditorContext;
import com.hybris.cockpitng.editors.EditorDefinition;
import com.hybris.cockpitng.editors.EditorListener;
import com.hybris.cockpitng.editors.EditorRegistry;
import com.hybris.cockpitng.testing.util.BeanLookupFactory;
import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeUnitModel;
import de.hybris.platform.catalog.model.classification.ClassificationSystemVersionModel;
import de.hybris.platform.classification.ClassificationSystemService;
import de.hybris.platform.classification.features.FeatureValue;
import de.hybris.platform.platformbackoffice.classification.ClassificationInfo;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.zkoss.zul.Div;

import static com.hybris.cockpitng.testing.util.CockpitTestUtil.mockBeanLookup;
import static com.hybris.cockpitng.testing.util.CockpitTestUtil.mockZkEnvironment;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyMap;
import static java.util.Collections.emptySet;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class VMKFeatureValueEditorTest {

    @Spy
    @InjectMocks
    private VMKFeatureValueEditor vmkFeatureValueEditor;

    @Mock
    private EditorDefinition definition;
    @Mock
    private EditorListener<FeatureValue> editorListener;
    @Mock
    private transient ClassificationSystemService classificationSystemService;
    @Mock
    private EditorRegistry editorRegistry;
    @Mock
    private ClassificationSystemVersionModel systemVersionModel;
    @Mock
    private ClassificationSystemVersionModel classificationSystemVersionModel;

    @Before
    public void setUp() {
        mockZkEnvironment();
        mockBeanLookup(BeanLookupFactory.createBeanLookup().registerBean("editorRegistry", this.editorRegistry));
    }

    @Test
    public void testRenderWithoutUnit() {
        FeatureValue featureValue = mock(FeatureValue.class);

        when(featureValue.getValue()).thenReturn(100);

        Div parent = new Div();

        vmkFeatureValueEditor.render(parent, mockEditorContext(featureValue), this.editorListener);

        assertThat(parent.getChildren()).hasSize(1);
        assertThat(parent.getChildren().get(0)).isInstanceOf(Div.class);
        Div container = (Div) parent.getChildren().get(0);
        assertThat(container.getSclass()).isEqualTo("yw-feature-value-container");
        assertThat(container.getChildren()).hasSize(2);
    }

    @Test
    public void testRenderWithUnit() {
        FeatureValue featureValue = mock(FeatureValue.class);
        ClassificationAttributeUnitModel classificationAttributeUnitModel = mock(ClassificationAttributeUnitModel.class);

        when(classificationAttributeUnitModel.getSystemVersion()).thenReturn(systemVersionModel);
        when(classificationAttributeUnitModel.getUnitType()).thenReturn("unit-type");

        when(featureValue.getValue()).thenReturn(100);
        when(featureValue.getUnit()).thenReturn(classificationAttributeUnitModel);

        Div parent = new Div();

        vmkFeatureValueEditor.render(parent, this.mockEditorContext(featureValue, classificationAttributeUnitModel), this.editorListener);

        assertThat(parent.getChildren()).hasSize(1);
        assertThat(parent.getChildren().get(0)).isInstanceOf(Div.class);
        Div container = (Div) parent.getChildren().get(0);
        assertThat(container.getSclass()).isEqualTo("yw-feature-value-container yw-feature-value-container-with-unit");
        assertThat(container.getChildren()).hasSize(3);
    }

    private EditorContext<FeatureValue> mockEditorContext(FeatureValue initialValue) {
        ClassAttributeAssignmentModel classAttributeAssignmentModel = mock(ClassAttributeAssignmentModel.class);

        ClassificationInfo classificationInfo = new ClassificationInfo(classAttributeAssignmentModel, initialValue);

        EditorContext<FeatureValue> ctx = new EditorContext(initialValue, this.definition, emptyMap(), emptyMap(), emptySet(), emptySet());
        ctx.setValueType(Integer.class.getCanonicalName());
        ctx.setParameter("classificationInfo", classificationInfo);
        return ctx;
    }

    private EditorContext<FeatureValue> mockEditorContext(FeatureValue initialValue, ClassificationAttributeUnitModel initialUnit) {
        ClassAttributeAssignmentModel classAttributeAssignmentModel = mock(ClassAttributeAssignmentModel.class);
        ClassificationAttributeModel classAttributeModel = mock(ClassificationAttributeModel.class);

        when(classAttributeModel.getSystemVersion()).thenReturn(classificationSystemVersionModel);

        when(classAttributeAssignmentModel.getUnit()).thenReturn(initialUnit);
        when(classAttributeAssignmentModel.getClassificationAttribute()).thenReturn(classAttributeModel);
        when(classAttributeAssignmentModel.getSystemVersion()).thenReturn(classificationSystemVersionModel);

        when(classificationSystemService.getUnitsOfTypeForSystemVersion(classificationSystemVersionModel, "unit-type")).thenReturn(asList(new ClassificationAttributeUnitModel[]{initialUnit}));

        ClassificationInfo classificationInfo = new ClassificationInfo(classAttributeAssignmentModel, initialValue);

        EditorContext<FeatureValue> ctx = new EditorContext(initialValue, this.definition, emptyMap(), emptyMap(), emptySet(), emptySet());
        ctx.setValueType(Integer.class.getCanonicalName());
        ctx.setParameter("classificationInfo", classificationInfo);
        return ctx;
    }

}
