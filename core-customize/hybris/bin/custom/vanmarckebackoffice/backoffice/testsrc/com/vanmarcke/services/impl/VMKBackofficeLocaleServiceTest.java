package com.vanmarcke.services.impl;

import com.hybris.backoffice.i18n.BackofficeLocaleService;
import com.hybris.cockpitng.config.locales.jaxb.CockpitLocale;
import com.hybris.cockpitng.config.locales.jaxb.CockpitLocales;
import com.hybris.cockpitng.core.config.CockpitConfigurationException;
import com.hybris.cockpitng.core.config.CockpitConfigurationService;
import com.hybris.cockpitng.core.config.ConfigContext;
import de.hybris.bootstrap.annotations.UnitTest;
import junit.framework.TestCase;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;
import java.util.Locale;

import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKBackofficeLocaleServiceTest extends TestCase {

    @Mock
    CockpitConfigurationService cockpitConfigurationService;

    @Spy
    @InjectMocks
    VMKBackofficeLocaleService vmkBackofficeLocaleService;

    @Test
    public void testGetAvailableLocales() throws CockpitConfigurationException {
        CockpitLocale cockpitLocale1 = mock(CockpitLocale.class);
        CockpitLocale cockpitLocale2 = mock(CockpitLocale.class);
        CockpitLocales cockpitLocales = mock(CockpitLocales.class);
        when(cockpitConfigurationService.loadConfiguration(any(ConfigContext.class), eq(CockpitLocales.class))).thenReturn(cockpitLocales);
        doReturn(List.of(Locale.GERMAN, Locale.FRANCE)).when(vmkBackofficeLocaleService).getAllLocales();
        when(cockpitLocales.getCockpitLocale()).thenReturn(List.of(cockpitLocale1, cockpitLocale2));
        when(cockpitLocale1.isDefaultLocale()).thenReturn(true);
        when(cockpitLocale2.isDefaultLocale()).thenReturn(false);
        when(cockpitLocale1.getLocale()).thenReturn(Locale.GERMAN);
        when(cockpitLocale1.getLocale()).thenReturn(Locale.FRANCE);

        List<CockpitLocale> returnedList = vmkBackofficeLocaleService.getAvailableCockpitLocales("admin");
        Assert.assertEquals(1, returnedList.size());
        Assert.assertEquals(Locale.FRANCE, returnedList.get(0).getLocale());
    }
}