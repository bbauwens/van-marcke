package com.vanmarcke.constants;

/**
 * Global class for all Ybackoffice constants. You can add global constants for your extension into this class.
 */
public final class VanmarckebackofficeConstants extends GeneratedVanmarckebackofficeConstants {
    public static final String EXTENSIONNAME = "vanmarckebackoffice";

    public static final String APPROVAL_GROUP = "approvalgroup";

    private VanmarckebackofficeConstants() {
        //empty to avoid instantiating this constant class
    }
}