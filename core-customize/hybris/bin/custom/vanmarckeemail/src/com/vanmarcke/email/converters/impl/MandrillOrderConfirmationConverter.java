package com.vanmarcke.email.converters.impl;

import com.elision.hybris.l10n.facades.LocalizedMessageFacade;
import com.microtripit.mandrillapp.lutung.view.MandrillMessage.MergeVar;
import com.microtripit.mandrillapp.lutung.view.MandrillMessage.MergeVarBucket;
import com.vanmarcke.email.converters.MandrillAbstractOrderConverter;
import com.vanmarcke.email.dto.AddressDTO;
import com.vanmarcke.email.dto.OrderDTO;
import de.hybris.platform.b2bacceleratorservices.enums.CheckoutPaymentType;
import de.hybris.platform.commerceservices.model.PickUpDeliveryModeModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.i18n.I18NService;
import org.apache.commons.lang3.time.FastDateFormat;

import java.util.Collections;
import java.util.List;

import static org.apache.commons.lang3.StringUtils.trimToEmpty;

/**
 * Converts orders to a DTO which can be send to the Mandrill mail service.
 *
 * @author Niels Raemaekers, Tom van den Berg
 * @since 22/01/2019
 */
public class MandrillOrderConfirmationConverter extends MandrillAbstractOrderConverter<OrderModel> {

    private static final String ORDER_VAR_NAME = "order";
    private static final String ORDER_DATE_FORMAT = "dd-MM-yyyy HH:mm";
    private static final String PAYMENT_METHOD_MESSAGE_KEY = "email.paymentmethod.%s.description";
    private static final String CREDIT_WARNING_MESSAGE_KEY = "text.checkout.payment.credit.warning";

    /**
     * Provides an instance of the MandrillOrderConfirmationConverter.
     *
     * @param messageFacade the message facade
     */
    public MandrillOrderConfirmationConverter(LocalizedMessageFacade messageFacade, I18NService i18NService) {
        super(messageFacade, i18NService);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<MergeVarBucket> convert(OrderModel order) {
        OrderDTO orderDTO = new OrderDTO();

        super.populate(order, orderDTO);

        populateOrderInformation(order, orderDTO);
        populateDates(order, orderDTO);
        populatePaymentInfo(order, orderDTO);
        populateAddresses(order, orderDTO);
        populateCreditLimitWarning(order, orderDTO);

        orderDTO.setSuborders(getSuborders(order));

        return createMergeVarBucket(orderDTO);
    }

    private void populateCreditLimitWarning(OrderModel order, OrderDTO orderDTO) {
        if (order.getCreditLimitWarning() && CheckoutPaymentType.ACCOUNT.equals(order.getPaymentType())) {
            orderDTO.setCredit_limit_warning(messageFacade.getMessageForCodeAndLocale(CREDIT_WARNING_MESSAGE_KEY, getCurrentLocale(order)));
        }

    }

    /**
     * Populates order information on the order DTO
     *
     * @param order    the order model
     * @param orderDTO the order DTO
     */
    private void populateOrderInformation(OrderModel order, OrderDTO orderDTO) {
        orderDTO.setCustomer_id(getB2BUnitId(order.getUser()));

        orderDTO.setYard_reference(trimToEmpty(order.getYardReference()));
        orderDTO.setTransport_information(trimToEmpty(order.getDeliveryComment()));

        if (order.getContactPerson() != null) {
            orderDTO.setContact_person_email(order.getContactPerson().getEmail());
            orderDTO.setContact_person_telephone(order.getContactPerson().getTelephone());
        }
    }


    /**
     * Populates payment info on the order DTO.
     *
     * @param order    the order model
     * @param orderDTO the order DTO
     */
    private void populatePaymentInfo(OrderModel order, OrderDTO orderDTO) {
        if (order.getPaymentType() != null && order.getLocale() != null) {
            String messageKey = String.format(PAYMENT_METHOD_MESSAGE_KEY, order.getPaymentType().getCode());
            orderDTO.setPayment_method(messageFacade.getMessageForCodeAndLocale(messageKey, getCurrentLocale(order)));
        }
    }

    /**
     * Populates delivery and order dates on the order DTO.
     *
     * @param order    the order model
     * @param orderDTO the order DTO
     */
    private void populateDates(OrderModel order, OrderDTO orderDTO) {
        orderDTO.setOrder_date(FastDateFormat.getInstance(ORDER_DATE_FORMAT).format(order.getDate()));
    }

    /**
     * Populates addresses on the order DTO.
     *
     * @param order    the order model
     * @param orderDTO the order DTO
     */
    private void populateAddresses(OrderModel order, OrderDTO orderDTO) {
        orderDTO.setPayment_address(getPaymentAddress(order));
        orderDTO.setShipment_address(getDeliveryAddress(order));
    }

    /**
     * Returns the delivery address of the order.
     *
     * @param order the order
     * @return the delivery address
     */
    private AddressDTO getDeliveryAddress(OrderModel order) {
        if (order.getDeliveryMode() instanceof PickUpDeliveryModeModel) {
            return populatePointOfServiceAddress(order);
        }

        return populateShipmentAddress(order);
    }


    /**
     * Returns the point of service address.
     *
     * @param order the order
     * @return the point of service address dto
     */
    private AddressDTO populatePointOfServiceAddress(OrderModel order) {
        AddressDTO addressDTO = convertAddressToDTO(order.getDeliveryPointOfService().getAddress(), getCurrentLocale(order));
        addressDTO.setName("TEC " + order.getDeliveryPointOfService().getDisplayName());
        return addressDTO;
    }

    /**
     * Returns the payment address of the order.
     *
     * @param order the order
     * @return the payment address
     */
    private AddressDTO getPaymentAddress(OrderModel order) {
        AddressDTO addressDTO = convertAddressToDTO(order.getPaymentAddress(), getCurrentLocale(order));
        setAddressName(order.getPaymentAddress(), addressDTO);
        return addressDTO;
    }

    /**
     * Returns a list of {@link MergeVarBucket} instances based on the given Order DTO.
     *
     * @param orderDTO the order DTO
     * @return a list of MergeVarBuckets
     */
    private List<MergeVarBucket> createMergeVarBucket(OrderDTO orderDTO) {
        MergeVar orderVar = new MergeVar();
        orderVar.setName(ORDER_VAR_NAME);
        orderVar.setContent(orderDTO);
        MergeVarBucket orderBucket = new MergeVarBucket();
        orderBucket.setVars(new MergeVar[]{orderVar});
        return Collections.singletonList(orderBucket);
    }
}
