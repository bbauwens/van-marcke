package com.vanmarcke.email.converters.impl;

import be.elision.mandrillextension.converter.MandrillConverter;
import com.microtripit.mandrillapp.lutung.view.MandrillMessage;
import com.vanmarcke.core.model.CustomerInteractionModel;
import com.vanmarcke.core.model.IBMOrderModel;
import com.vanmarcke.core.model.OrderCustomerInteractionModel;
import com.vanmarcke.email.dto.CustomerInteractionDTO;
import com.vanmarcke.email.dto.SuborderDTO;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.UserModel;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Converter for Customer Interactions
 */
public class MandrillCustomerInteractionConverter implements MandrillConverter<CustomerInteractionModel> {

    private static final String CUSTOMER_INTERACTION_VAR_NAME = "ci";

    @Override
    public List<MandrillMessage.MergeVarBucket> convert(final CustomerInteractionModel customerInteraction) {
        final CustomerInteractionDTO customerInteractionDTO = new CustomerInteractionDTO();

        customerInteractionDTO.setMessage(customerInteraction.getMessage());

        if (customerInteraction instanceof OrderCustomerInteractionModel) {
            final OrderCustomerInteractionModel orderCustomerInteraction = (OrderCustomerInteractionModel) customerInteraction;
            customerInteractionDTO.setCustomer_name(orderCustomerInteraction.getOrder().getUser().getName());
            customerInteractionDTO.setCustomer_id(getB2BUnitId(orderCustomerInteraction.getOrder().getUser()));
            customerInteractionDTO.setSuborders(getSuborders(orderCustomerInteraction.getOrder()));
        }

        //Create MergeVarBucket to return
        final MandrillMessage.MergeVar orderVar = new MandrillMessage.MergeVar();
        orderVar.setName(CUSTOMER_INTERACTION_VAR_NAME);
        orderVar.setContent(customerInteractionDTO);
        final MandrillMessage.MergeVarBucket orderBucket = new MandrillMessage.MergeVarBucket();
        orderBucket.setVars(new MandrillMessage.MergeVar[]{orderVar});
        return Collections.singletonList(orderBucket);
    }

    /**
     * Retrieves the B2B Unit UID.
     *
     * @param customer the customer
     * @return the B2B Unit UID
     */
    private String getB2BUnitId(UserModel customer) {
        if (customer instanceof B2BCustomerModel) {
            B2BCustomerModel b2bCustomer = (B2BCustomerModel) customer;
            if (b2bCustomer.getDefaultB2BUnit() != null) {
                return b2bCustomer.getDefaultB2BUnit().getUid();
            }
        }
        return null;
    }

    /**
     * Returns the suborder DTOs for the given {@code order}.
     *
     * @param order the order model
     * @return the suborder DTOs
     */
    private List<SuborderDTO> getSuborders(final OrderModel order) {
        return order.getIbmOrders()
                .stream()
                .map(this::convert)
                .collect(Collectors.toList());
    }

    /**
     * Converts the given {@code suborderModel} to the a {@link SuborderDTO} instance.
     * <p>
     * In this case only the order's unique identifier is required.
     *
     * @param suborderModel the suborder model
     * @return the suborder DTO
     */
    private SuborderDTO convert(final IBMOrderModel suborderModel) {
        final SuborderDTO suborderDTO = new SuborderDTO();
        suborderDTO.setCode(suborderModel.getCode());
        return suborderDTO;
    }
}
