package com.vanmarcke.email.converters.impl;

import com.elision.hybris.l10n.facades.LocalizedMessageFacade;
import com.microtripit.mandrillapp.lutung.view.MandrillMessage.MergeVar;
import com.microtripit.mandrillapp.lutung.view.MandrillMessage.MergeVarBucket;
import com.vanmarcke.core.model.VanmarckeContactModel;
import com.vanmarcke.email.dto.AddressDTO;
import com.vanmarcke.email.dto.OrderDTO;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2bacceleratorservices.enums.CheckoutPaymentType;
import de.hybris.platform.commerceservices.model.PickUpDeliveryModeModel;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import org.apache.commons.lang.RandomStringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.*;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class MandrillOrderConfirmationConverterTest {

    private static final String B2B_UNIT_ID = RandomStringUtils.randomAlphabetic(10);
    private static final String ORDER_CODE = RandomStringUtils.randomAlphabetic(10);
    private static final String USER_NAME = RandomStringUtils.randomAlphabetic(10);
    private static final String LOCALE = "en";
    private static final String EURO_SYMBOL = "€";
    private static final String YARD_REFERENCE = RandomStringUtils.randomAlphabetic(10);
    private static final String TRANSPORT_INFORMATION = RandomStringUtils.randomAlphabetic(10);
    private static final String POS_DISPLAY_NAME = RandomStringUtils.randomAlphabetic(10);
    private static final String CONTACT_PERSON_EMAIL = "return@vanmarcke.com";
    private static final String CONTACT_PERSON_TELEPHONE = "012.34.56.78";
    private static final String PAYMENT_INFO_DESCRIPTION = RandomStringUtils.randomAlphabetic(10);
    private static final String PAYMENT_METHOD_MESSAGE_KEY = String.format("email.paymentmethod.%s.description", "ACCOUNT");
    private static final String PRODUCT_SUMMARY = RandomStringUtils.randomAlphabetic(10);
    private static final String COMPANY_NAME = RandomStringUtils.randomAlphabetic(10);
    private static final String WARNING_MESSAGE = RandomStringUtils.randomAlphabetic(10);
    private static final String ADDRESS_LINE1 = RandomStringUtils.randomAlphabetic(10);
    private static final String ADDRESS_LINE2 = RandomStringUtils.randomAlphabetic(10);
    private static final String ADDRESS_TOWN = RandomStringUtils.randomAlphabetic(10);
    private static final String ADDRESS_FIRST_NAME = RandomStringUtils.randomAlphabetic(10);
    private static final String ADDRESS_LAST_NAME = RandomStringUtils.randomAlphabetic(10);
    private static final String ADDRESS_POSTAL_CODE = "3500";
    private static final String ADDRESS_TELEPHONE = "012 15 15 15";
    private static final String ADDRESS_MOBILE = "0488 12 34 56";
    private static final String ADDRESS_COUNTRY = "België";
    private static final String ADDRESS_COMPANY = RandomStringUtils.randomAlphabetic(10);
    private static final String ADDRESS_COMPANY_NAME = RandomStringUtils.randomAlphabetic(10);
    private static final String MERGE_VAR_NAME = "order";
    private static final double DELIVERY_COST = 7.50;
    private static final long QUANTITY = 1L;
    private static final double BASE_PRICE = 79.00;
    private static final double TOTAL_PRICE = 79.00;
    private static final double TOTAL_TAX = 21.00;
    private static final double SUB_TOTAL = 79.00;
    private static final String SKU = RandomStringUtils.randomAlphabetic(10);
    private static final String VANMARCKE_COMPANY_NAME = "vanmarcke.company.name";
    private static final String CREDIT_WARNING_MESSAGE_KEY = "text.checkout.payment.credit.warning";
    private static final Date date = Date.from(new Calendar.Builder()
            .setDate(2001, 3, 3)
            .setTimeOfDay(14, 50, 0).build().toInstant());
    private static final String DELIVERY_DATE = "03-04-2001";
    private static final String ORDER_DATE = "03-04-2001 14:50";

    private OrderModel order;

    @Mock
    private LocalizedMessageFacade messageFacade;

    @Mock
    private I18NService i18NService;

    @InjectMocks
    private MandrillOrderConfirmationConverter mandrillOrderConverter;

    @Before
    public void setUp() {
        order = mock(OrderModel.class);
        B2BCustomerModel user = mock(B2BCustomerModel.class);
        CurrencyModel currency = mock(CurrencyModel.class);
        OrderEntryModel orderEntry = mock(OrderEntryModel.class);
        ProductModel product = mock(ProductModel.class);
        AddressModel addressModel = mock(AddressModel.class);
        CountryModel country = mock(CountryModel.class);
        B2BUnitModel company = mock(B2BUnitModel.class);
        VanmarckeContactModel contactPerson = mock(VanmarckeContactModel.class);
        PointOfServiceModel pos = mock(PointOfServiceModel.class);

        when(order.getUser()).thenReturn(user);
        when(order.getExternalCode()).thenReturn(ORDER_CODE);
        when(user.getName()).thenReturn(USER_NAME);
        when(user.getDefaultB2BUnit()).thenReturn(company);
        when(company.getUid()).thenReturn(B2B_UNIT_ID);
        when(order.getCurrency()).thenReturn(currency);
        when(order.getPaymentType()).thenReturn(CheckoutPaymentType.ACCOUNT);
        when(order.getLocale()).thenReturn(LOCALE);
        when(order.getContactPerson()).thenReturn(contactPerson);
        when(contactPerson.getEmail()).thenReturn(CONTACT_PERSON_EMAIL);
        when(contactPerson.getTelephone()).thenReturn(CONTACT_PERSON_TELEPHONE);
        when(messageFacade.getMessageForCodeAndLocale(PAYMENT_METHOD_MESSAGE_KEY, Locale.ENGLISH)).thenReturn(PAYMENT_INFO_DESCRIPTION);
        when(messageFacade.getMessageForCodeAndLocale(VANMARCKE_COMPANY_NAME, Locale.ENGLISH)).thenReturn(COMPANY_NAME);
        when(messageFacade.getMessageForCodeAndLocale(CREDIT_WARNING_MESSAGE_KEY, Locale.ENGLISH)).thenReturn(WARNING_MESSAGE);
        when(order.getCreditLimitWarning()).thenReturn(true);

        when(currency.getSymbol()).thenReturn(EURO_SYMBOL);
        when(currency.getDigits()).thenReturn(2);
        when(order.getDeliveryCost()).thenReturn(DELIVERY_COST);
        when(order.getTotalTax()).thenReturn(TOTAL_TAX);
        when(order.getSubtotal()).thenReturn(SUB_TOTAL);
        when(order.getTotalPrice()).thenReturn(TOTAL_PRICE);
        when(order.getYardReference()).thenReturn(YARD_REFERENCE);
        when(order.getDeliveryComment()).thenReturn(TRANSPORT_INFORMATION);
        when(order.getDeliveryDate()).thenReturn(date);
        when(order.getDate()).thenReturn(date);

        when(order.getDeliveryPointOfService()).thenReturn(pos);
        when(pos.getAddress()).thenReturn(addressModel);
        when(pos.getDisplayName()).thenReturn(POS_DISPLAY_NAME);

        when(order.getEntries()).thenReturn(Collections.singletonList(orderEntry));
        when(orderEntry.getProduct()).thenReturn(product);
        when(orderEntry.getOrder()).thenReturn(order);
        when(orderEntry.getQuantity()).thenReturn(QUANTITY);
        when(orderEntry.getBasePrice()).thenReturn(BASE_PRICE);
        when(orderEntry.getTotalPrice()).thenReturn(TOTAL_PRICE);
        when(product.getSummary(Locale.ENGLISH)).thenReturn(PRODUCT_SUMMARY);
        when(product.getCode()).thenReturn(SKU);

        when(order.getPaymentAddress()).thenReturn(addressModel);
        when(order.getDeliveryAddress()).thenReturn(addressModel);
        when(addressModel.getLine1()).thenReturn(ADDRESS_LINE1);
        when(addressModel.getLine2()).thenReturn(ADDRESS_LINE2);
        when(addressModel.getTown()).thenReturn(ADDRESS_TOWN);
        when(addressModel.getFirstname()).thenReturn(ADDRESS_FIRST_NAME);
        when(addressModel.getLastname()).thenReturn(ADDRESS_LAST_NAME);
        when(addressModel.getPostalcode()).thenReturn(ADDRESS_POSTAL_CODE);
        when(addressModel.getPhone1()).thenReturn(ADDRESS_TELEPHONE);
        when(addressModel.getPhone2()).thenReturn(ADDRESS_MOBILE);
        when(addressModel.getCountry()).thenReturn(country);
        when(country.getName(Locale.ENGLISH)).thenReturn(ADDRESS_COUNTRY);
        when(addressModel.getCompany()).thenReturn(ADDRESS_COMPANY);
        when(addressModel.getOwner()).thenReturn(order);
        when(order.getUnit()).thenReturn(company);
        when(company.getName()).thenReturn(ADDRESS_COMPANY_NAME);
    }

    @Test
    public void testCreateMergeVarBucket() {
        List<MergeVarBucket> result = mandrillOrderConverter.convert(order);

        assertThat(result).isNotEmpty();
        MergeVarBucket resultEntry = result.get(0);
        MergeVar[] vars = resultEntry.getVars();
        assertThat(vars).isNotEmpty();
        MergeVar var = vars[0];
        assertThat(var.getName()).isEqualTo(MERGE_VAR_NAME);

    }

    @Test
    public void testPopulateOrderInformation() {
        OrderDTO result = (OrderDTO) mandrillOrderConverter.convert(order).get(0).getVars()[0].getContent();

        assertThat(result.getCustomer_id()).isEqualTo(B2B_UNIT_ID);
        assertThat(result.getYard_reference()).isEqualTo(YARD_REFERENCE);
        assertThat(result.getTransport_information()).isEqualTo(TRANSPORT_INFORMATION);
        assertThat(result.getContact_person_telephone()).isEqualTo(CONTACT_PERSON_TELEPHONE);
        assertThat(result.getContact_person_email()).isEqualTo(CONTACT_PERSON_EMAIL);
    }

    @Test
    public void testPopulatePaymentInfo() {
        OrderDTO result = (OrderDTO) mandrillOrderConverter.convert(order).get(0).getVars()[0].getContent();

        assertThat(result.getPayment_method()).isEqualTo(PAYMENT_INFO_DESCRIPTION);
    }

    @Test
    public void testPopulateCreditLimitWarning() {
        OrderDTO result = (OrderDTO) mandrillOrderConverter.convert(order).get(0).getVars()[0].getContent();

        assertThat(result.getCredit_limit_warning()).isEqualTo(WARNING_MESSAGE);
    }

    @Test
    public void testPopulateDates() {
        OrderDTO result = (OrderDTO) mandrillOrderConverter.convert(order).get(0).getVars()[0].getContent();

        assertThat(result.getOrder_date()).isEqualTo(ORDER_DATE);
    }

    @Test
    public void testGetDeliveryAddress_pickup() {
        PickUpDeliveryModeModel pickupMode = mock(PickUpDeliveryModeModel.class);
        when(order.getDeliveryMode()).thenReturn(pickupMode);

        OrderDTO result = (OrderDTO) mandrillOrderConverter.convert(order).get(0).getVars()[0].getContent();

        AddressDTO addressDTO = result.getShipment_address();
        assertThat(addressDTO.getAddress1()).isEqualTo(ADDRESS_LINE1);
        assertThat(addressDTO.getAddress2()).isEqualTo(ADDRESS_LINE2);
        assertThat(addressDTO.getCity()).isEqualTo(ADDRESS_TOWN);
        assertThat(addressDTO.getPostal_code()).isEqualTo(ADDRESS_POSTAL_CODE);
        assertThat(addressDTO.getTelephone()).isEqualTo(ADDRESS_TELEPHONE);
        assertThat(addressDTO.getMobile()).isEqualTo(ADDRESS_MOBILE);
        assertThat(addressDTO.getCountry()).isEqualTo(ADDRESS_COUNTRY);
        assertThat(addressDTO.getName()).isEqualTo("TEC " + POS_DISPLAY_NAME);
    }

    @Test
    public void testGetDeliveryAddress_shipping() {
        OrderDTO result = (OrderDTO) mandrillOrderConverter.convert(order).get(0).getVars()[0].getContent();

        AddressDTO addressDTO = result.getShipment_address();
        assertThat(addressDTO.getAddress1()).isEqualTo(ADDRESS_LINE1);
        assertThat(addressDTO.getAddress2()).isEqualTo(ADDRESS_LINE2);
        assertThat(addressDTO.getCity()).isEqualTo(ADDRESS_TOWN);
        assertThat(addressDTO.getPostal_code()).isEqualTo(ADDRESS_POSTAL_CODE);
        assertThat(addressDTO.getTelephone()).isEqualTo(ADDRESS_TELEPHONE);
        assertThat(addressDTO.getMobile()).isEqualTo(ADDRESS_MOBILE);
        assertThat(addressDTO.getCountry()).isEqualTo(ADDRESS_COUNTRY);
        assertThat(addressDTO.getName()).isEqualTo(ADDRESS_FIRST_NAME + " " + ADDRESS_LAST_NAME);
    }
}