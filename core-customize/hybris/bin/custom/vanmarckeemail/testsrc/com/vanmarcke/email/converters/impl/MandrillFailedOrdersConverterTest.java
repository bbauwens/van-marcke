package com.vanmarcke.email.converters.impl;

import com.microtripit.mandrillapp.lutung.view.MandrillMessage;
import com.vanmarcke.email.dto.FailedOrderDTO;
import com.vanmarcke.email.dto.FailedOrdersDTO;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.model.order.OrderModel;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.*;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class MandrillFailedOrdersConverterTest {

    private static final String B2B_UNIT_ID = RandomStringUtils.randomAlphabetic(10);

    @InjectMocks
    private MandrillFailedOrdersConverter mandrillFailedOrdersConverter;

    @Test
    public void testConvert() {
        OrderModel failedOrder = mock(OrderModel.class);
        B2BCustomerModel user = mock(B2BCustomerModel.class);
        B2BUnitModel b2bUnit = mock(B2BUnitModel.class);
        Date date = new GregorianCalendar(2020, Calendar.MAY, 20, 10, 0, 0).getTime();

        when(failedOrder.getExternalCode()).thenReturn("0001");
        when(failedOrder.getUser()).thenReturn(user);
        when(failedOrder.getDate()).thenReturn(date);
        when(user.getDefaultB2BUnit()).thenReturn(b2bUnit);
        when(b2bUnit.getUid()).thenReturn(B2B_UNIT_ID);
        List<MandrillMessage.MergeVarBucket> result = mandrillFailedOrdersConverter.convert(Collections.singletonList(failedOrder));

        assertThat(result).isNotEmpty();
        final MandrillMessage.MergeVarBucket resultEntry = result.get(0);
        final MandrillMessage.MergeVar[] vars = resultEntry.getVars();
        assertThat(vars).isNotEmpty();
        final MandrillMessage.MergeVar var = vars[0];
        assertThat(var.getName()).isEqualTo("orders");

        final FailedOrdersDTO failedOrdersDTO = (FailedOrdersDTO) var.getContent();
        assertThat(failedOrdersDTO.getFailed_orders()).isNotEmpty();
        FailedOrderDTO failedOrderDTO = failedOrdersDTO.getFailed_orders().get(0);
        assertThat(failedOrderDTO.getOrder_number()).isEqualTo("0001");
        assertThat(failedOrderDTO.getCustomer_id()).isEqualTo(B2B_UNIT_ID);
        assertThat(failedOrderDTO.getOrder_date()).isEqualTo("20-05-2020 10:00");
    }
}