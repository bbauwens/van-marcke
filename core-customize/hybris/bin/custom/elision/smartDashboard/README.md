# Smart Dashboard Extension

This extension adds functionality to hybris for filtering in the PCM.

To use this, you can just add this extension to your project. 
Additional configuration with impexes might be useful. 

Take a look at the 'initial-solr-configuration.impex' for an example. 
You probably want additional types/fields in the constraints, to use more project specific setup/configuration of the constraints.
