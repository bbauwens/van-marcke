package eu.elision.smartDashboard.solrfacetsearch.provider;

import de.hybris.platform.catalog.CatalogService;
import de.hybris.platform.catalog.model.CatalogModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.provider.FacetValueDisplayNameProvider;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import org.apache.commons.lang.LocaleUtils;
import org.springframework.beans.factory.annotation.Required;

import java.util.Locale;

import static eu.elision.smartDashboard.constants.SmartDashboardConstants.Facets.SEPARATOR;
import static eu.elision.smartDashboard.constants.SmartDashboardConstants.Facets.UNLOCALIZED_STRING;

/**
 * Facet Search Value provider for the Classification System names
 *
 * @author vangeda
 */
public class PcmClassificationSystemFacetDisplayNameProvider implements FacetValueDisplayNameProvider {
    private CatalogService catalogService;
    private static final String CATALOGVERSION_SEPARATOR = "_";

    /**
     * {@inheritDoc}
     */
    @Override
    public String getDisplayName(SearchQuery searchQuery, IndexedProperty indexedProperty, String name) {
        if (name != null) {
            String[] splitValue = name.split(SEPARATOR);
            String language = splitValue[1];
            if (language.equals(UNLOCALIZED_STRING)) {
                return splitValue[0];
            } else {
                //name contains both, catalog AND catalogVersion, split this...
                String[] catalogInfo = splitValue[0].split(CATALOGVERSION_SEPARATOR);
                try {
                    CatalogModel catalog = catalogService.getCatalogForId(catalogInfo[0]);
                    Locale locale = LocaleUtils.toLocale(language);
                    String localizedName = catalog.getName(locale);
                    if (!UNLOCALIZED_STRING.equalsIgnoreCase(splitValue[1])) {
                        localizedName = localizedName + " [" + splitValue[1] + "]";
                    }
                    return localizedName;
                } catch (UnknownIdentifierException e) {
                    // catalog with this name cannot be found...
                    return splitValue[0];
                }
            }
        }
        return null;
    }

    @Required
    public void setCatalogService(CatalogService catalogService) {
        this.catalogService = catalogService;
    }
}
