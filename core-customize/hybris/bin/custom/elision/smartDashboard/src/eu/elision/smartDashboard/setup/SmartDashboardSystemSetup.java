package eu.elision.smartDashboard.setup;

import de.hybris.platform.commerceservices.setup.AbstractSystemSetup;
import de.hybris.platform.core.initialization.SystemSetup;
import de.hybris.platform.core.initialization.SystemSetupContext;
import de.hybris.platform.core.initialization.SystemSetupParameter;
import de.hybris.platform.core.initialization.SystemSetupParameterMethod;
import eu.elision.smartDashboard.constants.SmartDashboardConstants;

import java.util.Collections;
import java.util.List;

/**
 * System Setup class to setup SmartDashBoard extension
 *
 * @author vangeda
 */
@SystemSetup(extension = SmartDashboardConstants.EXTENSIONNAME)
public class SmartDashboardSystemSetup extends AbstractSystemSetup {
    private static final String INITIAL_SOLR_CONFIG = "importInitialSolrConfiguration";

    /**
     * Essential Data
     *
     * @param context systemSetupContext object
     */
    @SystemSetup(process = SystemSetup.Process.ALL, type = SystemSetup.Type.ESSENTIAL)
    public void createEssentialData(final SystemSetupContext context) {
        if (getBooleanSystemSetupParameter(context, INITIAL_SOLR_CONFIG)) {
            importImpexFile(context, "/impex/initial-solr-configuration.impex", true);
        }
    }

    /**
     * Options used during initialization/update system.
     *
     * @return List of SystemSetup Parameters
     */
    @Override
    @SystemSetupParameterMethod
    public List<SystemSetupParameter> getInitializationOptions() {
        return Collections.singletonList(
                createBooleanSystemSetupParameter(INITIAL_SOLR_CONFIG, "Import Default Solr Setup for Data Quality Constraints", false)
        );
    }

}
