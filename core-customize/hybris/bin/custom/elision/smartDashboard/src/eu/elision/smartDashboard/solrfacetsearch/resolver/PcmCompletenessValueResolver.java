package eu.elision.smartDashboard.solrfacetsearch.resolver;

import de.hybris.platform.classification.ClassificationService;
import de.hybris.platform.classification.features.FeatureList;
import de.hybris.platform.classification.features.LocalizedFeature;
import de.hybris.platform.cockpit.model.CoverageConstraintGroupModel;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.type.ComposedTypeModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.i18n.impl.DefaultI18NService;
import de.hybris.platform.servicelayer.type.TypeService;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolver;
import de.hybris.platform.validation.coverage.daos.CoverageConstraintGroupDao;
import de.hybris.platform.validation.exceptions.HybrisConstraintViolation;
import de.hybris.platform.validation.model.constraints.AttributeConstraintModel;
import de.hybris.platform.validation.model.constraints.ConstraintGroupModel;
import de.hybris.platform.validation.services.ValidationService;
import de.hybris.platform.validation.services.impl.LocalizedHybrisConstraintViolation;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import java.util.*;

import static eu.elision.smartDashboard.constants.SmartDashboardConstants.ConfigurationProperties.INDEX_ONLY_MANDATORY_CLASS_ATTRIBUTES;
import static eu.elision.smartDashboard.constants.SmartDashboardConstants.Facets.*;

/**
 * Resolver used during indexing to provide the data quality coverage results.
 *
 * @author vangeda
 */
public class PcmCompletenessValueResolver extends AbstractValueResolver<ProductModel, Object, Object> {

    //TODO review this entire class, not sure if we want/need all of this...
    //TODO javadoc & testing

    private ConfigurationService configurationService;
    private ValidationService validationService;
    private TypeService typeService;
    private CoverageConstraintGroupDao coverageConstraintGroupDao;
    private String[] constraintGroups;

    private ClassificationService classificationService;
    private DefaultI18NService i18nService;

    /**
     * {@inheritDoc}
     */
    @Override
    protected void addFieldValues(InputDocument inputDocument, IndexerBatchContext indexerBatchContext, IndexedProperty indexedProperty, ProductModel productModel,
                                  ValueResolverContext<Object, Object> valueResolverContext) throws FieldValueProviderException {
        if (!indexedProperty.getName().equals(COMPLETENESS_PREFIX)) {
            return;
        }
        Set<String> localesViolated = new HashSet<>();
        Set<String> localesClassificationSystemViolated = new HashSet<>();
        Set<String> domainsViolated = new HashSet<>();
        Set<String> classificationSystemViolated = new HashSet<>();
        Set<String> classificationCategoryViolated = new HashSet<>();
        Set<String> attributesViolated = new HashSet<>();
        Set<String> featuresViolated = new HashSet<>();
        Set<String> uniqueAttributes = new HashSet<>();

        Set<Locale> supportedLocales = i18nService.getSupportedLocales();

        boolean indexOnlyMandatory = indexOnlyMandatory();

        FeatureList features = classificationService.getFeatures(productModel);

        long attributes = features.getFeatures().stream().filter(f -> !indexOnlyMandatory || f.getClassAttributeAssignment().getMandatory()).peek(feature -> {
            String domain =
                    feature.getClassAttributeAssignment().getSystemVersion().getCatalog().getId() + "_" + feature.getClassAttributeAssignment().getSystemVersion().getVersion();
            if (feature instanceof LocalizedFeature && feature.getClassAttributeAssignment().getLocalized()) {
                supportedLocales.forEach(locale -> {
                    if (((LocalizedFeature) feature).getValue(locale) == null) {
                        localesClassificationSystemViolated.add(locale.toString().concat(SEPARATOR));
                        classificationSystemViolated.add(domain.concat(SEPARATOR).concat(locale.toString()).concat(SEPARATOR));
                        classificationCategoryViolated.add(domain.concat(SEPARATOR).concat(locale.toString()).concat(SEPARATOR).concat(feature.getClassAttributeAssignment().getClassificationClass().getCode()).concat(SEPARATOR));
                        featuresViolated.add(domain.concat(SEPARATOR).concat(locale.toString()).concat(SEPARATOR).concat(feature.getClassAttributeAssignment().getClassificationClass().getCode()).concat(SEPARATOR).concat(feature.getCode()));
                        uniqueAttributes.add(feature.getCode());
                    }
                });
            } else if (feature.getValue() == null) {
                localesClassificationSystemViolated.add(UNLOCALIZED_LOCALE.toString().concat(SEPARATOR));
                classificationSystemViolated.add(domain.concat(SEPARATOR).concat(UNLOCALIZED_LOCALE.toString()).concat(SEPARATOR));
                classificationCategoryViolated.add(domain.concat(SEPARATOR).concat(UNLOCALIZED_LOCALE.toString()).concat(SEPARATOR).concat(feature.getClassAttributeAssignment().getClassificationClass().getCode()).concat(SEPARATOR));
                featuresViolated.add(domain.concat(SEPARATOR).concat(UNLOCALIZED_LOCALE.toString()).concat(SEPARATOR).concat(feature.getClassAttributeAssignment().getClassificationClass().getCode()).concat(SEPARATOR).concat(feature.getCode()));
                uniqueAttributes.add(feature.getCode());
            }
        }).count();

        for (String constraintGroup : constraintGroups) {
            ConstraintGroupModel constraintGroupModel = getConstraintGroup(productModel, constraintGroup);
            if (constraintGroupModel == null) {
                continue;
            }
            attributes += constraintGroupModel.getConstraints().stream().map(c -> {
                Set<LanguageModel> languages = new HashSet<>();
                if (c instanceof AttributeConstraintModel) {
                    languages = ((AttributeConstraintModel) c).getLanguages();
                }
                if (languages.isEmpty()) {
                    languages = new HashSet<>();
                    languages.add(new LanguageModel());
                }
                return languages;
            }).count();

            Set<HybrisConstraintViolation> violations = this.validationService.validate(productModel, Collections.singleton(constraintGroupModel));
            violations.forEach(violation -> {
                Locale locale = UNLOCALIZED_LOCALE;
                if (StringUtils.isNotBlank(violation.getConstraintViolation().getPropertyPath().toString())) {
                    if (violation instanceof LocalizedHybrisConstraintViolation) {
                        locale = ((LocalizedHybrisConstraintViolation) violation).getViolationLanguage();
                    }
                    localesViolated.add(locale.toString().concat(SEPARATOR));
                    domainsViolated.add(locale.toString().concat(SEPARATOR).concat(constraintGroup).concat(SEPARATOR));
                    attributesViolated.add(locale.toString().concat(SEPARATOR).concat(constraintGroup).concat(SEPARATOR).concat(violation.getConstraintViolation().getPropertyPath().toString()));
                    uniqueAttributes.add(violation.getConstraintViolation().getPropertyPath().toString());
                }
            });

        }

        double completenessPercentage = uniqueAttributes.size() * 100.0f / attributes;
        inputDocument.addField(PRODUCT_COMPLETENESS_FACET_SOLR_NAMESPACE, completenessPercentage);

        for (String classificationLocale : localesClassificationSystemViolated) {
            inputDocument.addField(indexedProperty, classificationLocale, LOCALE_CLASSIFICATION_SYSTEM_COMPLETENESS_SUFFIX);
        }
        for (String classificationSystem : classificationSystemViolated) {
            inputDocument.addField(indexedProperty, classificationSystem, CLASSIFICATION_SYSTEM_COMPLETENESS_SUFFIX);
        }
        for (String locale : localesViolated) {
            inputDocument.addField(indexedProperty, locale, LOCALE_COMPLETENESS_SUFFIX);
        }
        for (String classificationCategory : classificationCategoryViolated) {
            inputDocument.addField(indexedProperty, classificationCategory, CLASSIFICATION_CATEGORY_COMPLETENESS_SUFFIX);
        }
        for (String attribute : attributesViolated) {
            inputDocument.addField(indexedProperty, attribute, ATTRIBUTE_COMPLETENESS_SUFFIX);
        }
        for (String feature : featuresViolated) {
            inputDocument.addField(indexedProperty, feature, FEATURE_COMPLETENESS_SUFFIX);
        }
    }

    private boolean indexOnlyMandatory() {
        return getConfigurationService().getConfiguration().getBoolean(INDEX_ONLY_MANDATORY_CLASS_ATTRIBUTES, false);
    }

    @Required
    public void setValidationService(ValidationService validationService) {
        this.validationService = validationService;
    }

    private ConstraintGroupModel getConstraintGroup(ItemModel item, String constraintGroup) {
        ComposedTypeModel composedType = this.typeService.getComposedTypeForCode(item.getItemtype());
        Collection<CoverageConstraintGroupModel> groups = this.coverageConstraintGroupDao.findGroupsForDomainAndType(constraintGroup, composedType);
        if (CollectionUtils.isEmpty(groups)) {
            groups = this.coverageConstraintGroupDao.findGroupsForDefaultDomain();
        }
        return CollectionUtils.isEmpty(groups) ? null : groups.iterator().next();
    }

    @Required
    public void setTypeService(TypeService typeService) {
        this.typeService = typeService;
    }

    @Required
    public void setCoverageConstraintGroupDao(CoverageConstraintGroupDao coverageConstraintGroupDao) {
        this.coverageConstraintGroupDao = coverageConstraintGroupDao;
    }

    @Required
    public void setConstraintGroups(String[] constraintGroups) {
        this.constraintGroups = constraintGroups;
    }

    @Required
    public void setClassificationService(ClassificationService classificationService) {
        this.classificationService = classificationService;
    }

    @Required
    public void setI18nService(DefaultI18NService i18nService) {
        this.i18nService = i18nService;
    }

    public ConfigurationService getConfigurationService() {
        return configurationService;
    }

    @Required
    public void setConfigurationService(final ConfigurationService configurationService) {
        this.configurationService = configurationService;
    }
}
