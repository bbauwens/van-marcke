package eu.elision.smartDashboard.solrfacetsearch.provider;

import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.provider.FacetValueDisplayNameProvider;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import org.apache.commons.lang.LocaleUtils;

import java.util.Locale;

import static eu.elision.smartDashboard.constants.SmartDashboardConstants.Facets.SEPARATOR;
import static eu.elision.smartDashboard.constants.SmartDashboardConstants.Facets.UNLOCALIZED_STRING;

/**
 * Facet Search Value provider for the locales
 *
 * @author vangeda
 */
public class PcmLocaleCompletenessFacetDisplayNameProvider implements FacetValueDisplayNameProvider {

    /**
     * {@inheritDoc}
     */
    @Override
    public String getDisplayName(SearchQuery searchQuery, IndexedProperty indexedProperty, String name) {
        if (name != null) {
            String value = name.split(SEPARATOR)[0];
            if (value.equals(UNLOCALIZED_STRING)) {
                return "Unlocalized";
            } else {
                Locale locale = LocaleUtils.toLocale(value);
                Locale queryLocale = LocaleUtils.toLocale(searchQuery.getLanguage());
                return locale.getDisplayName(queryLocale);
            }
        }
        return null;
    }

}
