package eu.elision.smartDashboard.solrfacetsearch.provider;

import de.hybris.platform.catalog.model.classification.ClassificationSystemVersionModel;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.classification.ClassificationSystemService;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.provider.FacetValueDisplayNameProvider;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import org.apache.commons.lang.LocaleUtils;
import org.springframework.beans.factory.annotation.Required;

import static eu.elision.smartDashboard.constants.SmartDashboardConstants.Facets.SEPARATOR;
import static eu.elision.smartDashboard.constants.SmartDashboardConstants.Facets.UNLOCALIZED_STRING;

/**
 * Facet Search Value provider for the Classification Categegory names
 *
 * @author vangeda
 */
public class PcmClassificationCategoryFacetDisplayNameProvider implements FacetValueDisplayNameProvider {

    private CategoryService categoryService;
    private ClassificationSystemService classificationSystemService;

    /**
     * {@inheritDoc}
     */
    @Override
    public String getDisplayName(SearchQuery searchQuery, IndexedProperty indexedProperty, String name) {
        if (name != null) {
            String[] splitValue = name.split(SEPARATOR);
            String catalogCode = splitValue[0].split("_")[0];
            String catalogVersion = splitValue[0].split("_")[1];
            ClassificationSystemVersionModel classificationCatalog = classificationSystemService.getSystemVersion(catalogCode, catalogVersion);
            String code = splitValue[2];
            CategoryModel categoryModel = categoryService.getCategoryForCode(classificationCatalog, code);
            String localizedName = getDisplayName(categoryModel, searchQuery, code);
            if (!UNLOCALIZED_STRING.equalsIgnoreCase(splitValue[1])) {
                localizedName = localizedName + " [" + splitValue[1] + "]";
            }
            return localizedName;
        }
        return null;
    }

    /**
     * Get the name of the category for the language of the query. If the name is null, return the fallback.
     *
     * @param categoryModel The category
     * @param searchQuery   The query
     * @param fallback      The fallback value for when the name is null
     * @return The displayName
     */
    protected String getDisplayName(CategoryModel categoryModel, SearchQuery searchQuery, String fallback) {
        String categoryName = categoryModel.getName(LocaleUtils.toLocale(searchQuery.getLanguage()));
        return categoryName != null ? categoryName : fallback;
    }

    @Required
    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    public void setClassificationSystemService(ClassificationSystemService classificationSystemService) {
        this.classificationSystemService = classificationSystemService;
    }
}
