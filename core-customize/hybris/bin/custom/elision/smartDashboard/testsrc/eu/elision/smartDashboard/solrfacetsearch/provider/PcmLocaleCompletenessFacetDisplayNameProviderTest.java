package eu.elision.smartDashboard.solrfacetsearch.provider;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;

/**
 * @author vangeda
 */
@UnitTest
public class PcmLocaleCompletenessFacetDisplayNameProviderTest {

    @InjectMocks
    private PcmLocaleCompletenessFacetDisplayNameProvider provider;
    @Mock
    private FacetSearchConfig facetSearchConfig;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        provider = new PcmLocaleCompletenessFacetDisplayNameProvider();
    }

    @Test
    public void getDisplayName_NoName() {
        IndexedType indexedType = mock(IndexedType.class);
        SearchQuery searchQuery = spy(new SearchQuery(facetSearchConfig, indexedType));
        IndexedProperty property = new IndexedProperty();

        String result = provider.getDisplayName(searchQuery, property, null);

        assertThat(result).isNullOrEmpty();
    }

    @Test
    public void getDislayName_unlocalized() {
        String name = "unlocalized#";

        IndexedType indexedType = mock(IndexedType.class);
        SearchQuery searchQuery = spy(new SearchQuery(facetSearchConfig, indexedType));
        searchQuery.setLanguage("en");
        IndexedProperty property = new IndexedProperty();

        String result = provider.getDisplayName(searchQuery, property, name);

        assertThat(result).isNotEmpty().isEqualTo("Unlocalized");
    }

    @Test
    public void getDislayName_localized() {
        String name = "nl#";

        IndexedType indexedType = mock(IndexedType.class);
        SearchQuery searchQuery = spy(new SearchQuery(facetSearchConfig, indexedType));
        searchQuery.setLanguage("en");
        IndexedProperty property = new IndexedProperty();

        String result = provider.getDisplayName(searchQuery, property, name);

        assertThat(result).isNotEmpty().isEqualTo("Dutch");
    }

}