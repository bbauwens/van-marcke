package eu.elision.smartDashboard.solrfacetsearch.provider;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.classification.ClassificationSystemVersionModel;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.classification.ClassificationSystemService;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Locale;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;

/**
 * @author vangeda
 */
@UnitTest
public class PcmClassificationCategoryFacetDisplayNameProviderTest {

    @InjectMocks
    private PcmClassificationCategoryFacetDisplayNameProvider provider;

    @Mock
    private CategoryService categoryService;
    @Mock
    private FacetSearchConfig facetSearchConfig;
    @Mock
    private ClassificationSystemService classificationSystemService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        provider.setCategoryService(categoryService);
    }

    @Test
    public void getDisplayName_NoName() {
        IndexedType indexedType = mock(IndexedType.class);
        SearchQuery searchQuery = spy(new SearchQuery(facetSearchConfig, indexedType));
        IndexedProperty property = new IndexedProperty();

        String result = provider.getDisplayName(searchQuery, property, null);

        assertThat(result).isNullOrEmpty();
    }

    @Test
    public void getDisplayName_localizedAttribute() {
        String catalog = "buildingProductsDecoGS1ClassificationCatalog_1.0";
        String locale = "en";
        String attribute = "GS1_83000000_10002444_Deco_Flooring_Accessories";

        IndexedType indexedType = mock(IndexedType.class);
        SearchQuery searchQuery = spy(new SearchQuery(facetSearchConfig, indexedType));
        searchQuery.setLanguage("en");
        searchQuery.setCurrency("EUR");

        IndexedProperty property = new IndexedProperty();
        property.setName("completeness_attribute");
        property.setType("string");

        CategoryModel categoryModel = mock(CategoryModel.class);
        ClassificationSystemVersionModel classificationSystemVersionModel = mock(ClassificationSystemVersionModel.class);
        given(categoryModel.getName(Locale.ENGLISH)).willReturn("name");
        given(classificationSystemService.getSystemVersion("buildingProductsDecoGS1ClassificationCatalog", "1.0")).willReturn(classificationSystemVersionModel);
        given(categoryService.getCategoryForCode(classificationSystemVersionModel, attribute)).willReturn(categoryModel);

        String result = provider.getDisplayName(searchQuery, property, catalog + "#" + locale + "#" + attribute + "#");

        assertThat(result).isNotEmpty().isEqualTo("name [" + locale + "]");
    }

    @Test
    public void getDisplayName_localizedAttributeFallback() {
        String catalog = "buildingProductsDecoGS1ClassificationCatalog_1.0";
        String locale = "en";
        String attribute = "GS1_83000000_10002444_Deco_Flooring_Accessories";

        IndexedType indexedType = mock(IndexedType.class);
        SearchQuery searchQuery = spy(new SearchQuery(facetSearchConfig, indexedType));
        searchQuery.setLanguage("en");
        searchQuery.setCurrency("EUR");

        IndexedProperty property = new IndexedProperty();
        property.setName("completeness_attribute");
        property.setType("string");

        CategoryModel categoryModel = mock(CategoryModel.class);
        ClassificationSystemVersionModel classificationSystemVersionModel = mock(ClassificationSystemVersionModel.class);
        given(categoryModel.getName(any())).willReturn(null);
        given(classificationSystemService.getSystemVersion("buildingProductsDecoGS1ClassificationCatalog", "1.0")).willReturn(classificationSystemVersionModel);
        given(categoryService.getCategoryForCode(classificationSystemVersionModel, attribute)).willReturn(categoryModel);

        String result = provider.getDisplayName(searchQuery, property, catalog + "#" + locale + "#" + attribute + "#");

        assertThat(result).isNotEmpty().isEqualTo(attribute + " [" + locale + "]");
    }

    @Test
    public void getDisplayName_unlocalizedAttribute() {
        String catalog = "buildingProductsDecoGS1ClassificationCatalog_1.0";
        String locale = "unlocalized";
        String attribute = "GS1_83000000_10002444_Deco_Flooring_Accessories";

        IndexedType indexedType = mock(IndexedType.class);
        SearchQuery searchQuery = spy(new SearchQuery(facetSearchConfig, indexedType));
        searchQuery.setLanguage("en");
        searchQuery.setCurrency("EUR");

        IndexedProperty property = new IndexedProperty();
        property.setName("completeness_attribute");
        property.setType("string");

        CategoryModel categoryModel = mock(CategoryModel.class);
        ClassificationSystemVersionModel classificationSystemVersionModel = mock(ClassificationSystemVersionModel.class);
        given(categoryModel.getName(Locale.ENGLISH)).willReturn("name");
        given(classificationSystemService.getSystemVersion("buildingProductsDecoGS1ClassificationCatalog", "1.0")).willReturn(classificationSystemVersionModel);
        given(categoryService.getCategoryForCode(classificationSystemVersionModel, attribute)).willReturn(categoryModel);

        String result = provider.getDisplayName(searchQuery, property, catalog + "#" + locale + "#" + attribute + "#");

        assertThat(result).isNotEmpty().isEqualTo("name");
    }

    @Test
    public void getDisplayName_unlocalizedAttributeFallback() {
        String catalog = "buildingProductsDecoGS1ClassificationCatalog_1.0";
        String locale = "unlocalized";
        String attribute = "GS1_83000000_10002444_Deco_Flooring_Accessories";

        IndexedType indexedType = mock(IndexedType.class);
        SearchQuery searchQuery = spy(new SearchQuery(facetSearchConfig, indexedType));
        searchQuery.setLanguage("en");
        searchQuery.setCurrency("EUR");

        IndexedProperty property = new IndexedProperty();
        property.setName("completeness_attribute");
        property.setType("string");

        CategoryModel categoryModel = mock(CategoryModel.class);
        ClassificationSystemVersionModel classificationSystemVersionModel = mock(ClassificationSystemVersionModel.class);
        given(categoryModel.getName(any())).willReturn(null);
        given(classificationSystemService.getSystemVersion("buildingProductsDecoGS1ClassificationCatalog", "1.0")).willReturn(classificationSystemVersionModel);
        given(categoryService.getCategoryForCode(classificationSystemVersionModel, attribute)).willReturn(categoryModel);

        String result = provider.getDisplayName(searchQuery, property, catalog + "#" + locale + "#" + attribute + "#");

        assertThat(result).isNotEmpty().isEqualTo(attribute);
    }
}