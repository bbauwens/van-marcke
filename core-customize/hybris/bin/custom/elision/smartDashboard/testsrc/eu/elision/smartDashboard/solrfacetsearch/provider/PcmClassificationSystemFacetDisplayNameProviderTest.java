package eu.elision.smartDashboard.solrfacetsearch.provider;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.CatalogService;
import de.hybris.platform.catalog.model.CatalogModel;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Locale;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;

/**
 * @author vangeda
 */
@UnitTest
public class PcmClassificationSystemFacetDisplayNameProviderTest {

    @InjectMocks
    private PcmClassificationSystemFacetDisplayNameProvider provider;

    @Mock
    private FacetSearchConfig facetSearchConfig;
    @Mock
    private CatalogService catalogService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        provider.setCatalogService(catalogService);
    }

    @Test
    public void getDisplayName_NoName() {
        IndexedType indexedType = mock(IndexedType.class);
        SearchQuery searchQuery = spy(new SearchQuery(facetSearchConfig, indexedType));
        IndexedProperty property = new IndexedProperty();

        String result = provider.getDisplayName(searchQuery, property, null);

        assertThat(result).isNullOrEmpty();
    }

    @Test
    public void getDisplayName_localizedAttribute() {
        String catalog = "buildingProductsDecoGS1ClassificationCatalog_1.0";
        String locale = "en";
        String attribute = "GS1_83000000_10002444_Deco_Flooring_Accessories";
        IndexedType indexedType = mock(IndexedType.class);
        SearchQuery searchQuery = spy(new SearchQuery(facetSearchConfig, indexedType));
        searchQuery.setLanguage("en");
        searchQuery.setCurrency("EUR");

        IndexedProperty property = new IndexedProperty();
        property.setName("completeness_attribute");
        property.setType("string");

        CatalogModel catalogModel = mock(CatalogModel.class);
        given(catalogModel.getName(Locale.ENGLISH)).willReturn(catalog);
        given(catalogService.getCatalogForId(catalog.split("_")[0])).willReturn(catalogModel);

        String result = provider.getDisplayName(searchQuery, property, catalog + "#" + locale + "#" + attribute + "#");

        assertThat(result).isNotEmpty().isEqualTo(catalog + " [" + locale + "]");
    }

    @Test
    public void getDisplayName_unlocalizedAttribute() {
        String catalog = "buildingProductsDecoGS1ClassificationCatalog_1.0";
        String locale = "unlocalized";
        String attribute = "GS1_83000000_10002444_Deco_Flooring_Accessories";
        IndexedType indexedType = mock(IndexedType.class);
        SearchQuery searchQuery = spy(new SearchQuery(facetSearchConfig, indexedType));
        searchQuery.setLanguage("en");
        searchQuery.setCurrency("EUR");

        IndexedProperty property = new IndexedProperty();
        property.setName("completeness_attribute");
        property.setType("string");

        String result = provider.getDisplayName(searchQuery, property, catalog + "#" + locale + "#" + attribute + "#");

        assertThat(result).isNotEmpty().isEqualTo(catalog);
    }
}