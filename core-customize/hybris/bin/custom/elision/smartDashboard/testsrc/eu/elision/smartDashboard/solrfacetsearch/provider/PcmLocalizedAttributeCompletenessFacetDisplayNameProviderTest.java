package eu.elision.smartDashboard.solrfacetsearch.provider;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.type.AttributeDescriptorModel;
import de.hybris.platform.core.model.type.ComposedTypeModel;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.servicelayer.type.TypeService;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Locale;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;

/**
 * @author vangeda
 */
@UnitTest
public class PcmLocalizedAttributeCompletenessFacetDisplayNameProviderTest {

    @InjectMocks
    private PcmLocalizedAttributeCompletenessFacetDisplayNameProvider provider;

    @Mock
    private AttributeDescriptorModel attributeDescriptor;
    @Mock
    private FacetSearchConfig facetSearchConfig;
    @Mock
    private I18NService i18NService;
    @Mock
    private TypeService typeService;
    private ComposedTypeModel productComposedType;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        provider = new PcmLocalizedAttributeCompletenessFacetDisplayNameProvider();
        provider.setI18nService(i18NService);
        provider.setTypeService(typeService);
        productComposedType = mock(ComposedTypeModel.class);
    }

    @Test
    public void getDisplayName_localizedAttributeFromTypeService() {
        String locale = "en";
        String group = "pcmCoreAttributesCoverageGroup";
        String attribute = "materialDescription";

        IndexedType indexedType = mock(IndexedType.class);
        SearchQuery searchQuery = spy(new SearchQuery(facetSearchConfig, indexedType));
        searchQuery.setLanguage("en");
        searchQuery.setCurrency("EUR");

        IndexedProperty property = new IndexedProperty();
        property.setName("completeness_attribute");
        property.setType("string");
        given(searchQuery.getIndexedType().getComposedType()).willReturn(productComposedType);
        given(typeService.getAttributeDescriptor(productComposedType, attribute)).willReturn(attributeDescriptor);
        given(attributeDescriptor.getName(Locale.ENGLISH)).willReturn(attribute);

        String result = provider.getDisplayName(searchQuery, property, locale + "#" + group + "#" + attribute);

        assertThat(result).isNotEmpty().isEqualTo(attribute + " [" + locale + "]");
    }

    @Test
    public void getDisplayName_localizedAttributeNotFromTypeService() {
        String locale = "en";
        String group = "pcmCoreAttributesCoverageGroup";
        String attribute = "materialDescription";

        IndexedType indexedType = mock(IndexedType.class);
        SearchQuery searchQuery = spy(new SearchQuery(facetSearchConfig, indexedType));
        searchQuery.setLanguage("en");
        searchQuery.setCurrency("EUR");

        IndexedProperty property = new IndexedProperty();
        property.setName("completeness_attribute");
        property.setType("string");
        given(searchQuery.getIndexedType().getComposedType()).willReturn(productComposedType);
        given(typeService.getAttributeDescriptor(productComposedType, attribute)).willThrow(new NullPointerException());

        String result = provider.getDisplayName(searchQuery, property, locale + "#" + group + "#" + attribute);

        assertThat(result).isNotEmpty().isEqualTo(attribute + " [" + locale + "]");
    }

    @Test
    public void getDisplayName_unlocalizedAttributeNotFromTypeService() {
        String locale = "unlocalized";
        String group = "pcmCoreAttributesCoverageGroup";
        String attribute = "code";

        IndexedType indexedType = mock(IndexedType.class);
        SearchQuery searchQuery = spy(new SearchQuery(facetSearchConfig, indexedType));
        searchQuery.setLanguage("en");
        searchQuery.setCurrency("EUR");

        IndexedProperty property = new IndexedProperty();
        property.setName("completeness_attribute");
        property.setType("string");
        given(searchQuery.getIndexedType().getComposedType()).willReturn(productComposedType);
        given(typeService.getAttributeDescriptor(productComposedType, attribute)).willThrow(new NullPointerException());

        String result = provider.getDisplayName(searchQuery, property, locale + "#" + group + "#" + attribute);

        assertThat(result).isNotEmpty().isEqualTo(attribute);
    }

    @Test
    public void getDisplayName_NoName() {
        IndexedType indexedType = mock(IndexedType.class);
        SearchQuery searchQuery = spy(new SearchQuery(facetSearchConfig, indexedType));
        IndexedProperty property = new IndexedProperty();

        String result = provider.getDisplayName(searchQuery, property, null);

        assertThat(result).isNullOrEmpty();
    }
}