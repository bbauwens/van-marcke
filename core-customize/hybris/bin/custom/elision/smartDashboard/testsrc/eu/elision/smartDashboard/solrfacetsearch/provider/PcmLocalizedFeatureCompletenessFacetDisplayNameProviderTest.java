package eu.elision.smartDashboard.solrfacetsearch.provider;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeModel;
import de.hybris.platform.platformbackoffice.services.ClassificationAttributeAssignmentService;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Locale;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;

/**
 * @author vangeda
 */

@UnitTest
public class PcmLocalizedFeatureCompletenessFacetDisplayNameProviderTest {

    private static final String SEPARATOR = "#";

    @InjectMocks
    private PcmLocalizedFeatureCompletenessFacetDisplayNameProvider provider;
    @Mock
    private I18NService i18nService;
    @Mock
    private ClassificationAttributeAssignmentService classificationAttributeAssignmentService;
    @Mock
    private FacetSearchConfig facetSearchConfig;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        provider = new PcmLocalizedFeatureCompletenessFacetDisplayNameProvider();
        provider.setI18nService(i18nService);
        provider.setClassificationAttributeAssignmentService(classificationAttributeAssignmentService);

    }

    @Test
    public void getDisplayName_NoName() {
        IndexedType indexedType = mock(IndexedType.class);
        SearchQuery searchQuery = spy(new SearchQuery(facetSearchConfig, indexedType));
        IndexedProperty property = new IndexedProperty();

        String result = provider.getDisplayName(searchQuery, property, null);

        assertThat(result).isNullOrEmpty();
    }

    @Test
    public void getDisplayName_unlocalized() {
        String catalog = "buildingProductsDecoGS1ClassificationCatalog_1.0";
        String locale = "unlocalized";
        String category = "GS1_83000000_10002444_Deco_Flooring_Accessories";
        String attribute = "gs1_83000000_10002444_finish";
        String attributeTranslation = "Finish";
        String splitValue3 = catalog + category + "." + attribute;
        String name = catalog + SEPARATOR + locale + SEPARATOR + category + SEPARATOR + catalog + category + "." + attribute;

        IndexedType indexedType = mock(IndexedType.class);
        SearchQuery searchQuery = spy(new SearchQuery(facetSearchConfig, indexedType));
        searchQuery.setLanguage("en");
        searchQuery.setCurrency("EUR");

        IndexedProperty property = new IndexedProperty();
        property.setName("completeness_attribute");
        property.setType("string");

        ClassAttributeAssignmentModel classAttributeAssignmentModel = mock(ClassAttributeAssignmentModel.class);
        given(classificationAttributeAssignmentService.findClassAttributeAssignment(splitValue3)).willReturn(classAttributeAssignmentModel);
        ClassificationAttributeModel classificationAttributeModel = mock(ClassificationAttributeModel.class);
        classificationAttributeModel.setName(category, Locale.ENGLISH);
        given(classAttributeAssignmentModel.getClassificationAttribute()).willReturn(classificationAttributeModel);
        given(classificationAttributeModel.getName(Locale.ENGLISH)).willReturn(attributeTranslation);

        String result = provider.getDisplayName(searchQuery, property, name);

        assertThat(result).isNotEmpty().isEqualTo(attributeTranslation);
    }

    @Test
    public void getDisplayName_localized() {
        String catalog = "buildingProductsDecoGS1ClassificationCatalog_1.0";
        String locale = "en";
        String category = "GS1_83000000_10002444_Deco_Flooring_Accessories";
        String attribute = "gs1_83000000_10002444_finish";
        String attributeTranslation = "Finish";
        String splitValue3 = catalog + category + "." + attribute;
        String name = catalog + SEPARATOR + locale + SEPARATOR + category + SEPARATOR + catalog + category + "." + attribute;

        IndexedType indexedType = mock(IndexedType.class);
        SearchQuery searchQuery = spy(new SearchQuery(facetSearchConfig, indexedType));
        searchQuery.setLanguage("en");
        searchQuery.setCurrency("EUR");

        IndexedProperty property = new IndexedProperty();
        property.setName("completeness_attribute");
        property.setType("string");

        ClassAttributeAssignmentModel classAttributeAssignmentModel = mock(ClassAttributeAssignmentModel.class);
        given(classificationAttributeAssignmentService.findClassAttributeAssignment(splitValue3)).willReturn(classAttributeAssignmentModel);
        ClassificationAttributeModel classificationAttributeModel = mock(ClassificationAttributeModel.class);
        classificationAttributeModel.setName(category, Locale.ENGLISH);
        given(classAttributeAssignmentModel.getClassificationAttribute()).willReturn(classificationAttributeModel);
        given(classificationAttributeModel.getName(Locale.ENGLISH)).willReturn(attributeTranslation);

        String result = provider.getDisplayName(searchQuery, property, name);

        assertThat(result).isNotEmpty().isEqualTo(attributeTranslation + " [" + locale + "]");
    }

    @Test
    public void getDisplayName_noTranslationAvailable() {
        String catalog = "buildingProductsDecoGS1ClassificationCatalog_1.0";
        String locale = "en";
        String category = "GS1_83000000_10002444_Deco_Flooring_Accessories";
        String attribute = "gs1_83000000_10002444_finish";
        String attributeTranslation = "";
        String splitValue3 = catalog + category + "." + attribute;
        String name = catalog + SEPARATOR + locale + SEPARATOR + category + SEPARATOR + catalog + category + "." + attribute;

        IndexedType indexedType = mock(IndexedType.class);
        SearchQuery searchQuery = spy(new SearchQuery(facetSearchConfig, indexedType));
        searchQuery.setLanguage("en");
        searchQuery.setCurrency("EUR");

        IndexedProperty property = new IndexedProperty();
        property.setName("completeness_attribute");
        property.setType("string");

        ClassAttributeAssignmentModel classAttributeAssignmentModel = mock(ClassAttributeAssignmentModel.class);
        given(classificationAttributeAssignmentService.findClassAttributeAssignment(splitValue3)).willReturn(classAttributeAssignmentModel);
        ClassificationAttributeModel classificationAttributeModel = mock(ClassificationAttributeModel.class);
        classificationAttributeModel.setName(category, Locale.ENGLISH);
        given(classAttributeAssignmentModel.getClassificationAttribute()).willReturn(classificationAttributeModel);
        given(classificationAttributeModel.getName(Locale.ENGLISH)).willReturn(attributeTranslation);

        String result = provider.getDisplayName(searchQuery, property, name);

        assertThat(result).isNotEmpty().isEqualTo(catalog + category + "." + attribute + " [" + locale + "]");
    }

}