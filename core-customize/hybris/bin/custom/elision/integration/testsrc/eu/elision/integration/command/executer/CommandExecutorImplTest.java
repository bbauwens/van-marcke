package eu.elision.integration.command.executer;

import eu.elision.integration.command.Command;
import eu.elision.integration.command.Response;
import eu.elision.integration.command.executer.impl.CommandExecutorImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.HashMap;
import java.util.Map;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CommandExecutorImplTest {

    @Mock
    private Response response;
    @Mock
    private Command command;
    @Mock
    private CommandExecutorCallback commandExecutorCallback;

    private CommandExecutorImpl commandExecuter;

    @Test(expected = IllegalArgumentException.class)
    public void testExecuteCommand_withoutBaseUrl() {
        commandExecuter = new CommandExecutorImpl(null);
    }

    @Test
    public void testExecuteCommand() {
        Map<String, Object> properties = new HashMap<>();
        properties.put("baseUrl", "baseURL");

        when(command.execute(properties)).thenReturn(response);

        commandExecuter = new CommandExecutorImpl(properties);

        Response result = commandExecuter.executeCommand(command, commandExecutorCallback);
        assertThat(result).isEqualTo(response);

        verify(commandExecutorCallback).execute(result);
    }
}