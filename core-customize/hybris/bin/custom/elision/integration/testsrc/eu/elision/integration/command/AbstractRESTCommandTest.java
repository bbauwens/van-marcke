package eu.elision.integration.command;

import eu.elision.integration.command.configuration.AuthenticationType;
import eu.elision.integration.command.configuration.annotation.CommandConfig;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.internal.util.reflection.Whitebox;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static org.fest.assertions.Assertions.assertThat;
import static org.fest.assertions.MapAssert.entry;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AbstractRESTCommandTest {

    private static final String RESPONSE_BODY = RandomStringUtils.randomAlphabetic(10);
    private static final String PARAM_KEY = RandomStringUtils.randomAlphabetic(10);
    private static final String PARAM_VALUE = RandomStringUtils.randomAlphabetic(10);
    private static final String PAYLOAD = RandomStringUtils.randomAlphabetic(10);
    private static final String BASE_URL = RandomStringUtils.randomAlphabetic(10);

    private static final String BASE_URL_PROPERTY = "baseUrl";
    private static final String PASSWORD_PROPERTY = "password";
    private static final String USER_PROPERTY = "user";

    private static final String USER = "admin";
    private static final String PASSWORD = "nimda";

    private static final String URL = "test_url";
    private static final String REST_TEMPLATE = "restTemplate";
    private static final String SERVICE = "test_service";

    @Mock
    private RestTemplate restTemplate;

    @Mock
    private ResponseEntity responseEntity;

    @Before
    public void setUp() throws Exception {
        when(responseEntity.getStatusCode()).thenReturn(HttpStatus.ACCEPTED);
        when(responseEntity.getBody()).thenReturn(RESPONSE_BODY);
    }

    @Test
    public void testConstructorType() {
        MockGetRESTCommand command = new MockGetRESTCommand();

        assertThat(command.getResponseType()).isEqualTo(String.class);
    }

    @Test
    public void testCommandAnnotation() {
        MockGetRESTCommand command = new MockGetRESTCommand();

        assertThat(command.getHttpMethod()).isEqualTo(HttpMethod.GET);
        assertThat(command.getUrl()).isEqualTo(URL);
        assertThat(command.getServiceName()).isEqualTo(SERVICE);
    }

    @Test
    public void testPayload() {
        MockPostRESTCommand command = new MockPostRESTCommand();
        command.setPayLoad(PAYLOAD);

        assertThat(command.getPayLoad()).isEqualTo(PAYLOAD);
    }

    @Test
    public void testAddRequestParam() {
        MockGetRESTCommand command = new MockGetRESTCommand();

        assertThat(command.getRequestParams())
                .isNotNull()
                .isEmpty();

        command.addRequestParam(PARAM_KEY, PARAM_VALUE);

        assertThat(command.getRequestParams())
                .isNotNull()
                .isNotEmpty()
                .includes(entry(PARAM_KEY, PARAM_VALUE));
    }

    @Test
    public void testAddPathParam() {
        MockGetRESTCommand command = new MockGetRESTCommand();

        assertThat(command.getPathParams())
                .isNotNull()
                .isEmpty();

        command.addPathParam(PARAM_KEY, PARAM_VALUE);

        assertThat(command.getPathParams())
                .isNotNull()
                .isNotEmpty()
                .includes(entry(PARAM_KEY, PARAM_VALUE));
    }

    @Test
    public void testExecute_withHttpMethodGet() {
        MockGetRESTCommand command = new MockGetRESTCommand();

        Whitebox.setInternalState(command, REST_TEMPLATE, restTemplate);

        Map<String, Object> properties = new HashMap<>();
        properties.put(BASE_URL_PROPERTY, BASE_URL);

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN));
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity entity = new HttpEntity(command.getPayLoad(), httpHeaders);

        doReturn(responseEntity).when(restTemplate).exchange(eq(BASE_URL + URL), eq(HttpMethod.GET), eq(entity), eq(command.getResponseType()), eq(new HashMap<>()));

        Response<String> response = command.execute(properties);
        assertThat(response).isNotNull();
        assertThat(response.getHttpStatus()).isEqualTo(HttpStatus.ACCEPTED);
        assertThat(response.getPayLoad()).isEqualTo(RESPONSE_BODY);
    }

    @Test
    public void testExecute_withHttpMethodGet_withRequestParams() {
        MockGetRESTCommandWithRequestParams command = new MockGetRESTCommandWithRequestParams();

        Whitebox.setInternalState(command, REST_TEMPLATE, restTemplate);

        Map<String, Object> properties = new HashMap<>();
        properties.put(BASE_URL_PROPERTY, BASE_URL);

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN));
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity entity = new HttpEntity(command.getPayLoad(), httpHeaders);

        doReturn(responseEntity).when(restTemplate).exchange(eq(BASE_URL + URL + "?param1=param1&param2=param 2"), eq(HttpMethod.GET), eq(entity), eq(command.getResponseType()), eq(new HashMap<>()));

        Response<String> response = command.execute(properties);
        assertThat(response).isNotNull();
        assertThat(response.getHttpStatus()).isEqualTo(HttpStatus.ACCEPTED);
        assertThat(response.getPayLoad()).isEqualTo(RESPONSE_BODY);
    }

    @Test
    public void testExecute_withHttpMethodPost() {
        MockPostRESTCommand command = new MockPostRESTCommand();
        command.addPathParam(PARAM_KEY, PARAM_VALUE);

        Whitebox.setInternalState(command, REST_TEMPLATE, restTemplate);

        Map<String, Object> properties = new HashMap<>();
        properties.put(USER_PROPERTY, USER);
        properties.put(PASSWORD_PROPERTY, PASSWORD);
        properties.put(BASE_URL_PROPERTY, BASE_URL);

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN));
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity entity = new HttpEntity(command.getPayLoad(), httpHeaders);

        doReturn(responseEntity)
                .when(restTemplate)
                .exchange(eq(BASE_URL + URL), eq(HttpMethod.POST), eq(entity), eq(command.getResponseType()), eq(command.getPathParams()));

        Response<String> response = command.execute(properties);
        assertThat(response).isNotNull();
        assertThat(response.getHttpStatus()).isEqualTo(HttpStatus.ACCEPTED);
        assertThat(response.getPayLoad()).isEqualTo(RESPONSE_BODY);
    }

    @Test
    public void testExecute_withHttpMethodPut() {
        MockPutRESTCommand command = new MockPutRESTCommand();
        command.addPathParam(PARAM_KEY, PARAM_VALUE);

        Whitebox.setInternalState(command, REST_TEMPLATE, restTemplate);

        Map<String, Object> properties = new HashMap<>();
        properties.put(USER_PROPERTY, USER);
        properties.put(PASSWORD_PROPERTY, PASSWORD);
        properties.put(BASE_URL_PROPERTY, BASE_URL);

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN));
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity entity = new HttpEntity(command.getPayLoad(), httpHeaders);

        doReturn(responseEntity)
                .when(restTemplate)
                .exchange(eq(BASE_URL + URL), eq(HttpMethod.PUT), eq(entity), eq(command.getResponseType()), eq(command.getPathParams()));

        Response<String> response = command.execute(properties);
        assertThat(response).isNotNull();
        assertThat(response.getHttpStatus()).isEqualTo(HttpStatus.ACCEPTED);
        assertThat(response.getPayLoad()).isEqualTo(RESPONSE_BODY);
    }

    @CommandConfig(httpMethod = HttpMethod.GET, url = URL, serviceName = SERVICE)
    private class MockGetRESTCommand extends AbstractRESTCommand<String> {

    }

    @CommandConfig(httpMethod = HttpMethod.GET, url = URL, serviceName = SERVICE)
    private class MockGetRESTCommandWithRequestParams extends AbstractRESTCommand<String> {

        public MockGetRESTCommandWithRequestParams() {
            super();
            this.addRequestParam("param1", "param1");
            this.addRequestParam("param2", "param 2");
        }

    }

    @CommandConfig(httpMethod = HttpMethod.POST, url = URL, serviceName = SERVICE, authenticationType = AuthenticationType.BASIC)
    private class MockPostRESTCommand extends AbstractRESTCommand<String> {

    }

    @CommandConfig(httpMethod = HttpMethod.PUT, url = URL, serviceName = SERVICE, authenticationType = AuthenticationType.BASIC)
    private class MockPutRESTCommand extends AbstractRESTCommand<String> {

    }

}