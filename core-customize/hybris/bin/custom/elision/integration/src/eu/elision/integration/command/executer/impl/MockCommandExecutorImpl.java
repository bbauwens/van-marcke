package eu.elision.integration.command.executer.impl;

import eu.elision.integration.command.Command;
import eu.elision.integration.command.Response;
import eu.elision.integration.command.executer.CommandExecutor;
import eu.elision.integration.command.executer.CommandExecutorCallback;

/**
 * Mock implementation of the CommandExecutorImpl, this is here to be able to mock the commands and define return values/response objects as a text file.
 * This makes testing easier, but also makes it possible to start implementation if the "backend" is not yet ready.
 *
 * @author vangeda
 */
public abstract class MockCommandExecutorImpl implements CommandExecutor {

    /**
     * Default constructor
     */
    public MockCommandExecutorImpl() {
        // default constructor
    }

    /**
     * Goal of this method is to return the correct mock implementation, based on your command.
     * Please do not change this class directly, but use this as a 'template' in your own extensions!
     *
     * @param command                 the command to execute
     * @param commandExecutorCallback the callback to call after the command has been executed
     */
    @Override
    public abstract <T> Response<T> executeCommand(Command<T> command, CommandExecutorCallback<T> commandExecutorCallback);
}
