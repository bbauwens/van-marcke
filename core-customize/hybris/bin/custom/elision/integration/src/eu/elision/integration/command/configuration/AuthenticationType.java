package eu.elision.integration.command.configuration;

/**
 * Defines different HTTP authentication types, such as BASIC or OAUTH.
 */
public enum AuthenticationType {

    NONE, BASIC, OAUTH2

}
