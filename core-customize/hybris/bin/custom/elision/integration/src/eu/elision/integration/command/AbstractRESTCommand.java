package eu.elision.integration.command;

import eu.elision.integration.command.configuration.AuthenticationType;
import eu.elision.integration.command.configuration.annotation.CommandConfig;
import eu.elision.integration.command.impl.ResponseImpl;
import eu.elision.integration.interceptors.LogClientHttpRequestInterceptor;
import org.apache.commons.collections4.MapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import javax.net.ssl.*;
import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.net.Socket;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.*;

import static eu.elision.integration.command.executer.CommandExecutor.*;
import static org.apache.commons.collections4.MapUtils.getInteger;
import static org.apache.commons.collections4.MapUtils.getString;

/**
 * Abstract base class for a REST Command
 *
 * @param <T> type of Command to use
 */
public abstract class AbstractRESTCommand<T> implements Command<T> {

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractRESTCommand.class);

    private Map<String, String> pathParams = new HashMap<>();
    protected Map<String, String> requestParams = new LinkedHashMap<>();
    protected HttpHeaders httpHeaders = new HttpHeaders();
    private Class<T> type;

    protected HttpMethod httpMethod;
    private String url;
    private String serviceName;
    protected AuthenticationType authenticationType;

    private Object payload;

    protected RestTemplate restTemplate;

    public AbstractRESTCommand() {
        Type t = getClass().getGenericSuperclass();
        if (t instanceof ParameterizedType) {
            ParameterizedType parameterizedType = (ParameterizedType) t;
            this.type = (Class<T>) parameterizedType.getActualTypeArguments()[0];
        }

        for (Annotation a : this.getClass().getAnnotationsByType(CommandConfig.class)) {
            CommandConfig commandConfig = (CommandConfig) a;

            this.httpMethod = commandConfig.httpMethod();
            this.url = commandConfig.url();
            this.serviceName = commandConfig.serviceName();
            this.authenticationType = commandConfig.authenticationType();
        }

        TrustManager[] trustAllCerts = new TrustManager[]{
                new X509ExtendedTrustManager() {

                    @Override
                    public void checkClientTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {
                    }

                    @Override
                    public void checkServerTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {
                    }

                    @Override
                    public X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }

                    @Override
                    public void checkClientTrusted(X509Certificate[] x509Certificates, String s, Socket socket) throws CertificateException {
                    }

                    @Override
                    public void checkServerTrusted(X509Certificate[] x509Certificates, String s, Socket socket) throws CertificateException {
                    }

                    @Override
                    public void checkClientTrusted(X509Certificate[] x509Certificates, String s, SSLEngine sslEngine) throws CertificateException {
                    }

                    @Override
                    public void checkServerTrusted(X509Certificate[] x509Certificates, String s, SSLEngine sslEngine) throws CertificateException {
                    }
                }
        };
        try {
            SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());

        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }

        this.restTemplate = newRestTemplate();
        this.restTemplate.setErrorHandler(new ElisionResponseErrorHandler());
    }

    protected RestTemplate newRestTemplate() {
        return new RestTemplate();
    }

    @Override
    public Object getPayLoad() {
        return this.payload;
    }

    @Override
    public final void setPayLoad(Object payLoad) {
        this.payload = payLoad;
    }

    @Override
    public String getUrl() {
        return this.url;
    }

    @Override
    public HttpMethod getHttpMethod() {
        return this.httpMethod;
    }

    @Override
    public Class<T> getResponseType() {
        return this.type;
    }

    @Override
    public Map<String, String> getPathParams() {
        return this.pathParams;
    }

    @Override
    public Map<String, String> getRequestParams() {
        return this.requestParams;
    }

    @Override
    public String getServiceName() {
        return this.serviceName;
    }

    protected final void addRequestParam(String key, String value) {
        this.requestParams.put(key, value);
    }

    protected final void addPathParam(String key, String value) {
        this.pathParams.put(key, value);
    }

    protected final void addHttpHeader(final String headerName, final String headerValue) {
        this.httpHeaders.add(headerName, headerValue);
    }

    protected final void addHttpHeader(final String headerName, final List<String> headerValues) {
        this.httpHeaders.put(headerName, headerValues);
    }

    @Override
    public Response<T> execute(Map<String, Object> serviceProperties) {
        Response<T> response = new ResponseImpl<>();

        restTemplate.setRequestFactory(buildRequestFactory(serviceProperties));

        restTemplate.getInterceptors().add(new LogClientHttpRequestInterceptor());

        if (AuthenticationType.BASIC.equals(this.authenticationType)) {
            restTemplate.getInterceptors().add(new BasicAuthorizationInterceptor(getString(serviceProperties, USER), getString(serviceProperties, PASSWORD)));
        }

        buildHttpHeaders(httpHeaders);

        final String uri = buildUri(serviceProperties);

        try {
//            TODO: RE-ENABLE THIS ONCE THE CSRF ISSUE IS FIXED.
//            beforeExecute(restTemplate, uri);

            HttpEntity<Object> request = null;
            if (HttpMethod.GET.equals(httpMethod)) {
                request = new HttpEntity<>(httpHeaders);
            } else if (HttpMethod.POST.equals(httpMethod) || HttpMethod.PUT.equals(httpMethod)) {
                request = new HttpEntity<>(getPayLoad(), httpHeaders);
            }

            ResponseEntity<T> exchangeResponse = restTemplate.exchange(uri, httpMethod, request, getResponseType(), getPathParams());
            response.setHttpStatus(exchangeResponse.getStatusCode());
            T body = exchangeResponse.getBody();
            if (body != null) {
                response.setPayLoad(body);
            }
        } catch (Exception e) {
            LOGGER.error("Exception during command execution: " + e.getMessage(), e);
        }
        return response;
    }

    protected ClientHttpRequestFactory buildRequestFactory(final Map<String, Object> serviceProperties) {
        final HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();

        final Integer connectionRequestTimeout = getInteger(serviceProperties, CONNECTION_REQUEST_TIMEOUT);
        if (connectionRequestTimeout != null) {
            requestFactory.setConnectionRequestTimeout(connectionRequestTimeout);
        }

        final Integer connectTimeout = getInteger(serviceProperties, CONNECT_TIMEOUT);
        if (connectTimeout != null) {
            requestFactory.setConnectTimeout(connectTimeout);
        }

        final Integer readTimeout = getInteger(serviceProperties, READ_TIMEOUT);
        if (readTimeout != null) {
            requestFactory.setReadTimeout(readTimeout);
        }

        return new BufferingClientHttpRequestFactory(requestFactory);
    }

    protected void buildHttpHeaders(final HttpHeaders httpHeaders) {
        httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN));
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
    }

    protected String buildUri(Map<String, Object> serviceProperties) {
        UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder.fromUriString(getString(serviceProperties, BASE_URL) + getUrl());
        if (MapUtils.isNotEmpty(requestParams)) {
            for (Map.Entry<String, String> requestParam : requestParams.entrySet()) {
                uriComponentsBuilder.queryParam(requestParam.getKey(), requestParam.getValue());
            }
        }

        return uriComponentsBuilder.build().toUriString();
    }

    protected void beforeExecute(final RestTemplate restTemplate, final String url) {
    }
}