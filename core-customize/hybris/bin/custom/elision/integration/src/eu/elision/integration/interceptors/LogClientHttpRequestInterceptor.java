package eu.elision.integration.interceptors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

import java.io.IOException;

import static java.lang.String.format;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.apache.commons.lang3.ArrayUtils.isNotEmpty;
import static org.springframework.util.StreamUtils.copyToString;

public class LogClientHttpRequestInterceptor implements ClientHttpRequestInterceptor {

    private static final Logger LOGGER = LoggerFactory.getLogger(LogClientHttpRequestInterceptor.class);

    @Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(formatRequest(request, body));
        }

        final ClientHttpResponse response = execution.execute(request, body);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(formatResponse(response));
        }

        return response;
    }

    private String formatRequest(final HttpRequest request, final byte[] body) {
        if (isNotEmpty(body)) {
            final String formattedBody = new String(body, UTF_8);
            return format("http-request >> %s %s %s", request.getMethod(), request.getURI(), formattedBody);
        } else {
            return format("http-request >> %s %s", request.getMethod(), request.getURI());
        }
    }

    private String formatResponse(final ClientHttpResponse response) throws IOException {
        return format("http-response << %s %s", response.getStatusCode(), copyToString(response.getBody(), UTF_8));
    }
}