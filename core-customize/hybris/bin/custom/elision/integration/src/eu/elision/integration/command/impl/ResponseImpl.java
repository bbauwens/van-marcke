package eu.elision.integration.command.impl;

import eu.elision.integration.command.Response;
import org.springframework.http.HttpStatus;

/**
 * Response Implementation
 *
 * @param <T> type of the response
 */
public class ResponseImpl<T> implements Response<T> {

    private T payLoad;
    private HttpStatus httpStatus;
    private String errorMessage;

    @Override
    public HttpStatus getHttpStatus() {
        return this.httpStatus;
    }

    @Override
    public void setHttpStatus(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }

    @Override
    public String getErrorMessage() {
        return this.errorMessage;
    }

    @Override
    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    @Override
    public T getPayLoad() {
        return this.payLoad;
    }

    @Override
    public void setPayLoad(T payLoad) {
        this.payLoad = payLoad;
    }

}