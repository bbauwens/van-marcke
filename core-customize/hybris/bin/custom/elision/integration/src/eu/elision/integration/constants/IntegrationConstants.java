package eu.elision.integration.constants;

/**
 * Global class for all Integration constants. You can add global constants for your extension into this class.
 */
public final class IntegrationConstants extends GeneratedIntegrationConstants {

    public static final String EXTENSIONNAME = "integration";

    private IntegrationConstants() {
        //empty to avoid instantiating this constant class
    }
}