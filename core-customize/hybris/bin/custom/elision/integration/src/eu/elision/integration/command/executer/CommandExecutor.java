package eu.elision.integration.command.executer;

import eu.elision.integration.command.Command;
import eu.elision.integration.command.Response;

/**
 * Executes the given command.
 * If present (not null), the provided callback will be invoked after executing the command.
 */
@FunctionalInterface
public interface CommandExecutor {

    String BASE_URL = "baseUrl";
    String PASSWORD = "password";
    String USER = "user";
    String CONNECTION_REQUEST_TIMEOUT = "connectionRequestTimeout";
    String CONNECT_TIMEOUT = "connectTimeout";
    String READ_TIMEOUT = "readTimeout";

    /**
     * Execute the given command
     *
     * @param command                 the command to execute
     * @param commandExecutorCallback the callback to call after the command has been executed
     * @param <T>                     The type of command that should be used and returned
     * @return a response of type T
     */
    <T> Response<T> executeCommand(Command<T> command, CommandExecutorCallback<T> commandExecutorCallback);

}