package eu.elision.integration.command.executer;

/**
 *  Factory to return the correct instance of the executor.
 *  This makes it possible to return mock objects if needed.
 *
 *  This is made abstract, because you will have to forsee the implementation yourself (we don't know which commands to return :) )
 */
public interface CommandExecutorFactory {

    /**
     * Get the instance of the correct CommandExecutor
     *
     * @param serviceName name of the Service
     * @return command executor instance
     */
    public CommandExecutor getInstance(String serviceName);

}
