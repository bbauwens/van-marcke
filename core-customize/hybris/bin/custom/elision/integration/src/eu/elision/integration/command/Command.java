package eu.elision.integration.command;

import org.springframework.http.HttpMethod;

import java.util.Map;

/**
 * Command interface for executing HTTP operations
 *
 * @param <T> type of Command to use
 */
public interface Command<T> {

    /**
     * Getter that returns the payload
     *
     * @return Object the payload
     */
    Object getPayLoad();

    /**
     * Sets the given payload
     *
     * @param payLoad the payload to set
     */
    void setPayLoad(Object payLoad);

    /**
     * Getter that returns the Url
     *
     * @return String the url to return
     */
    String getUrl();

    /**
     * Getter that returns the HttpRequestType
     *
     * @return HttpRequestType the HttpRequestType to return
     */
    HttpMethod getHttpMethod();

    /**
     * Getter that returns the ResponseType
     *
     * @return Class<T> the ResponseType to return
     */
    Class<T> getResponseType();

    /**
     * Getter that returns the path parameters
     *
     * @return Map<String, String> the PathParams to return
     */
    Map<String, String> getPathParams();

    /**
     * Getter that returns the request parameters
     *
     * @return Map<String, String> the RequestParams to return
     */
    Map<String, String> getRequestParams();

    /**
     * Executes the current command with the given properties
     *
     * @param serviceProperties the properties to use to execute this command
     * @return Response<T> the response
     */
    Response<T> execute(Map<String, Object> serviceProperties);

    /**
     * Getter that returns the service name
     *
     * @return String the ServiceName to return
     */
    String getServiceName();

}