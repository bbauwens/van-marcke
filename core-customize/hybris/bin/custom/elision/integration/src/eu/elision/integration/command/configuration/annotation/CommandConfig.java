package eu.elision.integration.command.configuration.annotation;

import eu.elision.integration.command.configuration.AuthenticationType;
import org.springframework.http.HttpMethod;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Interface that defines a configuration for a HTTP Command
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface CommandConfig {

    /**
     * Returns the url which defaults to an empty string
     *
     * @return url
     */
    String url() default "";

    /**
     * Returns the requestType which defaults to GET
     *
     * @return HttpRequestType
     */
    HttpMethod httpMethod() default HttpMethod.GET;

    /**
     * Returns the service name
     *
     * @return serviceName
     */
    String serviceName();

    /**
     * Returns the authenticationType which defaults to NONE
     *
     * @return AuthenticationType
     */
    AuthenticationType authenticationType() default AuthenticationType.NONE;

}