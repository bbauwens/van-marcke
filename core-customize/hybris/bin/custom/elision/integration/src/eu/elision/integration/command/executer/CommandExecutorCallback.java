package eu.elision.integration.command.executer;

import eu.elision.integration.command.Response;

/**
 * Callback that will be executed after a Command has finished.
 *
 * @param <T> type to use for the response
 */
@FunctionalInterface
public interface CommandExecutorCallback<T> {

    /**
     * Execute response
     *
     * @param response the response to execute as callback
     */
    void execute(Response<T> response);

}