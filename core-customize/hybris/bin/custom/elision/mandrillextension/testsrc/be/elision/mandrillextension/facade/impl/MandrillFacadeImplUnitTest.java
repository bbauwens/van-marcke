package be.elision.mandrillextension.facade.impl;

import be.elision.mandrillextension.converter.MandrillConverter;
import be.elision.mandrillextension.service.MandrillService;
import com.microtripit.mandrillapp.lutung.model.MandrillApiError;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.io.IOException;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * @author marteto
 */
@UnitTest
public class MandrillFacadeImplUnitTest {

    private DefaultMandrillFacadeImpl mandrillFacade;

    @Mock
    private MandrillService mandrillService;

    @Mock
    private MandrillConverter<OrderModel> converter;

    @Before
    public void setUp() {
        initMocks(this);

        mandrillFacade = new DefaultMandrillFacadeImpl();
        mandrillFacade.setMandrillService(mandrillService);
    }

    @Test
    public void testSend_Success() throws IOException, MandrillApiError {
        String templateName = "templateName";
        OrderModel orderModel = mock(OrderModel.class);
        String email = "email";

        mandrillFacade.send(templateName, orderModel, converter, email);

        verify(mandrillService).send(templateName, orderModel, converter, email);
    }
}
