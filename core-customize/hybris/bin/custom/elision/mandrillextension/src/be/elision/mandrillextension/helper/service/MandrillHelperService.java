package be.elision.mandrillextension.helper.service;

import java.util.List;

import static com.microtripit.mandrillapp.lutung.view.MandrillMessage.MergeVar;
import static com.microtripit.mandrillapp.lutung.view.MandrillMessage.MergeVarBucket;
import static com.microtripit.mandrillapp.lutung.view.MandrillMessage.Recipient;

/**
 * Interface for {@link MandrillHelperService}.
 *
 * @author marteto
 */
public interface MandrillHelperService {

    /**
     * Converts emails to a List of {@link Recipient}.
     *
     * @param emails emails
     * @return A list of {@link Recipient}.
     */
    List<Recipient> getRecipients(final String... emails);

    /**
     * Converts emails to a list of cc's
     *
     * @param emails emails
     * @return A list of {@link Recipient}.
     */
    List<Recipient> getCCs(final String... emails);

    /**
     * Converts emails to a list of bcc's
     *
     * @param emails emails
     * @return A list of {@link Recipient}.
     */
    List<Recipient> getBCCs(final String... emails);

    /**
     * Converts an email into a {@link Recipient}.
     *
     * @param email The email.
     * @return The {@link Recipient}.
     */
    Recipient convertEmailToRecipient(final String email);

    /**
     * Converts an email into a CC {@link Recipient}.
     *
     * @param email The email.
     * @return The {@link Recipient}.
     */
    Recipient convertEmailToCCRecipient(final String email);

    /**
     * Converts an email into a BCC {@link Recipient}.
     *
     * @param email The email.
     * @return The {@link Recipient}.
     */
    Recipient convertEmailToBCCRecipient(final String email);

    /**
     * Extracts the first {@link MergeVar} from the first {@link MergeVarBucket}.
     *
     * @param mergeVarBuckets A list of {@link MergeVarBucket}.
     * @return The first {@link MergeVar} from the first {@link MergeVarBucket} or null.
     */
    MergeVar extractSingleMergeVar(final List<MergeVarBucket> mergeVarBuckets);

}
