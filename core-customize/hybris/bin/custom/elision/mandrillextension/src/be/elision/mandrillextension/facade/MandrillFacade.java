package be.elision.mandrillextension.facade;

import be.elision.mandrillextension.converter.MandrillConverter;
import com.microtripit.mandrillapp.lutung.model.MandrillApiError;
import com.microtripit.mandrillapp.lutung.view.MandrillMessageStatus;

import java.io.IOException;

/**
 * Interface for {@link MandrillFacade}.
 *
 * @author marteto
 */
@FunctionalInterface
public interface MandrillFacade {

    /**
     * Sends an email.
     *
     * @param templateName The name of the template to be used.
     * @param object       The object that contains the variables.
     * @param converter    The converter that should be used in conjunction with the object.
     * @param email        The email of the recipient.
     * @return {@link MandrillMessageStatus[]}
     * @throws IOException
     * @throws MandrillApiError
     */
    <T> MandrillMessageStatus[] send(final String templateName, final T object, final MandrillConverter<T> converter, final String email) throws IOException, MandrillApiError;
}
