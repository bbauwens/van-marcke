package be.elision.mandrillextension.facade.impl;

import be.elision.mandrillextension.converter.MandrillConverter;
import be.elision.mandrillextension.facade.MandrillFacade;
import be.elision.mandrillextension.service.MandrillService;
import com.microtripit.mandrillapp.lutung.model.MandrillApiError;
import com.microtripit.mandrillapp.lutung.view.MandrillMessageStatus;
import org.springframework.beans.factory.annotation.Required;

import java.io.IOException;

/**
 * Default implementation of the {@link MandrillFacade}.
 *
 * @author marteto
 */
public class DefaultMandrillFacadeImpl implements MandrillFacade {

    private MandrillService mandrillService;

    /**
     * {@inheritDoc}
     */
    @Override
    public <T> MandrillMessageStatus[] send(final String templateName, final T object, final MandrillConverter<T> converter, final String email) throws IOException, MandrillApiError {
        return mandrillService.send(templateName, object, converter, email);
    }

    /**
     * Setter method for the {@link MandrillService}.
     *
     * @param mandrillService The {@link MandrillService} to be set.
     */
    @Required
    public void setMandrillService(final MandrillService mandrillService) {
        this.mandrillService = mandrillService;
    }
}
