package be.elision.mandrillextension.converter;

import com.microtripit.mandrillapp.lutung.view.MandrillMessage.MergeVarBucket;

import java.util.List;

/**
 * Interface for the {@link MandrillConverter}.
 *
 * @author marteto
 */
@FunctionalInterface
public interface MandrillConverter<T> {

    /**
     * Converts an object into the appropriate format for Mandrill.
     *
     * @param object The object to be converted.
     * @return The object represented by a {@link MergeVarBucket}.
     */
    List<MergeVarBucket> convert(final T object);

}
