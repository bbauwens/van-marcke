<%@ page import="com.vanmarcke.services.util.URIUtils" %>
<%@ taglib prefix="eli-asm" tagdir="/WEB-INF/tags/addons/elisionssoaddon/responsive/asm" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%-- login --%>
<div id="_asmLogin" class="ASM_login">
    <c:set var="currentUrl"
           value='<%=URIUtils.normalize(request.getAttribute("javax.servlet.forward.request_uri").toString())%>'/>
    <c:set value="/elisionsamlsinglesignon/saml${currentUrl}?asm=true" var="loginActionUrl"/>

    <eli-asm:login actionNameKey="asm.login" action="${fn:escapeXml(loginActionUrl)}"/>
</div>