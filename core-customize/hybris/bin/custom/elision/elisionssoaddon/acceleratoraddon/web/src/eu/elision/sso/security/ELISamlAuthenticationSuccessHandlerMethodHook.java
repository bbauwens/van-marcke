package eu.elision.sso.security;

import org.springframework.security.core.Authentication;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * A hook strategy to run custom code after authentication success
 */
@FunctionalInterface
public interface ELISamlAuthenticationSuccessHandlerMethodHook {

    /**
     * Execute custom logic after the authentication
     *
     * @param httpServletRequest  {@link HttpServletRequest}
     * @param httpServletResponse {@link HttpServletResponse}
     * @param authentication      {@link Authentication}
     */
    void doMethodHook(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication);
}
