package eu.elision.sso.filters;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.EmployeeModel;
import de.hybris.platform.jalo.user.CookieBasedLoginToken;
import de.hybris.platform.jalo.user.User;
import de.hybris.platform.servicelayer.user.UserService;
import eu.elision.sso.utils.ELISamlStorefrontUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ELIAssistedServiceFilterTest {

    @Mock
    private CookieBasedLoginToken cookieBasedLoginToken;
    @Mock
    private UserService userService;
    @Mock
    private HttpServletRequest request;
    @Mock
    private Cookie cookie;
    @Mock
    private ELISamlStorefrontUtils eliSamlStorefrontUtils;
    @InjectMocks
    private ELIAssistedServiceFilter eliAssistedServiceFilter;

    @Test
    public void testShouldNotFilterWithCustomer() throws Exception {
        CustomerModel customerModel = mock(CustomerModel.class);
        User user = mock(User.class);

        //whenNew(CookieBasedLoginToken.class).withArguments(cookie).thenReturn(cookieBasedLoginToken);
        when(eliSamlStorefrontUtils.getSamlCookie(request)).thenReturn(cookie);
        when(cookieBasedLoginToken.getUser()).thenReturn(user);
        when(userService.getUserForUID(any())).thenReturn(customerModel);

        boolean result = eliAssistedServiceFilter.shouldNotFilter(request);

        assertThat(result).isTrue();
    }

    @Test
    public void testShouldNotFilterWithEmployee() throws Exception {
        EmployeeModel employeeModel = mock(EmployeeModel.class);
        User user = mock(User.class);

        //whenNew(CookieBasedLoginToken.class).withArguments(cookie).thenReturn(cookieBasedLoginToken);
        when(eliSamlStorefrontUtils.getSamlCookie(request)).thenReturn(cookie);
        when(cookieBasedLoginToken.getUser()).thenReturn(user);
        when(eliSamlStorefrontUtils.getSamlCookie(request)).thenReturn(cookie);
        when(userService.getUserForUID(any())).thenReturn(employeeModel);

        boolean result = eliAssistedServiceFilter.shouldNotFilter(request);

        assertThat(result).isFalse();
    }

    @Test
    public void testShouldNotFilterNoSamlCookie() throws ServletException {
        boolean result = eliAssistedServiceFilter.shouldNotFilter(request);
        assertThat(result).isTrue();
    }
}