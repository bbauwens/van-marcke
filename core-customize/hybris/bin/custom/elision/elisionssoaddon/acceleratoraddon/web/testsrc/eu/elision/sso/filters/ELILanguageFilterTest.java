package eu.elision.sso.filters;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;
import eu.elision.sso.utils.ELISamlStorefrontUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;

import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ELILanguageFilterTest {

    @Mock
    private CommonI18NService commonI18NService;
    @Mock
    private BaseStoreService baseStoreService;
    @InjectMocks
    private ELILanguageFilter eliLanguageFilter;
    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    @Mock
    private FilterChain filterChain;
    @Mock
    private ELISamlStorefrontUtils eliSamlStorefrontUtils;

    @Test
    public void doFilterInternal() throws ServletException, IOException {
        LanguageModel languageModel = mock(LanguageModel.class);
        BaseStoreModel baseStoreModel = mock(BaseStoreModel.class);
        when(commonI18NService.getCurrentLanguage()).thenReturn(languageModel);
        when(baseStoreService.getCurrentBaseStore()).thenReturn(baseStoreModel);
        when(languageModel.getIsocode()).thenReturn("nl_BE");
        when(baseStoreModel.getLanguages()).thenReturn(new HashSet<>(Collections.singletonList(languageModel)));
        eliLanguageFilter.doFilterInternal(request, response, filterChain);
        verify(eliSamlStorefrontUtils).createOrUpdateLanguageCookie(response, "nl_BE");
    }

    @Test
    public void doFilterInternalLanguageNotInBaseStore() throws ServletException, IOException {
        LanguageModel languageModel = mock(LanguageModel.class);
        BaseStoreModel baseStoreModel = mock(BaseStoreModel.class);
        when(commonI18NService.getCurrentLanguage()).thenReturn(languageModel);
        when(baseStoreService.getCurrentBaseStore()).thenReturn(baseStoreModel);
        eliLanguageFilter.doFilterInternal(request, response, filterChain);
        //PowerMockito.verifyZeroInteractions(ELISamlStorefrontUtils.class);
    }

    @Test
    public void doFilterInternalNoLanguageFound() throws ServletException, IOException {
        eliLanguageFilter.doFilterInternal(request, response, filterChain);
        //PowerMockito.verifyZeroInteractions(ELISamlStorefrontUtils.class);
    }
}