package eu.elision.sso.filters;

import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.jalo.user.CookieBasedLoginToken;
import de.hybris.platform.jalo.user.LoginToken;
import de.hybris.platform.servicelayer.user.UserService;
import eu.elision.sso.strategies.ELISamlStorefrontLoginStrategy;
import eu.elision.sso.utils.ELISamlStorefrontUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Custom filter for SAML SingleSignOn
 */
public class ELISamlSingleSingOnFilter extends OncePerRequestFilter {

    private UserService userService;
    private ELISamlStorefrontLoginStrategy eliSamlStorefrontLoginStrategy;
    private ELISamlStorefrontUtils eliSamlStorefrontUtils;

    public ELISamlSingleSingOnFilter(UserService userService, ELISamlStorefrontLoginStrategy eliSamlStorefrontLoginStrategy, ELISamlStorefrontUtils eliSamlStorefrontUtils) {
        this.userService = userService;
        this.eliSamlStorefrontLoginStrategy = eliSamlStorefrontLoginStrategy;
        this.eliSamlStorefrontUtils = eliSamlStorefrontUtils;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        if (eliSamlStorefrontUtils.getSamlCookie(httpServletRequest) != null) {
            try {
                performLogin(httpServletRequest, httpServletResponse);
            } catch (final RuntimeException e) {
                eliSamlStorefrontUtils.eraseSamlCookie(httpServletResponse);
            }
        }
        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }

    private void performLogin(final HttpServletRequest httpServletRequest, final HttpServletResponse httpServletResponse) throws IOException, ServletException {
        final LoginToken token = new CookieBasedLoginToken(eliSamlStorefrontUtils.getSamlCookie(httpServletRequest));

        if (userService.getCurrentUser() instanceof CustomerModel && userService.getCurrentUser().getUid().equals(token.getUser().getUid())) {
            return;
        }
        userService.setCurrentUser(userService.getUserForUID(token.getUser().getUid()));
        eliSamlStorefrontLoginStrategy.login(token.getUser().getUid(), httpServletRequest, httpServletResponse);
    }

    @Override
    protected boolean shouldNotFilter(HttpServletRequest request) {
        Cookie cookie = eliSamlStorefrontUtils.getSamlCookie(request);
        if (cookie != null) {
            LoginToken token = new CookieBasedLoginToken(cookie);
            return !(userService.getUserForUID(token.getUser().getUid()) instanceof CustomerModel);
        }
        return true;
    }
}
