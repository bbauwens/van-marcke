package eu.elision.sso.filters;

import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.store.services.BaseStoreService;
import eu.elision.sso.utils.ELISamlStorefrontUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ELILanguageFilter extends OncePerRequestFilter {

    private CommonI18NService commonI18NService;
    private BaseStoreService baseStoreService;
    private ELISamlStorefrontUtils eliSamlStorefrontUtils;

    public ELILanguageFilter(CommonI18NService commonI18NService, BaseStoreService baseStoreService, ELISamlStorefrontUtils eliSamlStorefrontUtils) {
        this.commonI18NService = commonI18NService;
        this.baseStoreService = baseStoreService;
        this.eliSamlStorefrontUtils = eliSamlStorefrontUtils;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        LanguageModel languageModel = commonI18NService.getCurrentLanguage();
        if (languageModel != null && baseStoreService.getCurrentBaseStore().getLanguages().contains(languageModel)) {
            eliSamlStorefrontUtils.createOrUpdateLanguageCookie(response, languageModel.getIsocode());
        }
        filterChain.doFilter(request, response);
    }
}