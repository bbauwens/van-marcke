package eu.elision.sso.filters;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.EmployeeModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.jalo.user.CookieBasedLoginToken;
import de.hybris.platform.jalo.user.User;
import de.hybris.platform.servicelayer.user.UserService;
import eu.elision.sso.strategies.ELISamlStorefrontLoginStrategy;
import eu.elision.sso.utils.ELISamlStorefrontUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ELISamlSingleSingOnFilterTest {

    @Mock
    private UserService userService;
    @Mock
    private ELISamlStorefrontLoginStrategy eliSamlStorefrontLoginStrategy;
    @Mock
    private CookieBasedLoginToken cookieBasedLoginToken;
    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    @Mock
    private FilterChain filterChain;
    @Mock
    private Cookie cookie;
    @Mock
    private ELISamlStorefrontUtils eliSamlStorefrontUtils;

    @InjectMocks
    private ELISamlSingleSingOnFilter eliSamlSingleSingOnFilter;

    @Test
    public void testDoFilterInternalWhenNoSamlCookie() throws ServletException, IOException {
        eliSamlSingleSingOnFilter.doFilterInternal(request, response, filterChain);

        verifyZeroInteractions(userService);
        verifyZeroInteractions(eliSamlStorefrontLoginStrategy);
        verify(filterChain).doFilter(request, response);
    }

    @Test
    public void testDoFilterInternalWhenSamlCookieFromSameCustomer() throws Exception {
        CustomerModel customerModel = mock(CustomerModel.class);
        User user = mock(User.class);

        when(eliSamlStorefrontUtils.getSamlCookie(request)).thenReturn(cookie);
        //whenNew(CookieBasedLoginToken.class).withArguments(cookie).thenReturn(cookieBasedLoginToken);
        when(userService.getCurrentUser()).thenReturn(customerModel);
        when(customerModel.getUid()).thenReturn("customer");
        when(cookieBasedLoginToken.getUser()).thenReturn(user);
        when(user.getUid()).thenReturn("customer");

        eliSamlSingleSingOnFilter.doFilterInternal(request, response, filterChain);

        verifyZeroInteractions(eliSamlStorefrontLoginStrategy);
        verify(userService, times(0)).setCurrentUser(any(UserModel.class));
        verify(filterChain).doFilter(request, response);
    }

    @Test
    public void testDoFilterInternalWhenSamlCookieFromOtherCustomer() throws Exception {
        CustomerModel customerModel = mock(CustomerModel.class);
        CustomerModel otherCustomer = mock(CustomerModel.class);
        User user = mock(User.class);

        when(eliSamlStorefrontUtils.getSamlCookie(request)).thenReturn(cookie);
        //whenNew(CookieBasedLoginToken.class).withArguments(cookie).thenReturn(cookieBasedLoginToken);
        when(userService.getCurrentUser()).thenReturn(customerModel);
        when(customerModel.getUid()).thenReturn("customer");
        when(cookieBasedLoginToken.getUser()).thenReturn(user);
        when(user.getUid()).thenReturn("otherCustomer");
        when(userService.getUserForUID("otherCustomer")).thenReturn(otherCustomer);

        eliSamlSingleSingOnFilter.doFilterInternal(request, response, filterChain);

        verify(eliSamlStorefrontLoginStrategy).login("otherCustomer", request, response);
        verify(userService).setCurrentUser(otherCustomer);
        verify(filterChain).doFilter(request, response);
    }

    @Test
    public void testShouldNotFilterWithCustomer() throws Exception {
        CustomerModel customerModel = mock(CustomerModel.class);
        User user = mock(User.class);

        //whenNew(CookieBasedLoginToken.class).withArguments(cookie).thenReturn(cookieBasedLoginToken);
        when(eliSamlStorefrontUtils.getSamlCookie(request)).thenReturn(cookie);
        when(cookieBasedLoginToken.getUser()).thenReturn(user);
        when(userService.getUserForUID(any())).thenReturn(customerModel);

        boolean result = eliSamlSingleSingOnFilter.shouldNotFilter(request);

        assertThat(result).isFalse();
    }

    @Test
    public void testShouldNotFilterWithEmployee() throws Exception {
        EmployeeModel employeeModel = mock(EmployeeModel.class);
        User user = mock(User.class);

        //whenNew(CookieBasedLoginToken.class).withArguments(cookie).thenReturn(cookieBasedLoginToken);
        when(eliSamlStorefrontUtils.getSamlCookie(request)).thenReturn(cookie);
        when(cookieBasedLoginToken.getUser()).thenReturn(user);
        when(eliSamlStorefrontUtils.getSamlCookie(request)).thenReturn(cookie);
        when(userService.getUserForUID(any())).thenReturn(employeeModel);

        boolean result = eliSamlSingleSingOnFilter.shouldNotFilter(request);

        assertThat(result).isTrue();
    }

    @Test
    public void testShouldNotFilterNoSamlCookie() throws ServletException {
        boolean result = eliSamlSingleSingOnFilter.shouldNotFilter(request);
        assertThat(result).isTrue();
    }
}