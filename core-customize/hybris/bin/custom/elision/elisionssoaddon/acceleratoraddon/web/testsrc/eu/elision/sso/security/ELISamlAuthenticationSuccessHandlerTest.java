package eu.elision.sso.security;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorstorefrontcommons.security.GUIDCookieStrategy;
import de.hybris.platform.acceleratorstorefrontcommons.strategy.CartRestorationStrategy;
import de.hybris.platform.acceleratorstorefrontcommons.strategy.CustomerConsentDataStrategy;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static java.util.Collections.singletonList;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ELISamlAuthenticationSuccessHandlerTest {

    @Mock
    private CustomerFacade customerFacade;
    @Mock
    private CartRestorationStrategy cartRestorationStrategy;
    @Mock
    private CustomerConsentDataStrategy customerConsentDataStrategy;
    @Mock
    private GUIDCookieStrategy guidCookieStrategy;
    @Mock
    private ELISamlAuthenticationSuccessHandlerMethodHook methodHook;

    @InjectMocks
    private ELISamlAuthenticationSuccessHandler eliSamlAuthenticationSuccessHandler;

    @Test
    public void testOnAuthenticationSuccessAdminAuthority() throws IOException, ServletException {
        HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
        HttpServletResponse httpServletResponse = mock(HttpServletResponse.class);
        Authentication authentication = mock(Authentication.class);
        HttpSession httpSession = mock(HttpSession.class);
        List authority = Arrays.asList(new SimpleGrantedAuthority("ROLE_ADMINGROUP"));

        when(authentication.getAuthorities()).thenReturn(authority);
        when(httpServletRequest.getSession()).thenReturn(httpSession);

        eliSamlAuthenticationSuccessHandler.onAuthenticationSuccess(httpServletRequest, httpServletResponse, authentication);

        verify(customerFacade).loginSuccess();
        verify(guidCookieStrategy).setCookie(httpServletRequest, httpServletResponse);
        verifyZeroInteractions(cartRestorationStrategy);
        verifyZeroInteractions(customerConsentDataStrategy);
        verify(httpSession).invalidate();
        verify(httpServletResponse).sendRedirect(httpServletRequest.getContextPath());
    }

    @Test
    public void testOnAuthenticationSuccess() throws IOException, ServletException {
        HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
        HttpServletResponse httpServletResponse = mock(HttpServletResponse.class);
        Authentication authentication = mock(Authentication.class);
        HttpSession httpSession = mock(HttpSession.class);

        when(httpServletRequest.getSession()).thenReturn(httpSession);

        eliSamlAuthenticationSuccessHandler.setMethodHooks(singletonList(methodHook));
        eliSamlAuthenticationSuccessHandler.onAuthenticationSuccess(httpServletRequest, httpServletResponse, authentication);

        verify(customerFacade).loginSuccess();
        verify(guidCookieStrategy).setCookie(httpServletRequest, httpServletResponse);
        verify(cartRestorationStrategy).restoreCart(httpServletRequest);
        verify(customerConsentDataStrategy).populateCustomerConsentDataInSession();
        verify(methodHook).doMethodHook(httpServletRequest, httpServletResponse, authentication);
    }
}