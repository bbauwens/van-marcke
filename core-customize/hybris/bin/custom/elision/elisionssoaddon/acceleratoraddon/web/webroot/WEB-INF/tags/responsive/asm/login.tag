<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ attribute name="actionNameKey" required="true" type="java.lang.String" %>
<%@ attribute name="action" required="true" type="java.lang.String" %>

<div class="asmForm col-sm-4 col-xs-12">
    <button type="button" onclick="location.href='${action}'" class="ASM-btn ASM-btn-login"><spring:theme
            code="${actionNameKey}"/></button>
</div>