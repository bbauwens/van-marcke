package eu.elision.sso.security;

import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.security.GUIDCookieStrategy;
import de.hybris.platform.acceleratorstorefrontcommons.strategy.CartRestorationStrategy;
import de.hybris.platform.acceleratorstorefrontcommons.strategy.CustomerConsentDataStrategy;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.core.Constants;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.web.util.CookieGenerator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class ELISamlAuthenticationSuccessHandler implements AuthenticationSuccessHandler {
    private GrantedAuthority adminAuthority = new SimpleGrantedAuthority("ROLE_" + Constants.USER.ADMIN_USERGROUP.toUpperCase());

    private CustomerFacade customerFacade;
    private CartRestorationStrategy cartRestorationStrategy;
    private CustomerConsentDataStrategy customerConsentDataStrategy;
    private GUIDCookieStrategy guidCookieStrategy;
    private CookieGenerator cartRestoreCookieGenerator;
    private List<ELISamlAuthenticationSuccessHandlerMethodHook> methodHooks;

    public ELISamlAuthenticationSuccessHandler(CustomerFacade customerFacade, CartRestorationStrategy cartRestorationStrategy,
                                               CustomerConsentDataStrategy customerConsentDataStrategy, GUIDCookieStrategy guidCookieStrategy,
                                               CookieGenerator cartRestoreCookieGenerator) {
        this.customerFacade = customerFacade;
        this.cartRestorationStrategy = cartRestorationStrategy;
        this.customerConsentDataStrategy = customerConsentDataStrategy;
        this.guidCookieStrategy = guidCookieStrategy;
        this.cartRestoreCookieGenerator = cartRestoreCookieGenerator;
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {
        customerFacade.loginSuccess();
        httpServletRequest.setAttribute(WebConstants.CART_MERGED, Boolean.FALSE);
        guidCookieStrategy.setCookie(httpServletRequest, httpServletResponse);

        if (!isAdminAuthority(authentication)) {
            cartRestorationStrategy.restoreCart(httpServletRequest);
            customerConsentDataStrategy.populateCustomerConsentDataInSession();
            cartRestoreCookieGenerator.removeCookie(httpServletResponse);
            doMethodHooks(httpServletRequest, httpServletResponse, authentication);
        } else {
            invalidateSession(httpServletRequest, httpServletResponse);
        }
    }

    private void doMethodHooks(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) {
        if (CollectionUtils.isNotEmpty(methodHooks)) {
            methodHooks.forEach(v -> v.doMethodHook(httpServletRequest, httpServletResponse, authentication));
        }
    }

    private boolean isAdminAuthority(final Authentication authentication) {
        return CollectionUtils.isNotEmpty(authentication.getAuthorities())
                && authentication.getAuthorities().contains(adminAuthority);
    }

    private void invalidateSession(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
        SecurityContextHolder.getContext().setAuthentication(null);
        request.getSession().invalidate();
        response.sendRedirect(request.getContextPath());
    }

    public void setMethodHooks(List<ELISamlAuthenticationSuccessHandlerMethodHook> methodHooks) {
        this.methodHooks = methodHooks;
    }
}
