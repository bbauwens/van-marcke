package eu.elision.smartExcel.template.populator.typesheet;

import com.hybris.backoffice.excel.template.populator.typesheet.TypeSystemRow;

/**
 * Extended version of the TypeSystemRow, to add locked field
 *
 * @author Elision
 */
public class ElisionTypeSystemRow extends TypeSystemRow {
    private Boolean locked;

    public ElisionTypeSystemRow() {
    }

    public Boolean getLocked() {
        return locked;
    }

    public void setLocked(Boolean locked) {
        this.locked = locked;
    }
}
