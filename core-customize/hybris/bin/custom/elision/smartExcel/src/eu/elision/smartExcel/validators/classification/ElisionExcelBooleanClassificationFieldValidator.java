package eu.elision.smartExcel.validators.classification;

import com.hybris.backoffice.excel.data.ExcelClassificationAttribute;
import com.hybris.backoffice.excel.data.ImportParameters;
import com.hybris.backoffice.excel.validators.classification.ExcelBooleanClassificationFieldValidator;
import com.hybris.backoffice.excel.validators.data.ExcelValidationResult;
import com.hybris.backoffice.excel.validators.data.ValidationMessage;
import org.apache.commons.lang3.StringUtils;

import javax.annotation.Nonnull;
import java.io.Serializable;
import java.util.Map;

import static eu.elision.smartExcel.constants.SmartExcelConstants.Types.BOOLEAN_NOT_AVAILABLE;

/**
 * Custom implementation of the default excelBooleanClassificationFieldValidator
 *
 * @author Elision
 */
public class ElisionExcelBooleanClassificationFieldValidator extends ExcelBooleanClassificationFieldValidator {

    /**
     * {@inheritDoc}
     */
    @Override
    public ExcelValidationResult validate(@Nonnull ExcelClassificationAttribute excelAttribute, @Nonnull ImportParameters importParameters, @Nonnull Map<String, Object> context) {
        if (excelAttribute.getAttributeAssignment().getAttributeType().getCode().contains("boolean") && importParameters.getCellValue().equals(BOOLEAN_NOT_AVAILABLE)) {
            return ExcelValidationResult.SUCCESS;
        }
        boolean isBooleanValue = StringUtils.equalsAnyIgnoreCase(importParameters.getCellValue().toString(), new CharSequence[]{"true", "false"});
        return isBooleanValue ? ExcelValidationResult.SUCCESS : new ExcelValidationResult(new ValidationMessage("excel.import.validation.incorrecttype.boolean", new Serializable[]{importParameters.getCellValue()}));
    }

}
