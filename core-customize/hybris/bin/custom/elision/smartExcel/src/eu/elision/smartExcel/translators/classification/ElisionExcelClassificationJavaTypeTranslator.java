package eu.elision.smartExcel.translators.classification;

import com.hybris.backoffice.excel.data.ExcelClassificationAttribute;
import com.hybris.backoffice.excel.data.ImpexValue;
import com.hybris.backoffice.excel.data.ImportParameters;
import com.hybris.backoffice.excel.importing.ExcelImportContext;
import com.hybris.backoffice.excel.translators.classification.ExcelClassificationJavaTypeTranslator;
import de.hybris.platform.catalog.enums.ClassificationAttributeTypeEnum;
import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.core.model.product.ProductModel;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.assertj.core.util.Lists;
import org.apache.commons.lang3.StringUtils;

import javax.annotation.Nonnull;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Custom implementation of the ExcelClassificationJavaTypeTranslator, in order to be able to all classification attributes to the excel.
 *
 * @author Elision
 */
public class ElisionExcelClassificationJavaTypeTranslator extends ExcelClassificationJavaTypeTranslator {
    private static final Set<ClassificationAttributeTypeEnum> SUPPORTED_TYPES;

    static {
        SUPPORTED_TYPES = EnumSet.of(ClassificationAttributeTypeEnum.NUMBER, ClassificationAttributeTypeEnum.STRING, ClassificationAttributeTypeEnum.DATE);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<String> exportData(@Nonnull ExcelClassificationAttribute excelAttribute, @Nonnull Object objectToExport) {
        if (objectToExport instanceof ProductModel) {
            Feature feature = this.getClassificationService().getFeature((ProductModel) objectToExport, excelAttribute.getAttributeAssignment());
            if (feature != null) {
                return Optional.ofNullable(this.getStreamOfValuesToJoin(excelAttribute, feature).collect(Collectors.joining(",")));
            }
        }
        return Optional.empty();
    }

    @Override
    protected ImpexValue importValue(ExcelClassificationAttribute excelAttribute, ImportParameters importParameters, ExcelImportContext excelImportContext) {
        if (excelAttribute.getAttributeAssignment().getUnit() != null) {
            Pair<ImportParameters, String> pair = this.splitToImportParametersWithoutUnitAndUnit(importParameters, excelAttribute);
            if (pair != null) {
                ImportParameters importParametersWithoutUnit = pair.getLeft();
                String unit = pair.getRight();
                ImpexValue impexValueWithoutUnit = this.importSingle(excelAttribute, importParametersWithoutUnit, excelImportContext);
                if (impexValueWithoutUnit != null) {
                    String impexValueWithUnit = impexValueWithoutUnit.getValue() + ":" + unit;
                    return new ImpexValue(impexValueWithUnit, impexValueWithoutUnit.getHeaderValue());
                }
            }
        }

        return this.importSingle(excelAttribute, importParameters, excelImportContext);
    }

    private Pair<ImportParameters, String> splitToImportParametersWithoutUnitAndUnit(ImportParameters importParameters, ExcelClassificationAttribute excelAttribute) {
        String[] cellValues = getExcelParserSplitter().apply(String.valueOf(importParameters.getCellValue()));
        if (cellValues.length != 2) {
            return null;
        } else {
            String[] rawValues = getExcelParserSplitter().apply(importParameters.getSingleValueParameters().get("rawValue"));
            Map<String, String> paramsWithoutUnit = new LinkedHashMap<>();
            paramsWithoutUnit.putAll(importParameters.getSingleValueParameters());
            paramsWithoutUnit.replace("rawValue", rawValues[0]);

            //if unit is empty, replace with the default unit
            if(StringUtils.isEmpty(rawValues[1])) {
                String defaultUnit = excelAttribute.getAttributeAssignment().getUnit().getCode();
                paramsWithoutUnit.replace("unit", defaultUnit);
            }

            ImportParameters importParametersWithoutUnit = new ImportParameters(importParameters.getTypeCode(), importParameters.getIsoCode(), cellValues[0], importParameters.getEntryRef(), Lists.newArrayList(new Map[]{paramsWithoutUnit}));
            return ImmutablePair.of(importParametersWithoutUnit, paramsWithoutUnit.get("unit"));
        }
    }

}
