package eu.elision.smartExcel.translators;

import com.hybris.backoffice.excel.translators.ExcelEnumTypeTranslator;
import de.hybris.platform.core.HybrisEnumValue;

/**
 * Custom implementation to be able to add the Named Ranges functionality
 *
 * @author Elision
 */
public class ElisionExcelEnumTypeTranslator extends ExcelEnumTypeTranslator implements ElisionExcelValueTranslator<HybrisEnumValue> {

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean canHandleNamedRanges() {
        return true;
    }

}
