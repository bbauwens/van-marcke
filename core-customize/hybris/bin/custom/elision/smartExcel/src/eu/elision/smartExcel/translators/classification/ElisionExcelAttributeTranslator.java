package eu.elision.smartExcel.translators.classification;

import com.hybris.backoffice.excel.data.ExcelAttribute;
import com.hybris.backoffice.excel.translators.ExcelAttributeTranslator;


/**
 * Extension of the default ExcelAttributeTranslator, to be able to add custom functionality.
 *
 * @author Elision
 */
public interface ElisionExcelAttributeTranslator<T extends ExcelAttribute> extends ExcelAttributeTranslator<T> {

    /**
     * Boolean to indicate whether this type can handle Named Ranges.
     * If so, we will add the dropdowns to the sheet for this cell.
     *
     * @return true if this type can handle Named Ranges, false otherwise
     */
    boolean canHandleNamedRanges();

}
