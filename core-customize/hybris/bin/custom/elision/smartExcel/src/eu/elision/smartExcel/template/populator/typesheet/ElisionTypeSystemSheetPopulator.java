package eu.elision.smartExcel.template.populator.typesheet;

import com.hybris.backoffice.excel.data.ExcelExportResult;
import com.hybris.backoffice.excel.template.cell.ExcelCellService;
import com.hybris.backoffice.excel.template.mapper.ExcelMapper;
import com.hybris.backoffice.excel.template.populator.ExcelSheetPopulator;
import com.hybris.backoffice.excel.template.populator.typesheet.TypeSystemSheetPopulator;
import de.hybris.platform.core.model.type.AttributeDescriptorModel;
import de.hybris.platform.core.model.type.DescriptorModel;
import eu.elision.smartExcel.constants.SmartExcelConstants;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.springframework.beans.factory.annotation.Required;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Custom implementation of the TypeSystemSheetPopulator
 *
 * @author Elision
 */
public class ElisionTypeSystemSheetPopulator extends TypeSystemSheetPopulator implements ExcelSheetPopulator {
    private ExcelMapper<ExcelExportResult, AttributeDescriptorModel> mapper;
    private ExcelCellService excelCellService;
    private ElisionTypeSystemRowFactory typeSystemRowFactory;

    public ElisionTypeSystemSheetPopulator() {
    }

    public void populate(@Nonnull ExcelExportResult excelExportResult) {
        Sheet typeSystemSheet = excelExportResult.getWorkbook().getSheet(SmartExcelConstants.UtilitySheet.TYPE_SYSTEM.getSheetName());
        this.populate(typeSystemSheet, this.mapper.apply(excelExportResult));
    }

    protected void populate(Sheet typeSystemSheet, Collection<AttributeDescriptorModel> attributeDescriptors) {
        Map<String, ElisionTypeSystemRow> typeSystemRows = this.customMergeAttributesByQualifier(attributeDescriptors);
        this.customPopulateTypeSystemSheet(typeSystemSheet, typeSystemRows.values());
    }

    private Map<String, ElisionTypeSystemRow> customMergeAttributesByQualifier(Collection<AttributeDescriptorModel> attributes) {
        return attributes.stream().collect(Collectors.toMap(DescriptorModel::getQualifier, this.typeSystemRowFactory::create, this.typeSystemRowFactory::merge));
    }

    private void customPopulateTypeSystemSheet(Sheet typeSystemSheet, Collection<ElisionTypeSystemRow> typeSystemRows) {
        typeSystemRows.forEach((typeSystemRow) -> {
            this.appendTypeSystemRowToSheet(typeSystemSheet, typeSystemRow);
        });
    }

    private void appendTypeSystemRowToSheet(Sheet typeSystemSheet, ElisionTypeSystemRow typeSystemRow) {
        Row row = appendRow(typeSystemSheet);
        this.excelCellService.insertAttributeValue(row.createCell(SmartExcelConstants.TypeSystem.TYPE_CODE.getIndex()), typeSystemRow.getTypeCode());
        this.excelCellService.insertAttributeValue(row.createCell(SmartExcelConstants.TypeSystem.TYPE_NAME.getIndex()), typeSystemRow.getTypeName());
        this.excelCellService.insertAttributeValue(row.createCell(SmartExcelConstants.TypeSystem.ATTR_QUALIFIER.getIndex()), typeSystemRow.getAttrQualifier());
        this.excelCellService.insertAttributeValue(row.createCell(SmartExcelConstants.TypeSystem.ATTR_NAME.getIndex()), typeSystemRow.getAttrName());
        this.excelCellService.insertAttributeValue(row.createCell(SmartExcelConstants.TypeSystem.ATTR_OPTIONAL.getIndex()), typeSystemRow.getAttrOptional());
        this.excelCellService.insertAttributeValue(row.createCell(SmartExcelConstants.TypeSystem.ATTR_TYPE_CODE.getIndex()), typeSystemRow.getAttrTypeCode());
        this.excelCellService.insertAttributeValue(row.createCell(SmartExcelConstants.TypeSystem.ATTR_TYPE_ITEMTYPE.getIndex()), typeSystemRow.getAttrTypeItemType());
        this.excelCellService.insertAttributeValue(row.createCell(SmartExcelConstants.TypeSystem.ATTR_LOCALIZED.getIndex()), typeSystemRow.getAttrLocalized());
        this.excelCellService.insertAttributeValue(row.createCell(SmartExcelConstants.TypeSystem.ATTR_LOC_LANG.getIndex()), typeSystemRow.getAttrLocLang());
        this.excelCellService.insertAttributeValue(row.createCell(SmartExcelConstants.TypeSystem.ATTR_DISPLAYED_NAME.getIndex()), typeSystemRow.getAttrDisplayName());
        this.excelCellService.insertAttributeValue(row.createCell(SmartExcelConstants.TypeSystem.ATTR_UNIQUE.getIndex()), typeSystemRow.getAttrUnique());
        this.excelCellService.insertAttributeValue(row.createCell(SmartExcelConstants.TypeSystem.REFERENCE_FORMAT.getIndex()), typeSystemRow.getAttrReferenceFormat());
        this.excelCellService.insertAttributeValue(row.createCell(SmartExcelConstants.TypeSystem.ATTR_LOCKED.getIndex()), typeSystemRow.getLocked());
    }

    private static Row appendRow(Sheet sheet) {
        return sheet.createRow(sheet.getLastRowNum() + 1);
    }

    @Required
    public void setMapper(ExcelMapper<ExcelExportResult, AttributeDescriptorModel> mapper) {
        this.mapper = mapper;
    }

    @Required
    public void setTypeSystemRowFactory(ElisionTypeSystemRowFactory typeSystemRowFactory) {
        this.typeSystemRowFactory = typeSystemRowFactory;
    }

    @Required
    public void setExcelCellService(ExcelCellService excelCellService) {
        this.excelCellService = excelCellService;
    }
}