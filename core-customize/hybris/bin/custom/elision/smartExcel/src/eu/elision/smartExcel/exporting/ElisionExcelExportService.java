package eu.elision.smartExcel.exporting;

import com.hybris.backoffice.excel.data.SelectedAttribute;
import com.hybris.backoffice.excel.exporting.DefaultExcelExportService;
import com.hybris.backoffice.excel.exporting.ExcelExportService;
import com.hybris.backoffice.excel.template.ExcelTemplateConstants;
import com.hybris.backoffice.excel.translators.ExcelValueTranslator;
import de.hybris.platform.core.HybrisEnumValue;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.type.MapTypeModel;
import de.hybris.platform.enumeration.EnumerationService;
import eu.elision.smartExcel.permissions.BackofficePermissionFacadeStrategy;
import eu.elision.smartExcel.translators.ElisionExcelValueTranslator;
import eu.elision.smartExcel.utility.NamedRangeUtils;
import eu.elision.smartExcel.utility.SheetUtils;
import eu.elision.smartExcel.utility.WorkbookUtils;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.assertj.core.util.Sets;
import org.springframework.beans.factory.annotation.Required;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

import static eu.elision.smartExcel.constants.SmartExcelConstants.Types.*;
import static eu.elision.smartExcel.constants.SmartExcelConstants.UtilitySheet.VALUELISTS_TYPESYSTEM_SHEET;

/**
 * Custom implementation of the DefaultExcelExportService
 *
 * @author Elision
 */
public class ElisionExcelExportService extends DefaultExcelExportService implements ExcelExportService {
    private static final String ENUMERATION_TYPE = "EnumerationMetaType";
    private CellStyle cellStyleLocked;
    private CellStyle cellStyleUnlocked;

    private EnumerationService enumerationService;
    private BackofficePermissionFacadeStrategy permissionFacadeStrategy;

    /**
     * Export the selected data to excel
     *
     * @param itemsByType        all items sorted by type
     * @param selectedAttributes attributes to export
     * @return The workbook with data from the typesystem
     */
    @Override
    protected Workbook exportData(final Map<String, Set<ItemModel>> itemsByType, final List<SelectedAttribute> selectedAttributes) {
        final Map<String, Set<ItemModel>> itemsByTypeFiltered = this.applyTypePredicates(itemsByType);
        final Collection<SelectedAttribute> selectedAttributesFiltered = this.applyAttributePredicates(selectedAttributes);
        final Map<String, Set<SelectedAttribute>> attributesByType = this.getExcelExportDivider().groupAttributesByType(itemsByTypeFiltered.keySet(), selectedAttributesFiltered);
        final Workbook workbook = this.getExcelWorkbookService().createWorkbook(this.loadExcelTemplate());
        //generate styles so we can use them for locking the cells in the excel
        this.cellStyleLocked = SheetUtils.configureCellStyle(workbook.createCellStyle(), true);
        this.cellStyleUnlocked = SheetUtils.configureCellStyle(workbook.createCellStyle(), false);

        //protect the workbook with a password
        WorkbookUtils.protectWorkbook(workbook);

        //first create the sheet with all value lists for the attributes of each itemtype (not for classification yet)
        final XSSFSheet valueListsSheet = (XSSFSheet) workbook.createSheet(VALUELISTS_TYPESYSTEM_SHEET.getSheetName());
        this.addValueLists(workbook, valueListsSheet, attributesByType);
        SheetUtils.protectSheet(valueListsSheet);

        //now do the default stuff
        attributesByType.forEach((typeCode, attributes) -> {
            final Sheet sheet = this.getExcelSheetService().createTypeSheet(workbook, typeCode);
            this.addHeader(sheet, attributes);
            this.addValues(itemsByTypeFiltered, typeCode, attributes, sheet);
            SheetUtils.protectSheet(sheet);
        });
        this.signWorkbookFile(workbook);
        return workbook;
    }

    /**
     * Add values to the sheet. This method is also responsible for creating the dropdowns in the sheet.
     *
     * @param itemsByType        all items sorted by type
     * @param type               hybris typeCode
     * @param selectedAttributes attributes to export
     * @param sheet              the sheet where we want to write the data to
     */
    @Override
    protected void addValues(final Map<String, Set<ItemModel>> itemsByType, final String type, final Set<SelectedAttribute> selectedAttributes, final Sheet sheet) {
        final Sheet pkSheet = this.getExcelSheetService().createOrGetUtilitySheet(sheet.getWorkbook(), ExcelTemplateConstants.UtilitySheet.PK.getSheetName());

        ((Set) itemsByType.get(type)).forEach((item) -> {
            final Row row = this.getExcelSheetService().createEmptyRow(sheet);
            final AtomicInteger cellIndex = new AtomicInteger(0);
            this.insertPkRow(pkSheet, row, (ItemModel) item);
            final Iterator var8 = selectedAttributes.iterator();

            while (var8.hasNext()) {
                final SelectedAttribute selectedAttribute = (SelectedAttribute) var8.next();
                this.getExcelTranslatorRegistry().getTranslator(selectedAttribute.getAttributeDescriptor()).ifPresent((translator) -> {

                    final Optional<Object> optional = translator.exportData(selectedAttribute.getAttributeDescriptor(), getItemAttributeForDescriptor((ItemModel) item, selectedAttribute));
                    if (optional.isPresent()) {
                        optional.ifPresent((excelValue) -> {
                            final Cell cell = row.createCell(cellIndex.get());
                            if (SheetUtils.isNumericField(selectedAttribute.getAttributeDescriptor())) {
                                cell.setCellType(CellType.NUMERIC);
                            }

                            generateDropDownPresetListForSelectedAttributeOnCell(translator, selectedAttribute, sheet, cell);
                            this.getExcelCellService().insertAttributeValue(cell, excelValue);

                            if (!this.permissionFacadeStrategy.canChangeProperty(type, selectedAttribute.getQualifier())) {
                                cell.setCellStyle(this.cellStyleLocked);
                            } else {
                                cell.setCellStyle(this.cellStyleUnlocked);
                            }
                        });
                    } else {
                        //if no value present, just create an empty cell, if we don't do this, the cell-locking will be unpredictable
                        final Cell cell = row.createCell(cellIndex.get());
                        if (SheetUtils.isNumericField(selectedAttribute.getAttributeDescriptor())) {
                            cell.setCellType(CellType.NUMERIC);
                        }
                        generateDropDownPresetListForSelectedAttributeOnCell(translator, selectedAttribute, sheet, cell);
                        this.getExcelCellService().insertAttributeValue(cell, null);
                        if (!this.permissionFacadeStrategy.canChangeProperty(type, selectedAttribute.getQualifier())) {
                            cell.setCellStyle(this.cellStyleLocked);
                        } else {
                            cell.setCellStyle(this.cellStyleUnlocked);
                        }
                    }

                    cellIndex.incrementAndGet();
                });
            }

        });
    }

    private void insertPkRow(final Sheet pkSheet, final Row row, final ItemModel item) {
        final Row emptyRow = this.getExcelSheetService().createEmptyRow(pkSheet);
        this.getExcelCellService().insertAttributeValue(emptyRow.createCell(0), item.getPk().getLongValue());
        this.getExcelCellService().insertAttributeValue(emptyRow.createCell(1), row.getSheet().getSheetName());
        this.getExcelCellService().insertAttributeValue(emptyRow.createCell(2), row.getRowNum());
    }

    /**
     * This will generate the drop down list with presets to choose from for the selected attribute and place it on the given cell
     *
     * @param translator        the translator being used
     * @param selectedAttribute the selected attribute
     * @param sheet             the current sheet in the file
     * @param cell              the cell to populate
     */
    private void generateDropDownPresetListForSelectedAttributeOnCell(final ExcelValueTranslator<Object> translator, final SelectedAttribute selectedAttribute, final Sheet sheet, final Cell cell) {
        if ((translator instanceof ElisionExcelValueTranslator) &&
                (translator).canHandle(selectedAttribute.getAttributeDescriptor()) &&
                ((ElisionExcelValueTranslator) translator).canHandleNamedRanges()) {
            NamedRangeUtils.setCellDropDownList(sheet, selectedAttribute.getQualifier(), cell);
        }
    }

    /**
     * Generate the value lists to create the named ranges in a next phase.
     *
     * @param workbook           the workbook
     * @param sheet              the sheet on which we will add the valueLists
     * @param selectedAttributes the attributes which are selected, to create valueLists from...
     */
    protected void addValueLists(final Workbook workbook, final XSSFSheet sheet, final Map<String, Set<SelectedAttribute>> selectedAttributes) {
        //we should do some stuff here...
        final Map<String, Set<String>> attributesAndValues = new LinkedHashMap<>();

        for (final Map.Entry<String, Set<SelectedAttribute>> entry : selectedAttributes.entrySet()) {
            for (final SelectedAttribute selectedAttribute : entry.getValue()) {
                if (ENUMERATION_TYPE.equalsIgnoreCase(selectedAttribute.getAttributeDescriptor().getAttributeType().getItemtype())) {
                    //we have an enum, add all values of this enum to the list
                    final List<HybrisEnumValue> hybrisEnumValues = this.enumerationService.getEnumerationValues(selectedAttribute.getAttributeDescriptor().getAttributeType().getCode());
                    final Set<String> enumValues = new LinkedHashSet<>();
                    for (final HybrisEnumValue hybrisEnumValue : hybrisEnumValues) {
                        enumValues.add(hybrisEnumValue.getCode());
                    }
                    attributesAndValues.put(selectedAttribute.getQualifier(), enumValues);
                } else if (BOOLEAN_PRIMITIVE_TYPE.equalsIgnoreCase(selectedAttribute.getAttributeDescriptor().getAttributeType().getCode())) {
                    attributesAndValues.put(selectedAttribute.getQualifier(), Sets.newLinkedHashSet(BOOLEAN_PRIMITIVE_TYPE_VALUES));
                } else if (BOOLEAN_TYPE.equalsIgnoreCase(selectedAttribute.getAttributeDescriptor().getAttributeType().getCode())) {
                    attributesAndValues.put(selectedAttribute.getQualifier(), Sets.newLinkedHashSet(BOOLEAN_TYPE_VALUES));
                }
            }
        }

        //get the attributes which have booleans or enums as value, add values to valueList sheet
        final AtomicInteger rowIndex = new AtomicInteger(0);
        final AtomicInteger cellIndex = new AtomicInteger(0);
        final AtomicInteger columnIndex = new AtomicInteger(0);

        NamedRangeUtils.createNamedRange(workbook, sheet, attributesAndValues, rowIndex, cellIndex, columnIndex);
    }

    /**
     *  This method is fixing an OOTB bug in which the export
     *  is not working for Media items because the object returned is Jalo instead of Model,
     *  resulting in ClassCastException.
     *
     * @param item item containing the attribute
     * @param selectedAttribute selectedAttribute
     * @return object representing the selectedAttribute
     */
    private Object getItemAttributeForDescriptor(ItemModel item, SelectedAttribute selectedAttribute) {

        if (selectedAttribute.getAttributeDescriptor().getAttributeType() instanceof MapTypeModel) {
            MapTypeModel mapTypeModel = (MapTypeModel) selectedAttribute.getAttributeDescriptor().getAttributeType();

            if (mapTypeModel.getReturntype().getCode().equals(MediaModel._TYPECODE)) {
                if (selectedAttribute.isLocalized()) {
                    Locale locale = this.getCommonI18NService().getLocaleForIsoCode(selectedAttribute.getIsoCode());
                    return this.getModelService().getAttributeValue(item, selectedAttribute.getQualifier(), locale);
                } else {
                    return this.getModelService().getAttributeValue(item, selectedAttribute.getQualifier());
                }
            }
        }

        return super.getItemAttribute(item, selectedAttribute);
    }

    @Required
    public void setEnumerationService(final EnumerationService enumerationService) {
        this.enumerationService = enumerationService;
    }

    @Required
    public void setPermissionFacadeStrategy(final BackofficePermissionFacadeStrategy platformPermissionFacadeStrategy) {
        this.permissionFacadeStrategy = platformPermissionFacadeStrategy;
    }
}
