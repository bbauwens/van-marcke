package eu.elision.smartExcel.validators.classification;

import com.hybris.backoffice.excel.data.ExcelClassificationAttribute;
import com.hybris.backoffice.excel.data.ImportParameters;
import com.hybris.backoffice.excel.validators.classification.ExcelMandatoryClassificationFieldValidator;
import com.hybris.backoffice.excel.validators.data.ExcelValidationResult;
import com.hybris.backoffice.excel.validators.data.ValidationMessage;
import de.hybris.platform.util.Config;

import javax.annotation.Nonnull;
import java.util.Map;

/**
 * Custom implementation of the ExcelMandatoryClassificationFieldValidator.
 * This version makes it possible to skip validation of classification fields which are marked as "mandatory" during the import.
 *
 * @author Elision
 */
public class ElisionExcelMandatoryClassificationFieldValidator extends ExcelMandatoryClassificationFieldValidator {

    /**
     * Adapted version, this makes it possible to skip the validation of the mandatory fields.
     * This can be enabled by using the 'excel.validation.classification.mandatory.skip' parameter
     *
     * @param excelAttribute the classification attribute
     * @param importParameters import parameters
     * @param context context
     * @return validation result
     */
    @Override
    public ExcelValidationResult validate(@Nonnull ExcelClassificationAttribute excelAttribute, @Nonnull ImportParameters importParameters, @Nonnull Map<String, Object> context) {
        if(Config.getBoolean("excel.validation.classification.mandatory.skip", false)) {
            return ExcelValidationResult.SUCCESS;
        }
        return importParameters.isCellValueBlank() ? new ExcelValidationResult(new ValidationMessage("excel.import.validation.mandatory.classification.field")) : ExcelValidationResult.SUCCESS;
    }

}
