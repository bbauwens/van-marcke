package eu.elision.smartExcel.exporting;

import com.hybris.backoffice.excel.data.ExcelAttribute;
import com.hybris.backoffice.excel.data.ExcelClassificationAttribute;
import com.hybris.backoffice.excel.data.ExcelExportResult;
import com.hybris.backoffice.excel.exporting.DefaultExcelExportClassificationWorkbookDecorator;
import com.hybris.backoffice.excel.exporting.data.ExcelCellValue;
import com.hybris.backoffice.excel.template.populator.DefaultExcelAttributeContext;
import com.hybris.backoffice.excel.translators.ExcelAttributeTranslator;
import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeValueModel;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.product.ProductModel;
import eu.elision.smartExcel.translators.classification.ElisionExcelAttributeTranslator;
import eu.elision.smartExcel.utility.NamedRangeUtils;
import eu.elision.smartExcel.utility.SheetUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static eu.elision.smartExcel.constants.SmartExcelConstants.Types.BOOLEAN_NOT_AVAILABLE;
import static eu.elision.smartExcel.constants.SmartExcelConstants.UtilitySheet.VALUELISTS_CLASSIFICATION_SHEET;

/**
 * Custom version of the ExcelClassificationWorkbookDecorator.
 * This is used to extend the default implementation provided by hybris.
 *
 * @author Elision
 */
public class ElisionExcelExportClassificationWorkbookDecorator extends DefaultExcelExportClassificationWorkbookDecorator {
    private static final Logger LOG = LoggerFactory.getLogger(ElisionExcelExportClassificationWorkbookDecorator.class);
    private CellStyle cellStyleLocked;
    private CellStyle cellStyleUnlocked;

    /**
     * Default constructor
     */
    public ElisionExcelExportClassificationWorkbookDecorator() {
        //empty constructor
    }

    private static List<ExcelClassificationAttribute> extractClassificationAttributes(Collection<ExcelAttribute> attributes) {
        return attributes.stream().filter(ExcelClassificationAttribute.class::isInstance).map(ExcelClassificationAttribute.class::cast).collect(Collectors.toList());
    }

    /**
     * {@inheritDoc}
     * <p>
     * Overriden to be able to add valueLists for the classification to the excel file
     */
    public void decorate(ExcelExportResult excelExportResult) {
        Workbook workbook = excelExportResult.getWorkbook();

        //generate styles so we can use them for locking the cells in the excel
        cellStyleLocked = SheetUtils.configureCellStyle(workbook.createCellStyle(), true);
        cellStyleUnlocked = SheetUtils.configureCellStyle(workbook.createCellStyle(), false);

        Collection<ExcelClassificationAttribute> selectedAdditionalAttributes = extractClassificationAttributes(excelExportResult.getSelectedAdditionalAttributes());

        //first create the sheet with all value lists for the classification system
        XSSFSheet valueListsSheet = (XSSFSheet) workbook.createSheet(VALUELISTS_CLASSIFICATION_SHEET.getSheetName());
        this.addValueLists(workbook, valueListsSheet, selectedAdditionalAttributes);
        SheetUtils.protectSheet(valueListsSheet);

        Collection<ItemModel> selectedItems = excelExportResult.getSelectedItems();
        this.decorate(workbook, selectedAdditionalAttributes, selectedItems);
    }

    /**
     * Override this method to add functionality related to the named ranges to it...
     *
     * @param workbook   the workbook
     * @param attributes attributes to be exported
     * @param items      items to be exported
     */
    protected void decorate(Workbook workbook, Collection<ExcelClassificationAttribute> attributes, Collection<ItemModel> items) {
        Map<ItemModel, Optional<Row>> rowsCache = new HashMap();
        Iterator attributeIterator = attributes.iterator();

        while (attributeIterator.hasNext()) {
            ExcelClassificationAttribute attribute = (ExcelClassificationAttribute) attributeIterator.next();
            String headerValue = this.getAttributeNameFormatter().format(DefaultExcelAttributeContext.ofExcelAttribute(attribute));
            Optional<ExcelAttributeTranslator<ExcelAttribute>> translator = this.getExcelAttributeTranslatorRegistry().findTranslator(attribute);
            String referenceFormat = translator.isPresent() ? translator.get().referenceFormat(attribute) : "";
            Iterator itemModelIterator = items.iterator();

            while (itemModelIterator.hasNext()) {
                ItemModel item = (ItemModel) itemModelIterator.next();
                rowsCache.computeIfAbsent(item, (key) -> this.findRow(workbook, key)).ifPresent((row) -> {
                    Cell headerCell = this.insertHeaderIfNecessary(((Row) row).getSheet(), headerValue);
                    Cell valueCell = this.createCellIfNecessary((Row) row, headerCell.getColumnIndex());

                    this.insertReferenceFormatIfNecessary(valueCell, referenceFormat);
                    ExcelCellValue excelCellValue = new ExcelCellValue(valueCell, attribute, item);
                    boolean featureExistsForProduct = ((ProductModel)item).getClassificationClasses().stream().anyMatch(classificationClass -> classificationClass.getClassificationAttributes().stream().anyMatch(feature -> feature.getCode().equals(attribute.getQualifier())));
                    if(featureExistsForProduct) {
                        if (translator.isPresent()) {
                            translator.ifPresent((t) -> {
                                //check if type is able to handle named ranges, if so, create the dropdown list...
                                if ((t instanceof ElisionExcelAttributeTranslator) && ((ElisionExcelAttributeTranslator) t).canHandleNamedRanges()) {
                                    NamedRangeUtils.setCellDropDownList(((Row) row).getSheet(), headerValue, valueCell);
                                }
                                Cell cell = excelCellValue.getCell();
                                cell.setCellStyle(cellStyleUnlocked);
                                if (SheetUtils.isNumericField(attribute)) {
                                    cell.setCellType(CellType.NUMERIC);
                                }
                                this.exportDataIntoCell(t, excelCellValue);
                            });
                        } else {
                            Cell cell = excelCellValue.getCell();
                            cell.setCellStyle(cellStyleLocked);
                            if (SheetUtils.isNumericField(attribute)) {
                                cell.setCellType(CellType.NUMERIC);
                            }
                            this.getExcelCellService().insertAttributeValue(cell, null);
                        }
                    } else {
                        //feature does not exist for this product, lock the cell
                        Cell cell = excelCellValue.getCell();
                        cell.setCellStyle(cellStyleLocked);
                        this.getExcelCellService().insertAttributeValue(cell, null);
                    }

                });
            }
        }
    }

    /**
     * Create the named ranges to be used in the Excel file. Named ranges will populate the drop downs as reference
     *
     * @param workbook       the workbook
     * @param valueListSheet the sheet on which we place the references
     */
    protected void addValueLists(Workbook workbook, XSSFSheet valueListSheet, Collection<ExcelClassificationAttribute> additionalAttributes) {
        LOG.debug("Create valueLists for type system.");
        Map<String, Set<String>> classificationAttributesAndValues = new LinkedHashMap<>();
        Map<String, ClassAttributeAssignmentModel> classAttributeAssignmentModels = new LinkedHashMap<>();

        for (ExcelClassificationAttribute additionalAttribute : additionalAttributes) {
            String code = this.getAttributeNameFormatter().format(DefaultExcelAttributeContext.ofExcelAttribute(additionalAttribute));
            switch (additionalAttribute.getType().toLowerCase()) {
                case "boolean":
                    //directly add this to the classificationAttributesAndValues map
                    String[] booleanValues = {BOOLEAN_NOT_AVAILABLE, "true", "false"};
                    classificationAttributesAndValues.put(code, Arrays.stream(booleanValues).collect(Collectors.toSet()));
                    break;
                default:
                    if (additionalAttribute.getAttributeAssignment() != null) {
                        classAttributeAssignmentModels.put(code, additionalAttribute.getAttributeAssignment());
                    }
            }
        }

        for (Map.Entry<String, ClassAttributeAssignmentModel> attributeAssignmentModelEntry : classAttributeAssignmentModels.entrySet()) {
            ClassAttributeAssignmentModel classAttributeAssignment = attributeAssignmentModelEntry.getValue();
            ClassificationAttributeModel classificationAttribute = classAttributeAssignment.getClassificationAttribute();
            Set<String> classificationAttributeValuesForCurrentAttribute = new LinkedHashSet<>();

            for (ClassificationAttributeValueModel attributeValue : classAttributeAssignment.getAttributeValues()) {
                classificationAttributeValuesForCurrentAttribute.add(attributeValue.getName());
            }

            for (ClassificationAttributeValueModel classificationAttributeValue : classificationAttribute.getDefaultAttributeValues()) {
                classificationAttributeValuesForCurrentAttribute.add(classificationAttributeValue.getName());
            }

            if (CollectionUtils.isNotEmpty(classificationAttributeValuesForCurrentAttribute)) {
                classificationAttributesAndValues.put(attributeAssignmentModelEntry.getKey(), classificationAttributeValuesForCurrentAttribute);
            }
        }

        AtomicInteger rowIndex = new AtomicInteger(0);
        AtomicInteger cellIndex = new AtomicInteger(0);
        AtomicInteger columnIndex = new AtomicInteger(0);

        NamedRangeUtils.createNamedRange(workbook, valueListSheet, classificationAttributesAndValues, rowIndex, cellIndex, columnIndex);
    }

}
