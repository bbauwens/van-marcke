package eu.elision.smartExcel.translators;

import com.hybris.backoffice.excel.translators.ExcelValueTranslator;
import de.hybris.platform.core.model.type.AttributeDescriptorModel;

import java.util.Optional;

/**
 * Extended version of the ExcelValueTranslator, this is to be able to wire the namedRange functionality
 *
 * @author Elision
 */
public interface ElisionExcelValueTranslator<T> extends ExcelValueTranslator<T> {

    /**
     * flag to indicate whether this type can handle named ranges.
     * Also make sure to call the canHandle() method, to make sure translator can handle the provided attribute type. This only indicates whether it can work with named ranges.
     *
     * @return true if it can handle it...
     */
    default boolean canHandleNamedRanges() {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    default Optional<Object> exportData(AttributeDescriptorModel attributeDescriptorModel, T objectToExport) {
        return this.exportData(objectToExport);
    }
}
