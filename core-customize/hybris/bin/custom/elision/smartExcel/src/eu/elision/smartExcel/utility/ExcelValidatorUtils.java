package eu.elision.smartExcel.utility;

import com.hybris.backoffice.excel.data.ImportParameters;
import com.hybris.backoffice.excel.importing.parser.RangeParserUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * Copied from com.hybris.backoffice.excel.validators.classification to enable access
 *
 * @author Elision
 */
public class ExcelValidatorUtils {
    private ExcelValidatorUtils() {
    }

    public static boolean isMultivalue(ImportParameters importParameters) {
        return StringUtils.contains(String.valueOf(importParameters.getCellValue()), ",");
    }

    public static boolean isNotRange(ImportParameters importParameters) {
        return !RangeParserUtils.RANGE_PATTERN.matcher(String.valueOf(importParameters.getCellValue())).matches();
    }

    public static boolean isNotMultivalue(ImportParameters importParameters) {
        return !isMultivalue(importParameters);
    }

    public static boolean hasUnit(ImportParameters importParameters) {
        return importParameters.getSingleValueParameters().containsKey("unit");
    }
}
