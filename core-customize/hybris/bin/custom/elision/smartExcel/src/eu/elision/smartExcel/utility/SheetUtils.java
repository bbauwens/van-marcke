package eu.elision.smartExcel.utility;

import com.hybris.backoffice.excel.data.ExcelClassificationAttribute;
import de.hybris.platform.core.model.type.AttributeDescriptorModel;
import de.hybris.platform.util.Config;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Utility class to use for Sheet related tasks
 *
 * @author Elision
 */
public class SheetUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(SheetUtils.class);
    private static final String[] numericTypes = {"java.lang.Integer", "java.lang.Double", "number"};

    /**
     * Method which handles sheet protection. All lock options can be configured in the property files.
     *
     * @param sheet the sheet to protected
     */
    public static void protectSheet(Sheet sheet) {
        LOGGER.debug("Protecting sheet {}", sheet.getSheetName());

        if (Config.getBoolean("excel.lock.sheet.enabled", true)) {
            XSSFSheet xssfSheet = (XSSFSheet) sheet;

            xssfSheet.lockAutoFilter(Config.getBoolean("excel.lock.sheet.autoFilter", false));
            xssfSheet.lockDeleteColumns(Config.getBoolean("excel.lock.sheet.deleteColumns", false));
            xssfSheet.lockDeleteRows(Config.getBoolean("excel.lock.sheet.deleteRows", false));
            xssfSheet.lockFormatCells(Config.getBoolean("excel.lock.sheet.formatCells", false));
            xssfSheet.lockFormatColumns(Config.getBoolean("excel.lock.sheet.formatColumns", false));
            xssfSheet.lockFormatRows(Config.getBoolean("excel.lock.sheet.formatRows", false));
            xssfSheet.lockInsertColumns(Config.getBoolean("excel.lock.sheet.insertColumns", false));
            xssfSheet.lockInsertHyperlinks(Config.getBoolean("excel.lock.sheet.insertHyperlinks", false));
            xssfSheet.lockInsertRows(Config.getBoolean("excel.lock.sheet.insertRows", false));
            xssfSheet.lockPivotTables(Config.getBoolean("excel.lock.sheet.pivotTables", false));
            xssfSheet.lockSort(Config.getBoolean("excel.lock.sheet.sort", false));
            xssfSheet.lockObjects(Config.getBoolean("excel.lock.sheet.objects", false));
            xssfSheet.lockScenarios(Config.getBoolean("excel.lock.sheet.scenarios", false));
            xssfSheet.lockSelectLockedCells(Config.getBoolean("excel.lock.sheet.selectLockedCells", false));
            xssfSheet.lockSelectUnlockedCells(Config.getBoolean("excel.lock.sheet.selectUnlockedCells", false));

            sheet.protectSheet(Config.getString("excel.sheet.password", ""));
        }
    }

    /**
     * Create a locked cell style, this configures how these cells will look like in the excel.
     * By default the users will not see the colors, these are only visible when you unlock the sheet and remove all conditional formatting.
     *
     * @param style  the style to set...
     * @param locked boolean to indicate whether the cell should be locked
     */
    public static CellStyle configureCellStyle(CellStyle style, boolean locked) {
        style.setLocked(locked);
        style.setWrapText(Config.getBoolean("excel.sheet.textWrapping.enabled", false));
        if (locked) {
            style.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
            style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        } else {
            style.setFillForegroundColor(IndexedColors.LIGHT_CORNFLOWER_BLUE.getIndex());
            style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        }
        return style;
    }

    /**
     * Add filter headers to the sheet.
     *
     * @param sheet the sheet to which we want to add the filter headers
     */
    public static void addAutoFilter(Sheet sheet) {
        if (Config.getBoolean("excel.sheet.autofilters.enabled", true)) {
            int startRow = 2;
            int lastRow = sheet.getLastRowNum();
            int startCol = sheet.getRow(0).getFirstCellNum();
            int lastCol = sheet.getRow(0).getLastCellNum() - 1;
            CellRangeAddress cra = new CellRangeAddress(startRow, lastRow, startCol, lastCol);
            sheet.setAutoFilter(cra);
        }
    }

    /**
     * Check whether this field is a numeric field, if so, we will create a NUMERIC cell instead of a regular cell.
     *
     * @param attributeDescriptorModel attribute to write
     * @return true if field is numeric field
     */
    public static boolean isNumericField(AttributeDescriptorModel attributeDescriptorModel) {
        return ArrayUtils.contains(numericTypes, attributeDescriptorModel.getAttributeType().getCode());
    }

    /**
     * Check whether this field is a numeric field, if so, we will create a NUMERIC cell instead of a regular cell.
     * This method handles the classification attribute
     *
     * @param attribute classification attribute to write
     * @return true if field is numeric field
     */
    public static boolean isNumericField(ExcelClassificationAttribute attribute) {
        return ArrayUtils.contains(numericTypes, attribute.getType());
    }
}
