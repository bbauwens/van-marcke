package eu.elision.smartExcel.utility;

import com.hybris.backoffice.excel.data.ImportParameters;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Copied from com.hybris.backoffice.excel.validators.classification.ExcelUnitUtils to enable access
 *
 * @author Elision
 */
public class ExcelUnitUtils {
    static final String UNIT_KEY = "unit";
    static final String VALUE_KEY = "value";

    private ExcelUnitUtils() {
        throw new AssertionError();
    }

    public static ImportParameters getImportParametersForValue(ImportParameters importParameters, String cellValue) {
        String typeCode = importParameters.getTypeCode();
        String isoCode = importParameters.getIsoCode();
        String entryRef = importParameters.getEntryRef();
        List<Map<String, String>> multiValueParametersWithoutUnits = (List) importParameters.getMultiValueParameters().stream().peek((m) -> {
            m.remove("unit");
        }).collect(Collectors.toList());
        return new ImportParameters(typeCode, isoCode, cellValue, entryRef, multiValueParametersWithoutUnits);
    }

    public static String extractUnitFromParams(Map<String, String> params) {
        return (String) params.getOrDefault("unit", "");
    }

    public static String extractValueFromParams(Map<String, String> params) {
        return (String) params.getOrDefault("value", "");
    }
}
