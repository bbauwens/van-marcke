package eu.elision.smartExcel.importing;

import com.hybris.backoffice.excel.data.ExcelAttribute;
import com.hybris.backoffice.excel.data.ImportParameters;
import com.hybris.backoffice.excel.importing.DefaultExcelImportClassificationWorkbookDecorator;
import com.hybris.backoffice.excel.importing.parser.ExcelParserException;
import com.hybris.backoffice.excel.importing.parser.ParsedValues;
import com.hybris.backoffice.excel.translators.ExcelAttributeTranslator;
import com.hybris.backoffice.excel.validators.ExcelAttributeValidator;
import com.hybris.backoffice.excel.validators.data.ExcelValidationResult;
import com.hybris.backoffice.excel.validators.util.ExcelValidationResultUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * Custom implementation of the classificationWorkbookDecorator
 *
 * @author Elision
 */
public class ElisionExcelImportClassificationWorkbookDecorator extends DefaultExcelImportClassificationWorkbookDecorator {
    private List<ExcelAttributeValidator<? extends ExcelAttribute>> validators;
    private static Logger LOG = LoggerFactory.getLogger(ElisionExcelImportClassificationWorkbookDecorator.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ExcelValidationResult> validate(Workbook workbook) {
        List<ExcelValidationResult> results = new ArrayList<>();
        DecoratorConsumer consumer = (excelAttribute, row, columnIndex, typeCode) -> {
            Cell cell = getCell(row, columnIndex);
            ImportParameters importParameters = this.getImportParameters(typeCode, excelAttribute, cell);
            Map<String, Object> validationContext = new HashMap<>();
            Iterator validatorIterator = validators.iterator();

            while(validatorIterator.hasNext()) {
                ExcelAttributeValidator validator = (ExcelAttributeValidator)validatorIterator.next();
                if (!cell.getCellStyle().getLocked() && validator.canHandle(excelAttribute, importParameters)) {
                    ExcelValidationResult validationResult = validator.validate(excelAttribute, importParameters, validationContext);
                    if (validationResult != null && validationResult.hasErrors()) {
                        ExcelValidationResultUtil.insertHeaderIfNeeded(validationResult, row.getRowNum() + 1, typeCode, excelAttribute.getName());
                        results.add(validationResult);
                    }
                }
            }

        };
        this.consumeWorkbook(workbook, consumer);
        return results;
    }

    private static Cell getCell(Row row, int columnIndex) {
        Cell cell = row.getCell(columnIndex);
        return cell != null ? cell : row.createCell(columnIndex);
    }

    private ImportParameters getImportParameters(String typeCode, ExcelAttribute excelAttribute, Cell cell) {
        Optional<ExcelAttributeTranslator<ExcelAttribute>> translator = this.getExcelAttributeTranslatorRegistry().findTranslator(excelAttribute);
        int columnIndex = cell.getColumnIndex();
        String values = this.getExcelCellService().getCellValue(cell);
        String referenceFormat = translator.map((t) -> {
            return t.referenceFormat(excelAttribute);
        }).orElse("");

        try {
            String defaultValues = this.getExcelCellService().getCellValue(cell.getSheet().getRow(2).getCell(columnIndex));
            ParsedValues parsedValues = this.getParserRegistry().getParser(referenceFormat).parseValue(referenceFormat, defaultValues, values);
            return new ImportParameters(typeCode, excelAttribute.getIsoCode(), parsedValues.getCellValue(), null, parsedValues.getParameters());
        } catch (ExcelParserException ex) {
            LOG.warn("Error while parsing excel.", ex);
            return new ImportParameters(typeCode, excelAttribute.getIsoCode(), ex.getCellValue(), null, ex.getExpectedFormat());
        }
    }

    private void consumeWorkbook(Workbook workbook, DecoratorConsumer decoratorConsumer) {
        Iterator var4 = this.getExcelSheetService().getSheets(workbook).iterator();

        while(var4.hasNext()) {
            Sheet sheet = (Sheet)var4.next();
            this.consumeSheet(decoratorConsumer, sheet);
        }

    }

    private void consumeSheet(DecoratorConsumer decoratorConsumer, Sheet sheet) {
        Iterator var4 = this.getExcelAttributes(sheet).iterator();

        while(var4.hasNext()) {
            ExcelAttribute excelAttribute = (ExcelAttribute)var4.next();
            this.consumeAttribute(decoratorConsumer, sheet, excelAttribute);
        }

    }

    private void consumeAttribute(DecoratorConsumer decoratorConsumer, Sheet sheet, ExcelAttribute excelAttribute) {
        Optional<Integer> foundColumn = this.findColumnIndex(sheet.getRow(0), excelAttribute.getName());
        foundColumn.ifPresent((columnIndex) -> {
            for(int rowIndex = 3; rowIndex <= sheet.getLastRowNum(); ++rowIndex) {
                Row row = sheet.getRow(rowIndex);
                if (this.hasContent(row)) {
                    String typeCode = this.getExcelSheetService().findTypeCodeForSheetName(sheet.getWorkbook(), sheet.getSheetName());
                    decoratorConsumer.consume(excelAttribute, row, columnIndex, typeCode);
                }
            }

        });
    }

    private boolean hasContent(Row row) {
        for(int columnIndex = row.getFirstCellNum(); columnIndex <= row.getLastCellNum(); ++columnIndex) {
            if (StringUtils.isNotBlank(this.getExcelCellService().getCellValue(row.getCell(columnIndex)))) {
                return true;
            }
        }

        return false;
    }

    @FunctionalInterface
    private interface DecoratorConsumer {
        void consume(ExcelAttribute var1, Row var2, Integer var3, String var4);
    }

    @Override
    public void setValidators(List<ExcelAttributeValidator<? extends ExcelAttribute>> validators) {
        super.setValidators(validators);
        this.validators = validators;
    }
}
