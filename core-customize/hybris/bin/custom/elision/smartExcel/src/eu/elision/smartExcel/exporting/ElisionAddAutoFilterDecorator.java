package eu.elision.smartExcel.exporting;

import com.hybris.backoffice.excel.data.ExcelExportResult;
import com.hybris.backoffice.excel.exporting.ExcelExportWorkbookDecorator;
import eu.elision.smartExcel.constants.SmartExcelConstants.UtilitySheet;
import eu.elision.smartExcel.utility.SheetUtils;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.util.Collection;
import java.util.Collections;

/**
 * Decorator which adds the auto filter functionality to the sheets
 *
 * @author Elision
 */
public class ElisionAddAutoFilterDecorator implements ExcelExportWorkbookDecorator {
    protected Collection<UtilitySheet> excludedSheets = Collections.emptyList();

    /**
     * {@inheritDoc}
     */
    @Override
    public void decorate(ExcelExportResult excelExportResult) {
        Workbook workbook = excelExportResult.getWorkbook();

        for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
            Sheet sheet = workbook.getSheetAt(i);
            if (!UtilitySheet.isUtilitySheet(excludedSheets, sheet.getSheetName())) {
                SheetUtils.addAutoFilter(sheet);
            }
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getOrder() {
        return 1000;
    }

    /**
     * Set sheets to exclude
     *
     * @param excludedSheets sheets to exclude
     */
    public void setExcludedSheets(Collection<UtilitySheet> excludedSheets) {
        this.excludedSheets = excludedSheets;
    }

}
