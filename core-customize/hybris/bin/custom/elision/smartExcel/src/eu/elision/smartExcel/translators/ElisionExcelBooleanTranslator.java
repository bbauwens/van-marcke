package eu.elision.smartExcel.translators;

import com.hybris.backoffice.excel.data.ImpexHeaderValue;
import com.hybris.backoffice.excel.data.ImpexValue;
import com.hybris.backoffice.excel.data.ImportParameters;
import com.hybris.backoffice.excel.translators.ExcelJavaTypeTranslator;
import de.hybris.platform.core.model.type.AttributeDescriptorModel;

import java.io.Serializable;
import java.util.Optional;

import static eu.elision.smartExcel.constants.SmartExcelConstants.Types.*;

/**
 * Boolean translator to be able to link the cells to the named ranges.
 *
 * @author Elision
 */
public class ElisionExcelBooleanTranslator extends ExcelJavaTypeTranslator implements ElisionExcelValueTranslator<Object> {

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean canHandle(AttributeDescriptorModel attributeDescriptorModel) {
        return BOOLEAN_PRIMITIVE_TYPE.equalsIgnoreCase(attributeDescriptorModel.getAttributeType().getCode()) || BOOLEAN_TYPE.equalsIgnoreCase(attributeDescriptorModel.getAttributeType().getCode());
    }

    /**
     * Adapted version of the exportData method, this to be able to render "N/A" when a Boolean is null.
     *
     * @param objectToExport data to export to excel
     * @return optional
     */
    public Optional<Object> exportData(Object objectToExport) {
        if (objectToExport == null) {
            objectToExport = BOOLEAN_NOT_AVAILABLE;
        }
        return Optional.ofNullable(objectToExport);
    }

    /**
     * Adapted version of the importData method, to be able to handle the "N/A" value.
     *
     * @param attributeDescriptor attribute descriptor
     * @param importParameters    import parameters
     * @return impex value object
     */
    public ImpexValue importValue(AttributeDescriptorModel attributeDescriptor, ImportParameters importParameters) {
        Serializable value = importParameters.getCellValue().equals(BOOLEAN_NOT_AVAILABLE) ? "" : importParameters.getCellValue();
        return new ImpexValue(value,
                (new ImpexHeaderValue.Builder(attributeDescriptor.getQualifier())).withMandatory(this.getMandatoryFilter().test(attributeDescriptor)).withLang(importParameters.getIsoCode()).withQualifier(attributeDescriptor.getQualifier()).build());
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public boolean canHandleNamedRanges() {
        return true;
    }

}
