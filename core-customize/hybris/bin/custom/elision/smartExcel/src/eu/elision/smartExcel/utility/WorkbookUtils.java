package eu.elision.smartExcel.utility;

import de.hybris.platform.util.Config;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * Utility class to use for workbook related tasks
 *
 * @author Elision
 */
public class WorkbookUtils {

    /**
     * Workbook protection, whether this needs to happen can be configured in the property files.
     * This method will set a password to the workbook and lock the structure.
     *
     * @param workbook the workbook to protect
     */
    public static void protectWorkbook(Workbook workbook) {
        if (Config.getBoolean("excel.lock.workbook.structure", false)) {
            XSSFWorkbook xssfWorkbook = (XSSFWorkbook) workbook;
            xssfWorkbook.lockStructure();
            xssfWorkbook.setWorkbookPassword(Config.getString("excel.sheet.password", ""), null);
        }
    }

}
