package eu.elision.smartExcel.translators;

import com.hybris.backoffice.excel.data.ImpexValue;
import com.hybris.backoffice.excel.data.ImportParameters;
import com.hybris.backoffice.excel.translators.AbstractCatalogVersionAwareTranslator;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.type.AttributeDescriptorModel;
import de.hybris.platform.core.model.type.CollectionTypeModel;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.NotImplementedException;
import org.apache.commons.lang3.StringUtils;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Custom Excel Export translator for media container collection
 *
 * @author Cristian Stoica
 * @since 11-11-2021
 */
public class ElisionExcelMediaContainerCollectionImportTranslator extends AbstractCatalogVersionAwareTranslator<Collection<MediaContainerModel>> {

    private ElisionExcelMediaContainerImportTranslator elisionExcelMediaContainerImportTranslator;

    public ElisionExcelMediaContainerCollectionImportTranslator(ElisionExcelMediaContainerImportTranslator elisionExcelMediaContainerImportTranslator) {
        this.elisionExcelMediaContainerImportTranslator = elisionExcelMediaContainerImportTranslator;
    }


    @Override
    public boolean canHandle(AttributeDescriptorModel attributeDescriptor) {
        return attributeDescriptor.getAttributeType() instanceof CollectionTypeModel && this.getTypeService().isAssignableFrom(((CollectionTypeModel)attributeDescriptor.getAttributeType()).getElementType().getCode(), "MediaContainer");
    }

    public Optional<Object> exportData(Collection<MediaContainerModel> mediasToExport) {
        if (CollectionUtils.isEmpty(mediasToExport)) {
            return Optional.empty();
        } else {
            String export = mediasToExport.stream().map(elisionExcelMediaContainerImportTranslator::exportMedia)
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                    .collect(Collectors.joining(","));
            return StringUtils.isNotBlank(export) ? Optional.of(export) : Optional.empty();
        }
    }

    @Override
    public String referenceFormat(AttributeDescriptorModel attributeDescriptor) {
        return elisionExcelMediaContainerImportTranslator.referenceFormat(attributeDescriptor);
    }

    @Override
    public ImpexValue importValue(AttributeDescriptorModel attributeDescriptorModel, ImportParameters importParameters) {
        throw new NotImplementedException();
    }
}
