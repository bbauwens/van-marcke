package eu.elision.smartExcel.template.sheet;

import com.google.common.collect.ImmutableList;
import com.hybris.backoffice.excel.template.sheet.DefaultExcelSheetService;
import eu.elision.smartExcel.constants.SmartExcelConstants.UtilitySheet;
import org.apache.poi.ss.usermodel.Workbook;

import javax.annotation.WillNotClose;
import java.util.Collection;
import java.util.Collections;
import java.util.stream.IntStream;

/**
 * Custom version of the DefaultExcelSheetService. This is needed to be able to extend the UtilitySheet list
 *
 * @author Elision
 */
public class ElisionExcelSheetService extends DefaultExcelSheetService {
    protected Collection<UtilitySheet> excludedSheets = Collections.emptyList();

    /**
     * {@inheritDoc}
     */
    public Collection<String> getSheetsNames(@WillNotClose Workbook workbook) {
        return (Collection) IntStream.range(0, workbook.getNumberOfSheets()).mapToObj(workbook::getSheetName).filter((sheetName) -> {
            return !UtilitySheet.isUtilitySheet(this.excludedSheets, sheetName);
        }).collect(ImmutableList.toImmutableList());
    }

    /**
     * Set sheets to exclude
     *
     * @param excludedSheets sheets to exclude
     */
    public void setCustomExcludedSheets(Collection<UtilitySheet> excludedSheets) {
        this.excludedSheets = excludedSheets;
    }

}
