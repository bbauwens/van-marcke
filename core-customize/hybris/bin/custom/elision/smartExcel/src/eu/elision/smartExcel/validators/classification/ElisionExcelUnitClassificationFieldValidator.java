package eu.elision.smartExcel.validators.classification;

import com.hybris.backoffice.excel.data.ExcelClassificationAttribute;
import com.hybris.backoffice.excel.data.ImportParameters;
import com.hybris.backoffice.excel.validators.ExcelAttributeValidator;
import com.hybris.backoffice.excel.validators.classification.ExcelUnitClassificationFieldValidator;
import com.hybris.backoffice.excel.validators.data.ExcelValidationResult;
import com.hybris.backoffice.excel.validators.data.ValidationMessage;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeUnitModel;
import de.hybris.platform.catalog.model.classification.ClassificationSystemVersionModel;
import de.hybris.platform.classification.ClassificationSystemService;
import eu.elision.smartExcel.utility.ExcelUnitUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Required;

import javax.annotation.Nonnull;
import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Custom version of the ExcelUnitClassificationFieldValidator, this implementation will add the default unit if no unit provided in the excel...
 *
 * @author Elision
 */
public class ElisionExcelUnitClassificationFieldValidator extends ExcelUnitClassificationFieldValidator {

    private static final String CACHE_KEY_PATTERN = "PossibleUnitsOf:%s:%s:%s";
    private static final String INVALID_VALUE_MESSAGE_KEY = "excel.import.validation.workbook.value.not.valid";
    private List<ExcelAttributeValidator<ExcelClassificationAttribute>> validators = Collections.emptyList();
    private ClassificationSystemService classificationSystemService;
    private static String createUnitsCacheKey(ClassificationSystemVersionModel systemVersion, String unitType) {
        return String.format(CACHE_KEY_PATTERN, systemVersion.getCatalog().getId(), systemVersion.getVersion(), unitType);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ExcelValidationResult validate(@Nonnull ExcelClassificationAttribute excelAttribute, @Nonnull ImportParameters importParameters, @Nonnull Map<String, Object> context) {
        LinkedList<ValidationMessage> validationErrors = new LinkedList<>();
        List<String> allPossibleUnits = this.getAllPossibleUnitsForType(context, excelAttribute);
        Map<String, String> singleValueParams = importParameters.getSingleValueParameters();
        String unit = ExcelUnitUtils.extractUnitFromParams(singleValueParams);
        String value = ExcelUnitUtils.extractValueFromParams(singleValueParams);
        ImportParameters importParametersForValue = ExcelUnitUtils.getImportParametersForValue(importParameters, value);
        List<ValidationMessage> validationMessages = this.executeValidators(excelAttribute, importParametersForValue, context);
        validationErrors.addAll(validationMessages);
        //Check if the value is a valid number
        if (StringUtils.isNotBlank(value) && !NumberUtils.isCreatable(value)) {
            validationErrors.add(new ValidationMessage(INVALID_VALUE_MESSAGE_KEY, new Serializable[]{value}));
        }
        //Check if it is a valid unit
        if (StringUtils.isNotBlank(value) && !(allPossibleUnits.contains(unit) || StringUtils.isEmpty(unit))) {
            validationErrors.add(new ValidationMessage(INVALID_UNIT_MESSAGE_KEY, new Serializable[]{unit, excelAttribute.getAttributeAssignment().getUnit().getUnitType()}));
        }

        return new ExcelValidationResult(validationErrors);
    }

    private List<String> getAllPossibleUnitsForType(Map<String, Object> context, ExcelClassificationAttribute excelAttribute) {
        ClassificationSystemVersionModel systemVersion = excelAttribute.getAttributeAssignment().getSystemVersion();
        String unitType = excelAttribute.getAttributeAssignment().getUnit().getUnitType();
        String cacheKey = createUnitsCacheKey(systemVersion, unitType);
        if (!context.containsKey(cacheKey)) {
            List<String> units = this.loadUnitsForTypeOf(systemVersion, unitType);
            context.put(cacheKey, units);
            return units;
        } else {
            return (List<String>) context.get(cacheKey);
        }
    }

    private List<String> loadUnitsForTypeOf(ClassificationSystemVersionModel systemVersion, String unitType) {
        return classificationSystemService.getUnitsOfTypeForSystemVersion(systemVersion, unitType).stream().map(ClassificationAttributeUnitModel::getCode).collect(Collectors.toList());
    }

    private List<ValidationMessage> executeValidators(ExcelClassificationAttribute excelAttribute, ImportParameters importParameters, Map<String, Object> context) {
        return this.validators.stream().filter((validator) -> {
            return validator.canHandle(excelAttribute, importParameters);
        }).map((validator) -> {
            return validator.validate(excelAttribute, importParameters, context);
        }).map(ExcelValidationResult::getValidationErrors).flatMap(Collection::stream).collect(Collectors.toList());
    }

    @Required
    @Override
    public void setClassificationSystemService(ClassificationSystemService classificationSystemService) {
        super.setClassificationSystemService(classificationSystemService);
        this.classificationSystemService = classificationSystemService;
    }

}
