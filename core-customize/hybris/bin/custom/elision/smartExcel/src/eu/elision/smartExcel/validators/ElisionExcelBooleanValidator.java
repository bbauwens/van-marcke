package eu.elision.smartExcel.validators;

import com.hybris.backoffice.excel.data.ImportParameters;
import com.hybris.backoffice.excel.validators.ExcelBooleanValidator;
import com.hybris.backoffice.excel.validators.data.ExcelValidationResult;
import com.hybris.backoffice.excel.validators.data.ValidationMessage;
import de.hybris.platform.core.model.type.AttributeDescriptorModel;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.Map;

import static eu.elision.smartExcel.constants.SmartExcelConstants.Types.BOOLEAN_NOT_AVAILABLE;

/**
 * Custom version of the BooleanValidator
 *
 * @author Elision
 */
public class ElisionExcelBooleanValidator extends ExcelBooleanValidator {

    public ExcelValidationResult validate(ImportParameters importParameters, AttributeDescriptorModel attributeDescriptor, Map<String, Object> context) {
        if (attributeDescriptor.getAttributeType().getCode().contains("Boolean") && importParameters.getCellValue().equals(BOOLEAN_NOT_AVAILABLE)) {
            return ExcelValidationResult.SUCCESS;
        }
        boolean isBooleanValue = StringUtils.equalsAnyIgnoreCase(importParameters.getCellValue().toString(), new CharSequence[]{"true", "false"});
        return isBooleanValue ? ExcelValidationResult.SUCCESS : new ExcelValidationResult(new ValidationMessage("excel.import.validation.incorrecttype.boolean",
                new Serializable[]{importParameters.getCellValue()}));
    }

}
