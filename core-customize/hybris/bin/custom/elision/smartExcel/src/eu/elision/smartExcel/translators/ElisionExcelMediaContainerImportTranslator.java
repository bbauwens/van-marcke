package eu.elision.smartExcel.translators;

import com.hybris.backoffice.excel.data.ImpexValue;
import com.hybris.backoffice.excel.data.ImportParameters;
import com.hybris.backoffice.excel.translators.AbstractCatalogVersionAwareTranslator;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.type.AttributeDescriptorModel;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.NotImplementedException;

import java.util.Optional;

/**
 * Custom Excel Export translator for media container
 *
 * @author Cristian Stoica
 * @since 11-11-2021
 */
public class ElisionExcelMediaContainerImportTranslator extends AbstractCatalogVersionAwareTranslator<MediaContainerModel> {


    ElisionExcelMediaImportTranslator elisionExcelMediaImportTranslator;

    private static final String PREVIEW = "PREVIEW";

    public ElisionExcelMediaContainerImportTranslator(ElisionExcelMediaImportTranslator elisionExcelMediaImportTranslator) {
        this.elisionExcelMediaImportTranslator = elisionExcelMediaImportTranslator;
    }

    @Override
    public boolean canHandle(AttributeDescriptorModel attributeDescriptor) {
        return this.getTypeService().isAssignableFrom("MediaContainer", attributeDescriptor.getAttributeType().getCode()) || this.isLocalizedOfType(attributeDescriptor, "MediaContainer");
    }

    @Override
    public String referenceFormat(AttributeDescriptorModel attributeDescriptor) {
        return "filePath:qualifier:" + super.referenceCatalogVersionFormat();
    }

    public Optional<Object> exportData(MediaContainerModel mediaContainer) {
        Optional<String> exportedMediaContainer = this.exportMedia(mediaContainer);
        return exportedMediaContainer.map(Object.class::cast);
    }

    protected Optional<String> exportMedia(MediaContainerModel mediaContainer) {
        //when false, we will just execute the default logic

        if (!CollectionUtils.isEmpty(mediaContainer.getMedias())) {
            MediaModel preferredMedia = mediaContainer.getMedias().stream()
                    .filter(mediaModel -> PREVIEW.equals(mediaModel.getMediaFormat().getQualifier()))
                    .findFirst()
                    .orElse(mediaContainer.getMedias().stream()
                            .findFirst().get());

            return elisionExcelMediaImportTranslator.exportMedia(preferredMedia);
        } else {
            return Optional.of(String.format(":%s:%s", mediaContainer.getQualifier(), exportCatalogVersionData(mediaContainer.getCatalogVersion())));
        }
    }

    @Override
    public ImpexValue importValue(AttributeDescriptorModel attributeDescriptorModel, ImportParameters importParameters) {
        throw new NotImplementedException();
    }
}
