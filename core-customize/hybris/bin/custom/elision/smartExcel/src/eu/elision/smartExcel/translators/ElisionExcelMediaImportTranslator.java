package eu.elision.smartExcel.translators;

import com.hybris.backoffice.excel.data.ImportParameters;
import com.hybris.backoffice.excel.translators.ExcelMediaImportTranslator;
import com.hybris.backoffice.excel.validators.data.ExcelValidationResult;
import com.hybris.backoffice.excel.validators.data.ValidationMessage;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.type.AttributeDescriptorModel;
import de.hybris.platform.util.Config;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Extended the default hybris implementation to be able to export media objects as downloadable URL's
 *
 * @author Elision
 */
public class ElisionExcelMediaImportTranslator extends ExcelMediaImportTranslator {

    /**
     * {@inheritDoc}
     * <p>
     * Custom version of the valide method to be able to check for the downloadable URL's. if these are enabled, we will not validate...
     */
    public ExcelValidationResult validate(ImportParameters importParameters, AttributeDescriptorModel attributeDescriptor, Map<String, Object> context) {
        if (!Config.getBoolean("excel.export.media.prettyURLs", false)) {
            List<ExcelValidationResult> validationResults =
                    this.getValidators().stream().filter((validator) -> validator.canHandle(importParameters, attributeDescriptor)).map((validator) -> validator.validate(importParameters, attributeDescriptor, context)).collect(Collectors.toList());
            Optional<ValidationMessage> validationMessageHeader = validationResults.stream().filter((e) -> e.getHeader() != null).map(ExcelValidationResult::getHeader).findFirst();
            List<ValidationMessage> validationMessages =
                    validationResults.stream().flatMap((validationResult) -> validationResult.getValidationErrors().stream()).collect(Collectors.toList());
            ExcelValidationResult excelValidationResult = new ExcelValidationResult(new ArrayList<>());
            validationMessageHeader.ifPresent(excelValidationResult::setHeader);
            excelValidationResult.setValidationErrors(validationMessages);
            return excelValidationResult;
        } else {
            return ExcelValidationResult.SUCCESS;
        }
    }

    /**
     * By doing this, import will probably be broken, so catch this and make sure images are ignored during import if the excel contains "pretty URL's"
     * {@inheritDoc}
     */
    public Optional<String> exportMedia(MediaModel media) {
        //when false, we will just execute the default logic
        if (Config.getBoolean("excel.export.media.prettyURLs", false)) {
            StringBuilder sb = new StringBuilder();
            if (media != null) {
                if (media.getDownloadURL().startsWith("http")) {
                    sb.append(media.getDownloadURL());
                } else if (media.getDownloadURL().startsWith("/medias/?context")) {
                    sb.append(Config.getString("env.hybris.url", "")).append(media.getURL());
                } else {
                    sb.append(Config.getString("environment.mediaurl", "")).append(media.getURL());
                }
            }
            return Optional.of(sb.toString());
        } else {
            return media != null ? Optional.of(String.format(":%s:%s:%s", media.getCode(), super.exportCatalogVersionData(media.getCatalogVersion()),
                    media.getFolder().getQualifier())) : Optional.empty();
        }
    }

}
