package eu.elision.smartExcel.validators.classification;

import com.hybris.backoffice.excel.data.ExcelClassificationAttribute;
import com.hybris.backoffice.excel.data.ImportParameters;
import com.hybris.backoffice.excel.validators.classification.ExcelEnumClassificationValidator;
import com.hybris.backoffice.excel.validators.data.ExcelValidationResult;
import com.hybris.backoffice.excel.validators.data.ValidationMessage;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeValueModel;
import org.apache.commons.collections4.CollectionUtils;

import javax.annotation.Nonnull;
import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Custom validator for importing enums
 * Method overridden to validate against name instead of code
 *
 * @author Elision
 */

public class ElisionExcelEnumClassificationValidator extends ExcelEnumClassificationValidator {

    /**
     * Checks if the value of a cell is a valid element for the field.
     *
     * @param excelAttribute   contains the default attribute values
     * @param importParameters contains the cell value
     * @param context          not used
     * @return ExcelValidationResult containing error messages or an empty list if there are none
     */
    public ExcelValidationResult validate(@Nonnull ExcelClassificationAttribute excelAttribute, @Nonnull ImportParameters importParameters, @Nonnull Map<String, Object> context) {
        String cellValue = String.valueOf(importParameters.getCellValue()).trim();
        Collection<ClassificationAttributeValueModel> attributeValues = excelAttribute.getAttributeAssignment().getAttributeValues();
        if (CollectionUtils.isEmpty(attributeValues)) {
            attributeValues = excelAttribute.getAttributeAssignment().getClassificationAttribute().getDefaultAttributeValues();
        } else {
            // merge attribute assignment attribute values with product feature's default attribute values
            Collection<ClassificationAttributeValueModel> defaultAttributeValues = excelAttribute.getAttributeAssignment().getClassificationAttribute().getDefaultAttributeValues();
            attributeValues = Stream.of(attributeValues, defaultAttributeValues)
                    .flatMap(Collection::stream)
                    .collect(Collectors.toMap(ClassificationAttributeValueModel::getCode, Function.identity(), (existing, replacement) -> existing))
                    .values();
        }

        return this.isImportedEnumOnValueList(cellValue, attributeValues) ? ExcelValidationResult.SUCCESS : new ExcelValidationResult(new ValidationMessage("excel.import" +
                ".validation.incorrecttype.enum", new Serializable[]{cellValue, excelAttribute.getAttributeAssignment().getClassificationAttribute().getCode()}));
    }

    protected boolean isImportedEnumOnValueList(String attributeValueCode, Collection<ClassificationAttributeValueModel> attributeValues) {
        Iterator iterator = attributeValues.iterator();

        while (iterator.hasNext()) {
            ClassificationAttributeValueModel attributeValue = (ClassificationAttributeValueModel) iterator.next();
            if (Objects.equals(attributeValueCode, attributeValue.getName())) {
                return true;
            }
        }

        return false;
    }
}