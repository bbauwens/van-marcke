package eu.elision.smartExcel.translators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaFormatModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.type.AttributeDescriptorModel;
import de.hybris.platform.core.model.type.TypeModel;
import de.hybris.platform.mediaweb.assertions.assertj.Assertions;
import de.hybris.platform.servicelayer.type.TypeService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ElisionExcelMediaContainerImportTranslatorTest {

    @Mock
    ElisionExcelMediaImportTranslator elisionExcelMediaImportTranslator;

    @Mock
    TypeService typeService;

    @Spy
    @InjectMocks
    ElisionExcelMediaContainerImportTranslator elisionExcelMediaContainerImportTranslator;

    @Test
    public void testCanHandle() {
        // given
        AttributeDescriptorModel attributeDescriptorModel = mock(AttributeDescriptorModel.class);
        TypeModel typeModel = mock(TypeModel.class);
        when(attributeDescriptorModel.getAttributeType()).thenReturn(typeModel);
        when(typeModel.getCode()).thenReturn("typeCode");
        doReturn(typeService).when(elisionExcelMediaContainerImportTranslator).getTypeService();

        //when
        elisionExcelMediaContainerImportTranslator.canHandle(attributeDescriptorModel);

        // then
        verify(typeService).isAssignableFrom("MediaContainer", "typeCode" );
    }

    @Test
    public void testReferenceFormat() {
        String result = elisionExcelMediaContainerImportTranslator.referenceFormat(mock(AttributeDescriptorModel.class));
        Assertions.assertThat(result).isEqualTo("filePath:qualifier:catalog:version");
    }

    @Test
    public void testExportMediaContainer() {
        // given
        MediaContainerModel mediaContainerModel = mock(MediaContainerModel.class);
        MediaModel mediaModel = mock(MediaModel.class);
        MediaFormatModel mediaFormatModel = mock(MediaFormatModel.class);
        when(mediaFormatModel.getQualifier()).thenReturn("PREVIEW");
        when(mediaModel.getMediaFormat()).thenReturn(mediaFormatModel);
        when(mediaContainerModel.getMedias()).thenReturn(List.of(mediaModel));

        // when
        elisionExcelMediaContainerImportTranslator.exportMedia(mediaContainerModel);

        // then
        verify(elisionExcelMediaImportTranslator).exportMedia(mediaModel);
    }

    @Test
    public void testExportMediaContainer_noMedias() {
        // given
        CatalogVersionModel catalogVersionModel = mock(CatalogVersionModel.class);
        MediaContainerModel mediaContainerModel = mock(MediaContainerModel.class);
        when(mediaContainerModel.getCatalogVersion()).thenReturn(catalogVersionModel);
        when(mediaContainerModel.getMedias()).thenReturn(Collections.emptyList());
        when(mediaContainerModel.getQualifier()).thenReturn("mediaContainer");
        doReturn("Staged").when(elisionExcelMediaContainerImportTranslator).exportCatalogVersionData(catalogVersionModel);

        // when
        Optional<String> result = elisionExcelMediaContainerImportTranslator.exportMedia(mediaContainerModel);

        // then
        Assertions.assertThat(result.get()).isEqualTo(":mediaContainer:Staged");
    }

    @Test
    public void testExportData() {
        MediaContainerModel mediaContainerModel = mock(MediaContainerModel.class);
        elisionExcelMediaContainerImportTranslator.exportData(mediaContainerModel);
        verify(elisionExcelMediaContainerImportTranslator).exportData(mediaContainerModel);
    }
}