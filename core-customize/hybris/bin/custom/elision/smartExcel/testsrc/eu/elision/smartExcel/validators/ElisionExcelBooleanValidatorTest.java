package eu.elision.smartExcel.validators;

import com.hybris.backoffice.excel.data.ImportParameters;
import com.hybris.backoffice.excel.validators.data.ExcelValidationResult;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.type.AttributeDescriptorModel;
import de.hybris.platform.core.model.type.TypeModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.HashMap;

import static com.hybris.backoffice.excel.validators.ExcelBooleanValidator.VALIDATION_INCORRECTTYPE_BOOLEAN_MESSAGE_KEY;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Elision
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ElisionExcelBooleanValidatorTest {
    private final ElisionExcelBooleanValidator excelBooleanFieldValidator = new ElisionExcelBooleanValidator();

    @Test
    public void shouldNotReturnValidationErrorWhenCellHasBooleanValue() {
        // given
        ImportParameters importParameters = new ImportParameters(ProductModel._TYPECODE, null, "true", null, new ArrayList<>());
        AttributeDescriptorModel attributeDescriptor = Mockito.mock(AttributeDescriptorModel.class);
        TypeModel typeModel = Mockito.mock(TypeModel.class);
        Mockito.when(attributeDescriptor.getAttributeType()).thenReturn(typeModel);
        Mockito.when(typeModel.getCode()).thenReturn(Boolean.class.getCanonicalName());

        // when
        ExcelValidationResult validationResult = excelBooleanFieldValidator.validate(importParameters, attributeDescriptor, new HashMap<>());

        // then
        assertThat(validationResult.hasErrors()).isFalse();
        assertThat(validationResult.getValidationErrors()).isEmpty();
    }

    @Test
    public void shouldNotReturnValidationErrorWhenCellHasNAValue() {
        // given
        final ImportParameters importParameters = new ImportParameters(ProductModel._TYPECODE, null, "N/A", null, new ArrayList<>());
        AttributeDescriptorModel attributeDescriptor = Mockito.mock(AttributeDescriptorModel.class);
        TypeModel typeModel = Mockito.mock(TypeModel.class);
        Mockito.when(attributeDescriptor.getAttributeType()).thenReturn(typeModel);
        Mockito.when(typeModel.getCode()).thenReturn(Boolean.class.getCanonicalName());

        // when
        ExcelValidationResult validationResult = excelBooleanFieldValidator.validate(importParameters, attributeDescriptor, new HashMap<>());

        // then
        assertThat(validationResult.hasErrors()).isFalse();
        assertThat(validationResult.getValidationErrors()).isEmpty();
    }

    @Test
    public void shouldReturnValidationErrorWhenCellDoesntHaveBooleanValue() {
        // given
        ImportParameters importParameters = new ImportParameters(ProductModel._TYPECODE, null, "abc", null, new ArrayList<>());
        AttributeDescriptorModel attributeDescriptor = Mockito.mock(AttributeDescriptorModel.class);
        TypeModel typeModel = Mockito.mock(TypeModel.class);
        Mockito.when(attributeDescriptor.getAttributeType()).thenReturn(typeModel);
        Mockito.when(typeModel.getCode()).thenReturn(Boolean.class.getCanonicalName());

        // when
        ExcelValidationResult validationResult = excelBooleanFieldValidator.validate(importParameters, attributeDescriptor, new HashMap<>());

        // then
        assertThat(validationResult.hasErrors()).isTrue();
        assertThat(validationResult.getValidationErrors()).hasSize(1);
        assertThat(validationResult.getValidationErrors().get(0).getMessageKey()).isEqualTo(
                VALIDATION_INCORRECTTYPE_BOOLEAN_MESSAGE_KEY);
        assertThat(validationResult.getValidationErrors().get(0).getParams()).containsExactly(importParameters.getCellValue());
    }


}