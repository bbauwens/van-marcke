package eu.elision.smartExcel.extractor;

import com.hybris.backoffice.excel.data.ExcelClassificationAttribute;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ElisionClassificationFullNameExtractorTest {

    @InjectMocks
    private ElisionClassificationFullNameExtractor extractor;

    @Test
    public void testExtractFullName() {
        ExcelClassificationAttribute attribute = mock(ExcelClassificationAttribute.class);
        ClassAttributeAssignmentModel attributeAssignment = mock(ClassAttributeAssignmentModel.class);
        ClassificationAttributeModel attributeModel = mock(ClassificationAttributeModel.class);

        when(attribute.getAttributeAssignment()).thenReturn(attributeAssignment);
        when(attributeAssignment.getClassificationAttribute()).thenReturn(attributeModel);
        when(attributeModel.getName()).thenReturn("name");
        when(attributeModel.getCode()).thenReturn("code");

        String result = extractor.extract(attribute);

        assertThat(result).isEqualTo("code.name");
    }

    @Test
    public void testExtractFullNameWhenEmpty() {
        ExcelClassificationAttribute attribute = mock(ExcelClassificationAttribute.class);
        ClassAttributeAssignmentModel attributeAssignment = mock(ClassAttributeAssignmentModel.class);
        ClassificationAttributeModel attributeModel = mock(ClassificationAttributeModel.class);

        when(attribute.getAttributeAssignment()).thenReturn(attributeAssignment);
        when(attributeAssignment.getClassificationAttribute()).thenReturn(attributeModel);

        String result = extractor.extract(attribute);

        assertThat(result).isEqualTo(".");
    }

    @Test
    public void testExtractFullNameWhenNotExist() {
        ExcelClassificationAttribute attribute = mock(ExcelClassificationAttribute.class);
        ClassAttributeAssignmentModel attributeAssignment = mock(ClassAttributeAssignmentModel.class);

        when(attribute.getAttributeAssignment()).thenReturn(attributeAssignment);

        String result = extractor.extract(attribute);

        assertThat(result).isEqualTo(".");
    }
}
