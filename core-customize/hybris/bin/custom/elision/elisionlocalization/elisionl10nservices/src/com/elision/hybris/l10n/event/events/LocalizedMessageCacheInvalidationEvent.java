package com.elision.hybris.l10n.event.events;

import de.hybris.platform.servicelayer.event.ClusterAwareEvent;
import de.hybris.platform.servicelayer.event.events.AbstractEvent;

public class LocalizedMessageCacheInvalidationEvent extends AbstractEvent implements ClusterAwareEvent {

    private final String localizedMessageKey;

    public LocalizedMessageCacheInvalidationEvent(final String localizedMessageKey) {
        this.localizedMessageKey = localizedMessageKey;
    }

    @Override
    public boolean publish(final int sourceNodeId, final int targetNodeId) {
        return true; // broadcast from all to all cluster nodes
    }

    public String getLocalizedMessageKey() {
        return this.localizedMessageKey;
    }
}