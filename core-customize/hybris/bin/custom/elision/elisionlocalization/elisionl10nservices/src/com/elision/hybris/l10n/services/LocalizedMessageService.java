package com.elision.hybris.l10n.services;

import com.elision.hybris.l10n.model.LocalizedMessageModel;

public interface LocalizedMessageService {

    LocalizedMessageModel getCachedLocalizedMessageForKey(String key);

    LocalizedMessageModel getLocalizedMessageForKey(String key);

    String formatLocalizedMessage(String message, Object[] arguments);

    void evictLocalizedMessageFromCache(String key);
}