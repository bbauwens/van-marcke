package com.elision.hybris.l10n.listeners;

import com.elision.hybris.l10n.event.events.LocalizedMessageCacheInvalidationEvent;
import com.elision.hybris.l10n.model.LocalizedMessageModel;
import de.hybris.platform.core.PK;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.tx.AfterSaveEvent;
import de.hybris.platform.tx.AfterSaveListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;

public class LocalizedMessageAfterSaveListener implements AfterSaveListener {

    public static final int LOCALIZED_MESSAGE_TYPE_CODE = 11999;

    private static final Logger LOGGER = LoggerFactory.getLogger(LocalizedMessageAfterSaveListener.class);

    private final ModelService modelService;
    private final EventService eventService;

    @Autowired
    public LocalizedMessageAfterSaveListener(final ModelService modelService, final EventService eventService) {
        this.modelService = modelService;
        this.eventService = eventService;
    }

    @Override
    public void afterSave(final Collection<AfterSaveEvent> events) {
        try {
            for (final AfterSaveEvent event : events) {
                if (shouldProcessEvent(event)) {
                    processEvent(event);
                }
            }
        } catch (final RuntimeException ex) {
            LOGGER.error("Error caught in LocalizedMessageAfterSaveListener", ex);
            throw ex;
        }
    }

    private void processEvent(final AfterSaveEvent event) {
        final PK pk = event.getPk();
        final LocalizedMessageModel localizedMessage = this.modelService.get(pk);
        this.eventService.publishEvent(new LocalizedMessageCacheInvalidationEvent(localizedMessage.getKey()));
    }

    private static boolean shouldProcessEvent(final AfterSaveEvent event) {
        return isLocalizedMessageEvent(event) && isUpdateEvent(event);
    }

    private static boolean isUpdateEvent(final AfterSaveEvent event) {
        return AfterSaveEvent.UPDATE == event.getType();
    }

    private static boolean isLocalizedMessageEvent(final AfterSaveEvent event) {
        return LOCALIZED_MESSAGE_TYPE_CODE == event.getPk().getTypeCode();
    }
}