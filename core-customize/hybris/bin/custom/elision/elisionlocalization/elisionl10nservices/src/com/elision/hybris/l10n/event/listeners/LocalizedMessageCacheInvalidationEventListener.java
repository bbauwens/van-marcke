package com.elision.hybris.l10n.event.listeners;

import com.elision.hybris.l10n.event.events.LocalizedMessageCacheInvalidationEvent;
import com.elision.hybris.l10n.services.LocalizedMessageService;
import de.hybris.platform.servicelayer.event.impl.AbstractEventListener;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

public class LocalizedMessageCacheInvalidationEventListener extends AbstractEventListener<LocalizedMessageCacheInvalidationEvent> {

    private final LocalizedMessageService localizedMessageService;

    @Autowired
    public LocalizedMessageCacheInvalidationEventListener(final LocalizedMessageService localizedMessageService) {
        this.localizedMessageService = localizedMessageService;
    }

    @Override
    protected void onEvent(final LocalizedMessageCacheInvalidationEvent event) {
        final String localizedMessageKey = event.getLocalizedMessageKey();
        if (StringUtils.isNotEmpty(localizedMessageKey)) {
            this.localizedMessageService.evictLocalizedMessageFromCache(localizedMessageKey);
        }
    }
}