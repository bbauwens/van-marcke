package com.elision.hybris.l10n.dao;

import com.elision.hybris.l10n.model.LocalizedMessageModel;

public interface LocalizedMessageDao {

    LocalizedMessageModel getLocalizedMessageForKey(String key);
}
