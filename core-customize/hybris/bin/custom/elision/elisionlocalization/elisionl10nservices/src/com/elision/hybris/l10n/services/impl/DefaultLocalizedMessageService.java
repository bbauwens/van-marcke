package com.elision.hybris.l10n.services.impl;

import com.elision.hybris.l10n.dao.LocalizedMessageDao;
import com.elision.hybris.l10n.model.LocalizedMessageModel;
import com.elision.hybris.l10n.services.LocalizedMessageService;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.i18n.I18NService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;

import java.text.MessageFormat;
import java.util.Locale;

public class DefaultLocalizedMessageService implements LocalizedMessageService {

    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultLocalizedMessageService.class);

    private final LocalizedMessageDao localizedMessageDao;
    private final I18NService i18NService;

    @Autowired
    public DefaultLocalizedMessageService(final LocalizedMessageDao localizedMessageDao, I18NService i18NService) {
        this.localizedMessageDao = localizedMessageDao;
        this.i18NService = i18NService;
    }

    @Cacheable(value = "localizedMessages", key = "#key", cacheManager = "localizedMessageCacheManager")
    @Override
    public LocalizedMessageModel getCachedLocalizedMessageForKey(final String key) {
        return this.getLocalizedMessageForKey(key);
    }

    @Override
    public LocalizedMessageModel getLocalizedMessageForKey(final String key) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("LOCALIZED MESSAGE CACHE MIS FOR  " + key);
        }

        if (StringUtils.isEmpty(key)) {
            return null;
        }

        try {
            return this.localizedMessageDao.getLocalizedMessageForKey(key);
        } catch (final ModelNotFoundException notFoundEx) {
            // Ignore
        } catch (final Exception ex) {
            LOGGER.error("An exception occured while trying to get localized message from DB (key: {}): {}", key, ex.getMessage());
        }

        return null;
    }

    @Override
    public String formatLocalizedMessage(final String message, final Object[] arguments) {
        final Locale currentLocale = i18NService.getCurrentLocale();

        if (message != null && arguments != null && arguments.length != 0) {
            final MessageFormat messageFormat = new MessageFormat(message, currentLocale);

            return messageFormat.format(arguments, new StringBuffer(), null).toString();
        } else {
            return message;
        }
    }

    @CacheEvict(value = "localizedMessages", key = "#key", cacheManager = "localizedMessageCacheManager")
    @Override
    public void evictLocalizedMessageFromCache(final String key) {
        // Spring handles the eviction based on the method annotation.
    }
}