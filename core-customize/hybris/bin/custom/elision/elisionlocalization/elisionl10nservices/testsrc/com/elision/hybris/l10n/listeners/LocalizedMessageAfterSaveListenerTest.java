package com.elision.hybris.l10n.listeners;

import com.elision.hybris.l10n.model.LocalizedMessageModel;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.PK;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.tx.AfterSaveEvent;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;

import static com.elision.hybris.l10n.listeners.LocalizedMessageAfterSaveListener.LOCALIZED_MESSAGE_TYPE_CODE;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class LocalizedMessageAfterSaveListenerTest {

    private LocalizedMessageAfterSaveListener listener;

    @Mock
    private ModelService modelService;

    @Mock
    private EventService eventService;

    @Before
    public void setUp() {
        this.listener = new LocalizedMessageAfterSaveListener(this.modelService, this.eventService);
    }

    @Test
    public void afterSave_should_not_process_event_when_not_localized_message_event() {
        final AfterSaveEvent afterSaveEventMock = mock(AfterSaveEvent.class);
        when(afterSaveEventMock.getPk()).thenReturn(PK.createUUIDPK(999));
        when(afterSaveEventMock.getType()).thenReturn(AfterSaveEvent.UPDATE);

        this.listener.afterSave(Collections.singletonList(afterSaveEventMock));

        verify(this.modelService, never()).get(any(PK.class));
        verify(this.eventService, never()).publishEvent(any());
    }

    @Test
    public void afterSave_should_not_process_event_when_not_update_event() {
        final AfterSaveEvent afterSaveEventMock = mock(AfterSaveEvent.class);
        when(afterSaveEventMock.getPk()).thenReturn(PK.createUUIDPK(LOCALIZED_MESSAGE_TYPE_CODE));
        when(afterSaveEventMock.getType()).thenReturn(AfterSaveEvent.CREATE);

        this.listener.afterSave(Collections.singletonList(afterSaveEventMock));

        verify(this.modelService, never()).get(any(PK.class));
        verify(this.eventService, never()).publishEvent(any());
    }

    @Test
    public void afterSave_should_process_event_and_evict_localized_message_entry_from_cache() {
        final AfterSaveEvent afterSaveEventMock = mock(AfterSaveEvent.class);
        when(afterSaveEventMock.getPk()).thenReturn(PK.createUUIDPK(LOCALIZED_MESSAGE_TYPE_CODE));
        when(afterSaveEventMock.getType()).thenReturn(AfterSaveEvent.UPDATE);

        final LocalizedMessageModel localizedMessage = mock(LocalizedMessageModel.class);
        when(localizedMessage.getKey()).thenReturn(RandomStringUtils.randomAlphanumeric(4));

        when(this.modelService.get(any(PK.class))).thenReturn(localizedMessage);

        this.listener.afterSave(Collections.singletonList(afterSaveEventMock));

        verify(this.modelService, times(1)).get(any(PK.class));
        verify(this.eventService, times(1)).publishEvent(any());
    }
}