package com.elision.hybris.l10n.web.context;


import com.elision.hybris.l10n.facades.LocalizedMessageFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.context.MessageSource;
import org.springframework.context.support.AbstractMessageSource;
import org.springframework.util.ObjectUtils;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.Optional;

public class ElisionDatabaseDrivenMessageSource extends AbstractMessageSource {

    private final LocalizedMessageFacade localizedMessageFacade;

    private boolean fallbackToParentProperties;
    private boolean saveFallbackMessagesToDatabaseIfMissing;

    @Autowired
    public ElisionDatabaseDrivenMessageSource(final LocalizedMessageFacade localizedMessageFacade) {
        this.localizedMessageFacade = localizedMessageFacade;
    }

    @Override
    protected MessageFormat resolveCode(final String code, final Locale locale) {
        return Optional.ofNullable(findMessageForCodeAndLocale(code, locale))
                .map(message -> super.createMessageFormat(message, locale))
                .orElse(null);
    }

    @Override
    protected String resolveCodeWithoutArguments(final String code, final Locale locale) {
        return findMessageForCodeAndLocale(code, locale);
    }

    private String findMessageForCodeAndLocale(final String code, final Locale locale) {
        return this.localizedMessageFacade.getMessageForCodeAndLocale(code, locale);
    }

    @Override
    protected String getMessageFromParent(final String code, final Object[] args, final Locale locale) {
        if (!this.fallbackToParentProperties) {
            return null;
        }

        final MessageSource fallbackMessageSource = getParentMessageSource();
        if (fallbackMessageSource != null) {
            final String messageFromParent = fallbackMessageSource.getMessage(code, null, null, locale);

            if (messageFromParent != null && this.saveFallbackMessagesToDatabaseIfMissing) {
                this.localizedMessageFacade.createOrUpdateLocalizedMessage(code, locale, messageFromParent);
            }

            if (ObjectUtils.isEmpty(args)) {
                return messageFromParent;
            }

            final MessageFormat messageFormat = super.createMessageFormat(messageFromParent, locale);
            return messageFormat.format(args);
        }

        return null;
    }

    @Required
    public void setFallbackToParentProperties(final boolean fallbackToParentProperties) {
        this.fallbackToParentProperties = fallbackToParentProperties;
    }

    @Required
    public void setSaveFallbackMessagesToDatabaseIfMissing(final boolean saveFallbackMessagesToDatabaseIfMissing) {
        this.saveFallbackMessagesToDatabaseIfMissing = saveFallbackMessagesToDatabaseIfMissing;
    }
}
