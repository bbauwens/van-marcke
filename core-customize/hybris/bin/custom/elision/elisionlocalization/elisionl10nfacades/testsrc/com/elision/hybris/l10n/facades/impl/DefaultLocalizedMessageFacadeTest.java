package com.elision.hybris.l10n.facades.impl;

import com.elision.hybris.l10n.facades.LocalizedMessageFacade;
import com.elision.hybris.l10n.model.LocalizedMessageModel;
import com.elision.hybris.l10n.services.LocalizedMessageService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Locale;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultLocalizedMessageFacadeTest {

    private LocalizedMessageFacade localizedMessageFacade;

    @Mock
    private LocalizedMessageService localizedMessageService;

    @Mock
    private ModelService modelService;

    @Mock
    private I18NService i18NService;

    @Before
    public void setUp() {
        localizedMessageFacade = new DefaultLocalizedMessageFacade(i18NService,
                localizedMessageService,
                modelService);
    }

    @Test
    public void getMessageForCodeAndLocale_should_return_empty_string_when_code_is_empty() {
        assertThat(localizedMessageFacade.getMessageForCodeAndLocale(StringUtils.EMPTY, Locale.ENGLISH)).isEqualTo(StringUtils.EMPTY);
    }

    @Test
    public void getMessageForCodeAndLocale_should_return_empty_string_when_code_is_null() {
        assertThat(localizedMessageFacade.getMessageForCodeAndLocale(null, Locale.ENGLISH)).isEqualTo(StringUtils.EMPTY);
    }

    @Test
    public void getMessageForCodeAndLocale_should_return_empty_string_when_locale_is_null() {
        assertThat(localizedMessageFacade.getMessageForCodeAndLocale(RandomStringUtils.randomAlphanumeric(4), null)).isEqualTo(StringUtils.EMPTY);
    }

    @Test
    public void createOrUpdateLocalizedMessage_should_return_null_for_non_existing_localized_message() {
        when(localizedMessageService.getCachedLocalizedMessageForKey(anyString())).thenReturn(null);

        final String message = localizedMessageFacade.getMessageForCodeAndLocale(RandomStringUtils.random(4), Locale.FRANCE);

        verify(localizedMessageService, times(1)).getCachedLocalizedMessageForKey(anyString());

        assertThat(message).isEqualTo(null);
    }

    @Test
    public void createOrUpdateLocalizedMessage_should_return_correct_message_for_locale() {
        final String expectedMessageString = "FRENCH MESSAGE";

        final LocalizedMessageModel localizedMessageModel = mock(LocalizedMessageModel.class);
        when(localizedMessageModel.getMessage(Locale.ENGLISH)).thenReturn("ENGLISH MESSAGE");
        when(localizedMessageModel.getMessage(Locale.FRANCE)).thenReturn(expectedMessageString);

        when(localizedMessageService.getCachedLocalizedMessageForKey(anyString())).thenReturn(localizedMessageModel);

        final String message = localizedMessageFacade.getMessageForCodeAndLocale(RandomStringUtils.random(4), Locale.FRANCE);

        verify(localizedMessageService, times(1)).getCachedLocalizedMessageForKey(anyString());

        assertThat(message).isEqualTo(expectedMessageString);
    }

    @Test
    public void createOrUpdateLocalizedMessage_should_not_create_new_message_when_code_is_empty() {
        final String code = StringUtils.EMPTY;
        final Locale locale = Locale.FRANCE;
        final String message = RandomStringUtils.randomAlphanumeric(20);

        localizedMessageFacade.createOrUpdateLocalizedMessage(code, locale, message);

        verify(localizedMessageService, never()).getLocalizedMessageForKey(anyString());
        verify(modelService, never()).create(LocalizedMessageModel.class);
        verify(modelService, never()).save(any(LocalizedMessageModel.class));
        verify(localizedMessageService, never()).evictLocalizedMessageFromCache(anyString());
    }

    @Test
    public void createOrUpdateLocalizedMessage_should_not_create_new_message_when_code_is_null() {
        final String code = null;
        final Locale locale = Locale.FRANCE;
        final String message = RandomStringUtils.randomAlphanumeric(20);

        localizedMessageFacade.createOrUpdateLocalizedMessage(code, locale, message);

        verify(localizedMessageService, never()).getLocalizedMessageForKey(anyString());
        verify(modelService, never()).create(LocalizedMessageModel.class);
        verify(modelService, never()).save(any(LocalizedMessageModel.class));
        verify(localizedMessageService, never()).evictLocalizedMessageFromCache(anyString());
    }

    @Test
    public void createOrUpdateLocalizedMessage_should_not_create_new_message_when_locale_is_null() {
        final String code = RandomStringUtils.randomAlphanumeric(4);
        final Locale locale = null;
        final String message = RandomStringUtils.randomAlphanumeric(20);

        localizedMessageFacade.createOrUpdateLocalizedMessage(code, locale, message);

        verify(localizedMessageService, never()).getLocalizedMessageForKey(anyString());
        verify(modelService, never()).create(LocalizedMessageModel.class);
        verify(modelService, never()).save(any(LocalizedMessageModel.class));
        verify(localizedMessageService, never()).evictLocalizedMessageFromCache(anyString());
    }

    @Test
    public void createOrUpdateLocalizedMessage_should_create_new_message() {
        final String code = RandomStringUtils.randomAlphanumeric(4);
        final Locale locale = Locale.FRANCE;
        final String message = RandomStringUtils.randomAlphanumeric(20);

        final LocalizedMessageModel localizedMessage = mock(LocalizedMessageModel.class);

        when(localizedMessageService.getLocalizedMessageForKey(eq(code))).thenReturn(null);
        when(modelService.create(LocalizedMessageModel.class)).thenReturn(localizedMessage);

        localizedMessageFacade.createOrUpdateLocalizedMessage(code, locale, message);

        verify(localizedMessageService, times(1)).getLocalizedMessageForKey(anyString());
        verify(modelService, times(1)).create(LocalizedMessageModel.class);
        verify(modelService, times(1)).save(any(LocalizedMessageModel.class));
        verify(localizedMessageService, times(1)).evictLocalizedMessageFromCache(eq(code));

        verify(localizedMessage, times(1)).setKey(eq(code));
        verify(localizedMessage, times(1)).setMessage(eq(message), eq(locale));
    }

    @Test
    public void createOrUpdateLocalizedMessage_should_update_existing_message() {
        final String code = RandomStringUtils.randomAlphanumeric(4);
        final Locale locale = Locale.FRANCE;
        final String message = RandomStringUtils.randomAlphanumeric(20);

        final LocalizedMessageModel localizedMessage = mock(LocalizedMessageModel.class);

        when(localizedMessageService.getLocalizedMessageForKey(eq(code))).thenReturn(localizedMessage);

        localizedMessageFacade.createOrUpdateLocalizedMessage(code, locale, message);

        verify(localizedMessageService, times(1)).getLocalizedMessageForKey(anyString());
        verify(modelService, never()).create(LocalizedMessageModel.class);
        verify(modelService, times(1)).save(any(LocalizedMessageModel.class));
        verify(localizedMessageService, times(1)).evictLocalizedMessageFromCache(eq(code));

        verify(localizedMessage, times(1)).setKey(eq(code));
        verify(localizedMessage, times(1)).setMessage(eq(message), eq(locale));
    }


}