package eu.elision.samlsinglesignon.service;

import de.hybris.platform.b2b.model.B2BUnitModel;

/**
 * SSO service interface for getting/creating b2b units
 */
public interface SSOB2BUnitService {

    /**
     * Return {@link B2BUnitModel} for existing b2b unit or for the newly created.
     *
     * @param uid the b2b unit ud
     * @return existing or newly created b2b unit model
     */
    B2BUnitModel getOrCreateSSOB2BUnit(String uid);
}