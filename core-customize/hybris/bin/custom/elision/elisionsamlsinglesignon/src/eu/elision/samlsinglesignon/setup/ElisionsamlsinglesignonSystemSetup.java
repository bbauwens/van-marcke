package eu.elision.samlsinglesignon.setup;

import de.hybris.platform.core.initialization.SystemSetup;
import eu.elision.samlsinglesignon.constants.ElisionsamlsinglesignonConstants;

@SystemSetup(extension = ElisionsamlsinglesignonConstants.EXTENSIONNAME)
public class ElisionsamlsinglesignonSystemSetup {

}