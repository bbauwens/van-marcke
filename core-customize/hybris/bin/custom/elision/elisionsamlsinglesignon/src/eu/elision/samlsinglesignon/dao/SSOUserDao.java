package eu.elision.samlsinglesignon.dao;

import de.hybris.platform.core.model.user.UserModel;

/**
 * Dao for SSO User
 */
public interface SSOUserDao {

    /**
     * Find user by given customer ID
     *
     * @param customerID the given customer ID
     * @return the user if found, else null
     */
    UserModel findUserByCustomerID(String customerID);
}
