package eu.elision.samlsinglesignon.factory.impl;

import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.samlsinglesignon.SAMLService;
import eu.elision.samlsinglesignon.enums.SSOContext;
import eu.elision.samlsinglesignon.factory.ELISSOUserServiceFactory;
import eu.elision.samlsinglesignon.service.ELISSOUserService;
import org.springframework.security.saml.SAMLCredential;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * Default Implementation for {@link ELISSOUserServiceFactory}
 */
public class DefaultELISSOUserServiceFactory implements ELISSOUserServiceFactory {

    private final SAMLService samlService;
    private final Map<SSOContext, ELISSOUserService> factoryConfigurationMap;

    /**
     * Creates a new instance of {@link DefaultELISSOUserServiceFactory}
     *
     * @param samlService             the samlService
     * @param factoryConfigurationMap the factoryConfigurationMap
     */
    public DefaultELISSOUserServiceFactory(final SAMLService samlService,
                                           final Map<SSOContext, ELISSOUserService> factoryConfigurationMap) {
        this.samlService = samlService;
        this.factoryConfigurationMap = factoryConfigurationMap;
    }

    @Override
    public UserModel getOrCreateSSOUser(final SSOContext ssoContext, final SAMLCredential samlCredential, HttpServletRequest request) {
        final String userId = samlService.getUserId(samlCredential);
        final String userName = samlService.getUserName(samlCredential);
        return get(ssoContext).getOrCreateSSOUser(userId, userName, samlCredential, request);
    }


    /**
     * Gets the {@link ELISSOUserService} based on the {@link SSOContext} from the factoryConfigurationMap
     *
     * @param ssoContext the ssoContext
     * @return the {@link ELISSOUserService}
     */
    protected ELISSOUserService get(final SSOContext ssoContext) {
        return factoryConfigurationMap.get(ssoContext);
    }
}