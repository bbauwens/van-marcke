package eu.elision.samlsinglesignon.util;

import org.apache.commons.lang3.ArrayUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;

/**
 * This util class provides methods for the creation of cookies.
 *
 * @author Tom van den Berg
 * @since 18-06-2020
 */
public class VMKCookieUtil {

    /**
     * Returns the cookie for the given {@code name} from the {@code request}.
     * <p>
     * Returns {@code null} in case the cookie does not exist.
     *
     * @param request the HTTP request
     * @param name    the cookie name
     * @return the cookie
     */
    public static Cookie getCookie(HttpServletRequest request, String name) {
        Cookie cookie = null;
        if (request != null && ArrayUtils.isNotEmpty(request.getCookies())) {
            cookie = Arrays.stream(request.getCookies())
                    .filter(c -> name.equals(c.getName()))
                    .findFirst()
                    .orElse(null);
        }
        return cookie;
    }

    /**
     * Extracts the value from the given {@code cookie}.
     * <p>
     * Returns {@code null} in case the given {@code cookie} or its value is invalid.
     *
     * @param cookie the cookie to extract the value from
     * @return the value
     */
    public static String extractValue(Cookie cookie) {
        String value = null;
        if (cookie != null) {
            value = cookie.getValue();
        }
        return value;
    }
}
