/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package eu.elision.samlsinglesignon.constants;

/**
 * Global class for all Elisionsamlsinglesignon constants. You can add global constants for your extension into this class.
 */
public final class ElisionsamlsinglesignonConstants extends GeneratedElisionsamlsinglesignonConstants {
    public static final String EXTENSIONNAME = "elisionsamlsinglesignon";

    public static final String SSO_ATTRIBUTE_USERGROUP = "sso.usergroup.attribute.key";
    public static final String DEFAULT_SSO_ATTRIBUTE_USERGROUP = "usergroup";

    private ElisionsamlsinglesignonConstants() {
        //empty to avoid instantiating this constant class
    }

}