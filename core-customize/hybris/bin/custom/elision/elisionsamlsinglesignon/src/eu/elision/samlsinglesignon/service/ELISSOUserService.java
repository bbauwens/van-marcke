package eu.elision.samlsinglesignon.service;

import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.samlsinglesignon.SSOUserService;
import org.springframework.security.saml.SAMLCredential;

import javax.servlet.http.HttpServletRequest;

/**
 * Custom interface for {@link SSOUserService}
 */
public interface ELISSOUserService extends SSOUserService {

    /**
     * Gets existing {@link UserModel} for id; if it doesn't exists, one will be created
     *
     * @param id             Uid to get {@link UserModel} for
     * @param name           Name of the {@link UserModel}
     * @param samlCredential {@link SAMLCredential} contains all information to create new {@link UserModel}
     */
    UserModel getOrCreateSSOUser(String id, String name, SAMLCredential samlCredential);

    /**
     * Gets existing {@link UserModel} for id; if it doesn't exists, one will be created
     *
     * @param id             Uid to get {@link UserModel} for
     * @param name           Name of the {@link UserModel}
     * @param samlCredential {@link SAMLCredential} contains all information to create new {@link UserModel}
     * @param request        the http request
     */
    UserModel getOrCreateSSOUser(String id, String name, SAMLCredential samlCredential, HttpServletRequest request);
}
