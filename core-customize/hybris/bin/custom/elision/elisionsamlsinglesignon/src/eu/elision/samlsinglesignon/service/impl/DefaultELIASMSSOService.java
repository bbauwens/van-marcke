package eu.elision.samlsinglesignon.service.impl;

import de.hybris.platform.core.model.user.EmployeeModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.samlsinglesignon.DefaultSSOService;
import de.hybris.platform.samlsinglesignon.SAMLService;
import de.hybris.platform.samlsinglesignon.model.SamlUserGroupModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.util.Config;
import eu.elision.samlsinglesignon.service.ELISSOUserService;
import eu.elision.samlsinglesignon.strategy.ELISamlUserGroupLookupStrategy;
import org.springframework.security.saml.SAMLCredential;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;
import java.util.List;

import static com.google.common.base.Preconditions.checkState;
import static eu.elision.samlsinglesignon.constants.ElisionsamlsinglesignonConstants.DEFAULT_SSO_ATTRIBUTE_USERGROUP;
import static eu.elision.samlsinglesignon.constants.ElisionsamlsinglesignonConstants.SSO_ATTRIBUTE_USERGROUP;
import static java.lang.String.format;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;
import static org.apache.commons.lang3.StringUtils.lowerCase;

/**
 * Extends the default implementation {@link DefaultSSOService} to handle ASM Employees.
 */
public class DefaultELIASMSSOService extends DefaultSSOService implements ELISSOUserService {

    private final SAMLService samlService;
    private final ELISamlUserGroupLookupStrategy samlUserGroupLookupStrategy;
    private final ConfigurationService configurationService;

    /**
     * Creates a new instance of {@link DefaultELIASMSSOService}
     *
     * @param samlService                 the samlService
     * @param samlUserGroupLookupStrategy the samlUserGroupLookupStrategy
     */
    public DefaultELIASMSSOService(final SAMLService samlService, final ELISamlUserGroupLookupStrategy samlUserGroupLookupStrategy, final ConfigurationService configurationService) {
        this.samlService = samlService;
        this.samlUserGroupLookupStrategy = samlUserGroupLookupStrategy;
        this.configurationService = configurationService;
    }

    @Override
    public UserModel getOrCreateSSOUser(final String id, final String name, final SAMLCredential samlCredential) {

        final String userGroup = configurationService.getConfiguration().getString(SSO_ATTRIBUTE_USERGROUP, DEFAULT_SSO_ATTRIBUTE_USERGROUP);

        final Collection<String> roles = samlService.getCustomAttributes(samlCredential, userGroup);
        checkState(isNotEmpty(roles), format("Roles attribute is required - cannot accept user '%s'", id));

        return getOrCreateSSOUser(lowerCase("asm$" + id), name, roles);
    }

    @Override
    /**
     * Extracted so it can be used in a mockito test as it uses static Hybris implementations.
     */
    protected SSOUserMapping findMapping(Collection<String> roles) {
        return super.findMapping(roles);
    }

    /**
     * Extracted so it can be used in a mockito test as it uses static Hybris implementations.
     */
    @Override
    protected UserModel createNewUser(String id, String name, SSOUserMapping userMapping) {
        return super.createNewUser(id, name, userMapping);
    }

    @Override
    protected DefaultSSOService.SSOUserMapping findMappingInDatabase(final Collection<String> roles) {
        final List<SamlUserGroupModel> userGroupModels = samlUserGroupLookupStrategy.getSamlUserGroupsForUserType(roles, EmployeeModel._TYPECODE);
        validateMappings(roles, userGroupModels);
        return performMapping(userGroupModels);
    }

    @Override
    public UserModel getOrCreateSSOUser(String id, String name, SAMLCredential samlCredential, HttpServletRequest request) {
        return getOrCreateSSOUser(id, name, samlCredential);
    }

}