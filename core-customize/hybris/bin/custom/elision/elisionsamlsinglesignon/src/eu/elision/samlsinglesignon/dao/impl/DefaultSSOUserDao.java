package eu.elision.samlsinglesignon.dao.impl;

import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import eu.elision.samlsinglesignon.dao.SSOUserDao;

import java.util.Collections;
import java.util.List;

/**
 * Implementation for SSOUserDao
 */
public class DefaultSSOUserDao extends DefaultGenericDao<CustomerModel> implements SSOUserDao {

    /**
     * Default Constructor for DefaultSSOUserDao which sets the typeCode to search for
     */
    public DefaultSSOUserDao() {
        super("Customer");
    }

    @Override
    public UserModel findUserByCustomerID(String customerID) {
        List<CustomerModel> resList = this.find(Collections.singletonMap("customerID", customerID));
        ServicesUtil.validateIfSingleResult(resList, "No Customer with given id [" + customerID + "] was found", "More than one customer with given id [" + customerID + "] was found");
        return resList.get(0);
    }
}
