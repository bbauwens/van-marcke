package eu.elision.samlsinglesignon.converters.populators.impl;

import com.vanmarcke.core.enums.ReferenceConditionalType;
import com.vanmarcke.cpi.data.customer.CustomerInfoResponseData;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.storelocator.pos.PointOfServiceService;

import static java.util.Locale.ENGLISH;

/**
 * The {@link SSOB2BUnitModelPopulator} class populates the B2BUnit information.
 *
 * @author Taki Korovessis, Niels Raemaekers
 * @since 19-12-2019
 */
public class SSOB2BUnitModelPopulator implements Populator<CustomerInfoResponseData, B2BUnitModel> {

    private final EnumerationService enumerationService;
    private final PointOfServiceService pointOfServiceService;

    /**
     * Creates a new instance of the {@link EnumerationService} class.
     *
     * @param enumerationService the enumeration service
     * @param pointOfServiceService the point of service service
     */
    public SSOB2BUnitModelPopulator(EnumerationService enumerationService, PointOfServiceService pointOfServiceService) {
        this.enumerationService = enumerationService;
        this.pointOfServiceService = pointOfServiceService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void populate(final CustomerInfoResponseData source, final B2BUnitModel target) {
        target.setUid(source.getCode());
        target.setName(source.getName());
        target.setLocName(source.getName(), ENGLISH);
        target.setRef1(enumerationService.getEnumerationValue(ReferenceConditionalType.class, source.getRef1()));
        target.setRef2(enumerationService.getEnumerationValue(ReferenceConditionalType.class, source.getRef2()));

        if (source.getFavoriteTec() != null) {
            target.setFavoritePointOfService(pointOfServiceService.getPointOfServiceForName(source.getFavoriteTec()));
        }
    }
}
