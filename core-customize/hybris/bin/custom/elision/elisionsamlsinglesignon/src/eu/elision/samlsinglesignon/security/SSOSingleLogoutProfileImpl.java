package eu.elision.samlsinglesignon.security;

import org.springframework.security.saml.websso.SingleLogoutProfileImpl;

import java.time.ZonedDateTime;

public class SSOSingleLogoutProfileImpl extends SingleLogoutProfileImpl {

    @Override
    public int getResponseSkew() {
        // calculate the offset in seconds because the IDP sends UTC timestamps
        final int offset = calculateOffset();
        return super.getResponseSkew() + offset;
    }

    protected int calculateOffset() {
        return ZonedDateTime.now().getOffset().getTotalSeconds();
    }

}