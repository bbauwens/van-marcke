package eu.elision.samlsinglesignon.converters.populators.impl;

import com.vanmarcke.cpi.data.customer.CustomerInfoResponseData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;

/**
 * The {@link SSOAddressModelPopulator} class populates the address information.
 *
 * @author Taki Korovessis, Christiaan Janssen
 * @since 19/12/2019
 */
public class SSOAddressModelPopulator implements Populator<CustomerInfoResponseData, AddressModel> {

    private final CommonI18NService commonI18NService;
    private final Map<String, String> countryMappingsConfiguration;

    /**
     * Creates a new instance of {@link SSOAddressModelPopulator}
     *
     * @param commonI18NService            the commonI18NService
     * @param countryMappingsConfiguration the countryMappingsConfiguration
     */
    public SSOAddressModelPopulator(final CommonI18NService commonI18NService, final Map<String, String> countryMappingsConfiguration) {
        this.commonI18NService = commonI18NService;
        this.countryMappingsConfiguration = countryMappingsConfiguration;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void populate(final CustomerInfoResponseData source, final AddressModel target) {
        target.setCompany(source.getName());
        target.setStreetname(source.getAddressLine());
        target.setStreetnumber(source.getAddressLine2());
        target.setAppartment(source.getApartment());
        target.setTown(source.getTown());
        target.setPostalcode(source.getPostalcode());
        target.setPhone1(correctPhoneNumber(source.getPhone1(), 10));
        target.setPhone2(correctPhoneNumber(source.getPhone2(), 14));

        populateCountry(source, target);
    }

    protected void populateCountry(final CustomerInfoResponseData source, final AddressModel target) {
        final String iso = countryMappingsConfiguration.getOrDefault(source.getCountry(), source.getCountry());
        final CountryModel country = commonI18NService.getCountry(iso);
        target.setCountry(country);
    }

    /**
     * Limits the given {@code input} to the {@code max} number of characters.
     *
     * @param input the input
     * @param max   the output
     * @return the corrected phone number
     */
    protected String correctPhoneNumber(String input, int max) {
        String phone = input;
        if (StringUtils.isNotEmpty(input) && input.length() > max) {
            phone = input.substring(0, max);
        }
        return phone;
    }
}