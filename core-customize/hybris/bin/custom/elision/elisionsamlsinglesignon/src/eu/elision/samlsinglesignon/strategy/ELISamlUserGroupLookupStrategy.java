package eu.elision.samlsinglesignon.strategy;

import de.hybris.platform.samlsinglesignon.model.SamlUserGroupModel;

import java.util.Collection;
import java.util.List;

/**
 * A strategy to look up {@link SamlUserGroupModel} based on user type.
 * <p>
 * {@link SamlUserGroupModel} with different {@link SamlUserGroupModel#getUserType()} cannot be mixed!
 */
public interface ELISamlUserGroupLookupStrategy {

    /**
     * Gets the list of {@link SamlUserGroupModel} for the given Saml roles and user type.
     *
     * @param roles    the roles
     * @param userType the userType
     * @return list of {@link SamlUserGroupModel}
     */
    List<SamlUserGroupModel> getSamlUserGroupsForUserType(Collection<String> roles, String userType);
}