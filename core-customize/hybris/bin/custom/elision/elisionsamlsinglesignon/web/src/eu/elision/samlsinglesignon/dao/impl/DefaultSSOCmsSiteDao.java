package eu.elision.samlsinglesignon.dao.impl;

import com.vanmarcke.core.model.CommerceGroupModel;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.enumeration.EnumerationValueModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import eu.elision.samlsinglesignon.dao.SSOCmsSiteDao;

import java.util.Map;

/**
 * Implementation for {@link SSOCmsSiteDao}
 *
 * @author Niels Raemaekers
 * @since 17/06/2020
 */
public class DefaultSSOCmsSiteDao implements SSOCmsSiteDao {

    private static final String QUERY = "SELECT {cms.pk} " +
            "FROM {" + CMSSiteModel._TYPECODE + " AS cms " +
            "JOIN " + CommerceGroupModel._TYPECODE + " AS cg ON {cg.pk} = {cms." + CMSSiteModel.COMMERCEGROUP + "} " +
            "JOIN " + EnumerationValueModel._TYPECODE + " AS enum ON {enum.pk} = {cms." + CMSSiteModel.CHANNEL + "}} " +
            "WHERE {cg." + CommerceGroupModel.COUNTRY + "} = ?country " +
            "AND {enum." + EnumerationValueModel.CODE + "} != ?channel " +
            "AND {cms." + CMSSiteModel.ECOMMERCEENABLED + "} = ?ecommerceEnabled";

    private final FlexibleSearchService flexibleSearchService;

    /**
     * Constructor for {@link DefaultSSOCmsSiteDao}
     *
     * @param flexibleSearchService the {@link FlexibleSearchService}
     */
    public DefaultSSOCmsSiteDao(FlexibleSearchService flexibleSearchService) {
        this.flexibleSearchService = flexibleSearchService;
    }

    /**
     * @inheritDoc
     */
    @Override
    public CMSSiteModel findCMSSiteForUserBasedOnCountry(CountryModel country) {
        final FlexibleSearchQuery fQuery = new FlexibleSearchQuery(QUERY);
        fQuery.addQueryParameters(Map.of(
                "country", country,
                "channel", SiteChannel.DIY.getCode(),
                "ecommerceEnabled", 1
        ));
        return flexibleSearchService.searchUnique(fQuery);
    }
}
