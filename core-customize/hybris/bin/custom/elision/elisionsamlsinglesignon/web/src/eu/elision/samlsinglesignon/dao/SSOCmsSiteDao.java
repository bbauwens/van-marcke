package eu.elision.samlsinglesignon.dao;

import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.core.model.c2l.CountryModel;

/**
 * Dao for the SSO CMS Site
 *
 * @author Niels Raemaekers
 * @since 17/06/2020
 */
public interface SSOCmsSiteDao {

    /**
     * Find the CMS site for the user based on the country
     * <p>
     * CMS sites with channel {@link SiteChannel#DIY} are not included in search.
     *
     * @param country the Country
     * @return the CMSSite
     */
    CMSSiteModel findCMSSiteForUserBasedOnCountry(CountryModel country);
}
