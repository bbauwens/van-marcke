package eu.elision.samlsinglesignon.handler;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutHandler;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

import static com.google.common.base.Preconditions.checkArgument;
import static java.util.Arrays.asList;

/**
 * A logout handler which clears a defined list of cookie names
 */
public class SAMLCookieClearingLogoutHandler implements LogoutHandler {

    private final List<String> cookiesToClear;

    /**
     * Creates a new instance of {@link SAMLCookieClearingLogoutHandler}
     *
     * @param cookiesToClear the cookies to clear
     */
    public SAMLCookieClearingLogoutHandler(final String... cookiesToClear) {
        checkArgument(cookiesToClear != null, "List of cookies cannot be null");
        this.cookiesToClear = asList(cookiesToClear);
    }

    @Override
    public void logout(final HttpServletRequest request, final HttpServletResponse response, final Authentication authentication) {
        cookiesToClear.stream().forEach(e -> {
            Cookie cookie = new Cookie(e, null);
            cookie.setPath("/");
            cookie.setMaxAge(0);
            response.addCookie(cookie);
        });
    }
}