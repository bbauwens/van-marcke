package eu.elision.strategy.impl;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.user.UserModel;
import eu.elision.samlsinglesignon.dao.SSOCmsSiteDao;
import eu.elision.strategy.ELIReferenceURLStrategy;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;

/**
 * Implementation for {@link ELIReferenceURLStrategy}
 *
 * @author Niels Raemaekers
 * @since 17/06/2020
 */
public class DefaultELIReferenceURLStrategy implements ELIReferenceURLStrategy {

    private SSOCmsSiteDao ssoCmsSiteDao;

    /**
     * Constructor for {@link DefaultELIReferenceURLStrategy}
     *
     * @param ssoCmsSiteDao the {@link SSOCmsSiteDao}
     */
    public DefaultELIReferenceURLStrategy(SSOCmsSiteDao ssoCmsSiteDao) {
        this.ssoCmsSiteDao = ssoCmsSiteDao;
    }

    /**
     * @inheritDoc
     */
    @Override
    public String createReferenceURL(HttpServletRequest request, UserModel user) {
        String referenceUrl = StringUtils.substringAfter(request.getServletPath(), "/saml/");

        if (user instanceof B2BCustomerModel && StringUtils.isEmpty(referenceUrl)) {
            B2BCustomerModel customer = (B2BCustomerModel) user;

            if (customer.getSessionLanguage() != null) {
                referenceUrl = user.getSessionLanguage().getIsocode();
            } else {
                B2BUnitModel company = customer.getDefaultB2BUnit();
                CountryModel country = company.getBillingAddress().getCountry();
                if (country != null) {
                    CMSSiteModel cmsSite = ssoCmsSiteDao.findCMSSiteForUserBasedOnCountry(country);
                    referenceUrl = cmsSite.getDefaultLanguage().getIsocode();
                }
            }
        }

        if (StringUtils.isNotEmpty(request.getQueryString())) {
            return referenceUrl + (request.getQueryString().isEmpty() ? "" : "?" + request.getQueryString());
        }

        return referenceUrl;
    }
}
