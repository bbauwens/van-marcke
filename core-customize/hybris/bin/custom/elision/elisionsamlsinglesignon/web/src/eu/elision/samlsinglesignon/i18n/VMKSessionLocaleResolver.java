package eu.elision.samlsinglesignon.i18n;

import org.springframework.context.i18n.LocaleContext;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.i18n.TimeZoneAwareLocaleContext;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;
import java.util.TimeZone;

public class VMKSessionLocaleResolver extends SessionLocaleResolver {

    /**
     * {@inheritDoc}
     */
    @Override
    public LocaleContext resolveLocaleContext(HttpServletRequest request) {
        return new TimeZoneAwareLocaleContext() {

            public Locale getLocale() {
                Locale locale = LocaleContextHolder.getLocale();
                if (locale == null) {
                    locale = determineDefaultLocale(request);
                }
                return locale;
            }

            public TimeZone getTimeZone() {
                return determineDefaultTimeZone(request);
            }
        };
    }
}
