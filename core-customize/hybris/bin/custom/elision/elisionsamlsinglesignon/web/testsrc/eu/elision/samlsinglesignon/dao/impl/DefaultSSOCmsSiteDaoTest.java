package eu.elision.samlsinglesignon.dao.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;
import static org.fest.assertions.MapAssert.entry;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultSSOCmsSiteDaoTest {

    @Captor
    private ArgumentCaptor<FlexibleSearchQuery> flexibleSearchQueryCaptor;
    @Mock
    FlexibleSearchService flexibleSearchService;
    @InjectMocks
    private DefaultSSOCmsSiteDao defaultSSOCmsSiteDao;

    @Test
    public void testFindCMSSiteForUserBasedOnCountry() {
        CMSSiteModel cmsSite = mock(CMSSiteModel.class);
        CountryModel country = mock(CountryModel.class);

        when(this.flexibleSearchService.searchUnique(this.flexibleSearchQueryCaptor.capture())).thenReturn(cmsSite);
        defaultSSOCmsSiteDao.findCMSSiteForUserBasedOnCountry(country);

        final FlexibleSearchQuery fQuery = this.flexibleSearchQueryCaptor.getValue();
        assertThat(fQuery.getQueryParameters()).includes(entry("country", country), entry("channel", SiteChannel.DIY), entry("ecommerceEnabled", 1));
        assertThat(fQuery.getQuery()).isEqualTo("SELECT {cms.pk} FROM {CMSSite AS cms JOIN CommerceGroup AS cg ON {cg.pk} = {cms.commerceGroup} JOIN EnumerationValue AS enum ON {enum.pk} = {cms.channel}} WHERE {cg.country} = ?country AND {enum.code} != ?channel AND {cms.ecommerceEnabled} = ?ecommerceEnabled");
    }
}