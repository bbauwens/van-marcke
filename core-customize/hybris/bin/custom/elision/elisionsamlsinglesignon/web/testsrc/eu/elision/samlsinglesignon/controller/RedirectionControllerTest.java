package eu.elision.samlsinglesignon.controller;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.samlsinglesignon.SAMLService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import org.apache.commons.lang3.RandomStringUtils;
import org.fest.assertions.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.saml.SAMLCredential;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.net.URL;
import java.util.Collections;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

/**
 * The {@link RedirectionControllerTest} class contains the unit tests for the {@link RedirectionController} class.
 *
 * @author Christiaan Janssen
 * @since 07-08-2020
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class RedirectionControllerTest {

    private static final String BASE_URL = "https://blue.vmk.com:9002/";
    private static final String REFERENCE_URL = RandomStringUtils.random(10);

    @Mock
    private SAMLService samlService;

    @Mock
    private CMSSiteService cmsSiteService;

    @Mock
    private CommonI18NService i18NService;

    @InjectMocks
    private RedirectionController redirectionController;

    @Captor
    private ArgumentCaptor<URL> captor;

    @Test
    public void testGetLocalizedURLWithValidLanguageCountryCombination() throws CMSItemNotFoundException {
        when(cmsSiteService.getSiteForURL(any(URL.class))).thenReturn(null);

        String url = redirectionController.getLocalizedURL(BASE_URL, "nl_BE/" + REFERENCE_URL, "nl", "BE");

        Assertions
                .assertThat(url)
                .isEqualTo(BASE_URL + "nl_BE/" + REFERENCE_URL);

        verify(cmsSiteService).getSiteForURL(captor.capture());

        Assertions
                .assertThat(captor.getValue().toString())
                .isEqualTo(BASE_URL + "nl_BE/" + REFERENCE_URL);
    }

    @Test
    public void testGetLocalizedURLWithInvalidLanguageCountryCombination() throws CMSItemNotFoundException {
        CMSItemNotFoundException exception = mock(CMSItemNotFoundException.class);

        when(cmsSiteService.getSiteForURL(any(URL.class))).thenThrow(exception);

        LanguageModel language = mock(LanguageModel.class);
        when(language.getIsocode()).thenReturn("nl_BE");

        CMSSiteModel site = mock(CMSSiteModel.class);
        when(site.getUid()).thenReturn("blue-be");
        when(site.getDefaultLanguage()).thenReturn(language);

        when(cmsSiteService.getSites()).thenReturn(Collections.singletonList(site));

        String url = redirectionController.getLocalizedURL(BASE_URL, REFERENCE_URL, "en", "BE");

        Assertions
                .assertThat(url)
                .isEqualTo(BASE_URL + "nl_BE/" + REFERENCE_URL);

        verify(cmsSiteService).getSiteForURL(captor.capture());

        Assertions
                .assertThat(captor.getValue().toString())
                .isEqualTo(BASE_URL + "en_BE/" + REFERENCE_URL);
    }

    @Test
    public void testGetLanguage_cookie() {
        SAMLCredential credential = mock(SAMLCredential.class);
        HttpServletRequest request = mock(HttpServletRequest.class);
        Cookie languageCookie = new Cookie("language", "nl_DE");

        when(request.getCookies()).thenReturn(new Cookie[]{languageCookie});

        Assertions
                .assertThat(redirectionController.getLanguage(credential, request))
                .isEqualTo("nl");

        verifyZeroInteractions(samlService);
    }

    @Test
    public void testGetLanguage_credential() {
        SAMLCredential credential = mock(SAMLCredential.class);
        HttpServletRequest request = mock(HttpServletRequest.class);

        when(request.getCookies()).thenReturn(new Cookie[]{});
        when(samlService.getLanguage(credential, request, i18NService))
                .thenReturn("someLanguage");

        Assertions
                .assertThat(redirectionController.getLanguage(credential, request))
                .isEqualTo("someLanguage");

        verify(samlService).getLanguage(credential, request, i18NService);
    }
}