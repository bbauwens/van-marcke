<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<jsp:useBean id="currentDate" class="java.util.Date" />

<html>
<head>
    <title><spring:message code="error.header"/></title>
    <link rel="stylesheet" type="text/css" media="all" href="/_ui/responsive/theme-vanmarckeblue/css/style.css">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta name="robots" content="noindex,follow">
    <link rel="shortcut icon" type="image/x-icon" media="all" href="/_ui/responsive/theme-vanmarckeblue/images/favicons">
    <link rel="apple-touch-icon" sizes="180x180" href="/_ui/responsive/theme-vanmarckeblue/images/favicons/apple-touch-icon.png?v=vMQObMXjK2">
    <link rel="icon" type="image/png" sizes="32x32" href="/_ui/responsive/theme-vanmarckeblue/images/favicons/favicon-32x32.png?v=vMQObMXjK2">
    <link rel="icon" type="image/png" sizes="16x16" href="/_ui/responsive/theme-vanmarckeblue/images/favicons/favicon-16x16.png?v=vMQObMXjK2">
    <link rel="manifest" href="/_ui/responsive/theme-vanmarckeblue/images/favicons/site.webmanifest?v=vMQObMXjK3">
    <link rel="mask-icon" href="/_ui/responsive/theme-vanmarckeblue/images/favicons/safari-pinned-tab.svg?v=vMQObMXjK2" color="#00365c">
    <link rel="shortcut icon" href="/_ui/responsive/theme-vanmarckeblue/images/favicons/favicon.ico?v=vMQObMXjK2">
</head>
<body>
    <header class="vmb-main-header">
        <div class="vmb-site-logo">
            <a class="logo" href="<spring:message code="error.homeUrl"/>"></a>
        </div>
    </header>
    <div class="main__inner-wrapper">
        <div class="error-page">
            <img src="/_ui/responsive/theme-vanmarckeblue/images/404.png" alt="404">
            <h3><spring:message code="error.title"/></h3>
            <p>
                <spring:message code="error.paragraph.one" htmlEscape="false"/><br/><br/>
                <spring:message code="error.paragraph.two" htmlEscape="false"/>
            </p>
            <p></p>
        </div>
    </div>
    <footer>
        <div class="footer__bottom">
            <div class="footer__copyright">
                <div class="container">
                    <fmt:formatDate value="${currentDate}" pattern="yyyy" var="currentYear" />
                    <spring:message code="error.footer" arguments="${currentYear}"/>
                </div>
            </div>
        </div>
    </footer>
</body>
</html>
