package eu.elision.samlsinglesignon.dao.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultSSOUserDaoTest {

    @Mock
    List<CustomerModel> resList;
    @InjectMocks
    private MockDefaultSSOUserDao defaultSSOUserDao = new MockDefaultSSOUserDao();

    @Test
    public void findUserByCustomerID() {
        String customerID = "P00001";
        B2BCustomerModel user = mock(B2BCustomerModel.class);
        Iterator iterator = mock(Iterator.class);

        when(resList.size()).thenReturn(1);
        when(resList.get(0)).thenReturn(user);
        when(resList.iterator()).thenReturn(iterator);
        UserModel result = defaultSSOUserDao.findUserByCustomerID(customerID);

        assertThat(result).isEqualTo(user);
    }

    @Test
    public void findUserByCustomerID_noResult() {
        String customerID = "P00001";
        Iterator iterator = mock(Iterator.class);

        when(resList.size()).thenReturn(0);
        when(resList.iterator()).thenReturn(iterator);
        UserModel result = defaultSSOUserDao.findUserByCustomerID(customerID);

        assertThat(result).isEqualTo(null);
    }

    @Test(expected = AmbiguousIdentifierException.class)
    public void findUserByCustomerID_multipleResults() {
        String customerID = "P00001";

        when(resList.size()).thenReturn(2);
        defaultSSOUserDao.findUserByCustomerID(customerID);
    }

    public class MockDefaultSSOUserDao extends DefaultSSOUserDao {
        @Override
        public List<CustomerModel> find(Map<String, ? extends Object> params) {
            return resList;
        }
    }
}