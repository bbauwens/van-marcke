package eu.elision.samlsinglesignon.security;

import de.hybris.bootstrap.annotations.UnitTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.ZoneId;
import java.time.ZonedDateTime;

import static org.fest.assertions.Assertions.assertThat;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SSOSingleLogoutProfileImplTest {

    private SSOSingleLogoutProfileImpl ssoSingleLogoutProfile = new SSOSingleLogoutProfileImpl() {
        @Override
        protected int calculateOffset() {
            return ZonedDateTime.now(ZoneId.of("+02:00")).getOffset().getTotalSeconds();
        }
    };

    @Test
    public void testGetResponseSkew() {
        ssoSingleLogoutProfile.setResponseSkew(120);

        int result = ssoSingleLogoutProfile.getResponseSkew();

        assertThat(result).isEqualTo(7320);
    }

}