package eu.elision.samlsinglesignon.converters.populators.impl;

import com.vanmarcke.core.enums.ReferenceConditionalType;
import com.vanmarcke.cpi.data.customer.CustomerInfoResponseData;
import com.vanmarcke.services.model.builder.B2BUnitModelMockBuilder;
import com.vanmarcke.services.model.builder.PointOfServiceModelMockBuilder;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import de.hybris.platform.storelocator.pos.PointOfServiceService;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Locale;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * The {@link SSOB2BUnitModelPopulatorTest} class contains the unit tests for the
 * {@link SSOB2BUnitModelPopulator} class.
 *
 * @author Taki Korovessis, Niels Raemaekers
 * @since 19-12-2019
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SSOB2BUnitModelPopulatorTest {

    private static final String CODE = RandomStringUtils.random(10);
    private static final String NAME = RandomStringUtils.random(10);
    private static final String REF1 = RandomStringUtils.random(10);
    private static final String REF2 = RandomStringUtils.random(10);
    private static final String FAVORITE_TEC = "NQ";

    @Mock
    private EnumerationService enumerationService;
    @Mock
    private PointOfServiceService pointOfServiceService;

    @InjectMocks
    private SSOB2BUnitModelPopulator ssob2BUnitModelPopulator;

    @Test
    public void testPopulate() {
        // given
        CustomerInfoResponseData response = new CustomerInfoResponseData();
        response.setCode(CODE);
        response.setName(NAME);
        response.setRef1(REF1);
        response.setRef2(REF2);
        response.setFavoriteTec(FAVORITE_TEC);
        PointOfServiceModel favoriteTec = PointOfServiceModelMockBuilder.aPointOfService()
                .withName(FAVORITE_TEC)
                .build();

        when(enumerationService.getEnumerationValue(ReferenceConditionalType.class, REF1)).thenReturn(ReferenceConditionalType.OPTIONAL);
        when(enumerationService.getEnumerationValue(ReferenceConditionalType.class, REF2)).thenReturn(ReferenceConditionalType.MANDATORY);
        when(pointOfServiceService.getPointOfServiceForName(FAVORITE_TEC)).thenReturn(favoriteTec);

        B2BUnitModel result = B2BUnitModelMockBuilder
                .aB2BUnit(null)
                .build();

        // when
        ssob2BUnitModelPopulator.populate(response, result);

        // then
        verify(result).setUid(CODE);
        verify(result).setName(NAME);
        verify(result).setLocName(NAME, Locale.ENGLISH);
        verify(result).setRef1(ReferenceConditionalType.OPTIONAL);
        verify(result).setRef2(ReferenceConditionalType.MANDATORY);
        verify(result).setFavoritePointOfService(favoriteTec);

        verify(enumerationService).getEnumerationValue(ReferenceConditionalType.class, REF1);
        verify(enumerationService).getEnumerationValue(ReferenceConditionalType.class, REF2);
        verify(pointOfServiceService).getPointOfServiceForName(FAVORITE_TEC);
    }
}
