package eu.elision.samlsinglesignon.strategy.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.type.TypeModel;
import de.hybris.platform.samlsinglesignon.SamlUserGroupDAO;
import de.hybris.platform.samlsinglesignon.model.SamlUserGroupModel;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultELISamlUserGroupLookupStrategyTest {

    @Mock
    private SamlUserGroupDAO samlUserGroupDAO;
    @InjectMocks
    private DefaultELISamlUserGroupLookupStrategy defaultELISamlUserGroupLookupStrategy;

    @Mock
    private TypeModel userTypeModel1;
    @Mock
    private TypeModel userTypeModel2;
    @Mock
    private SamlUserGroupModel samlUserGroupModel1;
    @Mock
    private SamlUserGroupModel samlUserGroupModel2;

    @Before
    public void setup() {
        when(userTypeModel1.getCode()).thenReturn("user-type-1");
        when(userTypeModel2.getCode()).thenReturn("user-type-2");

        when(samlUserGroupModel1.getUserType()).thenReturn(userTypeModel1);
        when(samlUserGroupModel2.getUserType()).thenReturn(userTypeModel2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetSamlUserGroupsForUserType_without_roles_should_throw_IllegalArgumentException() {
        defaultELISamlUserGroupLookupStrategy.getSamlUserGroupsForUserType(null, "user-type");
    }

    @Test
    public void testGetSamlUserGroupsForUserType_should_return_only_samlUserGroups_matching_with_given_userType() {
        when(samlUserGroupDAO.findSamlUserGroup("role-1")).thenReturn(Optional.of(samlUserGroupModel1));
        when(samlUserGroupDAO.findSamlUserGroup("role-2")).thenReturn(Optional.of(samlUserGroupModel2));

        List<SamlUserGroupModel> result = defaultELISamlUserGroupLookupStrategy.getSamlUserGroupsForUserType(Arrays.asList("role-1", "role-2"), "user-type-1");

        assertThat(result).containsExactly(samlUserGroupModel1);
    }

}