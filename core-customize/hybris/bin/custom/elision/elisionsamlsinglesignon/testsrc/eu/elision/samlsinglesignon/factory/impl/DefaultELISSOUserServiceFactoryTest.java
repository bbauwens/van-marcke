package eu.elision.samlsinglesignon.factory.impl;


import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.core.model.user.EmployeeModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.samlsinglesignon.SAMLService;
import eu.elision.samlsinglesignon.enums.SSOContext;
import eu.elision.samlsinglesignon.service.ELISSOUserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.saml.SAMLCredential;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultELISSOUserServiceFactoryTest {

    @Mock
    private SAMLService samlService;

    @Mock
    private ELISSOUserService asmSsoUserService;

    @Mock
    private ELISSOUserService b2bSsoUserService;

    private DefaultELISSOUserServiceFactory defaultELISSOUserServiceFactory;

    @Mock
    private SAMLCredential samlCredential;

    @Mock
    private EmployeeModel employeeModel;

    @Mock
    private HttpServletRequest request;

    @Mock
    private B2BCustomerModel b2BCustomerModel;

    @Before
    public void setup() {
        Map<SSOContext, ELISSOUserService> factoryConfigurationMap = new HashMap<>();
        factoryConfigurationMap.put(SSOContext.ASM, asmSsoUserService);
        factoryConfigurationMap.put(SSOContext.B2B, b2bSsoUserService);

        defaultELISSOUserServiceFactory = new DefaultELISSOUserServiceFactory(samlService, factoryConfigurationMap);

        when(samlService.getUserId(samlCredential)).thenReturn("user-id");
        when(samlService.getUserName(samlCredential)).thenReturn("user-name");

        when(asmSsoUserService.getOrCreateSSOUser("user-id", "user-name", samlCredential, request)).thenReturn(employeeModel);
        when(b2bSsoUserService.getOrCreateSSOUser("user-id", "user-name", samlCredential, request)).thenReturn(b2BCustomerModel);
    }

    @Test
    public void testGetOrCreateSSOUser_withASM() {
        UserModel result = defaultELISSOUserServiceFactory.getOrCreateSSOUser(SSOContext.ASM, samlCredential, request);
        assertThat(result).isEqualTo(employeeModel);

        verifyZeroInteractions(b2bSsoUserService);
    }

    @Test
    public void testGetOrCreateSSOUser_withB2B() {
        UserModel result = defaultELISSOUserServiceFactory.getOrCreateSSOUser(SSOContext.B2B, samlCredential, request);
        assertThat(result).isEqualTo(b2BCustomerModel);

        verifyZeroInteractions(asmSsoUserService);
    }

}