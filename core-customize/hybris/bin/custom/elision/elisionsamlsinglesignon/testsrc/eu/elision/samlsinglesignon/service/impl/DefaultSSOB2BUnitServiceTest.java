package eu.elision.samlsinglesignon.service.impl;

import com.vanmarcke.cpi.data.customer.CustomerInfoResponseData;
import com.vanmarcke.cpi.data.customer.CustomerRequestData;
import com.vanmarcke.cpi.data.customer.CustomerResponseData;
import com.vanmarcke.cpi.services.CustomerService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2b.company.B2BCommerceUnitService;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.EmployeeModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.MockSessionService;
import de.hybris.platform.servicelayer.user.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static java.util.Collections.singletonList;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultSSOB2BUnitServiceTest {

    @Mock
    private ModelService modelService;
    @Mock
    private MockSessionService sessionService;
    @Mock
    private B2BCommerceUnitService b2BCommerceUnitService;
    @Mock
    private UserService userService;
    @Mock
    private CustomerService cpiCustomerService;
    @Mock
    private Converter<CustomerInfoResponseData, AddressModel> addressModelConverter;
    @Mock
    private Converter<CustomerInfoResponseData, B2BUnitModel> b2BUnitModelConverter;
    @InjectMocks
    private DefaultSSOB2BUnitService defaultSSOB2BUnitService;

    @Captor
    private ArgumentCaptor<CustomerRequestData> customerRequestDataArgumentCaptor;

    @Test
    public void testGetOrCreateSSOB2BUnit_withExistingB2BUnit() {
        B2BUnitModel unitModel = mock(B2BUnitModel.class);

        when(b2BCommerceUnitService.getUnitForUid("uid")).thenReturn(unitModel);

        B2BUnitModel result = defaultSSOB2BUnitService.getOrCreateSSOB2BUnit("uid");
        assertThat(result).isEqualTo(unitModel);

        verifyZeroInteractions(sessionService);
        verifyZeroInteractions(userService);
        verifyZeroInteractions(cpiCustomerService);
        verifyZeroInteractions(addressModelConverter);
        verifyZeroInteractions(b2BUnitModelConverter);
    }

    @Test
    public void testGetOrCreateSSOB2BUnit_withNewB2BUnit() {
        AddressModel addressModel = mock(AddressModel.class);
        B2BUnitModel unitModel = mock(B2BUnitModel.class);
        CustomerResponseData response = mock(CustomerResponseData.class);
        CustomerInfoResponseData customerInfo = mock(CustomerInfoResponseData.class);
        EmployeeModel adminUser = mock(EmployeeModel.class);

        when(response.getData()).thenReturn(singletonList(customerInfo));

        when(b2BCommerceUnitService.getUnitForUid("uid")).thenReturn(null);

        when(cpiCustomerService.getCustomer(customerRequestDataArgumentCaptor.capture())).thenReturn(response);

        when(userService.getAdminUser()).thenReturn(adminUser);

        doCallRealMethod().when(sessionService).executeInLocalView(any(), eq(adminUser));

        when(modelService.create(B2BUnitModel.class)).thenReturn(unitModel);

        when(modelService.create(AddressModel.class)).thenReturn(addressModel);

        B2BUnitModel result = defaultSSOB2BUnitService.getOrCreateSSOB2BUnit("uid");
        assertThat(result).isEqualTo(unitModel);

        verify(addressModel).setBillingAddress(true);
        verify(addressModel).setShippingAddress(true);
        verify(addressModel).setVisibleInAddressBook(true);

        verify(unitModel).setShippingAddress(addressModel);
        verify(unitModel).setBillingAddress(addressModel);

        verify(addressModelConverter).convert(customerInfo, addressModel);
        verify(b2BUnitModelConverter).convert(customerInfo, unitModel);
        verify(b2BCommerceUnitService).saveAddressEntry(unitModel, addressModel);

        assertThat(customerRequestDataArgumentCaptor.getValue().getCustomerID()).isEqualTo("uid");
    }

}