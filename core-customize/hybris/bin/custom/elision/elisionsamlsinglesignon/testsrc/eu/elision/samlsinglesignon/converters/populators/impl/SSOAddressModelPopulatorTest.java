package eu.elision.samlsinglesignon.converters.populators.impl;

import com.vanmarcke.cpi.data.customer.CustomerInfoResponseData;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import org.apache.commons.lang3.RandomStringUtils;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static java.util.Collections.emptyMap;
import static org.mockito.Mockito.*;

/**
 * The {@link SSOAddressModelPopulatorTest} class contains the unit tests for the {@link SSOAddressModelPopulator} class.
 *
 * @author Taki Korovessis, Christiaan Janssen
 * @since 19/12/2019
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SSOAddressModelPopulatorTest {

    private static final String COMPANY = RandomStringUtils.randomAlphabetic(10);
    private static final String LINE1 = RandomStringUtils.randomAlphabetic(10);
    private static final String LINE2 = RandomStringUtils.randomAlphabetic(10);
    private static final String APARTMENT = RandomStringUtils.randomAlphabetic(10);
    private static final String TOWN = RandomStringUtils.randomAlphabetic(10);
    private static final String POSTAL_CODE = RandomStringUtils.randomAlphabetic(4);
    private static final String COUNTRY_ISO = RandomStringUtils.randomAlphabetic(4);
    private static final String PHONE1 = RandomStringUtils.randomNumeric(10);
    private static final String PHONE2 = RandomStringUtils.randomNumeric(14);

    @Mock
    private CommonI18NService commonI18NService;

    private SSOAddressModelPopulator ssoAddressModelPopulator;

    @Before
    public void setup() {
        ssoAddressModelPopulator = new SSOAddressModelPopulator(commonI18NService, emptyMap());
    }

    @Test
    public void testPopulate() {
        CustomerInfoResponseData response = new CustomerInfoResponseData();
        response.setName(COMPANY);
        response.setAddressLine(LINE1);
        response.setAddressLine2(LINE2);
        response.setApartment(APARTMENT);
        response.setTown(TOWN);
        response.setPostalcode(POSTAL_CODE);
        response.setPhone1(PHONE1);
        response.setPhone2(PHONE2);
        response.setCountry(COUNTRY_ISO);

        CountryModel country = mock(CountryModel.class);

        when(commonI18NService.getCountry(COUNTRY_ISO)).thenReturn(country);

        AddressModel address = mock(AddressModel.class);

        ssoAddressModelPopulator.populate(response, address);

        verify(address).setCompany(COMPANY);
        verify(address).setStreetname(LINE1);
        verify(address).setStreetnumber(LINE2);
        verify(address).setAppartment(APARTMENT);
        verify(address).setPostalcode(POSTAL_CODE);
        verify(address).setTown(TOWN);
        verify(address).setPhone1(PHONE1);
        verify(address).setPhone2(PHONE2);
        verify(address).setCountry(country);

        verify(commonI18NService).getCountry(COUNTRY_ISO);
    }

    @Test
    public void testCorrectPhoneNumberWithNullValue() {
        String actualPhoneNumber = ssoAddressModelPopulator.correctPhoneNumber(null, 10);

        Assertions
                .assertThat(actualPhoneNumber)
                .isNull();
    }

    @Test
    public void testCorrectPhoneNumberWithValidValue() {
        String actualPhoneNumber = ssoAddressModelPopulator.correctPhoneNumber("0123456789", 10);

        Assertions
                .assertThat(actualPhoneNumber)
                .isEqualTo("0123456789");
    }

    @Test
    public void testCorrectPhoneNumberWithInvalidValue() {
        String actualPhoneNumber = ssoAddressModelPopulator.correctPhoneNumber("abcdefg0123456789", 10);

        Assertions
                .assertThat(actualPhoneNumber)
                .isEqualTo("abcdefg012");
    }
}