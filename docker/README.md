Run the command:
```
docker-compose up --detach
```

And that's it, your database is up, configured and running.
