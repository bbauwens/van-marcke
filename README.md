# SAP Commerce

## Requirements

### Requirements
In order to run SAP Commerce locally, you need to install the following software:

* Java 11 SDK or higher
* Git 2.24.2 or higher
* Apache Ant 1.10.7 or higher
* MySQL 8.0.17 or higher

Also, you need to get SAP Commerce 2005 (patch 8).

## Installation
To install SAP Commerce on you local machine, execute the following steps:

* Open a terminal window.
* Create a new directory in which you want to install SAP Commerce.
`mkdir ~/Development/van-marcke`
* Clone the Git repository into your installation folder.
`git clone https://[USERNAME]@bitbucket.org/xploregroup/van-marcke.git ~/Development/van-marcke`
* Unzip SAP Commerce and copy the hybris directory to your installation folder.
`cp -a CXCOMM200500P_X-X-XXXXXXXX/hybris/bin/* ~/Development/van-marcke/core-customize/hybris/bin`
* Unzip the cloudhotfolders_2005.zip and copy the required contents to your installation folder (Download can be found here: https://launchpad.support.sap.com/#/notes/2817992) 
`cp -a cloudhotfolders_2005.zip/* ~/Development/van-marcke/core-customize/hybris/bin/modules`
* Unzip the Integration Pack  and copy the required contents to your installation folder:
`cp -a CXCOMINTPK200500P_X-XXXXXXXX/hybris/bin/modules/* ~/Development/van-marcke/core-customize/hybris/bin/modules`

### Database

* Login to MySQL.
```
mysql -u _username_ -p
```
* Create a new database.
```
CREATE DATABASE vanmarcke;
```
* Create a new user. Don't forget to update the password.
```
CREATE USER 'vanmarcke'@'localhost' IDENTIFIED BY 'vanmarcke';
```
* Grant the user access to the created database.
```
GRANT ALL PRIVILEGES ON vanmarcke.* TO 'vanmarcke'@'localhost';
```
* Update the database password in the local.properties file.

### Database in Docker

* Change directory to the `docker` directory in the root dir.
* Run the Docker Compose command.
```
docker-compose up --detach
```

### Hostfile
* Open the hosts file.
```
sudo vi /etc/hosts
```

* Add the following entry:
```
127.0.0.1  blue.vmk.com
```

### Platform
* Run platform customize
```
ant customize
```
* Install the addons:
```
ant addoninstall -Daddonnames='b2bacceleratoraddon,smarteditaddon,vanmarckeblueaddon,assistedservicestorefront,elisionssoaddon,elisionl10naddon,vanmarckesaferpaystorefront,b2bpunchoutaddon' -DaddonStorefront.yacceleratorstorefront='vanmarckestorefront'
```
```
ant addoninstall -Daddonnames='acceleratorwebservicesaddon,cmsoccaddon,b2boccaddon,vanmarckeoccaddon' -DaddonStorefront.ycommercewebservices='ycommercewebservices'
```
* Run clean build
```
ant clean all
```
* Start the server
```
./hybrisserver.sh debug
```
* Initialize the system. Initialization can be done via command line or via the HAC.
* Make sure the patches are executed.


###  Azurite

* From Kitematic, create the arafato/azurite container. Azurite creates emulated components against the following Docker ports:
    - Azure Table Storage Emulator listening on port 10002
    - Azure Queue Storage Emulator listening on port 10001
    - Azure Blob Storage Emulator listening on port 10000
* Install and run Microsoft Azure Storage Explorer.
* Connect to Local Blob Storage with Microsoft Azure Storage Explorer.
    - On the left navigation panel, right click on 'Storage Accounts'.
    - Select 'Attach to local emulator' and click 'Next'.
    - Fill in the mapped localhost IP ports corresponding the Table, Queue and Blob Storage Emulators.

## Maintainers
Current maintainers:

*  Christiaan Janssen

