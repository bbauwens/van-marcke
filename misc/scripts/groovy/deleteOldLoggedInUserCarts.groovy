import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery
import de.hybris.platform.core.model.order.CartModel
import org.apache.commons.lang.StringUtils
import org.apache.log4j.Level
import org.apache.log4j.Logger
import java.time.format.DateTimeFormatter
import java.time.Instant
import java.time.LocalDateTime
import java.util.List
import java.util.Map
import java.util.HashMap
import java.util.Comparator
import java.util.stream.Collectors


LOGGER = Logger.getLogger "script.groovy.deleteOldLoggedInUserCarts"

def logInfo(message, Object... params) {
	finalMessage = String.format(message, params)
	LOGGER.info(finalMessage)
 	println(finalMessage)
}

def logError(message, e) {
  LOGGER.error(message, e)
  println(message + ": " + e.getStackTrace())
}


def cartsQuery = """select {u.customerid}, {u.uid}, {s.uid}, count({c.pk}) from {customer as u
	join cart as c on {u.pk} = {c.user}
	join cmssite as s on {c.site} = {s.pk}}
	where {u.customerid} is not NULL
	and {c.savetime} is NULL
	group by {u.customerid}, {u.uid}, {s.uid}
	having count({c.pk}) > 1"""

def flexibleSearchQuery = new FlexibleSearchQuery(cartsQuery);

flexibleSearchQuery.setResultClassList(Arrays.asList(String.class, String.class, String.class, Integer.class));
def rows = flexibleSearchService.search(flexibleSearchQuery).getResult();

println "********************"
println "Users with more than one cart per site"
println "********************"


for (List row : rows) {
	println "${row.get(0)}, ${row.get(1)}, ${row.get(2)}, ${row.get(3)}"
}

println "********************"


customerDao = spring.getBean("customerDao")
modelService = spring.getBean("modelService")

rows.each {row -> 
	customer = customerDao.findCustomerByCustomerId(row.get(0))
	logInfo("Cleaning up carts for user %s that has %s carts on site %s", row.get(0), row.get(3), row.get(2))
	carts = customer.getCarts()
		.findAll{ it.getSite().getUid().equals(row.get(2)) && it.getSaveTime() == null }

	currentCart = carts.max{ it.getModifiedtime() }
	logInfo("Current cart for user %s  is code: %s, value: %s, modifiedTime: %s", row.get(0), currentCart.getCode(), currentCart.getTotalPrice(), currentCart.getModifiedtime())

	carts.each{oldCart ->
		if (!oldCart.getCode().equals(currentCart.getCode())) {
			logInfo("Removing old cart for user %s  - cart code: %s, value: %s, modifiedTime: %s", row.get(0), oldCart.getCode(), oldCart.getTotalPrice(), oldCart.getModifiedtime())
			modelService.remove(oldCart)
		}
	}

}