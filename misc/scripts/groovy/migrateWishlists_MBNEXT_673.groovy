import de.hybris.platform.b2b.model.B2BUnitModel
import de.hybris.platform.core.model.media.MediaModel
import de.hybris.platform.core.model.product.ProductModel
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException
import de.hybris.platform.wishlist2.enums.Wishlist2EntryPriority
import de.hybris.platform.wishlist2.model.Wishlist2Model
import org.apache.log4j.Logger

LOGGER = Logger.getLogger "script.groovy.migrateSavedCarts"

// spring beans
userService = spring.getBean "userService"
modelService = spring.getBean "modelService"
b2bUnitService = spring.getBean "b2bUnitService"
b2bUnitDao = spring.getBean "b2bUnitDao"
wishlistService = spring.getBean "wishlistService"
wishlistDao = spring.getBean "wishlistDao"
productService = spring.getBean "productService"
catalogVersionService = spring.getBean "catalogVersionService"
mediaService = spring.getBean "mediaService"

// SET MEDIA HERE
def MEDIA_FILE_NAME = "wishlist_migration_prod"

def getRowsFromMedia(String mediaCode) {
    MediaModel media = mediaService.getMedia(catalogVersionService.getCatalogVersion("globalBlueContentCatalog", "Online"), mediaCode)
    InputStream rowStream = mediaService.getStreamFromMedia(media)
    List<String> rows = new ArrayList<>();
    try {
        BufferedReader reader = new BufferedReader(new InputStreamReader(rowStream));
        while(reader.ready()) {
            String line = reader.readLine()
            rows.add(line)
        }

    } catch(IOException e) {
        logError("Problem reading file from media %s", e, mediaCode)
    }

    return rows
}

def logInfo(message, Object... params) {
    def stringMessage = String.format(message, params);
    LOGGER.info(stringMessage)
    println(stringMessage)
}

def logError(message, e, Object... params) {
    def stringMessage = String.format(message, params);
    LOGGER.error(stringMessage, e)
    println(stringMessage + e.getMessage())
}

def getWishlistForUserAndValues(customer, rowValues) {
    def wishlistName = rowValues.get(2)
    Wishlist2Model wishlist
    try {
        wishlist = wishlistDao.getWishlistByUserAndName(customer, wishlistName)
    } catch (ModelNotFoundException e) {
        logInfo("Wishlist with code %s not found for user %s , creating a new one", wishlistName, customer.getUid());
        wishlist = modelService.create(Wishlist2Model.class);
        wishlist.setUser(customer)
        wishlist.setName(wishlistName)
        wishlist.setDescription(rowValues.get(4))
    }

    modelService.save(wishlist)
    return wishlist
}

def createWishlistFromRow(String row, int index) {
    List<String> rowValues = row.split(';')
    customer = userService.getUserForUID(rowValues.get(1))
    final String productCode = "" + rowValues.get(6)
    ProductModel productModel = productService.getProductForCode(catalogVersionService.getCatalogVersion("vanmarckeProductCatalog", "Online"), productCode)
    def quantity = Double.valueOf(rowValues.get(7)).intValue()

    def wishlist = getWishlistForUserAndValues(customer, rowValues);
    wishlistService.addWishlistEntry(wishlist, productModel, quantity, Wishlist2EntryPriority.HIGH, null)
    modelService.save(wishlist)
    logInfo("Successfully added product %s to wishlist %s for user %s", productModel.getCode(), wishlist.getName(), customer.getUid())
}


rows = getRowsFromMedia(MEDIA_FILE_NAME)
def sqlQueries = new HashSet<String>()
for (String line in rows) {
    logInfo("Importing line: %s", line)
    try {
        createWishlistFromRow(line, 0)
    } catch(UnknownIdentifierException e) {
        logError("Error on importing line: ", e)
        continue
    }
}