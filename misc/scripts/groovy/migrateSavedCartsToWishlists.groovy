import de.hybris.platform.core.model.product.ProductModel
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery
import org.apache.log4j.Level
import org.apache.log4j.Logger
import de.hybris.platform.wishlist2.model.Wishlist2EntryModel
import de.hybris.platform.wishlist2.model.Wishlist2Model;
import com.vanmarcke.services.wishlist2.impl.VMKWishlist2ServiceImpl
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeFormatter.*
import java.time.Instant;
import java.time.LocalDateTime;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException
import java.time.ZoneId
import java.util.*

LOGGER = Logger.getLogger "script.groovy.migrateSavedCarts"

def logInfo(message) {
  LOGGER.info(message)
  println(message)
}

def logError(message, e) {
  LOGGER.error(message, e)
  println(message + ": " + e.getStackTrace())
}

def addCartToMap(cartName, userId, map) {
  if (!map.containsKey(userId)) {
    map.put(userId, new HashSet<String>());
  }

  map.get(userId).add(cartName.toUpperCase());
}

def randomizeName(name, date) {
  LocalDateTime ldt = LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
  DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss")
  return String.format("%s (%s)", name, ldt.format(formatter))
}

protected String getNewWishListName() {
    return String.format("%s-%s", "created", System.currentTimeMillis());
}


flexibleSearchService = spring.getBean "flexibleSearchService"
modelService = spring.getBean "modelService"
vmkWishlist2Service = spring.getBean "vmkWishlist2Service"

def querySavedCarts =  """select {c.pk} from {cart as c}
                          where {c.saveTime} is NOT NULL
                          and NOT EXISTS({{SELECT {w.pk} from {Wishlist2 as w}
                                          where UPPER({w.name}) = UPPER({c.name})
                                          AND {w.user} = {c.user}
                                          }})"""

def query = new FlexibleSearchQuery(querySavedCarts)

carts=flexibleSearchService.search(query).getResult()
wishlistMap = new HashMap<String, HashSet>()

carts.each{cart ->
    try {
      def cartName = cart.getName();
      def userId = cart.getUser().getUid();

      // if name is null, generate a random wishlist name
      if (cartName == null) {
        cartName = getNewWishListName();
      }

      // check if user already has a cart with the same name that has been converted to wishlist
      else if (wishlistMap.containsKey(userId) && wishlistMap.get(userId).contains(cartName.toUpperCase())) {
        cartName =  randomizeName(cartName, cart.getSaveTime())
        logInfo(String.format("Cart name %s already in use by the same user, saving with new name %s", cart.getName(), cartName))
      }

      logInfo(String.format("converting cart with name: %s, code: %s, guid: %s, user: %s to wishlist with name: %s", cart.getName(), cart.getCode(), cart.getGuid(), userId, cartName));
      def newWishlist = vmkWishlist2Service.convertCartToWishlist(cartName, cart.getDescription(), cart, cart.getUser())
      addCartToMap(cartName, userId, wishlistMap)
    } catch (Exception e) {
      logError("Caught exception on converting cart code: " + cart.getCode(), e)
    }
}

return
