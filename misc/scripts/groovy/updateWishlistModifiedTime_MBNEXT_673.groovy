import de.hybris.platform.b2b.model.B2BUnitModel
import de.hybris.platform.core.model.media.MediaModel
import de.hybris.platform.core.model.product.ProductModel
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException
import de.hybris.platform.wishlist2.enums.Wishlist2EntryPriority
import de.hybris.platform.wishlist2.model.Wishlist2Model
import org.apache.log4j.Logger
import org.springframework.jdbc.datasource.DriverManagerDataSource
import org.springframework.jdbc.core.JdbcTemplate
import java.text.SimpleDateFormat
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery

LOGGER = Logger.getLogger "script.groovy.updateModifiedTimeWishlists"

flexibleSearchService = spring.getBean "flexibleSearchService"
modelService = spring.getBean "modelService"
vmkWishlist2Service = spring.getBean "vmkWishlist2Service"

DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource();
driverManagerDataSource.setDriverClassName(configurationService.getConfiguration().getString("db.driver"));
driverManagerDataSource.setUrl(configurationService.getConfiguration().getString("db.url"))
driverManagerDataSource.setUsername(configurationService.getConfiguration().getString("db.username"))
driverManagerDataSource.setPassword(configurationService.getConfiguration().getString("db.password"))

JdbcTemplate jdbcTemplate = new JdbcTemplate(driverManagerDataSource);

// date formats
sourceDateFormat = new SimpleDateFormat("yyyy-MM-dd-HH.mm.ss")
destinationDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")


// SET MEDIA HERE:
def MEDIA_FILE_NAME = "wishlist_migration_prod"

def logInfo(message, Object... params) {
    def stringMessage = String.format(message, params);
    LOGGER.info(stringMessage)
    println(stringMessage)
}

def getRowsFromMedia(String mediaCode) {
    MediaModel media = mediaService.getMedia(catalogVersionService.getCatalogVersion("globalBlueContentCatalog", "Online"), mediaCode)
    InputStream rowStream = mediaService.getStreamFromMedia(media)
    List<String> rows = new ArrayList<>();
    try {
        BufferedReader reader = new BufferedReader(new InputStreamReader(rowStream));
        while(reader.ready()) {
            String line = reader.readLine()
            rows.add(line)
        }

    } catch(IOException e) {
        logError("Problem reading file from media %s", e, mediaCode)
    }

    return rows
}

def updateModifiedTime(Wishlist2Model wishlist, Date newDate) {
	logInfo("Updating modified time of wishlist %s", wishlist.getPk())
	String sqlQuery = "UPDATE wishlist2 set modifiedTS = '" + destinationDateFormat.format(newDate) + "' where pk = '" + wishlist.getPk() + "'"
	logInfo("Running sql query: " + sqlQuery)
	jdbcTemplate.update(sqlQuery)
}

def getWishlist(String wishlistName, String userUid) {
	def stringQuery = "SELECT {pk} from {wishlist2 as w join user as u on {w.user} = {u.pk}} where {w.name} ='" + wishlistName +
			"' and {u.uid} = '" + userUid + "'"
	def query = new FlexibleSearchQuery(stringQuery)

	def result = flexibleSearchService.search(query).getResult()
	if (result != null && result.size() > 0) {
		return result.get(0)
	} 

	logInfo("Wishlist not found, name: %s, user: %s", wishlistName, userUid)
	return null
}


rows = getRowsFromMedia(MEDIA_FILE_NAME)
def sqlQueries = new HashSet<String>()
for (String line in rows) {
    logInfo("Updating modified time for line: %s", line)
    try {
    	List<String> rowValues = line.split(';')
    	def wishlist = getWishlist(rowValues.get(2), rowValues.get(1))
    	if (wishlist != null) {
	    	updateModifiedTime(wishlist, sourceDateFormat.parse(rowValues.get(5)))
    	}
    } catch(UnknownIdentifierException e) {
        logError("Error on importing line: ", e)
        continue
    }
}